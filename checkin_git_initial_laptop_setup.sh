#!/usr/bin/env bash
# Author: Pierre Gronau <Pierre.Gronau@ndaal.eu>
# Copyright 2024, 2025
# License: All content is licensed under the terms of the <MIT License>
# Developed on: Debian 12.x; macOS Sequoia x86 architecture
# Tested on: Debian 12.x; macOS Sequoia x86 architecture
#
# Exit on error. Append "|| true" if you expect an error.
set -o errexit
# This is equivalent to set -e. It causes the script to exit
# immediately if any command exits with a non-zero status.
#
# Exit on error inside any functions or subshells.
set -o errtrace
# This setting ensures that the ERR trap is inherited by shell functions,
# command substitutions, and commands executed in a subshell environment.
#
# Do not allow use of undefined vars. Use ${VAR:-} to use an undefined VAR
set -o nounset
# This is equivalent to set -u. It treats unset variables as
# an error when substituting.
#
# Catch the error in case mysqldump fails (but gzip succeeds) in `mysqldump |gzip`
# https://vaneyckt.io/posts/safer_bash
set -o pipefail
# This setting causes a pipeline to return the exit status of the last command
# in the pipe that returned a non-zero status.
#
# Turn on traces, useful while debugging but commented out by default
# set -o xtrace

# Set $IFS to only newline and tab.
#
# http://www.dwheeler.com/essays/filenames-in-shell.html
# nosemgrep: ifs-tampering
IFS=$'\n\t'

# Error trapping function
# Add this line before the error_handler function
# shellcheck disable=SC2317
error_handler() {
    local line_number="${1}"
    local error_code="${2}"
    local last_command="${BASH_COMMAND}"
    printf "Error: Error occurred in line %s (error code: %s)\n" "${line_number}" "${error_code}"
    printf "Error: Failed command: %s\n" "${last_command}"

    # Success case
    return 0
}

# Set the error trap
trap "error_handler ${LINENO} \$?" ERR

cleanup() {
    trap - SIGINT SIGTERM ERR EXIT
    printf "%b\n" "\nInfo: Cleanup is running ..."
    # Additional cleanup tasks can be added here if needed
    rm -f -v ./*.tmp
    printf "%b\n" "\nInfo: Cleanup finished ..."
}

trap cleanup SIGINT SIGTERM EXIT

if [[ "$(uname)" == "Darwin" ]]; then
    HOMEDIR="Users"
elif [[ "$(uname)" == "Linux" ]]; then
    HOMEDIR="home"
else
    printf "Error: Unsupported operating system: %s\n" "$(uname)"
    exit 1
fi
readonly HOMEDIR

# Under Linux you use `home` under macOS `Users`
printf "Info: Home directory: %s\n" "${HOMEDIR}"

USERSCRIPT="cloud"
readonly USERSCRIPT
# Your user! In which context it SHOULD run
printf "Info: User script: %s\n" "${USERSCRIPT}"

DIRDATE="$(date +"%Y-%m-%d")"
readonly DIRDATE
printf "Info: Current date: %s\n" "${DIRDATE}"

# Define constants
VERSION="0.3.7"
readonly VERSION
printf "Info: Current VERSION: %s\n" "${VERSION}"

SCRIPT_NAME="$(basename "${0}")"
readonly SCRIPT_NAME
printf "Info: Current SCRIPT_NAME: %s\n" "${SCRIPT_NAME}"

SCRIPT_PATH="$(realpath "$(dirname "${0}")")"
readonly SCRIPT_PATH
printf "Info: Current SCRIPT_PATH: %s\n" "${SCRIPT_PATH}"

SCRIPT_PATH_WITH_NAME="${SCRIPT_PATH}/${SCRIPT_NAME}"
readonly SCRIPT_PATH_WITH_NAME
printf "Info: Current SCRIPT_PATH_WITH_NAME: %s\n" "${SCRIPT_PATH_WITH_NAME}"

DESTINATION="/${HOMEDIR}/${USERSCRIPT}/repos/initial_laptop_setup/"
printf "Info: Destination Directory: %s\n" "${DESTINATION}"

EXECUTIONPATH="$(realpath "$(dirname "${0}")")"
printf "Info: Current EXECUTIONPATH: %s\n" "${EXECUTIONPATH}"

# Function to check if a command is available
check_command() {
    if ! command -v "${1}" &>/dev/null; then
        printf "Error: %s is not installed or not in PATH. Please install it and try again.\n" "${1}"
        return 1
    fi

    # Success case
    return 0
}

# Check for required commands
check_command "git" || exit 1
check_command "git-lfs" || exit 1
check_command "wget" || exit 1
check_command "curl" || exit 1
check_command "tokei" || exit 1
check_command "tree" || exit 1
check_command "sha512sum" || exit 1 
check_command "shellcheck" || exit 1
check_command "shfmt" || exit 1

create_directory() {
    local dir="${1}"
    printf "Info: Directory will be %s\n" "${dir}"
    if [[ ! -d ${dir} ]]; then
        mkdir -p -v "${dir}"
        printf "Info: Directory %s is created.\n" "${dir}"
        printf "Info: placeholder.txt will be created in %s\n" "${dir}"
        touch "${dir}/placeholder.txt"
        printf "Info: placeholder.txt will be removed from %s\n" "${dir}"
        rm -f -v "${dir}/placeholder.txt"
        printf "Info: placeholder.txt is removed from %s\n" "${dir}"
    else
        printf "Info: Directory %s already exists. No creation needed.\n" "${dir}"
    fi

    # Success case
    return 0
}

DIRECTORY="${DESTINATION}.build"
printf "Info: %s\n" "${DIRECTORY}"

create_directory "${DIRECTORY}"

DIRECTORY="${DESTINATION}.config"
printf "Info: %s\n" "${DIRECTORY}"

create_directory "${DIRECTORY}"

DIRECTORY="${DESTINATION}dep"
printf "Info: %s\n" "${DIRECTORY}"

create_directory "${DIRECTORY}"

DIRECTORY="${DESTINATION}documentation"
printf "Info: %s\n" "${DIRECTORY}"

create_directory "${DIRECTORY}"

DIRECTORY="${DESTINATION}example"
printf "Info: %s\n" "${DIRECTORY}"

create_directory "${DIRECTORY}"

DIRECTORY="${DESTINATION}res"
printf "Info: %s\n" "${DIRECTORY}"

create_directory "${DIRECTORY}"

DIRECTORY="${DESTINATION}test"
printf "Info: %s\n" "${DIRECTORY}"

create_directory "${DIRECTORY}"

DIRECTORY="${DESTINATION}tools"
printf "Info: %s\n" "${DIRECTORY}"

create_directory "${DIRECTORY}"

# Check if the create_functions_for_package_manager_cfg.sh script exists before executing
if [ -f "${DESTINATION}scripts/create_functions_for_package_manager_cfg.sh" ]; then
    printf "Info: Executing create_functions_for_package_manager_cfg.sh...\n"
    "${DESTINATION}scripts/create_functions_for_package_manager_cfg.sh" || true
else
    printf "Error: create_functions_for_package_manager_cfg.sh does not exist at %s.\n" "${DESTINATION}scripts/create_functions_for_package_manager_cfg.sh"
fi

#exit 0

Function_Git_Pull_Repos() {
    (
        printf "Info: \n"
        printf "Info: Git Pull from Repos\n"
        printf "Info: \n"

        #git branch main || true
        #branch_name="vPierre"
        #git branch "${branch_name}" || true
        #git checkout "${branch_name}" || true

        git config pull.rebase false || true
        git pull --verbose || true
    )

    # Success case
    return 0
}

Function_Git_Push_Repos() {
    printf "Info: \n"
    printf "Info: Git Push to Repos\n"
    printf "Info: \n"
    git add --verbose -A || true
    git commit -m "update initial macos laptop setup" || true
    #git commit -m "update # nosemgrep: ifs-tampering" || true
    git push origin HEAD:main || true
    git push --verbose --force || true

    # Success case
    return 0
}

Function_Create_Default_Information_for_Repos() {
    printf "Info: update structure.txt\n"
    tree >./"structure.txt" || exit
    cat ./"structure.txt" || exit
    printf "Info: update content_summary.txt\n"
    tokei ---sort code --no-ignore-parent --no-ignore-vcs --no-ignore-dot >"content_summary.txt" || exit
    cat ./"content_summary.txt" || exit
    printf "Info: update content_long_list.txt\n"
    tokei ---sort code --files --no-ignore-parent --no-ignore-vcs --no-ignore-dot >"content_long_list.txt" || exit
    cat ./"content_long_list.txt" || exit

    # Success case
    return 0
}

Function_Create_Default_Files_for_Repos() {
    printf "Info: Create Default Files for Repos\n"
    wget --secure-protocol=auto --https-only --continue --verbose -N --tries=10 --check-certificate "https://ndaal.eu/.well-known/security.txt" || true
    wget --secure-protocol=auto --https-only --continue --verbose -N --tries=10 --check-certificate "https://gitlab.com/ndaal_open_source/ndaal_public_git_ignore/-/blob/453358018843aae46da5be681a4635d24f7637c4/.gitignore" || true

    printf "Info: Create CODE_OF_CONDUCT Files for Repos\n"
    wget --secure-protocol=auto --https-only --continue --verbose -N --tries=10 --check-certificate --output-document "CODE_OF_CONDUCT.txt" "https://www.contributor-covenant.org/version/2/1/code_of_conduct/code_of_conduct.txt"
    wget --secure-protocol=auto --https-only --continue --verbose -N --tries=10 --check-certificate --output-document "CODE_OF_CONDUCT.md" "https://www.contributor-covenant.org/version/2/1/code_of_conduct/code_of_conduct.md"
    wget --secure-protocol=auto --https-only --continue --verbose -N --tries=10 --check-certificate --output-document "CODE_OF_CONDUCT_DE.txt" "https://www.contributor-covenant.org/de/version/2/1/code_of_conduct/code_of_conduct.txt"
    wget --secure-protocol=auto --https-only --continue --verbose -N --tries=10 --check-certificate --output-document "CODE_OF_CONDUCT_DE.md" "https://www.contributor-covenant.org/de/version/2/1/code_of_conduct/code_of_conduct.md"

    printf "Info: Create robots.txt for Repos\n"
    wget --secure-protocol=auto --https-only --continue --verbose -N --tries=10 --check-certificate --output-document "robots.txt" "https://gitlab.com/vPierre/ndaal_public_robots_txt/-/raw/main/robots.txt"
    wget --secure-protocol=auto --https-only --continue --verbose -N --tries=10 --check-certificate --output-document "${DESTINATION}documentation/robots.txt" "https://gitlab.com/vPierre/ndaal_public_robots_txt/-/raw/main/robots.txt"

    printf "Info: Create Vulnerability Disclosure Policy.md for Repos\n"
    wget --secure-protocol=auto --https-only --continue --verbose -N --tries=10 --check-certificate --output-document "Vulnerability_Disclosure_Policy.md" "https://gitlab.com/vPierre/ndaal_public_vulnerability_disclosure_policy/-/raw/main/Vulnerability_Disclosure_Policy.md?ref_type=heads"

    # Success case
    return 0
}

Function_Remove_Unwished_Files() {
    printf "\nInfo: Remove Unwished Files\n"
    printf "Info: Currently, files with these extensions are considered temporary files:\n"
    printf '%s\n' "Info: thumbs.db, .bak, ~, .tmp, .temp, .DS_Store, .crdownload, .part, .cache, .dmp, .download, .partial,"
    printf '%s\n' "Info: .swp, .log, .old, .$$$, .wbk, .xlk, .~lock, .lck, .err, .chk, .sik, .crash, .temp$, .bup, .save"
    printf "Warning: Some of these files may be important for recovery or debugging.\n"
    printf "Info: Always ensure you have backups before performing any mass deletion of files.\n\n"

    printf '%s\n' "Info: remove .DS_Store (macOS desktop services store)"
    find . -name ".DS_Store" -exec rm -rf {} \; || true
    printf '%s\n' "Info: remove thumbs.db and Thumbs.db (Windows thumbnail cache)"
    find . -name "thumbs.db" -exec rm -rf {} \; || true
    find . -name "Thumbs.db" -exec rm -rf {} \; || true
    printf '%s\n' "Info: remove .crdownload (Chrome download temp files)"
    find . -name ".crdownload" -exec rm -rf {} \; || true
    printf '%s\n' "Info: remove .download (Generic download temp files)"
    find . -name ".download" -exec rm -rf {} \; || true
    printf '%s\n' "Info: remove .partial (Firefox/Mozilla partial download files)"
    find . -name ".partial" -exec rm -rf {} \; || true
    printf '%s\n' "Info: remove .cache (Generic cache files)"
    find . -name ".cache" -exec rm -rf {} \; || true
    printf '%s\n' "Info: remove .dmp (Memory dump files)"
    find . -name ".dmp" -exec rm -rf {} \; || true
    printf '%s\n' "Info: remove .tmp and .temp (Generic temporary files)"
    find . -name ".tmp" -exec rm -rf {} \; || true
    find . -name ".temp" -exec rm -rf {} \; || true
    printf '%s\n' "Info: remove .bak (Generic backup files)"
    find . -name ".bak" -exec rm -rf {} \; || true
    printf '%s\n' "Info: remove .backup (Generic backup files)"
    find . -name ".backup" -exec rm -rf {} \; || true
    printf '%s\n' "Info: remove .log (Log files, use with caution)"
    find . -name ".log" -exec rm -rf {} \; || true
    printf '%s\n' "Info: remove .swp (Vim swap files)"
    find . -name "*.swp" -exec rm -rf {} \; || true
    printf '%s\n' "Info: remove .old (Old version files)"
    find . -name "*.old" -exec rm -rf {} \; || true
    printf '%s\n' "Info: remove .$$$ (Temporary files used by some programs)"
    find . -name "*.$$$" -exec rm -rf {} \; || true
    printf '%s\n' "Info: remove .wbk (Word backup files)"
    find . -name "*.wbk" -exec rm -rf {} \; || true
    printf '%s\n' "Info: remove .xlk (Excel backup files)"
    find . -name "*.xlk" -exec rm -rf {} \; || true
    printf '%s\n' "Info: remove .~lock (LibreOffice lock files)"
    find . -name ".~lock*" -exec rm -rf {} \; || true
    printf '%s\n' "Info: remove .lck (Generic lock files)"
    find . -name "*.lck" -exec rm -rf {} \; || true
    printf '%s\n' "Info: remove .err (Error log files)"
    find . -name "*.err" -exec rm -rf {} \; || true
    printf '%s\n' "Info: remove .chk (Checkpoint files)"
    find . -name "*.chk" -exec rm -rf {} \; || true
    printf '%s\n' "Info: remove .sik (Backup files used by some Adobe products)"
    find . -name "*.sik" -exec rm -rf {} \; || true
    printf '%s\n' "Info: remove .crash (Crash dump files)"
    find . -name "*.crash" -exec rm -rf {} \; || true
    printf '%s\n' "Info: remove .temp$ (Another variant of temp files)"
    find . -name "*.temp$" -exec rm -rf {} \; || true
    printf '%s\n' "Info: remove .bup (Backup files)"
    find . -name "*.bup" -exec rm -rf {} \; || true
    printf '%s\n' "Info: remove .save (Saved versions of files)"
    find . -name "*.save" -exec rm -rf {} \; || true

    printf "\nInfo: Removal process completed. Please review any error messages carefully.\n"

    # Success case
    return 0
}

Function_Create_Checksums_for_Scripts() {
    printf '%s\n' "Info: Create checksums files"
    printf '%s\n' "Warning: Some of these files may be important for verification or integrity checks."
    printf '%s\n' "Info: Always ensure you have backups before performing any mass deletion of files."
    printf '%s\n\n' ""

    # Remove .md5 files
    printf '%s\n' "Info: remove .md5 (MD5 hash files)"
    find . -name "*.md5" -print0 | xargs -0 rm -rf || true

    # Remove .sha-3-512 files
    printf '%s\n' "Info: remove .sha-3-512 (SHA-3 512-bit hash files)"
    find . -name "*.sha-3-512" -print0 | xargs -0 rm -rf || true

    # Remove .sha3-512 files
    printf '%s\n' "Info: remove .sha3-512 (SHA-3 512-bit hash files)"
    find . -name "*.sha3-512" -print0 | xargs -0 rm -rf || true

    # Remove .sha512 files
    printf '%s\n' "Info: remove .sha512 (SHA-512 hash files)"
    find . -name "*.sha512" -print0 | xargs -0 rm -rf || true

    # Remove .sha1 files
    printf '%s\n' "Info: remove .sha1 (SHA-1 hash files)"
    find . -name "*.sha1" -print0 | xargs -0 rm -rf || true

    # Remove .sha256 files
    printf '%s\n' "Info: remove .sha256 (SHA-256 hash files)"
    find . -name "*.sha256" -print0 | xargs -0 rm -rf || true

    # Remove .twoxhash files
    printf '%s\n' "Info: remove .twoxhash (TwoX hash files)"
    find . -name "*.twoxhash" -print0 | xargs -0 rm -rf || true

    # Remove .k12 files
    printf '%s\n' "Info: remove .k12 (KangarooTwelve hash files)"
    find . -name "*.k12" -print0 | xargs -0 rm -rf || true

    # Remove .blake2 files
    printf '%s\n' "Info: remove .blake2 (BLAKE2 hash files)"
    find . -name "*.blake2" -print0 | xargs -0 rm -rf || true

    # Remove .blake2b files
    printf '%s\n' "Info: remove .blake2 (BLAKE2 hash files)"
    find . -name "*.blake2b" -print0 | xargs -0 rm -rf || true

    # Remove .blake3 files
    printf '%s\n' "Info: remove .blake3 (BLAKE3 hash files)"
    find . -name "*.blake3" -print0 | xargs -0 rm -rf || true

    # Remove .crc32 files
    printf '%s\n' "Info: remove .crc32 (CRC32 checksum files)"
    find . -name "*.crc32" -print0 | xargs -0 rm -rf || true

    # Create SHA-3 512 checksums for .sh files
    find "${DESTINATION}" -type f -name "*.sh" -print0 |
        xargs -0 -n 1 sh -c "sha512sum \"\$1\" | awk '{print \$1}' | tee \"\$1.sha3-512\"" _

    # Create SHA-3 512 checksums for .cfg files
    find "${DESTINATION}" -type f -name "*.cfg" -print0 |
        xargs -0 -n 1 sh -c "sha512sum \"\$1\" | awk '{print \$1}' | tee \"\$1.sha3-512\"" _

    # Success case
    return 0
}

Function_Create_Checksums_for_Markdown() {
    printf "Info: Create checksums files \n"
    printf "%s\n" "${DESTINATION}"

    find "${DESTINATION}" -type f -name "*.md" -print0 |
        xargs -0 -n 1 sh -c "sha512sum \"\$1\" | awk '{print \$1}' | tee \"\$1.sha3-512\"" _

    # Success case
    return 0
}

Function_Create_Checksums_for_reStructuredText() {
    printf "Info: Create checksums files \n"
    printf "%s\n" "${DESTINATION}"

    find "${DESTINATION}" -type f -name "*.rst" -print0 |
        xargs -0 -n 1 sh -c "sha512sum \"\$1\" | awk '{print \$1}' | tee \"\$1.sha3-512\"" _

    # Success case
    return 0
}

create_sbom() {
    local SBOM_CREATE="${1}" # Accept the script path as a parameter

    # Check if the script exists before executing
    if [ -f "${SBOM_CREATE}" ]; then
        printf "Info: Executing create_sbom_and_check_for_vulnerabilties.sh...\n"

        # Execute the script
        if ! bash "${SBOM_CREATE}" "${EXECUTIONPATH}"; then
            printf "Error: Failed to execute create_sbom_and_check_for_vulnerabilties.sh\n"
            return 1
        fi

        # Print the destination directory
        printf "Info: Destination Directory: %s\n" "${DESTINATION}"

        # Check if sbom.json exists in the DESTINATION directory
        if [ -f "${DESTINATION}/sbom.json" ]; then
            printf "Info: sbom.json exists in the destination directory: %s\n" "${DESTINATION}"
            return 0
        else
            printf "Warning: sbom.json does not exist in the destination directory: %s\n" "${DESTINATION}"
            return 1
        fi
    else
        printf "Error: create_sbom_and_check_for_vulnerabilties.sh does not exist at %s.\n" "${SBOM_CREATE}"
        return 1
    fi
}

cd "${DESTINATION}" || exit
printf "Info: Changed to the desired Directory: %s\n" "${DESTINATION}"

# Print a message indicating that a read-only array of required files is being defined
printf "Info: Defining a read-only array of required files:\n"

# Define a read-only array of required files
declare -r required_files=(
    "git_credential.sh"
    "git_config.sh"
)

# Check if the destination directory exists
printf "Info: Destination Directory for array of required_files is: %s\n" "${DESTINATION}tools"
if [ -d "${DESTINATION}tools" ]; then
    # Loop through the array and copy the files
    for file in "${required_files[@]}"; do
        cp -f -p -v "/${HOMEDIR}/${USERSCRIPT}/repos/scripts/${file}" "${DESTINATION}tools" || true
    done
else
    printf "Error: Directory %stools does not exist. Skipping file copy.\n" "${DESTINATION}tools"
fi

# Define a read-only array of required files
declare -r required_files_for_vagrant=(
    "create_VMs_with_Vagrant_and_roboxes.sh"
    "create_VMs_with_Vagrant_and_jborean93.sh"
    "create_VMs_with_Vagrant.sh"
)

printf "Info: create VMs with Vagrant:\n"
printf "Info: vagrant up scripts: %s\n" "${required_files_for_vagrant[@]}"

# Check if the destination directory exists
printf "Info: Source Directory for array of required_files_for_vagrant is: %s\n" "/${HOMEDIR}/${USERSCRIPT}"
printf "Info: Destination Directory for array of required_files_for_vagrant is: %s\n" "${DESTINATION}scripts"

if [ -d "${DESTINATION}scripts" ]; then
    # Loop through the array and copy the files
    for file in "${required_files_for_vagrant[@]}"; do
        printf "Info: Source Directory for array of required_files_for_vagrant is: %s\n" "/${HOMEDIR}/${USERSCRIPT}${file}"
        cp -f -p -v "/${HOMEDIR}/${USERSCRIPT}/${file}" "${DESTINATION}/scripts" || true
    done
else
    printf "Error: Directory %s scripts does not exist. Skipping file copy.\n" "${DESTINATION}scripts"
fi

cp -f -p -v "/${HOMEDIR}/${USERSCRIPT}/create_VMs_with_Vagrant.sh" "${DESTINATION}/scripts/create_VMs_with_Vagrant_Hyper_Hyper.sh" || true
cp -f -p -v "/${HOMEDIR}/${USERSCRIPT}/create_VMs_with_Vagrant.sh" "/${HOMEDIR}/${USERSCRIPT}/create_VMs_with_Vagrant_${DIRDATE}.sh" || true

DIRECTORY="/${HOMEDIR}/${USERSCRIPT}/repos/initial_laptop_setup/documentation/source/_scripts"
printf "Info: %s\n" "${DIRECTORY}"

create_directory "${DIRECTORY}"

DIRECTORY="/${HOMEDIR}/${USERSCRIPT}/repos/initial_laptop_setup/documentation/source/_cfg"
printf "Info: %s\n" "${DIRECTORY}"

create_directory "${DIRECTORY}"

DIRECTORY="/${HOMEDIR}/${USERSCRIPT}/repos/initial_laptop_setup/documentation/source/_FAQs"
printf "Info: %s\n" "${DIRECTORY}"

create_directory "${DIRECTORY}"

DIRECTORY="/${HOMEDIR}/${USERSCRIPT}/repos/initial_laptop_setup/documentation/source/_FAQs/_static"
printf "Info: %s\n" "${DIRECTORY}"

create_directory "${DIRECTORY}"

DIRECTORY="/${HOMEDIR}/${USERSCRIPT}/repos/initial_laptop_setup/documentation/source/_root_cause"
printf "Info: %s\n" "${DIRECTORY}"

create_directory "${DIRECTORY}"

DIRECTORY="/${HOMEDIR}/${USERSCRIPT}/repos/initial_laptop_setup/documentation/source/_root_cause/_static"
printf "Info: %s\n" "${DIRECTORY}"

create_directory "${DIRECTORY}"

DIRECTORY="/${HOMEDIR}/${USERSCRIPT}/repos/initial_laptop_setup/documentation/source/_appendix"
printf "Info: %s\n" "${DIRECTORY}"

create_directory "${DIRECTORY}"

DIRECTORY="/${HOMEDIR}/${USERSCRIPT}/repos/initial_laptop_setup/documentation/source/_appendix/_static"
printf "Info: %s\n" "${DIRECTORY}"

create_directory "${DIRECTORY}"

DIRECTORY="/${HOMEDIR}/${USERSCRIPT}/repos/initial_laptop_setup/documentation/source/_components"
printf "Info: %s\n" "${DIRECTORY}"

create_directory "${DIRECTORY}"

DIRECTORY="/${HOMEDIR}/${USERSCRIPT}/repos/initial_laptop_setup/documentation/source/_components/_static"
printf "Info: %s\n" "${DIRECTORY}"

create_directory "${DIRECTORY}"

DIRECTORY="/${HOMEDIR}/${USERSCRIPT}/repos/initial_laptop_setup/documentation/source/_ADRs"
printf "Info: %s\n" "${DIRECTORY}"

create_directory "${DIRECTORY}"

DIRECTORY="/${HOMEDIR}/${USERSCRIPT}/repos/initial_laptop_setup/documentation/source/_ADRs/_static"
printf "Info: %s\n" "${DIRECTORY}"

create_directory "${DIRECTORY}"

DIRECTORY="/${HOMEDIR}/${USERSCRIPT}/repos/initial_laptop_setup/documentation/source/_GOSS"
printf "Info: %s\n" "${DIRECTORY}"

create_directory "${DIRECTORY}"

DIRECTORY="/${HOMEDIR}/${USERSCRIPT}/repos/initial_laptop_setup/documentation/source/_GOSS/_static"
printf "Info: %s\n" "${DIRECTORY}"

create_directory "${DIRECTORY}"

DIRECTORY="/${HOMEDIR}/${USERSCRIPT}/repos/initial_laptop_setup/documentation/source/_arc42"
printf "Info: %s\n" "${DIRECTORY}"

create_directory "${DIRECTORY}"

DIRECTORY="/${HOMEDIR}/${USERSCRIPT}/repos/initial_laptop_setup/documentation/source/_arc42/_static"
printf "Info: %s\n" "${DIRECTORY}"

create_directory "${DIRECTORY}"

DIRECTORY="/${HOMEDIR}/${USERSCRIPT}/repos/initial_laptop_setup/documentation/source/_hardening"
printf "Info: %s\n" "${DIRECTORY}"

create_directory "${DIRECTORY}"

cd "${DIRECTORY}" || exit
printf "Info: Changed to the desired Directory: %s\n" "${DIRECTORY}"

wget --secure-protocol=auto --https-only --continue --verbose -N --tries=10 --check-certificate "https://raw.githubusercontent.com/RedHatOfficial/ansible-role-rhel9-cis/refs/heads/main/tasks/main.yml"

DIRECTORY="/${HOMEDIR}/${USERSCRIPT}/repos/initial_laptop_setup/documentation/source/_hardening/_static"
printf "Info: %s\n" "${DIRECTORY}"

create_directory "${DIRECTORY}"


DIRECTORY="/${HOMEDIR}/${USERSCRIPT}/repos/initial_laptop_setup/documentation/source/_extensions"
printf "Info: %s\n" "${DIRECTORY}"

create_directory "${DIRECTORY}"

DIRECTORY="/${HOMEDIR}/${USERSCRIPT}/repos/initial_laptop_setup/documentation/source/_appendix"
printf "Info: %s\n" "${DIRECTORY}"

cd "${DIRECTORY}" || exit
printf "Info: Changed to the desired Directory: %s\n" "${DIRECTORY}"

wget --secure-protocol=auto --https-only --continue --verbose -N --tries=10 --check-certificate "https://raw.githubusercontent.com/open-gitops/documents/refs/heads/main/i18n/GLOSSARY_de.md" || true
wget --secure-protocol=auto --https-only --continue --verbose -N --tries=10 --check-certificate "https://raw.githubusercontent.com/open-gitops/documents/refs/heads/main/GLOSSARY.md" || true

DIRECTORY="/${HOMEDIR}/${USERSCRIPT}/repos/initial_laptop_setup/documentation/source/_appendix/_static"
printf "Info: %s\n" "${DIRECTORY}_static"

create_directory "${DIRECTORY}"

SOURCE="${DESTINATION}/documentation/source/"
readonly SOURCE
printf "Info: %s\n" "${SOURCE}"

cd "${SOURCE}_static/" || exit
printf "Info: Changed to the desired Directory: %s\n" "${SOURCE}_static/"

# Declare a readonly array of URLs
# shellcheck disable=SC2034
declare -r -a URLS=(
    "https://objective-see.org/downloads/MacMalware_2025.pdf"
    "https://github.com/free-icons/free-icons/blob/master/svgs/brands-square-git.svg"
    "https://github.com/ansible/logos/blob/main/vscode-ansible-logo/vscode-ansible.png"
    "https://www.debian.org/logos/openlogo-nd-100.png"
    "https://brandslogos.com/wp-content/uploads/images/large/git-logo.png"
    "https://brandslogos.com/wp-content/uploads/images/large/jenkins-logo-1.png"
    "https://www.malwarebytes.com/wp-content/uploads/sites/2/2017/09/yaralogo.jpg"
    "https://upload.wikimedia.org/wikipedia/commons/9/9a/Podman-logo-orig.png"
    "https://logos-download.com/wp-content/uploads/2016/10/Java_logo.png"
    "https://raw.githubusercontent.com/bridgecrewio/checkov/main/docs/web/images/checkov_blue_logo.png"
    "https://github.com/koalaman/shellcheck/blob/master/doc/shellcheck_logo.svg"
    "https://curl.se/logo/curl-logo.png"
    "https://lafibre.info/images/logo/logo_wget.png"
    "https://brsmedia.in/wp-content/uploads/2022/06/VSCodium.png"
    "https://it-sziget.hu/sites/default/files/2017/12/25/vagrant-vertical-color.png"
    "https://upload.wikimedia.org/wikipedia/commons/d/d5/Virtualbox_logo.png"
    "https://cdn.freelogovectors.net/wp-content/uploads/2023/08/packer_logo-freelogovectors.net_-640x400.png"
    "https://raw.githubusercontent.com/rust-lang/www.rust-lang.org/master/static/images/rust-social-wide-light.svg"
    "https://github.com/rust-lang/cargo/blob/master/src/doc/src/images/Cargo-Logo-Small.png"
    "https://i.ytimg.com/vi/KhQqp9GItnc/maxresdefault.jpg"
    "https://github.com/goranvasic/svg-logos/blob/main/vagrant-dark-logo.svg"
    "https://github.com/goranvasic/svg-logos/blob/main/ansible-dark-logo.svg"
    "https://logos-download.com/wp-content/uploads/2016/10/Ansible_logo-700x700.png"
    "https://github.com/goranvasic/svg-logos/blob/main/bash-dark-logo.svg"
    "https://cdn.dribbble.com/users/1285930/screenshots/4040291/bash-logo-by-vd.png"
    "https://github.com/goranvasic/svg-logos/blob/main/bitbucket-logo.svg"
    "https://leninmhs.com/wp-content/uploads/2023/11/que-es-bitbucket-1024x617.png"
    "https://github.com/goranvasic/svg-logos/blob/main/gitlab-logo.svg"
    "https://static-00.iconduck.com/assets.00/gitlab-icon-1024x942-f30d1qro.png"
    "https://github.com/goranvasic/svg-logos/blob/main/github-logo.svg"
    "https://logos-download.com/wp-content/uploads/2016/09/GitHub_logo.png"
    "https://github.com/goranvasic/svg-logos/blob/main/rhel-logo.svg"
)

# Loop through the array and download each file
for url in "${URLS[@]}"; do
    (
        printf "Info: Processing URL: %s\n" "${url}"
        wget --secure-protocol=auto --https-only --continue --verbose -N --tries=1 --check-certificate "${url}" || true
    )
done

wget --secure-protocol=auto --https-only --continue --verbose -N --tries=10 --check-certificate --output-document "ClamAV_logo.png" "https://raw.githubusercontent.com/micahsnyder/clamav-documentation/main/src/images/logo.png" || true

for image in *.svg; do
    (
    printf "image %s converted to %s\n" "${image}" "${image%.svg}.png"
	#convert  "${image}"  "${image%.svg}.png" || true
	)
done

for image in *.jpg; do
    (
    printf "image %s converted to %s\n" "${image}" "${image%.jpg}.png"
	#convert  "${image}"  "${image%.jpg}.png" || true
	)
done 

#exit 1

for file in *.svg; do
    
    png_file="${file%.svg}.png"

    if [ ! -f "${png_file}" ]; then
        printf "image %s converted to %s\n" "${file}" "${file%.svg}.png"
        #magick -define svg:renderer=rsvg "${file}" "${file%.svg}.png" || true
        ##magick -define svg:renderer=rsvg -size 800x600 "${file}" -quality 90 "${file%.svg}.png" || true
    fi
    
    #exit 1

    if [ ! -f "${png_file}" ]; then
        printf "image %s converted to %s\n" "${file}" "${file%.svg}.png"
        #inkscape "${file}" --export-type=png --export-filename="${png_file}" || true
    fi
done

#exit 1

cd "${SOURCE}" || exit
printf "Info: Changed to the desired Directory: %s\n" "${SOURCE}"

Function_Emptying_Directories () {
printf "Info: remove _static/* files to prevent duplettes\n"
for n in "${array[@]}"
do
    (
    printf "Emptying Directory %s\n" "${n}"
    DIRECTORY="${SOURCE}_${n}/"
    printf "Info: %s\n" "${DIRECTORY}"

    if [ -d "${DIRECTORY}" ]; then
      (
      # Control will enter here if $DIRECTORY exist.
      printf "Warning: emptying of directory %s in progress\n" "${SOURCE}_${n}/"
      rm -f -v "${DIRECTORY}"* || true
      )
    fi
    )
done
}

Function_Copy_Content () {
for n in "${array[@]}"
do
    (
    printf "Copy Content ../_static to %s\n" "${n}"
    DIRECTORY="_${n}"
    
    if [ -d "${DIRECTORY}" ]; then
      (
      # Control will enter here if $DIRECTORY exist.
      printf "%s\n" "${DIRECTORY}"
      printf "%s in progress\n" "${SOURCE}${DIRECTORY}"
      printf "%s is Source\n" "${SOURCE}"
      printf "%s is Source with _static\n" "${SOURCE}_static/"
      xcp -v ${SOURCE}_static/* "${SOURCE}${DIRECTORY}/"
      )
    fi
    )
done
}

# declare an array called array
declare -a array
array=(
"abstract/_static"
"document/_static"
"legal/_static"
"example/_DE/_static"
"example/_EN/_static"
"example/_ES/_static"
"appendix/_static"
"components/_static"
"FAQs/_static"
"root_cause/_static"
"arc42/_static"
"hardening/_static"
"ADRs/_static"
"GOSS/_static"
)

# declare an array called array
declare -a substitute
substitute=(
"document"
"legal"
) 

documentation="Systemhärtung, auch als System Hardening.rst"

cp -f -p -v "/${HOMEDIR}/${USERSCRIPT}/downloads/${documentation}" "${DESTINATION}/scripts"
cp -f -p -v "/${HOMEDIR}/${USERSCRIPT}/downloads/${documentation}" "${DESTINATION}/documentation/source/Systemhärtung.rst"
cp -f -p -v "${DESTINATION}/scripts/Einführung.rst" "${DESTINATION}/documentation/source/Einführung.rst"

cp -f -p -v "${DESTINATION}/scripts/"*.cfg "/${HOMEDIR}/${USERSCRIPT}/repos/initial_laptop_setup/documentation/source/_cfg"
cp -f -p -v "${DESTINATION}/scripts/"*.sh "/${HOMEDIR}/${USERSCRIPT}/repos/initial_laptop_setup/documentation/source/_scripts"
cp -f -p -v "/${HOMEDIR}/${USERSCRIPT}/repos/ndaal_public_eicar_test_files/create_sbom_and_check_for_vulnerabilties.sh" "/${HOMEDIR}/${USERSCRIPT}/repos/initial_laptop_setup/documentation/source/_scripts"

cd "${SOURCE}" || exit
printf "Info: Changed to the desired Directory: %s\n" "${SOURCE}"

Function_Emptying_Directories
Function_Copy_Content

#exit 0

cd "${DESTINATION}/documentation" || exit

make clean || true
make html || true
#make singlehtml || true
#make epub || true
#make htmlhelp || true
#make markdown || true
make latexpdf || true 

#exit 0

git --no-pager config --list
# Check if the git_credential.sh script exists before executing
if [ -f "${DESTINATION}tools/git_credential.sh" ]; then
    printf "Info: Executing git_credential.sh...\n"
    "${DESTINATION}tools/git_credential.sh" || true
else
    printf "Error: git_credential.sh does not exist at %s.\n" "${DESTINATION}tools/git_credential.sh"
fi

# Check if the git_config.sh script exists before executing
if [ -f "${DESTINATION}tools/git_config.sh" ]; then
    printf "Info: Executing git_config.sh...\n"
    "${DESTINATION}tools/git_config.sh" || true
else
    printf "Error: git_config.sh does not exist at %s.\n" "${DESTINATION}tools/git_config.sh"
fi

git --no-pager config --list

Function_Git_Pull_Repos
Function_Create_Default_Files_for_Repos
Function_Remove_Unwished_Files
Function_Create_Checksums_for_Markdown
Function_Create_Checksums_for_reStructuredText
Function_Create_Checksums_for_Scripts
git repack -a -d -f --depth=250 --window=250

# Define the path to the SBOM creation script
SBOM_CREATE="/${HOMEDIR}/${USERSCRIPT}/repos/ndaal_public_eicar_test_files/create_sbom_and_check_for_vulnerabilties.sh"
printf "Info: Current SBOM_CREATE: %s\n" "${SBOM_CREATE}"

# Call the function with the script path as an argument
create_sbom "${SBOM_CREATE}"

# Capture the return status
status="${?}"

# Optionally, you can handle the return status here
if [ "${status}" -eq 0 ]; then
    printf "Info: SBOM creation was successful.\n"
else
    printf "Error: SBOM creation failed.\n"
fi

Function_Create_Default_Information_for_Repos
Function_Git_Push_Repos

printf "Info: Processing completed.\n"

cleanup

script_name1="$(basename "${0}")"
printf "\nInfo: script_name1: %s\n" "${script_name1}"
script_path1="$(realpath "$(dirname "${0}")")"
printf "Info: script_path1: %s\n" "${script_path1}"
script_path_with_name="${script_path1}/${script_name1}"
printf "Info: Script path with name: %s\n" "${script_path_with_name}"
printf "Info: Script finished\n"
exit 0
