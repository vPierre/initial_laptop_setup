******************************************************************************
Systemhärtung - Verzeichnisse
******************************************************************************

.. sectionauthor:: Pierre Gronau <Pierre.Gronau@ndaal.eu>

.. contents:: Inhalt - Sicherheit - Container
   :depth: 3

Verzeichnisse in Linux gemäß POSIX
------------------------------------------------------------------------------

Die verschiedenen **Verzeichnisse** in Linux [1]_ dienen unterschiedlichen
Zwecken und folgen einer hierarchischen Struktur. Hier ist eine Erklärung
der wichtigsten **POSIX-Verzeichnisse** [2]_ :

/ (Root)
==============================================================================

- Das Wurzelverzeichnis des gesamten Dateisystems [3]_

- Ausgangspunkt für alle anderen Verzeichnisse

/bin (Binaries)
==============================================================================

- Enthält essentielle Systemprogramme und Befehle, die für alle Benutzer
  zugänglich sind

- Beinhaltet grundlegende Befehle wie ``ls``, ``cp``, ``mv``, und ``bash``

- Wird beim Booten und für grundlegende Systemoperationen benötigt

/sbin (System Binaries)
==============================================================================

- Enthält systemkritische Programme, hauptsächlich für Systemadministratoren

- Beinhaltet Befehle wie ``fdisk``, ``init``, und ``ifconfig``

- Programme hier erfordern oft Root-Rechte zur Ausführung

/etc (Etcetera)
==============================================================================

- Enthält systemweite Konfigurationsdateien

Beispiele sind Netzkonfigurationen, Benutzerdatenbanken und Startskripte

/home
==============================================================================

- Enthält die persönlichen Verzeichnisse der Benutzer

- Jeder Benutzer hat normalerweise ein Unterverzeichnis hier

/tmp (Temporary)
==============================================================================

- Für temporäre Dateien

- Wird typischwerweise oft beim Neustart geleert

/usr (Unix System Resources)
==============================================================================

- Enthält den Großteil der Benutzeranwendungen und -daten

- Beinhaltet Unterverzeichnisse wie ``/usr/bin``, ``/usr/lib``, ``/usr/local``

/var (Variable)
==============================================================================

- Enthält Dateien, deren Inhalt sich häufig ändert

- Beinhaltet Logdateien, Spools und temporäre Dateien

/dev (Devices)
==============================================================================

- Enthält spezielle Gerätedateien, die Hardware repräsentieren

Beispiele sind Festplatten, Terminals und Drucker

/lib (Libraries)
==============================================================================

- Enthält gemeinsam genutzte Bibliotheken, die von Programmen in
  ``/bin`` und ``/sbin`` benötigt werden

/opt (Optional)
==============================================================================

- Für die Installation optionaler Softwarepakete
- Oft von Drittanbietern genutzt

Diese Struktur hilft bei der Organisation von Systemprogrammen, 
Konfigurationsdateien und Benutzerdaten, wobei die Trennung zwischen 
systemkritischen und benutzerorientierten Bereichen sowie zwischen Basis- und
zusätzlicher Software beibehalten wird.

Verzeichnisse in Linux - /bin, /sbin, /usr/local/bin und /usr/local/sbin
------------------------------------------------------------------------------

Hier ist eine ausführliche Erklärung der Unterschiede
zwischen ``/bin``, ``/sbin``, ``/usr/local/bin`` und ``/usr/local/sbin``:

/bin und /sbin
==============================================================================

.. Rubric:: **/bin** (Binaries)

* Enthält essentielle Systemprogramme und Befehle, die für alle Benutzer
  zugänglich sind

* Beinhaltet grundlegende Befehle wie ``ls``, ``cp``, ``mv``, und ``bash``

* Wird beim Booten und für grundlegende Systemoperationen benötigt

.. Rubric:: **/sbin** (System Binaries)

* Enthält systemkritische Programme, hauptsächlich für Systemadministratoren

* Beinhaltet Befehle wie ``fdisk``, ``init``, und ``ifconfig``

* Programme hier erfordern oft Root-Rechte zur Ausführung

/usr/local/bin und /usr/local/sbin
==============================================================================

.. Rubric:: **/usr/local/bin**

* Für lokal kompilierte und installierte Programme, die für alle Benutzer
  verfügbar sein sollen

* Nicht Teil des Betriebssystems, sondern von Systemadministratoren
  oder Benutzern hinzugefügt

* Hat eine niedrigere Priorität in der PATH-Variable
  als ``/bin`` und ``/usr/bin``

.. Rubric:: **/usr/local/sbin**

* Ähnlich wie ``/usr/local/bin``, aber für systemadministrative Programme

* Für lokal installierte Programme, die Administratorrechte erfordern

* Nicht im Standard-PATH normaler Benutzer enthalten

Hauptunterschiede
==============================================================================

1. Systemrelevanz:
    - */bin* und */sbin*: Teil des Basissystems
    - */usr/local/bin* und */usr/local/sbin*: Für zusätzliche,
      lokal installierte Software

2. Zugriffsrechte:
    - */bin* und */usr/local/bin*: Für alle Benutzer
    - */sbin* und */usr/local/sbin*: Primär für Systemadministratoren

3. Bootrelevanz:
    - */bin* und */sbin*: Können für den Bootprozess benötigt werden
    - */usr/local/*: Nicht für den Bootprozess erforderlich

4. Verwaltung:
    - */bin* und */sbin*: Vom Paketmanager des Systems verwaltet
    - */usr/local/*: Vom lokalen Administrator verwaltet

5. Priorität im Pfad:
    - */bin* und */sbin*: Höhere Priorität
    - */usr/local/*: Niedrigere Priorität

Diese Struktur hilft bei der Organisation von Systemprogrammen und 
lokal installierten Anwendungen, wobei die Trennung zwischen systemkritischen
und benutzerorientierten Programmen sowie zwischen Basis- und zusätzlicher 
Software beibehalten wird.


.....

.. Rubric:: Footnotes

.. [1]
   Wikipedia Artikel Linux
   https://de.wikipedia.org/wiki/Linux

.. [2]
   Wikipedia Artikel POSIX
   https://de.wikipedia.org/wiki/POSIX

.. [3]
   Linux-Verzeichnisstruktur für Anfänger erklärt
   https://de.linux-console.net/?p=20104
