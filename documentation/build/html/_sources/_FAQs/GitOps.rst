******************************************************************************
FAQ - GitOps - Prinzipien erklärt
******************************************************************************

.. sectionauthor:: Pierre Gronau <Pierre.Gronau@ndaal.eu>

..

.. _Section_Abstract:

.. contents:: Inhalt GitOps - Prinzipien erklärt
    :depth: 3

GitOps - Prinzipien erklärt


.. toctree::
   :maxdepth: 1
   :glob:
   :caption: Inhalt von GitOps - Prinzipien erklärt:

   ../_appendix/GLOSSARY.md
   ../_appendix/GLOSSARY_de.md

