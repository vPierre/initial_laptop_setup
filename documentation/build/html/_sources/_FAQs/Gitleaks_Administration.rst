******************************************************************************
FAQ - gitleaks - Dokumentation für die Administration
******************************************************************************

.. sectionauthor:: Pierre Gronau <Pierre.Gronau@ndaal.eu>

.. _Section_FAQ_gitleaks_-_Dokumentation_für_die_Administration:

.. contents:: Inhalt - gitleaks - Dokumentation für die Administration
   :depth: 3

.. admonition:: Motivation
   :class: tip

   Der Einsatz von Gitleaks [1]_ zur Erkennung von Geheimnissen 
   wie Passwörtern, API-Schlüsseln und Tokens ist 
   aus mehreren Gründen wichtig:

   - Datenschutz: Gitleaks verhindert die versehentliche Offenlegung
     sensibler Informationen in Code-Repositories, die von Angreifern
     ausgenutzt werden könnten.

   - Compliance: Viele Datenschutzbestimmungen und Sicherheitsstandards
     erfordern den Schutz sensibler Daten, einschließlich der Verhinderung
     ihrer Speicherung in Quellcode-Repositories.

   - Reduzierung der Angriffsfläche: Durch die Entfernung von Geheimnissen
     aus dem Code wird die potenzielle Angriffsfläche für Hacker
     erheblich reduziert.

   - Frühzeitige Fehlererkennung: Gitleaks ermöglicht die Erkennung
     von Sicherheitslecks in einer frühen Phase des Entwicklungsprozesses,
     was Zeit und Ressourcen spart.

   - Förderung bewährter Praktiken: Die Verwendung von Gitleaks fördert
     sichere Coding-Praktiken und sensibilisiert Entwickler für den Umgang
     mit sensiblen Daten.

   - Automatisierte Sicherheitsüberprüfungen: Gitleaks kann
     in CI/CD-Pipelines integriert werden, um kontinuierliche
     und automatisierte Sicherheitsüberprüfungen durchzuführen.

   Durch den Einsatz von Gitleaks wird das Sicherheitsniveau
   von Softwareprojekten erhöht und das Risiko von Datenlecks
   und damit verbundenen Sicherheitsvorfällen minimiert.

Einführung
------------------------------------------------------------------------------

Gitleaks [1]_ ist ein leistungsfähiges Open-Source-Tool zur Erkennung 
von Geheimnissen wie Passwörtern, API-Schlüsseln und Tokens 
in Git-Repositories und Verzeichnissen. Es hilft Entwicklern
und Administratoren, versehentlich commitete sensitive Daten
zu identifizieren und zu verhindern.

Installation
------------------------------------------------------------------------------

Debian 12
==============================================================================

.. code-block:: bash
   :caption: gitleaks Installation auf Debian 12

   sudo apt update && sudo apt upgrade -y
   sudo apt install gitleaks

Red Hat Linux 9
==============================================================================

.. code-block:: bash
   :caption: gitleaks Installation auf Red Hat Linux 9

   sudo dnf update -y
   sudo dnf install gitleaks

Oracle Linux 9
==============================================================================

.. code-block:: bash
   :caption: gitleaks Installation auf Oracle Linux 9

   sudo yum update -y
   sudo yum install epel-release -y
   sudo yum install gitleaks -y

Diese Schritte aktualisieren zunächst das System, fügen dann 
das EPEL-Repository hinzu (falls Gitleaks dort verfügbar ist) 
und installieren schließlich Gitleaks über den Paketmanager yum.

Bitte beachten Sie, dass die Verfügbarkeit von Gitleaks 
in den Standard-Repositories von Oracle Linux 9 nicht garantiert ist.
Falls die Installation über yum fehlschlägt, könnte es notwendig sein, 
Gitleaks manuell zu kompilieren oder alternative Installationsmethoden 
wie die Verwendung von Go oder den Download einer vorkompilierten Binärdatei 
in Betracht zu ziehen.

macOS
==============================================================================

.. code-block:: bash
   :caption: gitleaks Installation auf macOS

   brew install gitleaks

Docker (DockerHub)
==============================================================================

.. code-block:: bash
   :caption: gitleaks Installation von Docker (DockerHub)

   docker pull zricethezav/gitleaks:latest
   docker run -v ${path_to_host_folder_to_scan}:/path zricethezav/gitleaks:latest [COMMAND] --source="/path" [OPTIONS]

Docker (ghcr.io)
==============================================================================

.. code-block:: bash
   :caption: gitleaks Installation von Docker (ghcr.io)

   docker pull ghcr.io/zricethezav/gitleaks:latest
   docker run -v ${path_to_host_folder_to_scan}:/path zricethezav/gitleaks:latest [COMMAND] --source="/path" [OPTIONS]

Verwendung
------------------------------------------------------------------------------

Git-Repository scannen
==============================================================================

.. code-block:: bash
   :caption: gitleaks - Git-Repository scannen

   gitleaks git /pfad/zum/repo

Pre-Commit-Scan (ohne Verfolgung von staged Änderungen)
==============================================================================

.. code-block:: bash
   :caption: gitleaks - Pre-Commit-Scan (ohne Verfolgung von staged Änderungen)

   gitleaks git --pre-commit /pfad/zum/repo

Pre-Commit-Scan (mit Verfolgung von staged Änderungen)
==============================================================================

.. code-block:: bash
   :caption: gitleaks - Pre-Commit-Scan (mit Verfolgung von staged Änderungen)

   gitleaks git --pre-commit --staged /pfad/zum/repo

Verzeichnis oder Datei scannen
==============================================================================

.. code-block:: bash
   :caption: gitleaks - Verzeichnis oder Datei scannen

   gitleaks directory /pfad/zum/verzeichnis/oder/datei

Daten aus einer Pipeline scannen
==============================================================================

.. code-block:: bash
   :caption: gitleaks - Daten aus einer Pipeline scannen

   cat datei | gitleaks stdin

Optionen
-v, --verbose: Ausführliche Ausgabe anzeigen
--config: Pfad zur Konfigurationsdatei
--report-format: Ausgabeformat (json, csv, sarif)
--report-path: Pfad für die Ausgabedatei
--redact: Geheimnisse in der Ausgabe zensieren
--max-target-megabytes: Maximale Dateigröße für den Scan

Beispiele
------------------------------------------------------------------------------

Bericht erstellen
==============================================================================

.. code-block:: bash
   :caption: gitleaks - Bericht erstellen

   gitleaks git /pfad/zum/repo --report-path bericht.json --report-format json

Fazit
------------------------------------------------------------------------------

Gitleaks ist ein wertvolles Werkzeug zur Verbesserung der Sicherheit 
von Git-Repositories. Durch regelmäßige Scans können Administratoren 
potenzielle Sicherheitsrisiken frühzeitig erkennen und beheben. Die 
flexible Konfiguration und verschiedenen Ausgabeformate machen Gitleaks 
zu einem vielseitigen Tool für verschiedene Einsatzszenarien.
Beachten Sie, dass die Befehle "detect" und "protect" 
seit Version 8.19.0 als veraltet gelten und durch 
die neuen Befehlsstrukturen ersetzt wurden.


.....

.. Rubric:: Footnotes

.. [1]
   Gitleaks
   https://github.com/gitleaks/gitleaks
