******************************************************************************
FAQ - AIDE (Advanced Intrusion Detection Environment)
******************************************************************************

.. sectionauthor:: Pierre Gronau <Pierre.Gronau@ndaal.eu>

.. contents:: Inhalt - AIDE
   :depth: 3

.. _Section_FAQ_AIDE:

.. admonition:: Motivation
   :class: tip

   Die Verwendung von AIDE (Advanced Intrusion Detection Environment) ist
   aus mehreren Gründen wichtig:

   - Integritätssicherung: AIDE überwacht kritische Systemdateien und
     -verzeichnisse auf unbefugte Änderungen, was die Erkennung von
     Manipulationen und Kompromittierungen erleichtert.

   - Compliance: Viele Sicherheitsstandards und Regulierungen erfordern
     den Einsatz von Integritätsprüfungssystemen wie AIDE zur Einhaltung
     von Sicherheitsrichtlinien.

   - Frühwarnsystem: AIDE dient als effektives Frühwarnsystem für
     potenzielle Sicherheitsvorfälle, indem es unerwartete Systemänderungen
     zeitnah erkennt und meldet.

   - Forensische Unterstützung: Im Falle eines Sicherheitsvorfalls liefert
     AIDE wertvolle Informationen für forensische Untersuchungen und
     hilft bei der Rekonstruktion von Ereignissen.

   - Konfigurationsmanagement: AIDE unterstützt bei der Überwachung und
     Kontrolle von Systemkonfigurationen, was besonders in komplexen
     IT-Umgebungen von Vorteil ist.

   - Best Practices: Der Einsatz von AIDE fördert bewährte
     Sicherheitspraktiken und sensibilisiert für die Bedeutung der
     Systemintegrität im IT-Betrieb.

   Durch den regelmäßigen Einsatz von AIDE wird das Sicherheitsniveau
   des Systems erhöht und das Risiko unentdeckter Sicherheitsvorfälle
   minimiert.


Zweck von AIDE
------------------------------------------------------------------------------

AIDE ist ein Host-basiertes Intrusion Detection System für Linux-Systeme. 
Es dient folgenden Hauptzwecken:

1. Integritätsprüfung von Dateien und Verzeichnissen
2. Erkennung von unbefugten Änderungen am Dateisystem
3. Unterstützung bei der Identifikation von Sicherheitsvorfällen

AIDE erstellt eine Datenbank mit Prüfsummen und anderen Attributen 
von Dateien und Verzeichnissen. Diese Datenbank dient als Referenz,
um später Änderungen am System zu erkennen.

Installation von AIDE
------------------------------------------------------------------------------

Auf Debian 12:
==============================================================================

.. code-block:: bash
   :caption: Installation von AIDE auf Debian 12

   sudo apt update
   sudo apt install aide

Auf Oracle Linux 9:
==============================================================================

Mit yum:

.. code-block:: bash
   :caption: Installation von AIDE auf Oracle Linux 9 mit yum

   sudo yum install aide

Mit dnf:

.. code-block:: bash
   :caption: Installation von AIDE auf Oracle Linux 9 mit dnf

   sudo dnf install aide

Konfiguration der unterstützten Ciphers
------------------------------------------------------------------------------

Um moderne Hashing-Algorithmen wie SHA3-512 zu konfigurieren, bearbeiten Sie 
die AIDE-Konfigurationsdatei:

.. code-block:: bash
   :caption: Konfiguration der unterstützten Ciphers

   sudo nano /etc/aide/aide.conf

Fügen Sie folgende Zeile hinzu oder modifizieren Sie bestehende Einträge:

.. code-block:: text

   Checksums = sha3-512+sha512+sha256

Beispiel zur Nutzung von AIDE
------------------------------------------------------------------------------

1. Initialisieren der AIDE-Datenbank:

   .. code-block:: bash
      :caption: Initialisieren der AIDE-Datenbank

      sudo aide --init

2. Umbenennen der initialisierten AIDE-Datenbank:

   .. code-block:: bash
      :caption: Umbenennen der initialisierten AIDE-Datenbank

      sudo mv /var/lib/aide/aide.db.new /var/lib/aide/aide.db

3. Durchführen einer Integritätsprüfung der AIDE-Datenbank:

   .. code-block:: bash
      :caption: Durchführen einer Integritätsprüfung der AIDE-Datenbank

      sudo aide --check

4. Beispiel für eine Änderung:

   .. code-block:: bash
      :caption: Beispiel für eine Änderung

      echo "Test" | sudo tee -a /etc/motd

5. Erneute Integritätsprüfung:

   .. code-block:: bash
      :caption: Erneute Integritätsprüfung

      sudo aide --check

   Dies wird die Änderung an /etc/motd melden.

6. Aktualisieren der Datenbank nach geplanten Änderungen:

   .. code-block:: bash
      :caption: Aktualisieren der Datenbank nach geplanten Änderungen

      sudo aide --update

7. Umbenennen der aktualisierten AIDE Datenbank:

   .. code-block:: bash
      :caption: Umbenennen der aktualisierten AIDE Datenbank

      sudo mv /var/lib/aide/aide.db.new /var/lib/aide/aide.db

AIDE sollte regelmäßig ausgeführt werden, idealerweise als Cron-Job, 
um Systemänderungen zeitnah zu erkennen. Es ist wichtig, 
die AIDE-Datenbank nach geplanten Systemänderungen zu aktualisieren, 
um Fehlalarme zu vermeiden.

Deployment Sicht
------------------------------------------------------------------------------

Hier wird angeführt, wie das alles ausgerollt wird (Deployment Sicht).

.. seealso:: Deployment Sicht

   Diese Einstellungen werden mittels eines bash Skriptes aufgebracht.

   .. figure::
      _static/bash-logo-by-vd.png
      :align: center
      :scale: 25 %
      :alt: bash Skript

      bash Skript

   
   <Platzhalter für den Link zum Repo>

   Diese Einstellungen werden mittels einer Ansible Rolle umgesetzt.

   .. figure::
      _static/Ansible_logo-700x700.png
      :align: center
      :scale: 25 %
      :alt: Ansible Rolle

      Ansible Rolle

   
   <Platzhalter für den Link zum Repo>

