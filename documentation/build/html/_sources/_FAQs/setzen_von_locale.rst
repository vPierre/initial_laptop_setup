******************************************************************************
FAQ - Setzen von locale unter Linux
******************************************************************************

.. sectionauthor:: Pierre Gronau <Pierre.Gronau@ndaal.eu>

.. _Section_FAQ_Setzen_von_locale_unter_Linux:

.. contents:: Inhalt - Sicherheit - Container
   :depth: 3

Setzen von locale
------------------------------------------------------------------------------

.. Warning:: Systemmeldung
   perl: warning: Please check that your locale settings: LANGUAGE = (unset),
   LC_ALL = (unset), LC_CTYPE = "de_DE.UTF-8", LANG = "C.UTF-8" are supported
   and installed on your system. perl: warning: Falling back
   to a fallback locale ("C.UTF-8").

Die Warnung, die Sie sehen, deutet darauf hin, dass
die Locale-Einstellungen [1]_ auf Ihrem System nicht korrekt konfiguriert
sind oder dass die angegebenen Locale nicht installiert sind. Dies kann
zu Problemen bei der Ausführung von Perl-Skripten oder anderen Programmen
führen, die auf Locale-Einstellungen angewiesen sind.

Hier sind einige Schritte, um das Problem zu beheben:

1. Überprüfen Sie die installierten Locales
==============================================================================

Zuerst sollten Sie überprüfen, welche Locales auf Ihrem System installiert
sind. Führen Sie den folgenden Befehl aus:

bash

``locale -a``

Dieser Befehl listet alle verfügbaren Locales auf. Stellen Sie sicher,
dass de_DE.UTF-8 [2]_ in der Liste enthalten ist.

2. Installieren Sie die fehlenden Locales
==============================================================================

Wenn de_DE.UTF-8 nicht in der Liste der verfügbaren Locales erscheint,
können Sie es installieren. Führen Sie dazu die folgenden Schritte aus:

Auf Debian/Ubuntu-basierten Systemen:

Öffnen Sie ein Terminal.

Führen Sie den folgenden Befehl aus, um die Locale zu generieren:

bash

``sudo locale-gen de_DE.UTF-8``

Aktualisieren Sie die Locale-Einstellungen:

bash

``sudo update-locale``

Auf Red Hat/CentOS-basierten Systemen:

Öffnen Sie ein Terminal.

Fügen Sie die Locale zu /etc/locale.gen hinzu (falls nicht vorhanden):

bash

``echo "de_DE.UTF-8 UTF-8" | sudo tee -a /etc/locale.gen``

Generieren Sie die Locale:

bash

``sudo locale-gen``

3. Setzen Sie die Locale-Umgebungsvariablen
==============================================================================

Um sicherzustellen, dass die Locale-Umgebungsvariablen korrekt gesetzt sind, können Sie die folgenden Zeilen zu Ihrer ~/.bashrc oder ~/.profile Datei hinzufügen:

bash

``export LANG=de_DE.UTF-8``

``export LC_ALL=de_DE.UTF-8``

Nach dem Hinzufügen dieser Zeilen müssen Sie die Datei neu laden oder sich ab- und wieder anmelden:

bash

``source ~/.bashrc``

4. Überprüfen Sie die Locale-Einstellungen
==============================================================================

Führen Sie den folgenden Befehl aus, um zu überprüfen, ob die Locale-Einstellungen korrekt gesetzt sind:

bash

.. code-block:: bash
    
    locale

    LANG=de_DE.UTF-8
    LANGUAGE=
    LC_CTYPE="de_DE.UTF-8"
    LC_NUMERIC="de_DE.UTF-8"
    LC_TIME="de_DE.UTF-8"
    LC_COLLATE="de_DE.UTF-8"
    LC_MONETARY="de_DE.UTF-8"
    LC_MESSAGES="de_DE.UTF-8"
    LC_PAPER="de_DE.UTF-8"
    LC_NAME="de_DE.UTF-8"
    LC_ADDRESS="de_DE.UTF-8"
    LC_TELEPHONE="de_DE.UTF-8"
    LC_MEASUREMENT="de_DE.UTF-8"
    LC_IDENTIFICATION="de_DE.UTF-8"
    LC_ALL=de_DE.UTF-8

Die Ausgabe sollte nun de_DE.UTF-8 für LANG und LC_ALL anzeigen.

Wenn sie US Englisch wünschen ersetzen sie bitte ``de_DE``mit ``en_US``.

Zusammenfassung
==============================================================================

Durch die Installation der fehlenden Locale und das Setzen der entsprechenden
Umgebungsvariablen sollten die Perl-Warnungen verschwinden. Wenn das Problem
weiterhin besteht, überprüfen Sie die Konfiguration und stellen Sie sicher,
dass alle Schritte korrekt ausgeführt wurden.

Links
==============================================================================

- Wikipedia Artikel über locale [1]_
- <https://saimana.com/list-of-country-locale-code/>
- <https://github.com/umpirsky/locale-list>
- <https://www.fincher.org/Utilities/CountryLanguageList.shtml>
- <https://www.loc.gov/standards/iso639-2/ISO-639-2_8859-1.txt>
- <https://www.science.co.il/language/Locale-codes.php>
- <https://simplelocalize.io/data/locales/>
- <https://learn.microsoft.com/en-us/globalization/encoding/code-pages>

Deployment Sicht
------------------------------------------------------------------------------

Hier wird angeführt, wie das alles ausgerollt wird (Deployment Sicht).

.. seealso:: Deployment Sicht

   Diese Einstellungen werden mittels eines bash Skriptes aufgebracht.

   .. figure::
      _static/bash-logo-by-vd.png
      :align: center
      :scale: 25 %
      :alt: bash Skript

      bash Skript


   <Platzhalter für den Link zum Repo>

   Diese Einstellungen werden mittels einer Ansible Rolle umgesetzt.

   .. figure::
      _static/Ansible_logo-700x700.png
      :align: center
      :scale: 25 %
      :alt: Ansible Rolle

      Ansible Rolle


   <Platzhalter für den Link zum Repo>


.....

.. Rubric:: Footnotes

.. [1]
   Wikipedia Artikel über locale
   https://de.wikipedia.org/wiki/Locale

.. [2]
   list of country locale code
   https://saimana.com/list-of-country-locale-code/
   https://github.com/umpirsky/locale-list
   https://www.fincher.org/Utilities/CountryLanguageList.shtml

