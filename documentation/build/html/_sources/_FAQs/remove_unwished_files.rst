******************************************************************************
FAQ - Löschung diverser unerwünschten Dateien
******************************************************************************

.. sectionauthor:: Pierre Gronau <Pierre.Gronau@ndaal.eu>

.. _Section_FAQ_Löschung_diverser_unerwünschten_Dateien:

.. contents:: Inhalt - Löschung diverser unerwünschten Dateien
   :depth: 3

.. admonition:: Motivation
   :class: tip

   Die Löschung diverser unerwünschten Dateien ist
   aus mehreren Gründen wichtig:

   - Temporäre und Backup-Dateien
     Dateien wie .tmp, .temp, .bak, .backup, ~, .old, .save sind oft temporäre
     oder Backup-Dateien, die nach ihrer Nutzung nicht mehr benötigt werden.
     Sie können sensible Informationen enthalten und unnötig Speicherplatz 
     belegen.
     
    - Systemspezifische Dateien
      thumbs.db (Windows) und .DS_Store (macOS) sind systemgenerierte Dateien
      für Vorschaubilder und Ordnereinstellungen.
      Diese Dateien sind plattformspezifisch und können 
      in plattformübergreifenden Projekten Probleme verursachen.

    - Unvollständige Downloads
      Dateien wie .crdownload, .part, .download, .partial sind Indikatoren 
      für unvollständige Downloads.
      Sie sollten entfernt werden, da sie keine vollständigen 
      oder nutzbaren Daten enthalten.

    - Editor-spezifische Dateien
      .swp, .swn, .swo sind temporäre Dateien, die von Texteditoren 
      wie Vim erstellt werden.
      .wbk, .xlk sind temporäre Dateien von Microsoft Office-Produkten.
      Diese Dateien dienen nur dem jeweiligen Editor und sind 
      für andere Benutzer nicht relevant.

    - Crash- und Fehlerdateien
      .dmp, .crash, .err, .log sind oft Dateien, die bei Systemabstürzen
      oder Fehlern generiert werden.
      Sie können sensible Informationen enthalten und sind nach 
      der Fehleranalyse nicht mehr nötig.

    - Cache-Dateien
      .cache-Dateien speichern temporäre Daten zur Leistungsverbesserung, 
      sind aber nicht essentiell.

    - Sperrdateien
      .lck, .~lock sind Sperrdateien, die anzeigen, dass eine Datei gerade 
      bearbeitet wird.
      Sie sollten nach Beendigung der Bearbeitung nicht mehr vorhanden sein.

    - Andere temporäre Dateien
      Dateien wie .$$$, .chk, .sik sind oft temporäre Dateien 
      verschiedener Programme.

    Die Entfernung dieser Dateien hilft, das Dateisystem aufzuräumen,
    Speicherplatz zu sparen, potenzielle Sicherheitsrisiken zu minimieren
    und die Übersichtlichkeit des Projekts zu verbessern. Zudem werden 
    mögliche Konflikte in Versionskontrollsystemen und 
    bei der Zusammenarbeit in Teams vermieden.

Deployment Sicht
------------------------------------------------------------------------------

Hier wird angeführt, wie das alles ausgerollt wird (Deployment Sicht).

.. seealso:: Deployment Sicht

   Diese Einstellungen werden mittels eines bash Skriptes aufgebracht.

   .. figure::
      _static/bash-logo-by-vd.png
      :align: center
      :scale: 25 %
      :alt: bash Skript

      bash Skript

   
   <Platzhalter für den Link zum Repo>

   Diese Einstellungen werden mittels einer Ansible Rolle umgesetzt.

   .. figure::
      _static/Ansible_logo-700x700.png
      :align: center
      :scale: 25 %
      :alt: Ansible Rolle

      Ansible Rolle

   
   <Platzhalter für den Link zum Repo>

