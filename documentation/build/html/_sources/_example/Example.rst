********************************************************************************************
ndaal - Examples with arc42 in German, English and Spain
********************************************************************************************

.. sectionauthor:: Pierre Gronau <Pierre.Gronau@ndaal.eu>

.. image:: 
   _static/Logo.png
   :align: center
   
.. contents:: Content
   :depth: 3

.. _Section_example:

**arc42** answers the following two questions in a pragmatic way, but can be tailored to your specific needs:

- **What** should we document/communicate about our architecture?
- **How** should we document/communicate?

.. toctree::
   :numbered:
   :glob:
   :maxdepth: 2
   :caption: Content German arc42 Templates:

   ../_example/_DE/*

.....

.. toctree::
   :numbered:
   :glob:
   :maxdepth: 2
   :caption: Content English arc42 Templates:

   ../_example/_EN/*

......

.. toctree::
   :numbered:
   :glob:
   :maxdepth: 2
   :caption: Content Spain arc42 Templates:

   ../_example/_ES/*


.....

.. Rubric:: Footnotes

.. [1]
   https://arc42.org/download
