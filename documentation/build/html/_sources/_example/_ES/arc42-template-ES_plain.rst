******************************************************************************
ndaal - Test - arc42 - Spain - Plain Text
******************************************************************************

.. sectionauthor:: Pierre Gronau <Pierre.Gronau@ndaal.eu>

.. meta::
   :keywords: ndaal sphinx documentation generator
   :keywords lang=en: IT Compliance IT Security Automation
   :keywords lang=de: bittediesenkeyfinden

.. image:: 
   _static/Logo.png

.. contents:: Content
   :depth: 3

.. Warning:: Please ignore this chapter - it is just for testing purpose.



.. _section-introduction-and-goals:

Introducción y Metas
====================

.. __vista_de_requerimientos:

Vista de Requerimientos
-----------------------

.. __metas_de_calidad:

Metas de Calidad
----------------

.. __partes_interesadas_stakeholders:

Partes interesadas (Stakeholders)
---------------------------------

+-------------+---------------------------+---------------------------+
| Rol/Nombre  | Contacto                  | Expectativas              |
+=============+===========================+===========================+
| *<Role-1>*  | *<Contact-1>*             | *<Expectation-1>*         |
+-------------+---------------------------+---------------------------+
| *<Role-2>*  | *<Contact-2>*             | *<Expectation-2>*         |
+-------------+---------------------------+---------------------------+

.. _section-architecture-constraints:

Restricciones de la Arquitectura
================================

.. _section-system-scope-and-context:

Alcance y Contexto del Sistema
==============================

.. __contexto_de_negocio:

Contexto de Negocio
-------------------

**<Diagrama o Tabla>**

**<optionally: Explanation of external domain interfaces>**

.. __contexto_t_cnico:

Contexto Técnico
----------------

**<Diagrama o Tabla>**

**<Opcional: Explicación de las interfases técnicas>**

**<Mapeo de Entrada/Salida a canales>**

.. _section-solution-strategy:

Estrategia de solución
======================

.. _section-building-block-view:

Vista de Bloques
================

.. __sistema_general_de_caja_blanca:

Sistema General de Caja Blanca
------------------------------

**<Diagrama general>**

Motivación
   *<Explicación en texto>*

Bloques de construcción contenidos
   *<Desripción de los bloques de construcción contenidos (Cajas
   negras)>*

Interfases importantes
   *<Descripción de las interfases importantes>*

.. ___caja_negra_1:

<Caja Negra 1>
~~~~~~~~~~~~~~

*<Propósito/Responsabilidad>*

*<Interfase(s)>*

*<(Opcional) Características de Calidad/Performance>*

*<(Opcional) Ubicación Archivo/Directorio>*

*<(Opcional) Requerimientos Satisfechos>*

*<(Opcional) Riesgos/Problemas/Incidentes Abiertos>*

.. ___caja_negra_2:

<Caja Negra 2>
~~~~~~~~~~~~~~

*<plantilla de caja negra>*

.. ___caja_negra_n:

<Caja Negra N>
~~~~~~~~~~~~~~

*<Plantilla de caja negra>*

.. ___interfase_1:

<Interfase 1>
~~~~~~~~~~~~~

…

.. ___interfase_m:

<Interfase m>
~~~~~~~~~~~~~

.. __nivel_2:

Nivel 2
-------

.. __caja_blanca_emphasis_bloque_de_construcci_n_1_emphasis:

Caja Blanca *<bloque de construcción 1>*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

*<plantilla de caja blanca>*

.. __caja_blanca_emphasis_bloque_de_construcci_n_2_emphasis:

Caja Blanca *<bloque de construcción 2>*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

*<plantilla de caja blanca>*

…

.. __caja_blanca_emphasis_bloque_de_construcci_n_m_emphasis:

Caja Blanca *<bloque de construcción m>*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

*<plantilla de caja blanca>*

.. __nivel_3:

Nivel 3
-------

.. __caja_blanca_bloque_de_construcci_n_x_1:

Caja Blanca <_bloque de construcción x.1_>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

*<plantilla de caja blanca>*

.. __caja_blanca_bloque_de_construcci_n_x_2:

Caja Blanca <_bloque de construcción x.2_>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

*<plantilla de caja blanca>*

.. __caja_blanca_bloque_de_construcci_n_y_1:

Caja Blanca <_bloque de construcción y.1_>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

*<plantilla de caja blanca>*

.. _section-runtime-view:

Vista de Ejecución
==================

.. ___escenario_de_ejecuci_n_1:

<Escenario de ejecución 1>
--------------------------

-  *<Inserte un diagrama de ejecución o la descripción del escenario>*

-  *<Inserte la descripción de aspectos notables de las interacciones
   entre los bloques de construcción mostrados en este diagrama.>*

.. ___escenario_de_ejecuci_n_2:

<Escenario de ejecución 2>
--------------------------

.. __:

…
-

.. ___escenario_de_ejecuci_n_n:

<Escenario de ejecución n>
--------------------------

.. _section-deployment-view:

Vista de Despliegue
===================

.. __nivel_de_infraestructura_1:

Nivel de infraestructura 1
--------------------------

**<Diagrama General>**

Motivación
   *<Explicación en forma textual>*

Características de Calidad/Rendimiento
   *<Explicación en forma textual>*

   Mapeo de los Bloques de Construcción a Infraestructura
      *<Descripción del mapeo>*

.. __nivel_de_infraestructura_2:

Nivel de Infraestructura 2
--------------------------

.. ___emphasis_elemento_de_infraestructura_1_emphasis:

*<Elemento de Infraestructura 1>*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

*<diagrama + explicación>*

.. ___emphasis_elemento_de_infraestructura_2_emphasis:

*<Elemento de Infraestructura 2>*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

*<diagrama + explicación>*

…

.. ___emphasis_elemento_de_infraestructura_n_emphasis:

*<Elemento de Infraestructura n>*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

*<diagrama + explicación>*

.. _section-concepts:

Conceptos Transversales (Cross-cutting)
=======================================

.. ___emphasis_concepto_1_emphasis:

*<Concepto 1>*
--------------

*<explicación>*

.. ___emphasis_concepto_2_emphasis:

*<Concepto 2>*
--------------

*<explicación>*

…

.. ___emphasis_concepto_n_emphasis:

*<Concepto n>*
--------------

*<explicación>*

.. _section-design-decisions:

Decisiones de Diseño
====================

.. _section-quality-scenarios:

Requerimientos de Calidad
=========================

.. ___rbol_de_calidad:

Árbol de Calidad
----------------

.. __escenarios_de_calidad:

Escenarios de calidad
---------------------

.. _section-technical-risks:

Riesgos y deuda técnica
=======================

.. _section-glossary:

Glosario
========

+-----------------------+-----------------------------------------------+
| Término               | Definición                                    |
+=======================+===============================================+
| *<Término-1>*         | *<definicion-1>*                              |
+-----------------------+-----------------------------------------------+
| *<Término-2>*         | *<definicion-2>*                              |
+-----------------------+-----------------------------------------------+

