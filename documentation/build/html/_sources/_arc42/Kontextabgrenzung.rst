******************************************************************************
Systemhärtung - Kontextabgrenzung
******************************************************************************

.. sectionauthor:: Pierre Gronau <Pierre.Gronau@ndaal.eu>

.. _Section_arc42_Kontextabgrenzung:

.. contents:: Inhalt - Kontextabgrenzung
   :depth: 3

Kontextabgrenzung
------------------------------------------------------------------------------

Die Kontextabgrenzung für die Systemhärtung von Linux-Systemen definiert 
die Grenzen und Schnittstellen des Härtungssystems sowie dessen Interaktionen
mit externen Entitäten. Sie umfasst sowohl den fachlichen als auch 
den technischen Kontext.

Fachlicher Kontext
------------------------------------------------------------------------------

Systemhärtung
------------------------------------------------------------------------------

Verantwortlichkeiten
==============================================================================

- Implementierung von Sicherheitsrichtlinien

- Konfiguration von Systemeinstellungen

- Härtung von Netzdiensten

- Verwaltung von Benutzerrechten und -zugriffen

- Überwachung und Protokollierung von Systemaktivitäten

Eingaben
==============================================================================

- Sicherheitsrichtlinien und -standards (z.B. CIS Benchmarks, BSI-Grundschutz)

- Organisations-spezifische Sicherheitsanforderungen

- Ergebnisse von Sicherheitsaudits und Penetrationstests

- Aktualisierte Bedrohungsinformationen

Ausgaben
==============================================================================

- Gehärtete Systemkonfigurationen

- Sicherheitsberichte und Compliance-Nachweise

- Protokolle über durchgeführte Härtungsmaßnahmen

- Empfehlungen für weitere Sicherheitsverbesserungen

Externe Entitäten
------------------------------------------------------------------------------

Systemadministratoren
==============================================================================

- Verantwortlich für die Umsetzung und Wartung der Härtungsmaßnahmen

- Interaktion: Konfiguration, Überwachung, Fehlerbehebung

Sicherheitsbeauftragte (CISO, ISB)
==============================================================================

- Definieren Sicherheitsrichtlinien und überwachen deren Einhaltung

- Interaktion: Richtlinienvorgaben, Auditierung, Berichterstattung

Endbenutzer
==============================================================================

- Nutzen die gehärteten Systeme im täglichen Betrieb

- Interaktion: Authentifizierung, Zugriff auf Ressourcen

Compliance-Abteilung
==============================================================================

- Stellt die Einhaltung gesetzlicher und regulatorischer Anforderungen sicher

- Interaktion: Anforderungsdefinition, Compliance-Überprüfung

Externe Auditoren
==============================================================================

Führen unabhängige Überprüfungen der Sicherheitsmaßnahmen durch
Interaktion: Auditdurchführung, Berichterstellung

Technischer Kontext
------------------------------------------------------------------------------

Härtungssystem
------------------------------------------------------------------------------

Komponenten
==============================================================================

- Konfigurationsmanagement-Tools (z.B. Ansible)

- Sicherheitsscanner und Vulnerability Assessment Tools

- Logging- und Monitoring-Systeme

- Patch-Management-Systeme

Schnittstellen:
==============================================================================

- SSH für Remote-Verwaltung

- APIs für Automatisierung und Integration

- Syslog für zentralisiertes Logging

- SNMP für Netzüberwachung

Externe Systeme
------------------------------------------------------------------------------

Zielsysteme (zu härtende Linux-Server)
==============================================================================

- Kanal: Netzverbindung (LAN/WAN)

- Protokolle: SSH, HTTPS, SNMP

- Interaktion: Konfigurationsänderungen, Softwareinstallation, Logging

Zentrale Management-Konsole
==============================================================================

- Kanal: Gesicherte Netzverbindung

- Protokolle: HTTPS, SSH

- Interaktion: Steuerung der Härtungsprozesse, Berichterstattung

Patch-Management-Server
==============================================================================

- Kanal: Intranet

- Protokolle: HTTPS, WSUS (für Windows-Komponenten)

- Interaktion: Bereitstellung und Installation von Sicherheitsupdates

SIEM-System (Security Information and Event Management)
==============================================================================

- Kanal: Netzverbindung

- Protokolle: Syslog, SNMP

- Interaktion: Sammlung und Analyse von Sicherheitsereignissen

Backup-System
==============================================================================

- Kanal: Dedizierte Backup-Netzverbindung

- Protokolle: Proprietäre Backup-Protokolle, NDMP

- Interaktion: Sicherung von Systemkonfigurationen und kritischen Daten

Identity and Access Management (IAM) System
==============================================================================

- Kanal: Intranet

- Protokolle: LDAP, SAML, OAuth

- Interaktion: Zentrale Verwaltung von Benutzeridentitäten und -berechtigungen

Abgrenzung der Verantwortlichkeiten
------------------------------------------------------------------------------

- Das Härtungssystem ist verantwortlich für die Implementierung 
  und Aufrechterhaltung der Sicherheitsmaßnahmen auf den Zielsystemen.

- Die Zielsysteme selbst sind verantwortlich für die Ausführung 
  ihrer spezifischen Anwendungen und Dienste.

- Das zentrale Management-System ist verantwortlich für die Orchestrierung 
  und Überwachung der Härtungsprozesse.

- Das SIEM-System ist verantwortlich für die Erkennung und Analyse 
  von Sicherheitsereignissen.

- Das IAM-System ist verantwortlich für die zentrale Verwaltung 
  von Benutzeridentitäten und -zugriffsrechten.

Diese Kontextabgrenzung definiert klar die Rolle und Verantwortlichkeiten 
des Systemhärtungssystems innerhalb der gesamten ITK-Infrastruktur und 
legt die Grundlage für eine effektive und sichere Implementierung 
der Härtungsmaßnahmen.
