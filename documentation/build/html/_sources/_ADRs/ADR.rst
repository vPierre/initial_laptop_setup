******************************************************************************
Systemhärtung - ADR (Architectural Decision Record)
******************************************************************************

.. sectionauthor:: Pierre Gronau <Pierre.Gronau@ndaal.eu>

.. contents:: Inhalt - Systemhärtung - ADR
    :depth: 3

.. _Section_ADR:

.. Hint::
   Hier sind alle ADRs (Architectural Decision Record ), 
   auf deutsch Architektur-Entscheidungs-Records unabhängig 
   von Ihrem Status aufgeführt.

.. toctree::
   :maxdepth: 1
   :glob:
   :caption: Inhalt - Systemhärtung - ADR

   ../_ADRs/*

