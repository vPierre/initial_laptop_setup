******************************************************************************
Wenn mal was schief geht - UAC (Unix-like Artifacts Collector)
******************************************************************************

.. sectionauthor:: Pierre Gronau <Pierre.Gronau@ndaal.eu>

.. _Section_UAC:

.. contents:: Inhalt - UAC (Unix-like Artifacts Collector)
   :depth: 5

Unix-like Artifacts Collector
------------------------------------------------------------------------------

**UAC** ist ein Live-Response-Sammlungsskript für die Vorfallreaktion,
das native Binärdateien und Tools verwendet, um die Sammlung von Artefakten 
auf AIX, ESXi, FreeBSD, Linux, macOS, NetBSD, NetScaler, OpenBSD und 
Solaris-Systemen zu automatisieren. Es wurde entwickelt, 
um die Datensammlung zu erleichtern und zu beschleunigen und weniger 
auf Remote-Support während Vorfallreaktionsengagements angewiesen zu sein.

UAC liest **YAML-Dateien** zur Laufzeit und sammelt basierend 
auf deren Inhalten relevante Artefakte. Dies macht UAC sehr anpassbar 
und erweiterbar.

Der Quellcode ist auf der Projektseite [1]_ verfügbar.

UAC (Unix-like Artifacts Collector) collection
------------------------------------------------------------------------------

Hauptmerkmale
==============================================================================

- Überall ausführbar ohne Abhängigkeiten (keine Installation erforderlich).

- Anpassbare und erweiterbare Sammlungen und Artefakte.

- Beachtung der Reihenfolge der Flüchtigkeit während der Artefaktsammlung.

- Sammlung von Informationen über aktuell laufende Prozesse 
  (einschließlich Prozesse ohne Binärdatei auf der Festplatte).

- Hashing von laufenden Prozessen und ausführbaren Dateien.

- Extraktion von Dateien und Verzeichnisstatus zur Erstellung einer Bodyfile.

- Sammlung von system- und benutzerspezifischen Daten, Konfigurationsdateien 
  und Protokollen.

- Erwerb von flüchtigem Speicher von Linux-Systemen 
  mit verschiedenen Methoden und Tools.

Unterstützte Betriebssysteme
==============================================================================

UAC läuft auf jedem Unix-ähnlichen System, unabhängig 
von der Prozessorarchitektur. Alles, was UAC benötigt, 
ist eine Shell. 

Unterstützte Systeme sind:

- AIX, 

- ESXi, 

- FreeBSD, 

- Linux, 

- macOS, 

- NetBSD, 

- NetScaler, 

- OpenBSD und 

- Solaris

Beachten Sie, dass UAC sogar auf Systemen wie Network Attached Storage 
(NAS)-Geräten, Netzgeräten wie OpenWrt und IoT-Geräten 
ausgeführt werden kann.

Nutzung von UAC
==============================================================================

**UAC** muss nicht auf dem Zielsystem installiert werden. Laden Sie einfach 
die neueste Version von der Releases-Seite herunter, entpacken Sie sie und 
starten Sie sie. So einfach ist das!

Die Berechtigung für den vollständigen Festplattenzugriff ist 
eine Datenschutzfunktion, die in macOS Mojave (10.14) eingeführt wurde 
und verhindert, dass einige Anwendungen auf wichtige Daten wie E-Mail, 
Nachrichten und Safari-Dateien zugreifen. Es wird daher dringend empfohlen, 
die Berechtigung für die Terminalanwendung manuell zu gewähren, 
bevor Sie UAC aus dem Terminal ausführen, oder die Berechtigung 
für Remote-Benutzer zu gewähren, bevor Sie UAC über SSH ausführen.

Um eine Sammlung durchzuführen, müssen Sie mindestens ein Profil 
und/oder eine Liste von Artefakten angeben und das Zielverzeichnis 
festlegen. Zusätzliche Parameter sind optional.

Beispiele
==============================================================================

- Alle Artefakte basierend auf dem `ir_triage`-Profil sammeln 
  und die Ausgabedatei in `/tmp` speichern:
  
  ```bash
  ./uac -p ir_triage /tmp

.. Seealso:: 
   - :ref:`Wenn mal was schief geht - Linux Performance Analyse <Section_Linux_Performance_Analyse>`
   - :ref:`Wenn mal was schief geht - Root Cause Analyse <Section_Root_Cause_Analyse>`
   - :ref:`Wenn mal was schief geht - UAC (Unix-like Artifacts Collector) <Section_UAC>`
.....

.. Rubric:: Footnotes

.. [1]
   Unix-like Artifacts Collector
   https://github.com/tclahr/uac