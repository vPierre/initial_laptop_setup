CIS Debian Linux 11 Benchmark v1.0.0 - MITRE ATT&CK Mappings
==============================================================================


License 
-----------------------

Please see our terms of use here: https://www.cisecurity.org/cis-securesuite/cis-securesuite-membership-terms-of-use/

Section #: 1.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.9

**Profiles:** Level 1 - Server, Level 2 - Workstation

**Description:** `autofs` allows automatic mounting of devices, typically including CD/DVDs and USB drives.

**MITRE ATT&CK Mappings:** T1068


Section #: 1.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.9

**Profiles:** Level 1 - Server, Level 2 - Workstation

**Description:** `autofs` allows automatic mounting of devices, typically including CD/DVDs and USB drives.

**MITRE ATT&CK Mappings:** T1068.000


Section #: 1.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.9

**Profiles:** Level 1 - Server, Level 2 - Workstation

**Description:** `autofs` allows automatic mounting of devices, typically including CD/DVDs and USB drives.

**MITRE ATT&CK Mappings:** T1203


Section #: 1.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.9

**Profiles:** Level 1 - Server, Level 2 - Workstation

**Description:** `autofs` allows automatic mounting of devices, typically including CD/DVDs and USB drives.

**MITRE ATT&CK Mappings:** T1203.000


Section #: 1.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.9

**Profiles:** Level 1 - Server, Level 2 - Workstation

**Description:** `autofs` allows automatic mounting of devices, typically including CD/DVDs and USB drives.

**MITRE ATT&CK Mappings:** T1211


Section #: 1.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.9

**Profiles:** Level 1 - Server, Level 2 - Workstation

**Description:** `autofs` allows automatic mounting of devices, typically including CD/DVDs and USB drives.

**MITRE ATT&CK Mappings:** T1211.000


Section #: 1.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.9

**Profiles:** Level 1 - Server, Level 2 - Workstation

**Description:** `autofs` allows automatic mounting of devices, typically including CD/DVDs and USB drives.

**MITRE ATT&CK Mappings:** T1212


Section #: 1.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.9

**Profiles:** Level 1 - Server, Level 2 - Workstation

**Description:** `autofs` allows automatic mounting of devices, typically including CD/DVDs and USB drives.

**MITRE ATT&CK Mappings:** T1212.000


Section #: 1.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.10

**Profiles:** Level 1 - Server, Level 2 - Workstation

**Description:** USB storage provides a means to transfer and store files insuring persistence and availability of the files independent of network connection status. Its popularity and utility has led to USB-based malware being a simple and common means for network infiltration and a first step to establishing a persistent threat within a networked environment.

**MITRE ATT&CK Mappings:** TA0001


Section #: 1.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.10

**Profiles:** Level 1 - Server, Level 2 - Workstation

**Description:** USB storage provides a means to transfer and store files insuring persistence and availability of the files independent of network connection status. Its popularity and utility has led to USB-based malware being a simple and common means for network infiltration and a first step to establishing a persistent threat within a networked environment.

**MITRE ATT&CK Mappings:** TA0010


Section #: 1.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.10

**Profiles:** Level 1 - Server, Level 2 - Workstation

**Description:** USB storage provides a means to transfer and store files insuring persistence and availability of the files independent of network connection status. Its popularity and utility has led to USB-based malware being a simple and common means for network infiltration and a first step to establishing a persistent threat within a networked environment.

**MITRE ATT&CK Mappings:** T1052


Section #: 1.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.10

**Profiles:** Level 1 - Server, Level 2 - Workstation

**Description:** USB storage provides a means to transfer and store files insuring persistence and availability of the files independent of network connection status. Its popularity and utility has led to USB-based malware being a simple and common means for network infiltration and a first step to establishing a persistent threat within a networked environment.

**MITRE ATT&CK Mappings:** T1052.001


Section #: 1.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.10

**Profiles:** Level 1 - Server, Level 2 - Workstation

**Description:** USB storage provides a means to transfer and store files insuring persistence and availability of the files independent of network connection status. Its popularity and utility has led to USB-based malware being a simple and common means for network infiltration and a first step to establishing a persistent threat within a networked environment.

**MITRE ATT&CK Mappings:** T1091


Section #: 1.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.10

**Profiles:** Level 1 - Server, Level 2 - Workstation

**Description:** USB storage provides a means to transfer and store files insuring persistence and availability of the files independent of network connection status. Its popularity and utility has led to USB-based malware being a simple and common means for network infiltration and a first step to establishing a persistent threat within a networked environment.

**MITRE ATT&CK Mappings:** T1091.000


Section #: 1.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.10

**Profiles:** Level 1 - Server, Level 2 - Workstation

**Description:** USB storage provides a means to transfer and store files insuring persistence and availability of the files independent of network connection status. Its popularity and utility has led to USB-based malware being a simple and common means for network infiltration and a first step to establishing a persistent threat within a networked environment.

**MITRE ATT&CK Mappings:** T1200


Section #: 1.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.10

**Profiles:** Level 1 - Server, Level 2 - Workstation

**Description:** USB storage provides a means to transfer and store files insuring persistence and availability of the files independent of network connection status. Its popularity and utility has led to USB-based malware being a simple and common means for network infiltration and a first step to establishing a persistent threat within a networked environment.

**MITRE ATT&CK Mappings:** T1200.000


Section #: 1.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.10

**Profiles:** Level 1 - Server, Level 2 - Workstation

**Description:** USB storage provides a means to transfer and store files insuring persistence and availability of the files independent of network connection status. Its popularity and utility has led to USB-based malware being a simple and common means for network infiltration and a first step to establishing a persistent threat within a networked environment.

**MITRE ATT&CK Mappings:** M1034


Section #: 1.1.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.1.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `cramfs` filesystem type is a compressed read-only Linux filesystem embedded in small footprint systems. A `cramfs` image can be used without having to first decompress the image.

**MITRE ATT&CK Mappings:** TA0005


Section #: 1.1.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.1.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `cramfs` filesystem type is a compressed read-only Linux filesystem embedded in small footprint systems. A `cramfs` image can be used without having to first decompress the image.

**MITRE ATT&CK Mappings:** T1005


Section #: 1.1.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.1.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `cramfs` filesystem type is a compressed read-only Linux filesystem embedded in small footprint systems. A `cramfs` image can be used without having to first decompress the image.

**MITRE ATT&CK Mappings:** T1005.000


Section #: 1.1.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.1.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `cramfs` filesystem type is a compressed read-only Linux filesystem embedded in small footprint systems. A `cramfs` image can be used without having to first decompress the image.

**MITRE ATT&CK Mappings:** M1050


Section #: 1.1.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.1.2

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** The `squashfs` filesystem type is a compressed read-only Linux filesystem embedded in small footprint systems. A `squashfs` image can be used without having to first decompress the image.

**MITRE ATT&CK Mappings:** TA0005


Section #: 1.1.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.1.2

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** The `squashfs` filesystem type is a compressed read-only Linux filesystem embedded in small footprint systems. A `squashfs` image can be used without having to first decompress the image.

**MITRE ATT&CK Mappings:** T1005


Section #: 1.1.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.1.2

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** The `squashfs` filesystem type is a compressed read-only Linux filesystem embedded in small footprint systems. A `squashfs` image can be used without having to first decompress the image.

**MITRE ATT&CK Mappings:** T1005.000


Section #: 1.1.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.1.2

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** The `squashfs` filesystem type is a compressed read-only Linux filesystem embedded in small footprint systems. A `squashfs` image can be used without having to first decompress the image.

**MITRE ATT&CK Mappings:** M1050


Section #: 1.1.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.1.3

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** The `udf` filesystem type is the universal disk format used to implement ISO/IEC 13346 and ECMA-167 specifications. This is an open vendor filesystem type for data storage on a broad range of media. This filesystem type is necessary to support writing DVDs and newer optical disc formats.

**MITRE ATT&CK Mappings:** TA0005


Section #: 1.1.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.1.3

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** The `udf` filesystem type is the universal disk format used to implement ISO/IEC 13346 and ECMA-167 specifications. This is an open vendor filesystem type for data storage on a broad range of media. This filesystem type is necessary to support writing DVDs and newer optical disc formats.

**MITRE ATT&CK Mappings:** T1005


Section #: 1.1.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.1.3

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** The `udf` filesystem type is the universal disk format used to implement ISO/IEC 13346 and ECMA-167 specifications. This is an open vendor filesystem type for data storage on a broad range of media. This filesystem type is necessary to support writing DVDs and newer optical disc formats.

**MITRE ATT&CK Mappings:** T1005.000


Section #: 1.1.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.1.3

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** The `udf` filesystem type is the universal disk format used to implement ISO/IEC 13346 and ECMA-167 specifications. This is an open vendor filesystem type for data storage on a broad range of media. This filesystem type is necessary to support writing DVDs and newer optical disc formats.

**MITRE ATT&CK Mappings:** M1050


Section #: 1.1.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.2.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/tmp` directory is a world-writable directory used for temporary storage by all users and some applications.

**MITRE ATT&CK Mappings:** TA0005


Section #: 1.1.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.2.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/tmp` directory is a world-writable directory used for temporary storage by all users and some applications.

**MITRE ATT&CK Mappings:** T1499


Section #: 1.1.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.2.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/tmp` directory is a world-writable directory used for temporary storage by all users and some applications.

**MITRE ATT&CK Mappings:** T1499.001


Section #: 1.1.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.2.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/tmp` directory is a world-writable directory used for temporary storage by all users and some applications.

**MITRE ATT&CK Mappings:** M1022


Section #: 1.1.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.2.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `nodev` mount option specifies that the filesystem cannot contain special devices.

**MITRE ATT&CK Mappings:** TA0005


Section #: 1.1.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.2.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `nodev` mount option specifies that the filesystem cannot contain special devices.

**MITRE ATT&CK Mappings:** T1200


Section #: 1.1.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.2.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `nodev` mount option specifies that the filesystem cannot contain special devices.

**MITRE ATT&CK Mappings:** T1200.000


Section #: 1.1.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.2.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `nodev` mount option specifies that the filesystem cannot contain special devices.

**MITRE ATT&CK Mappings:** M1022


Section #: 1.1.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.2.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `noexec` mount option specifies that the filesystem cannot contain executable binaries.

**MITRE ATT&CK Mappings:** TA0005


Section #: 1.1.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.2.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `noexec` mount option specifies that the filesystem cannot contain executable binaries.

**MITRE ATT&CK Mappings:** T1204


Section #: 1.1.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.2.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `noexec` mount option specifies that the filesystem cannot contain executable binaries.

**MITRE ATT&CK Mappings:** T1204.002


Section #: 1.1.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.2.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `noexec` mount option specifies that the filesystem cannot contain executable binaries.

**MITRE ATT&CK Mappings:** M1022


Section #: 1.1.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.2.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `nosuid` mount option specifies that the filesystem cannot contain `setuid` files.

**MITRE ATT&CK Mappings:** TA0005


Section #: 1.1.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.2.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `nosuid` mount option specifies that the filesystem cannot contain `setuid` files.

**MITRE ATT&CK Mappings:** T1548


Section #: 1.1.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.2.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `nosuid` mount option specifies that the filesystem cannot contain `setuid` files.

**MITRE ATT&CK Mappings:** T1548.001


Section #: 1.1.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.2.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `nosuid` mount option specifies that the filesystem cannot contain `setuid` files.

**MITRE ATT&CK Mappings:** M1022


Section #: 1.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.3.1

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** The `/var` directory is used by daemons and other system services to temporarily store dynamic data. Some directories created by these processes may be world-writable.

**MITRE ATT&CK Mappings:** TA0006


Section #: 1.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.3.1

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** The `/var` directory is used by daemons and other system services to temporarily store dynamic data. Some directories created by these processes may be world-writable.

**MITRE ATT&CK Mappings:** T1499


Section #: 1.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.3.1

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** The `/var` directory is used by daemons and other system services to temporarily store dynamic data. Some directories created by these processes may be world-writable.

**MITRE ATT&CK Mappings:** T1499.001


Section #: 1.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.3.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `nodev` mount option specifies that the filesystem cannot contain special devices.

**MITRE ATT&CK Mappings:** TA0005


Section #: 1.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.3.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `nodev` mount option specifies that the filesystem cannot contain special devices.

**MITRE ATT&CK Mappings:** T1200


Section #: 1.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.3.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `nodev` mount option specifies that the filesystem cannot contain special devices.

**MITRE ATT&CK Mappings:** T1200.000


Section #: 1.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.3.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `nodev` mount option specifies that the filesystem cannot contain special devices.

**MITRE ATT&CK Mappings:** M1038


Section #: 1.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.3.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `nosuid` mount option specifies that the filesystem cannot contain `setuid` files.

**MITRE ATT&CK Mappings:** TA0005


Section #: 1.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.3.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `nosuid` mount option specifies that the filesystem cannot contain `setuid` files.

**MITRE ATT&CK Mappings:** T1548


Section #: 1.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.3.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `nosuid` mount option specifies that the filesystem cannot contain `setuid` files.

**MITRE ATT&CK Mappings:** T1548.001


Section #: 1.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.3.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `nosuid` mount option specifies that the filesystem cannot contain `setuid` files.

**MITRE ATT&CK Mappings:** M1038


Section #: 1.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.4.1

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** The `/var/tmp` directory is a world-writable directory used for temporary storage by all users and some applications. Temporary files residing in `/var/tmp` are to be preserved between reboots.

**MITRE ATT&CK Mappings:** TA0005


Section #: 1.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.4.1

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** The `/var/tmp` directory is a world-writable directory used for temporary storage by all users and some applications. Temporary files residing in `/var/tmp` are to be preserved between reboots.

**MITRE ATT&CK Mappings:** T1499


Section #: 1.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.4.1

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** The `/var/tmp` directory is a world-writable directory used for temporary storage by all users and some applications. Temporary files residing in `/var/tmp` are to be preserved between reboots.

**MITRE ATT&CK Mappings:** T1499.001


Section #: 1.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.4.1

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** The `/var/tmp` directory is a world-writable directory used for temporary storage by all users and some applications. Temporary files residing in `/var/tmp` are to be preserved between reboots.

**MITRE ATT&CK Mappings:** M1022


Section #: 1.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.4.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `noexec` mount option specifies that the filesystem cannot contain executable binaries.

**MITRE ATT&CK Mappings:** TA0005


Section #: 1.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.4.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `noexec` mount option specifies that the filesystem cannot contain executable binaries.

**MITRE ATT&CK Mappings:** T1204


Section #: 1.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.4.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `noexec` mount option specifies that the filesystem cannot contain executable binaries.

**MITRE ATT&CK Mappings:** T1204.002


Section #: 1.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.4.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `noexec` mount option specifies that the filesystem cannot contain executable binaries.

**MITRE ATT&CK Mappings:** M1022


Section #: 1.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.4.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `nosuid` mount option specifies that the filesystem cannot contain `setuid` files.

**MITRE ATT&CK Mappings:** TA0005


Section #: 1.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.4.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `nosuid` mount option specifies that the filesystem cannot contain `setuid` files.

**MITRE ATT&CK Mappings:** T1548


Section #: 1.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.4.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `nosuid` mount option specifies that the filesystem cannot contain `setuid` files.

**MITRE ATT&CK Mappings:** T1548.001


Section #: 1.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.4.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `nosuid` mount option specifies that the filesystem cannot contain `setuid` files.

**MITRE ATT&CK Mappings:** M1022


Section #: 1.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.4.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `nodev` mount option specifies that the filesystem cannot contain special devices.

**MITRE ATT&CK Mappings:** TA0005


Section #: 1.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.4.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `nodev` mount option specifies that the filesystem cannot contain special devices.

**MITRE ATT&CK Mappings:** T1200


Section #: 1.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.4.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `nodev` mount option specifies that the filesystem cannot contain special devices.

**MITRE ATT&CK Mappings:** T1200.000


Section #: 1.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.4.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `nodev` mount option specifies that the filesystem cannot contain special devices.

**MITRE ATT&CK Mappings:** M1022


Section #: 1.1.5
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.5.1

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** The `/var/log` directory is used by system services to store log data.

**MITRE ATT&CK Mappings:** TA0005


Section #: 1.1.5
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.5.1

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** The `/var/log` directory is used by system services to store log data.

**MITRE ATT&CK Mappings:** T1499


Section #: 1.1.5
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.5.1

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** The `/var/log` directory is used by system services to store log data.

**MITRE ATT&CK Mappings:** T1499.001


Section #: 1.1.5
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.5.1

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** The `/var/log` directory is used by system services to store log data.

**MITRE ATT&CK Mappings:** M1022


Section #: 1.1.5
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.5.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `nodev` mount option specifies that the filesystem cannot contain special devices.

**MITRE ATT&CK Mappings:** TA0005


Section #: 1.1.5
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.5.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `nodev` mount option specifies that the filesystem cannot contain special devices.

**MITRE ATT&CK Mappings:** T1200


Section #: 1.1.5
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.5.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `nodev` mount option specifies that the filesystem cannot contain special devices.

**MITRE ATT&CK Mappings:** T1200.000


Section #: 1.1.5
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.5.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `nodev` mount option specifies that the filesystem cannot contain special devices.

**MITRE ATT&CK Mappings:** M1022


Section #: 1.1.5
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.5.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `noexec` mount option specifies that the filesystem cannot contain executable binaries.

**MITRE ATT&CK Mappings:** TA0005


Section #: 1.1.5
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.5.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `noexec` mount option specifies that the filesystem cannot contain executable binaries.

**MITRE ATT&CK Mappings:** T1204


Section #: 1.1.5
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.5.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `noexec` mount option specifies that the filesystem cannot contain executable binaries.

**MITRE ATT&CK Mappings:** T1204.002


Section #: 1.1.5
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.5.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `noexec` mount option specifies that the filesystem cannot contain executable binaries.

**MITRE ATT&CK Mappings:** M1022


Section #: 1.1.5
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.5.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `nosuid` mount option specifies that the filesystem cannot contain `setuid` files.

**MITRE ATT&CK Mappings:** TA0005


Section #: 1.1.5
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.5.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `nosuid` mount option specifies that the filesystem cannot contain `setuid` files.

**MITRE ATT&CK Mappings:** T1548


Section #: 1.1.5
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.5.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `nosuid` mount option specifies that the filesystem cannot contain `setuid` files.

**MITRE ATT&CK Mappings:** T1548.001


Section #: 1.1.5
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.5.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `nosuid` mount option specifies that the filesystem cannot contain `setuid` files.

**MITRE ATT&CK Mappings:** M1022


Section #: 1.1.6
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.6.1

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** The auditing daemon, `auditd`, stores log data in the `/var/log/audit` directory.

**MITRE ATT&CK Mappings:** TA0005


Section #: 1.1.6
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.6.1

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** The auditing daemon, `auditd`, stores log data in the `/var/log/audit` directory.

**MITRE ATT&CK Mappings:** T1499


Section #: 1.1.6
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.6.1

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** The auditing daemon, `auditd`, stores log data in the `/var/log/audit` directory.

**MITRE ATT&CK Mappings:** T1499.001


Section #: 1.1.6
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.6.1

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** The auditing daemon, `auditd`, stores log data in the `/var/log/audit` directory.

**MITRE ATT&CK Mappings:** M1022


Section #: 1.1.6
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.6.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `noexec` mount option specifies that the filesystem cannot contain executable binaries.

**MITRE ATT&CK Mappings:** TA0005


Section #: 1.1.6
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.6.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `noexec` mount option specifies that the filesystem cannot contain executable binaries.

**MITRE ATT&CK Mappings:** T1204


Section #: 1.1.6
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.6.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `noexec` mount option specifies that the filesystem cannot contain executable binaries.

**MITRE ATT&CK Mappings:** T1204.002


Section #: 1.1.6
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.6.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `noexec` mount option specifies that the filesystem cannot contain executable binaries.

**MITRE ATT&CK Mappings:** M1022


Section #: 1.1.6
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.6.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `nodev` mount option specifies that the filesystem cannot contain special devices.

**MITRE ATT&CK Mappings:** TA0005


Section #: 1.1.6
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.6.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `nodev` mount option specifies that the filesystem cannot contain special devices.

**MITRE ATT&CK Mappings:** T1200


Section #: 1.1.6
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.6.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `nodev` mount option specifies that the filesystem cannot contain special devices.

**MITRE ATT&CK Mappings:** T1200.000


Section #: 1.1.6
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.6.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `nodev` mount option specifies that the filesystem cannot contain special devices.

**MITRE ATT&CK Mappings:** M1022


Section #: 1.1.6
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.6.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `nosuid` mount option specifies that the filesystem cannot contain `setuid` files.

**MITRE ATT&CK Mappings:** TA0005


Section #: 1.1.6
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.6.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `nosuid` mount option specifies that the filesystem cannot contain `setuid` files.

**MITRE ATT&CK Mappings:** T1548


Section #: 1.1.6
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.6.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `nosuid` mount option specifies that the filesystem cannot contain `setuid` files.

**MITRE ATT&CK Mappings:** T1548.001


Section #: 1.1.6
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.6.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `nosuid` mount option specifies that the filesystem cannot contain `setuid` files.

**MITRE ATT&CK Mappings:** M1022


Section #: 1.1.7
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.7.1

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** The `/home` directory is used to support disk storage needs of local users.

**MITRE ATT&CK Mappings:** TA0005


Section #: 1.1.7
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.7.1

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** The `/home` directory is used to support disk storage needs of local users.

**MITRE ATT&CK Mappings:** T1499


Section #: 1.1.7
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.7.1

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** The `/home` directory is used to support disk storage needs of local users.

**MITRE ATT&CK Mappings:** T1499.001


Section #: 1.1.7
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.7.1

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** The `/home` directory is used to support disk storage needs of local users.

**MITRE ATT&CK Mappings:** M1038


Section #: 1.1.7
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.7.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `nodev` mount option specifies that the filesystem cannot contain special devices.

**MITRE ATT&CK Mappings:** TA0005


Section #: 1.1.7
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.7.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `nodev` mount option specifies that the filesystem cannot contain special devices.

**MITRE ATT&CK Mappings:** T1200


Section #: 1.1.7
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.7.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `nodev` mount option specifies that the filesystem cannot contain special devices.

**MITRE ATT&CK Mappings:** T1200.000


Section #: 1.1.7
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.7.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `nodev` mount option specifies that the filesystem cannot contain special devices.

**MITRE ATT&CK Mappings:** M1022


Section #: 1.1.7
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.7.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `nosuid` mount option specifies that the filesystem cannot contain `setuid` files.

**MITRE ATT&CK Mappings:** TA0005


Section #: 1.1.7
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.7.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `nosuid` mount option specifies that the filesystem cannot contain `setuid` files.

**MITRE ATT&CK Mappings:** T1548


Section #: 1.1.7
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.7.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `nosuid` mount option specifies that the filesystem cannot contain `setuid` files.

**MITRE ATT&CK Mappings:** T1548.001


Section #: 1.1.7
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.7.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `nosuid` mount option specifies that the filesystem cannot contain `setuid` files.

**MITRE ATT&CK Mappings:** M1022


Section #: 1.1.8
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.8.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `nodev` mount option specifies that the filesystem cannot contain special devices.

**MITRE ATT&CK Mappings:** TA0005


Section #: 1.1.8
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.8.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `nodev` mount option specifies that the filesystem cannot contain special devices.

**MITRE ATT&CK Mappings:** T1200


Section #: 1.1.8
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.8.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `nodev` mount option specifies that the filesystem cannot contain special devices.

**MITRE ATT&CK Mappings:** T1200.000


Section #: 1.1.8
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.8.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `nodev` mount option specifies that the filesystem cannot contain special devices.

**MITRE ATT&CK Mappings:** M1038


Section #: 1.1.8
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.8.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `noexec` mount option specifies that the filesystem cannot contain executable binaries.

**MITRE ATT&CK Mappings:** TA0005


Section #: 1.1.8
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.8.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `noexec` mount option specifies that the filesystem cannot contain executable binaries.

**MITRE ATT&CK Mappings:** T1204


Section #: 1.1.8
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.8.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `noexec` mount option specifies that the filesystem cannot contain executable binaries.

**MITRE ATT&CK Mappings:** T1204.002


Section #: 1.1.8
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.8.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `noexec` mount option specifies that the filesystem cannot contain executable binaries.

**MITRE ATT&CK Mappings:** M1022


Section #: 1.1.8
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.8.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `nosuid` mount option specifies that the filesystem cannot contain `setuid` files.

**MITRE ATT&CK Mappings:** TA0005


Section #: 1.1.8
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.8.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `nosuid` mount option specifies that the filesystem cannot contain `setuid` files.

**MITRE ATT&CK Mappings:** T1548


Section #: 1.1.8
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.8.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `nosuid` mount option specifies that the filesystem cannot contain `setuid` files.

**MITRE ATT&CK Mappings:** T1548.001


Section #: 1.1.8
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.8.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `nosuid` mount option specifies that the filesystem cannot contain `setuid` files.

**MITRE ATT&CK Mappings:** M1038


Section #: 1.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.2.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Systems need to have package manager repositories configured to ensure they receive the latest patches and updates.

**MITRE ATT&CK Mappings:** TA0001


Section #: 1.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.2.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Systems need to have package manager repositories configured to ensure they receive the latest patches and updates.

**MITRE ATT&CK Mappings:** T1068


Section #: 1.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.2.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Systems need to have package manager repositories configured to ensure they receive the latest patches and updates.

**MITRE ATT&CK Mappings:** T1068.000


Section #: 1.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.2.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Systems need to have package manager repositories configured to ensure they receive the latest patches and updates.

**MITRE ATT&CK Mappings:** T1195


Section #: 1.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.2.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Systems need to have package manager repositories configured to ensure they receive the latest patches and updates.

**MITRE ATT&CK Mappings:** T1195.001


Section #: 1.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.2.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Systems need to have package manager repositories configured to ensure they receive the latest patches and updates.

**MITRE ATT&CK Mappings:** T1195.002


Section #: 1.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.2.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Systems need to have package manager repositories configured to ensure they receive the latest patches and updates.

**MITRE ATT&CK Mappings:** T1203


Section #: 1.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.2.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Systems need to have package manager repositories configured to ensure they receive the latest patches and updates.

**MITRE ATT&CK Mappings:** T1203.000


Section #: 1.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.2.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Systems need to have package manager repositories configured to ensure they receive the latest patches and updates.

**MITRE ATT&CK Mappings:** T1210


Section #: 1.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.2.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Systems need to have package manager repositories configured to ensure they receive the latest patches and updates.

**MITRE ATT&CK Mappings:** T1210.000


Section #: 1.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.2.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Systems need to have package manager repositories configured to ensure they receive the latest patches and updates.

**MITRE ATT&CK Mappings:** T1211


Section #: 1.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.2.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Systems need to have package manager repositories configured to ensure they receive the latest patches and updates.

**MITRE ATT&CK Mappings:** T1211.000


Section #: 1.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.2.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Systems need to have package manager repositories configured to ensure they receive the latest patches and updates.

**MITRE ATT&CK Mappings:** T1212


Section #: 1.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.2.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Systems need to have package manager repositories configured to ensure they receive the latest patches and updates.

**MITRE ATT&CK Mappings:** T1212.000


Section #: 1.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.2.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Systems need to have package manager repositories configured to ensure they receive the latest patches and updates.

**MITRE ATT&CK Mappings:** M1051


Section #: 1.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.2.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Most packages managers implement GPG key signing to verify package integrity during installation.

**MITRE ATT&CK Mappings:** TA0001


Section #: 1.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.2.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Most packages managers implement GPG key signing to verify package integrity during installation.

**MITRE ATT&CK Mappings:** T1195


Section #: 1.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.2.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Most packages managers implement GPG key signing to verify package integrity during installation.

**MITRE ATT&CK Mappings:** T1195.001


Section #: 1.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.2.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Most packages managers implement GPG key signing to verify package integrity during installation.

**MITRE ATT&CK Mappings:** T1195.002


Section #: 1.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.2.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Most packages managers implement GPG key signing to verify package integrity during installation.

**MITRE ATT&CK Mappings:** M1051


Section #: 1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.3.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** AIDE takes a snapshot of filesystem state including modification times, permissions, and file hashes which can then be used to compare against the current state of the filesystem to detect modifications to the system.

**MITRE ATT&CK Mappings:** T1036


Section #: 1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.3.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** AIDE takes a snapshot of filesystem state including modification times, permissions, and file hashes which can then be used to compare against the current state of the filesystem to detect modifications to the system.

**MITRE ATT&CK Mappings:** T1036.002


Section #: 1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.3.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** AIDE takes a snapshot of filesystem state including modification times, permissions, and file hashes which can then be used to compare against the current state of the filesystem to detect modifications to the system.

**MITRE ATT&CK Mappings:** T1036.003


Section #: 1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.3.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** AIDE takes a snapshot of filesystem state including modification times, permissions, and file hashes which can then be used to compare against the current state of the filesystem to detect modifications to the system.

**MITRE ATT&CK Mappings:** T1036.004


Section #: 1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.3.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** AIDE takes a snapshot of filesystem state including modification times, permissions, and file hashes which can then be used to compare against the current state of the filesystem to detect modifications to the system.

**MITRE ATT&CK Mappings:** T1036.005


Section #: 1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.3.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** AIDE takes a snapshot of filesystem state including modification times, permissions, and file hashes which can then be used to compare against the current state of the filesystem to detect modifications to the system.

**MITRE ATT&CK Mappings:** T1565


Section #: 1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.3.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** AIDE takes a snapshot of filesystem state including modification times, permissions, and file hashes which can then be used to compare against the current state of the filesystem to detect modifications to the system.

**MITRE ATT&CK Mappings:** T1565.001


Section #: 1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.3.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Periodic checking of the filesystem integrity is needed to detect changes to the filesystem.

**MITRE ATT&CK Mappings:** TA0040


Section #: 1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.3.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Periodic checking of the filesystem integrity is needed to detect changes to the filesystem.

**MITRE ATT&CK Mappings:** T1036


Section #: 1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.3.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Periodic checking of the filesystem integrity is needed to detect changes to the filesystem.

**MITRE ATT&CK Mappings:** T1036.002


Section #: 1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.3.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Periodic checking of the filesystem integrity is needed to detect changes to the filesystem.

**MITRE ATT&CK Mappings:** T1036.003


Section #: 1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.3.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Periodic checking of the filesystem integrity is needed to detect changes to the filesystem.

**MITRE ATT&CK Mappings:** T1036.004


Section #: 1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.3.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Periodic checking of the filesystem integrity is needed to detect changes to the filesystem.

**MITRE ATT&CK Mappings:** T1036.005


Section #: 1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.3.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Periodic checking of the filesystem integrity is needed to detect changes to the filesystem.

**MITRE ATT&CK Mappings:** T1565


Section #: 1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.3.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Periodic checking of the filesystem integrity is needed to detect changes to the filesystem.

**MITRE ATT&CK Mappings:** T1565.001


Section #: 1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.3.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Periodic checking of the filesystem integrity is needed to detect changes to the filesystem.

**MITRE ATT&CK Mappings:** M1022


Section #: 1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.4.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Setting the boot loader password will require that anyone rebooting the system must enter a password before being able to set command line boot parameters

**MITRE ATT&CK Mappings:** TA0003


Section #: 1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.4.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Setting the boot loader password will require that anyone rebooting the system must enter a password before being able to set command line boot parameters

**MITRE ATT&CK Mappings:** T1542


Section #: 1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.4.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Setting the boot loader password will require that anyone rebooting the system must enter a password before being able to set command line boot parameters

**MITRE ATT&CK Mappings:** T1542.000


Section #: 1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.4.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Setting the boot loader password will require that anyone rebooting the system must enter a password before being able to set command line boot parameters

**MITRE ATT&CK Mappings:** M1046


Section #: 1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.4.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The grub configuration file contains information on boot settings and passwords for unlocking boot options.

**MITRE ATT&CK Mappings:** TA0005


Section #: 1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.4.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The grub configuration file contains information on boot settings and passwords for unlocking boot options.

**MITRE ATT&CK Mappings:** TA0007


Section #: 1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.4.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The grub configuration file contains information on boot settings and passwords for unlocking boot options.

**MITRE ATT&CK Mappings:** T1542


Section #: 1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.4.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The grub configuration file contains information on boot settings and passwords for unlocking boot options.

**MITRE ATT&CK Mappings:** T1542.000


Section #: 1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.4.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The grub configuration file contains information on boot settings and passwords for unlocking boot options.

**MITRE ATT&CK Mappings:** M1022


Section #: 1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.4.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Single user mode is used for recovery when the system detects an issue during boot or by manual selection from the bootloader.

**MITRE ATT&CK Mappings:** TA0005


Section #: 1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.4.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Single user mode is used for recovery when the system detects an issue during boot or by manual selection from the bootloader.

**MITRE ATT&CK Mappings:** T1548


Section #: 1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.4.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Single user mode is used for recovery when the system detects an issue during boot or by manual selection from the bootloader.

**MITRE ATT&CK Mappings:** T1548.000


Section #: 1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.4.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Single user mode is used for recovery when the system detects an issue during boot or by manual selection from the bootloader.

**MITRE ATT&CK Mappings:** M1022


Section #: 1.5
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.5.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Address space layout randomization (ASLR) is an exploit mitigation technique which randomly arranges the address space of key data areas of a process.

**MITRE ATT&CK Mappings:** TA0002


Section #: 1.5
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.5.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Address space layout randomization (ASLR) is an exploit mitigation technique which randomly arranges the address space of key data areas of a process.

**MITRE ATT&CK Mappings:** T1068


Section #: 1.5
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.5.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Address space layout randomization (ASLR) is an exploit mitigation technique which randomly arranges the address space of key data areas of a process.

**MITRE ATT&CK Mappings:** T1068.000


Section #: 1.5
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.5.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Address space layout randomization (ASLR) is an exploit mitigation technique which randomly arranges the address space of key data areas of a process.

**MITRE ATT&CK Mappings:** M1050


Section #: 1.5
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.5.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** `prelink` is a program that modifies ELF shared libraries and ELF dynamically linked binaries in such a way that the time needed for the dynamic linker to perform relocations at startup significantly decreases.

**MITRE ATT&CK Mappings:** TA0002


Section #: 1.5
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.5.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** `prelink` is a program that modifies ELF shared libraries and ELF dynamically linked binaries in such a way that the time needed for the dynamic linker to perform relocations at startup significantly decreases.

**MITRE ATT&CK Mappings:** T1055


Section #: 1.5
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.5.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** `prelink` is a program that modifies ELF shared libraries and ELF dynamically linked binaries in such a way that the time needed for the dynamic linker to perform relocations at startup significantly decreases.

**MITRE ATT&CK Mappings:** T1055.009


Section #: 1.5
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.5.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** `prelink` is a program that modifies ELF shared libraries and ELF dynamically linked binaries in such a way that the time needed for the dynamic linker to perform relocations at startup significantly decreases.

**MITRE ATT&CK Mappings:** T1065


Section #: 1.5
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.5.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** `prelink` is a program that modifies ELF shared libraries and ELF dynamically linked binaries in such a way that the time needed for the dynamic linker to perform relocations at startup significantly decreases.

**MITRE ATT&CK Mappings:** T1065.001


Section #: 1.5
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.5.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** `prelink` is a program that modifies ELF shared libraries and ELF dynamically linked binaries in such a way that the time needed for the dynamic linker to perform relocations at startup significantly decreases.

**MITRE ATT&CK Mappings:** M1050


Section #: 1.5
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.5.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** A core dump is the memory of an executable program. It is generally used to determine why a program aborted. It can also be used to glean confidential information from a core file. The system provides the ability to set a soft limit for core dumps, but this can be overridden by the user.

**MITRE ATT&CK Mappings:** TA0007


Section #: 1.5
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.5.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** A core dump is the memory of an executable program. It is generally used to determine why a program aborted. It can also be used to glean confidential information from a core file. The system provides the ability to set a soft limit for core dumps, but this can be overridden by the user.

**MITRE ATT&CK Mappings:** T1005


Section #: 1.5
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.5.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** A core dump is the memory of an executable program. It is generally used to determine why a program aborted. It can also be used to glean confidential information from a core file. The system provides the ability to set a soft limit for core dumps, but this can be overridden by the user.

**MITRE ATT&CK Mappings:** T1005.000


Section #: 1.6.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.6.1.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** AppArmor provides Mandatory Access Controls.

**MITRE ATT&CK Mappings:** TA0003


Section #: 1.6.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.6.1.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** AppArmor provides Mandatory Access Controls.

**MITRE ATT&CK Mappings:** T1068


Section #: 1.6.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.6.1.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** AppArmor provides Mandatory Access Controls.

**MITRE ATT&CK Mappings:** T1068.000


Section #: 1.6.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.6.1.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** AppArmor provides Mandatory Access Controls.

**MITRE ATT&CK Mappings:** T1565


Section #: 1.6.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.6.1.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** AppArmor provides Mandatory Access Controls.

**MITRE ATT&CK Mappings:** T1565.001


Section #: 1.6.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.6.1.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** AppArmor provides Mandatory Access Controls.

**MITRE ATT&CK Mappings:** T1565.003


Section #: 1.6.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.6.1.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** AppArmor provides Mandatory Access Controls.

**MITRE ATT&CK Mappings:** M1026


Section #: 1.6.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.6.1.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Configure AppArmor to be enabled at boot time and verify that it has not been overwritten by the bootloader boot parameters.

_Note: This recommendation is designed around the grub bootloader, if LILO or another bootloader is in use in your environment enact equivalent settings._

**MITRE ATT&CK Mappings:** TA0003


Section #: 1.6.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.6.1.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Configure AppArmor to be enabled at boot time and verify that it has not been overwritten by the bootloader boot parameters.

_Note: This recommendation is designed around the grub bootloader, if LILO or another bootloader is in use in your environment enact equivalent settings._

**MITRE ATT&CK Mappings:** T1068


Section #: 1.6.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.6.1.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Configure AppArmor to be enabled at boot time and verify that it has not been overwritten by the bootloader boot parameters.

_Note: This recommendation is designed around the grub bootloader, if LILO or another bootloader is in use in your environment enact equivalent settings._

**MITRE ATT&CK Mappings:** T1068.000


Section #: 1.6.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.6.1.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Configure AppArmor to be enabled at boot time and verify that it has not been overwritten by the bootloader boot parameters.

_Note: This recommendation is designed around the grub bootloader, if LILO or another bootloader is in use in your environment enact equivalent settings._

**MITRE ATT&CK Mappings:** T1565


Section #: 1.6.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.6.1.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Configure AppArmor to be enabled at boot time and verify that it has not been overwritten by the bootloader boot parameters.

_Note: This recommendation is designed around the grub bootloader, if LILO or another bootloader is in use in your environment enact equivalent settings._

**MITRE ATT&CK Mappings:** T1565.001


Section #: 1.6.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.6.1.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Configure AppArmor to be enabled at boot time and verify that it has not been overwritten by the bootloader boot parameters.

_Note: This recommendation is designed around the grub bootloader, if LILO or another bootloader is in use in your environment enact equivalent settings._

**MITRE ATT&CK Mappings:** T1565.003


Section #: 1.6.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.6.1.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Configure AppArmor to be enabled at boot time and verify that it has not been overwritten by the bootloader boot parameters.

_Note: This recommendation is designed around the grub bootloader, if LILO or another bootloader is in use in your environment enact equivalent settings._

**MITRE ATT&CK Mappings:** M1026


Section #: 1.6.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.6.1.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** AppArmor profiles define what resources applications are able to access.

**MITRE ATT&CK Mappings:** TA0005


Section #: 1.6.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.6.1.4

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** AppArmor profiles define what resources applications are able to access.

**MITRE ATT&CK Mappings:** TA0005


Section #: 1.6.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.6.1.4

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** AppArmor profiles define what resources applications are able to access.

**MITRE ATT&CK Mappings:** T1068


Section #: 1.6.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.6.1.4

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** AppArmor profiles define what resources applications are able to access.

**MITRE ATT&CK Mappings:** T1068.000


Section #: 1.6.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.6.1.4

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** AppArmor profiles define what resources applications are able to access.

**MITRE ATT&CK Mappings:** T1565


Section #: 1.6.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.6.1.4

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** AppArmor profiles define what resources applications are able to access.

**MITRE ATT&CK Mappings:** T1565.001


Section #: 1.6.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.6.1.4

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** AppArmor profiles define what resources applications are able to access.

**MITRE ATT&CK Mappings:** T1565.003


Section #: 1.7
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.7.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The contents of the `/etc/motd` file are displayed to users after login and function as a message of the day for authenticated users.

Unix-based systems have typically displayed information about the OS release and patch level upon logging in to the system. This information can be useful to developers who are developing software for a particular OS platform. If `mingetty(8)` supports the following options, they display operating system information: `\m` - machine architecture `\r` - operating system release `\s` - operating system name `\v` - operating system version

**MITRE ATT&CK Mappings:** TA0007


Section #: 1.7
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.7.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The contents of the `/etc/motd` file are displayed to users after login and function as a message of the day for authenticated users.

Unix-based systems have typically displayed information about the OS release and patch level upon logging in to the system. This information can be useful to developers who are developing software for a particular OS platform. If `mingetty(8)` supports the following options, they display operating system information: `\m` - machine architecture `\r` - operating system release `\s` - operating system name `\v` - operating system version

**MITRE ATT&CK Mappings:** T1082


Section #: 1.7
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.7.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The contents of the `/etc/motd` file are displayed to users after login and function as a message of the day for authenticated users.

Unix-based systems have typically displayed information about the OS release and patch level upon logging in to the system. This information can be useful to developers who are developing software for a particular OS platform. If `mingetty(8)` supports the following options, they display operating system information: `\m` - machine architecture `\r` - operating system release `\s` - operating system name `\v` - operating system version

**MITRE ATT&CK Mappings:** T1082.000


Section #: 1.7
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.7.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The contents of the `/etc/motd` file are displayed to users after login and function as a message of the day for authenticated users.

Unix-based systems have typically displayed information about the OS release and patch level upon logging in to the system. This information can be useful to developers who are developing software for a particular OS platform. If `mingetty(8)` supports the following options, they display operating system information: `\m` - machine architecture `\r` - operating system release `\s` - operating system name `\v` - operating system version

**MITRE ATT&CK Mappings:** T1592


Section #: 1.7
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.7.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The contents of the `/etc/motd` file are displayed to users after login and function as a message of the day for authenticated users.

Unix-based systems have typically displayed information about the OS release and patch level upon logging in to the system. This information can be useful to developers who are developing software for a particular OS platform. If `mingetty(8)` supports the following options, they display operating system information: `\m` - machine architecture `\r` - operating system release `\s` - operating system name `\v` - operating system version

**MITRE ATT&CK Mappings:** T1592.004


Section #: 1.7
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.7.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The contents of the `/etc/issue` file are displayed to users prior to login for local terminals.

Unix-based systems have typically displayed information about the OS release and patch level upon logging in to the system. This information can be useful to developers who are developing software for a particular OS platform. If `mingetty(8)` supports the following options, they display operating system information: `\m` - machine architecture `\r` - operating system release `\s` - operating system name `\v` - operating system version - or the operating system's name

**MITRE ATT&CK Mappings:** TA0007


Section #: 1.7
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.7.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The contents of the `/etc/issue` file are displayed to users prior to login for local terminals.

Unix-based systems have typically displayed information about the OS release and patch level upon logging in to the system. This information can be useful to developers who are developing software for a particular OS platform. If `mingetty(8)` supports the following options, they display operating system information: `\m` - machine architecture `\r` - operating system release `\s` - operating system name `\v` - operating system version - or the operating system's name

**MITRE ATT&CK Mappings:** T1082


Section #: 1.7
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.7.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The contents of the `/etc/issue` file are displayed to users prior to login for local terminals.

Unix-based systems have typically displayed information about the OS release and patch level upon logging in to the system. This information can be useful to developers who are developing software for a particular OS platform. If `mingetty(8)` supports the following options, they display operating system information: `\m` - machine architecture `\r` - operating system release `\s` - operating system name `\v` - operating system version - or the operating system's name

**MITRE ATT&CK Mappings:** T1082.000


Section #: 1.7
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.7.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The contents of the `/etc/issue` file are displayed to users prior to login for local terminals.

Unix-based systems have typically displayed information about the OS release and patch level upon logging in to the system. This information can be useful to developers who are developing software for a particular OS platform. If `mingetty(8)` supports the following options, they display operating system information: `\m` - machine architecture `\r` - operating system release `\s` - operating system name `\v` - operating system version - or the operating system's name

**MITRE ATT&CK Mappings:** T1592


Section #: 1.7
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.7.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The contents of the `/etc/issue` file are displayed to users prior to login for local terminals.

Unix-based systems have typically displayed information about the OS release and patch level upon logging in to the system. This information can be useful to developers who are developing software for a particular OS platform. If `mingetty(8)` supports the following options, they display operating system information: `\m` - machine architecture `\r` - operating system release `\s` - operating system name `\v` - operating system version - or the operating system's name

**MITRE ATT&CK Mappings:** T1592.004


Section #: 1.7
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.7.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The contents of the `/etc/issue.net` file are displayed to users prior to login for remote connections from configured services.

Unix-based systems have typically displayed information about the OS release and patch level upon logging in to the system. This information can be useful to developers who are developing software for a particular OS platform. If `mingetty(8)` supports the following options, they display operating system information: `\m` - machine architecture `\r` - operating system release `\s` - operating system name `\v` - operating system version

**MITRE ATT&CK Mappings:** TA0007


Section #: 1.7
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.7.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The contents of the `/etc/issue.net` file are displayed to users prior to login for remote connections from configured services.

Unix-based systems have typically displayed information about the OS release and patch level upon logging in to the system. This information can be useful to developers who are developing software for a particular OS platform. If `mingetty(8)` supports the following options, they display operating system information: `\m` - machine architecture `\r` - operating system release `\s` - operating system name `\v` - operating system version

**MITRE ATT&CK Mappings:** T1018


Section #: 1.7
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.7.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The contents of the `/etc/issue.net` file are displayed to users prior to login for remote connections from configured services.

Unix-based systems have typically displayed information about the OS release and patch level upon logging in to the system. This information can be useful to developers who are developing software for a particular OS platform. If `mingetty(8)` supports the following options, they display operating system information: `\m` - machine architecture `\r` - operating system release `\s` - operating system name `\v` - operating system version

**MITRE ATT&CK Mappings:** T1018.000


Section #: 1.7
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.7.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The contents of the `/etc/issue.net` file are displayed to users prior to login for remote connections from configured services.

Unix-based systems have typically displayed information about the OS release and patch level upon logging in to the system. This information can be useful to developers who are developing software for a particular OS platform. If `mingetty(8)` supports the following options, they display operating system information: `\m` - machine architecture `\r` - operating system release `\s` - operating system name `\v` - operating system version

**MITRE ATT&CK Mappings:** T1082


Section #: 1.7
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.7.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The contents of the `/etc/issue.net` file are displayed to users prior to login for remote connections from configured services.

Unix-based systems have typically displayed information about the OS release and patch level upon logging in to the system. This information can be useful to developers who are developing software for a particular OS platform. If `mingetty(8)` supports the following options, they display operating system information: `\m` - machine architecture `\r` - operating system release `\s` - operating system name `\v` - operating system version

**MITRE ATT&CK Mappings:** T1082.000


Section #: 1.7
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.7.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The contents of the `/etc/issue.net` file are displayed to users prior to login for remote connections from configured services.

Unix-based systems have typically displayed information about the OS release and patch level upon logging in to the system. This information can be useful to developers who are developing software for a particular OS platform. If `mingetty(8)` supports the following options, they display operating system information: `\m` - machine architecture `\r` - operating system release `\s` - operating system name `\v` - operating system version

**MITRE ATT&CK Mappings:** T1592


Section #: 1.7
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.7.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The contents of the `/etc/issue.net` file are displayed to users prior to login for remote connections from configured services.

Unix-based systems have typically displayed information about the OS release and patch level upon logging in to the system. This information can be useful to developers who are developing software for a particular OS platform. If `mingetty(8)` supports the following options, they display operating system information: `\m` - machine architecture `\r` - operating system release `\s` - operating system name `\v` - operating system version

**MITRE ATT&CK Mappings:** T1592.004


Section #: 1.7
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.7.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The contents of the `/etc/motd` file are displayed to users after login and function as a message of the day for authenticated users.

**MITRE ATT&CK Mappings:** TA0005


Section #: 1.7
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.7.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The contents of the `/etc/motd` file are displayed to users after login and function as a message of the day for authenticated users.

**MITRE ATT&CK Mappings:** T1222


Section #: 1.7
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.7.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The contents of the `/etc/motd` file are displayed to users after login and function as a message of the day for authenticated users.

**MITRE ATT&CK Mappings:** T1222.002


Section #: 1.7
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.7.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The contents of the `/etc/motd` file are displayed to users after login and function as a message of the day for authenticated users.

**MITRE ATT&CK Mappings:** M1022


Section #: 1.7
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.7.5

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The contents of the `/etc/issue` file are displayed to users prior to login for local terminals.

**MITRE ATT&CK Mappings:** TA0005


Section #: 1.7
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.7.5

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The contents of the `/etc/issue` file are displayed to users prior to login for local terminals.

**MITRE ATT&CK Mappings:** T1222


Section #: 1.7
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.7.5

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The contents of the `/etc/issue` file are displayed to users prior to login for local terminals.

**MITRE ATT&CK Mappings:** T1222.002


Section #: 1.7
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.7.5

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The contents of the `/etc/issue` file are displayed to users prior to login for local terminals.

**MITRE ATT&CK Mappings:** M1022


Section #: 1.7
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.7.6

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The contents of the `/etc/issue.net` file are displayed to users prior to login for remote connections from configured services.

**MITRE ATT&CK Mappings:** TA0005


Section #: 1.7
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.7.6

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The contents of the `/etc/issue.net` file are displayed to users prior to login for remote connections from configured services.

**MITRE ATT&CK Mappings:** T1222


Section #: 1.7
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.7.6

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The contents of the `/etc/issue.net` file are displayed to users prior to login for remote connections from configured services.

**MITRE ATT&CK Mappings:** T1222.002


Section #: 1.7
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.7.6

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The contents of the `/etc/issue.net` file are displayed to users prior to login for remote connections from configured services.

**MITRE ATT&CK Mappings:** M1022


Section #: 1.8
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.8.1

**Profiles:** Level 2 - Server

**Description:** The GNOME Display Manager (GDM) is a program that manages graphical display servers and handles graphical user logins.

**MITRE ATT&CK Mappings:** TA0002


Section #: 1.8
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.8.1

**Profiles:** Level 2 - Server

**Description:** The GNOME Display Manager (GDM) is a program that manages graphical display servers and handles graphical user logins.

**MITRE ATT&CK Mappings:** T1543


Section #: 1.8
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.8.1

**Profiles:** Level 2 - Server

**Description:** The GNOME Display Manager (GDM) is a program that manages graphical display servers and handles graphical user logins.

**MITRE ATT&CK Mappings:** T1543.002


Section #: 1.8
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.8.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** GDM is the GNOME Display Manager which handles graphical login for GNOME based systems.

**MITRE ATT&CK Mappings:** TA0007


Section #: 1.8
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.8.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** GDM is the GNOME Display Manager which handles graphical login for GNOME based systems.

The `disable-user-list` option controls if a list of users is displayed on the login screen

**MITRE ATT&CK Mappings:** TA0007


Section #: 1.8
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.8.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** GDM is the GNOME Display Manager which handles graphical login for GNOME based systems.

The `disable-user-list` option controls if a list of users is displayed on the login screen

**MITRE ATT&CK Mappings:** T1078


Section #: 1.8
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.8.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** GDM is the GNOME Display Manager which handles graphical login for GNOME based systems.

The `disable-user-list` option controls if a list of users is displayed on the login screen

**MITRE ATT&CK Mappings:** T1078.001


Section #: 1.8
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.8.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** GDM is the GNOME Display Manager which handles graphical login for GNOME based systems.

The `disable-user-list` option controls if a list of users is displayed on the login screen

**MITRE ATT&CK Mappings:** T1078.002


Section #: 1.8
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.8.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** GDM is the GNOME Display Manager which handles graphical login for GNOME based systems.

The `disable-user-list` option controls if a list of users is displayed on the login screen

**MITRE ATT&CK Mappings:** T1078.003


Section #: 1.8
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.8.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** GDM is the GNOME Display Manager which handles graphical login for GNOME based systems.

The `disable-user-list` option controls if a list of users is displayed on the login screen

**MITRE ATT&CK Mappings:** T1087


Section #: 1.8
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.8.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** GDM is the GNOME Display Manager which handles graphical login for GNOME based systems.

The `disable-user-list` option controls if a list of users is displayed on the login screen

**MITRE ATT&CK Mappings:** T1087.001


Section #: 1.8
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.8.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** GDM is the GNOME Display Manager which handles graphical login for GNOME based systems.

The `disable-user-list` option controls if a list of users is displayed on the login screen

**MITRE ATT&CK Mappings:** T1087.002


Section #: 1.8
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.8.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** GDM is the GNOME Display Manager which handles graphical login for GNOME based systems.

The `disable-user-list` option controls if a list of users is displayed on the login screen

**MITRE ATT&CK Mappings:** M1028


Section #: 1.8
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.8.8

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `autorun-never` setting allows the GNOME Desktop Display Manager to disable autorun through GDM.

**MITRE ATT&CK Mappings:** T1091


Section #: 1.8
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.8.9

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The autorun-never setting allows the GNOME Desktop Display Manager to disable autorun through GDM.

By using the lockdown mode in dconf, you can prevent users from changing specific settings.

To lock down a dconf key or subpath, create a locks subdirectory in the keyfile directory. The files inside this directory contain a list of keys or subpaths to lock. Just as with the keyfiles, you may add any number of files to this directory.

Example Lock File:
```
# Lock desktop media-handling settings
/org/gnome/desktop/media-handling/autorun-never
```

**MITRE ATT&CK Mappings:** T1091


Section #: 1.8
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.8.10

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** X Display Manager Control Protocol (XDMCP) is designed to provide authenticated access to display management services for remote displays

**MITRE ATT&CK Mappings:** TA0002


Section #: 1.8
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.8.10

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** X Display Manager Control Protocol (XDMCP) is designed to provide authenticated access to display management services for remote displays

**MITRE ATT&CK Mappings:** T1040


Section #: 1.8
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.8.10

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** X Display Manager Control Protocol (XDMCP) is designed to provide authenticated access to display management services for remote displays

**MITRE ATT&CK Mappings:** T1040.000


Section #: 1.8
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.8.10

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** X Display Manager Control Protocol (XDMCP) is designed to provide authenticated access to display management services for remote displays

**MITRE ATT&CK Mappings:** T1056


Section #: 1.8
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.8.10

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** X Display Manager Control Protocol (XDMCP) is designed to provide authenticated access to display management services for remote displays

**MITRE ATT&CK Mappings:** T1056.001


Section #: 1.8
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.8.10

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** X Display Manager Control Protocol (XDMCP) is designed to provide authenticated access to display management services for remote displays

**MITRE ATT&CK Mappings:** T1557


Section #: 1.8
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.8.10

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** X Display Manager Control Protocol (XDMCP) is designed to provide authenticated access to display management services for remote displays

**MITRE ATT&CK Mappings:** T1557.000


Section #: 1.8
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.8.10

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** X Display Manager Control Protocol (XDMCP) is designed to provide authenticated access to display management services for remote displays

**MITRE ATT&CK Mappings:** M1050


Section #: 2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** A network port is identified by its number, the associated IP address, and the type of the communication protocol such as TCP or UDP.

A listening port is a network port on which an application or process listens on, acting as a communication endpoint.

Each listening port can be open or closed (filtered) using a firewall. In general terms, an open port is a network port that accepts incoming packets from remote locations.

**MITRE ATT&CK Mappings:** TA0008


Section #: 2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** A network port is identified by its number, the associated IP address, and the type of the communication protocol such as TCP or UDP.

A listening port is a network port on which an application or process listens on, acting as a communication endpoint.

Each listening port can be open or closed (filtered) using a firewall. In general terms, an open port is a network port that accepts incoming packets from remote locations.

**MITRE ATT&CK Mappings:** T1203


Section #: 2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** A network port is identified by its number, the associated IP address, and the type of the communication protocol such as TCP or UDP.

A listening port is a network port on which an application or process listens on, acting as a communication endpoint.

Each listening port can be open or closed (filtered) using a firewall. In general terms, an open port is a network port that accepts incoming packets from remote locations.

**MITRE ATT&CK Mappings:** T1203.000


Section #: 2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** A network port is identified by its number, the associated IP address, and the type of the communication protocol such as TCP or UDP.

A listening port is a network port on which an application or process listens on, acting as a communication endpoint.

Each listening port can be open or closed (filtered) using a firewall. In general terms, an open port is a network port that accepts incoming packets from remote locations.

**MITRE ATT&CK Mappings:** T1210


Section #: 2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** A network port is identified by its number, the associated IP address, and the type of the communication protocol such as TCP or UDP.

A listening port is a network port on which an application or process listens on, acting as a communication endpoint.

Each listening port can be open or closed (filtered) using a firewall. In general terms, an open port is a network port that accepts incoming packets from remote locations.

**MITRE ATT&CK Mappings:** T1210.000


Section #: 2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** A network port is identified by its number, the associated IP address, and the type of the communication protocol such as TCP or UDP.

A listening port is a network port on which an application or process listens on, acting as a communication endpoint.

Each listening port can be open or closed (filtered) using a firewall. In general terms, an open port is a network port that accepts incoming packets from remote locations.

**MITRE ATT&CK Mappings:** T1543


Section #: 2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** A network port is identified by its number, the associated IP address, and the type of the communication protocol such as TCP or UDP.

A listening port is a network port on which an application or process listens on, acting as a communication endpoint.

Each listening port can be open or closed (filtered) using a firewall. In general terms, an open port is a network port that accepts incoming packets from remote locations.

**MITRE ATT&CK Mappings:** T1543.002


Section #: 2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** A network port is identified by its number, the associated IP address, and the type of the communication protocol such as TCP or UDP.

A listening port is a network port on which an application or process listens on, acting as a communication endpoint.

Each listening port can be open or closed (filtered) using a firewall. In general terms, an open port is a network port that accepts incoming packets from remote locations.

**MITRE ATT&CK Mappings:** M1042


Section #: 2.1.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.1.1.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** System time should be synchronized between all systems in an environment. This is typically done by establishing an authoritative time server or set of servers and having all systems synchronize their clocks to them.

**Note:**
- **On virtual systems where host based time synchronization is available consult your virtualization software documentation and verify that host based synchronization is in use and follows local site policy. In this scenario, this section should be skipped**
- Only **one** time synchronization method should be in use on the system. Configuring multiple time synchronization methods could lead to unexpected or unreliable results

**MITRE ATT&CK Mappings:** TA0005


Section #: 2.1.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.1.1.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** System time should be synchronized between all systems in an environment. This is typically done by establishing an authoritative time server or set of servers and having all systems synchronize their clocks to them.

**Note:**
- **On virtual systems where host based time synchronization is available consult your virtualization software documentation and verify that host based synchronization is in use and follows local site policy. In this scenario, this section should be skipped**
- Only **one** time synchronization method should be in use on the system. Configuring multiple time synchronization methods could lead to unexpected or unreliable results

**MITRE ATT&CK Mappings:** T1070


Section #: 2.1.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.1.1.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** System time should be synchronized between all systems in an environment. This is typically done by establishing an authoritative time server or set of servers and having all systems synchronize their clocks to them.

**Note:**
- **On virtual systems where host based time synchronization is available consult your virtualization software documentation and verify that host based synchronization is in use and follows local site policy. In this scenario, this section should be skipped**
- Only **one** time synchronization method should be in use on the system. Configuring multiple time synchronization methods could lead to unexpected or unreliable results

**MITRE ATT&CK Mappings:** T1070.002


Section #: 2.1.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.1.1.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** System time should be synchronized between all systems in an environment. This is typically done by establishing an authoritative time server or set of servers and having all systems synchronize their clocks to them.

**Note:**
- **On virtual systems where host based time synchronization is available consult your virtualization software documentation and verify that host based synchronization is in use and follows local site policy. In this scenario, this section should be skipped**
- Only **one** time synchronization method should be in use on the system. Configuring multiple time synchronization methods could lead to unexpected or unreliable results

**MITRE ATT&CK Mappings:** T1562


Section #: 2.1.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.1.1.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** System time should be synchronized between all systems in an environment. This is typically done by establishing an authoritative time server or set of servers and having all systems synchronize their clocks to them.

**Note:**
- **On virtual systems where host based time synchronization is available consult your virtualization software documentation and verify that host based synchronization is in use and follows local site policy. In this scenario, this section should be skipped**
- Only **one** time synchronization method should be in use on the system. Configuring multiple time synchronization methods could lead to unexpected or unreliable results

**MITRE ATT&CK Mappings:** T1562.001


Section #: 2.1.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.1.2.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** '- server
 - The server directive specifies an NTP server which can be used as a time source. The client-server relationship is strictly hierarchical: a client might synchronize its system time to that of the server, but the server’s system time will never be influenced by that of a client.
 - This directive can be used multiple times to specify multiple servers.
 - The directive is immediately followed by either the name of the server, or its IP address.

- pool
 - The syntax of this directive is similar to that for the server directive, except that it is used to specify a pool of NTP servers rather than a single NTP server. The pool name is expected to resolve to multiple addresses which might change over time.
 - This directive can be used multiple times to specify multiple pools.
 - All options valid in the server directive can be used in this directive too.

**MITRE ATT&CK Mappings:** TA0002


Section #: 2.1.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.1.2.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** '- server
 - The server directive specifies an NTP server which can be used as a time source. The client-server relationship is strictly hierarchical: a client might synchronize its system time to that of the server, but the server’s system time will never be influenced by that of a client.
 - This directive can be used multiple times to specify multiple servers.
 - The directive is immediately followed by either the name of the server, or its IP address.

- pool
 - The syntax of this directive is similar to that for the server directive, except that it is used to specify a pool of NTP servers rather than a single NTP server. The pool name is expected to resolve to multiple addresses which might change over time.
 - This directive can be used multiple times to specify multiple pools.
 - All options valid in the server directive can be used in this directive too.

**MITRE ATT&CK Mappings:** T1070


Section #: 2.1.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.1.2.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** '- server
 - The server directive specifies an NTP server which can be used as a time source. The client-server relationship is strictly hierarchical: a client might synchronize its system time to that of the server, but the server’s system time will never be influenced by that of a client.
 - This directive can be used multiple times to specify multiple servers.
 - The directive is immediately followed by either the name of the server, or its IP address.

- pool
 - The syntax of this directive is similar to that for the server directive, except that it is used to specify a pool of NTP servers rather than a single NTP server. The pool name is expected to resolve to multiple addresses which might change over time.
 - This directive can be used multiple times to specify multiple pools.
 - All options valid in the server directive can be used in this directive too.

**MITRE ATT&CK Mappings:** T1070.002


Section #: 2.1.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.1.2.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** '- server
 - The server directive specifies an NTP server which can be used as a time source. The client-server relationship is strictly hierarchical: a client might synchronize its system time to that of the server, but the server’s system time will never be influenced by that of a client.
 - This directive can be used multiple times to specify multiple servers.
 - The directive is immediately followed by either the name of the server, or its IP address.

- pool
 - The syntax of this directive is similar to that for the server directive, except that it is used to specify a pool of NTP servers rather than a single NTP server. The pool name is expected to resolve to multiple addresses which might change over time.
 - This directive can be used multiple times to specify multiple pools.
 - All options valid in the server directive can be used in this directive too.

**MITRE ATT&CK Mappings:** T1562


Section #: 2.1.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.1.2.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** '- server
 - The server directive specifies an NTP server which can be used as a time source. The client-server relationship is strictly hierarchical: a client might synchronize its system time to that of the server, but the server’s system time will never be influenced by that of a client.
 - This directive can be used multiple times to specify multiple servers.
 - The directive is immediately followed by either the name of the server, or its IP address.

- pool
 - The syntax of this directive is similar to that for the server directive, except that it is used to specify a pool of NTP servers rather than a single NTP server. The pool name is expected to resolve to multiple addresses which might change over time.
 - This directive can be used multiple times to specify multiple pools.
 - All options valid in the server directive can be used in this directive too.

**MITRE ATT&CK Mappings:** T1562.001


Section #: 2.1.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.1.2.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** '- server
 - The server directive specifies an NTP server which can be used as a time source. The client-server relationship is strictly hierarchical: a client might synchronize its system time to that of the server, but the server’s system time will never be influenced by that of a client.
 - This directive can be used multiple times to specify multiple servers.
 - The directive is immediately followed by either the name of the server, or its IP address.

- pool
 - The syntax of this directive is similar to that for the server directive, except that it is used to specify a pool of NTP servers rather than a single NTP server. The pool name is expected to resolve to multiple addresses which might change over time.
 - This directive can be used multiple times to specify multiple pools.
 - All options valid in the server directive can be used in this directive too.

**MITRE ATT&CK Mappings:** M1022


Section #: 2.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.1.3.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** `NTP=`
- A space-separated list of NTP server host names or IP addresses. During runtime this list is combined with any per-interface NTP servers acquired from systemd-networkd.service(8). systemd-timesyncd will contact all configured system or per-interface servers in turn, until one responds. When the empty string is assigned, the list of NTP servers is reset, and all prior assignments will have no effect. This setting defaults to an empty list.

`FallbackNTP=`
- A space-separated list of NTP server host names or IP addresses to be used as the fallback NTP servers. Any per-interface NTP servers obtained from systemd-networkd.service(8) take precedence over this setting, as do any servers set via NTP= above. This setting is hence only relevant if no other NTP server information is known. When the empty string is assigned, the list of NTP servers is reset, and all prior assignments will have no effect. If this option is not given, a compiled-in list of NTP servers is used.

**MITRE ATT&CK Mappings:** TA0002


Section #: 2.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.1.3.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** `NTP=`
- A space-separated list of NTP server host names or IP addresses. During runtime this list is combined with any per-interface NTP servers acquired from systemd-networkd.service(8). systemd-timesyncd will contact all configured system or per-interface servers in turn, until one responds. When the empty string is assigned, the list of NTP servers is reset, and all prior assignments will have no effect. This setting defaults to an empty list.

`FallbackNTP=`
- A space-separated list of NTP server host names or IP addresses to be used as the fallback NTP servers. Any per-interface NTP servers obtained from systemd-networkd.service(8) take precedence over this setting, as do any servers set via NTP= above. This setting is hence only relevant if no other NTP server information is known. When the empty string is assigned, the list of NTP servers is reset, and all prior assignments will have no effect. If this option is not given, a compiled-in list of NTP servers is used.

**MITRE ATT&CK Mappings:** T1070


Section #: 2.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.1.3.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** `NTP=`
- A space-separated list of NTP server host names or IP addresses. During runtime this list is combined with any per-interface NTP servers acquired from systemd-networkd.service(8). systemd-timesyncd will contact all configured system or per-interface servers in turn, until one responds. When the empty string is assigned, the list of NTP servers is reset, and all prior assignments will have no effect. This setting defaults to an empty list.

`FallbackNTP=`
- A space-separated list of NTP server host names or IP addresses to be used as the fallback NTP servers. Any per-interface NTP servers obtained from systemd-networkd.service(8) take precedence over this setting, as do any servers set via NTP= above. This setting is hence only relevant if no other NTP server information is known. When the empty string is assigned, the list of NTP servers is reset, and all prior assignments will have no effect. If this option is not given, a compiled-in list of NTP servers is used.

**MITRE ATT&CK Mappings:** T1070.002


Section #: 2.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.1.3.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** `NTP=`
- A space-separated list of NTP server host names or IP addresses. During runtime this list is combined with any per-interface NTP servers acquired from systemd-networkd.service(8). systemd-timesyncd will contact all configured system or per-interface servers in turn, until one responds. When the empty string is assigned, the list of NTP servers is reset, and all prior assignments will have no effect. This setting defaults to an empty list.

`FallbackNTP=`
- A space-separated list of NTP server host names or IP addresses to be used as the fallback NTP servers. Any per-interface NTP servers obtained from systemd-networkd.service(8) take precedence over this setting, as do any servers set via NTP= above. This setting is hence only relevant if no other NTP server information is known. When the empty string is assigned, the list of NTP servers is reset, and all prior assignments will have no effect. If this option is not given, a compiled-in list of NTP servers is used.

**MITRE ATT&CK Mappings:** T1562


Section #: 2.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.1.3.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** `NTP=`
- A space-separated list of NTP server host names or IP addresses. During runtime this list is combined with any per-interface NTP servers acquired from systemd-networkd.service(8). systemd-timesyncd will contact all configured system or per-interface servers in turn, until one responds. When the empty string is assigned, the list of NTP servers is reset, and all prior assignments will have no effect. This setting defaults to an empty list.

`FallbackNTP=`
- A space-separated list of NTP server host names or IP addresses to be used as the fallback NTP servers. Any per-interface NTP servers obtained from systemd-networkd.service(8) take precedence over this setting, as do any servers set via NTP= above. This setting is hence only relevant if no other NTP server information is known. When the empty string is assigned, the list of NTP servers is reset, and all prior assignments will have no effect. If this option is not given, a compiled-in list of NTP servers is used.

**MITRE ATT&CK Mappings:** T1562.001


Section #: 2.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.1.3.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** `NTP=`
- A space-separated list of NTP server host names or IP addresses. During runtime this list is combined with any per-interface NTP servers acquired from systemd-networkd.service(8). systemd-timesyncd will contact all configured system or per-interface servers in turn, until one responds. When the empty string is assigned, the list of NTP servers is reset, and all prior assignments will have no effect. This setting defaults to an empty list.

`FallbackNTP=`
- A space-separated list of NTP server host names or IP addresses to be used as the fallback NTP servers. Any per-interface NTP servers obtained from systemd-networkd.service(8) take precedence over this setting, as do any servers set via NTP= above. This setting is hence only relevant if no other NTP server information is known. When the empty string is assigned, the list of NTP servers is reset, and all prior assignments will have no effect. If this option is not given, a compiled-in list of NTP servers is used.

**MITRE ATT&CK Mappings:** M1022


Section #: 2.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.1.4.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The various modes are determined by the command keyword and the type of the required IP address. Addresses are classed by type as (s) a remote server or peer (IPv4 class A, B and C), (b) the broadcast address of a local interface, (m) a multicast address (IPv4 class D), or (r) a reference clock address (127.127.x.x). 

**Note:** That only those options applicable to each command are listed below. Use of options not listed may not be caught as an error, but may result in some weird and even destructive behavior.

If the Basic Socket Interface Extensions for IPv6 (RFC-2553) is detected, support for the IPv6 address family is generated in addition to the default support of the IPv4 address family. In a few cases, including the reslist billboard generated by `ntpq` or `ntpdc`, IPv6 addresses are automatically generated. IPv6 addresses can be identified by the presence of colons “:” in the address field. IPv6 addresses can be used almost everywhere where IPv4 addresses can be used, with the exception of reference clock addresses, which are always IPv4.

**Note:** In contexts where a host name is expected, a -4 qualifier preceding the host name forces DNS resolution to the IPv4 namespace, while a -6 qualifier forces DNS resolution to the IPv6 namespace. See IPv6 references for the equivalent classes for that address family.

- pool - For type s addresses, this command mobilizes a persistent client mode association with a number of remote servers. In this mode the local clock can synchronized to the remote server, but the remote server can never be synchronized to the local clock.

- server - For type s and r addresses, this command mobilizes a persistent client mode association with the specified remote server or local radio clock. In this mode the local clock can synchronized to the remote server, but the remote server can never be synchronized to the local clock. This command should not be used for type b or m addresses.

**MITRE ATT&CK Mappings:** TA0002


Section #: 2.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.1.4.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The various modes are determined by the command keyword and the type of the required IP address. Addresses are classed by type as (s) a remote server or peer (IPv4 class A, B and C), (b) the broadcast address of a local interface, (m) a multicast address (IPv4 class D), or (r) a reference clock address (127.127.x.x). 

**Note:** That only those options applicable to each command are listed below. Use of options not listed may not be caught as an error, but may result in some weird and even destructive behavior.

If the Basic Socket Interface Extensions for IPv6 (RFC-2553) is detected, support for the IPv6 address family is generated in addition to the default support of the IPv4 address family. In a few cases, including the reslist billboard generated by `ntpq` or `ntpdc`, IPv6 addresses are automatically generated. IPv6 addresses can be identified by the presence of colons “:” in the address field. IPv6 addresses can be used almost everywhere where IPv4 addresses can be used, with the exception of reference clock addresses, which are always IPv4.

**Note:** In contexts where a host name is expected, a -4 qualifier preceding the host name forces DNS resolution to the IPv4 namespace, while a -6 qualifier forces DNS resolution to the IPv6 namespace. See IPv6 references for the equivalent classes for that address family.

- pool - For type s addresses, this command mobilizes a persistent client mode association with a number of remote servers. In this mode the local clock can synchronized to the remote server, but the remote server can never be synchronized to the local clock.

- server - For type s and r addresses, this command mobilizes a persistent client mode association with the specified remote server or local radio clock. In this mode the local clock can synchronized to the remote server, but the remote server can never be synchronized to the local clock. This command should not be used for type b or m addresses.

**MITRE ATT&CK Mappings:** T1070


Section #: 2.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.1.4.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The various modes are determined by the command keyword and the type of the required IP address. Addresses are classed by type as (s) a remote server or peer (IPv4 class A, B and C), (b) the broadcast address of a local interface, (m) a multicast address (IPv4 class D), or (r) a reference clock address (127.127.x.x). 

**Note:** That only those options applicable to each command are listed below. Use of options not listed may not be caught as an error, but may result in some weird and even destructive behavior.

If the Basic Socket Interface Extensions for IPv6 (RFC-2553) is detected, support for the IPv6 address family is generated in addition to the default support of the IPv4 address family. In a few cases, including the reslist billboard generated by `ntpq` or `ntpdc`, IPv6 addresses are automatically generated. IPv6 addresses can be identified by the presence of colons “:” in the address field. IPv6 addresses can be used almost everywhere where IPv4 addresses can be used, with the exception of reference clock addresses, which are always IPv4.

**Note:** In contexts where a host name is expected, a -4 qualifier preceding the host name forces DNS resolution to the IPv4 namespace, while a -6 qualifier forces DNS resolution to the IPv6 namespace. See IPv6 references for the equivalent classes for that address family.

- pool - For type s addresses, this command mobilizes a persistent client mode association with a number of remote servers. In this mode the local clock can synchronized to the remote server, but the remote server can never be synchronized to the local clock.

- server - For type s and r addresses, this command mobilizes a persistent client mode association with the specified remote server or local radio clock. In this mode the local clock can synchronized to the remote server, but the remote server can never be synchronized to the local clock. This command should not be used for type b or m addresses.

**MITRE ATT&CK Mappings:** T1070.002


Section #: 2.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.1.4.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The various modes are determined by the command keyword and the type of the required IP address. Addresses are classed by type as (s) a remote server or peer (IPv4 class A, B and C), (b) the broadcast address of a local interface, (m) a multicast address (IPv4 class D), or (r) a reference clock address (127.127.x.x). 

**Note:** That only those options applicable to each command are listed below. Use of options not listed may not be caught as an error, but may result in some weird and even destructive behavior.

If the Basic Socket Interface Extensions for IPv6 (RFC-2553) is detected, support for the IPv6 address family is generated in addition to the default support of the IPv4 address family. In a few cases, including the reslist billboard generated by `ntpq` or `ntpdc`, IPv6 addresses are automatically generated. IPv6 addresses can be identified by the presence of colons “:” in the address field. IPv6 addresses can be used almost everywhere where IPv4 addresses can be used, with the exception of reference clock addresses, which are always IPv4.

**Note:** In contexts where a host name is expected, a -4 qualifier preceding the host name forces DNS resolution to the IPv4 namespace, while a -6 qualifier forces DNS resolution to the IPv6 namespace. See IPv6 references for the equivalent classes for that address family.

- pool - For type s addresses, this command mobilizes a persistent client mode association with a number of remote servers. In this mode the local clock can synchronized to the remote server, but the remote server can never be synchronized to the local clock.

- server - For type s and r addresses, this command mobilizes a persistent client mode association with the specified remote server or local radio clock. In this mode the local clock can synchronized to the remote server, but the remote server can never be synchronized to the local clock. This command should not be used for type b or m addresses.

**MITRE ATT&CK Mappings:** T1498


Section #: 2.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.1.4.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The various modes are determined by the command keyword and the type of the required IP address. Addresses are classed by type as (s) a remote server or peer (IPv4 class A, B and C), (b) the broadcast address of a local interface, (m) a multicast address (IPv4 class D), or (r) a reference clock address (127.127.x.x). 

**Note:** That only those options applicable to each command are listed below. Use of options not listed may not be caught as an error, but may result in some weird and even destructive behavior.

If the Basic Socket Interface Extensions for IPv6 (RFC-2553) is detected, support for the IPv6 address family is generated in addition to the default support of the IPv4 address family. In a few cases, including the reslist billboard generated by `ntpq` or `ntpdc`, IPv6 addresses are automatically generated. IPv6 addresses can be identified by the presence of colons “:” in the address field. IPv6 addresses can be used almost everywhere where IPv4 addresses can be used, with the exception of reference clock addresses, which are always IPv4.

**Note:** In contexts where a host name is expected, a -4 qualifier preceding the host name forces DNS resolution to the IPv4 namespace, while a -6 qualifier forces DNS resolution to the IPv6 namespace. See IPv6 references for the equivalent classes for that address family.

- pool - For type s addresses, this command mobilizes a persistent client mode association with a number of remote servers. In this mode the local clock can synchronized to the remote server, but the remote server can never be synchronized to the local clock.

- server - For type s and r addresses, this command mobilizes a persistent client mode association with the specified remote server or local radio clock. In this mode the local clock can synchronized to the remote server, but the remote server can never be synchronized to the local clock. This command should not be used for type b or m addresses.

**MITRE ATT&CK Mappings:** T1498.002


Section #: 2.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.1.4.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The various modes are determined by the command keyword and the type of the required IP address. Addresses are classed by type as (s) a remote server or peer (IPv4 class A, B and C), (b) the broadcast address of a local interface, (m) a multicast address (IPv4 class D), or (r) a reference clock address (127.127.x.x). 

**Note:** That only those options applicable to each command are listed below. Use of options not listed may not be caught as an error, but may result in some weird and even destructive behavior.

If the Basic Socket Interface Extensions for IPv6 (RFC-2553) is detected, support for the IPv6 address family is generated in addition to the default support of the IPv4 address family. In a few cases, including the reslist billboard generated by `ntpq` or `ntpdc`, IPv6 addresses are automatically generated. IPv6 addresses can be identified by the presence of colons “:” in the address field. IPv6 addresses can be used almost everywhere where IPv4 addresses can be used, with the exception of reference clock addresses, which are always IPv4.

**Note:** In contexts where a host name is expected, a -4 qualifier preceding the host name forces DNS resolution to the IPv4 namespace, while a -6 qualifier forces DNS resolution to the IPv6 namespace. See IPv6 references for the equivalent classes for that address family.

- pool - For type s addresses, this command mobilizes a persistent client mode association with a number of remote servers. In this mode the local clock can synchronized to the remote server, but the remote server can never be synchronized to the local clock.

- server - For type s and r addresses, this command mobilizes a persistent client mode association with the specified remote server or local radio clock. In this mode the local clock can synchronized to the remote server, but the remote server can never be synchronized to the local clock. This command should not be used for type b or m addresses.

**MITRE ATT&CK Mappings:** T1562


Section #: 2.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.1.4.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The various modes are determined by the command keyword and the type of the required IP address. Addresses are classed by type as (s) a remote server or peer (IPv4 class A, B and C), (b) the broadcast address of a local interface, (m) a multicast address (IPv4 class D), or (r) a reference clock address (127.127.x.x). 

**Note:** That only those options applicable to each command are listed below. Use of options not listed may not be caught as an error, but may result in some weird and even destructive behavior.

If the Basic Socket Interface Extensions for IPv6 (RFC-2553) is detected, support for the IPv6 address family is generated in addition to the default support of the IPv4 address family. In a few cases, including the reslist billboard generated by `ntpq` or `ntpdc`, IPv6 addresses are automatically generated. IPv6 addresses can be identified by the presence of colons “:” in the address field. IPv6 addresses can be used almost everywhere where IPv4 addresses can be used, with the exception of reference clock addresses, which are always IPv4.

**Note:** In contexts where a host name is expected, a -4 qualifier preceding the host name forces DNS resolution to the IPv4 namespace, while a -6 qualifier forces DNS resolution to the IPv6 namespace. See IPv6 references for the equivalent classes for that address family.

- pool - For type s addresses, this command mobilizes a persistent client mode association with a number of remote servers. In this mode the local clock can synchronized to the remote server, but the remote server can never be synchronized to the local clock.

- server - For type s and r addresses, this command mobilizes a persistent client mode association with the specified remote server or local radio clock. In this mode the local clock can synchronized to the remote server, but the remote server can never be synchronized to the local clock. This command should not be used for type b or m addresses.

**MITRE ATT&CK Mappings:** T1562.001


Section #: 2.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.1.4.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The various modes are determined by the command keyword and the type of the required IP address. Addresses are classed by type as (s) a remote server or peer (IPv4 class A, B and C), (b) the broadcast address of a local interface, (m) a multicast address (IPv4 class D), or (r) a reference clock address (127.127.x.x). 

**Note:** That only those options applicable to each command are listed below. Use of options not listed may not be caught as an error, but may result in some weird and even destructive behavior.

If the Basic Socket Interface Extensions for IPv6 (RFC-2553) is detected, support for the IPv6 address family is generated in addition to the default support of the IPv4 address family. In a few cases, including the reslist billboard generated by `ntpq` or `ntpdc`, IPv6 addresses are automatically generated. IPv6 addresses can be identified by the presence of colons “:” in the address field. IPv6 addresses can be used almost everywhere where IPv4 addresses can be used, with the exception of reference clock addresses, which are always IPv4.

**Note:** In contexts where a host name is expected, a -4 qualifier preceding the host name forces DNS resolution to the IPv4 namespace, while a -6 qualifier forces DNS resolution to the IPv6 namespace. See IPv6 references for the equivalent classes for that address family.

- pool - For type s addresses, this command mobilizes a persistent client mode association with a number of remote servers. In this mode the local clock can synchronized to the remote server, but the remote server can never be synchronized to the local clock.

- server - For type s and r addresses, this command mobilizes a persistent client mode association with the specified remote server or local radio clock. In this mode the local clock can synchronized to the remote server, but the remote server can never be synchronized to the local clock. This command should not be used for type b or m addresses.

**MITRE ATT&CK Mappings:** M1022


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.1

**Profiles:** Level 1 - Server

**Description:** The X Window System provides a Graphical User Interface (GUI) where users can have multiple windows in which to run programs and various add on. The X Windows system is typically used on workstations where users login, but not on servers where users typically do not login.

**MITRE ATT&CK Mappings:** TA0008


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.1

**Profiles:** Level 1 - Server

**Description:** The X Window System provides a Graphical User Interface (GUI) where users can have multiple windows in which to run programs and various add on. The X Windows system is typically used on workstations where users login, but not on servers where users typically do not login.

**MITRE ATT&CK Mappings:** T1203


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.1

**Profiles:** Level 1 - Server

**Description:** The X Window System provides a Graphical User Interface (GUI) where users can have multiple windows in which to run programs and various add on. The X Windows system is typically used on workstations where users login, but not on servers where users typically do not login.

**MITRE ATT&CK Mappings:** T1203.000


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.1

**Profiles:** Level 1 - Server

**Description:** The X Window System provides a Graphical User Interface (GUI) where users can have multiple windows in which to run programs and various add on. The X Windows system is typically used on workstations where users login, but not on servers where users typically do not login.

**MITRE ATT&CK Mappings:** T1210


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.1

**Profiles:** Level 1 - Server

**Description:** The X Window System provides a Graphical User Interface (GUI) where users can have multiple windows in which to run programs and various add on. The X Windows system is typically used on workstations where users login, but not on servers where users typically do not login.

**MITRE ATT&CK Mappings:** T1210.000


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.1

**Profiles:** Level 1 - Server

**Description:** The X Window System provides a Graphical User Interface (GUI) where users can have multiple windows in which to run programs and various add on. The X Windows system is typically used on workstations where users login, but not on servers where users typically do not login.

**MITRE ATT&CK Mappings:** T1543


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.1

**Profiles:** Level 1 - Server

**Description:** The X Window System provides a Graphical User Interface (GUI) where users can have multiple windows in which to run programs and various add on. The X Windows system is typically used on workstations where users login, but not on servers where users typically do not login.

**MITRE ATT&CK Mappings:** T1543.002


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.1

**Profiles:** Level 1 - Server

**Description:** The X Window System provides a Graphical User Interface (GUI) where users can have multiple windows in which to run programs and various add on. The X Windows system is typically used on workstations where users login, but not on servers where users typically do not login.

**MITRE ATT&CK Mappings:** M1042


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Avahi is a free zeroconf implementation, including a system for multicast DNS/DNS-SD service discovery. Avahi allows programs to publish and discover services and hosts running on a local network with no specific configuration. For example, a user can plug a computer into a network and Avahi automatically finds printers to print to, files to look at and people to talk to, as well as network services running on the machine.

**MITRE ATT&CK Mappings:** TA0008


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Avahi is a free zeroconf implementation, including a system for multicast DNS/DNS-SD service discovery. Avahi allows programs to publish and discover services and hosts running on a local network with no specific configuration. For example, a user can plug a computer into a network and Avahi automatically finds printers to print to, files to look at and people to talk to, as well as network services running on the machine.

**MITRE ATT&CK Mappings:** T1203


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Avahi is a free zeroconf implementation, including a system for multicast DNS/DNS-SD service discovery. Avahi allows programs to publish and discover services and hosts running on a local network with no specific configuration. For example, a user can plug a computer into a network and Avahi automatically finds printers to print to, files to look at and people to talk to, as well as network services running on the machine.

**MITRE ATT&CK Mappings:** T1203.000


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Avahi is a free zeroconf implementation, including a system for multicast DNS/DNS-SD service discovery. Avahi allows programs to publish and discover services and hosts running on a local network with no specific configuration. For example, a user can plug a computer into a network and Avahi automatically finds printers to print to, files to look at and people to talk to, as well as network services running on the machine.

**MITRE ATT&CK Mappings:** T1210


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Avahi is a free zeroconf implementation, including a system for multicast DNS/DNS-SD service discovery. Avahi allows programs to publish and discover services and hosts running on a local network with no specific configuration. For example, a user can plug a computer into a network and Avahi automatically finds printers to print to, files to look at and people to talk to, as well as network services running on the machine.

**MITRE ATT&CK Mappings:** T1210.000


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Avahi is a free zeroconf implementation, including a system for multicast DNS/DNS-SD service discovery. Avahi allows programs to publish and discover services and hosts running on a local network with no specific configuration. For example, a user can plug a computer into a network and Avahi automatically finds printers to print to, files to look at and people to talk to, as well as network services running on the machine.

**MITRE ATT&CK Mappings:** T1543


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Avahi is a free zeroconf implementation, including a system for multicast DNS/DNS-SD service discovery. Avahi allows programs to publish and discover services and hosts running on a local network with no specific configuration. For example, a user can plug a computer into a network and Avahi automatically finds printers to print to, files to look at and people to talk to, as well as network services running on the machine.

**MITRE ATT&CK Mappings:** T1543.002


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Avahi is a free zeroconf implementation, including a system for multicast DNS/DNS-SD service discovery. Avahi allows programs to publish and discover services and hosts running on a local network with no specific configuration. For example, a user can plug a computer into a network and Avahi automatically finds printers to print to, files to look at and people to talk to, as well as network services running on the machine.

**MITRE ATT&CK Mappings:** M1042


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.3

**Profiles:** Level 1 - Server, Level 2 - Workstation

**Description:** The Common Unix Print System (CUPS) provides the ability to print to both local and network printers. A system running CUPS can also accept print jobs from remote systems and print them to local printers. It also provides a web based remote administration capability.

**MITRE ATT&CK Mappings:** TA0008


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.3

**Profiles:** Level 1 - Server, Level 2 - Workstation

**Description:** The Common Unix Print System (CUPS) provides the ability to print to both local and network printers. A system running CUPS can also accept print jobs from remote systems and print them to local printers. It also provides a web based remote administration capability.

**MITRE ATT&CK Mappings:** T1203


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.3

**Profiles:** Level 1 - Server, Level 2 - Workstation

**Description:** The Common Unix Print System (CUPS) provides the ability to print to both local and network printers. A system running CUPS can also accept print jobs from remote systems and print them to local printers. It also provides a web based remote administration capability.

**MITRE ATT&CK Mappings:** T1203.000


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.3

**Profiles:** Level 1 - Server, Level 2 - Workstation

**Description:** The Common Unix Print System (CUPS) provides the ability to print to both local and network printers. A system running CUPS can also accept print jobs from remote systems and print them to local printers. It also provides a web based remote administration capability.

**MITRE ATT&CK Mappings:** T1210


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.3

**Profiles:** Level 1 - Server, Level 2 - Workstation

**Description:** The Common Unix Print System (CUPS) provides the ability to print to both local and network printers. A system running CUPS can also accept print jobs from remote systems and print them to local printers. It also provides a web based remote administration capability.

**MITRE ATT&CK Mappings:** T1210.000


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.3

**Profiles:** Level 1 - Server, Level 2 - Workstation

**Description:** The Common Unix Print System (CUPS) provides the ability to print to both local and network printers. A system running CUPS can also accept print jobs from remote systems and print them to local printers. It also provides a web based remote administration capability.

**MITRE ATT&CK Mappings:** T1543


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.3

**Profiles:** Level 1 - Server, Level 2 - Workstation

**Description:** The Common Unix Print System (CUPS) provides the ability to print to both local and network printers. A system running CUPS can also accept print jobs from remote systems and print them to local printers. It also provides a web based remote administration capability.

**MITRE ATT&CK Mappings:** T1543.002


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.3

**Profiles:** Level 1 - Server, Level 2 - Workstation

**Description:** The Common Unix Print System (CUPS) provides the ability to print to both local and network printers. A system running CUPS can also accept print jobs from remote systems and print them to local printers. It also provides a web based remote administration capability.

**MITRE ATT&CK Mappings:** M1042


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The Dynamic Host Configuration Protocol (DHCP) is a service that allows machines to be dynamically assigned IP addresses.

**MITRE ATT&CK Mappings:** TA0008


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The Dynamic Host Configuration Protocol (DHCP) is a service that allows machines to be dynamically assigned IP addresses.

**MITRE ATT&CK Mappings:** T1203


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The Dynamic Host Configuration Protocol (DHCP) is a service that allows machines to be dynamically assigned IP addresses.

**MITRE ATT&CK Mappings:** T1203.000


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The Dynamic Host Configuration Protocol (DHCP) is a service that allows machines to be dynamically assigned IP addresses.

**MITRE ATT&CK Mappings:** T1210


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The Dynamic Host Configuration Protocol (DHCP) is a service that allows machines to be dynamically assigned IP addresses.

**MITRE ATT&CK Mappings:** T1210.000


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The Dynamic Host Configuration Protocol (DHCP) is a service that allows machines to be dynamically assigned IP addresses.

**MITRE ATT&CK Mappings:** T1543


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The Dynamic Host Configuration Protocol (DHCP) is a service that allows machines to be dynamically assigned IP addresses.

**MITRE ATT&CK Mappings:** T1543.002


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The Dynamic Host Configuration Protocol (DHCP) is a service that allows machines to be dynamically assigned IP addresses.

**MITRE ATT&CK Mappings:** M1042


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.5

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The Lightweight Directory Access Protocol (LDAP) was introduced as a replacement for NIS/YP. It is a service that provides a method for looking up information from a central database.

**MITRE ATT&CK Mappings:** TA0008


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.5

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The Lightweight Directory Access Protocol (LDAP) was introduced as a replacement for NIS/YP. It is a service that provides a method for looking up information from a central database.

**MITRE ATT&CK Mappings:** T1203


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.5

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The Lightweight Directory Access Protocol (LDAP) was introduced as a replacement for NIS/YP. It is a service that provides a method for looking up information from a central database.

**MITRE ATT&CK Mappings:** T1203.000


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.5

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The Lightweight Directory Access Protocol (LDAP) was introduced as a replacement for NIS/YP. It is a service that provides a method for looking up information from a central database.

**MITRE ATT&CK Mappings:** T1210


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.5

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The Lightweight Directory Access Protocol (LDAP) was introduced as a replacement for NIS/YP. It is a service that provides a method for looking up information from a central database.

**MITRE ATT&CK Mappings:** T1210.000


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.5

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The Lightweight Directory Access Protocol (LDAP) was introduced as a replacement for NIS/YP. It is a service that provides a method for looking up information from a central database.

**MITRE ATT&CK Mappings:** T1543


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.5

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The Lightweight Directory Access Protocol (LDAP) was introduced as a replacement for NIS/YP. It is a service that provides a method for looking up information from a central database.

**MITRE ATT&CK Mappings:** T1543.002


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.5

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The Lightweight Directory Access Protocol (LDAP) was introduced as a replacement for NIS/YP. It is a service that provides a method for looking up information from a central database.

**MITRE ATT&CK Mappings:** M1042


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.7

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The Domain Name System (DNS) is a hierarchical naming system that maps names to IP addresses for computers, services and other resources connected to a network.

**MITRE ATT&CK Mappings:** TA0008


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.7

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The Domain Name System (DNS) is a hierarchical naming system that maps names to IP addresses for computers, services and other resources connected to a network.

**MITRE ATT&CK Mappings:** T1203


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.7

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The Domain Name System (DNS) is a hierarchical naming system that maps names to IP addresses for computers, services and other resources connected to a network.

**MITRE ATT&CK Mappings:** T1203.000


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.7

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The Domain Name System (DNS) is a hierarchical naming system that maps names to IP addresses for computers, services and other resources connected to a network.

**MITRE ATT&CK Mappings:** T1210


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.7

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The Domain Name System (DNS) is a hierarchical naming system that maps names to IP addresses for computers, services and other resources connected to a network.

**MITRE ATT&CK Mappings:** T1210.000


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.7

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The Domain Name System (DNS) is a hierarchical naming system that maps names to IP addresses for computers, services and other resources connected to a network.

**MITRE ATT&CK Mappings:** T1543


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.7

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The Domain Name System (DNS) is a hierarchical naming system that maps names to IP addresses for computers, services and other resources connected to a network.

**MITRE ATT&CK Mappings:** T1543.002


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.7

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The Domain Name System (DNS) is a hierarchical naming system that maps names to IP addresses for computers, services and other resources connected to a network.

**MITRE ATT&CK Mappings:** M1042


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.8

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The File Transfer Protocol (FTP) provides networked computers with the ability to transfer files.

**MITRE ATT&CK Mappings:** TA0008


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.8

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The File Transfer Protocol (FTP) provides networked computers with the ability to transfer files.

**MITRE ATT&CK Mappings:** T1203


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.8

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The File Transfer Protocol (FTP) provides networked computers with the ability to transfer files.

**MITRE ATT&CK Mappings:** T1203.000


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.8

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The File Transfer Protocol (FTP) provides networked computers with the ability to transfer files.

**MITRE ATT&CK Mappings:** T1210


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.8

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The File Transfer Protocol (FTP) provides networked computers with the ability to transfer files.

**MITRE ATT&CK Mappings:** T1210.000


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.8

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The File Transfer Protocol (FTP) provides networked computers with the ability to transfer files.

**MITRE ATT&CK Mappings:** T1543


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.8

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The File Transfer Protocol (FTP) provides networked computers with the ability to transfer files.

**MITRE ATT&CK Mappings:** T1543.002


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.8

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The File Transfer Protocol (FTP) provides networked computers with the ability to transfer files.

**MITRE ATT&CK Mappings:** M1042


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.9

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** HTTP or web servers provide the ability to host web site content.

**MITRE ATT&CK Mappings:** TA0008


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.9

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** HTTP or web servers provide the ability to host web site content.

**MITRE ATT&CK Mappings:** T1203


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.9

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** HTTP or web servers provide the ability to host web site content.

**MITRE ATT&CK Mappings:** T1203.000


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.9

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** HTTP or web servers provide the ability to host web site content.

**MITRE ATT&CK Mappings:** T1210


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.9

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** HTTP or web servers provide the ability to host web site content.

**MITRE ATT&CK Mappings:** T1210.000


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.9

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** HTTP or web servers provide the ability to host web site content.

**MITRE ATT&CK Mappings:** T1543


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.9

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** HTTP or web servers provide the ability to host web site content.

**MITRE ATT&CK Mappings:** T1543.002


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.9

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** HTTP or web servers provide the ability to host web site content.

**MITRE ATT&CK Mappings:** M1042


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.10

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** `dovecot-imapd` and `dovecot-pop3d` are an open source IMAP and POP3 server for Linux based systems.

**MITRE ATT&CK Mappings:** TA0008


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.10

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** `dovecot-imapd` and `dovecot-pop3d` are an open source IMAP and POP3 server for Linux based systems.

**MITRE ATT&CK Mappings:** T1203


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.10

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** `dovecot-imapd` and `dovecot-pop3d` are an open source IMAP and POP3 server for Linux based systems.

**MITRE ATT&CK Mappings:** T1203.000


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.10

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** `dovecot-imapd` and `dovecot-pop3d` are an open source IMAP and POP3 server for Linux based systems.

**MITRE ATT&CK Mappings:** T1210


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.10

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** `dovecot-imapd` and `dovecot-pop3d` are an open source IMAP and POP3 server for Linux based systems.

**MITRE ATT&CK Mappings:** T1210.000


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.10

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** `dovecot-imapd` and `dovecot-pop3d` are an open source IMAP and POP3 server for Linux based systems.

**MITRE ATT&CK Mappings:** T1543


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.10

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** `dovecot-imapd` and `dovecot-pop3d` are an open source IMAP and POP3 server for Linux based systems.

**MITRE ATT&CK Mappings:** T1543.002


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.10

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** `dovecot-imapd` and `dovecot-pop3d` are an open source IMAP and POP3 server for Linux based systems.

**MITRE ATT&CK Mappings:** M1042


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.11

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The Samba daemon allows system administrators to configure their Linux systems to share file systems and directories with Windows desktops. Samba will advertise the file systems and directories via the Server Message Block (SMB) protocol. Windows desktop users will be able to mount these directories and file systems as letter drives on their systems.

**MITRE ATT&CK Mappings:** TA0008


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.11

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The Samba daemon allows system administrators to configure their Linux systems to share file systems and directories with Windows desktops. Samba will advertise the file systems and directories via the Server Message Block (SMB) protocol. Windows desktop users will be able to mount these directories and file systems as letter drives on their systems.

**MITRE ATT&CK Mappings:** T1005


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.11

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The Samba daemon allows system administrators to configure their Linux systems to share file systems and directories with Windows desktops. Samba will advertise the file systems and directories via the Server Message Block (SMB) protocol. Windows desktop users will be able to mount these directories and file systems as letter drives on their systems.

**MITRE ATT&CK Mappings:** T1005.000


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.11

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The Samba daemon allows system administrators to configure their Linux systems to share file systems and directories with Windows desktops. Samba will advertise the file systems and directories via the Server Message Block (SMB) protocol. Windows desktop users will be able to mount these directories and file systems as letter drives on their systems.

**MITRE ATT&CK Mappings:** T1039


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.11

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The Samba daemon allows system administrators to configure their Linux systems to share file systems and directories with Windows desktops. Samba will advertise the file systems and directories via the Server Message Block (SMB) protocol. Windows desktop users will be able to mount these directories and file systems as letter drives on their systems.

**MITRE ATT&CK Mappings:** T1039.000


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.11

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The Samba daemon allows system administrators to configure their Linux systems to share file systems and directories with Windows desktops. Samba will advertise the file systems and directories via the Server Message Block (SMB) protocol. Windows desktop users will be able to mount these directories and file systems as letter drives on their systems.

**MITRE ATT&CK Mappings:** T1083


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.11

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The Samba daemon allows system administrators to configure their Linux systems to share file systems and directories with Windows desktops. Samba will advertise the file systems and directories via the Server Message Block (SMB) protocol. Windows desktop users will be able to mount these directories and file systems as letter drives on their systems.

**MITRE ATT&CK Mappings:** T1083.000


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.11

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The Samba daemon allows system administrators to configure their Linux systems to share file systems and directories with Windows desktops. Samba will advertise the file systems and directories via the Server Message Block (SMB) protocol. Windows desktop users will be able to mount these directories and file systems as letter drives on their systems.

**MITRE ATT&CK Mappings:** T1135


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.11

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The Samba daemon allows system administrators to configure their Linux systems to share file systems and directories with Windows desktops. Samba will advertise the file systems and directories via the Server Message Block (SMB) protocol. Windows desktop users will be able to mount these directories and file systems as letter drives on their systems.

**MITRE ATT&CK Mappings:** T1135.000


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.11

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The Samba daemon allows system administrators to configure their Linux systems to share file systems and directories with Windows desktops. Samba will advertise the file systems and directories via the Server Message Block (SMB) protocol. Windows desktop users will be able to mount these directories and file systems as letter drives on their systems.

**MITRE ATT&CK Mappings:** T1203


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.11

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The Samba daemon allows system administrators to configure their Linux systems to share file systems and directories with Windows desktops. Samba will advertise the file systems and directories via the Server Message Block (SMB) protocol. Windows desktop users will be able to mount these directories and file systems as letter drives on their systems.

**MITRE ATT&CK Mappings:** T1203.000


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.11

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The Samba daemon allows system administrators to configure their Linux systems to share file systems and directories with Windows desktops. Samba will advertise the file systems and directories via the Server Message Block (SMB) protocol. Windows desktop users will be able to mount these directories and file systems as letter drives on their systems.

**MITRE ATT&CK Mappings:** T1210


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.11

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The Samba daemon allows system administrators to configure their Linux systems to share file systems and directories with Windows desktops. Samba will advertise the file systems and directories via the Server Message Block (SMB) protocol. Windows desktop users will be able to mount these directories and file systems as letter drives on their systems.

**MITRE ATT&CK Mappings:** T1210.000


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.11

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The Samba daemon allows system administrators to configure their Linux systems to share file systems and directories with Windows desktops. Samba will advertise the file systems and directories via the Server Message Block (SMB) protocol. Windows desktop users will be able to mount these directories and file systems as letter drives on their systems.

**MITRE ATT&CK Mappings:** T1543


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.11

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The Samba daemon allows system administrators to configure their Linux systems to share file systems and directories with Windows desktops. Samba will advertise the file systems and directories via the Server Message Block (SMB) protocol. Windows desktop users will be able to mount these directories and file systems as letter drives on their systems.

**MITRE ATT&CK Mappings:** T1543.002


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.12

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Squid is a standard proxy server used in many distributions and environments.

**MITRE ATT&CK Mappings:** TA0008


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.12

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Squid is a standard proxy server used in many distributions and environments.

**MITRE ATT&CK Mappings:** T1203


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.12

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Squid is a standard proxy server used in many distributions and environments.

**MITRE ATT&CK Mappings:** T1203.000


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.12

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Squid is a standard proxy server used in many distributions and environments.

**MITRE ATT&CK Mappings:** T1210


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.12

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Squid is a standard proxy server used in many distributions and environments.

**MITRE ATT&CK Mappings:** T1210.000


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.12

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Squid is a standard proxy server used in many distributions and environments.

**MITRE ATT&CK Mappings:** T1543


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.12

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Squid is a standard proxy server used in many distributions and environments.

**MITRE ATT&CK Mappings:** T1543.002


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.12

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Squid is a standard proxy server used in many distributions and environments.

**MITRE ATT&CK Mappings:** M1042


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.13

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Simple Network Management Protocol (SNMP) is a widely used protocol for monitoring the health and welfare of network equipment, computer equipment and devices like UPSs. 

Net-SNMP is a suite of applications used to implement SNMPv1 (RFC 1157), SNMPv2 (RFCs 1901-1908), and SNMPv3 (RFCs 3411-3418) using both IPv4 and IPv6. 

Support for SNMPv2 classic (a.k.a. "SNMPv2 historic" - RFCs 1441-1452) was dropped with the 4.0 release of the UCD-snmp package.

The Simple Network Management Protocol (SNMP) server is used to listen for SNMP commands from an SNMP management system, execute the commands or collect the information and then send results back to the requesting system.

**MITRE ATT&CK Mappings:** TA0008


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.13

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Simple Network Management Protocol (SNMP) is a widely used protocol for monitoring the health and welfare of network equipment, computer equipment and devices like UPSs. 

Net-SNMP is a suite of applications used to implement SNMPv1 (RFC 1157), SNMPv2 (RFCs 1901-1908), and SNMPv3 (RFCs 3411-3418) using both IPv4 and IPv6. 

Support for SNMPv2 classic (a.k.a. "SNMPv2 historic" - RFCs 1441-1452) was dropped with the 4.0 release of the UCD-snmp package.

The Simple Network Management Protocol (SNMP) server is used to listen for SNMP commands from an SNMP management system, execute the commands or collect the information and then send results back to the requesting system.

**MITRE ATT&CK Mappings:** T1203


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.13

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Simple Network Management Protocol (SNMP) is a widely used protocol for monitoring the health and welfare of network equipment, computer equipment and devices like UPSs. 

Net-SNMP is a suite of applications used to implement SNMPv1 (RFC 1157), SNMPv2 (RFCs 1901-1908), and SNMPv3 (RFCs 3411-3418) using both IPv4 and IPv6. 

Support for SNMPv2 classic (a.k.a. "SNMPv2 historic" - RFCs 1441-1452) was dropped with the 4.0 release of the UCD-snmp package.

The Simple Network Management Protocol (SNMP) server is used to listen for SNMP commands from an SNMP management system, execute the commands or collect the information and then send results back to the requesting system.

**MITRE ATT&CK Mappings:** T1203.000


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.13

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Simple Network Management Protocol (SNMP) is a widely used protocol for monitoring the health and welfare of network equipment, computer equipment and devices like UPSs. 

Net-SNMP is a suite of applications used to implement SNMPv1 (RFC 1157), SNMPv2 (RFCs 1901-1908), and SNMPv3 (RFCs 3411-3418) using both IPv4 and IPv6. 

Support for SNMPv2 classic (a.k.a. "SNMPv2 historic" - RFCs 1441-1452) was dropped with the 4.0 release of the UCD-snmp package.

The Simple Network Management Protocol (SNMP) server is used to listen for SNMP commands from an SNMP management system, execute the commands or collect the information and then send results back to the requesting system.

**MITRE ATT&CK Mappings:** T1210


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.13

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Simple Network Management Protocol (SNMP) is a widely used protocol for monitoring the health and welfare of network equipment, computer equipment and devices like UPSs. 

Net-SNMP is a suite of applications used to implement SNMPv1 (RFC 1157), SNMPv2 (RFCs 1901-1908), and SNMPv3 (RFCs 3411-3418) using both IPv4 and IPv6. 

Support for SNMPv2 classic (a.k.a. "SNMPv2 historic" - RFCs 1441-1452) was dropped with the 4.0 release of the UCD-snmp package.

The Simple Network Management Protocol (SNMP) server is used to listen for SNMP commands from an SNMP management system, execute the commands or collect the information and then send results back to the requesting system.

**MITRE ATT&CK Mappings:** T1210.000


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.13

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Simple Network Management Protocol (SNMP) is a widely used protocol for monitoring the health and welfare of network equipment, computer equipment and devices like UPSs. 

Net-SNMP is a suite of applications used to implement SNMPv1 (RFC 1157), SNMPv2 (RFCs 1901-1908), and SNMPv3 (RFCs 3411-3418) using both IPv4 and IPv6. 

Support for SNMPv2 classic (a.k.a. "SNMPv2 historic" - RFCs 1441-1452) was dropped with the 4.0 release of the UCD-snmp package.

The Simple Network Management Protocol (SNMP) server is used to listen for SNMP commands from an SNMP management system, execute the commands or collect the information and then send results back to the requesting system.

**MITRE ATT&CK Mappings:** T1543


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.13

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Simple Network Management Protocol (SNMP) is a widely used protocol for monitoring the health and welfare of network equipment, computer equipment and devices like UPSs. 

Net-SNMP is a suite of applications used to implement SNMPv1 (RFC 1157), SNMPv2 (RFCs 1901-1908), and SNMPv3 (RFCs 3411-3418) using both IPv4 and IPv6. 

Support for SNMPv2 classic (a.k.a. "SNMPv2 historic" - RFCs 1441-1452) was dropped with the 4.0 release of the UCD-snmp package.

The Simple Network Management Protocol (SNMP) server is used to listen for SNMP commands from an SNMP management system, execute the commands or collect the information and then send results back to the requesting system.

**MITRE ATT&CK Mappings:** T1543.002


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.13

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Simple Network Management Protocol (SNMP) is a widely used protocol for monitoring the health and welfare of network equipment, computer equipment and devices like UPSs. 

Net-SNMP is a suite of applications used to implement SNMPv1 (RFC 1157), SNMPv2 (RFCs 1901-1908), and SNMPv3 (RFCs 3411-3418) using both IPv4 and IPv6. 

Support for SNMPv2 classic (a.k.a. "SNMPv2 historic" - RFCs 1441-1452) was dropped with the 4.0 release of the UCD-snmp package.

The Simple Network Management Protocol (SNMP) server is used to listen for SNMP commands from an SNMP management system, execute the commands or collect the information and then send results back to the requesting system.

**MITRE ATT&CK Mappings:** M1042


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.14

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The Network Information Service (NIS) (formally known as Yellow Pages) is a client-server directory service protocol for distributing system configuration files. The NIS server is a collection of programs that allow for the distribution of configuration files.

**MITRE ATT&CK Mappings:** TA0008


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.14

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The Network Information Service (NIS) (formally known as Yellow Pages) is a client-server directory service protocol for distributing system configuration files. The NIS server is a collection of programs that allow for the distribution of configuration files.

**MITRE ATT&CK Mappings:** T1203


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.14

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The Network Information Service (NIS) (formally known as Yellow Pages) is a client-server directory service protocol for distributing system configuration files. The NIS server is a collection of programs that allow for the distribution of configuration files.

**MITRE ATT&CK Mappings:** T1203.000


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.14

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The Network Information Service (NIS) (formally known as Yellow Pages) is a client-server directory service protocol for distributing system configuration files. The NIS server is a collection of programs that allow for the distribution of configuration files.

**MITRE ATT&CK Mappings:** T1210


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.14

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The Network Information Service (NIS) (formally known as Yellow Pages) is a client-server directory service protocol for distributing system configuration files. The NIS server is a collection of programs that allow for the distribution of configuration files.

**MITRE ATT&CK Mappings:** T1210.000


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.14

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The Network Information Service (NIS) (formally known as Yellow Pages) is a client-server directory service protocol for distributing system configuration files. The NIS server is a collection of programs that allow for the distribution of configuration files.

**MITRE ATT&CK Mappings:** T1543


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.14

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The Network Information Service (NIS) (formally known as Yellow Pages) is a client-server directory service protocol for distributing system configuration files. The NIS server is a collection of programs that allow for the distribution of configuration files.

**MITRE ATT&CK Mappings:** T1543.002


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.14

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The Network Information Service (NIS) (formally known as Yellow Pages) is a client-server directory service protocol for distributing system configuration files. The NIS server is a collection of programs that allow for the distribution of configuration files.

**MITRE ATT&CK Mappings:** M1042


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.15

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Mail Transfer Agents (MTA), such as sendmail and Postfix, are used to listen for incoming mail and transfer the messages to the appropriate user or mail server. If the system is not intended to be a mail server, it is recommended that the MTA be configured to only process local mail.

**MITRE ATT&CK Mappings:** TA0008


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.15

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Mail Transfer Agents (MTA), such as sendmail and Postfix, are used to listen for incoming mail and transfer the messages to the appropriate user or mail server. If the system is not intended to be a mail server, it is recommended that the MTA be configured to only process local mail.

**MITRE ATT&CK Mappings:** T1018


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.15

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Mail Transfer Agents (MTA), such as sendmail and Postfix, are used to listen for incoming mail and transfer the messages to the appropriate user or mail server. If the system is not intended to be a mail server, it is recommended that the MTA be configured to only process local mail.

**MITRE ATT&CK Mappings:** T1018.000


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.15

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Mail Transfer Agents (MTA), such as sendmail and Postfix, are used to listen for incoming mail and transfer the messages to the appropriate user or mail server. If the system is not intended to be a mail server, it is recommended that the MTA be configured to only process local mail.

**MITRE ATT&CK Mappings:** T1210


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.15

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Mail Transfer Agents (MTA), such as sendmail and Postfix, are used to listen for incoming mail and transfer the messages to the appropriate user or mail server. If the system is not intended to be a mail server, it is recommended that the MTA be configured to only process local mail.

**MITRE ATT&CK Mappings:** T1210.000


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.15

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Mail Transfer Agents (MTA), such as sendmail and Postfix, are used to listen for incoming mail and transfer the messages to the appropriate user or mail server. If the system is not intended to be a mail server, it is recommended that the MTA be configured to only process local mail.

**MITRE ATT&CK Mappings:** M1042


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.16

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `rsync` service can be used to synchronize files between systems over network links.

**MITRE ATT&CK Mappings:** TA0008


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.16

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `rsync` service can be used to synchronize files between systems over network links.

**MITRE ATT&CK Mappings:** T1105


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.16

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `rsync` service can be used to synchronize files between systems over network links.

**MITRE ATT&CK Mappings:** T1105.000


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.16

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `rsync` service can be used to synchronize files between systems over network links.

**MITRE ATT&CK Mappings:** T1203


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.16

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `rsync` service can be used to synchronize files between systems over network links.

**MITRE ATT&CK Mappings:** T1203.000


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.16

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `rsync` service can be used to synchronize files between systems over network links.

**MITRE ATT&CK Mappings:** T1210


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.16

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `rsync` service can be used to synchronize files between systems over network links.

**MITRE ATT&CK Mappings:** T1210.000


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.16

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `rsync` service can be used to synchronize files between systems over network links.

**MITRE ATT&CK Mappings:** T1543


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.16

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `rsync` service can be used to synchronize files between systems over network links.

**MITRE ATT&CK Mappings:** T1543.002


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.16

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `rsync` service can be used to synchronize files between systems over network links.

**MITRE ATT&CK Mappings:** T1570


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.16

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `rsync` service can be used to synchronize files between systems over network links.

**MITRE ATT&CK Mappings:** T1570.000


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.2.16

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `rsync` service can be used to synchronize files between systems over network links.

**MITRE ATT&CK Mappings:** M1042


Section #: 2.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.3.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The Network Information Service (NIS), formerly known as Yellow Pages, is a client-server directory service protocol used to distribute system configuration files. The NIS client was used to bind a machine to an NIS server and receive the distributed configuration files.

**MITRE ATT&CK Mappings:** TA0008


Section #: 2.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.3.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The Network Information Service (NIS), formerly known as Yellow Pages, is a client-server directory service protocol used to distribute system configuration files. The NIS client was used to bind a machine to an NIS server and receive the distributed configuration files.

**MITRE ATT&CK Mappings:** T1203


Section #: 2.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.3.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The Network Information Service (NIS), formerly known as Yellow Pages, is a client-server directory service protocol used to distribute system configuration files. The NIS client was used to bind a machine to an NIS server and receive the distributed configuration files.

**MITRE ATT&CK Mappings:** T1203.000


Section #: 2.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.3.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The Network Information Service (NIS), formerly known as Yellow Pages, is a client-server directory service protocol used to distribute system configuration files. The NIS client was used to bind a machine to an NIS server and receive the distributed configuration files.

**MITRE ATT&CK Mappings:** T1543


Section #: 2.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.3.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The Network Information Service (NIS), formerly known as Yellow Pages, is a client-server directory service protocol used to distribute system configuration files. The NIS client was used to bind a machine to an NIS server and receive the distributed configuration files.

**MITRE ATT&CK Mappings:** T1543.002


Section #: 2.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.3.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The Network Information Service (NIS), formerly known as Yellow Pages, is a client-server directory service protocol used to distribute system configuration files. The NIS client was used to bind a machine to an NIS server and receive the distributed configuration files.

**MITRE ATT&CK Mappings:** M1042


Section #: 2.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.3.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `rsh-client` package contains the client commands for the rsh services.

**MITRE ATT&CK Mappings:** TA0008


Section #: 2.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.3.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `rsh-client` package contains the client commands for the rsh services.

**MITRE ATT&CK Mappings:** T1040


Section #: 2.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.3.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `rsh-client` package contains the client commands for the rsh services.

**MITRE ATT&CK Mappings:** T1040.000


Section #: 2.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.3.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `rsh-client` package contains the client commands for the rsh services.

**MITRE ATT&CK Mappings:** T1203


Section #: 2.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.3.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `rsh-client` package contains the client commands for the rsh services.

**MITRE ATT&CK Mappings:** T1203.000


Section #: 2.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.3.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `rsh-client` package contains the client commands for the rsh services.

**MITRE ATT&CK Mappings:** T1543


Section #: 2.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.3.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `rsh-client` package contains the client commands for the rsh services.

**MITRE ATT&CK Mappings:** T1543.002


Section #: 2.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.3.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `rsh-client` package contains the client commands for the rsh services.

**MITRE ATT&CK Mappings:** M1041


Section #: 2.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.3.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `rsh-client` package contains the client commands for the rsh services.

**MITRE ATT&CK Mappings:** M1042


Section #: 2.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.3.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `talk` software makes it possible for users to send and receive messages across systems through a terminal session. The `talk` client, which allows initialization of talk sessions, is installed by default.

**MITRE ATT&CK Mappings:** TA0006


Section #: 2.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.3.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `talk` software makes it possible for users to send and receive messages across systems through a terminal session. The `talk` client, which allows initialization of talk sessions, is installed by default.

**MITRE ATT&CK Mappings:** TA0008


Section #: 2.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.3.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `talk` software makes it possible for users to send and receive messages across systems through a terminal session. The `talk` client, which allows initialization of talk sessions, is installed by default.

**MITRE ATT&CK Mappings:** T1203


Section #: 2.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.3.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `talk` software makes it possible for users to send and receive messages across systems through a terminal session. The `talk` client, which allows initialization of talk sessions, is installed by default.

**MITRE ATT&CK Mappings:** T1203.000


Section #: 2.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.3.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `talk` software makes it possible for users to send and receive messages across systems through a terminal session. The `talk` client, which allows initialization of talk sessions, is installed by default.

**MITRE ATT&CK Mappings:** T1543


Section #: 2.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.3.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `talk` software makes it possible for users to send and receive messages across systems through a terminal session. The `talk` client, which allows initialization of talk sessions, is installed by default.

**MITRE ATT&CK Mappings:** T1543.002


Section #: 2.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.3.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `talk` software makes it possible for users to send and receive messages across systems through a terminal session. The `talk` client, which allows initialization of talk sessions, is installed by default.

**MITRE ATT&CK Mappings:** M1041


Section #: 2.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.3.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `talk` software makes it possible for users to send and receive messages across systems through a terminal session. The `talk` client, which allows initialization of talk sessions, is installed by default.

**MITRE ATT&CK Mappings:** M1042


Section #: 2.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.3.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `telnet` package contains the `telnet` client, which allows users to start connections to other systems via the telnet protocol.

**MITRE ATT&CK Mappings:** TA0006


Section #: 2.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.3.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `telnet` package contains the `telnet` client, which allows users to start connections to other systems via the telnet protocol.

**MITRE ATT&CK Mappings:** TA0008


Section #: 2.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.3.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `telnet` package contains the `telnet` client, which allows users to start connections to other systems via the telnet protocol.

**MITRE ATT&CK Mappings:** T1040


Section #: 2.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.3.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `telnet` package contains the `telnet` client, which allows users to start connections to other systems via the telnet protocol.

**MITRE ATT&CK Mappings:** T1040.000


Section #: 2.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.3.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `telnet` package contains the `telnet` client, which allows users to start connections to other systems via the telnet protocol.

**MITRE ATT&CK Mappings:** T1203


Section #: 2.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.3.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `telnet` package contains the `telnet` client, which allows users to start connections to other systems via the telnet protocol.

**MITRE ATT&CK Mappings:** T1203.000


Section #: 2.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.3.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `telnet` package contains the `telnet` client, which allows users to start connections to other systems via the telnet protocol.

**MITRE ATT&CK Mappings:** T1543


Section #: 2.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.3.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `telnet` package contains the `telnet` client, which allows users to start connections to other systems via the telnet protocol.

**MITRE ATT&CK Mappings:** T1543.002


Section #: 2.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.3.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `telnet` package contains the `telnet` client, which allows users to start connections to other systems via the telnet protocol.

**MITRE ATT&CK Mappings:** M1041


Section #: 2.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.3.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `telnet` package contains the `telnet` client, which allows users to start connections to other systems via the telnet protocol.

**MITRE ATT&CK Mappings:** M1042


Section #: 2.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.3.5

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The Lightweight Directory Access Protocol (LDAP) was introduced as a replacement for NIS/YP. It is a service that provides a method for looking up information from a central database.

**MITRE ATT&CK Mappings:** TA0008


Section #: 2.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.3.5

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The Lightweight Directory Access Protocol (LDAP) was introduced as a replacement for NIS/YP. It is a service that provides a method for looking up information from a central database.

**MITRE ATT&CK Mappings:** T1203


Section #: 2.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.3.5

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The Lightweight Directory Access Protocol (LDAP) was introduced as a replacement for NIS/YP. It is a service that provides a method for looking up information from a central database.

**MITRE ATT&CK Mappings:** T1203.000


Section #: 2.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.3.5

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The Lightweight Directory Access Protocol (LDAP) was introduced as a replacement for NIS/YP. It is a service that provides a method for looking up information from a central database.

**MITRE ATT&CK Mappings:** T1543


Section #: 2.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.3.5

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The Lightweight Directory Access Protocol (LDAP) was introduced as a replacement for NIS/YP. It is a service that provides a method for looking up information from a central database.

**MITRE ATT&CK Mappings:** T1543.002


Section #: 2.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.3.5

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The Lightweight Directory Access Protocol (LDAP) was introduced as a replacement for NIS/YP. It is a service that provides a method for looking up information from a central database.

**MITRE ATT&CK Mappings:** M1042


Section #: 2.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.3.6

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Remote Procedure Call (RPC) is a method for creating low level client server applications across different system architectures. It requires an RPC compliant client listening on a network port. The supporting package is rpcbind."

**MITRE ATT&CK Mappings:** TA0008


Section #: 2.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.3.6

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Remote Procedure Call (RPC) is a method for creating low level client server applications across different system architectures. It requires an RPC compliant client listening on a network port. The supporting package is rpcbind."

**MITRE ATT&CK Mappings:** T1203


Section #: 2.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.3.6

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Remote Procedure Call (RPC) is a method for creating low level client server applications across different system architectures. It requires an RPC compliant client listening on a network port. The supporting package is rpcbind."

**MITRE ATT&CK Mappings:** T1203.000


Section #: 2.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.3.6

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Remote Procedure Call (RPC) is a method for creating low level client server applications across different system architectures. It requires an RPC compliant client listening on a network port. The supporting package is rpcbind."

**MITRE ATT&CK Mappings:** T1543


Section #: 2.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.3.6

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Remote Procedure Call (RPC) is a method for creating low level client server applications across different system architectures. It requires an RPC compliant client listening on a network port. The supporting package is rpcbind."

**MITRE ATT&CK Mappings:** T1543.002


Section #: 2.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 2.3.6

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Remote Procedure Call (RPC) is a method for creating low level client server applications across different system architectures. It requires an RPC compliant client listening on a network port. The supporting package is rpcbind."

**MITRE ATT&CK Mappings:** M1042


Section #: 3.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.1.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Internet Protocol Version 6 (IPv6) is the most recent version of Internet Protocol (IP). It's designed to supply IP addressing and additional security to support the predicted growth of connected devices. IPv6 is based on 128-bit addressing and can support 340 undecillion, which is 340 trillion3 addresses.

Features of IPv6
- Hierarchical addressing and routing infrastructure
- Stateful and Stateless configuration
- Support for quality of service (QoS)
- An ideal protocol for neighboring node interaction

**MITRE ATT&CK Mappings:** TA0008


Section #: 3.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.1.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Internet Protocol Version 6 (IPv6) is the most recent version of Internet Protocol (IP). It's designed to supply IP addressing and additional security to support the predicted growth of connected devices. IPv6 is based on 128-bit addressing and can support 340 undecillion, which is 340 trillion3 addresses.

Features of IPv6
- Hierarchical addressing and routing infrastructure
- Stateful and Stateless configuration
- Support for quality of service (QoS)
- An ideal protocol for neighboring node interaction

**MITRE ATT&CK Mappings:** T1557


Section #: 3.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.1.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Internet Protocol Version 6 (IPv6) is the most recent version of Internet Protocol (IP). It's designed to supply IP addressing and additional security to support the predicted growth of connected devices. IPv6 is based on 128-bit addressing and can support 340 undecillion, which is 340 trillion3 addresses.

Features of IPv6
- Hierarchical addressing and routing infrastructure
- Stateful and Stateless configuration
- Support for quality of service (QoS)
- An ideal protocol for neighboring node interaction

**MITRE ATT&CK Mappings:** T1557.000


Section #: 3.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.1.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Internet Protocol Version 6 (IPv6) is the most recent version of Internet Protocol (IP). It's designed to supply IP addressing and additional security to support the predicted growth of connected devices. IPv6 is based on 128-bit addressing and can support 340 undecillion, which is 340 trillion3 addresses.

Features of IPv6
- Hierarchical addressing and routing infrastructure
- Stateful and Stateless configuration
- Support for quality of service (QoS)
- An ideal protocol for neighboring node interaction

**MITRE ATT&CK Mappings:** T1595


Section #: 3.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.1.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Internet Protocol Version 6 (IPv6) is the most recent version of Internet Protocol (IP). It's designed to supply IP addressing and additional security to support the predicted growth of connected devices. IPv6 is based on 128-bit addressing and can support 340 undecillion, which is 340 trillion3 addresses.

Features of IPv6
- Hierarchical addressing and routing infrastructure
- Stateful and Stateless configuration
- Support for quality of service (QoS)
- An ideal protocol for neighboring node interaction

**MITRE ATT&CK Mappings:** T1595.001


Section #: 3.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.1.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Internet Protocol Version 6 (IPv6) is the most recent version of Internet Protocol (IP). It's designed to supply IP addressing and additional security to support the predicted growth of connected devices. IPv6 is based on 128-bit addressing and can support 340 undecillion, which is 340 trillion3 addresses.

Features of IPv6
- Hierarchical addressing and routing infrastructure
- Stateful and Stateless configuration
- Support for quality of service (QoS)
- An ideal protocol for neighboring node interaction

**MITRE ATT&CK Mappings:** T1595.002


Section #: 3.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.1.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Internet Protocol Version 6 (IPv6) is the most recent version of Internet Protocol (IP). It's designed to supply IP addressing and additional security to support the predicted growth of connected devices. IPv6 is based on 128-bit addressing and can support 340 undecillion, which is 340 trillion3 addresses.

Features of IPv6
- Hierarchical addressing and routing infrastructure
- Stateful and Stateless configuration
- Support for quality of service (QoS)
- An ideal protocol for neighboring node interaction

**MITRE ATT&CK Mappings:** M1042


Section #: 3.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.1.2

**Profiles:** Level 1 - Server, Level 2 - Workstation

**Description:** Wireless networking is used when wired networks are unavailable. Debian contains a wireless tool kit to allow system administrators to configure and use wireless networks.

**MITRE ATT&CK Mappings:** TA0010


Section #: 3.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.1.2

**Profiles:** Level 1 - Server, Level 2 - Workstation

**Description:** Wireless networking is used when wired networks are unavailable. Debian contains a wireless tool kit to allow system administrators to configure and use wireless networks.

**MITRE ATT&CK Mappings:** T1011


Section #: 3.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.1.2

**Profiles:** Level 1 - Server, Level 2 - Workstation

**Description:** Wireless networking is used when wired networks are unavailable. Debian contains a wireless tool kit to allow system administrators to configure and use wireless networks.

**MITRE ATT&CK Mappings:** T1011.000


Section #: 3.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.1.2

**Profiles:** Level 1 - Server, Level 2 - Workstation

**Description:** Wireless networking is used when wired networks are unavailable. Debian contains a wireless tool kit to allow system administrators to configure and use wireless networks.

**MITRE ATT&CK Mappings:** T1595


Section #: 3.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.1.2

**Profiles:** Level 1 - Server, Level 2 - Workstation

**Description:** Wireless networking is used when wired networks are unavailable. Debian contains a wireless tool kit to allow system administrators to configure and use wireless networks.

**MITRE ATT&CK Mappings:** T1595.001


Section #: 3.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.1.2

**Profiles:** Level 1 - Server, Level 2 - Workstation

**Description:** Wireless networking is used when wired networks are unavailable. Debian contains a wireless tool kit to allow system administrators to configure and use wireless networks.

**MITRE ATT&CK Mappings:** T1595.002


Section #: 3.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.1.2

**Profiles:** Level 1 - Server, Level 2 - Workstation

**Description:** Wireless networking is used when wired networks are unavailable. Debian contains a wireless tool kit to allow system administrators to configure and use wireless networks.

**MITRE ATT&CK Mappings:** M1028


Section #: 3.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.1.3

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** The Datagram Congestion Control Protocol (DCCP) is a transport layer protocol that supports streaming media and telephony. DCCP provides a way to gain access to congestion control, without having to do it at the application layer, but does not provide in-sequence delivery.

**MITRE ATT&CK Mappings:** T1068


Section #: 3.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.1.3

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** The Datagram Congestion Control Protocol (DCCP) is a transport layer protocol that supports streaming media and telephony. DCCP provides a way to gain access to congestion control, without having to do it at the application layer, but does not provide in-sequence delivery.

**MITRE ATT&CK Mappings:** T1068.000


Section #: 3.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.1.3

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** The Datagram Congestion Control Protocol (DCCP) is a transport layer protocol that supports streaming media and telephony. DCCP provides a way to gain access to congestion control, without having to do it at the application layer, but does not provide in-sequence delivery.

**MITRE ATT&CK Mappings:** T1210


Section #: 3.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.1.3

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** The Datagram Congestion Control Protocol (DCCP) is a transport layer protocol that supports streaming media and telephony. DCCP provides a way to gain access to congestion control, without having to do it at the application layer, but does not provide in-sequence delivery.

**MITRE ATT&CK Mappings:** T1210.000


Section #: 3.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.1.3

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** The Datagram Congestion Control Protocol (DCCP) is a transport layer protocol that supports streaming media and telephony. DCCP provides a way to gain access to congestion control, without having to do it at the application layer, but does not provide in-sequence delivery.

**MITRE ATT&CK Mappings:** TA0008


Section #: 3.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.1.3

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** The Datagram Congestion Control Protocol (DCCP) is a transport layer protocol that supports streaming media and telephony. DCCP provides a way to gain access to congestion control, without having to do it at the application layer, but does not provide in-sequence delivery.

**MITRE ATT&CK Mappings:** M1042


Section #: 3.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.1.4

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** The Stream Control Transmission Protocol (SCTP) is a transport layer protocol used to support message oriented communication, with several streams of messages in one connection. It serves a similar function as TCP and UDP, incorporating features of both. It is message-oriented like UDP, and ensures reliable in-sequence transport of messages with congestion control like TCP.

**MITRE ATT&CK Mappings:** T1068


Section #: 3.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.1.4

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** The Stream Control Transmission Protocol (SCTP) is a transport layer protocol used to support message oriented communication, with several streams of messages in one connection. It serves a similar function as TCP and UDP, incorporating features of both. It is message-oriented like UDP, and ensures reliable in-sequence transport of messages with congestion control like TCP.

**MITRE ATT&CK Mappings:** T1068.000


Section #: 3.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.1.4

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** The Stream Control Transmission Protocol (SCTP) is a transport layer protocol used to support message oriented communication, with several streams of messages in one connection. It serves a similar function as TCP and UDP, incorporating features of both. It is message-oriented like UDP, and ensures reliable in-sequence transport of messages with congestion control like TCP.

**MITRE ATT&CK Mappings:** T1210


Section #: 3.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.1.4

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** The Stream Control Transmission Protocol (SCTP) is a transport layer protocol used to support message oriented communication, with several streams of messages in one connection. It serves a similar function as TCP and UDP, incorporating features of both. It is message-oriented like UDP, and ensures reliable in-sequence transport of messages with congestion control like TCP.

**MITRE ATT&CK Mappings:** T1210.000


Section #: 3.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.1.4

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** The Stream Control Transmission Protocol (SCTP) is a transport layer protocol used to support message oriented communication, with several streams of messages in one connection. It serves a similar function as TCP and UDP, incorporating features of both. It is message-oriented like UDP, and ensures reliable in-sequence transport of messages with congestion control like TCP.

**MITRE ATT&CK Mappings:** TA0008


Section #: 3.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.1.4

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** The Stream Control Transmission Protocol (SCTP) is a transport layer protocol used to support message oriented communication, with several streams of messages in one connection. It serves a similar function as TCP and UDP, incorporating features of both. It is message-oriented like UDP, and ensures reliable in-sequence transport of messages with congestion control like TCP.

**MITRE ATT&CK Mappings:** M1042


Section #: 3.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.1.5

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** The Reliable Datagram Sockets (RDS) protocol is a transport layer protocol designed to provide low-latency, high-bandwidth communications between cluster nodes. It was developed by the Oracle Corporation.

**MITRE ATT&CK Mappings:** T1068


Section #: 3.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.1.5

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** The Reliable Datagram Sockets (RDS) protocol is a transport layer protocol designed to provide low-latency, high-bandwidth communications between cluster nodes. It was developed by the Oracle Corporation.

**MITRE ATT&CK Mappings:** T1068.000


Section #: 3.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.1.5

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** The Reliable Datagram Sockets (RDS) protocol is a transport layer protocol designed to provide low-latency, high-bandwidth communications between cluster nodes. It was developed by the Oracle Corporation.

**MITRE ATT&CK Mappings:** T1210


Section #: 3.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.1.5

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** The Reliable Datagram Sockets (RDS) protocol is a transport layer protocol designed to provide low-latency, high-bandwidth communications between cluster nodes. It was developed by the Oracle Corporation.

**MITRE ATT&CK Mappings:** T1210.000


Section #: 3.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.1.5

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** The Reliable Datagram Sockets (RDS) protocol is a transport layer protocol designed to provide low-latency, high-bandwidth communications between cluster nodes. It was developed by the Oracle Corporation.

**MITRE ATT&CK Mappings:** TA0008


Section #: 3.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.1.5

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** The Reliable Datagram Sockets (RDS) protocol is a transport layer protocol designed to provide low-latency, high-bandwidth communications between cluster nodes. It was developed by the Oracle Corporation.

**MITRE ATT&CK Mappings:** M1042


Section #: 3.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.1.6

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** The Transparent Inter-Process Communication (TIPC) protocol is designed to provide communication between cluster nodes.

**MITRE ATT&CK Mappings:** T1068


Section #: 3.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.1.6

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** The Transparent Inter-Process Communication (TIPC) protocol is designed to provide communication between cluster nodes.

**MITRE ATT&CK Mappings:** T1068.000


Section #: 3.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.1.6

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** The Transparent Inter-Process Communication (TIPC) protocol is designed to provide communication between cluster nodes.

**MITRE ATT&CK Mappings:** T1210


Section #: 3.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.1.6

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** The Transparent Inter-Process Communication (TIPC) protocol is designed to provide communication between cluster nodes.

**MITRE ATT&CK Mappings:** T1210.000


Section #: 3.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.1.6

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** The Transparent Inter-Process Communication (TIPC) protocol is designed to provide communication between cluster nodes.

**MITRE ATT&CK Mappings:** TA0008


Section #: 3.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.1.6

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** The Transparent Inter-Process Communication (TIPC) protocol is designed to provide communication between cluster nodes.

**MITRE ATT&CK Mappings:** M1042


Section #: 3.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.2.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** ICMP Redirects are used to send routing information to other hosts. As a host itself does not act as a router (in a host only configuration), there is no need to send redirects.

**MITRE ATT&CK Mappings:** TA0006


Section #: 3.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.2.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** ICMP Redirects are used to send routing information to other hosts. As a host itself does not act as a router (in a host only configuration), there is no need to send redirects.

**MITRE ATT&CK Mappings:** TA0009


Section #: 3.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.2.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** ICMP Redirects are used to send routing information to other hosts. As a host itself does not act as a router (in a host only configuration), there is no need to send redirects.

**MITRE ATT&CK Mappings:** T1557


Section #: 3.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.2.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** ICMP Redirects are used to send routing information to other hosts. As a host itself does not act as a router (in a host only configuration), there is no need to send redirects.

**MITRE ATT&CK Mappings:** T1557.000


Section #: 3.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.2.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** ICMP Redirects are used to send routing information to other hosts. As a host itself does not act as a router (in a host only configuration), there is no need to send redirects.

**MITRE ATT&CK Mappings:** M1030


Section #: 3.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.2.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** ICMP Redirects are used to send routing information to other hosts. As a host itself does not act as a router (in a host only configuration), there is no need to send redirects.

**MITRE ATT&CK Mappings:** M1042


Section #: 3.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.2.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `net.ipv4.ip_forward` and `net.ipv6.conf.all.forwarding` flags are used to tell the system whether it can forward packets or not.

**MITRE ATT&CK Mappings:** TA0006


Section #: 3.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.2.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `net.ipv4.ip_forward` and `net.ipv6.conf.all.forwarding` flags are used to tell the system whether it can forward packets or not.

**MITRE ATT&CK Mappings:** TA0009


Section #: 3.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.2.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `net.ipv4.ip_forward` and `net.ipv6.conf.all.forwarding` flags are used to tell the system whether it can forward packets or not.

**MITRE ATT&CK Mappings:** T1557


Section #: 3.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.2.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `net.ipv4.ip_forward` and `net.ipv6.conf.all.forwarding` flags are used to tell the system whether it can forward packets or not.

**MITRE ATT&CK Mappings:** T1557.000


Section #: 3.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.2.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `net.ipv4.ip_forward` and `net.ipv6.conf.all.forwarding` flags are used to tell the system whether it can forward packets or not.

**MITRE ATT&CK Mappings:** M1030


Section #: 3.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.2.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `net.ipv4.ip_forward` and `net.ipv6.conf.all.forwarding` flags are used to tell the system whether it can forward packets or not.

**MITRE ATT&CK Mappings:** M1042


Section #: 3.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.3.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** In networking, source routing allows a sender to partially or fully specify the route packets take through a network. In contrast, non-source routed packets travel a path determined by routers in the network. In some cases, systems may not be routable or reachable from some locations (e.g. private addresses vs. Internet routable), and so source routed packets would need to be used.

**MITRE ATT&CK Mappings:** TA0007


Section #: 3.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.3.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** In networking, source routing allows a sender to partially or fully specify the route packets take through a network. In contrast, non-source routed packets travel a path determined by routers in the network. In some cases, systems may not be routable or reachable from some locations (e.g. private addresses vs. Internet routable), and so source routed packets would need to be used.

**MITRE ATT&CK Mappings:** T1590


Section #: 3.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.3.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** In networking, source routing allows a sender to partially or fully specify the route packets take through a network. In contrast, non-source routed packets travel a path determined by routers in the network. In some cases, systems may not be routable or reachable from some locations (e.g. private addresses vs. Internet routable), and so source routed packets would need to be used.

**MITRE ATT&CK Mappings:** T1590.005


Section #: 3.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.3.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** ICMP redirect messages are packets that convey routing information and tell your host (acting as a router) to send packets via an alternate path. It is a way of allowing an outside routing device to update your system routing tables.

**MITRE ATT&CK Mappings:** TA0006


Section #: 3.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.3.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** ICMP redirect messages are packets that convey routing information and tell your host (acting as a router) to send packets via an alternate path. It is a way of allowing an outside routing device to update your system routing tables.

**MITRE ATT&CK Mappings:** TA0009


Section #: 3.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.3.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** ICMP redirect messages are packets that convey routing information and tell your host (acting as a router) to send packets via an alternate path. It is a way of allowing an outside routing device to update your system routing tables.

**MITRE ATT&CK Mappings:** T1557


Section #: 3.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.3.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** ICMP redirect messages are packets that convey routing information and tell your host (acting as a router) to send packets via an alternate path. It is a way of allowing an outside routing device to update your system routing tables.

**MITRE ATT&CK Mappings:** T1557.000


Section #: 3.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.3.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** ICMP redirect messages are packets that convey routing information and tell your host (acting as a router) to send packets via an alternate path. It is a way of allowing an outside routing device to update your system routing tables.

**MITRE ATT&CK Mappings:** M1030


Section #: 3.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.3.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** ICMP redirect messages are packets that convey routing information and tell your host (acting as a router) to send packets via an alternate path. It is a way of allowing an outside routing device to update your system routing tables.

**MITRE ATT&CK Mappings:** M1042


Section #: 3.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.3.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Secure ICMP redirects are the same as ICMP redirects, except they come from gateways listed on the default gateway list. It is assumed that these gateways are known to your system, and that they are likely to be secure.

**MITRE ATT&CK Mappings:** TA0006


Section #: 3.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.3.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Secure ICMP redirects are the same as ICMP redirects, except they come from gateways listed on the default gateway list. It is assumed that these gateways are known to your system, and that they are likely to be secure.

**MITRE ATT&CK Mappings:** TA0009


Section #: 3.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.3.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Secure ICMP redirects are the same as ICMP redirects, except they come from gateways listed on the default gateway list. It is assumed that these gateways are known to your system, and that they are likely to be secure.

**MITRE ATT&CK Mappings:** T1557


Section #: 3.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.3.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Secure ICMP redirects are the same as ICMP redirects, except they come from gateways listed on the default gateway list. It is assumed that these gateways are known to your system, and that they are likely to be secure.

**MITRE ATT&CK Mappings:** T1557.000


Section #: 3.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.3.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Secure ICMP redirects are the same as ICMP redirects, except they come from gateways listed on the default gateway list. It is assumed that these gateways are known to your system, and that they are likely to be secure.

**MITRE ATT&CK Mappings:** M1030


Section #: 3.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.3.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Secure ICMP redirects are the same as ICMP redirects, except they come from gateways listed on the default gateway list. It is assumed that these gateways are known to your system, and that they are likely to be secure.

**MITRE ATT&CK Mappings:** M1042


Section #: 3.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.3.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** When enabled, this feature logs packets with un-routable source addresses to the kernel log.

**MITRE ATT&CK Mappings:** TA0005


Section #: 3.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.3.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** When enabled, this feature logs packets with un-routable source addresses to the kernel log.

**MITRE ATT&CK Mappings:** T1562


Section #: 3.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.3.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** When enabled, this feature logs packets with un-routable source addresses to the kernel log.

**MITRE ATT&CK Mappings:** T1562.006


Section #: 3.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.3.5

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Setting `net.ipv4.icmp_echo_ignore_broadcasts = 1` will cause the system to ignore all ICMP echo and timestamp requests to broadcast and multicast addresses.

**MITRE ATT&CK Mappings:** TA0040


Section #: 3.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.3.5

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Setting `net.ipv4.icmp_echo_ignore_broadcasts = 1` will cause the system to ignore all ICMP echo and timestamp requests to broadcast and multicast addresses.

**MITRE ATT&CK Mappings:** T1498


Section #: 3.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.3.5

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Setting `net.ipv4.icmp_echo_ignore_broadcasts = 1` will cause the system to ignore all ICMP echo and timestamp requests to broadcast and multicast addresses.

**MITRE ATT&CK Mappings:** T1498.001


Section #: 3.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.3.5

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Setting `net.ipv4.icmp_echo_ignore_broadcasts = 1` will cause the system to ignore all ICMP echo and timestamp requests to broadcast and multicast addresses.

**MITRE ATT&CK Mappings:** M1037


Section #: 3.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.3.6

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Setting `icmp_ignore_bogus_error_responses = 1` prevents the kernel from logging bogus responses (RFC-1122 non-compliant) from broadcast reframes, keeping file systems from filling up with useless log messages.

**MITRE ATT&CK Mappings:** TA0040


Section #: 3.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.3.6

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Setting `icmp_ignore_bogus_error_responses = 1` prevents the kernel from logging bogus responses (RFC-1122 non-compliant) from broadcast reframes, keeping file systems from filling up with useless log messages.

**MITRE ATT&CK Mappings:** T1562


Section #: 3.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.3.6

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Setting `icmp_ignore_bogus_error_responses = 1` prevents the kernel from logging bogus responses (RFC-1122 non-compliant) from broadcast reframes, keeping file systems from filling up with useless log messages.

**MITRE ATT&CK Mappings:** T1562.006


Section #: 3.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.3.6

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Setting `icmp_ignore_bogus_error_responses = 1` prevents the kernel from logging bogus responses (RFC-1122 non-compliant) from broadcast reframes, keeping file systems from filling up with useless log messages.

**MITRE ATT&CK Mappings:** M1053


Section #: 3.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.3.7

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Setting `net.ipv4.conf.all.rp_filter` and `net.ipv4.conf.default.rp_filter` to `1` forces the Linux kernel to utilize reverse path filtering on a received packet to determine if the packet was valid. Essentially, with reverse path filtering, if the return packet does not go out the same interface that the corresponding source packet came from, the packet is dropped (and logged if `log_martians` is set).

**MITRE ATT&CK Mappings:** TA0006


Section #: 3.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.3.7

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Setting `net.ipv4.conf.all.rp_filter` and `net.ipv4.conf.default.rp_filter` to `1` forces the Linux kernel to utilize reverse path filtering on a received packet to determine if the packet was valid. Essentially, with reverse path filtering, if the return packet does not go out the same interface that the corresponding source packet came from, the packet is dropped (and logged if `log_martians` is set).

**MITRE ATT&CK Mappings:** TA0040


Section #: 3.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.3.7

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Setting `net.ipv4.conf.all.rp_filter` and `net.ipv4.conf.default.rp_filter` to `1` forces the Linux kernel to utilize reverse path filtering on a received packet to determine if the packet was valid. Essentially, with reverse path filtering, if the return packet does not go out the same interface that the corresponding source packet came from, the packet is dropped (and logged if `log_martians` is set).

**MITRE ATT&CK Mappings:** T1498


Section #: 3.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.3.7

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Setting `net.ipv4.conf.all.rp_filter` and `net.ipv4.conf.default.rp_filter` to `1` forces the Linux kernel to utilize reverse path filtering on a received packet to determine if the packet was valid. Essentially, with reverse path filtering, if the return packet does not go out the same interface that the corresponding source packet came from, the packet is dropped (and logged if `log_martians` is set).

**MITRE ATT&CK Mappings:** T1498.001


Section #: 3.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.3.7

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Setting `net.ipv4.conf.all.rp_filter` and `net.ipv4.conf.default.rp_filter` to `1` forces the Linux kernel to utilize reverse path filtering on a received packet to determine if the packet was valid. Essentially, with reverse path filtering, if the return packet does not go out the same interface that the corresponding source packet came from, the packet is dropped (and logged if `log_martians` is set).

**MITRE ATT&CK Mappings:** M1030


Section #: 3.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.3.7

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Setting `net.ipv4.conf.all.rp_filter` and `net.ipv4.conf.default.rp_filter` to `1` forces the Linux kernel to utilize reverse path filtering on a received packet to determine if the packet was valid. Essentially, with reverse path filtering, if the return packet does not go out the same interface that the corresponding source packet came from, the packet is dropped (and logged if `log_martians` is set).

**MITRE ATT&CK Mappings:** M1042


Section #: 3.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.3.8

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** When `tcp_syncookies` is set, the kernel will handle TCP SYN packets normally until the half-open connection queue is full, at which time, the SYN cookie functionality kicks in. SYN cookies work by not using the SYN queue at all. Instead, the kernel simply replies to the SYN with a SYN|ACK, but will include a specially crafted TCP sequence number that encodes the source and destination IP address and port number and the time the packet was sent. A legitimate connection would send the ACK packet of the three way handshake with the specially crafted sequence number. This allows the system to verify that it has received a valid response to a SYN cookie and allow the connection, even though there is no corresponding SYN in the queue.

**MITRE ATT&CK Mappings:** TA0040


Section #: 3.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.3.8

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** When `tcp_syncookies` is set, the kernel will handle TCP SYN packets normally until the half-open connection queue is full, at which time, the SYN cookie functionality kicks in. SYN cookies work by not using the SYN queue at all. Instead, the kernel simply replies to the SYN with a SYN|ACK, but will include a specially crafted TCP sequence number that encodes the source and destination IP address and port number and the time the packet was sent. A legitimate connection would send the ACK packet of the three way handshake with the specially crafted sequence number. This allows the system to verify that it has received a valid response to a SYN cookie and allow the connection, even though there is no corresponding SYN in the queue.

**MITRE ATT&CK Mappings:** T1499


Section #: 3.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.3.8

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** When `tcp_syncookies` is set, the kernel will handle TCP SYN packets normally until the half-open connection queue is full, at which time, the SYN cookie functionality kicks in. SYN cookies work by not using the SYN queue at all. Instead, the kernel simply replies to the SYN with a SYN|ACK, but will include a specially crafted TCP sequence number that encodes the source and destination IP address and port number and the time the packet was sent. A legitimate connection would send the ACK packet of the three way handshake with the specially crafted sequence number. This allows the system to verify that it has received a valid response to a SYN cookie and allow the connection, even though there is no corresponding SYN in the queue.

**MITRE ATT&CK Mappings:** T1499.001


Section #: 3.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.3.8

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** When `tcp_syncookies` is set, the kernel will handle TCP SYN packets normally until the half-open connection queue is full, at which time, the SYN cookie functionality kicks in. SYN cookies work by not using the SYN queue at all. Instead, the kernel simply replies to the SYN with a SYN|ACK, but will include a specially crafted TCP sequence number that encodes the source and destination IP address and port number and the time the packet was sent. A legitimate connection would send the ACK packet of the three way handshake with the specially crafted sequence number. This allows the system to verify that it has received a valid response to a SYN cookie and allow the connection, even though there is no corresponding SYN in the queue.

**MITRE ATT&CK Mappings:** M1037


Section #: 3.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.3.9

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** This setting disables the system's ability to accept IPv6 router advertisements.

**MITRE ATT&CK Mappings:** TA0006


Section #: 3.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.3.9

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** This setting disables the system's ability to accept IPv6 router advertisements.

**MITRE ATT&CK Mappings:** TA0040


Section #: 3.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.3.9

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** This setting disables the system's ability to accept IPv6 router advertisements.

**MITRE ATT&CK Mappings:** T1557


Section #: 3.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.3.9

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** This setting disables the system's ability to accept IPv6 router advertisements.

**MITRE ATT&CK Mappings:** T1557.000


Section #: 3.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.3.9

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** This setting disables the system's ability to accept IPv6 router advertisements.

**MITRE ATT&CK Mappings:** M1030


Section #: 3.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.3.9

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** This setting disables the system's ability to accept IPv6 router advertisements.

**MITRE ATT&CK Mappings:** M1042


Section #: 3.5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.1.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The Uncomplicated Firewall (ufw) is a frontend for iptables and is particularly well-suited for host-based firewalls. ufw provides a framework for managing netfilter, as well as a command-line interface for manipulating the firewall

**MITRE ATT&CK Mappings:** TA0011


Section #: 3.5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.1.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The Uncomplicated Firewall (ufw) is a frontend for iptables and is particularly well-suited for host-based firewalls. ufw provides a framework for managing netfilter, as well as a command-line interface for manipulating the firewall

**MITRE ATT&CK Mappings:** T1562


Section #: 3.5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.1.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The Uncomplicated Firewall (ufw) is a frontend for iptables and is particularly well-suited for host-based firewalls. ufw provides a framework for managing netfilter, as well as a command-line interface for manipulating the firewall

**MITRE ATT&CK Mappings:** T1562.004


Section #: 3.5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.1.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The Uncomplicated Firewall (ufw) is a frontend for iptables and is particularly well-suited for host-based firewalls. ufw provides a framework for managing netfilter, as well as a command-line interface for manipulating the firewall

**MITRE ATT&CK Mappings:** M1031


Section #: 3.5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.1.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The Uncomplicated Firewall (ufw) is a frontend for iptables and is particularly well-suited for host-based firewalls. ufw provides a framework for managing netfilter, as well as a command-line interface for manipulating the firewall

**MITRE ATT&CK Mappings:** M1037


Section #: 3.5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.1.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `iptables-persistent` is a boot-time loader for netfilter rules, iptables plugin

**MITRE ATT&CK Mappings:** TA0005


Section #: 3.5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.1.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `iptables-persistent` is a boot-time loader for netfilter rules, iptables plugin

**MITRE ATT&CK Mappings:** T1562


Section #: 3.5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.1.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `iptables-persistent` is a boot-time loader for netfilter rules, iptables plugin

**MITRE ATT&CK Mappings:** T1562.004


Section #: 3.5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.1.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** UncomplicatedFirewall (ufw) is a frontend for iptables. ufw provides a framework for managing netfilter, as well as a command-line and available graphical user interface for manipulating the firewall.

_Notes:_
- _When running ufw enable or starting ufw via its initscript, ufw will flush its chains. This is required so ufw can maintain a consistent state, but it may drop existing connections (eg ssh). ufw does support adding rules before enabling the firewall._
- _Run the following command before running `ufw enable`._
```
# ufw allow proto tcp from any to any port 22
```
- _The rules will still be flushed, but the ssh port will be open after enabling the firewall. Please note that once ufw is 'enabled', ufw will not flush the chains when adding or removing rules (but will when modifying a rule or changing the default policy)_
- _By default, ufw will prompt when enabling the firewall while running under ssh. This can be disabled by using `ufw --force enable`_

**MITRE ATT&CK Mappings:** TA0005


Section #: 3.5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.1.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** UncomplicatedFirewall (ufw) is a frontend for iptables. ufw provides a framework for managing netfilter, as well as a command-line and available graphical user interface for manipulating the firewall.

_Notes:_
- _When running ufw enable or starting ufw via its initscript, ufw will flush its chains. This is required so ufw can maintain a consistent state, but it may drop existing connections (eg ssh). ufw does support adding rules before enabling the firewall._
- _Run the following command before running `ufw enable`._
```
# ufw allow proto tcp from any to any port 22
```
- _The rules will still be flushed, but the ssh port will be open after enabling the firewall. Please note that once ufw is 'enabled', ufw will not flush the chains when adding or removing rules (but will when modifying a rule or changing the default policy)_
- _By default, ufw will prompt when enabling the firewall while running under ssh. This can be disabled by using `ufw --force enable`_

**MITRE ATT&CK Mappings:** T1562


Section #: 3.5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.1.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** UncomplicatedFirewall (ufw) is a frontend for iptables. ufw provides a framework for managing netfilter, as well as a command-line and available graphical user interface for manipulating the firewall.

_Notes:_
- _When running ufw enable or starting ufw via its initscript, ufw will flush its chains. This is required so ufw can maintain a consistent state, but it may drop existing connections (eg ssh). ufw does support adding rules before enabling the firewall._
- _Run the following command before running `ufw enable`._
```
# ufw allow proto tcp from any to any port 22
```
- _The rules will still be flushed, but the ssh port will be open after enabling the firewall. Please note that once ufw is 'enabled', ufw will not flush the chains when adding or removing rules (but will when modifying a rule or changing the default policy)_
- _By default, ufw will prompt when enabling the firewall while running under ssh. This can be disabled by using `ufw --force enable`_

**MITRE ATT&CK Mappings:** T1562.004


Section #: 3.5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.1.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Configure the loopback interface to accept traffic. Configure all other interfaces to deny traffic to the loopback network (127.0.0.0/8 for IPv4 and ::1/128 for IPv6).

**MITRE ATT&CK Mappings:** TA0011


Section #: 3.5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.1.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Configure the loopback interface to accept traffic. Configure all other interfaces to deny traffic to the loopback network (127.0.0.0/8 for IPv4 and ::1/128 for IPv6).

**MITRE ATT&CK Mappings:** T1562


Section #: 3.5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.1.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Configure the loopback interface to accept traffic. Configure all other interfaces to deny traffic to the loopback network (127.0.0.0/8 for IPv4 and ::1/128 for IPv6).

**MITRE ATT&CK Mappings:** T1562.004


Section #: 3.5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.1.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Configure the loopback interface to accept traffic. Configure all other interfaces to deny traffic to the loopback network (127.0.0.0/8 for IPv4 and ::1/128 for IPv6).

**MITRE ATT&CK Mappings:** M1031


Section #: 3.5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.1.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Configure the loopback interface to accept traffic. Configure all other interfaces to deny traffic to the loopback network (127.0.0.0/8 for IPv4 and ::1/128 for IPv6).

**MITRE ATT&CK Mappings:** M1037


Section #: 3.5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.1.5

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Configure the firewall rules for new outbound connections.

_Notes:_
- _Changing firewall settings while connected over network can result in being locked out of the system._
- _Unlike iptables, when a new outbound rule is added, ufw automatically takes care of associated established connections, so no rules for the latter kind are required._

**MITRE ATT&CK Mappings:** TA0011


Section #: 3.5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.1.5

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Configure the firewall rules for new outbound connections.

_Notes:_
- _Changing firewall settings while connected over network can result in being locked out of the system._
- _Unlike iptables, when a new outbound rule is added, ufw automatically takes care of associated established connections, so no rules for the latter kind are required._

**MITRE ATT&CK Mappings:** M1031


Section #: 3.5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.1.5

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Configure the firewall rules for new outbound connections.

_Notes:_
- _Changing firewall settings while connected over network can result in being locked out of the system._
- _Unlike iptables, when a new outbound rule is added, ufw automatically takes care of associated established connections, so no rules for the latter kind are required._

**MITRE ATT&CK Mappings:** M1037


Section #: 3.5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.1.6

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Any ports that have been opened on non-loopback addresses need firewall rules to govern traffic.

_Notes:_
- _Changing firewall settings while connected over network can result in being locked out of the system_
- _The remediation command opens up the port to traffic from all sources. Consult ufw documentation and set any restrictions in compliance with site policy_

**MITRE ATT&CK Mappings:** TA0011


Section #: 3.5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.1.6

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Any ports that have been opened on non-loopback addresses need firewall rules to govern traffic.

_Notes:_
- _Changing firewall settings while connected over network can result in being locked out of the system_
- _The remediation command opens up the port to traffic from all sources. Consult ufw documentation and set any restrictions in compliance with site policy_

**MITRE ATT&CK Mappings:** T1562


Section #: 3.5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.1.6

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Any ports that have been opened on non-loopback addresses need firewall rules to govern traffic.

_Notes:_
- _Changing firewall settings while connected over network can result in being locked out of the system_
- _The remediation command opens up the port to traffic from all sources. Consult ufw documentation and set any restrictions in compliance with site policy_

**MITRE ATT&CK Mappings:** T1562.004


Section #: 3.5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.1.6

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Any ports that have been opened on non-loopback addresses need firewall rules to govern traffic.

_Notes:_
- _Changing firewall settings while connected over network can result in being locked out of the system_
- _The remediation command opens up the port to traffic from all sources. Consult ufw documentation and set any restrictions in compliance with site policy_

**MITRE ATT&CK Mappings:** M1031


Section #: 3.5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.1.6

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Any ports that have been opened on non-loopback addresses need firewall rules to govern traffic.

_Notes:_
- _Changing firewall settings while connected over network can result in being locked out of the system_
- _The remediation command opens up the port to traffic from all sources. Consult ufw documentation and set any restrictions in compliance with site policy_

**MITRE ATT&CK Mappings:** M1037


Section #: 3.5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.1.7

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** A default deny policy on connections ensures that any unconfigured network usage will be rejected.

_Note: Any port or protocol without a explicit allow before the default deny will be blocked_

**MITRE ATT&CK Mappings:** TA0011


Section #: 3.5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.1.7

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** A default deny policy on connections ensures that any unconfigured network usage will be rejected.

_Note: Any port or protocol without a explicit allow before the default deny will be blocked_

**MITRE ATT&CK Mappings:** T1562


Section #: 3.5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.1.7

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** A default deny policy on connections ensures that any unconfigured network usage will be rejected.

_Note: Any port or protocol without a explicit allow before the default deny will be blocked_

**MITRE ATT&CK Mappings:** T1562.004


Section #: 3.5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.1.7

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** A default deny policy on connections ensures that any unconfigured network usage will be rejected.

_Note: Any port or protocol without a explicit allow before the default deny will be blocked_

**MITRE ATT&CK Mappings:** M1031


Section #: 3.5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.1.7

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** A default deny policy on connections ensures that any unconfigured network usage will be rejected.

_Note: Any port or protocol without a explicit allow before the default deny will be blocked_

**MITRE ATT&CK Mappings:** M1037


Section #: 3.5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.2.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** nftables provides a new in-kernel packet classification framework that is based on a network-specific Virtual Machine (VM) and a new nft userspace command line tool. nftables reuses the existing Netfilter subsystems such as the existing hook infrastructure, the connection tracking system, NAT, userspace queuing and logging subsystem.

_Notes:_
- _nftables is available in Linux kernel 3.13 and newer_
- _Only one firewall utility should be installed and configured_
- _Changing firewall settings while connected over the network can result in being locked out of the system_

**MITRE ATT&CK Mappings:** TA0011


Section #: 3.5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.2.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** nftables provides a new in-kernel packet classification framework that is based on a network-specific Virtual Machine (VM) and a new nft userspace command line tool. nftables reuses the existing Netfilter subsystems such as the existing hook infrastructure, the connection tracking system, NAT, userspace queuing and logging subsystem.

_Notes:_
- _nftables is available in Linux kernel 3.13 and newer_
- _Only one firewall utility should be installed and configured_
- _Changing firewall settings while connected over the network can result in being locked out of the system_

**MITRE ATT&CK Mappings:** T1562


Section #: 3.5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.2.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** nftables provides a new in-kernel packet classification framework that is based on a network-specific Virtual Machine (VM) and a new nft userspace command line tool. nftables reuses the existing Netfilter subsystems such as the existing hook infrastructure, the connection tracking system, NAT, userspace queuing and logging subsystem.

_Notes:_
- _nftables is available in Linux kernel 3.13 and newer_
- _Only one firewall utility should be installed and configured_
- _Changing firewall settings while connected over the network can result in being locked out of the system_

**MITRE ATT&CK Mappings:** T1562.004


Section #: 3.5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.2.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** nftables provides a new in-kernel packet classification framework that is based on a network-specific Virtual Machine (VM) and a new nft userspace command line tool. nftables reuses the existing Netfilter subsystems such as the existing hook infrastructure, the connection tracking system, NAT, userspace queuing and logging subsystem.

_Notes:_
- _nftables is available in Linux kernel 3.13 and newer_
- _Only one firewall utility should be installed and configured_
- _Changing firewall settings while connected over the network can result in being locked out of the system_

**MITRE ATT&CK Mappings:** M1031


Section #: 3.5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.2.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** nftables provides a new in-kernel packet classification framework that is based on a network-specific Virtual Machine (VM) and a new nft userspace command line tool. nftables reuses the existing Netfilter subsystems such as the existing hook infrastructure, the connection tracking system, NAT, userspace queuing and logging subsystem.

_Notes:_
- _nftables is available in Linux kernel 3.13 and newer_
- _Only one firewall utility should be installed and configured_
- _Changing firewall settings while connected over the network can result in being locked out of the system_

**MITRE ATT&CK Mappings:** M1037


Section #: 3.5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.2.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Uncomplicated Firewall (UFW) is a program for managing a netfilter firewall designed to be easy to use.

**MITRE ATT&CK Mappings:** TA0005


Section #: 3.5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.2.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Uncomplicated Firewall (UFW) is a program for managing a netfilter firewall designed to be easy to use.

**MITRE ATT&CK Mappings:** T1562


Section #: 3.5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.2.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Uncomplicated Firewall (UFW) is a program for managing a netfilter firewall designed to be easy to use.

**MITRE ATT&CK Mappings:** T1562.004


Section #: 3.5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.2.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** nftables is a replacement for iptables, ip6tables, ebtables and arptables

**MITRE ATT&CK Mappings:** TA0005


Section #: 3.5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.2.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** nftables is a replacement for iptables, ip6tables, ebtables and arptables

**MITRE ATT&CK Mappings:** T1562


Section #: 3.5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.2.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** nftables is a replacement for iptables, ip6tables, ebtables and arptables

**MITRE ATT&CK Mappings:** T1562.004


Section #: 3.5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.2.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Tables hold chains. Each table only has one address family and only applies to packets of this family. Tables can have one of five families.

**MITRE ATT&CK Mappings:** TA0011


Section #: 3.5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.2.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Tables hold chains. Each table only has one address family and only applies to packets of this family. Tables can have one of five families.

**MITRE ATT&CK Mappings:** T1562


Section #: 3.5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.2.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Tables hold chains. Each table only has one address family and only applies to packets of this family. Tables can have one of five families.

**MITRE ATT&CK Mappings:** T1562.004


Section #: 3.5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.2.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Tables hold chains. Each table only has one address family and only applies to packets of this family. Tables can have one of five families.

**MITRE ATT&CK Mappings:** M1031


Section #: 3.5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.2.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Tables hold chains. Each table only has one address family and only applies to packets of this family. Tables can have one of five families.

**MITRE ATT&CK Mappings:** M1037


Section #: 3.5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.2.5

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Chains are containers for rules. They exist in two kinds, base chains and regular chains. A base chain is an entry point for packets from the networking stack, a regular chain may be used as jump target and is used for better rule organization.

**MITRE ATT&CK Mappings:** TA0005


Section #: 3.5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.2.5

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Chains are containers for rules. They exist in two kinds, base chains and regular chains. A base chain is an entry point for packets from the networking stack, a regular chain may be used as jump target and is used for better rule organization.

**MITRE ATT&CK Mappings:** T1562


Section #: 3.5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.2.5

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Chains are containers for rules. They exist in two kinds, base chains and regular chains. A base chain is an entry point for packets from the networking stack, a regular chain may be used as jump target and is used for better rule organization.

**MITRE ATT&CK Mappings:** T1562.004


Section #: 3.5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.2.6

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Configure the loopback interface to accept traffic. Configure all other interfaces to deny traffic to the loopback network

**MITRE ATT&CK Mappings:** TA0005


Section #: 3.5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.2.6

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Configure the loopback interface to accept traffic. Configure all other interfaces to deny traffic to the loopback network

**MITRE ATT&CK Mappings:** T1562


Section #: 3.5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.2.6

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Configure the loopback interface to accept traffic. Configure all other interfaces to deny traffic to the loopback network

**MITRE ATT&CK Mappings:** T1562.004


Section #: 3.5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.2.7

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Configure the firewall rules for new outbound, and established connections

**MITRE ATT&CK Mappings:** TA0011


Section #: 3.5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.2.7

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Configure the firewall rules for new outbound, and established connections

**MITRE ATT&CK Mappings:** T1562


Section #: 3.5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.2.7

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Configure the firewall rules for new outbound, and established connections

**MITRE ATT&CK Mappings:** M1031


Section #: 3.5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.2.7

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Configure the firewall rules for new outbound, and established connections

**MITRE ATT&CK Mappings:** M1037


Section #: 3.5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.2.8

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Base chain policy is the default verdict that will be applied to packets reaching the end of the chain.

**MITRE ATT&CK Mappings:** TA0011


Section #: 3.5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.2.8

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Base chain policy is the default verdict that will be applied to packets reaching the end of the chain.

**MITRE ATT&CK Mappings:** T1562


Section #: 3.5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.2.8

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Base chain policy is the default verdict that will be applied to packets reaching the end of the chain.

**MITRE ATT&CK Mappings:** T1562.004


Section #: 3.5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.2.8

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Base chain policy is the default verdict that will be applied to packets reaching the end of the chain.

**MITRE ATT&CK Mappings:** M1031


Section #: 3.5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.2.8

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Base chain policy is the default verdict that will be applied to packets reaching the end of the chain.

**MITRE ATT&CK Mappings:** M1037


Section #: 3.5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.2.9

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The nftables service allows for the loading of nftables rulesets during boot, or starting on the nftables service

**MITRE ATT&CK Mappings:** TA0011


Section #: 3.5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.2.9

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The nftables service allows for the loading of nftables rulesets during boot, or starting on the nftables service

**MITRE ATT&CK Mappings:** T1562


Section #: 3.5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.2.9

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The nftables service allows for the loading of nftables rulesets during boot, or starting on the nftables service

**MITRE ATT&CK Mappings:** T1562.004


Section #: 3.5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.2.9

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The nftables service allows for the loading of nftables rulesets during boot, or starting on the nftables service

**MITRE ATT&CK Mappings:** M1031


Section #: 3.5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.2.9

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The nftables service allows for the loading of nftables rulesets during boot, or starting on the nftables service

**MITRE ATT&CK Mappings:** M1037


Section #: 3.5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.2.10

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** nftables is a subsystem of the Linux kernel providing filtering and classification of network packets/datagrams/frames.

The nftables service reads the `/etc/nftables.conf` file for a nftables file or files to include in the nftables ruleset.

A nftables ruleset containing the input, forward, and output base chains allow network traffic to be filtered.

**MITRE ATT&CK Mappings:** TA0011


Section #: 3.5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.2.10

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** nftables is a subsystem of the Linux kernel providing filtering and classification of network packets/datagrams/frames.

The nftables service reads the `/etc/nftables.conf` file for a nftables file or files to include in the nftables ruleset.

A nftables ruleset containing the input, forward, and output base chains allow network traffic to be filtered.

**MITRE ATT&CK Mappings:** T1562


Section #: 3.5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.2.10

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** nftables is a subsystem of the Linux kernel providing filtering and classification of network packets/datagrams/frames.

The nftables service reads the `/etc/nftables.conf` file for a nftables file or files to include in the nftables ruleset.

A nftables ruleset containing the input, forward, and output base chains allow network traffic to be filtered.

**MITRE ATT&CK Mappings:** T1562.004


Section #: 3.5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.2.10

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** nftables is a subsystem of the Linux kernel providing filtering and classification of network packets/datagrams/frames.

The nftables service reads the `/etc/nftables.conf` file for a nftables file or files to include in the nftables ruleset.

A nftables ruleset containing the input, forward, and output base chains allow network traffic to be filtered.

**MITRE ATT&CK Mappings:** M1031


Section #: 3.5.3.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.3.1.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** iptables is a utility program that allows a system administrator to configure the tables provided by the Linux kernel firewall, implemented as different Netfilter modules, and the chains and rules it stores. Different kernel modules and programs are used for different protocols; iptables applies to IPv4, ip6tables to IPv6, arptables to ARP, and ebtables to Ethernet frames.

**MITRE ATT&CK Mappings:** TA0011


Section #: 3.5.3.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.3.1.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** iptables is a utility program that allows a system administrator to configure the tables provided by the Linux kernel firewall, implemented as different Netfilter modules, and the chains and rules it stores. Different kernel modules and programs are used for different protocols; iptables applies to IPv4, ip6tables to IPv6, arptables to ARP, and ebtables to Ethernet frames.

**MITRE ATT&CK Mappings:** T1562


Section #: 3.5.3.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.3.1.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** iptables is a utility program that allows a system administrator to configure the tables provided by the Linux kernel firewall, implemented as different Netfilter modules, and the chains and rules it stores. Different kernel modules and programs are used for different protocols; iptables applies to IPv4, ip6tables to IPv6, arptables to ARP, and ebtables to Ethernet frames.

**MITRE ATT&CK Mappings:** T1562.004


Section #: 3.5.3.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.3.1.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** iptables is a utility program that allows a system administrator to configure the tables provided by the Linux kernel firewall, implemented as different Netfilter modules, and the chains and rules it stores. Different kernel modules and programs are used for different protocols; iptables applies to IPv4, ip6tables to IPv6, arptables to ARP, and ebtables to Ethernet frames.

**MITRE ATT&CK Mappings:** M1031


Section #: 3.5.3.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.3.1.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** iptables is a utility program that allows a system administrator to configure the tables provided by the Linux kernel firewall, implemented as different Netfilter modules, and the chains and rules it stores. Different kernel modules and programs are used for different protocols; iptables applies to IPv4, ip6tables to IPv6, arptables to ARP, and ebtables to Ethernet frames.

**MITRE ATT&CK Mappings:** M1037


Section #: 3.5.3.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.3.1.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** nftables is a subsystem of the Linux kernel providing filtering and classification of network packets/datagrams/frames and is the successor to iptables.

**MITRE ATT&CK Mappings:** TA0011


Section #: 3.5.3.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.3.1.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** nftables is a subsystem of the Linux kernel providing filtering and classification of network packets/datagrams/frames and is the successor to iptables.

**MITRE ATT&CK Mappings:** T1562


Section #: 3.5.3.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.3.1.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** nftables is a subsystem of the Linux kernel providing filtering and classification of network packets/datagrams/frames and is the successor to iptables.

**MITRE ATT&CK Mappings:** T1562.004


Section #: 3.5.3.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.3.1.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Uncomplicated Firewall (UFW) is a program for managing a netfilter firewall designed to be easy to use.
- Uses a command-line interface consisting of a small number of simple commands
- Uses iptables for configuration

**MITRE ATT&CK Mappings:** TA0011


Section #: 3.5.3.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.3.1.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Uncomplicated Firewall (UFW) is a program for managing a netfilter firewall designed to be easy to use.
- Uses a command-line interface consisting of a small number of simple commands
- Uses iptables for configuration

**MITRE ATT&CK Mappings:** T1562


Section #: 3.5.3.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.3.1.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Uncomplicated Firewall (UFW) is a program for managing a netfilter firewall designed to be easy to use.
- Uses a command-line interface consisting of a small number of simple commands
- Uses iptables for configuration

**MITRE ATT&CK Mappings:** T1562.004


Section #: 3.5.3.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.3.2.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** A default deny all policy on connections ensures that any unconfigured network usage will be rejected.

_Notes:_
- _Changing firewall settings while connected over network can result in being locked out of the system_
- _Remediation will only affect the active system firewall, be sure to configure the default policy in your firewall management to apply on boot as well_

**MITRE ATT&CK Mappings:** TA0011


Section #: 3.5.3.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.3.2.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** A default deny all policy on connections ensures that any unconfigured network usage will be rejected.

_Notes:_
- _Changing firewall settings while connected over network can result in being locked out of the system_
- _Remediation will only affect the active system firewall, be sure to configure the default policy in your firewall management to apply on boot as well_

**MITRE ATT&CK Mappings:** T1562


Section #: 3.5.3.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.3.2.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** A default deny all policy on connections ensures that any unconfigured network usage will be rejected.

_Notes:_
- _Changing firewall settings while connected over network can result in being locked out of the system_
- _Remediation will only affect the active system firewall, be sure to configure the default policy in your firewall management to apply on boot as well_

**MITRE ATT&CK Mappings:** T1562.004


Section #: 3.5.3.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.3.2.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** A default deny all policy on connections ensures that any unconfigured network usage will be rejected.

_Notes:_
- _Changing firewall settings while connected over network can result in being locked out of the system_
- _Remediation will only affect the active system firewall, be sure to configure the default policy in your firewall management to apply on boot as well_

**MITRE ATT&CK Mappings:** M1031


Section #: 3.5.3.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.3.2.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** A default deny all policy on connections ensures that any unconfigured network usage will be rejected.

_Notes:_
- _Changing firewall settings while connected over network can result in being locked out of the system_
- _Remediation will only affect the active system firewall, be sure to configure the default policy in your firewall management to apply on boot as well_

**MITRE ATT&CK Mappings:** M1037


Section #: 3.5.3.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.3.2.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Configure the loopback interface to accept traffic. Configure all other interfaces to deny traffic to the loopback network (127.0.0.0/8).

_Notes:_
- _Changing firewall settings while connected over network can result in being locked out of the system_
- _Remediation will only affect the active system firewall, be sure to configure the default policy in your firewall management to apply on boot as well_

**MITRE ATT&CK Mappings:** TA0011


Section #: 3.5.3.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.3.2.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Configure the loopback interface to accept traffic. Configure all other interfaces to deny traffic to the loopback network (127.0.0.0/8).

_Notes:_
- _Changing firewall settings while connected over network can result in being locked out of the system_
- _Remediation will only affect the active system firewall, be sure to configure the default policy in your firewall management to apply on boot as well_

**MITRE ATT&CK Mappings:** T1562


Section #: 3.5.3.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.3.2.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Configure the loopback interface to accept traffic. Configure all other interfaces to deny traffic to the loopback network (127.0.0.0/8).

_Notes:_
- _Changing firewall settings while connected over network can result in being locked out of the system_
- _Remediation will only affect the active system firewall, be sure to configure the default policy in your firewall management to apply on boot as well_

**MITRE ATT&CK Mappings:** T1562.004


Section #: 3.5.3.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.3.2.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Configure the loopback interface to accept traffic. Configure all other interfaces to deny traffic to the loopback network (127.0.0.0/8).

_Notes:_
- _Changing firewall settings while connected over network can result in being locked out of the system_
- _Remediation will only affect the active system firewall, be sure to configure the default policy in your firewall management to apply on boot as well_

**MITRE ATT&CK Mappings:** M1031


Section #: 3.5.3.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.3.2.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Configure the loopback interface to accept traffic. Configure all other interfaces to deny traffic to the loopback network (127.0.0.0/8).

_Notes:_
- _Changing firewall settings while connected over network can result in being locked out of the system_
- _Remediation will only affect the active system firewall, be sure to configure the default policy in your firewall management to apply on boot as well_

**MITRE ATT&CK Mappings:** M1037


Section #: 3.5.3.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.3.2.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Configure the firewall rules for new outbound, and established connections.

_Notes:_
- _Changing firewall settings while connected over network can result in being locked out of the system_
- _Remediation will only affect the active system firewall, be sure to configure the default policy in your firewall management to apply on boot as well_

**MITRE ATT&CK Mappings:** TA0011


Section #: 3.5.3.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.3.2.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Configure the firewall rules for new outbound, and established connections.

_Notes:_
- _Changing firewall settings while connected over network can result in being locked out of the system_
- _Remediation will only affect the active system firewall, be sure to configure the default policy in your firewall management to apply on boot as well_

**MITRE ATT&CK Mappings:** M1031


Section #: 3.5.3.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.3.2.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Configure the firewall rules for new outbound, and established connections.

_Notes:_
- _Changing firewall settings while connected over network can result in being locked out of the system_
- _Remediation will only affect the active system firewall, be sure to configure the default policy in your firewall management to apply on boot as well_

**MITRE ATT&CK Mappings:** M1037


Section #: 3.5.3.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.3.2.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Any ports that have been opened on non-loopback addresses need firewall rules to govern traffic.

**Note:**
- Changing firewall settings while connected over network can result in being locked out of the system
- Remediation will only affect the active system firewall, be sure to configure the default policy in your firewall management to apply on boot as well
- The remediation command opens up the port to traffic from all sources. Consult iptables documentation and set any restrictions in compliance with site policy

**MITRE ATT&CK Mappings:** TA0011


Section #: 3.5.3.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.3.2.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Any ports that have been opened on non-loopback addresses need firewall rules to govern traffic.

**Note:**
- Changing firewall settings while connected over network can result in being locked out of the system
- Remediation will only affect the active system firewall, be sure to configure the default policy in your firewall management to apply on boot as well
- The remediation command opens up the port to traffic from all sources. Consult iptables documentation and set any restrictions in compliance with site policy

**MITRE ATT&CK Mappings:** T1562


Section #: 3.5.3.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.3.2.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Any ports that have been opened on non-loopback addresses need firewall rules to govern traffic.

**Note:**
- Changing firewall settings while connected over network can result in being locked out of the system
- Remediation will only affect the active system firewall, be sure to configure the default policy in your firewall management to apply on boot as well
- The remediation command opens up the port to traffic from all sources. Consult iptables documentation and set any restrictions in compliance with site policy

**MITRE ATT&CK Mappings:** T1562.004


Section #: 3.5.3.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.3.2.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Any ports that have been opened on non-loopback addresses need firewall rules to govern traffic.

**Note:**
- Changing firewall settings while connected over network can result in being locked out of the system
- Remediation will only affect the active system firewall, be sure to configure the default policy in your firewall management to apply on boot as well
- The remediation command opens up the port to traffic from all sources. Consult iptables documentation and set any restrictions in compliance with site policy

**MITRE ATT&CK Mappings:** M1031


Section #: 3.5.3.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.3.2.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Any ports that have been opened on non-loopback addresses need firewall rules to govern traffic.

**Note:**
- Changing firewall settings while connected over network can result in being locked out of the system
- Remediation will only affect the active system firewall, be sure to configure the default policy in your firewall management to apply on boot as well
- The remediation command opens up the port to traffic from all sources. Consult iptables documentation and set any restrictions in compliance with site policy

**MITRE ATT&CK Mappings:** M1037


Section #: 3.5.3.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.3.3.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** A default deny all policy on connections ensures that any unconfigured network usage will be rejected.

**Note:**
- Changing firewall settings while connected over network can result in being locked out of the system
- Remediation will only affect the active system firewall, be sure to configure the default policy in your firewall management to apply on boot as well

**MITRE ATT&CK Mappings:** TA0011


Section #: 3.5.3.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.3.3.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** A default deny all policy on connections ensures that any unconfigured network usage will be rejected.

**Note:**
- Changing firewall settings while connected over network can result in being locked out of the system
- Remediation will only affect the active system firewall, be sure to configure the default policy in your firewall management to apply on boot as well

**MITRE ATT&CK Mappings:** T1562


Section #: 3.5.3.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.3.3.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** A default deny all policy on connections ensures that any unconfigured network usage will be rejected.

**Note:**
- Changing firewall settings while connected over network can result in being locked out of the system
- Remediation will only affect the active system firewall, be sure to configure the default policy in your firewall management to apply on boot as well

**MITRE ATT&CK Mappings:** T1562.004


Section #: 3.5.3.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.3.3.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** A default deny all policy on connections ensures that any unconfigured network usage will be rejected.

**Note:**
- Changing firewall settings while connected over network can result in being locked out of the system
- Remediation will only affect the active system firewall, be sure to configure the default policy in your firewall management to apply on boot as well

**MITRE ATT&CK Mappings:** M1031


Section #: 3.5.3.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.3.3.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** A default deny all policy on connections ensures that any unconfigured network usage will be rejected.

**Note:**
- Changing firewall settings while connected over network can result in being locked out of the system
- Remediation will only affect the active system firewall, be sure to configure the default policy in your firewall management to apply on boot as well

**MITRE ATT&CK Mappings:** M1037


Section #: 3.5.3.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.3.3.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Configure the loopback interface to accept traffic. Configure all other interfaces to deny traffic to the loopback network (::1).

**Note:**
- Changing firewall settings while connected over network can result in being locked out of the system
- Remediation will only affect the active system firewall, be sure to configure the default policy in your firewall management to apply on boot as well

**MITRE ATT&CK Mappings:** TA0011


Section #: 3.5.3.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.3.3.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Configure the loopback interface to accept traffic. Configure all other interfaces to deny traffic to the loopback network (::1).

**Note:**
- Changing firewall settings while connected over network can result in being locked out of the system
- Remediation will only affect the active system firewall, be sure to configure the default policy in your firewall management to apply on boot as well

**MITRE ATT&CK Mappings:** T1562


Section #: 3.5.3.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.3.3.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Configure the loopback interface to accept traffic. Configure all other interfaces to deny traffic to the loopback network (::1).

**Note:**
- Changing firewall settings while connected over network can result in being locked out of the system
- Remediation will only affect the active system firewall, be sure to configure the default policy in your firewall management to apply on boot as well

**MITRE ATT&CK Mappings:** T1562.004


Section #: 3.5.3.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.3.3.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Configure the loopback interface to accept traffic. Configure all other interfaces to deny traffic to the loopback network (::1).

**Note:**
- Changing firewall settings while connected over network can result in being locked out of the system
- Remediation will only affect the active system firewall, be sure to configure the default policy in your firewall management to apply on boot as well

**MITRE ATT&CK Mappings:** M1031


Section #: 3.5.3.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.3.3.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Configure the loopback interface to accept traffic. Configure all other interfaces to deny traffic to the loopback network (::1).

**Note:**
- Changing firewall settings while connected over network can result in being locked out of the system
- Remediation will only affect the active system firewall, be sure to configure the default policy in your firewall management to apply on boot as well

**MITRE ATT&CK Mappings:** M1037


Section #: 3.5.3.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.3.3.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Configure the firewall rules for new outbound, and established IPv6 connections.

**Note:**
- Changing firewall settings while connected over network can result in being locked out of the system
- Remediation will only affect the active system firewall, be sure to configure the default policy in your firewall management to apply on boot as well

**MITRE ATT&CK Mappings:** TA0011


Section #: 3.5.3.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.3.3.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Configure the firewall rules for new outbound, and established IPv6 connections.

**Note:**
- Changing firewall settings while connected over network can result in being locked out of the system
- Remediation will only affect the active system firewall, be sure to configure the default policy in your firewall management to apply on boot as well

**MITRE ATT&CK Mappings:** M1031


Section #: 3.5.3.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.3.3.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Configure the firewall rules for new outbound, and established IPv6 connections.

**Note:**
- Changing firewall settings while connected over network can result in being locked out of the system
- Remediation will only affect the active system firewall, be sure to configure the default policy in your firewall management to apply on boot as well

**MITRE ATT&CK Mappings:** M1037


Section #: 3.5.3.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.3.3.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Any ports that have been opened on non-loopback addresses need firewall rules to govern traffic.

**Note:**
- Changing firewall settings while connected over network can result in being locked out of the system
- Remediation will only affect the active system firewall, be sure to configure the default policy in your firewall management to apply on boot as well
- The remediation command opens up the port to traffic from all sources. Consult iptables documentation and set any restrictions in compliance with site policy

**MITRE ATT&CK Mappings:** TA0011


Section #: 3.5.3.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.3.3.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Any ports that have been opened on non-loopback addresses need firewall rules to govern traffic.

**Note:**
- Changing firewall settings while connected over network can result in being locked out of the system
- Remediation will only affect the active system firewall, be sure to configure the default policy in your firewall management to apply on boot as well
- The remediation command opens up the port to traffic from all sources. Consult iptables documentation and set any restrictions in compliance with site policy

**MITRE ATT&CK Mappings:** T1562


Section #: 3.5.3.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.3.3.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Any ports that have been opened on non-loopback addresses need firewall rules to govern traffic.

**Note:**
- Changing firewall settings while connected over network can result in being locked out of the system
- Remediation will only affect the active system firewall, be sure to configure the default policy in your firewall management to apply on boot as well
- The remediation command opens up the port to traffic from all sources. Consult iptables documentation and set any restrictions in compliance with site policy

**MITRE ATT&CK Mappings:** T1562.004


Section #: 3.5.3.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.3.3.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Any ports that have been opened on non-loopback addresses need firewall rules to govern traffic.

**Note:**
- Changing firewall settings while connected over network can result in being locked out of the system
- Remediation will only affect the active system firewall, be sure to configure the default policy in your firewall management to apply on boot as well
- The remediation command opens up the port to traffic from all sources. Consult iptables documentation and set any restrictions in compliance with site policy

**MITRE ATT&CK Mappings:** M1031


Section #: 3.5.3.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.5.3.3.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Any ports that have been opened on non-loopback addresses need firewall rules to govern traffic.

**Note:**
- Changing firewall settings while connected over network can result in being locked out of the system
- Remediation will only affect the active system firewall, be sure to configure the default policy in your firewall management to apply on boot as well
- The remediation command opens up the port to traffic from all sources. Consult iptables documentation and set any restrictions in compliance with site policy

**MITRE ATT&CK Mappings:** M1037


Section #: 4.1.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.1.1

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** auditd is the userspace component to the Linux Auditing System. It's responsible for writing audit records to the disk

**MITRE ATT&CK Mappings:** TA0005


Section #: 4.1.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.1.1

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** auditd is the userspace component to the Linux Auditing System. It's responsible for writing audit records to the disk

**MITRE ATT&CK Mappings:** T1562


Section #: 4.1.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.1.1

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** auditd is the userspace component to the Linux Auditing System. It's responsible for writing audit records to the disk

**MITRE ATT&CK Mappings:** T1562.001


Section #: 4.1.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.1.2

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Turn on the `auditd` daemon to record system events.

**MITRE ATT&CK Mappings:** TA0005


Section #: 4.1.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.1.2

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Turn on the `auditd` daemon to record system events.

**MITRE ATT&CK Mappings:** T1562


Section #: 4.1.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.1.2

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Turn on the `auditd` daemon to record system events.

**MITRE ATT&CK Mappings:** T1562.001


Section #: 4.1.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.1.3

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Configure `grub2` so that processes that are capable of being audited can be audited even if they start up prior to `auditd` startup.

**MITRE ATT&CK Mappings:** TA0005


Section #: 4.1.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.1.3

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Configure `grub2` so that processes that are capable of being audited can be audited even if they start up prior to `auditd` startup.

**MITRE ATT&CK Mappings:** T1562


Section #: 4.1.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.1.3

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Configure `grub2` so that processes that are capable of being audited can be audited even if they start up prior to `auditd` startup.

**MITRE ATT&CK Mappings:** T1562.001


Section #: 4.1.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.1.4

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** In the kernel-level audit subsystem, a socket buffer queue is used to hold audit events. Whenever a new audit event is received, it is logged and prepared to be added to this queue. 

The kernel boot parameter `audit_backlog_limit=N`, with `N` representing the amount of messages, will ensure that a queue cannot grow beyond a certain size. If an audit event is logged which would grow the queue beyond this limit, then a failure occurs and is handled according to the system configuration

**MITRE ATT&CK Mappings:** TA0005


Section #: 4.1.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.1.4

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** In the kernel-level audit subsystem, a socket buffer queue is used to hold audit events. Whenever a new audit event is received, it is logged and prepared to be added to this queue. 

The kernel boot parameter `audit_backlog_limit=N`, with `N` representing the amount of messages, will ensure that a queue cannot grow beyond a certain size. If an audit event is logged which would grow the queue beyond this limit, then a failure occurs and is handled according to the system configuration

**MITRE ATT&CK Mappings:** T1562


Section #: 4.1.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.1.4

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** In the kernel-level audit subsystem, a socket buffer queue is used to hold audit events. Whenever a new audit event is received, it is logged and prepared to be added to this queue. 

The kernel boot parameter `audit_backlog_limit=N`, with `N` representing the amount of messages, will ensure that a queue cannot grow beyond a certain size. If an audit event is logged which would grow the queue beyond this limit, then a failure occurs and is handled according to the system configuration

**MITRE ATT&CK Mappings:** T1562.001


Section #: 4.1.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.2.1

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Configure the maximum size of the audit log file. Once the log reaches the maximum size, it will be rotated and a new log file will be started.

**MITRE ATT&CK Mappings:** TA0040


Section #: 4.1.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.2.1

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Configure the maximum size of the audit log file. Once the log reaches the maximum size, it will be rotated and a new log file will be started.

**MITRE ATT&CK Mappings:** T1562


Section #: 4.1.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.2.1

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Configure the maximum size of the audit log file. Once the log reaches the maximum size, it will be rotated and a new log file will be started.

**MITRE ATT&CK Mappings:** T1562.006


Section #: 4.1.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.2.1

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Configure the maximum size of the audit log file. Once the log reaches the maximum size, it will be rotated and a new log file will be started.

**MITRE ATT&CK Mappings:** M1053


Section #: 4.1.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.2.2

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** The `max_log_file_action` setting determines how to handle the audit log file reaching the max file size. A value of `keep_logs` will rotate the logs but never delete old logs.

**MITRE ATT&CK Mappings:** TA0005


Section #: 4.1.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.2.2

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** The `max_log_file_action` setting determines how to handle the audit log file reaching the max file size. A value of `keep_logs` will rotate the logs but never delete old logs.

**MITRE ATT&CK Mappings:** T1562


Section #: 4.1.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.2.2

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** The `max_log_file_action` setting determines how to handle the audit log file reaching the max file size. A value of `keep_logs` will rotate the logs but never delete old logs.

**MITRE ATT&CK Mappings:** T1562.006


Section #: 4.1.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.2.2

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** The `max_log_file_action` setting determines how to handle the audit log file reaching the max file size. A value of `keep_logs` will rotate the logs but never delete old logs.

**MITRE ATT&CK Mappings:** M1053


Section #: 4.1.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.2.3

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** The `auditd` daemon can be configured to halt the system when the audit logs are full.

The admin_space_left_action parameter tells the system what action to take when the system has detected that it is low on disk space. Valid values are ignore, syslog, suspend, single, and halt.
- `ignore`, the audit daemon does nothing
- `Syslog`, the audit daemon will issue a warning to syslog
- `Suspend`, the audit daemon will stop writing records to the disk
- `single`, the audit daemon will put the computer system in single user mode
- `halt`, the audit daemon will shutdown the system

**MITRE ATT&CK Mappings:** TA0005


Section #: 4.1.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.2.3

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** The `auditd` daemon can be configured to halt the system when the audit logs are full.

The admin_space_left_action parameter tells the system what action to take when the system has detected that it is low on disk space. Valid values are ignore, syslog, suspend, single, and halt.
- `ignore`, the audit daemon does nothing
- `Syslog`, the audit daemon will issue a warning to syslog
- `Suspend`, the audit daemon will stop writing records to the disk
- `single`, the audit daemon will put the computer system in single user mode
- `halt`, the audit daemon will shutdown the system

**MITRE ATT&CK Mappings:** T1562


Section #: 4.1.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.2.3

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** The `auditd` daemon can be configured to halt the system when the audit logs are full.

The admin_space_left_action parameter tells the system what action to take when the system has detected that it is low on disk space. Valid values are ignore, syslog, suspend, single, and halt.
- `ignore`, the audit daemon does nothing
- `Syslog`, the audit daemon will issue a warning to syslog
- `Suspend`, the audit daemon will stop writing records to the disk
- `single`, the audit daemon will put the computer system in single user mode
- `halt`, the audit daemon will shutdown the system

**MITRE ATT&CK Mappings:** T1562.006


Section #: 4.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.3.1

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Monitor scope changes for system administrators. If the system has been properly configured to force system administrators to log in as themselves first and then use the `sudo` command to execute privileged commands, it is possible to monitor changes in scope. The file `/etc/sudoers`, or files in `/etc/sudoers.d`, will be written to when the file(s) or related attributes have changed. The audit records will be tagged with the identifier "scope".

**MITRE ATT&CK Mappings:** T1562


Section #: 4.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.3.1

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Monitor scope changes for system administrators. If the system has been properly configured to force system administrators to log in as themselves first and then use the `sudo` command to execute privileged commands, it is possible to monitor changes in scope. The file `/etc/sudoers`, or files in `/etc/sudoers.d`, will be written to when the file(s) or related attributes have changed. The audit records will be tagged with the identifier "scope".

**MITRE ATT&CK Mappings:** T1562.006


Section #: 4.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.3.1

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Monitor scope changes for system administrators. If the system has been properly configured to force system administrators to log in as themselves first and then use the `sudo` command to execute privileged commands, it is possible to monitor changes in scope. The file `/etc/sudoers`, or files in `/etc/sudoers.d`, will be written to when the file(s) or related attributes have changed. The audit records will be tagged with the identifier "scope".

**MITRE ATT&CK Mappings:** TA0004


Section #: 4.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.3.1

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Monitor scope changes for system administrators. If the system has been properly configured to force system administrators to log in as themselves first and then use the `sudo` command to execute privileged commands, it is possible to monitor changes in scope. The file `/etc/sudoers`, or files in `/etc/sudoers.d`, will be written to when the file(s) or related attributes have changed. The audit records will be tagged with the identifier "scope".

**MITRE ATT&CK Mappings:** M1047


Section #: 4.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.3.2

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** `sudo` provides users with temporary elevated privileges to perform operations, either as the superuser or another user.

**MITRE ATT&CK Mappings:** T1562


Section #: 4.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.3.2

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** `sudo` provides users with temporary elevated privileges to perform operations, either as the superuser or another user.

**MITRE ATT&CK Mappings:** T1562.006


Section #: 4.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.3.2

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** `sudo` provides users with temporary elevated privileges to perform operations, either as the superuser or another user.

**MITRE ATT&CK Mappings:** TA0004


Section #: 4.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.3.2

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** `sudo` provides users with temporary elevated privileges to perform operations, either as the superuser or another user.

**MITRE ATT&CK Mappings:** M1047


Section #: 4.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.3.4

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Capture events where the system date and/or time has been modified. The parameters in this section are set to determine if the;
- `adjtimex` - tune kernel clock
- `settimeofday` - set time using `timeval` and `timezone` structures
- `stime` - using seconds since 1/1/1970
- `clock_settime` - allows for the setting of several internal clocks and timers

system calls have been executed. Further, ensure to write an audit record to the configured audit log file upon exit, tagging the records with a unique identifier such as "time-change".

**MITRE ATT&CK Mappings:** T1562


Section #: 4.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.3.4

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Capture events where the system date and/or time has been modified. The parameters in this section are set to determine if the;
- `adjtimex` - tune kernel clock
- `settimeofday` - set time using `timeval` and `timezone` structures
- `stime` - using seconds since 1/1/1970
- `clock_settime` - allows for the setting of several internal clocks and timers

system calls have been executed. Further, ensure to write an audit record to the configured audit log file upon exit, tagging the records with a unique identifier such as "time-change".

**MITRE ATT&CK Mappings:** T1562.006


Section #: 4.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.3.4

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Capture events where the system date and/or time has been modified. The parameters in this section are set to determine if the;
- `adjtimex` - tune kernel clock
- `settimeofday` - set time using `timeval` and `timezone` structures
- `stime` - using seconds since 1/1/1970
- `clock_settime` - allows for the setting of several internal clocks and timers

system calls have been executed. Further, ensure to write an audit record to the configured audit log file upon exit, tagging the records with a unique identifier such as "time-change".

**MITRE ATT&CK Mappings:** TA0005


Section #: 4.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.3.5

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Record changes to network environment files or system calls. The below parameters monitors the following system calls, and write an audit event on system call exit:
- `sethostname` - set the systems host name
- `setdomainname` - set the systems domain name

The files being monitored are:
- `/etc/issue` and `/etc/issue.net` - messages displayed pre-login
- `/etc/hosts` - file containing host names and associated IP addresses
- `/etc/networks` - symbolic names for networks
- `/etc/network/` - directory containing network interface scripts and configurations files

**MITRE ATT&CK Mappings:** T1562


Section #: 4.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.3.5

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Record changes to network environment files or system calls. The below parameters monitors the following system calls, and write an audit event on system call exit:
- `sethostname` - set the systems host name
- `setdomainname` - set the systems domain name

The files being monitored are:
- `/etc/issue` and `/etc/issue.net` - messages displayed pre-login
- `/etc/hosts` - file containing host names and associated IP addresses
- `/etc/networks` - symbolic names for networks
- `/etc/network/` - directory containing network interface scripts and configurations files

**MITRE ATT&CK Mappings:** T1562.006


Section #: 4.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.3.5

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Record changes to network environment files or system calls. The below parameters monitors the following system calls, and write an audit event on system call exit:
- `sethostname` - set the systems host name
- `setdomainname` - set the systems domain name

The files being monitored are:
- `/etc/issue` and `/etc/issue.net` - messages displayed pre-login
- `/etc/hosts` - file containing host names and associated IP addresses
- `/etc/networks` - symbolic names for networks
- `/etc/network/` - directory containing network interface scripts and configurations files

**MITRE ATT&CK Mappings:** M1047


Section #: 4.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.3.5

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Record changes to network environment files or system calls. The below parameters monitors the following system calls, and write an audit event on system call exit:
- `sethostname` - set the systems host name
- `setdomainname` - set the systems domain name

The files being monitored are:
- `/etc/issue` and `/etc/issue.net` - messages displayed pre-login
- `/etc/hosts` - file containing host names and associated IP addresses
- `/etc/networks` - symbolic names for networks
- `/etc/network/` - directory containing network interface scripts and configurations files

**MITRE ATT&CK Mappings:** TA0003


Section #: 4.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.3.6

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Monitor privileged programs, those that have the `setuid` and/or `setgid` bit set on execution, to determine if unprivileged users are running these commands.

**MITRE ATT&CK Mappings:** T1562


Section #: 4.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.3.6

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Monitor privileged programs, those that have the `setuid` and/or `setgid` bit set on execution, to determine if unprivileged users are running these commands.

**MITRE ATT&CK Mappings:** T1562.006


Section #: 4.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.3.6

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Monitor privileged programs, those that have the `setuid` and/or `setgid` bit set on execution, to determine if unprivileged users are running these commands.

**MITRE ATT&CK Mappings:** TA0002


Section #: 4.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.3.6

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Monitor privileged programs, those that have the `setuid` and/or `setgid` bit set on execution, to determine if unprivileged users are running these commands.

**MITRE ATT&CK Mappings:** M1026


Section #: 4.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.3.7

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Monitor for unsuccessful attempts to access files. The following parameters are associated with system calls that control files:
- creation - `creat`
- opening - `open` , `openat`
- truncation - `truncate` , `ftruncate`

An audit log record will only be written if all of the following criteria is met for the user when trying to access a file:
- a non-privileged user (auid>=UID_MIN)
- is not a Daemon event (auid=4294967295/unset/-1)
- if the system call returned EACCES (permission denied) or EPERM (some other permanent error associated with the specific system call)

**MITRE ATT&CK Mappings:** TA0007


Section #: 4.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.3.7

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Monitor for unsuccessful attempts to access files. The following parameters are associated with system calls that control files:
- creation - `creat`
- opening - `open` , `openat`
- truncation - `truncate` , `ftruncate`

An audit log record will only be written if all of the following criteria is met for the user when trying to access a file:
- a non-privileged user (auid>=UID_MIN)
- is not a Daemon event (auid=4294967295/unset/-1)
- if the system call returned EACCES (permission denied) or EPERM (some other permanent error associated with the specific system call)

**MITRE ATT&CK Mappings:** T1562


Section #: 4.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.3.7

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Monitor for unsuccessful attempts to access files. The following parameters are associated with system calls that control files:
- creation - `creat`
- opening - `open` , `openat`
- truncation - `truncate` , `ftruncate`

An audit log record will only be written if all of the following criteria is met for the user when trying to access a file:
- a non-privileged user (auid>=UID_MIN)
- is not a Daemon event (auid=4294967295/unset/-1)
- if the system call returned EACCES (permission denied) or EPERM (some other permanent error associated with the specific system call)

**MITRE ATT&CK Mappings:** T1562.006


Section #: 4.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.3.8

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Record events affecting the modification of user or group information, including that of passwords and old passwords if in use.
- `/etc/group` - system groups
- `/etc/passwd` - system users
- `/etc/gshadow` - encrypted password for each group
- `/etc/shadow` - system user passwords
- `/etc/security/opasswd` - storage of old passwords if the relevant PAM module is in use

The parameters in this section will watch the files to see if they have been opened for write or have had attribute changes (e.g. permissions) and tag them with the identifier "identity" in the audit log file.

**MITRE ATT&CK Mappings:** T1562


Section #: 4.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.3.8

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Record events affecting the modification of user or group information, including that of passwords and old passwords if in use.
- `/etc/group` - system groups
- `/etc/passwd` - system users
- `/etc/gshadow` - encrypted password for each group
- `/etc/shadow` - system user passwords
- `/etc/security/opasswd` - storage of old passwords if the relevant PAM module is in use

The parameters in this section will watch the files to see if they have been opened for write or have had attribute changes (e.g. permissions) and tag them with the identifier "identity" in the audit log file.

**MITRE ATT&CK Mappings:** T1562.006


Section #: 4.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.3.8

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Record events affecting the modification of user or group information, including that of passwords and old passwords if in use.
- `/etc/group` - system groups
- `/etc/passwd` - system users
- `/etc/gshadow` - encrypted password for each group
- `/etc/shadow` - system user passwords
- `/etc/security/opasswd` - storage of old passwords if the relevant PAM module is in use

The parameters in this section will watch the files to see if they have been opened for write or have had attribute changes (e.g. permissions) and tag them with the identifier "identity" in the audit log file.

**MITRE ATT&CK Mappings:** TA0004


Section #: 4.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.3.8

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Record events affecting the modification of user or group information, including that of passwords and old passwords if in use.
- `/etc/group` - system groups
- `/etc/passwd` - system users
- `/etc/gshadow` - encrypted password for each group
- `/etc/shadow` - system user passwords
- `/etc/security/opasswd` - storage of old passwords if the relevant PAM module is in use

The parameters in this section will watch the files to see if they have been opened for write or have had attribute changes (e.g. permissions) and tag them with the identifier "identity" in the audit log file.

**MITRE ATT&CK Mappings:** M1047


Section #: 4.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.3.9

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Monitor changes to file permissions, attributes, ownership and group. The parameters in this section track changes for system calls that affect file permissions and attributes. The following commands and system calls effect the permissions, ownership and various attributes of files.
- `chmod`
- `fchmod`
- `fchmodat`
- `chown`
- `fchown`
- `fchownat`
- `lchown`
- `setxattr`
- `lsetxattr`
- `fsetxattr`
- `removexattr`
- `lremovexattr`
- `fremovexattr`

In all cases, an audit record will only be written for non-system user ids and will ignore Daemon events. All audit records will be tagged with the identifier "perm_mod."

**MITRE ATT&CK Mappings:** T1562


Section #: 4.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.3.9

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Monitor changes to file permissions, attributes, ownership and group. The parameters in this section track changes for system calls that affect file permissions and attributes. The following commands and system calls effect the permissions, ownership and various attributes of files.
- `chmod`
- `fchmod`
- `fchmodat`
- `chown`
- `fchown`
- `fchownat`
- `lchown`
- `setxattr`
- `lsetxattr`
- `fsetxattr`
- `removexattr`
- `lremovexattr`
- `fremovexattr`

In all cases, an audit record will only be written for non-system user ids and will ignore Daemon events. All audit records will be tagged with the identifier "perm_mod."

**MITRE ATT&CK Mappings:** T1562.006


Section #: 4.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.3.9

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Monitor changes to file permissions, attributes, ownership and group. The parameters in this section track changes for system calls that affect file permissions and attributes. The following commands and system calls effect the permissions, ownership and various attributes of files.
- `chmod`
- `fchmod`
- `fchmodat`
- `chown`
- `fchown`
- `fchownat`
- `lchown`
- `setxattr`
- `lsetxattr`
- `fsetxattr`
- `removexattr`
- `lremovexattr`
- `fremovexattr`

In all cases, an audit record will only be written for non-system user ids and will ignore Daemon events. All audit records will be tagged with the identifier "perm_mod."

**MITRE ATT&CK Mappings:** TA0005


Section #: 4.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.3.9

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Monitor changes to file permissions, attributes, ownership and group. The parameters in this section track changes for system calls that affect file permissions and attributes. The following commands and system calls effect the permissions, ownership and various attributes of files.
- `chmod`
- `fchmod`
- `fchmodat`
- `chown`
- `fchown`
- `fchownat`
- `lchown`
- `setxattr`
- `lsetxattr`
- `fsetxattr`
- `removexattr`
- `lremovexattr`
- `fremovexattr`

In all cases, an audit record will only be written for non-system user ids and will ignore Daemon events. All audit records will be tagged with the identifier "perm_mod."

**MITRE ATT&CK Mappings:** M1022


Section #: 4.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.3.10

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Monitor the use of the `mount` system call. The `mount` (and `umount` ) system call controls the mounting and unmounting of file systems. The parameters below configure the system to create an audit record when the mount system call is used by a non-privileged user

**MITRE ATT&CK Mappings:** T1562


Section #: 4.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.3.10

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Monitor the use of the `mount` system call. The `mount` (and `umount` ) system call controls the mounting and unmounting of file systems. The parameters below configure the system to create an audit record when the mount system call is used by a non-privileged user

**MITRE ATT&CK Mappings:** T1562.006


Section #: 4.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.3.10

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Monitor the use of the `mount` system call. The `mount` (and `umount` ) system call controls the mounting and unmounting of file systems. The parameters below configure the system to create an audit record when the mount system call is used by a non-privileged user

**MITRE ATT&CK Mappings:** TA0010


Section #: 4.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.3.10

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Monitor the use of the `mount` system call. The `mount` (and `umount` ) system call controls the mounting and unmounting of file systems. The parameters below configure the system to create an audit record when the mount system call is used by a non-privileged user

**MITRE ATT&CK Mappings:** M1034


Section #: 4.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.3.11

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Monitor session initiation events. The parameters in this section track changes to the files associated with session events.
- `/var/run/utmp` - tracks all currently logged in users.
- `/var/log/wtmp` - file tracks logins, logouts, shutdown, and reboot events.
- `/var/log/btmp` - keeps track of failed login attempts and can be read by entering the command `/usr/bin/last -f /var/log/btmp`.

All audit records will be tagged with the identifier "session."

**MITRE ATT&CK Mappings:** T1562


Section #: 4.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.3.11

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Monitor session initiation events. The parameters in this section track changes to the files associated with session events.
- `/var/run/utmp` - tracks all currently logged in users.
- `/var/log/wtmp` - file tracks logins, logouts, shutdown, and reboot events.
- `/var/log/btmp` - keeps track of failed login attempts and can be read by entering the command `/usr/bin/last -f /var/log/btmp`.

All audit records will be tagged with the identifier "session."

**MITRE ATT&CK Mappings:** T1562.006


Section #: 4.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.3.11

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Monitor session initiation events. The parameters in this section track changes to the files associated with session events.
- `/var/run/utmp` - tracks all currently logged in users.
- `/var/log/wtmp` - file tracks logins, logouts, shutdown, and reboot events.
- `/var/log/btmp` - keeps track of failed login attempts and can be read by entering the command `/usr/bin/last -f /var/log/btmp`.

All audit records will be tagged with the identifier "session."

**MITRE ATT&CK Mappings:** TA0001


Section #: 4.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.3.12

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Monitor login and logout events. The parameters below track changes to files associated with login/logout events.
- `/var/log/lastlog` - maintain records of the last time a user successfully logged in. 
- `/var/run/faillock` - directory maintains records of login failures via the `pam_faillock` module.

**MITRE ATT&CK Mappings:** T1562


Section #: 4.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.3.12

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Monitor login and logout events. The parameters below track changes to files associated with login/logout events.
- `/var/log/lastlog` - maintain records of the last time a user successfully logged in. 
- `/var/run/faillock` - directory maintains records of login failures via the `pam_faillock` module.

**MITRE ATT&CK Mappings:** T1562.006


Section #: 4.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.3.12

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Monitor login and logout events. The parameters below track changes to files associated with login/logout events.
- `/var/log/lastlog` - maintain records of the last time a user successfully logged in. 
- `/var/run/faillock` - directory maintains records of login failures via the `pam_faillock` module.

**MITRE ATT&CK Mappings:** TA0001


Section #: 4.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.3.13

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Monitor the use of system calls associated with the deletion or renaming of files and file attributes. This configuration statement sets up monitoring for:
- `unlink` - remove a file
- `unlinkat` - remove a file attribute
- `rename` - rename a file
- `renameat` rename a file attribute
system calls and tags them with the identifier "delete".

**MITRE ATT&CK Mappings:** T1562


Section #: 4.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.3.13

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Monitor the use of system calls associated with the deletion or renaming of files and file attributes. This configuration statement sets up monitoring for:
- `unlink` - remove a file
- `unlinkat` - remove a file attribute
- `rename` - rename a file
- `renameat` rename a file attribute
system calls and tags them with the identifier "delete".

**MITRE ATT&CK Mappings:** T1562.006


Section #: 4.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.3.13

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Monitor the use of system calls associated with the deletion or renaming of files and file attributes. This configuration statement sets up monitoring for:
- `unlink` - remove a file
- `unlinkat` - remove a file attribute
- `rename` - rename a file
- `renameat` rename a file attribute
system calls and tags them with the identifier "delete".

**MITRE ATT&CK Mappings:** TA0005


Section #: 4.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.3.14

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Monitor AppArmor, an implementation of mandatory access controls. The parameters below monitor any write access (potential additional, deletion or modification of files in the directory) or attribute changes to the `/etc/apparmor/` and `/etc/apparmor.d/` directories.

**Note:** If a different Mandatory Access Control method is used, changes to the corresponding directories should be audited.

**MITRE ATT&CK Mappings:** T1562


Section #: 4.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.3.14

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Monitor AppArmor, an implementation of mandatory access controls. The parameters below monitor any write access (potential additional, deletion or modification of files in the directory) or attribute changes to the `/etc/apparmor/` and `/etc/apparmor.d/` directories.

**Note:** If a different Mandatory Access Control method is used, changes to the corresponding directories should be audited.

**MITRE ATT&CK Mappings:** T1562.006


Section #: 4.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.3.14

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Monitor AppArmor, an implementation of mandatory access controls. The parameters below monitor any write access (potential additional, deletion or modification of files in the directory) or attribute changes to the `/etc/apparmor/` and `/etc/apparmor.d/` directories.

**Note:** If a different Mandatory Access Control method is used, changes to the corresponding directories should be audited.

**MITRE ATT&CK Mappings:** TA0004


Section #: 4.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.3.14

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Monitor AppArmor, an implementation of mandatory access controls. The parameters below monitor any write access (potential additional, deletion or modification of files in the directory) or attribute changes to the `/etc/apparmor/` and `/etc/apparmor.d/` directories.

**Note:** If a different Mandatory Access Control method is used, changes to the corresponding directories should be audited.

**MITRE ATT&CK Mappings:** M1022


Section #: 4.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.3.19

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Monitor the loading and unloading of kernel modules. All the loading / listing / dependency checking of modules is done by `kmod` via symbolic links.

The following system calls control loading and unloading of modules:
- `init_module` - load a module
- `finit_module` - load a module (used when the overhead of using cryptographically signed modules to determine the authenticity of a module can be avoided)
- `delete_module` - delete a module
- `create_module` - create a loadable module entry
- `query_module` - query the kernel for various bits pertaining to modules

Any execution of the loading and unloading module programs and system calls will trigger an audit record with an identifier of `modules`.

**MITRE ATT&CK Mappings:** T1562


Section #: 4.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.3.19

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Monitor the loading and unloading of kernel modules. All the loading / listing / dependency checking of modules is done by `kmod` via symbolic links.

The following system calls control loading and unloading of modules:
- `init_module` - load a module
- `finit_module` - load a module (used when the overhead of using cryptographically signed modules to determine the authenticity of a module can be avoided)
- `delete_module` - delete a module
- `create_module` - create a loadable module entry
- `query_module` - query the kernel for various bits pertaining to modules

Any execution of the loading and unloading module programs and system calls will trigger an audit record with an identifier of `modules`.

**MITRE ATT&CK Mappings:** T1562.006


Section #: 4.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.3.19

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Monitor the loading and unloading of kernel modules. All the loading / listing / dependency checking of modules is done by `kmod` via symbolic links.

The following system calls control loading and unloading of modules:
- `init_module` - load a module
- `finit_module` - load a module (used when the overhead of using cryptographically signed modules to determine the authenticity of a module can be avoided)
- `delete_module` - delete a module
- `create_module` - create a loadable module entry
- `query_module` - query the kernel for various bits pertaining to modules

Any execution of the loading and unloading module programs and system calls will trigger an audit record with an identifier of `modules`.

**MITRE ATT&CK Mappings:** TA0004


Section #: 4.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.3.19

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Monitor the loading and unloading of kernel modules. All the loading / listing / dependency checking of modules is done by `kmod` via symbolic links.

The following system calls control loading and unloading of modules:
- `init_module` - load a module
- `finit_module` - load a module (used when the overhead of using cryptographically signed modules to determine the authenticity of a module can be avoided)
- `delete_module` - delete a module
- `create_module` - create a loadable module entry
- `query_module` - query the kernel for various bits pertaining to modules

Any execution of the loading and unloading module programs and system calls will trigger an audit record with an identifier of `modules`.

**MITRE ATT&CK Mappings:** M1047


Section #: 4.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.3.20

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Set system audit so that audit rules cannot be modified with `auditctl` . Setting the flag "-e 2" forces audit to be put in immutable mode. Audit changes can only be made on system reboot.

**Note:** This setting will require the system to be rebooted to update the active `auditd` configuration settings.

**MITRE ATT&CK Mappings:** T1562


Section #: 4.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.3.20

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Set system audit so that audit rules cannot be modified with `auditctl` . Setting the flag "-e 2" forces audit to be put in immutable mode. Audit changes can only be made on system reboot.

**Note:** This setting will require the system to be rebooted to update the active `auditd` configuration settings.

**MITRE ATT&CK Mappings:** T1562.001


Section #: 4.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.3.20

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Set system audit so that audit rules cannot be modified with `auditctl` . Setting the flag "-e 2" forces audit to be put in immutable mode. Audit changes can only be made on system reboot.

**Note:** This setting will require the system to be rebooted to update the active `auditd` configuration settings.

**MITRE ATT&CK Mappings:** TA0005


Section #: 4.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.4.1

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Audit log files contain information about the system and system activity.

**MITRE ATT&CK Mappings:** TA0007


Section #: 4.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.4.1

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Audit log files contain information about the system and system activity.

**MITRE ATT&CK Mappings:** T1070


Section #: 4.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.4.1

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Audit log files contain information about the system and system activity.

**MITRE ATT&CK Mappings:** T1070.002


Section #: 4.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.4.1

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Audit log files contain information about the system and system activity.

**MITRE ATT&CK Mappings:** T1083


Section #: 4.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.4.1

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Audit log files contain information about the system and system activity.

**MITRE ATT&CK Mappings:** T1083.000


Section #: 4.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.4.2

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Audit log files contain information about the system and system activity.

**MITRE ATT&CK Mappings:** TA0007


Section #: 4.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.4.2

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Audit log files contain information about the system and system activity.

**MITRE ATT&CK Mappings:** T1070


Section #: 4.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.4.2

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Audit log files contain information about the system and system activity.

**MITRE ATT&CK Mappings:** T1070.002


Section #: 4.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.4.2

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Audit log files contain information about the system and system activity.

**MITRE ATT&CK Mappings:** T1083


Section #: 4.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.4.2

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Audit log files contain information about the system and system activity.

**MITRE ATT&CK Mappings:** T1083.000


Section #: 4.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.4.3

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Audit log files contain information about the system and system activity.

**MITRE ATT&CK Mappings:** TA0007


Section #: 4.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.4.3

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Audit log files contain information about the system and system activity.

**MITRE ATT&CK Mappings:** T1070


Section #: 4.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.4.3

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Audit log files contain information about the system and system activity.

**MITRE ATT&CK Mappings:** T1070.002


Section #: 4.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.4.3

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Audit log files contain information about the system and system activity.

**MITRE ATT&CK Mappings:** T1083


Section #: 4.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.4.3

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Audit log files contain information about the system and system activity.

**MITRE ATT&CK Mappings:** T1083.000


Section #: 4.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.4.4

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** The audit log directory contains audit log files.

**MITRE ATT&CK Mappings:** TA0007


Section #: 4.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.4.4

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** The audit log directory contains audit log files.

**MITRE ATT&CK Mappings:** T1070


Section #: 4.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.4.4

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** The audit log directory contains audit log files.

**MITRE ATT&CK Mappings:** T1070.002


Section #: 4.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.4.4

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** The audit log directory contains audit log files.

**MITRE ATT&CK Mappings:** T1083


Section #: 4.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.4.4

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** The audit log directory contains audit log files.

**MITRE ATT&CK Mappings:** T1083.000


Section #: 4.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.4.5

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Audit configuration files control auditd and what events are audited.

**MITRE ATT&CK Mappings:** TA0007


Section #: 4.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.4.5

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Audit configuration files control auditd and what events are audited.

**MITRE ATT&CK Mappings:** T1070


Section #: 4.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.4.5

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Audit configuration files control auditd and what events are audited.

**MITRE ATT&CK Mappings:** T1070.002


Section #: 4.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.4.5

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Audit configuration files control auditd and what events are audited.

**MITRE ATT&CK Mappings:** T1083


Section #: 4.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.4.5

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Audit configuration files control auditd and what events are audited.

**MITRE ATT&CK Mappings:** T1083.000


Section #: 4.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.4.6

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Audit configuration files control auditd and what events are audited.

**MITRE ATT&CK Mappings:** TA0007


Section #: 4.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.4.6

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Audit configuration files control auditd and what events are audited.

**MITRE ATT&CK Mappings:** T1070


Section #: 4.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.4.6

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Audit configuration files control auditd and what events are audited.

**MITRE ATT&CK Mappings:** T1070.002


Section #: 4.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.4.6

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Audit configuration files control auditd and what events are audited.

**MITRE ATT&CK Mappings:** T1083


Section #: 4.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.4.6

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Audit configuration files control auditd and what events are audited.

**MITRE ATT&CK Mappings:** T1083.000


Section #: 4.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.4.7

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Audit configuration files control auditd and what events are audited.

**MITRE ATT&CK Mappings:** TA0007


Section #: 4.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.4.7

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Audit configuration files control auditd and what events are audited.

**MITRE ATT&CK Mappings:** T1070


Section #: 4.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.4.7

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Audit configuration files control auditd and what events are audited.

**MITRE ATT&CK Mappings:** T1070.002


Section #: 4.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.4.7

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Audit configuration files control auditd and what events are audited.

**MITRE ATT&CK Mappings:** T1083


Section #: 4.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.4.7

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Audit configuration files control auditd and what events are audited.

**MITRE ATT&CK Mappings:** T1083.000


Section #: 4.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.4.8

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Audit tools include, but are not limited to, vendor-provided and open source audit tools needed to successfully view and manipulate audit information system activity and records. Audit tools include custom queries and report generators.

**MITRE ATT&CK Mappings:** TA0007


Section #: 4.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.4.8

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Audit tools include, but are not limited to, vendor-provided and open source audit tools needed to successfully view and manipulate audit information system activity and records. Audit tools include custom queries and report generators.

**MITRE ATT&CK Mappings:** T1070


Section #: 4.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.4.8

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Audit tools include, but are not limited to, vendor-provided and open source audit tools needed to successfully view and manipulate audit information system activity and records. Audit tools include custom queries and report generators.

**MITRE ATT&CK Mappings:** T1070.002


Section #: 4.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.4.8

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Audit tools include, but are not limited to, vendor-provided and open source audit tools needed to successfully view and manipulate audit information system activity and records. Audit tools include custom queries and report generators.

**MITRE ATT&CK Mappings:** T1083


Section #: 4.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.4.8

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Audit tools include, but are not limited to, vendor-provided and open source audit tools needed to successfully view and manipulate audit information system activity and records. Audit tools include custom queries and report generators.

**MITRE ATT&CK Mappings:** T1083.000


Section #: 4.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.4.9

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Audit tools include, but are not limited to, vendor-provided and open source audit tools needed to successfully view and manipulate audit information system activity and records. Audit tools include custom queries and report generators.

**MITRE ATT&CK Mappings:** TA0007


Section #: 4.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.4.9

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Audit tools include, but are not limited to, vendor-provided and open source audit tools needed to successfully view and manipulate audit information system activity and records. Audit tools include custom queries and report generators.

**MITRE ATT&CK Mappings:** T1070


Section #: 4.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.4.9

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Audit tools include, but are not limited to, vendor-provided and open source audit tools needed to successfully view and manipulate audit information system activity and records. Audit tools include custom queries and report generators.

**MITRE ATT&CK Mappings:** T1070.002


Section #: 4.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.4.9

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Audit tools include, but are not limited to, vendor-provided and open source audit tools needed to successfully view and manipulate audit information system activity and records. Audit tools include custom queries and report generators.

**MITRE ATT&CK Mappings:** T1083


Section #: 4.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.4.9

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Audit tools include, but are not limited to, vendor-provided and open source audit tools needed to successfully view and manipulate audit information system activity and records. Audit tools include custom queries and report generators.

**MITRE ATT&CK Mappings:** T1083.000


Section #: 4.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.4.10

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Audit tools include, but are not limited to, vendor-provided and open source audit tools needed to successfully view and manipulate audit information system activity and records. Audit tools include custom queries and report generators.

**MITRE ATT&CK Mappings:** TA0007


Section #: 4.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.4.10

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Audit tools include, but are not limited to, vendor-provided and open source audit tools needed to successfully view and manipulate audit information system activity and records. Audit tools include custom queries and report generators.

**MITRE ATT&CK Mappings:** T1070


Section #: 4.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.4.10

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Audit tools include, but are not limited to, vendor-provided and open source audit tools needed to successfully view and manipulate audit information system activity and records. Audit tools include custom queries and report generators.

**MITRE ATT&CK Mappings:** T1070.002


Section #: 4.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.4.10

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Audit tools include, but are not limited to, vendor-provided and open source audit tools needed to successfully view and manipulate audit information system activity and records. Audit tools include custom queries and report generators.

**MITRE ATT&CK Mappings:** T1083


Section #: 4.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.4.10

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** Audit tools include, but are not limited to, vendor-provided and open source audit tools needed to successfully view and manipulate audit information system activity and records. Audit tools include custom queries and report generators.

**MITRE ATT&CK Mappings:** T1083.000


Section #: 4.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.4.11

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Audit tools include, but are not limited to, vendor-provided and open source audit tools needed to successfully view and manipulate audit information system activity and records. Audit tools include custom queries and report generators.

**MITRE ATT&CK Mappings:** TA0007


Section #: 4.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.4.11

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Audit tools include, but are not limited to, vendor-provided and open source audit tools needed to successfully view and manipulate audit information system activity and records. Audit tools include custom queries and report generators.

**MITRE ATT&CK Mappings:** T1070


Section #: 4.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.4.11

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Audit tools include, but are not limited to, vendor-provided and open source audit tools needed to successfully view and manipulate audit information system activity and records. Audit tools include custom queries and report generators.

**MITRE ATT&CK Mappings:** T1070.002


Section #: 4.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.4.11

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Audit tools include, but are not limited to, vendor-provided and open source audit tools needed to successfully view and manipulate audit information system activity and records. Audit tools include custom queries and report generators.

**MITRE ATT&CK Mappings:** T1083


Section #: 4.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.4.11

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Audit tools include, but are not limited to, vendor-provided and open source audit tools needed to successfully view and manipulate audit information system activity and records. Audit tools include custom queries and report generators.

**MITRE ATT&CK Mappings:** T1083.000


Section #: 4.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.2.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Log files contain information from many services on the the local system, or in the event of a centralized log server, others systems logs as well. In general log files are found in `/var/log/`, although application can be configured to store logs elsewhere. Should your application store logs in another, ensure to run the same test on that location.

**MITRE ATT&CK Mappings:** TA0007


Section #: 4.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.2.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Log files contain information from many services on the the local system, or in the event of a centralized log server, others systems logs as well. In general log files are found in `/var/log/`, although application can be configured to store logs elsewhere. Should your application store logs in another, ensure to run the same test on that location.

**MITRE ATT&CK Mappings:** T1070


Section #: 4.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.2.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Log files contain information from many services on the the local system, or in the event of a centralized log server, others systems logs as well. In general log files are found in `/var/log/`, although application can be configured to store logs elsewhere. Should your application store logs in another, ensure to run the same test on that location.

**MITRE ATT&CK Mappings:** T1070.002


Section #: 4.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.2.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Log files contain information from many services on the the local system, or in the event of a centralized log server, others systems logs as well. In general log files are found in `/var/log/`, although application can be configured to store logs elsewhere. Should your application store logs in another, ensure to run the same test on that location.

**MITRE ATT&CK Mappings:** T1083


Section #: 4.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.2.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Log files contain information from many services on the the local system, or in the event of a centralized log server, others systems logs as well. In general log files are found in `/var/log/`, although application can be configured to store logs elsewhere. Should your application store logs in another, ensure to run the same test on that location.

**MITRE ATT&CK Mappings:** T1083.000


Section #: 4.2.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.2.1.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Ensure that the `systemd-journald` service is enabled to allow capturing of logging events.

**MITRE ATT&CK Mappings:** TA0005


Section #: 4.2.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.2.1.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Ensure that the `systemd-journald` service is enabled to allow capturing of logging events.

**MITRE ATT&CK Mappings:** T1070


Section #: 4.2.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.2.1.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Ensure that the `systemd-journald` service is enabled to allow capturing of logging events.

**MITRE ATT&CK Mappings:** T1070.002


Section #: 4.2.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.2.1.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Ensure that the `systemd-journald` service is enabled to allow capturing of logging events.

**MITRE ATT&CK Mappings:** T1562


Section #: 4.2.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.2.1.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Ensure that the `systemd-journald` service is enabled to allow capturing of logging events.

**MITRE ATT&CK Mappings:** T1562.001


Section #: 4.2.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.2.1.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The journald system includes the capability of compressing overly large files to avoid filling up the system with logs or making the logs unmanageably large.

**MITRE ATT&CK Mappings:** TA0040


Section #: 4.2.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.2.1.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The journald system includes the capability of compressing overly large files to avoid filling up the system with logs or making the logs unmanageably large.

**MITRE ATT&CK Mappings:** T1562


Section #: 4.2.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.2.1.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The journald system includes the capability of compressing overly large files to avoid filling up the system with logs or making the logs unmanageably large.

**MITRE ATT&CK Mappings:** T1562.002


Section #: 4.2.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.2.1.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The journald system includes the capability of compressing overly large files to avoid filling up the system with logs or making the logs unmanageably large.

**MITRE ATT&CK Mappings:** M1053


Section #: 4.2.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.2.1.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Data from journald may be stored in volatile memory or persisted locally on the server. Logs in memory will be lost upon a system reboot. By persisting logs to local disk on the server they are protected from loss due to a reboot.

**MITRE ATT&CK Mappings:** TA0005


Section #: 4.2.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.2.1.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Data from journald may be stored in volatile memory or persisted locally on the server. Logs in memory will be lost upon a system reboot. By persisting logs to local disk on the server they are protected from loss due to a reboot.

**MITRE ATT&CK Mappings:** T1070


Section #: 4.2.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.2.1.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Data from journald may be stored in volatile memory or persisted locally on the server. Logs in memory will be lost upon a system reboot. By persisting logs to local disk on the server they are protected from loss due to a reboot.

**MITRE ATT&CK Mappings:** T1070.002


Section #: 4.2.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.2.1.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Data from journald may be stored in volatile memory or persisted locally on the server. Logs in memory will be lost upon a system reboot. By persisting logs to local disk on the server they are protected from loss due to a reboot.

**MITRE ATT&CK Mappings:** T1562


Section #: 4.2.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.2.1.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Data from journald may be stored in volatile memory or persisted locally on the server. Logs in memory will be lost upon a system reboot. By persisting logs to local disk on the server they are protected from loss due to a reboot.

**MITRE ATT&CK Mappings:** T1562.006


Section #: 4.2.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.2.1.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Data from journald may be stored in volatile memory or persisted locally on the server. Logs in memory will be lost upon a system reboot. By persisting logs to local disk on the server they are protected from loss due to a reboot.

**MITRE ATT&CK Mappings:** M1022


Section #: 4.2.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.2.1.5

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Data from `journald` should be kept in the confines of the service and not forwarded on to other services.

**MITRE ATT&CK Mappings:** TA0040


Section #: 4.2.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.2.1.5

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Data from `journald` should be kept in the confines of the service and not forwarded on to other services.

**MITRE ATT&CK Mappings:** T1070


Section #: 4.2.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.2.1.5

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Data from `journald` should be kept in the confines of the service and not forwarded on to other services.

**MITRE ATT&CK Mappings:** T1070.002


Section #: 4.2.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.2.1.5

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Data from `journald` should be kept in the confines of the service and not forwarded on to other services.

**MITRE ATT&CK Mappings:** T1562


Section #: 4.2.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.2.1.5

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Data from `journald` should be kept in the confines of the service and not forwarded on to other services.

**MITRE ATT&CK Mappings:** T1562.006


Section #: 4.2.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.2.1.5

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Data from `journald` should be kept in the confines of the service and not forwarded on to other services.

**MITRE ATT&CK Mappings:** M1029


Section #: 4.2.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.2.1.6

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Journald includes the capability of rotating log files regularly to avoid filling up the system with logs or making the logs unmanageably large. The file `/etc/systemd/journald.conf` is the configuration file used to specify how logs generated by Journald should be rotated.

**MITRE ATT&CK Mappings:** TA0040


Section #: 4.2.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.2.1.6

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Journald includes the capability of rotating log files regularly to avoid filling up the system with logs or making the logs unmanageably large. The file `/etc/systemd/journald.conf` is the configuration file used to specify how logs generated by Journald should be rotated.

**MITRE ATT&CK Mappings:** T1070


Section #: 4.2.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.2.1.6

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Journald includes the capability of rotating log files regularly to avoid filling up the system with logs or making the logs unmanageably large. The file `/etc/systemd/journald.conf` is the configuration file used to specify how logs generated by Journald should be rotated.

**MITRE ATT&CK Mappings:** T1070.002


Section #: 4.2.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.2.1.6

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Journald includes the capability of rotating log files regularly to avoid filling up the system with logs or making the logs unmanageably large. The file `/etc/systemd/journald.conf` is the configuration file used to specify how logs generated by Journald should be rotated.

**MITRE ATT&CK Mappings:** M1022


Section #: 4.2.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.2.1.7

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Journald will create logfiles that do not already exist on the system. This setting controls what permissions will be applied to these newly created files.

**MITRE ATT&CK Mappings:** TA0007


Section #: 4.2.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.2.1.7

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Journald will create logfiles that do not already exist on the system. This setting controls what permissions will be applied to these newly created files.

**MITRE ATT&CK Mappings:** T1070


Section #: 4.2.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.2.1.7

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Journald will create logfiles that do not already exist on the system. This setting controls what permissions will be applied to these newly created files.

**MITRE ATT&CK Mappings:** T1070.002


Section #: 4.2.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.2.1.7

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Journald will create logfiles that do not already exist on the system. This setting controls what permissions will be applied to these newly created files.

**MITRE ATT&CK Mappings:** T1083


Section #: 4.2.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.2.1.7

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Journald will create logfiles that do not already exist on the system. This setting controls what permissions will be applied to these newly created files.

**MITRE ATT&CK Mappings:** T1083.000


Section #: 4.2.1.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.2.1.1.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Journald (via `systemd-journal-remote`) supports the ability to send log events it gathers to a remote log host or to receive messages from remote hosts, thus enabling centralised log management.

**MITRE ATT&CK Mappings:** TA0040


Section #: 4.2.1.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.2.1.1.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Journald (via `systemd-journal-remote`) supports the ability to send log events it gathers to a remote log host or to receive messages from remote hosts, thus enabling centralised log management.

**MITRE ATT&CK Mappings:** T1070


Section #: 4.2.1.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.2.1.1.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Journald (via `systemd-journal-remote`) supports the ability to send log events it gathers to a remote log host or to receive messages from remote hosts, thus enabling centralised log management.

**MITRE ATT&CK Mappings:** T1070.002


Section #: 4.2.1.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.2.1.1.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Journald (via `systemd-journal-remote`) supports the ability to send log events it gathers to a remote log host or to receive messages from remote hosts, thus enabling centralised log management.

**MITRE ATT&CK Mappings:** T1562


Section #: 4.2.1.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.2.1.1.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Journald (via `systemd-journal-remote`) supports the ability to send log events it gathers to a remote log host or to receive messages from remote hosts, thus enabling centralised log management.

**MITRE ATT&CK Mappings:** T1562.006


Section #: 4.2.1.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.2.1.1.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Journald (via `systemd-journal-remote`) supports the ability to send log events it gathers to a remote log host or to receive messages from remote hosts, thus enabling centralised log management.

**MITRE ATT&CK Mappings:** M1029


Section #: 4.2.1.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.2.1.1.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Journald (via `systemd-journal-remote`) supports the ability to send log events it gathers to a remote log host or to receive messages from remote hosts, thus enabling centralised log management.

**MITRE ATT&CK Mappings:** TA0040


Section #: 4.2.1.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.2.1.1.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Journald (via `systemd-journal-remote`) supports the ability to send log events it gathers to a remote log host or to receive messages from remote hosts, thus enabling centralised log management.

**MITRE ATT&CK Mappings:** T1070


Section #: 4.2.1.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.2.1.1.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Journald (via `systemd-journal-remote`) supports the ability to send log events it gathers to a remote log host or to receive messages from remote hosts, thus enabling centralised log management.

**MITRE ATT&CK Mappings:** T1070.002


Section #: 4.2.1.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.2.1.1.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Journald (via `systemd-journal-remote`) supports the ability to send log events it gathers to a remote log host or to receive messages from remote hosts, thus enabling centralised log management.

**MITRE ATT&CK Mappings:** T1562


Section #: 4.2.1.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.2.1.1.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Journald (via `systemd-journal-remote`) supports the ability to send log events it gathers to a remote log host or to receive messages from remote hosts, thus enabling centralised log management.

**MITRE ATT&CK Mappings:** T1562.006


Section #: 4.2.1.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.2.1.1.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Journald (via `systemd-journal-remote`) supports the ability to send log events it gathers to a remote log host or to receive messages from remote hosts, thus enabling centralised log management.

**MITRE ATT&CK Mappings:** M1029


Section #: 4.2.1.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.2.1.1.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Journald (via `systemd-journal-remote`) supports the ability to send log events it gathers to a remote log host or to receive messages from remote hosts, thus enabling centralised log management.

**MITRE ATT&CK Mappings:** TA0040


Section #: 4.2.1.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.2.1.1.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Journald (via `systemd-journal-remote`) supports the ability to send log events it gathers to a remote log host or to receive messages from remote hosts, thus enabling centralised log management.

**MITRE ATT&CK Mappings:** T1070


Section #: 4.2.1.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.2.1.1.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Journald (via `systemd-journal-remote`) supports the ability to send log events it gathers to a remote log host or to receive messages from remote hosts, thus enabling centralised log management.

**MITRE ATT&CK Mappings:** T1070.002


Section #: 4.2.1.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.2.1.1.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Journald (via `systemd-journal-remote`) supports the ability to send log events it gathers to a remote log host or to receive messages from remote hosts, thus enabling centralised log management.

**MITRE ATT&CK Mappings:** T1562


Section #: 4.2.1.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.2.1.1.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Journald (via `systemd-journal-remote`) supports the ability to send log events it gathers to a remote log host or to receive messages from remote hosts, thus enabling centralised log management.

**MITRE ATT&CK Mappings:** T1562.006


Section #: 4.2.1.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.2.1.1.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Journald (via `systemd-journal-remote`) supports the ability to send log events it gathers to a remote log host or to receive messages from remote hosts, thus enabling centralised log management.

**MITRE ATT&CK Mappings:** M1029


Section #: 4.2.1.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.2.1.1.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Journald supports the ability to receive messages from remote hosts, thus acting as a log server. Clients should not receive data from other hosts.

**NOTE:** 
- The same package, `systemd-journal-remote`, is used for both sending logs to remote hosts and receiving incoming logs.
- With regards to receiving logs, there are two services; `systemd-journal-remote.socket` and `systemd-journal-remote.service`.

**MITRE ATT&CK Mappings:** TA0040


Section #: 4.2.1.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.2.1.1.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Journald supports the ability to receive messages from remote hosts, thus acting as a log server. Clients should not receive data from other hosts.

**NOTE:** 
- The same package, `systemd-journal-remote`, is used for both sending logs to remote hosts and receiving incoming logs.
- With regards to receiving logs, there are two services; `systemd-journal-remote.socket` and `systemd-journal-remote.service`.

**MITRE ATT&CK Mappings:** T1070


Section #: 4.2.1.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.2.1.1.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Journald supports the ability to receive messages from remote hosts, thus acting as a log server. Clients should not receive data from other hosts.

**NOTE:** 
- The same package, `systemd-journal-remote`, is used for both sending logs to remote hosts and receiving incoming logs.
- With regards to receiving logs, there are two services; `systemd-journal-remote.socket` and `systemd-journal-remote.service`.

**MITRE ATT&CK Mappings:** T1070.002


Section #: 4.2.1.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.2.1.1.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Journald supports the ability to receive messages from remote hosts, thus acting as a log server. Clients should not receive data from other hosts.

**NOTE:** 
- The same package, `systemd-journal-remote`, is used for both sending logs to remote hosts and receiving incoming logs.
- With regards to receiving logs, there are two services; `systemd-journal-remote.socket` and `systemd-journal-remote.service`.

**MITRE ATT&CK Mappings:** T1562


Section #: 4.2.1.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.2.1.1.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Journald supports the ability to receive messages from remote hosts, thus acting as a log server. Clients should not receive data from other hosts.

**NOTE:** 
- The same package, `systemd-journal-remote`, is used for both sending logs to remote hosts and receiving incoming logs.
- With regards to receiving logs, there are two services; `systemd-journal-remote.socket` and `systemd-journal-remote.service`.

**MITRE ATT&CK Mappings:** T1562.006


Section #: 4.2.1.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.2.1.1.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Journald supports the ability to receive messages from remote hosts, thus acting as a log server. Clients should not receive data from other hosts.

**NOTE:** 
- The same package, `systemd-journal-remote`, is used for both sending logs to remote hosts and receiving incoming logs.
- With regards to receiving logs, there are two services; `systemd-journal-remote.socket` and `systemd-journal-remote.service`.

**MITRE ATT&CK Mappings:** M1029


Section #: 4.2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.2.2.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `rsyslog` software is recommended in environments where `journald` does not meet operation requirements.

**MITRE ATT&CK Mappings:** TA0005


Section #: 4.2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.2.2.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `rsyslog` software is recommended in environments where `journald` does not meet operation requirements.

**MITRE ATT&CK Mappings:** T1005


Section #: 4.2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.2.2.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `rsyslog` software is recommended in environments where `journald` does not meet operation requirements.

**MITRE ATT&CK Mappings:** T1005.000


Section #: 4.2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.2.2.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `rsyslog` software is recommended in environments where `journald` does not meet operation requirements.

**MITRE ATT&CK Mappings:** T1070


Section #: 4.2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.2.2.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `rsyslog` software is recommended in environments where `journald` does not meet operation requirements.

**MITRE ATT&CK Mappings:** T1070.002


Section #: 4.2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.2.2.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Once the `rsyslog` package is installed, ensure that the service is enabled.

**MITRE ATT&CK Mappings:** TA0005


Section #: 4.2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.2.2.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Once the `rsyslog` package is installed, ensure that the service is enabled.

**MITRE ATT&CK Mappings:** T1070


Section #: 4.2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.2.2.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Once the `rsyslog` package is installed, ensure that the service is enabled.

**MITRE ATT&CK Mappings:** T1070.002


Section #: 4.2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.2.2.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Once the `rsyslog` package is installed, ensure that the service is enabled.

**MITRE ATT&CK Mappings:** T1562


Section #: 4.2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.2.2.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Once the `rsyslog` package is installed, ensure that the service is enabled.

**MITRE ATT&CK Mappings:** T1562.001


Section #: 4.2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.2.2.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** RSyslog will create logfiles that do not already exist on the system. This setting controls what permissions will be applied to these newly created files.

**MITRE ATT&CK Mappings:** TA0007


Section #: 4.2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.2.2.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** RSyslog will create logfiles that do not already exist on the system. This setting controls what permissions will be applied to these newly created files.

**MITRE ATT&CK Mappings:** T1070


Section #: 4.2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.2.2.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** RSyslog will create logfiles that do not already exist on the system. This setting controls what permissions will be applied to these newly created files.

**MITRE ATT&CK Mappings:** T1070.002


Section #: 4.2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.2.2.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** RSyslog will create logfiles that do not already exist on the system. This setting controls what permissions will be applied to these newly created files.

**MITRE ATT&CK Mappings:** T1083


Section #: 4.2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.2.2.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** RSyslog will create logfiles that do not already exist on the system. This setting controls what permissions will be applied to these newly created files.

**MITRE ATT&CK Mappings:** T1083.000


Section #: 4.2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.2.2.5

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/etc/rsyslog.conf` and `/etc/rsyslog.d/*.conf` files specifies rules for logging and which files are to be used to log certain classes of messages.

**MITRE ATT&CK Mappings:** TA0005


Section #: 4.2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.2.2.5

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/etc/rsyslog.conf` and `/etc/rsyslog.d/*.conf` files specifies rules for logging and which files are to be used to log certain classes of messages.

**MITRE ATT&CK Mappings:** T1070


Section #: 4.2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.2.2.5

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/etc/rsyslog.conf` and `/etc/rsyslog.d/*.conf` files specifies rules for logging and which files are to be used to log certain classes of messages.

**MITRE ATT&CK Mappings:** T1070.002


Section #: 4.2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.2.2.6

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** RSyslog supports the ability to send log events it gathers to a remote log host or to receive messages from remote hosts, thus enabling centralised log management.

**MITRE ATT&CK Mappings:** TA0040


Section #: 4.2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.2.2.6

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** RSyslog supports the ability to send log events it gathers to a remote log host or to receive messages from remote hosts, thus enabling centralised log management.

**MITRE ATT&CK Mappings:** T1070


Section #: 4.2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.2.2.6

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** RSyslog supports the ability to send log events it gathers to a remote log host or to receive messages from remote hosts, thus enabling centralised log management.

**MITRE ATT&CK Mappings:** T1070.002


Section #: 4.2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.2.2.6

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** RSyslog supports the ability to send log events it gathers to a remote log host or to receive messages from remote hosts, thus enabling centralised log management.

**MITRE ATT&CK Mappings:** T1562


Section #: 4.2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.2.2.6

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** RSyslog supports the ability to send log events it gathers to a remote log host or to receive messages from remote hosts, thus enabling centralised log management.

**MITRE ATT&CK Mappings:** T1562.006


Section #: 4.2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.2.2.6

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** RSyslog supports the ability to send log events it gathers to a remote log host or to receive messages from remote hosts, thus enabling centralised log management.

**MITRE ATT&CK Mappings:** M1029


Section #: 4.2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.2.2.7

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** RSyslog supports the ability to receive messages from remote hosts, thus acting as a log server. Clients should not receive data from other hosts.

**MITRE ATT&CK Mappings:** TA0005


Section #: 4.2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.2.2.7

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** RSyslog supports the ability to receive messages from remote hosts, thus acting as a log server. Clients should not receive data from other hosts.

**MITRE ATT&CK Mappings:** T1070


Section #: 4.2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.2.2.7

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** RSyslog supports the ability to receive messages from remote hosts, thus acting as a log server. Clients should not receive data from other hosts.

**MITRE ATT&CK Mappings:** T1070.002


Section #: 4.2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.2.2.7

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** RSyslog supports the ability to receive messages from remote hosts, thus acting as a log server. Clients should not receive data from other hosts.

**MITRE ATT&CK Mappings:** T1562


Section #: 4.2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.2.2.7

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** RSyslog supports the ability to receive messages from remote hosts, thus acting as a log server. Clients should not receive data from other hosts.

**MITRE ATT&CK Mappings:** T1562.006


Section #: 5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.1.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `cron` daemon is used to execute batch jobs on the system.

_Note: Other methods, such as `systemd timers`, exist for scheduling jobs. If another method is used, `cron` should be removed, and the alternate method should be secured in accordance with local site policy_

**MITRE ATT&CK Mappings:** TA0005


Section #: 5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.1.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `cron` daemon is used to execute batch jobs on the system.

_Note: Other methods, such as `systemd timers`, exist for scheduling jobs. If another method is used, `cron` should be removed, and the alternate method should be secured in accordance with local site policy_

**MITRE ATT&CK Mappings:** T1562


Section #: 5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.1.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `cron` daemon is used to execute batch jobs on the system.

_Note: Other methods, such as `systemd timers`, exist for scheduling jobs. If another method is used, `cron` should be removed, and the alternate method should be secured in accordance with local site policy_

**MITRE ATT&CK Mappings:** T1562.001


Section #: 5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.1.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `cron` daemon is used to execute batch jobs on the system.

_Note: Other methods, such as `systemd timers`, exist for scheduling jobs. If another method is used, `cron` should be removed, and the alternate method should be secured in accordance with local site policy_

**MITRE ATT&CK Mappings:** M1018


Section #: 5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.1.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/etc/crontab` file is used by `cron` to control its own jobs. The commands in this item make sure that root is the user and group owner of the file and that only the owner can access the file.

_Note: Other methods, such as `systemd timers`, exist for scheduling jobs. If another method is used, `cron` should be removed, and the alternate method should be secured in accordance with local site policy_

**MITRE ATT&CK Mappings:** TA0002


Section #: 5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.1.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/etc/crontab` file is used by `cron` to control its own jobs. The commands in this item make sure that root is the user and group owner of the file and that only the owner can access the file.

_Note: Other methods, such as `systemd timers`, exist for scheduling jobs. If another method is used, `cron` should be removed, and the alternate method should be secured in accordance with local site policy_

**MITRE ATT&CK Mappings:** TA0007


Section #: 5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.1.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/etc/crontab` file is used by `cron` to control its own jobs. The commands in this item make sure that root is the user and group owner of the file and that only the owner can access the file.

_Note: Other methods, such as `systemd timers`, exist for scheduling jobs. If another method is used, `cron` should be removed, and the alternate method should be secured in accordance with local site policy_

**MITRE ATT&CK Mappings:** T1053


Section #: 5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.1.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/etc/crontab` file is used by `cron` to control its own jobs. The commands in this item make sure that root is the user and group owner of the file and that only the owner can access the file.

_Note: Other methods, such as `systemd timers`, exist for scheduling jobs. If another method is used, `cron` should be removed, and the alternate method should be secured in accordance with local site policy_

**MITRE ATT&CK Mappings:** T1053.003


Section #: 5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.1.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/etc/crontab` file is used by `cron` to control its own jobs. The commands in this item make sure that root is the user and group owner of the file and that only the owner can access the file.

_Note: Other methods, such as `systemd timers`, exist for scheduling jobs. If another method is used, `cron` should be removed, and the alternate method should be secured in accordance with local site policy_

**MITRE ATT&CK Mappings:** M1018


Section #: 5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.1.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** This directory contains system `cron` jobs that need to run on an hourly basis. The files in this directory cannot be manipulated by the `crontab` command, but are instead edited by system administrators using a text editor. The commands below restrict read/write and search access to user and group root, preventing regular users from accessing this directory.

_Note: Other methods, such as `systemd timers`, exist for scheduling jobs. If another method is used, `cron` should be removed, and the alternate method should be secured in accordance with local site policy_

**MITRE ATT&CK Mappings:** TA0002


Section #: 5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.1.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** This directory contains system `cron` jobs that need to run on an hourly basis. The files in this directory cannot be manipulated by the `crontab` command, but are instead edited by system administrators using a text editor. The commands below restrict read/write and search access to user and group root, preventing regular users from accessing this directory.

_Note: Other methods, such as `systemd timers`, exist for scheduling jobs. If another method is used, `cron` should be removed, and the alternate method should be secured in accordance with local site policy_

**MITRE ATT&CK Mappings:** TA0007


Section #: 5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.1.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** This directory contains system `cron` jobs that need to run on an hourly basis. The files in this directory cannot be manipulated by the `crontab` command, but are instead edited by system administrators using a text editor. The commands below restrict read/write and search access to user and group root, preventing regular users from accessing this directory.

_Note: Other methods, such as `systemd timers`, exist for scheduling jobs. If another method is used, `cron` should be removed, and the alternate method should be secured in accordance with local site policy_

**MITRE ATT&CK Mappings:** T1053


Section #: 5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.1.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** This directory contains system `cron` jobs that need to run on an hourly basis. The files in this directory cannot be manipulated by the `crontab` command, but are instead edited by system administrators using a text editor. The commands below restrict read/write and search access to user and group root, preventing regular users from accessing this directory.

_Note: Other methods, such as `systemd timers`, exist for scheduling jobs. If another method is used, `cron` should be removed, and the alternate method should be secured in accordance with local site policy_

**MITRE ATT&CK Mappings:** T1053.003


Section #: 5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.1.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** This directory contains system `cron` jobs that need to run on an hourly basis. The files in this directory cannot be manipulated by the `crontab` command, but are instead edited by system administrators using a text editor. The commands below restrict read/write and search access to user and group root, preventing regular users from accessing this directory.

_Note: Other methods, such as `systemd timers`, exist for scheduling jobs. If another method is used, `cron` should be removed, and the alternate method should be secured in accordance with local site policy_

**MITRE ATT&CK Mappings:** M1018


Section #: 5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.1.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/etc/cron.daily` directory contains system cron jobs that need to run on a daily basis. The files in this directory cannot be manipulated by the `crontab` command, but are instead edited by system administrators using a text editor. The commands below restrict read/write and search access to user and group root, preventing regular users from accessing this directory.

_Note: Other methods, such as `systemd timers`, exist for scheduling jobs. If another method is used, `cron` should be removed, and the alternate method should be secured in accordance with local site policy_

**MITRE ATT&CK Mappings:** TA0002


Section #: 5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.1.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/etc/cron.daily` directory contains system cron jobs that need to run on a daily basis. The files in this directory cannot be manipulated by the `crontab` command, but are instead edited by system administrators using a text editor. The commands below restrict read/write and search access to user and group root, preventing regular users from accessing this directory.

_Note: Other methods, such as `systemd timers`, exist for scheduling jobs. If another method is used, `cron` should be removed, and the alternate method should be secured in accordance with local site policy_

**MITRE ATT&CK Mappings:** TA0007


Section #: 5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.1.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/etc/cron.daily` directory contains system cron jobs that need to run on a daily basis. The files in this directory cannot be manipulated by the `crontab` command, but are instead edited by system administrators using a text editor. The commands below restrict read/write and search access to user and group root, preventing regular users from accessing this directory.

_Note: Other methods, such as `systemd timers`, exist for scheduling jobs. If another method is used, `cron` should be removed, and the alternate method should be secured in accordance with local site policy_

**MITRE ATT&CK Mappings:** T1053


Section #: 5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.1.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/etc/cron.daily` directory contains system cron jobs that need to run on a daily basis. The files in this directory cannot be manipulated by the `crontab` command, but are instead edited by system administrators using a text editor. The commands below restrict read/write and search access to user and group root, preventing regular users from accessing this directory.

_Note: Other methods, such as `systemd timers`, exist for scheduling jobs. If another method is used, `cron` should be removed, and the alternate method should be secured in accordance with local site policy_

**MITRE ATT&CK Mappings:** T1053.003


Section #: 5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.1.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/etc/cron.daily` directory contains system cron jobs that need to run on a daily basis. The files in this directory cannot be manipulated by the `crontab` command, but are instead edited by system administrators using a text editor. The commands below restrict read/write and search access to user and group root, preventing regular users from accessing this directory.

_Note: Other methods, such as `systemd timers`, exist for scheduling jobs. If another method is used, `cron` should be removed, and the alternate method should be secured in accordance with local site policy_

**MITRE ATT&CK Mappings:** M1018


Section #: 5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.1.5

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/etc/cron.weekly` directory contains system cron jobs that need to run on a weekly basis. The files in this directory cannot be manipulated by the `crontab` command, but are instead edited by system administrators using a text editor. The commands below restrict read/write and search access to user and group root, preventing regular users from accessing this directory.

_Note: Other methods, such as `systemd timers`, exist for scheduling jobs. If another method is used, `cron` should be removed, and the alternate method should be secured in accordance with local site policy_

**MITRE ATT&CK Mappings:** TA0002


Section #: 5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.1.5

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/etc/cron.weekly` directory contains system cron jobs that need to run on a weekly basis. The files in this directory cannot be manipulated by the `crontab` command, but are instead edited by system administrators using a text editor. The commands below restrict read/write and search access to user and group root, preventing regular users from accessing this directory.

_Note: Other methods, such as `systemd timers`, exist for scheduling jobs. If another method is used, `cron` should be removed, and the alternate method should be secured in accordance with local site policy_

**MITRE ATT&CK Mappings:** TA0007


Section #: 5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.1.5

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/etc/cron.weekly` directory contains system cron jobs that need to run on a weekly basis. The files in this directory cannot be manipulated by the `crontab` command, but are instead edited by system administrators using a text editor. The commands below restrict read/write and search access to user and group root, preventing regular users from accessing this directory.

_Note: Other methods, such as `systemd timers`, exist for scheduling jobs. If another method is used, `cron` should be removed, and the alternate method should be secured in accordance with local site policy_

**MITRE ATT&CK Mappings:** T1053


Section #: 5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.1.5

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/etc/cron.weekly` directory contains system cron jobs that need to run on a weekly basis. The files in this directory cannot be manipulated by the `crontab` command, but are instead edited by system administrators using a text editor. The commands below restrict read/write and search access to user and group root, preventing regular users from accessing this directory.

_Note: Other methods, such as `systemd timers`, exist for scheduling jobs. If another method is used, `cron` should be removed, and the alternate method should be secured in accordance with local site policy_

**MITRE ATT&CK Mappings:** T1053.003


Section #: 5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.1.5

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/etc/cron.weekly` directory contains system cron jobs that need to run on a weekly basis. The files in this directory cannot be manipulated by the `crontab` command, but are instead edited by system administrators using a text editor. The commands below restrict read/write and search access to user and group root, preventing regular users from accessing this directory.

_Note: Other methods, such as `systemd timers`, exist for scheduling jobs. If another method is used, `cron` should be removed, and the alternate method should be secured in accordance with local site policy_

**MITRE ATT&CK Mappings:** M1018


Section #: 5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.1.6

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/etc/cron.monthly` directory contains system cron jobs that need to run on a monthly basis. The files in this directory cannot be manipulated by the `crontab` command, but are instead edited by system administrators using a text editor. The commands below restrict read/write and search access to user and group root, preventing regular users from accessing this directory.

_Note: Other methods, such as `systemd timers`, exist for scheduling jobs. If another method is used, `cron` should be removed, and the alternate method should be secured in accordance with local site policy_

**MITRE ATT&CK Mappings:** TA0002


Section #: 5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.1.6

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/etc/cron.monthly` directory contains system cron jobs that need to run on a monthly basis. The files in this directory cannot be manipulated by the `crontab` command, but are instead edited by system administrators using a text editor. The commands below restrict read/write and search access to user and group root, preventing regular users from accessing this directory.

_Note: Other methods, such as `systemd timers`, exist for scheduling jobs. If another method is used, `cron` should be removed, and the alternate method should be secured in accordance with local site policy_

**MITRE ATT&CK Mappings:** TA0007


Section #: 5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.1.6

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/etc/cron.monthly` directory contains system cron jobs that need to run on a monthly basis. The files in this directory cannot be manipulated by the `crontab` command, but are instead edited by system administrators using a text editor. The commands below restrict read/write and search access to user and group root, preventing regular users from accessing this directory.

_Note: Other methods, such as `systemd timers`, exist for scheduling jobs. If another method is used, `cron` should be removed, and the alternate method should be secured in accordance with local site policy_

**MITRE ATT&CK Mappings:** T1053


Section #: 5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.1.6

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/etc/cron.monthly` directory contains system cron jobs that need to run on a monthly basis. The files in this directory cannot be manipulated by the `crontab` command, but are instead edited by system administrators using a text editor. The commands below restrict read/write and search access to user and group root, preventing regular users from accessing this directory.

_Note: Other methods, such as `systemd timers`, exist for scheduling jobs. If another method is used, `cron` should be removed, and the alternate method should be secured in accordance with local site policy_

**MITRE ATT&CK Mappings:** T1053.003


Section #: 5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.1.6

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/etc/cron.monthly` directory contains system cron jobs that need to run on a monthly basis. The files in this directory cannot be manipulated by the `crontab` command, but are instead edited by system administrators using a text editor. The commands below restrict read/write and search access to user and group root, preventing regular users from accessing this directory.

_Note: Other methods, such as `systemd timers`, exist for scheduling jobs. If another method is used, `cron` should be removed, and the alternate method should be secured in accordance with local site policy_

**MITRE ATT&CK Mappings:** M1018


Section #: 5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.1.7

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/etc/cron.d` directory contains system `cron` jobs that need to run in a similar manner to the hourly, daily weekly and monthly jobs from `/etc/crontab`, but require more granular control as to when they run. The files in this directory cannot be manipulated by the `crontab` command, but are instead edited by system administrators using a text editor. The commands below restrict read/write and search access to user and group root, preventing regular users from accessing this directory.

_Note: Other methods, such as `systemd timers`, exist for scheduling jobs. If another method is used, `cron` should be removed, and the alternate method should be secured in accordance with local site policy_

**MITRE ATT&CK Mappings:** TA0002


Section #: 5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.1.7

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/etc/cron.d` directory contains system `cron` jobs that need to run in a similar manner to the hourly, daily weekly and monthly jobs from `/etc/crontab`, but require more granular control as to when they run. The files in this directory cannot be manipulated by the `crontab` command, but are instead edited by system administrators using a text editor. The commands below restrict read/write and search access to user and group root, preventing regular users from accessing this directory.

_Note: Other methods, such as `systemd timers`, exist for scheduling jobs. If another method is used, `cron` should be removed, and the alternate method should be secured in accordance with local site policy_

**MITRE ATT&CK Mappings:** TA0007


Section #: 5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.1.7

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/etc/cron.d` directory contains system `cron` jobs that need to run in a similar manner to the hourly, daily weekly and monthly jobs from `/etc/crontab`, but require more granular control as to when they run. The files in this directory cannot be manipulated by the `crontab` command, but are instead edited by system administrators using a text editor. The commands below restrict read/write and search access to user and group root, preventing regular users from accessing this directory.

_Note: Other methods, such as `systemd timers`, exist for scheduling jobs. If another method is used, `cron` should be removed, and the alternate method should be secured in accordance with local site policy_

**MITRE ATT&CK Mappings:** T1053


Section #: 5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.1.7

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/etc/cron.d` directory contains system `cron` jobs that need to run in a similar manner to the hourly, daily weekly and monthly jobs from `/etc/crontab`, but require more granular control as to when they run. The files in this directory cannot be manipulated by the `crontab` command, but are instead edited by system administrators using a text editor. The commands below restrict read/write and search access to user and group root, preventing regular users from accessing this directory.

_Note: Other methods, such as `systemd timers`, exist for scheduling jobs. If another method is used, `cron` should be removed, and the alternate method should be secured in accordance with local site policy_

**MITRE ATT&CK Mappings:** T1053.003


Section #: 5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.1.7

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/etc/cron.d` directory contains system `cron` jobs that need to run in a similar manner to the hourly, daily weekly and monthly jobs from `/etc/crontab`, but require more granular control as to when they run. The files in this directory cannot be manipulated by the `crontab` command, but are instead edited by system administrators using a text editor. The commands below restrict read/write and search access to user and group root, preventing regular users from accessing this directory.

_Note: Other methods, such as `systemd timers`, exist for scheduling jobs. If another method is used, `cron` should be removed, and the alternate method should be secured in accordance with local site policy_

**MITRE ATT&CK Mappings:** M1018


Section #: 5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.1.8

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Configure `/etc/cron.allow` to allow specific users to use this service. If `/etc/cron.allow` does not exist, then `/etc/cron.deny` is checked. Any user not specifically defined in this file is allowed to use cron. By removing the file, only users in `/etc/cron.allow` are allowed to use cron.

_Notes:_
- _Other methods, such as `systemd timers`, exist for scheduling jobs. If another method is used, `cron` should be removed, and the alternate method should be secured in accordance with local site policy_
- _Even though a given user is not listed in `cron.allow`, cron jobs can still be run as that user_
- _The `cron.allow` file only controls administrative access to the crontab command for scheduling and modifying cron jobs_

**MITRE ATT&CK Mappings:** TA0002


Section #: 5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.1.8

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Configure `/etc/cron.allow` to allow specific users to use this service. If `/etc/cron.allow` does not exist, then `/etc/cron.deny` is checked. Any user not specifically defined in this file is allowed to use cron. By removing the file, only users in `/etc/cron.allow` are allowed to use cron.

_Notes:_
- _Other methods, such as `systemd timers`, exist for scheduling jobs. If another method is used, `cron` should be removed, and the alternate method should be secured in accordance with local site policy_
- _Even though a given user is not listed in `cron.allow`, cron jobs can still be run as that user_
- _The `cron.allow` file only controls administrative access to the crontab command for scheduling and modifying cron jobs_

**MITRE ATT&CK Mappings:** T1053


Section #: 5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.1.8

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Configure `/etc/cron.allow` to allow specific users to use this service. If `/etc/cron.allow` does not exist, then `/etc/cron.deny` is checked. Any user not specifically defined in this file is allowed to use cron. By removing the file, only users in `/etc/cron.allow` are allowed to use cron.

_Notes:_
- _Other methods, such as `systemd timers`, exist for scheduling jobs. If another method is used, `cron` should be removed, and the alternate method should be secured in accordance with local site policy_
- _Even though a given user is not listed in `cron.allow`, cron jobs can still be run as that user_
- _The `cron.allow` file only controls administrative access to the crontab command for scheduling and modifying cron jobs_

**MITRE ATT&CK Mappings:** T1053.003


Section #: 5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.1.8

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Configure `/etc/cron.allow` to allow specific users to use this service. If `/etc/cron.allow` does not exist, then `/etc/cron.deny` is checked. Any user not specifically defined in this file is allowed to use cron. By removing the file, only users in `/etc/cron.allow` are allowed to use cron.

_Notes:_
- _Other methods, such as `systemd timers`, exist for scheduling jobs. If another method is used, `cron` should be removed, and the alternate method should be secured in accordance with local site policy_
- _Even though a given user is not listed in `cron.allow`, cron jobs can still be run as that user_
- _The `cron.allow` file only controls administrative access to the crontab command for scheduling and modifying cron jobs_

**MITRE ATT&CK Mappings:** M1018


Section #: 5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.1.9

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Configure `/etc/at.allow` to allow specific users to use this service. If `/etc/at.allow` does not exist, then `/etc/at.deny` is checked. Any user not specifically defined in this file is allowed to use `at`. By removing the file, only users in `/etc/at.allow` are allowed to use `at`.

_Note: Other methods, such as `systemd timers`, exist for scheduling jobs. If another method is used, `at` should be removed, and the alternate method should be secured in accordance with local site policy_

**MITRE ATT&CK Mappings:** TA0002


Section #: 5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.1.9

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Configure `/etc/at.allow` to allow specific users to use this service. If `/etc/at.allow` does not exist, then `/etc/at.deny` is checked. Any user not specifically defined in this file is allowed to use `at`. By removing the file, only users in `/etc/at.allow` are allowed to use `at`.

_Note: Other methods, such as `systemd timers`, exist for scheduling jobs. If another method is used, `at` should be removed, and the alternate method should be secured in accordance with local site policy_

**MITRE ATT&CK Mappings:** T1053


Section #: 5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.1.9

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Configure `/etc/at.allow` to allow specific users to use this service. If `/etc/at.allow` does not exist, then `/etc/at.deny` is checked. Any user not specifically defined in this file is allowed to use `at`. By removing the file, only users in `/etc/at.allow` are allowed to use `at`.

_Note: Other methods, such as `systemd timers`, exist for scheduling jobs. If another method is used, `at` should be removed, and the alternate method should be secured in accordance with local site policy_

**MITRE ATT&CK Mappings:** T1053.003


Section #: 5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.1.9

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Configure `/etc/at.allow` to allow specific users to use this service. If `/etc/at.allow` does not exist, then `/etc/at.deny` is checked. Any user not specifically defined in this file is allowed to use `at`. By removing the file, only users in `/etc/at.allow` are allowed to use `at`.

_Note: Other methods, such as `systemd timers`, exist for scheduling jobs. If another method is used, `at` should be removed, and the alternate method should be secured in accordance with local site policy_

**MITRE ATT&CK Mappings:** M1018


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/etc/ssh/sshd_config` file contains configuration specifications for `sshd`. The command below sets the owner and group of the file to `root`.

**MITRE ATT&CK Mappings:** TA0005


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/etc/ssh/sshd_config` file contains configuration specifications for `sshd`. The command below sets the owner and group of the file to `root`.

**MITRE ATT&CK Mappings:** T1098


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/etc/ssh/sshd_config` file contains configuration specifications for `sshd`. The command below sets the owner and group of the file to `root`.

**MITRE ATT&CK Mappings:** T1098.004


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/etc/ssh/sshd_config` file contains configuration specifications for `sshd`. The command below sets the owner and group of the file to `root`.

**MITRE ATT&CK Mappings:** T1543


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/etc/ssh/sshd_config` file contains configuration specifications for `sshd`. The command below sets the owner and group of the file to `root`.

**MITRE ATT&CK Mappings:** T1543.002


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/etc/ssh/sshd_config` file contains configuration specifications for `sshd`. The command below sets the owner and group of the file to `root`.

**MITRE ATT&CK Mappings:** M1022


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** An SSH private key is one of two files used in SSH public key authentication. In this authentication method, the possession of the private key is proof of identity. Only a private key that corresponds to a public key will be able to authenticate successfully. The private keys need to be stored and handled carefully, and no copies of the private key should be distributed.

**MITRE ATT&CK Mappings:** TA0003


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** An SSH private key is one of two files used in SSH public key authentication. In this authentication method, the possession of the private key is proof of identity. Only a private key that corresponds to a public key will be able to authenticate successfully. The private keys need to be stored and handled carefully, and no copies of the private key should be distributed.

**MITRE ATT&CK Mappings:** TA0006


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** An SSH private key is one of two files used in SSH public key authentication. In this authentication method, the possession of the private key is proof of identity. Only a private key that corresponds to a public key will be able to authenticate successfully. The private keys need to be stored and handled carefully, and no copies of the private key should be distributed.

**MITRE ATT&CK Mappings:** T1552


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** An SSH private key is one of two files used in SSH public key authentication. In this authentication method, the possession of the private key is proof of identity. Only a private key that corresponds to a public key will be able to authenticate successfully. The private keys need to be stored and handled carefully, and no copies of the private key should be distributed.

**MITRE ATT&CK Mappings:** T1552.004


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** An SSH private key is one of two files used in SSH public key authentication. In this authentication method, the possession of the private key is proof of identity. Only a private key that corresponds to a public key will be able to authenticate successfully. The private keys need to be stored and handled carefully, and no copies of the private key should be distributed.

**MITRE ATT&CK Mappings:** M1022


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** An SSH public key is one of two files used in SSH public key authentication. In this authentication method, a public key is a key that can be used for verifying digital signatures generated using a corresponding private key. Only a public key that corresponds to a private key will be able to authenticate successfully.

**MITRE ATT&CK Mappings:** TA0003


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** An SSH public key is one of two files used in SSH public key authentication. In this authentication method, a public key is a key that can be used for verifying digital signatures generated using a corresponding private key. Only a public key that corresponds to a private key will be able to authenticate successfully.

**MITRE ATT&CK Mappings:** TA0006


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** An SSH public key is one of two files used in SSH public key authentication. In this authentication method, a public key is a key that can be used for verifying digital signatures generated using a corresponding private key. Only a public key that corresponds to a private key will be able to authenticate successfully.

**MITRE ATT&CK Mappings:** T1557


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** An SSH public key is one of two files used in SSH public key authentication. In this authentication method, a public key is a key that can be used for verifying digital signatures generated using a corresponding private key. Only a public key that corresponds to a private key will be able to authenticate successfully.

**MITRE ATT&CK Mappings:** T1557.000


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** An SSH public key is one of two files used in SSH public key authentication. In this authentication method, a public key is a key that can be used for verifying digital signatures generated using a corresponding private key. Only a public key that corresponds to a private key will be able to authenticate successfully.

**MITRE ATT&CK Mappings:** M1022


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** There are several options available to limit which users and group can access the system via SSH. It is recommended that at least one of the following options be leveraged:

- `AllowUsers`:
 - The `AllowUsers` variable gives the system administrator the option of allowing specific users to `ssh` into the system. The list consists of space separated user names. Numeric user IDs are not recognized with this variable. If a system administrator wants to restrict user access further by only allowing the allowed users to log in from a particular host, the entry can be specified in the form of user@host.
- `AllowGroups`:
 - The `AllowGroups` variable gives the system administrator the option of allowing specific groups of users to `ssh` into the system. The list consists of space separated group names. Numeric group IDs are not recognized with this variable.
- `DenyUsers`:
 - The `DenyUsers` variable gives the system administrator the option of denying specific users to `ssh` into the system. The list consists of space separated user names. Numeric user IDs are not recognized with this variable. If a system administrator wants to restrict user access further by specifically denying a user's access from a particular host, the entry can be specified in the form of user@host.
- `DenyGroups`:
 - The `DenyGroups` variable gives the system administrator the option of denying specific groups of users to `ssh` into the system. The list consists of space separated group names. Numeric group IDs are not recognized with this variable.

**MITRE ATT&CK Mappings:** TA0008


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** There are several options available to limit which users and group can access the system via SSH. It is recommended that at least one of the following options be leveraged:

- `AllowUsers`:
 - The `AllowUsers` variable gives the system administrator the option of allowing specific users to `ssh` into the system. The list consists of space separated user names. Numeric user IDs are not recognized with this variable. If a system administrator wants to restrict user access further by only allowing the allowed users to log in from a particular host, the entry can be specified in the form of user@host.
- `AllowGroups`:
 - The `AllowGroups` variable gives the system administrator the option of allowing specific groups of users to `ssh` into the system. The list consists of space separated group names. Numeric group IDs are not recognized with this variable.
- `DenyUsers`:
 - The `DenyUsers` variable gives the system administrator the option of denying specific users to `ssh` into the system. The list consists of space separated user names. Numeric user IDs are not recognized with this variable. If a system administrator wants to restrict user access further by specifically denying a user's access from a particular host, the entry can be specified in the form of user@host.
- `DenyGroups`:
 - The `DenyGroups` variable gives the system administrator the option of denying specific groups of users to `ssh` into the system. The list consists of space separated group names. Numeric group IDs are not recognized with this variable.

**MITRE ATT&CK Mappings:** T1021


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** There are several options available to limit which users and group can access the system via SSH. It is recommended that at least one of the following options be leveraged:

- `AllowUsers`:
 - The `AllowUsers` variable gives the system administrator the option of allowing specific users to `ssh` into the system. The list consists of space separated user names. Numeric user IDs are not recognized with this variable. If a system administrator wants to restrict user access further by only allowing the allowed users to log in from a particular host, the entry can be specified in the form of user@host.
- `AllowGroups`:
 - The `AllowGroups` variable gives the system administrator the option of allowing specific groups of users to `ssh` into the system. The list consists of space separated group names. Numeric group IDs are not recognized with this variable.
- `DenyUsers`:
 - The `DenyUsers` variable gives the system administrator the option of denying specific users to `ssh` into the system. The list consists of space separated user names. Numeric user IDs are not recognized with this variable. If a system administrator wants to restrict user access further by specifically denying a user's access from a particular host, the entry can be specified in the form of user@host.
- `DenyGroups`:
 - The `DenyGroups` variable gives the system administrator the option of denying specific groups of users to `ssh` into the system. The list consists of space separated group names. Numeric group IDs are not recognized with this variable.

**MITRE ATT&CK Mappings:** T1021.004


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** There are several options available to limit which users and group can access the system via SSH. It is recommended that at least one of the following options be leveraged:

- `AllowUsers`:
 - The `AllowUsers` variable gives the system administrator the option of allowing specific users to `ssh` into the system. The list consists of space separated user names. Numeric user IDs are not recognized with this variable. If a system administrator wants to restrict user access further by only allowing the allowed users to log in from a particular host, the entry can be specified in the form of user@host.
- `AllowGroups`:
 - The `AllowGroups` variable gives the system administrator the option of allowing specific groups of users to `ssh` into the system. The list consists of space separated group names. Numeric group IDs are not recognized with this variable.
- `DenyUsers`:
 - The `DenyUsers` variable gives the system administrator the option of denying specific users to `ssh` into the system. The list consists of space separated user names. Numeric user IDs are not recognized with this variable. If a system administrator wants to restrict user access further by specifically denying a user's access from a particular host, the entry can be specified in the form of user@host.
- `DenyGroups`:
 - The `DenyGroups` variable gives the system administrator the option of denying specific groups of users to `ssh` into the system. The list consists of space separated group names. Numeric group IDs are not recognized with this variable.

**MITRE ATT&CK Mappings:** M1018


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.5

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** `INFO` level is the basic level that only records login activity of SSH users. In many situations, such as Incident Response, it is important to determine when a particular user was active on a system. The logout record can eliminate those users who disconnected, which helps narrow the field.

`VERBOSE` level specifies that login and logout activity as well as the key fingerprint for any SSH key used for login will be logged. This information is important for SSH key management, especially in legacy environments.

**MITRE ATT&CK Mappings:** TA0005


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.5

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** `INFO` level is the basic level that only records login activity of SSH users. In many situations, such as Incident Response, it is important to determine when a particular user was active on a system. The logout record can eliminate those users who disconnected, which helps narrow the field.

`VERBOSE` level specifies that login and logout activity as well as the key fingerprint for any SSH key used for login will be logged. This information is important for SSH key management, especially in legacy environments.

**MITRE ATT&CK Mappings:** T1562


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.5

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** `INFO` level is the basic level that only records login activity of SSH users. In many situations, such as Incident Response, it is important to determine when a particular user was active on a system. The logout record can eliminate those users who disconnected, which helps narrow the field.

`VERBOSE` level specifies that login and logout activity as well as the key fingerprint for any SSH key used for login will be logged. This information is important for SSH key management, especially in legacy environments.

**MITRE ATT&CK Mappings:** T1562.006


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.6

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `UsePAM` directive enables the Pluggable Authentication Module (PAM) interface. If set to `yes` this will enable PAM authentication using `ChallengeResponseAuthentication` and `PasswordAuthentication` directives in addition to PAM account and session module processing for all authentication types.

**MITRE ATT&CK Mappings:** TA0001


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.6

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `UsePAM` directive enables the Pluggable Authentication Module (PAM) interface. If set to `yes` this will enable PAM authentication using `ChallengeResponseAuthentication` and `PasswordAuthentication` directives in addition to PAM account and session module processing for all authentication types.

**MITRE ATT&CK Mappings:** T1021


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.6

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `UsePAM` directive enables the Pluggable Authentication Module (PAM) interface. If set to `yes` this will enable PAM authentication using `ChallengeResponseAuthentication` and `PasswordAuthentication` directives in addition to PAM account and session module processing for all authentication types.

**MITRE ATT&CK Mappings:** T1021.004


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.6

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `UsePAM` directive enables the Pluggable Authentication Module (PAM) interface. If set to `yes` this will enable PAM authentication using `ChallengeResponseAuthentication` and `PasswordAuthentication` directives in addition to PAM account and session module processing for all authentication types.

**MITRE ATT&CK Mappings:** M1035


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.7

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `PermitRootLogin` parameter specifies if the root user can log in using SSH. The default is `prohibit-password`.

**MITRE ATT&CK Mappings:** TA0008


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.7

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `PermitRootLogin` parameter specifies if the root user can log in using SSH. The default is `prohibit-password`.

**MITRE ATT&CK Mappings:** T1021


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.7

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `PermitRootLogin` parameter specifies if the root user can log in using SSH. The default is `prohibit-password`.

**MITRE ATT&CK Mappings:** M1042


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.8

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `HostbasedAuthentication` parameter specifies if authentication is allowed through trusted hosts via the user of `.rhosts`, or `/etc/hosts.equiv`, along with successful public key client host authentication.

**MITRE ATT&CK Mappings:** TA0001


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.8

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `HostbasedAuthentication` parameter specifies if authentication is allowed through trusted hosts via the user of `.rhosts`, or `/etc/hosts.equiv`, along with successful public key client host authentication.

**MITRE ATT&CK Mappings:** T1078


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.8

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `HostbasedAuthentication` parameter specifies if authentication is allowed through trusted hosts via the user of `.rhosts`, or `/etc/hosts.equiv`, along with successful public key client host authentication.

**MITRE ATT&CK Mappings:** T1078.001


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.8

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `HostbasedAuthentication` parameter specifies if authentication is allowed through trusted hosts via the user of `.rhosts`, or `/etc/hosts.equiv`, along with successful public key client host authentication.

**MITRE ATT&CK Mappings:** T1078.003


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.8

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `HostbasedAuthentication` parameter specifies if authentication is allowed through trusted hosts via the user of `.rhosts`, or `/etc/hosts.equiv`, along with successful public key client host authentication.

**MITRE ATT&CK Mappings:** M1042


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.9

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `PermitEmptyPasswords` parameter specifies if the SSH server allows login to accounts with empty password strings.

**MITRE ATT&CK Mappings:** TA0008


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.9

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `PermitEmptyPasswords` parameter specifies if the SSH server allows login to accounts with empty password strings.

**MITRE ATT&CK Mappings:** T1021


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.9

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `PermitEmptyPasswords` parameter specifies if the SSH server allows login to accounts with empty password strings.

**MITRE ATT&CK Mappings:** M1042


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.10

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `PermitUserEnvironment` option allows users to present environment options to the SSH daemon.

**MITRE ATT&CK Mappings:** TA0008


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.10

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `PermitUserEnvironment` option allows users to present environment options to the SSH daemon.

**MITRE ATT&CK Mappings:** T1021


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.10

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `PermitUserEnvironment` option allows users to present environment options to the SSH daemon.

**MITRE ATT&CK Mappings:** M1042


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.11

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `IgnoreRhosts` parameter specifies that `.rhosts` and `.shosts` files will not be used in `RhostsRSAAuthentication` or `HostbasedAuthentication`.

**MITRE ATT&CK Mappings:** TA0001


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.11

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `IgnoreRhosts` parameter specifies that `.rhosts` and `.shosts` files will not be used in `RhostsRSAAuthentication` or `HostbasedAuthentication`.

**MITRE ATT&CK Mappings:** T1078


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.11

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `IgnoreRhosts` parameter specifies that `.rhosts` and `.shosts` files will not be used in `RhostsRSAAuthentication` or `HostbasedAuthentication`.

**MITRE ATT&CK Mappings:** T1078.001


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.11

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `IgnoreRhosts` parameter specifies that `.rhosts` and `.shosts` files will not be used in `RhostsRSAAuthentication` or `HostbasedAuthentication`.

**MITRE ATT&CK Mappings:** T1078.003


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.11

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `IgnoreRhosts` parameter specifies that `.rhosts` and `.shosts` files will not be used in `RhostsRSAAuthentication` or `HostbasedAuthentication`.

**MITRE ATT&CK Mappings:** M1027


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.12

**Profiles:** Level 1 - Workstation, Level 2 - Server

**Description:** The `X11Forwarding` parameter provides the ability to tunnel X11 traffic through the connection to enable remote graphic connections.

**MITRE ATT&CK Mappings:** TA0008


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.12

**Profiles:** Level 1 - Workstation, Level 2 - Server

**Description:** The `X11Forwarding` parameter provides the ability to tunnel X11 traffic through the connection to enable remote graphic connections.

**MITRE ATT&CK Mappings:** T1210


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.12

**Profiles:** Level 1 - Workstation, Level 2 - Server

**Description:** The `X11Forwarding` parameter provides the ability to tunnel X11 traffic through the connection to enable remote graphic connections.

**MITRE ATT&CK Mappings:** T1210.000


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.12

**Profiles:** Level 1 - Workstation, Level 2 - Server

**Description:** The `X11Forwarding` parameter provides the ability to tunnel X11 traffic through the connection to enable remote graphic connections.

**MITRE ATT&CK Mappings:** M1042


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.13

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** This variable limits the ciphers that SSH can use during communication.

*Note:*
- Some organizations may have stricter requirements for approved ciphers.
- Ensure that ciphers used are in compliance with site policy.
- The only "strong" ciphers currently FIPS 140-2 compliant are:
 - aes256-ctr
 - aes192-ctr
 - aes128-ctr
- Supported ciphers in openSSH 8.2:

```
3des-cbc
aes128-cbc
aes192-cbc
aes256-cbc
aes128-ctr
aes192-ctr
aes256-ctr
aes128-gcm@openssh.com
aes256-gcm@openssh.com
chacha20-poly1305@openssh.com
```

**MITRE ATT&CK Mappings:** TA0006


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.13

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** This variable limits the ciphers that SSH can use during communication.

*Note:*
- Some organizations may have stricter requirements for approved ciphers.
- Ensure that ciphers used are in compliance with site policy.
- The only "strong" ciphers currently FIPS 140-2 compliant are:
 - aes256-ctr
 - aes192-ctr
 - aes128-ctr
- Supported ciphers in openSSH 8.2:

```
3des-cbc
aes128-cbc
aes192-cbc
aes256-cbc
aes128-ctr
aes192-ctr
aes256-ctr
aes128-gcm@openssh.com
aes256-gcm@openssh.com
chacha20-poly1305@openssh.com
```

**MITRE ATT&CK Mappings:** T1040


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.13

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** This variable limits the ciphers that SSH can use during communication.

*Note:*
- Some organizations may have stricter requirements for approved ciphers.
- Ensure that ciphers used are in compliance with site policy.
- The only "strong" ciphers currently FIPS 140-2 compliant are:
 - aes256-ctr
 - aes192-ctr
 - aes128-ctr
- Supported ciphers in openSSH 8.2:

```
3des-cbc
aes128-cbc
aes192-cbc
aes256-cbc
aes128-ctr
aes192-ctr
aes256-ctr
aes128-gcm@openssh.com
aes256-gcm@openssh.com
chacha20-poly1305@openssh.com
```

**MITRE ATT&CK Mappings:** T1040.000


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.13

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** This variable limits the ciphers that SSH can use during communication.

*Note:*
- Some organizations may have stricter requirements for approved ciphers.
- Ensure that ciphers used are in compliance with site policy.
- The only "strong" ciphers currently FIPS 140-2 compliant are:
 - aes256-ctr
 - aes192-ctr
 - aes128-ctr
- Supported ciphers in openSSH 8.2:

```
3des-cbc
aes128-cbc
aes192-cbc
aes256-cbc
aes128-ctr
aes192-ctr
aes256-ctr
aes128-gcm@openssh.com
aes256-gcm@openssh.com
chacha20-poly1305@openssh.com
```

**MITRE ATT&CK Mappings:** T1557


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.13

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** This variable limits the ciphers that SSH can use during communication.

*Note:*
- Some organizations may have stricter requirements for approved ciphers.
- Ensure that ciphers used are in compliance with site policy.
- The only "strong" ciphers currently FIPS 140-2 compliant are:
 - aes256-ctr
 - aes192-ctr
 - aes128-ctr
- Supported ciphers in openSSH 8.2:

```
3des-cbc
aes128-cbc
aes192-cbc
aes256-cbc
aes128-ctr
aes192-ctr
aes256-ctr
aes128-gcm@openssh.com
aes256-gcm@openssh.com
chacha20-poly1305@openssh.com
```

**MITRE ATT&CK Mappings:** M1041


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.14

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** This variable limits the types of MAC algorithms that SSH can use during communication.

*Notes:*
- Some organizations may have stricter requirements for approved MACs.
- Ensure that MACs used are in compliance with site policy.
- The only "strong" MACs currently FIPS 140-2 approved are:
 - hmac-sha2-256
 - hmac-sha2-512
- The Supported MACs are:

```
hmac-md5
hmac-md5-96
hmac-sha1
hmac-sha1-96
hmac-sha2-256
hmac-sha2-512
umac-64@openssh.com
umac-128@openssh.com
hmac-md5-etm@openssh.com
hmac-md5-96-etm@openssh.com
hmac-sha1-etm@openssh.com
hmac-sha1-96-etm@openssh.com
hmac-sha2-256-etm@openssh.com
hmac-sha2-512-etm@openssh.com
umac-64-etm@openssh.com
umac-128-etm@openssh.com
```

**MITRE ATT&CK Mappings:** TA0006


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.14

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** This variable limits the types of MAC algorithms that SSH can use during communication.

*Notes:*
- Some organizations may have stricter requirements for approved MACs.
- Ensure that MACs used are in compliance with site policy.
- The only "strong" MACs currently FIPS 140-2 approved are:
 - hmac-sha2-256
 - hmac-sha2-512
- The Supported MACs are:

```
hmac-md5
hmac-md5-96
hmac-sha1
hmac-sha1-96
hmac-sha2-256
hmac-sha2-512
umac-64@openssh.com
umac-128@openssh.com
hmac-md5-etm@openssh.com
hmac-md5-96-etm@openssh.com
hmac-sha1-etm@openssh.com
hmac-sha1-96-etm@openssh.com
hmac-sha2-256-etm@openssh.com
hmac-sha2-512-etm@openssh.com
umac-64-etm@openssh.com
umac-128-etm@openssh.com
```

**MITRE ATT&CK Mappings:** T1040


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.14

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** This variable limits the types of MAC algorithms that SSH can use during communication.

*Notes:*
- Some organizations may have stricter requirements for approved MACs.
- Ensure that MACs used are in compliance with site policy.
- The only "strong" MACs currently FIPS 140-2 approved are:
 - hmac-sha2-256
 - hmac-sha2-512
- The Supported MACs are:

```
hmac-md5
hmac-md5-96
hmac-sha1
hmac-sha1-96
hmac-sha2-256
hmac-sha2-512
umac-64@openssh.com
umac-128@openssh.com
hmac-md5-etm@openssh.com
hmac-md5-96-etm@openssh.com
hmac-sha1-etm@openssh.com
hmac-sha1-96-etm@openssh.com
hmac-sha2-256-etm@openssh.com
hmac-sha2-512-etm@openssh.com
umac-64-etm@openssh.com
umac-128-etm@openssh.com
```

**MITRE ATT&CK Mappings:** T1040.000


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.14

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** This variable limits the types of MAC algorithms that SSH can use during communication.

*Notes:*
- Some organizations may have stricter requirements for approved MACs.
- Ensure that MACs used are in compliance with site policy.
- The only "strong" MACs currently FIPS 140-2 approved are:
 - hmac-sha2-256
 - hmac-sha2-512
- The Supported MACs are:

```
hmac-md5
hmac-md5-96
hmac-sha1
hmac-sha1-96
hmac-sha2-256
hmac-sha2-512
umac-64@openssh.com
umac-128@openssh.com
hmac-md5-etm@openssh.com
hmac-md5-96-etm@openssh.com
hmac-sha1-etm@openssh.com
hmac-sha1-96-etm@openssh.com
hmac-sha2-256-etm@openssh.com
hmac-sha2-512-etm@openssh.com
umac-64-etm@openssh.com
umac-128-etm@openssh.com
```

**MITRE ATT&CK Mappings:** T1557


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.14

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** This variable limits the types of MAC algorithms that SSH can use during communication.

*Notes:*
- Some organizations may have stricter requirements for approved MACs.
- Ensure that MACs used are in compliance with site policy.
- The only "strong" MACs currently FIPS 140-2 approved are:
 - hmac-sha2-256
 - hmac-sha2-512
- The Supported MACs are:

```
hmac-md5
hmac-md5-96
hmac-sha1
hmac-sha1-96
hmac-sha2-256
hmac-sha2-512
umac-64@openssh.com
umac-128@openssh.com
hmac-md5-etm@openssh.com
hmac-md5-96-etm@openssh.com
hmac-sha1-etm@openssh.com
hmac-sha1-96-etm@openssh.com
hmac-sha2-256-etm@openssh.com
hmac-sha2-512-etm@openssh.com
umac-64-etm@openssh.com
umac-128-etm@openssh.com
```

**MITRE ATT&CK Mappings:** T1557.000


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.14

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** This variable limits the types of MAC algorithms that SSH can use during communication.

*Notes:*
- Some organizations may have stricter requirements for approved MACs.
- Ensure that MACs used are in compliance with site policy.
- The only "strong" MACs currently FIPS 140-2 approved are:
 - hmac-sha2-256
 - hmac-sha2-512
- The Supported MACs are:

```
hmac-md5
hmac-md5-96
hmac-sha1
hmac-sha1-96
hmac-sha2-256
hmac-sha2-512
umac-64@openssh.com
umac-128@openssh.com
hmac-md5-etm@openssh.com
hmac-md5-96-etm@openssh.com
hmac-sha1-etm@openssh.com
hmac-sha1-96-etm@openssh.com
hmac-sha2-256-etm@openssh.com
hmac-sha2-512-etm@openssh.com
umac-64-etm@openssh.com
umac-128-etm@openssh.com
```

**MITRE ATT&CK Mappings:** M1041


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.15

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Key exchange is any method in cryptography by which cryptographic keys are exchanged between two parties, allowing use of a cryptographic algorithm. If the sender and receiver wish to exchange encrypted messages, each must be equipped to encrypt messages to be sent and decrypt messages received

_Notes:_
- _Kex algorithms have a higher preference the earlier they appear in the list_
- _Some organizations may have stricter requirements for approved Key exchange algorithms_
- _Ensure that Key exchange algorithms used are in compliance with site policy_
- _The only Key Exchange Algorithms currently FIPS 140-2 approved are:_
 - _ecdh-sha2-nistp256_
 - _ecdh-sha2-nistp384_
 - _ecdh-sha2-nistp521_
 - _diffie-hellman-group-exchange-sha256_
 - _diffie-hellman-group16-sha512_
 - _diffie-hellman-group18-sha512_
 - _diffie-hellman-group14-sha256_
- _The Key Exchange algorithms supported by OpenSSH 8.2 are:_
```
curve25519-sha256
curve25519-sha256@libssh.org
diffie-hellman-group1-sha1
diffie-hellman-group14-sha1
diffie-hellman-group14-sha256
diffie-hellman-group16-sha512
diffie-hellman-group18-sha512
diffie-hellman-group-exchange-sha1
diffie-hellman-group-exchange-sha256
ecdh-sha2-nistp256
ecdh-sha2-nistp384
ecdh-sha2-nistp521
sntrup4591761x25519-sha512@tinyssh.org
```

**MITRE ATT&CK Mappings:** TA0006


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.15

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Key exchange is any method in cryptography by which cryptographic keys are exchanged between two parties, allowing use of a cryptographic algorithm. If the sender and receiver wish to exchange encrypted messages, each must be equipped to encrypt messages to be sent and decrypt messages received

_Notes:_
- _Kex algorithms have a higher preference the earlier they appear in the list_
- _Some organizations may have stricter requirements for approved Key exchange algorithms_
- _Ensure that Key exchange algorithms used are in compliance with site policy_
- _The only Key Exchange Algorithms currently FIPS 140-2 approved are:_
 - _ecdh-sha2-nistp256_
 - _ecdh-sha2-nistp384_
 - _ecdh-sha2-nistp521_
 - _diffie-hellman-group-exchange-sha256_
 - _diffie-hellman-group16-sha512_
 - _diffie-hellman-group18-sha512_
 - _diffie-hellman-group14-sha256_
- _The Key Exchange algorithms supported by OpenSSH 8.2 are:_
```
curve25519-sha256
curve25519-sha256@libssh.org
diffie-hellman-group1-sha1
diffie-hellman-group14-sha1
diffie-hellman-group14-sha256
diffie-hellman-group16-sha512
diffie-hellman-group18-sha512
diffie-hellman-group-exchange-sha1
diffie-hellman-group-exchange-sha256
ecdh-sha2-nistp256
ecdh-sha2-nistp384
ecdh-sha2-nistp521
sntrup4591761x25519-sha512@tinyssh.org
```

**MITRE ATT&CK Mappings:** T1040


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.15

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Key exchange is any method in cryptography by which cryptographic keys are exchanged between two parties, allowing use of a cryptographic algorithm. If the sender and receiver wish to exchange encrypted messages, each must be equipped to encrypt messages to be sent and decrypt messages received

_Notes:_
- _Kex algorithms have a higher preference the earlier they appear in the list_
- _Some organizations may have stricter requirements for approved Key exchange algorithms_
- _Ensure that Key exchange algorithms used are in compliance with site policy_
- _The only Key Exchange Algorithms currently FIPS 140-2 approved are:_
 - _ecdh-sha2-nistp256_
 - _ecdh-sha2-nistp384_
 - _ecdh-sha2-nistp521_
 - _diffie-hellman-group-exchange-sha256_
 - _diffie-hellman-group16-sha512_
 - _diffie-hellman-group18-sha512_
 - _diffie-hellman-group14-sha256_
- _The Key Exchange algorithms supported by OpenSSH 8.2 are:_
```
curve25519-sha256
curve25519-sha256@libssh.org
diffie-hellman-group1-sha1
diffie-hellman-group14-sha1
diffie-hellman-group14-sha256
diffie-hellman-group16-sha512
diffie-hellman-group18-sha512
diffie-hellman-group-exchange-sha1
diffie-hellman-group-exchange-sha256
ecdh-sha2-nistp256
ecdh-sha2-nistp384
ecdh-sha2-nistp521
sntrup4591761x25519-sha512@tinyssh.org
```

**MITRE ATT&CK Mappings:** T1040.000


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.15

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Key exchange is any method in cryptography by which cryptographic keys are exchanged between two parties, allowing use of a cryptographic algorithm. If the sender and receiver wish to exchange encrypted messages, each must be equipped to encrypt messages to be sent and decrypt messages received

_Notes:_
- _Kex algorithms have a higher preference the earlier they appear in the list_
- _Some organizations may have stricter requirements for approved Key exchange algorithms_
- _Ensure that Key exchange algorithms used are in compliance with site policy_
- _The only Key Exchange Algorithms currently FIPS 140-2 approved are:_
 - _ecdh-sha2-nistp256_
 - _ecdh-sha2-nistp384_
 - _ecdh-sha2-nistp521_
 - _diffie-hellman-group-exchange-sha256_
 - _diffie-hellman-group16-sha512_
 - _diffie-hellman-group18-sha512_
 - _diffie-hellman-group14-sha256_
- _The Key Exchange algorithms supported by OpenSSH 8.2 are:_
```
curve25519-sha256
curve25519-sha256@libssh.org
diffie-hellman-group1-sha1
diffie-hellman-group14-sha1
diffie-hellman-group14-sha256
diffie-hellman-group16-sha512
diffie-hellman-group18-sha512
diffie-hellman-group-exchange-sha1
diffie-hellman-group-exchange-sha256
ecdh-sha2-nistp256
ecdh-sha2-nistp384
ecdh-sha2-nistp521
sntrup4591761x25519-sha512@tinyssh.org
```

**MITRE ATT&CK Mappings:** T1557


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.15

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Key exchange is any method in cryptography by which cryptographic keys are exchanged between two parties, allowing use of a cryptographic algorithm. If the sender and receiver wish to exchange encrypted messages, each must be equipped to encrypt messages to be sent and decrypt messages received

_Notes:_
- _Kex algorithms have a higher preference the earlier they appear in the list_
- _Some organizations may have stricter requirements for approved Key exchange algorithms_
- _Ensure that Key exchange algorithms used are in compliance with site policy_
- _The only Key Exchange Algorithms currently FIPS 140-2 approved are:_
 - _ecdh-sha2-nistp256_
 - _ecdh-sha2-nistp384_
 - _ecdh-sha2-nistp521_
 - _diffie-hellman-group-exchange-sha256_
 - _diffie-hellman-group16-sha512_
 - _diffie-hellman-group18-sha512_
 - _diffie-hellman-group14-sha256_
- _The Key Exchange algorithms supported by OpenSSH 8.2 are:_
```
curve25519-sha256
curve25519-sha256@libssh.org
diffie-hellman-group1-sha1
diffie-hellman-group14-sha1
diffie-hellman-group14-sha256
diffie-hellman-group16-sha512
diffie-hellman-group18-sha512
diffie-hellman-group-exchange-sha1
diffie-hellman-group-exchange-sha256
ecdh-sha2-nistp256
ecdh-sha2-nistp384
ecdh-sha2-nistp521
sntrup4591761x25519-sha512@tinyssh.org
```

**MITRE ATT&CK Mappings:** T1557.000


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.15

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Key exchange is any method in cryptography by which cryptographic keys are exchanged between two parties, allowing use of a cryptographic algorithm. If the sender and receiver wish to exchange encrypted messages, each must be equipped to encrypt messages to be sent and decrypt messages received

_Notes:_
- _Kex algorithms have a higher preference the earlier they appear in the list_
- _Some organizations may have stricter requirements for approved Key exchange algorithms_
- _Ensure that Key exchange algorithms used are in compliance with site policy_
- _The only Key Exchange Algorithms currently FIPS 140-2 approved are:_
 - _ecdh-sha2-nistp256_
 - _ecdh-sha2-nistp384_
 - _ecdh-sha2-nistp521_
 - _diffie-hellman-group-exchange-sha256_
 - _diffie-hellman-group16-sha512_
 - _diffie-hellman-group18-sha512_
 - _diffie-hellman-group14-sha256_
- _The Key Exchange algorithms supported by OpenSSH 8.2 are:_
```
curve25519-sha256
curve25519-sha256@libssh.org
diffie-hellman-group1-sha1
diffie-hellman-group14-sha1
diffie-hellman-group14-sha256
diffie-hellman-group16-sha512
diffie-hellman-group18-sha512
diffie-hellman-group-exchange-sha1
diffie-hellman-group-exchange-sha256
ecdh-sha2-nistp256
ecdh-sha2-nistp384
ecdh-sha2-nistp521
sntrup4591761x25519-sha512@tinyssh.org
```

**MITRE ATT&CK Mappings:** M1041


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.16

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** SSH port forwarding is a mechanism in SSH for tunneling application ports from the client to the server, or servers to clients. It can be used for adding encryption to legacy applications, going through firewalls, and some system administrators and IT professionals use it for opening backdoors into the internal network from their home machines.

**MITRE ATT&CK Mappings:** TA0008


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.16

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** SSH port forwarding is a mechanism in SSH for tunneling application ports from the client to the server, or servers to clients. It can be used for adding encryption to legacy applications, going through firewalls, and some system administrators and IT professionals use it for opening backdoors into the internal network from their home machines.

**MITRE ATT&CK Mappings:** T1048


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.16

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** SSH port forwarding is a mechanism in SSH for tunneling application ports from the client to the server, or servers to clients. It can be used for adding encryption to legacy applications, going through firewalls, and some system administrators and IT professionals use it for opening backdoors into the internal network from their home machines.

**MITRE ATT&CK Mappings:** T1048.002


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.16

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** SSH port forwarding is a mechanism in SSH for tunneling application ports from the client to the server, or servers to clients. It can be used for adding encryption to legacy applications, going through firewalls, and some system administrators and IT professionals use it for opening backdoors into the internal network from their home machines.

**MITRE ATT&CK Mappings:** T1572


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.16

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** SSH port forwarding is a mechanism in SSH for tunneling application ports from the client to the server, or servers to clients. It can be used for adding encryption to legacy applications, going through firewalls, and some system administrators and IT professionals use it for opening backdoors into the internal network from their home machines.

**MITRE ATT&CK Mappings:** T1572.000


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.16

**Profiles:** Level 2 - Server, Level 2 - Workstation

**Description:** SSH port forwarding is a mechanism in SSH for tunneling application ports from the client to the server, or servers to clients. It can be used for adding encryption to legacy applications, going through firewalls, and some system administrators and IT professionals use it for opening backdoors into the internal network from their home machines.

**MITRE ATT&CK Mappings:** M1042


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.17

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `Banner` parameter specifies a file whose contents must be sent to the remote user before authentication is permitted. By default, no banner is displayed.

**MITRE ATT&CK Mappings:** TA0001


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.17

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `Banner` parameter specifies a file whose contents must be sent to the remote user before authentication is permitted. By default, no banner is displayed.

**MITRE ATT&CK Mappings:** TA0007


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.17

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `Banner` parameter specifies a file whose contents must be sent to the remote user before authentication is permitted. By default, no banner is displayed.

**MITRE ATT&CK Mappings:** M1035


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.18

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `MaxAuthTries` parameter specifies the maximum number of authentication attempts permitted per connection. When the login failure count reaches half the number, error messages will be written to the `syslog` file detailing the login failure.

**MITRE ATT&CK Mappings:** TA0006


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.18

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `MaxAuthTries` parameter specifies the maximum number of authentication attempts permitted per connection. When the login failure count reaches half the number, error messages will be written to the `syslog` file detailing the login failure.

**MITRE ATT&CK Mappings:** T1110


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.18

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `MaxAuthTries` parameter specifies the maximum number of authentication attempts permitted per connection. When the login failure count reaches half the number, error messages will be written to the `syslog` file detailing the login failure.

**MITRE ATT&CK Mappings:** T1110.001


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.18

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `MaxAuthTries` parameter specifies the maximum number of authentication attempts permitted per connection. When the login failure count reaches half the number, error messages will be written to the `syslog` file detailing the login failure.

**MITRE ATT&CK Mappings:** T1110.003


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.18

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `MaxAuthTries` parameter specifies the maximum number of authentication attempts permitted per connection. When the login failure count reaches half the number, error messages will be written to the `syslog` file detailing the login failure.

**MITRE ATT&CK Mappings:** M1036


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.19

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `MaxStartups` parameter specifies the maximum number of concurrent unauthenticated connections to the SSH daemon.

**MITRE ATT&CK Mappings:** TA0040


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.19

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `MaxStartups` parameter specifies the maximum number of concurrent unauthenticated connections to the SSH daemon.

**MITRE ATT&CK Mappings:** T1499


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.19

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `MaxStartups` parameter specifies the maximum number of concurrent unauthenticated connections to the SSH daemon.

**MITRE ATT&CK Mappings:** T1499.002


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.20

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `MaxSessions` parameter specifies the maximum number of open sessions permitted from a given connection.

**MITRE ATT&CK Mappings:** TA0040


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.20

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `MaxSessions` parameter specifies the maximum number of open sessions permitted from a given connection.

**MITRE ATT&CK Mappings:** T1499


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.20

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `MaxSessions` parameter specifies the maximum number of open sessions permitted from a given connection.

**MITRE ATT&CK Mappings:** T1499.002


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.21

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `LoginGraceTime` parameter specifies the time allowed for successful authentication to the SSH server. The longer the Grace period is the more open unauthenticated connections can exist. Like other session controls in this session the Grace Period should be limited to appropriate organizational limits to ensure the service is available for needed access.

**MITRE ATT&CK Mappings:** TA0006


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.21

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `LoginGraceTime` parameter specifies the time allowed for successful authentication to the SSH server. The longer the Grace period is the more open unauthenticated connections can exist. Like other session controls in this session the Grace Period should be limited to appropriate organizational limits to ensure the service is available for needed access.

**MITRE ATT&CK Mappings:** T1110


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.21

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `LoginGraceTime` parameter specifies the time allowed for successful authentication to the SSH server. The longer the Grace period is the more open unauthenticated connections can exist. Like other session controls in this session the Grace Period should be limited to appropriate organizational limits to ensure the service is available for needed access.

**MITRE ATT&CK Mappings:** T1110.001


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.21

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `LoginGraceTime` parameter specifies the time allowed for successful authentication to the SSH server. The longer the Grace period is the more open unauthenticated connections can exist. Like other session controls in this session the Grace Period should be limited to appropriate organizational limits to ensure the service is available for needed access.

**MITRE ATT&CK Mappings:** T1110.003


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.21

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `LoginGraceTime` parameter specifies the time allowed for successful authentication to the SSH server. The longer the Grace period is the more open unauthenticated connections can exist. Like other session controls in this session the Grace Period should be limited to appropriate organizational limits to ensure the service is available for needed access.

**MITRE ATT&CK Mappings:** T1110.004


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.21

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `LoginGraceTime` parameter specifies the time allowed for successful authentication to the SSH server. The longer the Grace period is the more open unauthenticated connections can exist. Like other session controls in this session the Grace Period should be limited to appropriate organizational limits to ensure the service is available for needed access.

**MITRE ATT&CK Mappings:** T1499


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.21

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `LoginGraceTime` parameter specifies the time allowed for successful authentication to the SSH server. The longer the Grace period is the more open unauthenticated connections can exist. Like other session controls in this session the Grace Period should be limited to appropriate organizational limits to ensure the service is available for needed access.

**MITRE ATT&CK Mappings:** T1499.002


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.21

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `LoginGraceTime` parameter specifies the time allowed for successful authentication to the SSH server. The longer the Grace period is the more open unauthenticated connections can exist. Like other session controls in this session the Grace Period should be limited to appropriate organizational limits to ensure the service is available for needed access.

**MITRE ATT&CK Mappings:** M1036


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.22

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** **NOTE:** To clarify, the two settings described below is only meant for idle connections from a protocol perspective and not meant to check if the user is active or not. An idle user does not mean an idle connection. SSH does not and never had, intentionally, the capability to drop idle users. In SSH versions before `8.2p1` there was a bug that caused these values to behave in such a manner that they where abused to disconnect idle users. This bug has been resolved in `8.2p1` and thus it can no longer be abused disconnect idle users.

The two options `ClientAliveInterval` and `ClientAliveCountMax` control the timeout of SSH sessions. Taken directly from `man 5 sshd_config`:

- `ClientAliveInterval` Sets a timeout interval in seconds after which if no data has been received from the client, sshd(8) will send a message through the encrypted channel to request a response from the client. The default is 0, indicating that these messages will not be sent to the client.

- `ClientAliveCountMax` Sets the number of client alive messages which may be sent without sshd(8) receiving any messages back from the client. If this threshold is reached while client alive messages are being sent, sshd will disconnect the client, terminating the session. It is important to note that the use of client alive messages is very different from TCPKeepAlive. The client alive messages are sent through the encrypted channel and therefore will not be spoofable. The TCP keepalive option en‐abled by TCPKeepAlive is spoofable. The client alive mechanism is valuable when the client or server depend on knowing when a connection has become unresponsive.
The default value is 3. If ClientAliveInterval is set to 15, and ClientAliveCountMax is left at the default, unresponsive SSH clients will be disconnected after approximately 45 seconds. Setting a zero ClientAliveCountMax disables connection termination.

**MITRE ATT&CK Mappings:** TA0001


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.22

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** **NOTE:** To clarify, the two settings described below is only meant for idle connections from a protocol perspective and not meant to check if the user is active or not. An idle user does not mean an idle connection. SSH does not and never had, intentionally, the capability to drop idle users. In SSH versions before `8.2p1` there was a bug that caused these values to behave in such a manner that they where abused to disconnect idle users. This bug has been resolved in `8.2p1` and thus it can no longer be abused disconnect idle users.

The two options `ClientAliveInterval` and `ClientAliveCountMax` control the timeout of SSH sessions. Taken directly from `man 5 sshd_config`:

- `ClientAliveInterval` Sets a timeout interval in seconds after which if no data has been received from the client, sshd(8) will send a message through the encrypted channel to request a response from the client. The default is 0, indicating that these messages will not be sent to the client.

- `ClientAliveCountMax` Sets the number of client alive messages which may be sent without sshd(8) receiving any messages back from the client. If this threshold is reached while client alive messages are being sent, sshd will disconnect the client, terminating the session. It is important to note that the use of client alive messages is very different from TCPKeepAlive. The client alive messages are sent through the encrypted channel and therefore will not be spoofable. The TCP keepalive option en‐abled by TCPKeepAlive is spoofable. The client alive mechanism is valuable when the client or server depend on knowing when a connection has become unresponsive.
The default value is 3. If ClientAliveInterval is set to 15, and ClientAliveCountMax is left at the default, unresponsive SSH clients will be disconnected after approximately 45 seconds. Setting a zero ClientAliveCountMax disables connection termination.

**MITRE ATT&CK Mappings:** T1078


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.22

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** **NOTE:** To clarify, the two settings described below is only meant for idle connections from a protocol perspective and not meant to check if the user is active or not. An idle user does not mean an idle connection. SSH does not and never had, intentionally, the capability to drop idle users. In SSH versions before `8.2p1` there was a bug that caused these values to behave in such a manner that they where abused to disconnect idle users. This bug has been resolved in `8.2p1` and thus it can no longer be abused disconnect idle users.

The two options `ClientAliveInterval` and `ClientAliveCountMax` control the timeout of SSH sessions. Taken directly from `man 5 sshd_config`:

- `ClientAliveInterval` Sets a timeout interval in seconds after which if no data has been received from the client, sshd(8) will send a message through the encrypted channel to request a response from the client. The default is 0, indicating that these messages will not be sent to the client.

- `ClientAliveCountMax` Sets the number of client alive messages which may be sent without sshd(8) receiving any messages back from the client. If this threshold is reached while client alive messages are being sent, sshd will disconnect the client, terminating the session. It is important to note that the use of client alive messages is very different from TCPKeepAlive. The client alive messages are sent through the encrypted channel and therefore will not be spoofable. The TCP keepalive option en‐abled by TCPKeepAlive is spoofable. The client alive mechanism is valuable when the client or server depend on knowing when a connection has become unresponsive.
The default value is 3. If ClientAliveInterval is set to 15, and ClientAliveCountMax is left at the default, unresponsive SSH clients will be disconnected after approximately 45 seconds. Setting a zero ClientAliveCountMax disables connection termination.

**MITRE ATT&CK Mappings:** T1078.001


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.22

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** **NOTE:** To clarify, the two settings described below is only meant for idle connections from a protocol perspective and not meant to check if the user is active or not. An idle user does not mean an idle connection. SSH does not and never had, intentionally, the capability to drop idle users. In SSH versions before `8.2p1` there was a bug that caused these values to behave in such a manner that they where abused to disconnect idle users. This bug has been resolved in `8.2p1` and thus it can no longer be abused disconnect idle users.

The two options `ClientAliveInterval` and `ClientAliveCountMax` control the timeout of SSH sessions. Taken directly from `man 5 sshd_config`:

- `ClientAliveInterval` Sets a timeout interval in seconds after which if no data has been received from the client, sshd(8) will send a message through the encrypted channel to request a response from the client. The default is 0, indicating that these messages will not be sent to the client.

- `ClientAliveCountMax` Sets the number of client alive messages which may be sent without sshd(8) receiving any messages back from the client. If this threshold is reached while client alive messages are being sent, sshd will disconnect the client, terminating the session. It is important to note that the use of client alive messages is very different from TCPKeepAlive. The client alive messages are sent through the encrypted channel and therefore will not be spoofable. The TCP keepalive option en‐abled by TCPKeepAlive is spoofable. The client alive mechanism is valuable when the client or server depend on knowing when a connection has become unresponsive.
The default value is 3. If ClientAliveInterval is set to 15, and ClientAliveCountMax is left at the default, unresponsive SSH clients will be disconnected after approximately 45 seconds. Setting a zero ClientAliveCountMax disables connection termination.

**MITRE ATT&CK Mappings:** T1078.002


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.22

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** **NOTE:** To clarify, the two settings described below is only meant for idle connections from a protocol perspective and not meant to check if the user is active or not. An idle user does not mean an idle connection. SSH does not and never had, intentionally, the capability to drop idle users. In SSH versions before `8.2p1` there was a bug that caused these values to behave in such a manner that they where abused to disconnect idle users. This bug has been resolved in `8.2p1` and thus it can no longer be abused disconnect idle users.

The two options `ClientAliveInterval` and `ClientAliveCountMax` control the timeout of SSH sessions. Taken directly from `man 5 sshd_config`:

- `ClientAliveInterval` Sets a timeout interval in seconds after which if no data has been received from the client, sshd(8) will send a message through the encrypted channel to request a response from the client. The default is 0, indicating that these messages will not be sent to the client.

- `ClientAliveCountMax` Sets the number of client alive messages which may be sent without sshd(8) receiving any messages back from the client. If this threshold is reached while client alive messages are being sent, sshd will disconnect the client, terminating the session. It is important to note that the use of client alive messages is very different from TCPKeepAlive. The client alive messages are sent through the encrypted channel and therefore will not be spoofable. The TCP keepalive option en‐abled by TCPKeepAlive is spoofable. The client alive mechanism is valuable when the client or server depend on knowing when a connection has become unresponsive.
The default value is 3. If ClientAliveInterval is set to 15, and ClientAliveCountMax is left at the default, unresponsive SSH clients will be disconnected after approximately 45 seconds. Setting a zero ClientAliveCountMax disables connection termination.

**MITRE ATT&CK Mappings:** T1078.003


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.22

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** **NOTE:** To clarify, the two settings described below is only meant for idle connections from a protocol perspective and not meant to check if the user is active or not. An idle user does not mean an idle connection. SSH does not and never had, intentionally, the capability to drop idle users. In SSH versions before `8.2p1` there was a bug that caused these values to behave in such a manner that they where abused to disconnect idle users. This bug has been resolved in `8.2p1` and thus it can no longer be abused disconnect idle users.

The two options `ClientAliveInterval` and `ClientAliveCountMax` control the timeout of SSH sessions. Taken directly from `man 5 sshd_config`:

- `ClientAliveInterval` Sets a timeout interval in seconds after which if no data has been received from the client, sshd(8) will send a message through the encrypted channel to request a response from the client. The default is 0, indicating that these messages will not be sent to the client.

- `ClientAliveCountMax` Sets the number of client alive messages which may be sent without sshd(8) receiving any messages back from the client. If this threshold is reached while client alive messages are being sent, sshd will disconnect the client, terminating the session. It is important to note that the use of client alive messages is very different from TCPKeepAlive. The client alive messages are sent through the encrypted channel and therefore will not be spoofable. The TCP keepalive option en‐abled by TCPKeepAlive is spoofable. The client alive mechanism is valuable when the client or server depend on knowing when a connection has become unresponsive.
The default value is 3. If ClientAliveInterval is set to 15, and ClientAliveCountMax is left at the default, unresponsive SSH clients will be disconnected after approximately 45 seconds. Setting a zero ClientAliveCountMax disables connection termination.

**MITRE ATT&CK Mappings:** M1026


Section #: 5.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.3.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** `sudo` allows a permitted user to execute a command as the superuser or another user, as specified by the security policy. The invoking user's real (not effective) user ID is used to determine the user name with which to query the security policy.

**MITRE ATT&CK Mappings:** TA0001


Section #: 5.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.3.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** `sudo` allows a permitted user to execute a command as the superuser or another user, as specified by the security policy. The invoking user's real (not effective) user ID is used to determine the user name with which to query the security policy.

**MITRE ATT&CK Mappings:** T1078


Section #: 5.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.3.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** `sudo` allows a permitted user to execute a command as the superuser or another user, as specified by the security policy. The invoking user's real (not effective) user ID is used to determine the user name with which to query the security policy.

**MITRE ATT&CK Mappings:** T1078.003


Section #: 5.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.3.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** `sudo` can be configured to run only from a pseudo terminal (`pseudo-pty`).

**MITRE ATT&CK Mappings:** TA0003


Section #: 5.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.3.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** `sudo` can be configured to run only from a pseudo terminal (`pseudo-pty`).

**MITRE ATT&CK Mappings:** T1548


Section #: 5.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.3.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** `sudo` can be configured to run only from a pseudo terminal (`pseudo-pty`).

**MITRE ATT&CK Mappings:** T1548.003


Section #: 5.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.3.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** sudo can use a custom log file

**MITRE ATT&CK Mappings:** TA0004


Section #: 5.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.3.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** sudo can use a custom log file

**MITRE ATT&CK Mappings:** T1562


Section #: 5.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.3.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** sudo can use a custom log file

**MITRE ATT&CK Mappings:** T1562.006


Section #: 5.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.3.7

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `su` command allows a user to run a command or shell as another user. The program has been superseded by `sudo`, which allows for more granular control over privileged access. Normally, the `su` command can be executed by any user. By uncommenting the `pam_wheel.so` statement in `/etc/pam.d/su`, the `su` command will only allow users in a specific groups to execute `su`. This group should be empty to reinforce the use of `sudo` for privileged access.

**MITRE ATT&CK Mappings:** TA0005


Section #: 5.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.3.7

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `su` command allows a user to run a command or shell as another user. The program has been superseded by `sudo`, which allows for more granular control over privileged access. Normally, the `su` command can be executed by any user. By uncommenting the `pam_wheel.so` statement in `/etc/pam.d/su`, the `su` command will only allow users in a specific groups to execute `su`. This group should be empty to reinforce the use of `sudo` for privileged access.

**MITRE ATT&CK Mappings:** T1548


Section #: 5.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.3.7

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `su` command allows a user to run a command or shell as another user. The program has been superseded by `sudo`, which allows for more granular control over privileged access. Normally, the `su` command can be executed by any user. By uncommenting the `pam_wheel.so` statement in `/etc/pam.d/su`, the `su` command will only allow users in a specific groups to execute `su`. This group should be empty to reinforce the use of `sudo` for privileged access.

**MITRE ATT&CK Mappings:** T1548.000


Section #: 5.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.3.7

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `su` command allows a user to run a command or shell as another user. The program has been superseded by `sudo`, which allows for more granular control over privileged access. Normally, the `su` command can be executed by any user. By uncommenting the `pam_wheel.so` statement in `/etc/pam.d/su`, the `su` command will only allow users in a specific groups to execute `su`. This group should be empty to reinforce the use of `sudo` for privileged access.

**MITRE ATT&CK Mappings:** M1026


Section #: 5.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.4.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `pam_pwquality.so` module checks the strength of passwords. It performs checks such as making sure a password is not a dictionary word, it is a certain length, contains a mix of characters (e.g. alphabet, numeric, other) and more. 

The following options are set in the `/etc/security/pwquality.conf` file:

- Password Length:
 - `minlen = 14` - password must be 14 characters or more
- Password complexity:
 - `minclass = 4` - The minimum number of required classes of characters for the new password (digits, uppercase, lowercase, others)

 _OR_

 - `dcredit = -1` - provide at least one digit
 - `ucredit = -1` - provide at least one uppercase character
 - `ocredit = -1` - provide at least one special character
 - `lcredit = -1` - provide at least one lowercase character

**MITRE ATT&CK Mappings:** TA0006


Section #: 5.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.4.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `pam_pwquality.so` module checks the strength of passwords. It performs checks such as making sure a password is not a dictionary word, it is a certain length, contains a mix of characters (e.g. alphabet, numeric, other) and more. 

The following options are set in the `/etc/security/pwquality.conf` file:

- Password Length:
 - `minlen = 14` - password must be 14 characters or more
- Password complexity:
 - `minclass = 4` - The minimum number of required classes of characters for the new password (digits, uppercase, lowercase, others)

 _OR_

 - `dcredit = -1` - provide at least one digit
 - `ucredit = -1` - provide at least one uppercase character
 - `ocredit = -1` - provide at least one special character
 - `lcredit = -1` - provide at least one lowercase character

**MITRE ATT&CK Mappings:** T1078


Section #: 5.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.4.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `pam_pwquality.so` module checks the strength of passwords. It performs checks such as making sure a password is not a dictionary word, it is a certain length, contains a mix of characters (e.g. alphabet, numeric, other) and more. 

The following options are set in the `/etc/security/pwquality.conf` file:

- Password Length:
 - `minlen = 14` - password must be 14 characters or more
- Password complexity:
 - `minclass = 4` - The minimum number of required classes of characters for the new password (digits, uppercase, lowercase, others)

 _OR_

 - `dcredit = -1` - provide at least one digit
 - `ucredit = -1` - provide at least one uppercase character
 - `ocredit = -1` - provide at least one special character
 - `lcredit = -1` - provide at least one lowercase character

**MITRE ATT&CK Mappings:** T1078.001


Section #: 5.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.4.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `pam_pwquality.so` module checks the strength of passwords. It performs checks such as making sure a password is not a dictionary word, it is a certain length, contains a mix of characters (e.g. alphabet, numeric, other) and more. 

The following options are set in the `/etc/security/pwquality.conf` file:

- Password Length:
 - `minlen = 14` - password must be 14 characters or more
- Password complexity:
 - `minclass = 4` - The minimum number of required classes of characters for the new password (digits, uppercase, lowercase, others)

 _OR_

 - `dcredit = -1` - provide at least one digit
 - `ucredit = -1` - provide at least one uppercase character
 - `ocredit = -1` - provide at least one special character
 - `lcredit = -1` - provide at least one lowercase character

**MITRE ATT&CK Mappings:** T1078.002


Section #: 5.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.4.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `pam_pwquality.so` module checks the strength of passwords. It performs checks such as making sure a password is not a dictionary word, it is a certain length, contains a mix of characters (e.g. alphabet, numeric, other) and more. 

The following options are set in the `/etc/security/pwquality.conf` file:

- Password Length:
 - `minlen = 14` - password must be 14 characters or more
- Password complexity:
 - `minclass = 4` - The minimum number of required classes of characters for the new password (digits, uppercase, lowercase, others)

 _OR_

 - `dcredit = -1` - provide at least one digit
 - `ucredit = -1` - provide at least one uppercase character
 - `ocredit = -1` - provide at least one special character
 - `lcredit = -1` - provide at least one lowercase character

**MITRE ATT&CK Mappings:** T1078.003


Section #: 5.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.4.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `pam_pwquality.so` module checks the strength of passwords. It performs checks such as making sure a password is not a dictionary word, it is a certain length, contains a mix of characters (e.g. alphabet, numeric, other) and more. 

The following options are set in the `/etc/security/pwquality.conf` file:

- Password Length:
 - `minlen = 14` - password must be 14 characters or more
- Password complexity:
 - `minclass = 4` - The minimum number of required classes of characters for the new password (digits, uppercase, lowercase, others)

 _OR_

 - `dcredit = -1` - provide at least one digit
 - `ucredit = -1` - provide at least one uppercase character
 - `ocredit = -1` - provide at least one special character
 - `lcredit = -1` - provide at least one lowercase character

**MITRE ATT&CK Mappings:** T1078.004


Section #: 5.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.4.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `pam_pwquality.so` module checks the strength of passwords. It performs checks such as making sure a password is not a dictionary word, it is a certain length, contains a mix of characters (e.g. alphabet, numeric, other) and more. 

The following options are set in the `/etc/security/pwquality.conf` file:

- Password Length:
 - `minlen = 14` - password must be 14 characters or more
- Password complexity:
 - `minclass = 4` - The minimum number of required classes of characters for the new password (digits, uppercase, lowercase, others)

 _OR_

 - `dcredit = -1` - provide at least one digit
 - `ucredit = -1` - provide at least one uppercase character
 - `ocredit = -1` - provide at least one special character
 - `lcredit = -1` - provide at least one lowercase character

**MITRE ATT&CK Mappings:** T1110


Section #: 5.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.4.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `pam_pwquality.so` module checks the strength of passwords. It performs checks such as making sure a password is not a dictionary word, it is a certain length, contains a mix of characters (e.g. alphabet, numeric, other) and more. 

The following options are set in the `/etc/security/pwquality.conf` file:

- Password Length:
 - `minlen = 14` - password must be 14 characters or more
- Password complexity:
 - `minclass = 4` - The minimum number of required classes of characters for the new password (digits, uppercase, lowercase, others)

 _OR_

 - `dcredit = -1` - provide at least one digit
 - `ucredit = -1` - provide at least one uppercase character
 - `ocredit = -1` - provide at least one special character
 - `lcredit = -1` - provide at least one lowercase character

**MITRE ATT&CK Mappings:** T1110.001


Section #: 5.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.4.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `pam_pwquality.so` module checks the strength of passwords. It performs checks such as making sure a password is not a dictionary word, it is a certain length, contains a mix of characters (e.g. alphabet, numeric, other) and more. 

The following options are set in the `/etc/security/pwquality.conf` file:

- Password Length:
 - `minlen = 14` - password must be 14 characters or more
- Password complexity:
 - `minclass = 4` - The minimum number of required classes of characters for the new password (digits, uppercase, lowercase, others)

 _OR_

 - `dcredit = -1` - provide at least one digit
 - `ucredit = -1` - provide at least one uppercase character
 - `ocredit = -1` - provide at least one special character
 - `lcredit = -1` - provide at least one lowercase character

**MITRE ATT&CK Mappings:** T1110.002


Section #: 5.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.4.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `pam_pwquality.so` module checks the strength of passwords. It performs checks such as making sure a password is not a dictionary word, it is a certain length, contains a mix of characters (e.g. alphabet, numeric, other) and more. 

The following options are set in the `/etc/security/pwquality.conf` file:

- Password Length:
 - `minlen = 14` - password must be 14 characters or more
- Password complexity:
 - `minclass = 4` - The minimum number of required classes of characters for the new password (digits, uppercase, lowercase, others)

 _OR_

 - `dcredit = -1` - provide at least one digit
 - `ucredit = -1` - provide at least one uppercase character
 - `ocredit = -1` - provide at least one special character
 - `lcredit = -1` - provide at least one lowercase character

**MITRE ATT&CK Mappings:** T1110.003


Section #: 5.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.4.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `pam_pwquality.so` module checks the strength of passwords. It performs checks such as making sure a password is not a dictionary word, it is a certain length, contains a mix of characters (e.g. alphabet, numeric, other) and more. 

The following options are set in the `/etc/security/pwquality.conf` file:

- Password Length:
 - `minlen = 14` - password must be 14 characters or more
- Password complexity:
 - `minclass = 4` - The minimum number of required classes of characters for the new password (digits, uppercase, lowercase, others)

 _OR_

 - `dcredit = -1` - provide at least one digit
 - `ucredit = -1` - provide at least one uppercase character
 - `ocredit = -1` - provide at least one special character
 - `lcredit = -1` - provide at least one lowercase character

**MITRE ATT&CK Mappings:** M1027


Section #: 5.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.4.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Lock out users after _n_ unsuccessful consecutive login attempts. The first sets of changes are made to the common PAM configuration files. The second set of changes are applied to the program specific PAM configuration file. The second set of changes must be applied to each program that will lock out users. Check the documentation for each secondary program for instructions on how to configure them to work with PAM.

All configuration of `faillock` is located in `/etc/security/faillock.conf` and well commented.

- `deny` - Deny access if the number of consecutive authentication failures for this user during the recent interval exceeds n tries.
- `fail_interval` - The length of the interval, in seconds, during which the consecutive authentication failures must happen for the user account to be locked out
- `unlock_time` - The access will be re-enabled after n seconds after the lock out. The value `0` has the same meaning as value `never` - the access will not be re-enabled without resetting the faillock entries by the `faillock` command.

Set the lockout number and unlock time in accordance with local site policy.

**MITRE ATT&CK Mappings:** TA0006


Section #: 5.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.4.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Lock out users after _n_ unsuccessful consecutive login attempts. The first sets of changes are made to the common PAM configuration files. The second set of changes are applied to the program specific PAM configuration file. The second set of changes must be applied to each program that will lock out users. Check the documentation for each secondary program for instructions on how to configure them to work with PAM.

All configuration of `faillock` is located in `/etc/security/faillock.conf` and well commented.

- `deny` - Deny access if the number of consecutive authentication failures for this user during the recent interval exceeds n tries.
- `fail_interval` - The length of the interval, in seconds, during which the consecutive authentication failures must happen for the user account to be locked out
- `unlock_time` - The access will be re-enabled after n seconds after the lock out. The value `0` has the same meaning as value `never` - the access will not be re-enabled without resetting the faillock entries by the `faillock` command.

Set the lockout number and unlock time in accordance with local site policy.

**MITRE ATT&CK Mappings:** T1110


Section #: 5.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.4.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Lock out users after _n_ unsuccessful consecutive login attempts. The first sets of changes are made to the common PAM configuration files. The second set of changes are applied to the program specific PAM configuration file. The second set of changes must be applied to each program that will lock out users. Check the documentation for each secondary program for instructions on how to configure them to work with PAM.

All configuration of `faillock` is located in `/etc/security/faillock.conf` and well commented.

- `deny` - Deny access if the number of consecutive authentication failures for this user during the recent interval exceeds n tries.
- `fail_interval` - The length of the interval, in seconds, during which the consecutive authentication failures must happen for the user account to be locked out
- `unlock_time` - The access will be re-enabled after n seconds after the lock out. The value `0` has the same meaning as value `never` - the access will not be re-enabled without resetting the faillock entries by the `faillock` command.

Set the lockout number and unlock time in accordance with local site policy.

**MITRE ATT&CK Mappings:** T1110.001


Section #: 5.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.4.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Lock out users after _n_ unsuccessful consecutive login attempts. The first sets of changes are made to the common PAM configuration files. The second set of changes are applied to the program specific PAM configuration file. The second set of changes must be applied to each program that will lock out users. Check the documentation for each secondary program for instructions on how to configure them to work with PAM.

All configuration of `faillock` is located in `/etc/security/faillock.conf` and well commented.

- `deny` - Deny access if the number of consecutive authentication failures for this user during the recent interval exceeds n tries.
- `fail_interval` - The length of the interval, in seconds, during which the consecutive authentication failures must happen for the user account to be locked out
- `unlock_time` - The access will be re-enabled after n seconds after the lock out. The value `0` has the same meaning as value `never` - the access will not be re-enabled without resetting the faillock entries by the `faillock` command.

Set the lockout number and unlock time in accordance with local site policy.

**MITRE ATT&CK Mappings:** T1110.003


Section #: 5.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.4.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Lock out users after _n_ unsuccessful consecutive login attempts. The first sets of changes are made to the common PAM configuration files. The second set of changes are applied to the program specific PAM configuration file. The second set of changes must be applied to each program that will lock out users. Check the documentation for each secondary program for instructions on how to configure them to work with PAM.

All configuration of `faillock` is located in `/etc/security/faillock.conf` and well commented.

- `deny` - Deny access if the number of consecutive authentication failures for this user during the recent interval exceeds n tries.
- `fail_interval` - The length of the interval, in seconds, during which the consecutive authentication failures must happen for the user account to be locked out
- `unlock_time` - The access will be re-enabled after n seconds after the lock out. The value `0` has the same meaning as value `never` - the access will not be re-enabled without resetting the faillock entries by the `faillock` command.

Set the lockout number and unlock time in accordance with local site policy.

**MITRE ATT&CK Mappings:** M1027


Section #: 5.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.4.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/etc/security/opasswd` file stores the users' old passwords and can be checked to ensure that users are not recycling recent passwords.

**MITRE ATT&CK Mappings:** T1078


Section #: 5.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.4.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/etc/security/opasswd` file stores the users' old passwords and can be checked to ensure that users are not recycling recent passwords.

**MITRE ATT&CK Mappings:** T1078.001


Section #: 5.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.4.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/etc/security/opasswd` file stores the users' old passwords and can be checked to ensure that users are not recycling recent passwords.

**MITRE ATT&CK Mappings:** T1078.002


Section #: 5.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.4.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/etc/security/opasswd` file stores the users' old passwords and can be checked to ensure that users are not recycling recent passwords.

**MITRE ATT&CK Mappings:** T1078.003


Section #: 5.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.4.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/etc/security/opasswd` file stores the users' old passwords and can be checked to ensure that users are not recycling recent passwords.

**MITRE ATT&CK Mappings:** T1078.004


Section #: 5.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.4.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/etc/security/opasswd` file stores the users' old passwords and can be checked to ensure that users are not recycling recent passwords.

**MITRE ATT&CK Mappings:** T1110


Section #: 5.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.4.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/etc/security/opasswd` file stores the users' old passwords and can be checked to ensure that users are not recycling recent passwords.

**MITRE ATT&CK Mappings:** T1110.004


Section #: 5.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.4.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The commands below change password encryption to `yescrypt`. All existing accounts will need to perform a password change to upgrade the stored hashes to the new algorithm.

**MITRE ATT&CK Mappings:** TA0006


Section #: 5.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.4.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The commands below change password encryption to `yescrypt`. All existing accounts will need to perform a password change to upgrade the stored hashes to the new algorithm.

**MITRE ATT&CK Mappings:** T1003


Section #: 5.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.4.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The commands below change password encryption to `yescrypt`. All existing accounts will need to perform a password change to upgrade the stored hashes to the new algorithm.

**MITRE ATT&CK Mappings:** T1003.008


Section #: 5.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.4.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The commands below change password encryption to `yescrypt`. All existing accounts will need to perform a password change to upgrade the stored hashes to the new algorithm.

**MITRE ATT&CK Mappings:** T1110


Section #: 5.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.4.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The commands below change password encryption to `yescrypt`. All existing accounts will need to perform a password change to upgrade the stored hashes to the new algorithm.

**MITRE ATT&CK Mappings:** T1110.002


Section #: 5.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.4.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The commands below change password encryption to `yescrypt`. All existing accounts will need to perform a password change to upgrade the stored hashes to the new algorithm.

**MITRE ATT&CK Mappings:** M1041


Section #: 5.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.4.5

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Currently used passwords with out of date hashing algorithms may pose a security risk to the system.

**MITRE ATT&CK Mappings:** TA0006


Section #: 5.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.4.5

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Currently used passwords with out of date hashing algorithms may pose a security risk to the system.

**MITRE ATT&CK Mappings:** T1003


Section #: 5.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.4.5

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Currently used passwords with out of date hashing algorithms may pose a security risk to the system.

**MITRE ATT&CK Mappings:** T1003.008


Section #: 5.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.4.5

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Currently used passwords with out of date hashing algorithms may pose a security risk to the system.

**MITRE ATT&CK Mappings:** T1110


Section #: 5.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.4.5

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Currently used passwords with out of date hashing algorithms may pose a security risk to the system.

**MITRE ATT&CK Mappings:** T1110.002


Section #: 5.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.4.5

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Currently used passwords with out of date hashing algorithms may pose a security risk to the system.

**MITRE ATT&CK Mappings:** M1041


Section #: 5.5
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.5.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** There are a number of accounts provided with most distributions that are used to manage applications and are not intended to provide an interactive shell.

**MITRE ATT&CK Mappings:** TA0005


Section #: 5.5
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.5.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** There are a number of accounts provided with most distributions that are used to manage applications and are not intended to provide an interactive shell.

**MITRE ATT&CK Mappings:** T1078


Section #: 5.5
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.5.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** There are a number of accounts provided with most distributions that are used to manage applications and are not intended to provide an interactive shell.

**MITRE ATT&CK Mappings:** T1078.001


Section #: 5.5
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.5.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** There are a number of accounts provided with most distributions that are used to manage applications and are not intended to provide an interactive shell.

**MITRE ATT&CK Mappings:** T1078.003


Section #: 5.5
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.5.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** There are a number of accounts provided with most distributions that are used to manage applications and are not intended to provide an interactive shell.

**MITRE ATT&CK Mappings:** M1026


Section #: 5.5
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.5.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The usermod command can be used to specify which group the root user belongs to. This affects permissions of files that are created by the root user.

**MITRE ATT&CK Mappings:** TA0005


Section #: 5.5
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.5.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The usermod command can be used to specify which group the root user belongs to. This affects permissions of files that are created by the root user.

**MITRE ATT&CK Mappings:** T1548


Section #: 5.5
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.5.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The usermod command can be used to specify which group the root user belongs to. This affects permissions of files that are created by the root user.

**MITRE ATT&CK Mappings:** T1548.000


Section #: 5.5
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.5.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The usermod command can be used to specify which group the root user belongs to. This affects permissions of files that are created by the root user.

**MITRE ATT&CK Mappings:** M1026


Section #: 5.5
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.5.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The user file-creation mode mask (`umask`) is use to determine the file permission for newly created directories and files. In Linux, the default permissions for any newly created directory is 0777 (rwxrwxrwx), and for any newly created file it is 0666 (rw-rw-rw-). The `umask` modifies the default Linux permissions by restricting (masking) these permissions. The `umask` is not simply subtracted, but is processed bitwise. Bits set in the `umask` are cleared in the resulting file mode.

_`umask` can be set with either `octal` or `Symbolic` values_
- `Octal` (Numeric) Value - Represented by either three or four digits. ie `umask 0027` or `umask 027`. If a four digit umask is used, the first digit is ignored. The remaining three digits effect the resulting permissions for user, group, and world/other respectively.
- `Symbolic` Value - Represented by a comma separated list for User `u`, group `g`, and world/other `o`. The permissions listed are not masked by `umask`. ie a `umask` set by `umask u=rwx,g=rx,o=` is the `Symbolic` equivalent of the `Octal` `umask 027`. This `umask` would set a newly created directory with file mode `drwxr-x---` and a newly created file with file mode `rw-r-----`.

Setting the default `umask`:
- `pam_umask` module:
 - will set the umask according to the system default in `/etc/login.defs` and user settings, solving the problem of different `umask` settings with different shells, display managers, remote sessions etc.
 - `umask=<mask>` value in the `/etc/login.defs` file is interpreted as Octal
 - Setting `USERGROUPS_ENAB` to `yes` in `/etc/login.defs` (default):
 - will enable setting of the umask group bits to be the same as owner bits. (examples: 022 -> 002, 077 -> 007) for non-root users, if the uid is the same as gid, and username is the same as the primary group name
 - userdel will remove the user's group if it contains no more members, and useradd will create by default a group with the name of the user
- `System Wide Shell Configuration File`:
 - `/etc/profile` - used to set system wide environmental variables on users shells. The variables are sometimes the same ones that are in the `.profile`, however this file is used to set an initial PATH or PS1 for all shell users of the system. _is only executed for interactive `login` shells, or shells executed with the --login parameter_
 - `/etc/profile.d` - `/etc/profile` will execute the scripts within `/etc/profile.d/*.sh`. It is recommended to place your configuration in a shell script within `/etc/profile.d` to set your own system wide environmental variables.
 - `/etc/bash.bashrc` - System wide version of `.bashrc`. `etc/bashrc` also invokes `/etc/profile.d/*.sh` if `non-login` shell, but redirects output to `/dev/null` if `non-interactive.` _Is only executed for `interactive` shells or if `BASH_ENV` is set to `/etc/bash.bashrc`_

_User Shell Configuration Files:_
- `~/.profile` - Is executed to configure your shell before the initial command prompt. _Is only read by login shells._
- `~/.bashrc` - Is executed for interactive shells. _only read by a shell that's both interactive and non-login_

**MITRE ATT&CK Mappings:** TA0007


Section #: 5.5
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.5.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The user file-creation mode mask (`umask`) is use to determine the file permission for newly created directories and files. In Linux, the default permissions for any newly created directory is 0777 (rwxrwxrwx), and for any newly created file it is 0666 (rw-rw-rw-). The `umask` modifies the default Linux permissions by restricting (masking) these permissions. The `umask` is not simply subtracted, but is processed bitwise. Bits set in the `umask` are cleared in the resulting file mode.

_`umask` can be set with either `octal` or `Symbolic` values_
- `Octal` (Numeric) Value - Represented by either three or four digits. ie `umask 0027` or `umask 027`. If a four digit umask is used, the first digit is ignored. The remaining three digits effect the resulting permissions for user, group, and world/other respectively.
- `Symbolic` Value - Represented by a comma separated list for User `u`, group `g`, and world/other `o`. The permissions listed are not masked by `umask`. ie a `umask` set by `umask u=rwx,g=rx,o=` is the `Symbolic` equivalent of the `Octal` `umask 027`. This `umask` would set a newly created directory with file mode `drwxr-x---` and a newly created file with file mode `rw-r-----`.

Setting the default `umask`:
- `pam_umask` module:
 - will set the umask according to the system default in `/etc/login.defs` and user settings, solving the problem of different `umask` settings with different shells, display managers, remote sessions etc.
 - `umask=<mask>` value in the `/etc/login.defs` file is interpreted as Octal
 - Setting `USERGROUPS_ENAB` to `yes` in `/etc/login.defs` (default):
 - will enable setting of the umask group bits to be the same as owner bits. (examples: 022 -> 002, 077 -> 007) for non-root users, if the uid is the same as gid, and username is the same as the primary group name
 - userdel will remove the user's group if it contains no more members, and useradd will create by default a group with the name of the user
- `System Wide Shell Configuration File`:
 - `/etc/profile` - used to set system wide environmental variables on users shells. The variables are sometimes the same ones that are in the `.profile`, however this file is used to set an initial PATH or PS1 for all shell users of the system. _is only executed for interactive `login` shells, or shells executed with the --login parameter_
 - `/etc/profile.d` - `/etc/profile` will execute the scripts within `/etc/profile.d/*.sh`. It is recommended to place your configuration in a shell script within `/etc/profile.d` to set your own system wide environmental variables.
 - `/etc/bash.bashrc` - System wide version of `.bashrc`. `etc/bashrc` also invokes `/etc/profile.d/*.sh` if `non-login` shell, but redirects output to `/dev/null` if `non-interactive.` _Is only executed for `interactive` shells or if `BASH_ENV` is set to `/etc/bash.bashrc`_

_User Shell Configuration Files:_
- `~/.profile` - Is executed to configure your shell before the initial command prompt. _Is only read by login shells._
- `~/.bashrc` - Is executed for interactive shells. _only read by a shell that's both interactive and non-login_

**MITRE ATT&CK Mappings:** T1565


Section #: 5.5
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.5.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The user file-creation mode mask (`umask`) is use to determine the file permission for newly created directories and files. In Linux, the default permissions for any newly created directory is 0777 (rwxrwxrwx), and for any newly created file it is 0666 (rw-rw-rw-). The `umask` modifies the default Linux permissions by restricting (masking) these permissions. The `umask` is not simply subtracted, but is processed bitwise. Bits set in the `umask` are cleared in the resulting file mode.

_`umask` can be set with either `octal` or `Symbolic` values_
- `Octal` (Numeric) Value - Represented by either three or four digits. ie `umask 0027` or `umask 027`. If a four digit umask is used, the first digit is ignored. The remaining three digits effect the resulting permissions for user, group, and world/other respectively.
- `Symbolic` Value - Represented by a comma separated list for User `u`, group `g`, and world/other `o`. The permissions listed are not masked by `umask`. ie a `umask` set by `umask u=rwx,g=rx,o=` is the `Symbolic` equivalent of the `Octal` `umask 027`. This `umask` would set a newly created directory with file mode `drwxr-x---` and a newly created file with file mode `rw-r-----`.

Setting the default `umask`:
- `pam_umask` module:
 - will set the umask according to the system default in `/etc/login.defs` and user settings, solving the problem of different `umask` settings with different shells, display managers, remote sessions etc.
 - `umask=<mask>` value in the `/etc/login.defs` file is interpreted as Octal
 - Setting `USERGROUPS_ENAB` to `yes` in `/etc/login.defs` (default):
 - will enable setting of the umask group bits to be the same as owner bits. (examples: 022 -> 002, 077 -> 007) for non-root users, if the uid is the same as gid, and username is the same as the primary group name
 - userdel will remove the user's group if it contains no more members, and useradd will create by default a group with the name of the user
- `System Wide Shell Configuration File`:
 - `/etc/profile` - used to set system wide environmental variables on users shells. The variables are sometimes the same ones that are in the `.profile`, however this file is used to set an initial PATH or PS1 for all shell users of the system. _is only executed for interactive `login` shells, or shells executed with the --login parameter_
 - `/etc/profile.d` - `/etc/profile` will execute the scripts within `/etc/profile.d/*.sh`. It is recommended to place your configuration in a shell script within `/etc/profile.d` to set your own system wide environmental variables.
 - `/etc/bash.bashrc` - System wide version of `.bashrc`. `etc/bashrc` also invokes `/etc/profile.d/*.sh` if `non-login` shell, but redirects output to `/dev/null` if `non-interactive.` _Is only executed for `interactive` shells or if `BASH_ENV` is set to `/etc/bash.bashrc`_

_User Shell Configuration Files:_
- `~/.profile` - Is executed to configure your shell before the initial command prompt. _Is only read by login shells._
- `~/.bashrc` - Is executed for interactive shells. _only read by a shell that's both interactive and non-login_

**MITRE ATT&CK Mappings:** T1565.001


Section #: 5.5
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.5.5

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** `TMOUT` is an environmental setting that determines the timeout of a shell in seconds.
- TMOUT=_n_ - Sets the shell timeout to _n_ seconds. A setting of `TMOUT=0` disables timeout.
- readonly TMOUT- Sets the TMOUT environmental variable as readonly, preventing unwanted modification during run-time.
- export TMOUT - exports the TMOUT variable 

`System Wide Shell Configuration Files:`
- `/etc/profile` - used to set system wide environmental variables on users shells. The variables are sometimes the same ones that are in the `.bash_profile`, however this file is used to set an initial PATH or PS1 for all shell users of the system. _is only executed for interactive *login* shells, or shells executed with the --login parameter._ 
- `/etc/profile.d` - `/etc/profile` will execute the scripts within `/etc/profile.d/*.sh`. It is recommended to place your configuration in a shell script within `/etc/profile.d` to set your own system wide environmental variables.
- `/etc/bash.bashrc` - System wide version of `bash.bashrc`. `etc/bash.bashrc` also invokes /etc/profile.d/*.sh if *non-login* shell, but redirects output to `/dev/null` if *non-interactive.* _Is only executed for *interactive* shells or if `BASH_ENV` is set to `/etc/bash.bashrc`._

**MITRE ATT&CK Mappings:** TA0005


Section #: 5.5
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.5.5

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** `TMOUT` is an environmental setting that determines the timeout of a shell in seconds.
- TMOUT=_n_ - Sets the shell timeout to _n_ seconds. A setting of `TMOUT=0` disables timeout.
- readonly TMOUT- Sets the TMOUT environmental variable as readonly, preventing unwanted modification during run-time.
- export TMOUT - exports the TMOUT variable 

`System Wide Shell Configuration Files:`
- `/etc/profile` - used to set system wide environmental variables on users shells. The variables are sometimes the same ones that are in the `.bash_profile`, however this file is used to set an initial PATH or PS1 for all shell users of the system. _is only executed for interactive *login* shells, or shells executed with the --login parameter._ 
- `/etc/profile.d` - `/etc/profile` will execute the scripts within `/etc/profile.d/*.sh`. It is recommended to place your configuration in a shell script within `/etc/profile.d` to set your own system wide environmental variables.
- `/etc/bash.bashrc` - System wide version of `bash.bashrc`. `etc/bash.bashrc` also invokes /etc/profile.d/*.sh if *non-login* shell, but redirects output to `/dev/null` if *non-interactive.* _Is only executed for *interactive* shells or if `BASH_ENV` is set to `/etc/bash.bashrc`._

**MITRE ATT&CK Mappings:** T1078


Section #: 5.5
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.5.5

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** `TMOUT` is an environmental setting that determines the timeout of a shell in seconds.
- TMOUT=_n_ - Sets the shell timeout to _n_ seconds. A setting of `TMOUT=0` disables timeout.
- readonly TMOUT- Sets the TMOUT environmental variable as readonly, preventing unwanted modification during run-time.
- export TMOUT - exports the TMOUT variable 

`System Wide Shell Configuration Files:`
- `/etc/profile` - used to set system wide environmental variables on users shells. The variables are sometimes the same ones that are in the `.bash_profile`, however this file is used to set an initial PATH or PS1 for all shell users of the system. _is only executed for interactive *login* shells, or shells executed with the --login parameter._ 
- `/etc/profile.d` - `/etc/profile` will execute the scripts within `/etc/profile.d/*.sh`. It is recommended to place your configuration in a shell script within `/etc/profile.d` to set your own system wide environmental variables.
- `/etc/bash.bashrc` - System wide version of `bash.bashrc`. `etc/bash.bashrc` also invokes /etc/profile.d/*.sh if *non-login* shell, but redirects output to `/dev/null` if *non-interactive.* _Is only executed for *interactive* shells or if `BASH_ENV` is set to `/etc/bash.bashrc`._

**MITRE ATT&CK Mappings:** T1078.001


Section #: 5.5
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.5.5

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** `TMOUT` is an environmental setting that determines the timeout of a shell in seconds.
- TMOUT=_n_ - Sets the shell timeout to _n_ seconds. A setting of `TMOUT=0` disables timeout.
- readonly TMOUT- Sets the TMOUT environmental variable as readonly, preventing unwanted modification during run-time.
- export TMOUT - exports the TMOUT variable 

`System Wide Shell Configuration Files:`
- `/etc/profile` - used to set system wide environmental variables on users shells. The variables are sometimes the same ones that are in the `.bash_profile`, however this file is used to set an initial PATH or PS1 for all shell users of the system. _is only executed for interactive *login* shells, or shells executed with the --login parameter._ 
- `/etc/profile.d` - `/etc/profile` will execute the scripts within `/etc/profile.d/*.sh`. It is recommended to place your configuration in a shell script within `/etc/profile.d` to set your own system wide environmental variables.
- `/etc/bash.bashrc` - System wide version of `bash.bashrc`. `etc/bash.bashrc` also invokes /etc/profile.d/*.sh if *non-login* shell, but redirects output to `/dev/null` if *non-interactive.* _Is only executed for *interactive* shells or if `BASH_ENV` is set to `/etc/bash.bashrc`._

**MITRE ATT&CK Mappings:** T1078.002


Section #: 5.5
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.5.5

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** `TMOUT` is an environmental setting that determines the timeout of a shell in seconds.
- TMOUT=_n_ - Sets the shell timeout to _n_ seconds. A setting of `TMOUT=0` disables timeout.
- readonly TMOUT- Sets the TMOUT environmental variable as readonly, preventing unwanted modification during run-time.
- export TMOUT - exports the TMOUT variable 

`System Wide Shell Configuration Files:`
- `/etc/profile` - used to set system wide environmental variables on users shells. The variables are sometimes the same ones that are in the `.bash_profile`, however this file is used to set an initial PATH or PS1 for all shell users of the system. _is only executed for interactive *login* shells, or shells executed with the --login parameter._ 
- `/etc/profile.d` - `/etc/profile` will execute the scripts within `/etc/profile.d/*.sh`. It is recommended to place your configuration in a shell script within `/etc/profile.d` to set your own system wide environmental variables.
- `/etc/bash.bashrc` - System wide version of `bash.bashrc`. `etc/bash.bashrc` also invokes /etc/profile.d/*.sh if *non-login* shell, but redirects output to `/dev/null` if *non-interactive.* _Is only executed for *interactive* shells or if `BASH_ENV` is set to `/etc/bash.bashrc`._

**MITRE ATT&CK Mappings:** T1078.003


Section #: 5.5
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.5.5

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** `TMOUT` is an environmental setting that determines the timeout of a shell in seconds.
- TMOUT=_n_ - Sets the shell timeout to _n_ seconds. A setting of `TMOUT=0` disables timeout.
- readonly TMOUT- Sets the TMOUT environmental variable as readonly, preventing unwanted modification during run-time.
- export TMOUT - exports the TMOUT variable 

`System Wide Shell Configuration Files:`
- `/etc/profile` - used to set system wide environmental variables on users shells. The variables are sometimes the same ones that are in the `.bash_profile`, however this file is used to set an initial PATH or PS1 for all shell users of the system. _is only executed for interactive *login* shells, or shells executed with the --login parameter._ 
- `/etc/profile.d` - `/etc/profile` will execute the scripts within `/etc/profile.d/*.sh`. It is recommended to place your configuration in a shell script within `/etc/profile.d` to set your own system wide environmental variables.
- `/etc/bash.bashrc` - System wide version of `bash.bashrc`. `etc/bash.bashrc` also invokes /etc/profile.d/*.sh if *non-login* shell, but redirects output to `/dev/null` if *non-interactive.* _Is only executed for *interactive* shells or if `BASH_ENV` is set to `/etc/bash.bashrc`._

**MITRE ATT&CK Mappings:** M1026


Section #: 5.5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.5.1.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `PASS_MIN_DAYS` parameter in `/etc/login.defs` allows an administrator to prevent users from changing their password until a minimum number of days have passed since the last time the user changed their password. It is recommended that `PASS_MIN_DAYS` parameter be set to 1 or more days.

**MITRE ATT&CK Mappings:** TA0006


Section #: 5.5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.5.1.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `PASS_MIN_DAYS` parameter in `/etc/login.defs` allows an administrator to prevent users from changing their password until a minimum number of days have passed since the last time the user changed their password. It is recommended that `PASS_MIN_DAYS` parameter be set to 1 or more days.

**MITRE ATT&CK Mappings:** T1078


Section #: 5.5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.5.1.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `PASS_MIN_DAYS` parameter in `/etc/login.defs` allows an administrator to prevent users from changing their password until a minimum number of days have passed since the last time the user changed their password. It is recommended that `PASS_MIN_DAYS` parameter be set to 1 or more days.

**MITRE ATT&CK Mappings:** T1078.001


Section #: 5.5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.5.1.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `PASS_MIN_DAYS` parameter in `/etc/login.defs` allows an administrator to prevent users from changing their password until a minimum number of days have passed since the last time the user changed their password. It is recommended that `PASS_MIN_DAYS` parameter be set to 1 or more days.

**MITRE ATT&CK Mappings:** T1078.002


Section #: 5.5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.5.1.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `PASS_MIN_DAYS` parameter in `/etc/login.defs` allows an administrator to prevent users from changing their password until a minimum number of days have passed since the last time the user changed their password. It is recommended that `PASS_MIN_DAYS` parameter be set to 1 or more days.

**MITRE ATT&CK Mappings:** T1078.003


Section #: 5.5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.5.1.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `PASS_MIN_DAYS` parameter in `/etc/login.defs` allows an administrator to prevent users from changing their password until a minimum number of days have passed since the last time the user changed their password. It is recommended that `PASS_MIN_DAYS` parameter be set to 1 or more days.

**MITRE ATT&CK Mappings:** T1078.004


Section #: 5.5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.5.1.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `PASS_MIN_DAYS` parameter in `/etc/login.defs` allows an administrator to prevent users from changing their password until a minimum number of days have passed since the last time the user changed their password. It is recommended that `PASS_MIN_DAYS` parameter be set to 1 or more days.

**MITRE ATT&CK Mappings:** T1110


Section #: 5.5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.5.1.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `PASS_MIN_DAYS` parameter in `/etc/login.defs` allows an administrator to prevent users from changing their password until a minimum number of days have passed since the last time the user changed their password. It is recommended that `PASS_MIN_DAYS` parameter be set to 1 or more days.

**MITRE ATT&CK Mappings:** T1110.004


Section #: 5.5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.5.1.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `PASS_MIN_DAYS` parameter in `/etc/login.defs` allows an administrator to prevent users from changing their password until a minimum number of days have passed since the last time the user changed their password. It is recommended that `PASS_MIN_DAYS` parameter be set to 1 or more days.

**MITRE ATT&CK Mappings:** M1027


Section #: 5.5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.5.1.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `PASS_MAX_DAYS` parameter in `/etc/login.defs` allows an administrator to force passwords to expire once they reach a defined age.

**MITRE ATT&CK Mappings:** T1078


Section #: 5.5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.5.1.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `PASS_MAX_DAYS` parameter in `/etc/login.defs` allows an administrator to force passwords to expire once they reach a defined age.

**MITRE ATT&CK Mappings:** T1078.001


Section #: 5.5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.5.1.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `PASS_MAX_DAYS` parameter in `/etc/login.defs` allows an administrator to force passwords to expire once they reach a defined age.

**MITRE ATT&CK Mappings:** T1078.002


Section #: 5.5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.5.1.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `PASS_MAX_DAYS` parameter in `/etc/login.defs` allows an administrator to force passwords to expire once they reach a defined age.

**MITRE ATT&CK Mappings:** T1078.003


Section #: 5.5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.5.1.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `PASS_MAX_DAYS` parameter in `/etc/login.defs` allows an administrator to force passwords to expire once they reach a defined age.

**MITRE ATT&CK Mappings:** T1078.004


Section #: 5.5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.5.1.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `PASS_MAX_DAYS` parameter in `/etc/login.defs` allows an administrator to force passwords to expire once they reach a defined age.

**MITRE ATT&CK Mappings:** T1110


Section #: 5.5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.5.1.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `PASS_MAX_DAYS` parameter in `/etc/login.defs` allows an administrator to force passwords to expire once they reach a defined age.

**MITRE ATT&CK Mappings:** T1110.001


Section #: 5.5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.5.1.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `PASS_MAX_DAYS` parameter in `/etc/login.defs` allows an administrator to force passwords to expire once they reach a defined age.

**MITRE ATT&CK Mappings:** T1110.002


Section #: 5.5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.5.1.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `PASS_MAX_DAYS` parameter in `/etc/login.defs` allows an administrator to force passwords to expire once they reach a defined age.

**MITRE ATT&CK Mappings:** T1110.003


Section #: 5.5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.5.1.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `PASS_MAX_DAYS` parameter in `/etc/login.defs` allows an administrator to force passwords to expire once they reach a defined age.

**MITRE ATT&CK Mappings:** T1110.004


Section #: 5.5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.5.1.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `PASS_WARN_AGE` parameter in `/etc/login.defs` allows an administrator to notify users that their password will expire in a defined number of days. It is recommended that the `PASS_WARN_AGE` parameter be set to 7 or more days.

**MITRE ATT&CK Mappings:** TA0006


Section #: 5.5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.5.1.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `PASS_WARN_AGE` parameter in `/etc/login.defs` allows an administrator to notify users that their password will expire in a defined number of days. It is recommended that the `PASS_WARN_AGE` parameter be set to 7 or more days.

**MITRE ATT&CK Mappings:** M1027


Section #: 5.5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.5.1.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** User accounts that have been inactive for over a given period of time can be automatically disabled. It is recommended that accounts that are inactive for 30 days after password expiration be disabled.

**MITRE ATT&CK Mappings:** TA0001


Section #: 5.5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.5.1.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** User accounts that have been inactive for over a given period of time can be automatically disabled. It is recommended that accounts that are inactive for 30 days after password expiration be disabled.

**MITRE ATT&CK Mappings:** T1078


Section #: 5.5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.5.1.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** User accounts that have been inactive for over a given period of time can be automatically disabled. It is recommended that accounts that are inactive for 30 days after password expiration be disabled.

**MITRE ATT&CK Mappings:** T1078.002


Section #: 5.5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.5.1.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** User accounts that have been inactive for over a given period of time can be automatically disabled. It is recommended that accounts that are inactive for 30 days after password expiration be disabled.

**MITRE ATT&CK Mappings:** T1078.003


Section #: 5.5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.5.1.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** User accounts that have been inactive for over a given period of time can be automatically disabled. It is recommended that accounts that are inactive for 30 days after password expiration be disabled.

**MITRE ATT&CK Mappings:** M1027


Section #: 5.5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.5.1.5

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** All users should have a password change date in the past.

**MITRE ATT&CK Mappings:** T1078


Section #: 5.5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.5.1.5

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** All users should have a password change date in the past.

**MITRE ATT&CK Mappings:** T1078.001


Section #: 5.5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.5.1.5

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** All users should have a password change date in the past.

**MITRE ATT&CK Mappings:** T1078.002


Section #: 5.5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.5.1.5

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** All users should have a password change date in the past.

**MITRE ATT&CK Mappings:** T1078.003


Section #: 5.5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.5.1.5

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** All users should have a password change date in the past.

**MITRE ATT&CK Mappings:** T1078.004


Section #: 5.5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.5.1.5

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** All users should have a password change date in the past.

**MITRE ATT&CK Mappings:** T1110


Section #: 5.5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.5.1.5

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** All users should have a password change date in the past.

**MITRE ATT&CK Mappings:** T1110.001


Section #: 5.5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.5.1.5

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** All users should have a password change date in the past.

**MITRE ATT&CK Mappings:** T1110.002


Section #: 5.5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.5.1.5

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** All users should have a password change date in the past.

**MITRE ATT&CK Mappings:** T1110.003


Section #: 5.5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.5.1.5

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** All users should have a password change date in the past.

**MITRE ATT&CK Mappings:** T1110.004


Section #: 6.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.1.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/etc/passwd` file contains user account information that is used by many system utilities and therefore must be readable for these utilities to operate.

**MITRE ATT&CK Mappings:** TA0005


Section #: 6.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.1.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/etc/passwd` file contains user account information that is used by many system utilities and therefore must be readable for these utilities to operate.

**MITRE ATT&CK Mappings:** T1003


Section #: 6.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.1.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/etc/passwd` file contains user account information that is used by many system utilities and therefore must be readable for these utilities to operate.

**MITRE ATT&CK Mappings:** T1003.008


Section #: 6.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.1.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/etc/passwd` file contains user account information that is used by many system utilities and therefore must be readable for these utilities to operate.

**MITRE ATT&CK Mappings:** T1222


Section #: 6.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.1.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/etc/passwd` file contains user account information that is used by many system utilities and therefore must be readable for these utilities to operate.

**MITRE ATT&CK Mappings:** T1222.002


Section #: 6.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.1.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/etc/passwd` file contains user account information that is used by many system utilities and therefore must be readable for these utilities to operate.

**MITRE ATT&CK Mappings:** M1022


Section #: 6.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.1.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/etc/passwd-` file contains backup user account information.

**MITRE ATT&CK Mappings:** TA0005


Section #: 6.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.1.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/etc/passwd-` file contains backup user account information.

**MITRE ATT&CK Mappings:** T1003


Section #: 6.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.1.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/etc/passwd-` file contains backup user account information.

**MITRE ATT&CK Mappings:** T1003.008


Section #: 6.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.1.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/etc/passwd-` file contains backup user account information.

**MITRE ATT&CK Mappings:** T1222


Section #: 6.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.1.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/etc/passwd-` file contains backup user account information.

**MITRE ATT&CK Mappings:** T1222.002


Section #: 6.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.1.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/etc/passwd-` file contains backup user account information.

**MITRE ATT&CK Mappings:** M1022


Section #: 6.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.1.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/etc/group` file contains a list of all the valid groups defined in the system. The command below allows read/write access for root and read access for everyone else.

**MITRE ATT&CK Mappings:** TA0005


Section #: 6.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.1.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/etc/group` file contains a list of all the valid groups defined in the system. The command below allows read/write access for root and read access for everyone else.

**MITRE ATT&CK Mappings:** T1003


Section #: 6.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.1.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/etc/group` file contains a list of all the valid groups defined in the system. The command below allows read/write access for root and read access for everyone else.

**MITRE ATT&CK Mappings:** T1003.008


Section #: 6.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.1.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/etc/group` file contains a list of all the valid groups defined in the system. The command below allows read/write access for root and read access for everyone else.

**MITRE ATT&CK Mappings:** T1222


Section #: 6.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.1.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/etc/group` file contains a list of all the valid groups defined in the system. The command below allows read/write access for root and read access for everyone else.

**MITRE ATT&CK Mappings:** T1222.002


Section #: 6.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.1.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/etc/group` file contains a list of all the valid groups defined in the system. The command below allows read/write access for root and read access for everyone else.

**MITRE ATT&CK Mappings:** M1022


Section #: 6.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.1.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/etc/group-` file contains a backup list of all the valid groups defined in the system.

**MITRE ATT&CK Mappings:** TA0005


Section #: 6.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.1.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/etc/group-` file contains a backup list of all the valid groups defined in the system.

**MITRE ATT&CK Mappings:** T1003


Section #: 6.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.1.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/etc/group-` file contains a backup list of all the valid groups defined in the system.

**MITRE ATT&CK Mappings:** T1003.008


Section #: 6.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.1.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/etc/group-` file contains a backup list of all the valid groups defined in the system.

**MITRE ATT&CK Mappings:** T1222


Section #: 6.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.1.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/etc/group-` file contains a backup list of all the valid groups defined in the system.

**MITRE ATT&CK Mappings:** T1222.002


Section #: 6.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.1.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/etc/group-` file contains a backup list of all the valid groups defined in the system.

**MITRE ATT&CK Mappings:** M1022


Section #: 6.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.1.5

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/etc/shadow` file is used to store the information about user accounts that is critical to the security of those accounts, such as the hashed password and other security information.

**MITRE ATT&CK Mappings:** TA0005


Section #: 6.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.1.5

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/etc/shadow` file is used to store the information about user accounts that is critical to the security of those accounts, such as the hashed password and other security information.

**MITRE ATT&CK Mappings:** T1003


Section #: 6.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.1.5

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/etc/shadow` file is used to store the information about user accounts that is critical to the security of those accounts, such as the hashed password and other security information.

**MITRE ATT&CK Mappings:** T1003.008


Section #: 6.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.1.5

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/etc/shadow` file is used to store the information about user accounts that is critical to the security of those accounts, such as the hashed password and other security information.

**MITRE ATT&CK Mappings:** T1222


Section #: 6.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.1.5

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/etc/shadow` file is used to store the information about user accounts that is critical to the security of those accounts, such as the hashed password and other security information.

**MITRE ATT&CK Mappings:** T1222.002


Section #: 6.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.1.5

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/etc/shadow` file is used to store the information about user accounts that is critical to the security of those accounts, such as the hashed password and other security information.

**MITRE ATT&CK Mappings:** M1022


Section #: 6.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.1.6

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/etc/shadow-` file is used to store backup information about user accounts that is critical to the security of those accounts, such as the hashed password and other security information.

**MITRE ATT&CK Mappings:** TA0005


Section #: 6.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.1.6

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/etc/shadow-` file is used to store backup information about user accounts that is critical to the security of those accounts, such as the hashed password and other security information.

**MITRE ATT&CK Mappings:** T1003


Section #: 6.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.1.6

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/etc/shadow-` file is used to store backup information about user accounts that is critical to the security of those accounts, such as the hashed password and other security information.

**MITRE ATT&CK Mappings:** T1003.008


Section #: 6.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.1.6

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/etc/shadow-` file is used to store backup information about user accounts that is critical to the security of those accounts, such as the hashed password and other security information.

**MITRE ATT&CK Mappings:** T1222


Section #: 6.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.1.6

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/etc/shadow-` file is used to store backup information about user accounts that is critical to the security of those accounts, such as the hashed password and other security information.

**MITRE ATT&CK Mappings:** T1222.002


Section #: 6.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.1.6

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/etc/shadow-` file is used to store backup information about user accounts that is critical to the security of those accounts, such as the hashed password and other security information.

**MITRE ATT&CK Mappings:** M1022


Section #: 6.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.1.7

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/etc/gshadow` file is used to store the information about groups that is critical to the security of those accounts, such as the hashed password and other security information.

**MITRE ATT&CK Mappings:** TA0005


Section #: 6.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.1.7

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/etc/gshadow` file is used to store the information about groups that is critical to the security of those accounts, such as the hashed password and other security information.

**MITRE ATT&CK Mappings:** T1003


Section #: 6.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.1.7

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/etc/gshadow` file is used to store the information about groups that is critical to the security of those accounts, such as the hashed password and other security information.

**MITRE ATT&CK Mappings:** T1003.008


Section #: 6.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.1.7

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/etc/gshadow` file is used to store the information about groups that is critical to the security of those accounts, such as the hashed password and other security information.

**MITRE ATT&CK Mappings:** T1222


Section #: 6.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.1.7

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/etc/gshadow` file is used to store the information about groups that is critical to the security of those accounts, such as the hashed password and other security information.

**MITRE ATT&CK Mappings:** T1222.002


Section #: 6.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.1.7

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/etc/gshadow` file is used to store the information about groups that is critical to the security of those accounts, such as the hashed password and other security information.

**MITRE ATT&CK Mappings:** M1022


Section #: 6.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.1.8

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/etc/gshadow-` file is used to store backup information about groups that is critical to the security of those accounts, such as the hashed password and other security information.

**MITRE ATT&CK Mappings:** TA0005


Section #: 6.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.1.8

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/etc/gshadow-` file is used to store backup information about groups that is critical to the security of those accounts, such as the hashed password and other security information.

**MITRE ATT&CK Mappings:** T1003


Section #: 6.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.1.8

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/etc/gshadow-` file is used to store backup information about groups that is critical to the security of those accounts, such as the hashed password and other security information.

**MITRE ATT&CK Mappings:** T1003.008


Section #: 6.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.1.8

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/etc/gshadow-` file is used to store backup information about groups that is critical to the security of those accounts, such as the hashed password and other security information.

**MITRE ATT&CK Mappings:** T1222


Section #: 6.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.1.8

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/etc/gshadow-` file is used to store backup information about groups that is critical to the security of those accounts, such as the hashed password and other security information.

**MITRE ATT&CK Mappings:** T1222.002


Section #: 6.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.1.8

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `/etc/gshadow-` file is used to store backup information about groups that is critical to the security of those accounts, such as the hashed password and other security information.

**MITRE ATT&CK Mappings:** M1022


Section #: 6.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.1.9

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Unix-based systems support variable settings to control access to files. World writable files are the least secure. See the `chmod(2)` man page for more information.

**MITRE ATT&CK Mappings:** TA0005


Section #: 6.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.1.9

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Unix-based systems support variable settings to control access to files. World writable files are the least secure. See the `chmod(2)` man page for more information.

**MITRE ATT&CK Mappings:** T1222


Section #: 6.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.1.9

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Unix-based systems support variable settings to control access to files. World writable files are the least secure. See the `chmod(2)` man page for more information.

**MITRE ATT&CK Mappings:** T1222.002


Section #: 6.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.1.9

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Unix-based systems support variable settings to control access to files. World writable files are the least secure. See the `chmod(2)` man page for more information.

**MITRE ATT&CK Mappings:** M1022


Section #: 6.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.1.10

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Sometimes when administrators delete users from the password file they neglect to remove all files owned by those users from the system.

**MITRE ATT&CK Mappings:** TA0007


Section #: 6.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.1.10

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Sometimes when administrators delete users from the password file they neglect to remove all files owned by those users from the system.

**MITRE ATT&CK Mappings:** T1222


Section #: 6.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.1.10

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Sometimes when administrators delete users from the password file they neglect to remove all files owned by those users from the system.

**MITRE ATT&CK Mappings:** T1222.002


Section #: 6.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.1.11

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Sometimes when administrators delete users or groups from the system they neglect to remove all files owned by those users or groups.

**MITRE ATT&CK Mappings:** TA0007


Section #: 6.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.1.11

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Sometimes when administrators delete users or groups from the system they neglect to remove all files owned by those users or groups.

**MITRE ATT&CK Mappings:** T1222


Section #: 6.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.1.11

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Sometimes when administrators delete users or groups from the system they neglect to remove all files owned by those users or groups.

**MITRE ATT&CK Mappings:** T1222.002


Section #: 6.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.1.12

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The owner of a file can set the file's permissions to run with the owner's or group's permissions, even if the user running the program is not the owner or a member of the group. The most common reason for a SUID program is to enable users to perform functions (such as changing their password) that require root privileges.

**MITRE ATT&CK Mappings:** TA0004


Section #: 6.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.1.12

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The owner of a file can set the file's permissions to run with the owner's or group's permissions, even if the user running the program is not the owner or a member of the group. The most common reason for a SUID program is to enable users to perform functions (such as changing their password) that require root privileges.

**MITRE ATT&CK Mappings:** T1548


Section #: 6.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.1.12

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The owner of a file can set the file's permissions to run with the owner's or group's permissions, even if the user running the program is not the owner or a member of the group. The most common reason for a SUID program is to enable users to perform functions (such as changing their password) that require root privileges.

**MITRE ATT&CK Mappings:** T1548.001


Section #: 6.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.1.12

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The owner of a file can set the file's permissions to run with the owner's or group's permissions, even if the user running the program is not the owner or a member of the group. The most common reason for a SUID program is to enable users to perform functions (such as changing their password) that require root privileges.

**MITRE ATT&CK Mappings:** M1028


Section #: 6.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.1.13

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The owner of a file can set the file's permissions to run with the owner's or group's permissions, even if the user running the program is not the owner or a member of the group. The most common reason for a SGID program is to enable users to perform functions (such as changing their password) that require root privileges.

**MITRE ATT&CK Mappings:** TA0004


Section #: 6.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.1.13

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The owner of a file can set the file's permissions to run with the owner's or group's permissions, even if the user running the program is not the owner or a member of the group. The most common reason for a SGID program is to enable users to perform functions (such as changing their password) that require root privileges.

**MITRE ATT&CK Mappings:** T1548


Section #: 6.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.1.13

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The owner of a file can set the file's permissions to run with the owner's or group's permissions, even if the user running the program is not the owner or a member of the group. The most common reason for a SGID program is to enable users to perform functions (such as changing their password) that require root privileges.

**MITRE ATT&CK Mappings:** T1548.001


Section #: 6.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.1.13

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The owner of a file can set the file's permissions to run with the owner's or group's permissions, even if the user running the program is not the owner or a member of the group. The most common reason for a SGID program is to enable users to perform functions (such as changing their password) that require root privileges.

**MITRE ATT&CK Mappings:** M1028


Section #: 6.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.2.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Local accounts can uses shadowed passwords. With shadowed passwords, The passwords are saved in shadow password file, `/etc/shadow`, encrypted by a salted one-way hash. Accounts with a shadowed password have an `x` in the second field in `/etc/passwd`.

**MITRE ATT&CK Mappings:** TA0003


Section #: 6.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.2.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Local accounts can uses shadowed passwords. With shadowed passwords, The passwords are saved in shadow password file, `/etc/shadow`, encrypted by a salted one-way hash. Accounts with a shadowed password have an `x` in the second field in `/etc/passwd`.

**MITRE ATT&CK Mappings:** T1003


Section #: 6.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.2.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Local accounts can uses shadowed passwords. With shadowed passwords, The passwords are saved in shadow password file, `/etc/shadow`, encrypted by a salted one-way hash. Accounts with a shadowed password have an `x` in the second field in `/etc/passwd`.

**MITRE ATT&CK Mappings:** T1003.008


Section #: 6.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.2.1

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Local accounts can uses shadowed passwords. With shadowed passwords, The passwords are saved in shadow password file, `/etc/shadow`, encrypted by a salted one-way hash. Accounts with a shadowed password have an `x` in the second field in `/etc/passwd`.

**MITRE ATT&CK Mappings:** M1027


Section #: 6.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.2.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** An account with an empty password field means that anybody may log in as that user without providing a password.

**MITRE ATT&CK Mappings:** TA0003


Section #: 6.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.2.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** An account with an empty password field means that anybody may log in as that user without providing a password.

**MITRE ATT&CK Mappings:** T1078


Section #: 6.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.2.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** An account with an empty password field means that anybody may log in as that user without providing a password.

**MITRE ATT&CK Mappings:** T1078.001


Section #: 6.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.2.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** An account with an empty password field means that anybody may log in as that user without providing a password.

**MITRE ATT&CK Mappings:** T1078.003


Section #: 6.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.2.2

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** An account with an empty password field means that anybody may log in as that user without providing a password.

**MITRE ATT&CK Mappings:** M1027


Section #: 6.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.2.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Over time, system administration errors and changes can lead to groups being defined in `/etc/passwd` but not in `/etc/group` .

**MITRE ATT&CK Mappings:** TA0003


Section #: 6.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.2.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Over time, system administration errors and changes can lead to groups being defined in `/etc/passwd` but not in `/etc/group` .

**MITRE ATT&CK Mappings:** T1222


Section #: 6.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.2.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Over time, system administration errors and changes can lead to groups being defined in `/etc/passwd` but not in `/etc/group` .

**MITRE ATT&CK Mappings:** T1222.002


Section #: 6.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.2.3

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Over time, system administration errors and changes can lead to groups being defined in `/etc/passwd` but not in `/etc/group` .

**MITRE ATT&CK Mappings:** M1027


Section #: 6.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.2.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The shadow group allows system programs which require access the ability to read the /etc/shadow file. No users should be assigned to the shadow group.

**MITRE ATT&CK Mappings:** TA0005


Section #: 6.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.2.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The shadow group allows system programs which require access the ability to read the /etc/shadow file. No users should be assigned to the shadow group.

**MITRE ATT&CK Mappings:** T1003


Section #: 6.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.2.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The shadow group allows system programs which require access the ability to read the /etc/shadow file. No users should be assigned to the shadow group.

**MITRE ATT&CK Mappings:** T1003.008


Section #: 6.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.2.4

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The shadow group allows system programs which require access the ability to read the /etc/shadow file. No users should be assigned to the shadow group.

**MITRE ATT&CK Mappings:** M1022


Section #: 6.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.2.5

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Although the `useradd` program will not let you create a duplicate User ID (UID), it is possible for an administrator to manually edit the `/etc/passwd` file and change the UID field.

**MITRE ATT&CK Mappings:** TA0005


Section #: 6.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.2.5

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Although the `useradd` program will not let you create a duplicate User ID (UID), it is possible for an administrator to manually edit the `/etc/passwd` file and change the UID field.

**MITRE ATT&CK Mappings:** T1078


Section #: 6.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.2.5

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Although the `useradd` program will not let you create a duplicate User ID (UID), it is possible for an administrator to manually edit the `/etc/passwd` file and change the UID field.

**MITRE ATT&CK Mappings:** T1078.001


Section #: 6.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.2.5

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Although the `useradd` program will not let you create a duplicate User ID (UID), it is possible for an administrator to manually edit the `/etc/passwd` file and change the UID field.

**MITRE ATT&CK Mappings:** T1078.003


Section #: 6.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.2.5

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Although the `useradd` program will not let you create a duplicate User ID (UID), it is possible for an administrator to manually edit the `/etc/passwd` file and change the UID field.

**MITRE ATT&CK Mappings:** M1027


Section #: 6.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.2.6

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Although the `groupadd` program will not let you create a duplicate Group ID (GID), it is possible for an administrator to manually edit the `/etc/group` file and change the GID field.

**MITRE ATT&CK Mappings:** TA0005


Section #: 6.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.2.6

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Although the `groupadd` program will not let you create a duplicate Group ID (GID), it is possible for an administrator to manually edit the `/etc/group` file and change the GID field.

**MITRE ATT&CK Mappings:** T1078


Section #: 6.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.2.6

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Although the `groupadd` program will not let you create a duplicate Group ID (GID), it is possible for an administrator to manually edit the `/etc/group` file and change the GID field.

**MITRE ATT&CK Mappings:** T1078.001


Section #: 6.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.2.6

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Although the `groupadd` program will not let you create a duplicate Group ID (GID), it is possible for an administrator to manually edit the `/etc/group` file and change the GID field.

**MITRE ATT&CK Mappings:** T1078.003


Section #: 6.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.2.6

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Although the `groupadd` program will not let you create a duplicate Group ID (GID), it is possible for an administrator to manually edit the `/etc/group` file and change the GID field.

**MITRE ATT&CK Mappings:** M1027


Section #: 6.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.2.7

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Although the `useradd` program will not let you create a duplicate user name, it is possible for an administrator to manually edit the `/etc/passwd` file and change the user name.

**MITRE ATT&CK Mappings:** TA0004


Section #: 6.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.2.7

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Although the `useradd` program will not let you create a duplicate user name, it is possible for an administrator to manually edit the `/etc/passwd` file and change the user name.

**MITRE ATT&CK Mappings:** T1078


Section #: 6.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.2.7

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Although the `useradd` program will not let you create a duplicate user name, it is possible for an administrator to manually edit the `/etc/passwd` file and change the user name.

**MITRE ATT&CK Mappings:** T1078.001


Section #: 6.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.2.7

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Although the `useradd` program will not let you create a duplicate user name, it is possible for an administrator to manually edit the `/etc/passwd` file and change the user name.

**MITRE ATT&CK Mappings:** T1078.003


Section #: 6.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.2.7

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Although the `useradd` program will not let you create a duplicate user name, it is possible for an administrator to manually edit the `/etc/passwd` file and change the user name.

**MITRE ATT&CK Mappings:** M1027


Section #: 6.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.2.8

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Although the `groupadd` program will not let you create a duplicate group name, it is possible for an administrator to manually edit the `/etc/group` file and change the group name.

**MITRE ATT&CK Mappings:** TA0004


Section #: 6.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.2.8

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Although the `groupadd` program will not let you create a duplicate group name, it is possible for an administrator to manually edit the `/etc/group` file and change the group name.

**MITRE ATT&CK Mappings:** T1078


Section #: 6.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.2.8

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Although the `groupadd` program will not let you create a duplicate group name, it is possible for an administrator to manually edit the `/etc/group` file and change the group name.

**MITRE ATT&CK Mappings:** T1078.001


Section #: 6.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.2.8

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Although the `groupadd` program will not let you create a duplicate group name, it is possible for an administrator to manually edit the `/etc/group` file and change the group name.

**MITRE ATT&CK Mappings:** T1078.003


Section #: 6.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.2.8

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Although the `groupadd` program will not let you create a duplicate group name, it is possible for an administrator to manually edit the `/etc/group` file and change the group name.

**MITRE ATT&CK Mappings:** M1027


Section #: 6.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.2.9

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `root` user can execute any command on the system and could be fooled into executing programs unintentionally if the `PATH` is not set correctly.

**MITRE ATT&CK Mappings:** TA0006


Section #: 6.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.2.9

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `root` user can execute any command on the system and could be fooled into executing programs unintentionally if the `PATH` is not set correctly.

**MITRE ATT&CK Mappings:** T1204


Section #: 6.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.2.9

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `root` user can execute any command on the system and could be fooled into executing programs unintentionally if the `PATH` is not set correctly.

**MITRE ATT&CK Mappings:** T1204.002


Section #: 6.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.2.9

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `root` user can execute any command on the system and could be fooled into executing programs unintentionally if the `PATH` is not set correctly.

**MITRE ATT&CK Mappings:** M1022


Section #: 6.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.2.10

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Any account with UID 0 has superuser privileges on the system.

**MITRE ATT&CK Mappings:** TA0001


Section #: 6.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.2.10

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Any account with UID 0 has superuser privileges on the system.

**MITRE ATT&CK Mappings:** T1548


Section #: 6.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.2.10

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Any account with UID 0 has superuser privileges on the system.

**MITRE ATT&CK Mappings:** T1548.000


Section #: 6.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.2.10

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Any account with UID 0 has superuser privileges on the system.

**MITRE ATT&CK Mappings:** M1026


Section #: 6.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.2.11

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Users can be defined in `/etc/passwd` without a home directory or with a home directory that does not actually exist.

**MITRE ATT&CK Mappings:** TA0005


Section #: 6.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.2.11

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** Users can be defined in `/etc/passwd` without a home directory or with a home directory that does not actually exist.

**MITRE ATT&CK Mappings:** M1022


Section #: 6.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.2.12

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The user home directory is space defined for the particular user to set local environment variables and to store personal files.

**MITRE ATT&CK Mappings:** TA0005


Section #: 6.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.2.12

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The user home directory is space defined for the particular user to set local environment variables and to store personal files.

**MITRE ATT&CK Mappings:** T1222


Section #: 6.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.2.12

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The user home directory is space defined for the particular user to set local environment variables and to store personal files.

**MITRE ATT&CK Mappings:** T1222.002


Section #: 6.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.2.12

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The user home directory is space defined for the particular user to set local environment variables and to store personal files.

**MITRE ATT&CK Mappings:** M1022


Section #: 6.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.2.13

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** While the system administrator can establish secure permissions for users' home directories, the users can easily override these.

**MITRE ATT&CK Mappings:** TA0005


Section #: 6.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.2.13

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** While the system administrator can establish secure permissions for users' home directories, the users can easily override these.

**MITRE ATT&CK Mappings:** T1222


Section #: 6.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.2.13

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** While the system administrator can establish secure permissions for users' home directories, the users can easily override these.

**MITRE ATT&CK Mappings:** T1222.002


Section #: 6.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.2.13

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** While the system administrator can establish secure permissions for users' home directories, the users can easily override these.

**MITRE ATT&CK Mappings:** M1022


Section #: 6.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.2.14

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `.netrc` file contains data for logging into a remote host for file transfers via FTP.

While the system administrator can establish secure permissions for users' `.netrc` files, the users can easily override these.

**MITRE ATT&CK Mappings:** TA0006


Section #: 6.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.2.14

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `.netrc` file contains data for logging into a remote host for file transfers via FTP.

While the system administrator can establish secure permissions for users' `.netrc` files, the users can easily override these.

**MITRE ATT&CK Mappings:** T1152


Section #: 6.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.2.14

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `.netrc` file contains data for logging into a remote host for file transfers via FTP.

While the system administrator can establish secure permissions for users' `.netrc` files, the users can easily override these.

**MITRE ATT&CK Mappings:** T1552.001


Section #: 6.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.2.14

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `.netrc` file contains data for logging into a remote host for file transfers via FTP.

While the system administrator can establish secure permissions for users' `.netrc` files, the users can easily override these.

**MITRE ATT&CK Mappings:** M1027


Section #: 6.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.2.15

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `.forward` file specifies an email address to forward the user's mail to.

**MITRE ATT&CK Mappings:** TA0010


Section #: 6.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.2.15

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `.forward` file specifies an email address to forward the user's mail to.

**MITRE ATT&CK Mappings:** T1114


Section #: 6.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.2.15

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `.forward` file specifies an email address to forward the user's mail to.

**MITRE ATT&CK Mappings:** T1114.003


Section #: 6.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.2.15

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** The `.forward` file specifies an email address to forward the user's mail to.

**MITRE ATT&CK Mappings:** M1031


Section #: 6.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.2.16

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** While no `.rhosts` files are shipped by default, users can easily create them.

**MITRE ATT&CK Mappings:** TA0007


Section #: 6.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.2.16

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** While no `.rhosts` files are shipped by default, users can easily create them.

**MITRE ATT&CK Mappings:** T1049


Section #: 6.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.2.16

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** While no `.rhosts` files are shipped by default, users can easily create them.

**MITRE ATT&CK Mappings:** T1049.000


Section #: 6.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.2.17

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** While the system administrator can establish secure permissions for users' "dot" files, the users can easily override these.

**MITRE ATT&CK Mappings:** TA0005


Section #: 6.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.2.17

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** While the system administrator can establish secure permissions for users' "dot" files, the users can easily override these.

**MITRE ATT&CK Mappings:** T1222


Section #: 6.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.2.17

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** While the system administrator can establish secure permissions for users' "dot" files, the users can easily override these.

**MITRE ATT&CK Mappings:** T1222.001


Section #: 6.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.2.17

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** While the system administrator can establish secure permissions for users' "dot" files, the users can easily override these.

**MITRE ATT&CK Mappings:** T1222.002


Section #: 6.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.2.17

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** While the system administrator can establish secure permissions for users' "dot" files, the users can easily override these.

**MITRE ATT&CK Mappings:** T1552


Section #: 6.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.2.17

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** While the system administrator can establish secure permissions for users' "dot" files, the users can easily override these.

**MITRE ATT&CK Mappings:** T1552.003


Section #: 6.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.2.17

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** While the system administrator can establish secure permissions for users' "dot" files, the users can easily override these.

**MITRE ATT&CK Mappings:** T1552.004


Section #: 6.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 6.2.17

**Profiles:** Level 1 - Server, Level 1 - Workstation

**Description:** While the system administrator can establish secure permissions for users' "dot" files, the users can easily override these.

**MITRE ATT&CK Mappings:** M1022


