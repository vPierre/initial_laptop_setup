******************************************************************************
Systemhärtung - Härtungsmaßnahmen im Einzelnen auf Englisch Red Hat 9
******************************************************************************

.. sectionauthor:: Pierre Gronau <Pierre.Gronau@ndaal.eu>

.. contents:: Content - Härtungsmaßnahmen im Einzelnen auf Englisch Red Hat 9
   :depth: 3

.. _Section_Hardening_Red_Hat_9:

Red Hat 9 oder Derivate wie Oracle Linux, Alma Linux, Rocky Linux
------------------------------------------------------------------------------

Härtungsmaßnahmen im Einzelnen

.. literalinclude:: 2main.yml
   :language: yaml


Deployment Sicht
------------------------------------------------------------------------------

Hier wird angeführt, wie das alles ausgerollt wird (Deployment Sicht).

.. seealso:: Deployment Sicht

   Diese Einstellungen werden mittels eines bash Skriptes aufgebracht.

   .. figure::
      _static/bash-logo-by-vd.png
      :align: center
      :scale: 25 %
      :alt: bash Skript

      bash Skript

   
   <Platzhalter für den Link zum Repo>

   Diese Einstellungen werden mittels einer Ansible Rolle umgesetzt.

   .. figure::
      _static/Ansible_logo-700x700.png
      :align: center
      :scale: 25 %
      :alt: Ansible Rolle

      Ansible Rolle

   
   <Platzhalter für den Link zum Repo>


.....

.. Rubric:: Footnotes

.. [1]
   Debian Linux
   https://www.debian.org

.. [2]
   Ansible
   https://github.com/ansible/ansible

.. [3]
   ansible-lint
   https://github.com/ansible/ansible-lint

.. [4]
   Git
   https://github.com/git

.. [5]
   Git LFS (Large File Storage)
   https://git-lfs.com/

.. [6]
   Shellcheck
   https://github.com/koalaman/shellcheck

.. [7]
   shfmt
   https://github.com/patrickvane/shfmt

.. [8]
   Shellharden
   https://github.com/anordal/shellharden

.. [9]
   VirtualBox
   https://www.oracle.com/virtualization/technologies/vm/virtualbox.html

.. [10]
   Vagrant
   https://github.com/hashicorp/vagrant

.. [11]
   Vagrant Plugin - vagrant-scp
   https://github.com/invernizzi/vagrant-scp

.. [12]
   VSCodium
   https://github.com/VSCodium/vscodium

.. [13]
   Wget
   https://github.com/mirror/wget

.. [14]
   cURL
   https://github.com/curl/curl

.. [15]
   grep
   https://de.wikipedia.org/wiki/Grep

.. [16]
   egrep
   https://de.wikipedia.org/wiki/Grep

.. [17]
   ripgrep
   https://github.com/BurntSushi/ripgrep

.. [18]
   ClamAV
   https://github.com/Cisco-Talos/clamav

.. [19]
   Rust
   https://github.com/rust-lang/rust

.. [20]
   Cargo
   https://github.com/rust-lang/cargo

.. [21]
   YARA
   https://github.com/VirusTotal/yara

.. [22]
   YARA-X
   https://github.com/VirusTotal/yara-x

.. [23]
   Packer
   https://github.com/hashicorp/packer

.. [24]
   Checkov
   https://github.com/bridgecrewio/checkov

.. [25]
   Jenkins
   https://github.com/jenkinsci/jenkins

.. [26]
   Java - OpenJDK
   https://github.com/openjdk/jdk

.. [27]
   Podman
   https://github.com/containers/podman
