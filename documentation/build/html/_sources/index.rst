.. raw:: latex

    \pagestyle{plain}{
    \lhead{}
    \fancyhead[R]{\includegraphics{Logo.png}
    \fancyhead[L]{}}
    \fancyfoot[C]{\textbf{Internal}}}
    \fancyfoot[L]{\textbf{Chapter \thechapter}}

.. toctree::
   :numbered:
   :glob:
   :maxdepth: 3
   :caption: Inhalt - Systemhärtung:


   _document/intro.rst
   ;;definitions.rst
   ;;glossary.rst
   _abstract/abstract.rst
   Einführung.rst
   _arc42/Aufgabenstellung.rst
   _arc42/Qualitätsziele.rst
   _arc42/Stakeholder.rst
   _arc42/Randbedingungen.rst
   _arc42/Kontextabgrenzung.rst
   _ADRs/ADR.rst
   Regeln_für_die_Systemhärtung.rst
   Verzeichnisse.rst
   Aufbau_einer_Systemhärtung.rst
   _GOSS/GOSS.rst
   Systemhärtungsumgebung.rst

   #Systemhärtung.rst


   _hardening/Hardening.rst
   _FAQs/FAQs.rst
   _root_cause/root_cause.rst
   _document/Document.rst
   _legal/Information.rst
   Systemhärtung_components.rst

   ;search.rst

   _appendix/appendix.rst

.. toctree::
   :numbered:
   :glob:
   :maxdepth: 3

   _finalchapter/figs.rst
   _finalchapter/tables.rst
   _finalchapter/bib.rst
   _finalchapter/final.rst

.. index:: CIS
.. index:: Systemhärtung
.. index:: Härtung
.. index:: Linux
.. index:: bash
   
.. raw:: latex

    \clearpage
    \pagenumbering{arabic}
