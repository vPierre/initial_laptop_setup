******************************************************************************
Systemhärtung - Regeln für die Systemhärtung
******************************************************************************

.. sectionauthor:: Pierre Gronau <Pierre.Gronau@ndaal.eu>

.. contents:: Inhalt - Sicherheit - Container
   :depth: 3


Regeln für die Systemhärtung
------------------------------------------------------------------------------

Okay. Lass uns anfangen, 3, 2, 1... STOP!

Wesentliche Änderungen an Ihren Systemen können riskant sein.

Die wichtigste Regel der Systemhärtung
==============================================================================

Die **wichtigste Regel der Systemhärtung**, die vernünftige Administratoren 
befolgen, ist:

Eine Produktionsumgebung ist die echte Instanz der Anwendung, 
also machen Sie Ihre Änderungen in der Entwicklungs-/Testumgebung!

Die zweitwichtigste Regel
==============================================================================

Tun Sie nichts, was die Verfügbarkeit des Dienstes oder Ihres Systems 
beeinträchtigen könnte.

Die dritte Regel
==============================================================================

Machen Sie Sicherungskopien (Vollsicherung) der gesamten virtuellen Maschine,
Hardware und wichtiger Komponenten.

Und die letzte Regel
==============================================================================

Denken Sie darüber nach, was Sie tatsächlich mit Ihrem Server tun.

Ausführliche Erklärungen der Regeln
==============================================================================

**Eine Produktionsumgebung ist die echte Instanz der Anwendung, also machen 
Sie Ihre Änderungen in der Entwicklungs-/Testumgebung!**

Erklärung: Diese Regel betont die Bedeutung von Entwicklungs- und 
Testumgebungen, bevor Änderungen an der Produktionsumgebung vorgenommen 
werden. Die Produktionsumgebung ist die Umgebung, in der die Anwendung 
tatsächlich für die Benutzer läuft. Änderungen in dieser Umgebung können 
zu Ausfallzeiten oder Fehlern führen, die den Betrieb beeinträchtigen. 
Daher sollten alle neuen Funktionen, Updates oder Konfigurationen zuerst 
in einer sicheren Entwicklungs- oder Testumgebung getestet werden, 
um sicherzustellen, dass sie wie erwartet funktionieren, 
ohne die Benutzer zu stören.

**Tun Sie nichts, was die Verfügbarkeit des Dienstes oder Ihres Systems 
beeinträchtigen könnte.**

Erklärung: Diese Regel warnt davor, Änderungen vorzunehmen, 
die die Erreichbarkeit oder Funktionalität des Systems beeinträchtigen 
könnten. Dies umfasst Änderungen, die zu Ausfallzeiten führen, 
wie das Stoppen von Diensten oder das Ändern von Netzinstellungen. 
Administratoren sollten sicherstellen, dass alle Änderungen 
so durchgeführt werden, dass der Dienst weiterhin verfügbar bleibt, 
um die Benutzererfahrung nicht zu beeinträchtigen. Dies kann durch 
sorgfältige Planung, Wartungsfenster oder das Implementieren 
von Redundanzen erreicht werden.

**Machen Sie Sicherungskopien der gesamten virtuellen Maschine 
und wichtiger Komponenten.**

Erklärung: Diese Regel hebt die Notwendigkeit hervor, regelmäßige 
Datensicherungen durchzuführen, um Datenverlust zu vermeiden. 
Vor größeren Änderungen sollten vollständige Sicherungskopien 
der virtuellen Maschinen (VMs) und kritischer Komponenten 
erstellt werden. Im Falle eines Fehlers oder unerwarteter Probleme 
können Administratoren das System schnell wiederherstellen, 
ohne Daten zu verlieren oder lange Ausfallzeiten zu haben. 
Backups sind ein wesentlicher Bestandteil jeder IT-Strategie 
und sollten regelmäßig überprüft und getestet werden, 
um sicherzustellen, dass sie im Notfall funktionieren.

**Denken Sie darüber nach, was Sie tatsächlich 
mit Ihrem Server tun.**

Erklärung: Diese Regel fordert Administratoren auf, 
die Auswirkungen ihrer Handlungen zu reflektieren. 
Es ist wichtig, die Konsequenzen jeder Änderung zu verstehen 
und zu bewerten, ob sie notwendig und sinnvoll ist. 
Dies bedeutet, dass Administratoren die Ziele ihrer Änderungen 
klar definieren und sicherstellen sollten, dass sie im Einklang 
mit den Gesamtzielen der Organisation stehen. Überlegtes Handeln 
kann dazu beitragen, unnötige Risiken zu vermeiden 
und die Integrität und Sicherheit des Systems zu gewährleisten.

Diese Regeln sind grundlegende Prinzipien 
für die Systemadministration und -sicherheit, 
die dazu beitragen, die Stabilität und Sicherheit 
von ITK-Systemen zu gewährleisten.
