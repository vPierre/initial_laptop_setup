******************************************************************************
FAQ - Systemhärtung - Debian 12.9 out of the box
******************************************************************************

.. sectionauthor:: Pierre Gronau <Pierre.Gronau@ndaal.eu>

.. contents:: Inhalt - FAQ - Systemhärtung
   :depth: 3

.. _Section_FAQ_hardening_Debian_out_of_the_box:

Zusammenfassung Status Systemhärtung
------------------------------------------------------------------------------

Hier ist die Übersicht der Systemhärtung nach Neuinstallation 
von Debian 12.9x [1]_ :

.. code-block:: bash
   :force:
   :caption: Zusammenfassung Status Systemhärtung nach Neuinstallation von Debian 12
   
   ################### SUMMARY ###################
        Total Available Checks : 242
            Total Runned Checks : 242
            Total Passed Checks : [ 112/242 ]
            Total Failed Checks : [ 130/242 ]
    Enabled Checks Percentage : 100.00 %
        Conformity Percentage : 46.28 % 

Status Systemhärtung
------------------------------------------------------------------------------

Systemhärtung nach Neuinstallation von Debian 12.9x für die einzelnen 
Sicherheitsmaßnahmen:

.. code-block:: bash
   :force:
   :caption: Status Systemhärtung nach Neuinstallation von Debian 12

    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/1.1.1.1_disable_freevxfs.sh
    1.1.1.1_disable_freevxfs  [INFO] Working on 1.1.1.1_disable_freevxfs
    1.1.1.1_disable_freevxfs  [INFO] [DESCRIPTION] Disable mounting of freevxfs filesystems.
    1.1.1.1_disable_freevxfs  [INFO] Checking Configuration
    1.1.1.1_disable_freevxfs  [INFO] Performing audit
    1.1.1.1_disable_freevxfs  [ KO ] freevxfs is enabled!
    1.1.1.1_disable_freevxfs  [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/1.1.1.2_disable_jffs2.sh
    1.1.1.2_disable_jffs2     [INFO] Working on 1.1.1.2_disable_jffs2
    1.1.1.2_disable_jffs2     [INFO] [DESCRIPTION] Disable mounting of jffs2 filesystems.
    1.1.1.2_disable_jffs2     [INFO] Checking Configuration
    1.1.1.2_disable_jffs2     [INFO] Performing audit
    1.1.1.2_disable_jffs2     [ KO ] jffs2 is enabled!
    1.1.1.2_disable_jffs2     [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/1.1.1.3_disable_hfs.sh
    1.1.1.3_disable_hfs       [INFO] Working on 1.1.1.3_disable_hfs
    1.1.1.3_disable_hfs       [INFO] [DESCRIPTION] Disable mounting of hfs filesystems.
    1.1.1.3_disable_hfs       [INFO] Checking Configuration
    1.1.1.3_disable_hfs       [INFO] Performing audit
    1.1.1.3_disable_hfs       [ KO ] hfs is enabled!
    1.1.1.3_disable_hfs       [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/1.1.1.4_disable_hfsplus.sh
    1.1.1.4_disable_hfsplus   [INFO] Working on 1.1.1.4_disable_hfsplus
    1.1.1.4_disable_hfsplus   [INFO] [DESCRIPTION] Disable mounting of hfsplus filesystems.
    1.1.1.4_disable_hfsplus   [INFO] Checking Configuration
    1.1.1.4_disable_hfsplus   [INFO] Performing audit
    1.1.1.4_disable_hfsplus   [ KO ] hfsplus is enabled!
    1.1.1.4_disable_hfsplus   [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/1.1.1.5_disable_squashfs.sh
    1.1.1.5_disable_squashfs  [INFO] Working on 1.1.1.5_disable_squashfs
    1.1.1.5_disable_squashfs  [INFO] [DESCRIPTION] Disable mounting of squashfs filesytems.
    1.1.1.5_disable_squashfs  [INFO] Checking Configuration
    1.1.1.5_disable_squashfs  [INFO] Performing audit
    1.1.1.5_disable_squashfs  [ KO ] squashfs is enabled!
    1.1.1.5_disable_squashfs  [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/1.1.1.6_disable_udf.sh
    1.1.1.6_disable_udf       [INFO] Working on 1.1.1.6_disable_udf
    1.1.1.6_disable_udf       [INFO] [DESCRIPTION] Disable mounting of udf filesystems.
    1.1.1.6_disable_udf       [INFO] Checking Configuration
    1.1.1.6_disable_udf       [INFO] Performing audit
    1.1.1.6_disable_udf       [ KO ] udf is enabled!
    1.1.1.6_disable_udf       [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/1.1.1.7_restrict_fat.sh
    1.1.1.7_restrict_fat      [INFO] Working on 1.1.1.7_restrict_fat
    1.1.1.7_restrict_fat      [INFO] [DESCRIPTION] Limit mounting of FAT filesystems.
    1.1.1.7_restrict_fat      [INFO] Checking Configuration
    1.1.1.7_restrict_fat      [INFO] Performing audit
    1.1.1.7_restrict_fat      [ KO ] CONFIG_VFAT_FS is enabled!
    1.1.1.7_restrict_fat      [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/1.1.1.8_disable_cramfs.sh
    1.1.1.8_disable_cramfs    [INFO] Working on 1.1.1.8_disable_cramfs
    1.1.1.8_disable_cramfs    [INFO] [DESCRIPTION] Disable mounting of cramfs filesystems.
    1.1.1.8_disable_cramfs    [INFO] Checking Configuration
    1.1.1.8_disable_cramfs    [INFO] Performing audit
    1.1.1.8_disable_cramfs    [ OK ] cramfs is disabled
    1.1.1.8_disable_cramfs    [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/1.1.1.8_disable_cramfs.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/1.1.2_tmp_partition.sh
    1.1.2_tmp_partition       [INFO] Working on 1.1.2_tmp_partition
    1.1.2_tmp_partition       [INFO] [DESCRIPTION] Ensure /tmp is configured (Scored)
    1.1.2_tmp_partition       [INFO] Checking Configuration
    1.1.2_tmp_partition       [INFO] Performing audit
    1.1.2_tmp_partition       [INFO] Verifying that /tmp is a partition
    1.1.2_tmp_partition       [ KO ] /tmp is not a partition
    1.1.2_tmp_partition       [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/1.1.3_tmp_nodev.sh
    1.1.3_tmp_nodev           [INFO] Working on 1.1.3_tmp_nodev
    1.1.3_tmp_nodev           [INFO] [DESCRIPTION] /tmp partition with nodev option.
    1.1.3_tmp_nodev           [INFO] Checking Configuration
    1.1.3_tmp_nodev           [INFO] Performing audit
    1.1.3_tmp_nodev           [INFO] Verifying that /tmp is a partition
    1.1.3_tmp_nodev           [ KO ] /tmp is not a partition
    1.1.3_tmp_nodev           [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/1.1.4_tmp_nosuid.sh
    1.1.4_tmp_nosuid          [INFO] Working on 1.1.4_tmp_nosuid
    1.1.4_tmp_nosuid          [INFO] [DESCRIPTION] /tmp partition with nosuid option.
    1.1.4_tmp_nosuid          [INFO] Checking Configuration
    1.1.4_tmp_nosuid          [INFO] Performing audit
    1.1.4_tmp_nosuid          [INFO] Verifying that /tmp is a partition
    1.1.4_tmp_nosuid          [ KO ] /tmp is not a partition
    1.1.4_tmp_nosuid          [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/1.1.5_tmp_noexec.sh
    1.1.5_tmp_noexec          [INFO] Working on 1.1.5_tmp_noexec
    1.1.5_tmp_noexec          [INFO] [DESCRIPTION] /tmp partition with noexec option.
    1.1.5_tmp_noexec          [INFO] Checking Configuration
    1.1.5_tmp_noexec          [INFO] Performing audit
    1.1.5_tmp_noexec          [INFO] Verifying that /tmp is a partition
    1.1.5_tmp_noexec          [ KO ] /tmp is not a partition
    1.1.5_tmp_noexec          [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/1.1.6.1_var_nodev.sh
    1.1.6.1_var_nodev         [INFO] Working on 1.1.6.1_var_nodev
    1.1.6.1_var_nodev         [INFO] [DESCRIPTION] /var partition with nodev option.
    1.1.6.1_var_nodev         [INFO] Checking Configuration
    1.1.6.1_var_nodev         [INFO] Performing audit
    1.1.6.1_var_nodev         [INFO] Verifying that /var is a partition
    1.1.6.1_var_nodev         [ KO ] /var is not a partition
    1.1.6.1_var_nodev         [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/1.1.6.2_var_nosuid.sh
    1.1.6.2_var_nosuid        [INFO] Working on 1.1.6.2_var_nosuid
    1.1.6.2_var_nosuid        [INFO] [DESCRIPTION] /var partition with nosuid option.
    1.1.6.2_var_nosuid        [INFO] Checking Configuration
    1.1.6.2_var_nosuid        [INFO] Performing audit
    1.1.6.2_var_nosuid        [INFO] Verifying that /var is a partition
    1.1.6.2_var_nosuid        [ KO ] /var is not a partition
    1.1.6.2_var_nosuid        [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/1.1.6_var_partition.sh
    1.1.6_var_partition       [INFO] Working on 1.1.6_var_partition
    1.1.6_var_partition       [INFO] [DESCRIPTION] /var on a separate partition.
    1.1.6_var_partition       [INFO] Checking Configuration
    1.1.6_var_partition       [INFO] Performing audit
    1.1.6_var_partition       [INFO] Verifying that /var is a partition
    1.1.6_var_partition       [ KO ] /var is not a partition
    1.1.6_var_partition       [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/1.1.7_var_tmp_partition.sh
    1.1.7_var_tmp_partition   [INFO] Working on 1.1.7_var_tmp_partition
    1.1.7_var_tmp_partition   [INFO] [DESCRIPTION] /var/tmp on a separate partition.
    1.1.7_var_tmp_partition   [INFO] Checking Configuration
    1.1.7_var_tmp_partition   [INFO] Performing audit
    1.1.7_var_tmp_partition   [INFO] Verifying that /var/tmp is a partition
    1.1.7_var_tmp_partition   [ KO ] /var/tmp is not a partition
    1.1.7_var_tmp_partition   [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/1.1.8_var_tmp_nodev.sh
    1.1.8_var_tmp_nodev       [INFO] Working on 1.1.8_var_tmp_nodev
    1.1.8_var_tmp_nodev       [INFO] [DESCRIPTION] /var/tmp partition with nodev option.
    1.1.8_var_tmp_nodev       [INFO] Checking Configuration
    1.1.8_var_tmp_nodev       [INFO] Performing audit
    1.1.8_var_tmp_nodev       [INFO] Verifying that /var/tmp is a partition
    1.1.8_var_tmp_nodev       [ KO ] /var/tmp is not a partition
    1.1.8_var_tmp_nodev       [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/1.1.9_var_tmp_nosuid.sh
    1.1.9_var_tmp_nosuid      [INFO] Working on 1.1.9_var_tmp_nosuid
    1.1.9_var_tmp_nosuid      [INFO] [DESCRIPTION] /var/tmp partition with nosuid option.
    1.1.9_var_tmp_nosuid      [INFO] Checking Configuration
    1.1.9_var_tmp_nosuid      [INFO] Performing audit
    1.1.9_var_tmp_nosuid      [INFO] Verifying that /var/tmp is a partition
    1.1.9_var_tmp_nosuid      [ KO ] /var/tmp is not a partition
    1.1.9_var_tmp_nosuid      [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/1.1.10_var_tmp_noexec.sh
    ^C
    root@debian-automation-controller:/home/vagrant/debian-cis/bin# /home/vagrant/debian-cis/bin/hardening.sh --allow-unsupported-distribution --audit-all-enable-passed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/1.1.1.1_disable_freevxfs.sh
    1.1.1.1_disable_freevxfs  [INFO] Working on 1.1.1.1_disable_freevxfs
    1.1.1.1_disable_freevxfs  [INFO] [DESCRIPTION] Disable mounting of freevxfs filesystems.
    1.1.1.1_disable_freevxfs  [INFO] Checking Configuration
    1.1.1.1_disable_freevxfs  [INFO] Performing audit
    1.1.1.1_disable_freevxfs  [ KO ] freevxfs is enabled!
    1.1.1.1_disable_freevxfs  [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/1.1.1.2_disable_jffs2.sh
    1.1.1.2_disable_jffs2     [INFO] Working on 1.1.1.2_disable_jffs2
    1.1.1.2_disable_jffs2     [INFO] [DESCRIPTION] Disable mounting of jffs2 filesystems.
    1.1.1.2_disable_jffs2     [INFO] Checking Configuration
    1.1.1.2_disable_jffs2     [INFO] Performing audit
    1.1.1.2_disable_jffs2     [ KO ] jffs2 is enabled!
    1.1.1.2_disable_jffs2     [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/1.1.1.3_disable_hfs.sh
    1.1.1.3_disable_hfs       [INFO] Working on 1.1.1.3_disable_hfs
    1.1.1.3_disable_hfs       [INFO] [DESCRIPTION] Disable mounting of hfs filesystems.
    1.1.1.3_disable_hfs       [INFO] Checking Configuration
    1.1.1.3_disable_hfs       [INFO] Performing audit
    1.1.1.3_disable_hfs       [ KO ] hfs is enabled!
    1.1.1.3_disable_hfs       [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/1.1.1.4_disable_hfsplus.sh
    1.1.1.4_disable_hfsplus   [INFO] Working on 1.1.1.4_disable_hfsplus
    1.1.1.4_disable_hfsplus   [INFO] [DESCRIPTION] Disable mounting of hfsplus filesystems.
    1.1.1.4_disable_hfsplus   [INFO] Checking Configuration
    1.1.1.4_disable_hfsplus   [INFO] Performing audit
    1.1.1.4_disable_hfsplus   [ KO ] hfsplus is enabled!
    1.1.1.4_disable_hfsplus   [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/1.1.1.5_disable_squashfs.sh
    1.1.1.5_disable_squashfs  [INFO] Working on 1.1.1.5_disable_squashfs
    1.1.1.5_disable_squashfs  [INFO] [DESCRIPTION] Disable mounting of squashfs filesytems.
    1.1.1.5_disable_squashfs  [INFO] Checking Configuration
    1.1.1.5_disable_squashfs  [INFO] Performing audit
    1.1.1.5_disable_squashfs  [ KO ] squashfs is enabled!
    1.1.1.5_disable_squashfs  [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/1.1.1.6_disable_udf.sh
    1.1.1.6_disable_udf       [INFO] Working on 1.1.1.6_disable_udf
    1.1.1.6_disable_udf       [INFO] [DESCRIPTION] Disable mounting of udf filesystems.
    1.1.1.6_disable_udf       [INFO] Checking Configuration
    1.1.1.6_disable_udf       [INFO] Performing audit
    1.1.1.6_disable_udf       [ KO ] udf is enabled!
    1.1.1.6_disable_udf       [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/1.1.1.7_restrict_fat.sh
    1.1.1.7_restrict_fat      [INFO] Working on 1.1.1.7_restrict_fat
    1.1.1.7_restrict_fat      [INFO] [DESCRIPTION] Limit mounting of FAT filesystems.
    1.1.1.7_restrict_fat      [INFO] Checking Configuration
    1.1.1.7_restrict_fat      [INFO] Performing audit
    1.1.1.7_restrict_fat      [ KO ] CONFIG_VFAT_FS is enabled!
    1.1.1.7_restrict_fat      [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/1.1.1.8_disable_cramfs.sh
    1.1.1.8_disable_cramfs    [INFO] Working on 1.1.1.8_disable_cramfs
    1.1.1.8_disable_cramfs    [INFO] [DESCRIPTION] Disable mounting of cramfs filesystems.
    1.1.1.8_disable_cramfs    [INFO] Checking Configuration
    1.1.1.8_disable_cramfs    [INFO] Performing audit
    1.1.1.8_disable_cramfs    [ OK ] cramfs is disabled
    1.1.1.8_disable_cramfs    [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/1.1.1.8_disable_cramfs.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/1.1.2_tmp_partition.sh
    1.1.2_tmp_partition       [INFO] Working on 1.1.2_tmp_partition
    1.1.2_tmp_partition       [INFO] [DESCRIPTION] Ensure /tmp is configured (Scored)
    1.1.2_tmp_partition       [INFO] Checking Configuration
    1.1.2_tmp_partition       [INFO] Performing audit
    1.1.2_tmp_partition       [INFO] Verifying that /tmp is a partition
    1.1.2_tmp_partition       [ KO ] /tmp is not a partition
    1.1.2_tmp_partition       [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/1.1.3_tmp_nodev.sh
    1.1.3_tmp_nodev           [INFO] Working on 1.1.3_tmp_nodev
    1.1.3_tmp_nodev           [INFO] [DESCRIPTION] /tmp partition with nodev option.
    1.1.3_tmp_nodev           [INFO] Checking Configuration
    1.1.3_tmp_nodev           [INFO] Performing audit
    1.1.3_tmp_nodev           [INFO] Verifying that /tmp is a partition
    1.1.3_tmp_nodev           [ KO ] /tmp is not a partition
    1.1.3_tmp_nodev           [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/1.1.4_tmp_nosuid.sh
    1.1.4_tmp_nosuid          [INFO] Working on 1.1.4_tmp_nosuid
    1.1.4_tmp_nosuid          [INFO] [DESCRIPTION] /tmp partition with nosuid option.
    1.1.4_tmp_nosuid          [INFO] Checking Configuration
    1.1.4_tmp_nosuid          [INFO] Performing audit
    1.1.4_tmp_nosuid          [INFO] Verifying that /tmp is a partition
    1.1.4_tmp_nosuid          [ KO ] /tmp is not a partition
    1.1.4_tmp_nosuid          [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/1.1.5_tmp_noexec.sh
    1.1.5_tmp_noexec          [INFO] Working on 1.1.5_tmp_noexec
    1.1.5_tmp_noexec          [INFO] [DESCRIPTION] /tmp partition with noexec option.
    1.1.5_tmp_noexec          [INFO] Checking Configuration
    1.1.5_tmp_noexec          [INFO] Performing audit
    1.1.5_tmp_noexec          [INFO] Verifying that /tmp is a partition
    1.1.5_tmp_noexec          [ KO ] /tmp is not a partition
    1.1.5_tmp_noexec          [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/1.1.6.1_var_nodev.sh
    1.1.6.1_var_nodev         [INFO] Working on 1.1.6.1_var_nodev
    1.1.6.1_var_nodev         [INFO] [DESCRIPTION] /var partition with nodev option.
    1.1.6.1_var_nodev         [INFO] Checking Configuration
    1.1.6.1_var_nodev         [INFO] Performing audit
    1.1.6.1_var_nodev         [INFO] Verifying that /var is a partition
    1.1.6.1_var_nodev         [ KO ] /var is not a partition
    1.1.6.1_var_nodev         [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/1.1.6.2_var_nosuid.sh
    1.1.6.2_var_nosuid        [INFO] Working on 1.1.6.2_var_nosuid
    1.1.6.2_var_nosuid        [INFO] [DESCRIPTION] /var partition with nosuid option.
    1.1.6.2_var_nosuid        [INFO] Checking Configuration
    1.1.6.2_var_nosuid        [INFO] Performing audit
    1.1.6.2_var_nosuid        [INFO] Verifying that /var is a partition
    1.1.6.2_var_nosuid        [ KO ] /var is not a partition
    1.1.6.2_var_nosuid        [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/1.1.6_var_partition.sh
    1.1.6_var_partition       [INFO] Working on 1.1.6_var_partition
    1.1.6_var_partition       [INFO] [DESCRIPTION] /var on a separate partition.
    1.1.6_var_partition       [INFO] Checking Configuration
    1.1.6_var_partition       [INFO] Performing audit
    1.1.6_var_partition       [INFO] Verifying that /var is a partition
    1.1.6_var_partition       [ KO ] /var is not a partition
    1.1.6_var_partition       [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/1.1.7_var_tmp_partition.sh
    1.1.7_var_tmp_partition   [INFO] Working on 1.1.7_var_tmp_partition
    1.1.7_var_tmp_partition   [INFO] [DESCRIPTION] /var/tmp on a separate partition.
    1.1.7_var_tmp_partition   [INFO] Checking Configuration
    1.1.7_var_tmp_partition   [INFO] Performing audit
    1.1.7_var_tmp_partition   [INFO] Verifying that /var/tmp is a partition
    1.1.7_var_tmp_partition   [ KO ] /var/tmp is not a partition
    1.1.7_var_tmp_partition   [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/1.1.8_var_tmp_nodev.sh
    1.1.8_var_tmp_nodev       [INFO] Working on 1.1.8_var_tmp_nodev
    1.1.8_var_tmp_nodev       [INFO] [DESCRIPTION] /var/tmp partition with nodev option.
    1.1.8_var_tmp_nodev       [INFO] Checking Configuration
    1.1.8_var_tmp_nodev       [INFO] Performing audit
    1.1.8_var_tmp_nodev       [INFO] Verifying that /var/tmp is a partition
    1.1.8_var_tmp_nodev       [ KO ] /var/tmp is not a partition
    1.1.8_var_tmp_nodev       [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/1.1.9_var_tmp_nosuid.sh
    1.1.9_var_tmp_nosuid      [INFO] Working on 1.1.9_var_tmp_nosuid
    1.1.9_var_tmp_nosuid      [INFO] [DESCRIPTION] /var/tmp partition with nosuid option.
    1.1.9_var_tmp_nosuid      [INFO] Checking Configuration
    1.1.9_var_tmp_nosuid      [INFO] Performing audit
    1.1.9_var_tmp_nosuid      [INFO] Verifying that /var/tmp is a partition
    1.1.9_var_tmp_nosuid      [ KO ] /var/tmp is not a partition
    1.1.9_var_tmp_nosuid      [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/1.1.10_var_tmp_noexec.sh
    1.1.10_var_tmp_noexec     [INFO] Working on 1.1.10_var_tmp_noexec
    1.1.10_var_tmp_noexec     [INFO] [DESCRIPTION] /var/tmp partition with noexec option.
    1.1.10_var_tmp_noexec     [INFO] Checking Configuration
    1.1.10_var_tmp_noexec     [INFO] Performing audit
    1.1.10_var_tmp_noexec     [INFO] Verifying that /var/tmp is a partition
    1.1.10_var_tmp_noexec     [ KO ] /var/tmp is not a partition
    1.1.10_var_tmp_noexec     [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/1.1.11.1_var_log_noexec.sh
    1.1.11.1_var_log_noexec   [INFO] Working on 1.1.11.1_var_log_noexec
    1.1.11.1_var_log_noexec   [INFO] [DESCRIPTION] /var/log partition with noexec option.
    1.1.11.1_var_log_noexec   [INFO] Checking Configuration
    1.1.11.1_var_log_noexec   [INFO] Performing audit
    1.1.11.1_var_log_noexec   [INFO] Verifying that /var/log is a partition
    1.1.11.1_var_log_noexec   [ KO ] /var/log is not a partition
    1.1.11.1_var_log_noexec   [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/1.1.11.2_var_log_nosuid.sh
    1.1.11.2_var_log_nosuid   [INFO] Working on 1.1.11.2_var_log_nosuid
    1.1.11.2_var_log_nosuid   [INFO] [DESCRIPTION] /var/log partition with nosuid option.
    1.1.11.2_var_log_nosuid   [INFO] Checking Configuration
    1.1.11.2_var_log_nosuid   [INFO] Performing audit
    1.1.11.2_var_log_nosuid   [INFO] Verifying that /var/log is a partition
    1.1.11.2_var_log_nosuid   [ KO ] /var/log is not a partition
    1.1.11.2_var_log_nosuid   [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/1.1.11.3_var_log_nodev.sh
    1.1.11.3_var_log_nodev    [INFO] Working on 1.1.11.3_var_log_nodev
    1.1.11.3_var_log_nodev    [INFO] [DESCRIPTION] /var/log partition with nodev option.
    1.1.11.3_var_log_nodev    [INFO] Checking Configuration
    1.1.11.3_var_log_nodev    [INFO] Performing audit
    1.1.11.3_var_log_nodev    [INFO] Verifying that /var/log is a partition
    1.1.11.3_var_log_nodev    [ KO ] /var/log is not a partition
    1.1.11.3_var_log_nodev    [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/1.1.11_var_log_partition.sh
    1.1.11_var_log_partition  [INFO] Working on 1.1.11_var_log_partition
    1.1.11_var_log_partition  [INFO] [DESCRIPTION] /var/log on separate partition.
    1.1.11_var_log_partition  [INFO] Checking Configuration
    1.1.11_var_log_partition  [INFO] Performing audit
    1.1.11_var_log_partition  [INFO] Verifying that /var/log is a partition
    1.1.11_var_log_partition  [ KO ] /var/log is not a partition
    1.1.11_var_log_partition  [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/1.1.12.1_var_log_audit_noexec.sh
    1.1.12.1_var_log_audit_no [INFO] Working on 1.1.12.1_var_log_audit_noexec
    1.1.12.1_var_log_audit_no [INFO] [DESCRIPTION] /var/log/audit partition with noexec option.
    1.1.12.1_var_log_audit_no [INFO] Checking Configuration
    1.1.12.1_var_log_audit_no [INFO] Performing audit
    1.1.12.1_var_log_audit_no [INFO] Verifying that /var/log/audit is a partition
    1.1.12.1_var_log_audit_no [ KO ] /var/log/audit is not a partition
    1.1.12.1_var_log_audit_no [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/1.1.12.2_var_log_audit_nosuid.sh
    1.1.12.2_var_log_audit_no [INFO] Working on 1.1.12.2_var_log_audit_nosuid
    1.1.12.2_var_log_audit_no [INFO] [DESCRIPTION] /var/log/audit partition with nosuid option.
    1.1.12.2_var_log_audit_no [INFO] Checking Configuration
    1.1.12.2_var_log_audit_no [INFO] Performing audit
    1.1.12.2_var_log_audit_no [INFO] Verifying that /var/log/audit is a partition
    1.1.12.2_var_log_audit_no [ KO ] /var/log/audit is not a partition
    1.1.12.2_var_log_audit_no [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/1.1.12.3_var_log_audit_nodev.sh
    1.1.12.3_var_log_audit_no [INFO] Working on 1.1.12.3_var_log_audit_nodev
    1.1.12.3_var_log_audit_no [INFO] [DESCRIPTION] /var/log/audit partition with nodev option.
    1.1.12.3_var_log_audit_no [INFO] Checking Configuration
    1.1.12.3_var_log_audit_no [INFO] Performing audit
    1.1.12.3_var_log_audit_no [INFO] Verifying that /var/log/audit is a partition
    1.1.12.3_var_log_audit_no [ KO ] /var/log/audit is not a partition
    1.1.12.3_var_log_audit_no [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/1.1.12_var_log_audit_partition.sh
    1.1.12_var_log_audit_part [INFO] Working on 1.1.12_var_log_audit_partition
    1.1.12_var_log_audit_part [INFO] [DESCRIPTION] /var/log/audit on a separate partition.
    1.1.12_var_log_audit_part [INFO] Checking Configuration
    1.1.12_var_log_audit_part [INFO] Performing audit
    1.1.12_var_log_audit_part [INFO] Verifying that /var/log/audit is a partition
    1.1.12_var_log_audit_part [ KO ] /var/log/audit is not a partition
    1.1.12_var_log_audit_part [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/1.1.13_home_partition.sh
    1.1.13_home_partition     [INFO] Working on 1.1.13_home_partition
    1.1.13_home_partition     [INFO] [DESCRIPTION] /home on a separate partition.
    1.1.13_home_partition     [INFO] Checking Configuration
    1.1.13_home_partition     [INFO] Performing audit
    1.1.13_home_partition     [INFO] Verifying that /home is a partition
    1.1.13_home_partition     [ KO ] /home is not a partition
    1.1.13_home_partition     [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/1.1.14.1_home_nosuid.sh
    1.1.14.1_home_nosuid      [INFO] Working on 1.1.14.1_home_nosuid
    1.1.14.1_home_nosuid      [INFO] [DESCRIPTION] /home partition with nosuid option.
    1.1.14.1_home_nosuid      [INFO] Checking Configuration
    1.1.14.1_home_nosuid      [INFO] Performing audit
    1.1.14.1_home_nosuid      [INFO] Verifying that /home is a partition
    1.1.14.1_home_nosuid      [ KO ] /home is not a partition
    1.1.14.1_home_nosuid      [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/1.1.14_home_nodev.sh
    1.1.14_home_nodev         [INFO] Working on 1.1.14_home_nodev
    1.1.14_home_nodev         [INFO] [DESCRIPTION] /home partition with nodev option.
    1.1.14_home_nodev         [INFO] Checking Configuration
    1.1.14_home_nodev         [INFO] Performing audit
    1.1.14_home_nodev         [INFO] Verifying that /home is a partition
    1.1.14_home_nodev         [ KO ] /home is not a partition
    1.1.14_home_nodev         [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/1.1.15_run_shm_nodev.sh
    1.1.15_run_shm_nodev      [INFO] Working on 1.1.15_run_shm_nodev
    1.1.15_run_shm_nodev      [INFO] [DESCRIPTION] /run/shm with nodev option.
    1.1.15_run_shm_nodev      [INFO] Checking Configuration
    1.1.15_run_shm_nodev      [INFO] Performing audit
    1.1.15_run_shm_nodev      [INFO] Verifying that /run/shm is a partition
    1.1.15_run_shm_nodev      [ OK ] /dev/shm is a partition
    1.1.15_run_shm_nodev      [ OK ] /dev/shm has nodev in fstab
    1.1.15_run_shm_nodev      [ OK ] /dev/shm mounted with nodev
    1.1.15_run_shm_nodev      [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/1.1.15_run_shm_nodev.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/1.1.16_run_shm_nosuid.sh
    1.1.16_run_shm_nosuid     [INFO] Working on 1.1.16_run_shm_nosuid
    1.1.16_run_shm_nosuid     [INFO] [DESCRIPTION] /run/shm with nosuid option.
    1.1.16_run_shm_nosuid     [INFO] Checking Configuration
    1.1.16_run_shm_nosuid     [INFO] Performing audit
    1.1.16_run_shm_nosuid     [INFO] Verifying that /run/shm is a partition
    1.1.16_run_shm_nosuid     [ OK ] /dev/shm is a partition
    1.1.16_run_shm_nosuid     [ OK ] /dev/shm has nosuid in fstab
    1.1.16_run_shm_nosuid     [ OK ] /dev/shm mounted with nosuid
    1.1.16_run_shm_nosuid     [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/1.1.16_run_shm_nosuid.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/1.1.17_run_shm_noexec.sh
    1.1.17_run_shm_noexec     [INFO] Working on 1.1.17_run_shm_noexec
    1.1.17_run_shm_noexec     [INFO] [DESCRIPTION] /run/shm with noexec option.
    1.1.17_run_shm_noexec     [INFO] Checking Configuration
    1.1.17_run_shm_noexec     [INFO] Performing audit
    1.1.17_run_shm_noexec     [INFO] Verifying that /run/shm is a partition
    1.1.17_run_shm_noexec     [ OK ] /dev/shm is a partition
    1.1.17_run_shm_noexec     [ KO ] /dev/shm has no option noexec in fstab!
    1.1.17_run_shm_noexec     [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/1.1.18_removable_device_nodev.sh
    1.1.18_removable_device_n [INFO] Working on 1.1.18_removable_device_nodev
    1.1.18_removable_device_n [INFO] [DESCRIPTION] nodev option for removable media partitions.
    1.1.18_removable_device_n [INFO] Checking Configuration
    1.1.18_removable_device_n [INFO] Performing audit
    1.1.18_removable_device_n [INFO] Verifying if there is /media\S* like partition
    1.1.18_removable_device_n [ OK ] There is no partition like /media\S*
    1.1.18_removable_device_n [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/1.1.18_removable_device_nodev.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/1.1.19_removable_device_nosuid.sh
    1.1.19_removable_device_n [INFO] Working on 1.1.19_removable_device_nosuid
    1.1.19_removable_device_n [INFO] [DESCRIPTION] nosuid option for removable media partitions.
    1.1.19_removable_device_n [INFO] Checking Configuration
    1.1.19_removable_device_n [INFO] Performing audit
    1.1.19_removable_device_n [INFO] Verifying if there is /media\S* like partition
    1.1.19_removable_device_n [ OK ] There is no partition like /media\S*
    1.1.19_removable_device_n [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/1.1.19_removable_device_nosuid.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/1.1.20_removable_device_noexec.sh
    1.1.20_removable_device_n [INFO] Working on 1.1.20_removable_device_noexec
    1.1.20_removable_device_n [INFO] [DESCRIPTION] noexec option for removable media partitions.
    1.1.20_removable_device_n [INFO] Checking Configuration
    1.1.20_removable_device_n [INFO] Performing audit
    1.1.20_removable_device_n [INFO] Verifying if there is /media\S* like partition
    1.1.20_removable_device_n [ OK ] There is no partition like /media\S*
    1.1.20_removable_device_n [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/1.1.20_removable_device_noexec.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/1.1.21_sticky_bit_world_writable_folder.sh
    1.1.21_sticky_bit_world_w [INFO] Working on 1.1.21_sticky_bit_world_writable_folder
    1.1.21_sticky_bit_world_w [INFO] [DESCRIPTION] Set sticky bit on world writable directories to prevent users from deleting or renaming files that are not owned by them.
    1.1.21_sticky_bit_world_w [INFO] Checking Configuration
    1.1.21_sticky_bit_world_w [INFO] Performing audit
    1.1.21_sticky_bit_world_w [INFO] Checking if setuid is set on world writable Directories
    1.1.21_sticky_bit_world_w [ OK ] All world writable directories have a sticky bit
    1.1.21_sticky_bit_world_w [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/1.1.21_sticky_bit_world_writable_folder.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/1.1.22_disable_automounting.sh
    1.1.22_disable_automounti [INFO] Working on 1.1.22_disable_automounting
    1.1.22_disable_automounti [INFO] [DESCRIPTION] Disable automounting of devices.
    1.1.22_disable_automounti [INFO] Checking Configuration
    1.1.22_disable_automounti [INFO] Performing audit
    1.1.22_disable_automounti [INFO] Checking if autofs is enabled
    1.1.22_disable_automounti [ OK ] autofs is disabled
    1.1.22_disable_automounti [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/1.1.22_disable_automounting.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/1.1.23_disable_usb_storage.sh
    1.1.23_disable_usb_storag [INFO] Working on 1.1.23_disable_usb_storage
    1.1.23_disable_usb_storag [INFO] [DESCRIPTION] Disable USB storage.
    1.1.23_disable_usb_storag [INFO] Checking Configuration
    1.1.23_disable_usb_storag [INFO] Performing audit
    1.1.23_disable_usb_storag [ KO ] usb-storage is enabled!
    1.1.23_disable_usb_storag [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/1.3.1_install_sudo.sh
    1.3.1_install_sudo        [INFO] Working on 1.3.1_install_sudo
    1.3.1_install_sudo        [INFO] [DESCRIPTION] Install sudo to permit users to execute command as superuser or as another user.
    1.3.1_install_sudo        [INFO] Checking Configuration
    1.3.1_install_sudo        [INFO] Performing audit
    1.3.1_install_sudo        [ OK ] sudo is installed
    1.3.1_install_sudo        [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/1.3.1_install_sudo.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/1.3.2_pty_sudo.sh
    1.3.2_pty_sudo            [INFO] Working on 1.3.2_pty_sudo
    1.3.2_pty_sudo            [INFO] [DESCRIPTION] Ensure sudo can only be run from a pseudo pty.
    1.3.2_pty_sudo            [INFO] Checking Configuration
    1.3.2_pty_sudo            [INFO] Performing audit
    1.3.2_pty_sudo            [ OK ] Defaults use_pty found in sudoers file
    1.3.2_pty_sudo            [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/1.3.2_pty_sudo.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/1.3.3_logfile_sudo.sh
    1.3.3_logfile_sudo        [INFO] Working on 1.3.3_logfile_sudo
    1.3.3_logfile_sudo        [INFO] [DESCRIPTION] Ensure sudo log files exists.
    1.3.3_logfile_sudo        [INFO] Checking Configuration
    1.3.3_logfile_sudo        [INFO] Performing audit
    1.3.3_logfile_sudo        [ KO ] Defaults log file not found in sudoers files
    1.3.3_logfile_sudo        [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/1.4.1_install_tripwire.sh
    1.4.1_install_tripwire    [INFO] Working on 1.4.1_install_tripwire
    1.4.1_install_tripwire    [INFO] [DESCRIPTION] Ensure tripwire package is installed.
    1.4.1_install_tripwire    [INFO] Checking Configuration
    1.4.1_install_tripwire    [INFO] Performing audit
    1.4.1_install_tripwire    [ KO ] tripwire is not installed!
    1.4.1_install_tripwire    [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/1.4.2_tripwire_cron.sh
    1.4.2_tripwire_cron       [INFO] Working on 1.4.2_tripwire_cron
    1.4.2_tripwire_cron       [INFO] [DESCRIPTION] Implemet periodic execution of file integrity.
    1.4.2_tripwire_cron       [INFO] Checking Configuration
    1.4.2_tripwire_cron       [INFO] Performing audit
    1.4.2_tripwire_cron       [ KO ] tripwire --check is not present in /etc/crontab /etc/cron.d/e2scrub_all
    /etc/cron.d/.placeholder
    1.4.2_tripwire_cron       [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/1.5.1_bootloader_ownership.sh
    1.5.1_bootloader_ownershi [INFO] Working on 1.5.1_bootloader_ownership
    1.5.1_bootloader_ownershi [INFO] [DESCRIPTION] User and group root owner of grub bootloader config.
    1.5.1_bootloader_ownershi [INFO] Checking Configuration
    1.5.1_bootloader_ownershi [INFO] Performing audit
    1.5.1_bootloader_ownershi [ OK ] /boot/grub/grub.cfg has correct ownership
    1.5.1_bootloader_ownershi [ OK ] /boot/grub/grub.cfg has correct permissions
    1.5.1_bootloader_ownershi [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/1.5.1_bootloader_ownership.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/1.5.2_bootloader_password.sh
    1.5.2_bootloader_password [INFO] Working on 1.5.2_bootloader_password
    1.5.2_bootloader_password [INFO] [DESCRIPTION] Setting bootloader password to secure boot parameters.
    1.5.2_bootloader_password [INFO] Checking Configuration
    1.5.2_bootloader_password [INFO] Performing audit
    1.5.2_bootloader_password [ KO ] ^set superusers not present in /boot/grub/grub.cfg
    1.5.2_bootloader_password [ KO ] ^password_pbkdf2 not present in /boot/grub/grub.cfg
    1.5.2_bootloader_password [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/1.5.3_root_password.sh
    1.5.3_root_password       [INFO] Working on 1.5.3_root_password
    1.5.3_root_password       [INFO] [DESCRIPTION] Root password for single user mode.
    1.5.3_root_password       [INFO] Checking Configuration
    1.5.3_root_password       [INFO] Performing audit
    1.5.3_root_password       [ OK ] ^root:[*\!]: is not present in /etc/shadow
    1.5.3_root_password       [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/1.5.3_root_password.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/1.6.1_enable_nx_support.sh
    1.6.1_enable_nx_support   [INFO] Working on 1.6.1_enable_nx_support
    1.6.1_enable_nx_support   [INFO] [DESCRIPTION] Enable NoExecute/ExecuteDisable to prevent buffer overflow attacks.
    1.6.1_enable_nx_support   [INFO] Checking Configuration
    1.6.1_enable_nx_support   [INFO] Performing audit
    1.6.1_enable_nx_support   [ OK ] NX[[:space:]]\(Execute[[:space:]]Disable\)[[:space:]]protection:[[:space:]]active is present in dmesg
    1.6.1_enable_nx_support   [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/1.6.1_enable_nx_support.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/1.6.2_enable_randomized_vm_placement.sh
    1.6.2_enable_randomized_v [INFO] Working on 1.6.2_enable_randomized_vm_placement
    1.6.2_enable_randomized_v [INFO] [DESCRIPTION] Enable Randomized Virtual Memory Region Placement to prevent memory page exploits.
    1.6.2_enable_randomized_v [INFO] Checking Configuration
    1.6.2_enable_randomized_v [INFO] Performing audit
    1.6.2_enable_randomized_v [ OK ] kernel.randomize_va_space correctly set to 2
    1.6.2_enable_randomized_v [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/1.6.2_enable_randomized_vm_placement.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/1.6.3.1_disable_apport.sh
    1.6.3.1_disable_apport    [INFO] Working on 1.6.3.1_disable_apport
    1.6.3.1_disable_apport    [INFO] [DESCRIPTION] Disable apport to avoid confidential data leaks.
    1.6.3.1_disable_apport    [INFO] Checking Configuration
    1.6.3.1_disable_apport    [INFO] Performing audit
    1.6.3.1_disable_apport    [ OK ] apport is absent
    1.6.3.1_disable_apport    [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/1.6.3.1_disable_apport.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/1.6.3_disable_prelink.sh
    1.6.3_disable_prelink     [INFO] Working on 1.6.3_disable_prelink
    1.6.3_disable_prelink     [INFO] [DESCRIPTION] Disable prelink to prevent libraries compromission.
    1.6.3_disable_prelink     [INFO] Checking Configuration
    1.6.3_disable_prelink     [INFO] Performing audit
    1.6.3_disable_prelink     [ OK ] prelink is absent
    1.6.3_disable_prelink     [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/1.6.3_disable_prelink.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/1.6.4_restrict_core_dumps.sh
    1.6.4_restrict_core_dumps [INFO] Working on 1.6.4_restrict_core_dumps
    1.6.4_restrict_core_dumps [INFO] [DESCRIPTION] Restrict core dumps.
    1.6.4_restrict_core_dumps [INFO] Checking Configuration
    1.6.4_restrict_core_dumps [INFO] Performing audit
    1.6.4_restrict_core_dumps [ KO ] ^\*[[:space:]]*hard[[:space:]]*core[[:space:]]*0$ is not present in /etc/security/limits.conf 
    1.6.4_restrict_core_dumps [ OK ] fs.suid_dumpable correctly set to 0
    1.6.4_restrict_core_dumps [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/1.7.1.1_install_apparmor.sh
    1.7.1.1_install_apparmor  [INFO] Working on 1.7.1.1_install_apparmor
    1.7.1.1_install_apparmor  [INFO] [DESCRIPTION] Install AppArmor.
    1.7.1.1_install_apparmor  [INFO] Checking Configuration
    1.7.1.1_install_apparmor  [INFO] Performing audit
    1.7.1.1_install_apparmor  [ OK ] apparmor is installed
    1.7.1.1_install_apparmor  [ KO ] apparmor-utils is absent!
    1.7.1.1_install_apparmor  [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/1.7.1.2_enable_apparmor.sh
    1.7.1.2_enable_apparmor   [INFO] Working on 1.7.1.2_enable_apparmor
    1.7.1.2_enable_apparmor   [INFO] [DESCRIPTION] Activate AppArmor to enforce permissions control.
    1.7.1.2_enable_apparmor   [INFO] Checking Configuration
    1.7.1.2_enable_apparmor   [INFO] Performing audit
    1.7.1.2_enable_apparmor   [ OK ] apparmor is installed
    1.7.1.2_enable_apparmor   [ KO ] apparmor-utils is absent!
    1.7.1.2_enable_apparmor   [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/1.7.1.3_enforce_or_complain_apparmor.sh
    1.7.1.3_enforce_or_compla [INFO] Working on 1.7.1.3_enforce_or_complain_apparmor
    1.7.1.3_enforce_or_compla [INFO] [DESCRIPTION] Enforce or complain AppArmor profiles.
    1.7.1.3_enforce_or_compla [INFO] Checking Configuration
    1.7.1.3_enforce_or_compla [INFO] Performing audit
    1.7.1.3_enforce_or_compla [ OK ] apparmor is installed
    1.7.1.3_enforce_or_compla [ KO ] apparmor-utils is absent!
    1.7.1.3_enforce_or_compla [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/1.7.1.4_enforcing_apparmor.sh
    1.7.1.4_enforcing_apparmo [INFO] Working on 1.7.1.4_enforcing_apparmor
    1.7.1.4_enforcing_apparmo [INFO] [DESCRIPTION] Enforce Apparmor profiles.
    1.7.1.4_enforcing_apparmo [INFO] Checking Configuration
    1.7.1.4_enforcing_apparmo [INFO] Performing audit
    1.7.1.4_enforcing_apparmo [ OK ] apparmor is installed
    1.7.1.4_enforcing_apparmo [ KO ] apparmor-utils is absent!
    1.7.1.4_enforcing_apparmo [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/1.8.1.1_remove_os_info_motd.sh
    1.8.1.1_remove_os_info_mo [INFO] Working on 1.8.1.1_remove_os_info_motd
    1.8.1.1_remove_os_info_mo [INFO] [DESCRIPTION] Remove OS information from motd
    1.8.1.1_remove_os_info_mo [INFO] Checking Configuration
    1.8.1.1_remove_os_info_mo [INFO] Performing audit
    1.8.1.1_remove_os_info_mo [ OK ] (\v|\r|\m|\s) is not present in /etc/motd
    1.8.1.1_remove_os_info_mo [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/1.8.1.1_remove_os_info_motd.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/1.8.1.2_remove_os_info_issue.sh
    1.8.1.2_remove_os_info_is [INFO] Working on 1.8.1.2_remove_os_info_issue
    1.8.1.2_remove_os_info_is [INFO] [DESCRIPTION] Remove OS information from Login Warning Banners.
    1.8.1.2_remove_os_info_is [INFO] Checking Configuration
    1.8.1.2_remove_os_info_is [INFO] Performing audit
    1.8.1.2_remove_os_info_is [ OK ] (\v|\r|\m|\s) is not present in /etc/issue
    1.8.1.2_remove_os_info_is [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/1.8.1.2_remove_os_info_issue.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/1.8.1.3_remove_os_info_issue_net.sh
    1.8.1.3_remove_os_info_is [INFO] Working on 1.8.1.3_remove_os_info_issue_net
    1.8.1.3_remove_os_info_is [INFO] [DESCRIPTION] Remove OS information from remote Login Warning Banners.
    1.8.1.3_remove_os_info_is [INFO] Checking Configuration
    1.8.1.3_remove_os_info_is [INFO] Performing audit
    1.8.1.3_remove_os_info_is [ OK ] (\v|\r|\m|\s) is not present in /etc/issue.net
    1.8.1.3_remove_os_info_is [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/1.8.1.3_remove_os_info_issue_net.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/1.8.1.4_motd_perms.sh
    1.8.1.4_motd_perms        [INFO] Working on 1.8.1.4_motd_perms
    1.8.1.4_motd_perms        [INFO] [DESCRIPTION] Checking root ownership and 644 permissions on banner files: /etc/motd|issue|issue.net .
    1.8.1.4_motd_perms        [INFO] Checking Configuration
    1.8.1.4_motd_perms        [INFO] Performing audit
    1.8.1.4_motd_perms        [ OK ] /etc/motd has correct ownership
    1.8.1.4_motd_perms        [ OK ] /etc/motd has correct permissions
    1.8.1.4_motd_perms        [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/1.8.1.4_motd_perms.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/1.8.1.5_etc_issue_perms.sh
    1.8.1.5_etc_issue_perms   [INFO] Working on 1.8.1.5_etc_issue_perms
    1.8.1.5_etc_issue_perms   [INFO] [DESCRIPTION] Checking root ownership and 644 permissions on banner files: /etc/motd|issue|issue.net .
    1.8.1.5_etc_issue_perms   [INFO] Checking Configuration
    1.8.1.5_etc_issue_perms   [INFO] Performing audit
    1.8.1.5_etc_issue_perms   [ OK ] /etc/issue has correct ownership
    1.8.1.5_etc_issue_perms   [ OK ] /etc/issue has correct permissions
    1.8.1.5_etc_issue_perms   [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/1.8.1.5_etc_issue_perms.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/1.8.1.6_etc_issue_net_perms.sh
    1.8.1.6_etc_issue_net_per [INFO] Working on 1.8.1.6_etc_issue_net_perms
    1.8.1.6_etc_issue_net_per [INFO] [DESCRIPTION] Checking root ownership and 644 permissions on banner files: /etc/motd|issue|issue.net .
    1.8.1.6_etc_issue_net_per [INFO] Checking Configuration
    1.8.1.6_etc_issue_net_per [INFO] Performing audit
    1.8.1.6_etc_issue_net_per [ OK ] /etc/issue.net has correct ownership
    1.8.1.6_etc_issue_net_per [ OK ] /etc/issue.net has correct permissions
    1.8.1.6_etc_issue_net_per [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/1.8.1.6_etc_issue_net_perms.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/1.8.2_graphical_warning_banners.sh
    1.8.2_graphical_warning_b [INFO] Working on 1.8.2_graphical_warning_banners
    1.8.2_graphical_warning_b [INFO] [DESCRIPTION] Set graphical warning banner.
    1.8.2_graphical_warning_b [INFO] Checking Configuration
    1.8.2_graphical_warning_b [INFO] Performing audit
    1.8.2_graphical_warning_b [INFO] Not implemented yet
    1.8.2_graphical_warning_b [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/1.8.2_graphical_warning_banners.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/1.9_install_updates.sh
    1.9_install_updates       [INFO] Working on 1.9_install_updates
    1.9_install_updates       [INFO] [DESCRIPTION] Ensure updates, patches, and additional security software are installed (Not Scored)
    1.9_install_updates       [INFO] Checking Configuration
    1.9_install_updates       [INFO] Performing audit
    1.9_install_updates       [INFO] Checking if apt needs an update
    1.9_install_updates       [INFO] Fetching upgrades ...
    1.9_install_updates       [ KO ] There is 49 updates available :
    Inst base-files [12.4+deb12u7] (12.4+deb12u9 Debian:12.9/stable [amd64])
    Inst bsdutils [1:2.38.1-5+deb12u1] (1:2.38.1-5+deb12u3 Debian:12.9/stable [amd64])
    Inst libc6 [2.36-9+deb12u8] (2.36-9+deb12u9 Debian:12.9/stable [amd64])
    Inst libblkid1 [2.38.1-5+deb12u1] (2.38.1-5+deb12u3 Debian:12.9/stable [amd64])
    Inst libuuid1 [2.38.1-5+deb12u1] (2.38.1-5+deb12u3 Debian:12.9/stable [amd64])
    Inst libfdisk1 [2.38.1-5+deb12u1] (2.38.1-5+deb12u3 Debian:12.9/stable [amd64])
    Inst libmount1 [2.38.1-5+deb12u1] (2.38.1-5+deb12u3 Debian:12.9/stable [amd64])
    Inst libsmartcols1 [2.38.1-5+deb12u1] (2.38.1-5+deb12u3 Debian:12.9/stable [amd64])
    Inst fdisk [2.38.1-5+deb12u1] (2.38.1-5+deb12u3 Debian:12.9/stable [amd64])
    Inst cloud-initramfs-growroot [0.18.debian13] (0.18.debian13+deb12u1 Debian:12.9/stable [all])
    Inst util-linux-extra [2.38.1-5+deb12u1] (2.38.1-5+deb12u3 Debian:12.9/stable [amd64])
    Inst util-linux [2.38.1-5+deb12u1] (2.38.1-5+deb12u3 Debian:12.9/stable [amd64])
    Inst libc-bin [2.36-9+deb12u8] (2.36-9+deb12u9 Debian:12.9/stable [amd64])
    Inst mount [2.38.1-5+deb12u1] (2.38.1-5+deb12u3 Debian:12.9/stable [amd64])
    Inst libnss-systemd [252.30-1~deb12u2] (252.33-1~deb12u1 Debian:12.9/stable [amd64]) []
    Inst systemd-sysv [252.30-1~deb12u2] (252.33-1~deb12u1 Debian:12.9/stable [amd64]) []
    Inst libpam-systemd [252.30-1~deb12u2] (252.33-1~deb12u1 Debian:12.9/stable [amd64]) []
    Inst libssl3 [3.0.14-1~deb12u1] (3.0.15-1~deb12u1 Debian:12.9/stable [amd64]) []
    Inst systemd [252.30-1~deb12u2] (252.33-1~deb12u1 Debian:12.9/stable [amd64]) []
    Inst libsystemd-shared [252.30-1~deb12u2] (252.33-1~deb12u1 Debian:12.9/stable [amd64]) []
    Inst libsystemd0 [252.30-1~deb12u2] (252.33-1~deb12u1 Debian:12.9/stable [amd64])
    Inst openssh-sftp-server [1:9.2p1-2+deb12u3] (1:9.2p1-2+deb12u4 Debian:12.9/stable [amd64]) []
    Inst openssh-server [1:9.2p1-2+deb12u3] (1:9.2p1-2+deb12u4 Debian:12.9/stable [amd64]) []
    Inst openssh-client [1:9.2p1-2+deb12u3] (1:9.2p1-2+deb12u4 Debian:12.9/stable [amd64])
    Inst ucf [3.0043+nmu1] (3.0043+nmu1+deb12u1 Debian:12.9/stable [all])
    Inst python3.11 [3.11.2-6+deb12u2] (3.11.2-6+deb12u5 Debian:12.9/stable [amd64]) []
    Inst libpython3.11-stdlib [3.11.2-6+deb12u2] (3.11.2-6+deb12u5 Debian:12.9/stable [amd64]) []
    Inst python3.11-minimal [3.11.2-6+deb12u2] (3.11.2-6+deb12u5 Debian:12.9/stable [amd64]) []
    Inst libpython3.11-minimal [3.11.2-6+deb12u2] (3.11.2-6+deb12u5 Debian:12.9/stable [amd64])
    Inst libsqlite3-0 [3.40.1-2] (3.40.1-2+deb12u1 Debian:12.9/stable [amd64])
    Inst libexpat1 [2.5.0-1] (2.5.0-1+deb12u1 Debian:12.9/stable, Debian-Security:12/stable-security [amd64])
    Inst udev [252.30-1~deb12u2] (252.33-1~deb12u1 Debian:12.9/stable [amd64]) []
    Inst libudev1 [252.30-1~deb12u2] (252.33-1~deb12u1 Debian:12.9/stable [amd64])
    Inst tzdata [2024a-0+deb12u1] (2024b-0+deb12u1 Debian:12.9/stable, Debian:12-updates/stable-updates [all])
    Inst iputils-ping [3:20221126-1] (3:20221126-1+deb12u1 Debian:12.9/stable [amd64])
    Inst libnghttp2-14 [1.52.0-1+deb12u1] (1.52.0-1+deb12u2 Debian:12.9/stable [amd64])
    Inst bind9-host [1:9.18.28-1~deb12u2] (1:9.18.33-1~deb12u2 Debian-Security:12/stable-security [amd64]) []
    Inst bind9-dnsutils [1:9.18.28-1~deb12u2] (1:9.18.33-1~deb12u2 Debian-Security:12/stable-security [amd64]) []
    Inst bind9-libs [1:9.18.28-1~deb12u2] (1:9.18.33-1~deb12u2 Debian-Security:12/stable-security [amd64])
    Inst libc-l10n [2.36-9+deb12u8] (2.36-9+deb12u9 Debian:12.9/stable [all])
    Inst locales [2.36-9+deb12u8] (2.36-9+deb12u9 Debian:12.9/stable [all])
    Inst bsdextrautils [2.38.1-5+deb12u1] (2.38.1-5+deb12u3 Debian:12.9/stable [amd64])
    Inst distro-info-data [0.58+deb12u2] (0.58+deb12u3 Debian:12.9/stable [all])
    Inst libcurl3-gnutls [7.88.1-10+deb12u7] (7.88.1-10+deb12u8 Debian:12.9/stable [amd64])
    Inst libglib2.0-0 [2.74.6-2+deb12u3] (2.74.6-2+deb12u5 Debian:12.9/stable [amd64])
    Inst openssl [3.0.14-1~deb12u1] (3.0.15-1~deb12u1 Debian:12.9/stable [amd64])
    Inst python3-pkg-resources [66.1.1-1] (66.1.1-1+deb12u1 Debian:12.9/stable [all])
    Inst python3-urllib3 [1.26.12-1] (1.26.12-1+deb12u1 Debian:12.9/stable [all])
    Inst qemu-utils [1:7.2+dfsg-7+deb12u7] (1:7.2+dfsg-7+deb12u12 Debian:12.9/stable [amd64])
    1.9_install_updates       [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/2.1.1_disable_xinetd.sh
    2.1.1_disable_xinetd      [INFO] Working on 2.1.1_disable_xinetd
    2.1.1_disable_xinetd      [INFO] [DESCRIPTION] Ensure xinetd is not enabled.
    2.1.1_disable_xinetd      [INFO] Checking Configuration
    2.1.1_disable_xinetd      [INFO] Performing audit
    2.1.1_disable_xinetd      [ OK ] xinetd is absent
    2.1.1_disable_xinetd      [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/2.1.1_disable_xinetd.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/2.1.2_disable_bsd_inetd.sh
    2.1.2_disable_bsd_inetd   [INFO] Working on 2.1.2_disable_bsd_inetd
    2.1.2_disable_bsd_inetd   [INFO] [DESCRIPTION] Ensure bsd-inetd is not enabled.
    2.1.2_disable_bsd_inetd   [INFO] Checking Configuration
    2.1.2_disable_bsd_inetd   [INFO] Performing audit
    2.1.2_disable_bsd_inetd   [ OK ] openbsd-inetd is absent
    2.1.2_disable_bsd_inetd   [ OK ] inetutils-inetd is absent
    2.1.2_disable_bsd_inetd   [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/2.1.2_disable_bsd_inetd.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/2.2.1.1_use_time_sync.sh
    2.2.1.1_use_time_sync     [INFO] Working on 2.2.1.1_use_time_sync
    2.2.1.1_use_time_sync     [INFO] [DESCRIPTION] Ensure time synchronization is in use
    2.2.1.1_use_time_sync     [INFO] Checking Configuration
    2.2.1.1_use_time_sync     [INFO] Performing audit
    2.2.1.1_use_time_sync     [ OK ] Time synchronization is available through chrony
    2.2.1.1_use_time_sync     [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/2.2.1.1_use_time_sync.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/2.2.1.2_configure_systemd-timesyncd.sh
    2.2.1.2_configure_systemd [INFO] Working on 2.2.1.2_configure_systemd-timesyncd
    2.2.1.2_configure_systemd [INFO] [DESCRIPTION] Configure systemd-timesyncd.
    2.2.1.2_configure_systemd [INFO] Checking Configuration
    2.2.1.2_configure_systemd [INFO] Performing audit
    Failed to get unit file state for systemd-timesyncd.service: No such file or directory
    2.2.1.2_configure_systemd [ KO ] Check failed with unexpected exit code: 1
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/2.2.1.3_configure_chrony.sh
    2.2.1.3_configure_chrony  [INFO] Working on 2.2.1.3_configure_chrony
    2.2.1.3_configure_chrony  [INFO] [DESCRIPTION] Configure Network Time Protocol (ntp). Check restrict parameters and ntp daemon runs ad unprivileged user.
    2.2.1.3_configure_chrony  [INFO] Checking Configuration
    2.2.1.3_configure_chrony  [INFO] Performing audit
    2.2.1.3_configure_chrony  [ OK ] ^(server|pool) found in /etc/chrony/chrony.conf
    2.2.1.3_configure_chrony  [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/2.2.1.3_configure_chrony.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/2.2.1.4_configure_ntp.sh
    2.2.1.4_configure_ntp     [INFO] Working on 2.2.1.4_configure_ntp
    2.2.1.4_configure_ntp     [INFO] [DESCRIPTION] Configure Network Time Protocol (ntp). Check restrict parameters and ntp daemon runs ad unprivileged user.
    2.2.1.4_configure_ntp     [INFO] Checking Configuration
    2.2.1.4_configure_ntp     [WARN] ntp is not installed, not handling configuration
    2.2.1.4_configure_ntp     [ KO ] Check failed with unexpected exit code: 2
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/2.2.2_disable_xwindow_system.sh
    2.2.2_disable_xwindow_sys [INFO] Working on 2.2.2_disable_xwindow_system
    2.2.2_disable_xwindow_sys [INFO] [DESCRIPTION] Ensure the X Window system is not installed.
    2.2.2_disable_xwindow_sys [INFO] Checking Configuration
    2.2.2_disable_xwindow_sys [INFO] Performing audit
    2.2.2_disable_xwindow_sys [ OK ] xserver-xorg-core is absent
    2.2.2_disable_xwindow_sys [ OK ] xserver-xorg-core-dbg is absent
    2.2.2_disable_xwindow_sys [ OK ] xserver-common is absent
    2.2.2_disable_xwindow_sys [ OK ] xserver-xephyr is absent
    2.2.2_disable_xwindow_sys [ OK ] xserver-xfbdev is absent
    2.2.2_disable_xwindow_sys [ OK ] tightvncserver is absent
    2.2.2_disable_xwindow_sys [ OK ] vnc4server is absent
    2.2.2_disable_xwindow_sys [ OK ] fglrx-driver is absent
    2.2.2_disable_xwindow_sys [ OK ] xvfb is absent
    2.2.2_disable_xwindow_sys [ OK ] xserver-xorg-video-nvidia-legacy-173xx is absent
    2.2.2_disable_xwindow_sys [ OK ] xserver-xorg-video-nvidia-legacy-96xx is absent
    2.2.2_disable_xwindow_sys [ OK ] xnest is absent
    2.2.2_disable_xwindow_sys [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/2.2.2_disable_xwindow_system.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/2.2.3_disable_avahi_server.sh
    2.2.3_disable_avahi_serve [INFO] Working on 2.2.3_disable_avahi_server
    2.2.3_disable_avahi_serve [INFO] [DESCRIPTION] Ensure Avahi server is not enabled.
    2.2.3_disable_avahi_serve [INFO] Checking Configuration
    2.2.3_disable_avahi_serve [INFO] Performing audit
    2.2.3_disable_avahi_serve [ OK ] avahi-daemon is absent
    2.2.3_disable_avahi_serve [ OK ] libavahi-common-data is absent
    2.2.3_disable_avahi_serve [ OK ] libavahi-common3 is absent
    2.2.3_disable_avahi_serve [ OK ] libavahi-core7 is absent
    2.2.3_disable_avahi_serve [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/2.2.3_disable_avahi_server.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/2.2.4_disable_print_server.sh
    2.2.4_disable_print_serve [INFO] Working on 2.2.4_disable_print_server
    2.2.4_disable_print_serve [INFO] [DESCRIPTION] Ensure print server (Common Unix Print System) is not enabled.
    2.2.4_disable_print_serve [INFO] Checking Configuration
    2.2.4_disable_print_serve [INFO] Performing audit
    2.2.4_disable_print_serve [ OK ] libcups2 is absent
    2.2.4_disable_print_serve [ OK ] libcupscgi1 is absent
    2.2.4_disable_print_serve [ OK ] libcupsimage2 is absent
    2.2.4_disable_print_serve [ OK ] libcupsmime1 is absent
    2.2.4_disable_print_serve [ OK ] libcupsppdc1 is absent
    2.2.4_disable_print_serve [ OK ] cups-common is absent
    2.2.4_disable_print_serve [ OK ] cups-client is absent
    2.2.4_disable_print_serve [ OK ] cups-ppdc is absent
    2.2.4_disable_print_serve [ OK ] libcupsfilters1 is absent
    2.2.4_disable_print_serve [ OK ] cups-filters is absent
    2.2.4_disable_print_serve [ OK ] cups is absent
    2.2.4_disable_print_serve [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/2.2.4_disable_print_server.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/2.2.5_disable_dhcp.sh
    2.2.5_disable_dhcp        [INFO] Working on 2.2.5_disable_dhcp
    2.2.5_disable_dhcp        [INFO] [DESCRIPTION] Ensure DHCP server is not enabled.
    2.2.5_disable_dhcp        [INFO] Checking Configuration
    2.2.5_disable_dhcp        [INFO] Performing audit
    2.2.5_disable_dhcp        [ OK ] udhcpd is absent
    2.2.5_disable_dhcp        [ OK ] isc-dhcp-server is absent
    2.2.5_disable_dhcp        [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/2.2.5_disable_dhcp.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/2.2.6_disable_ldap.sh
    2.2.6_disable_ldap        [INFO] Working on 2.2.6_disable_ldap
    2.2.6_disable_ldap        [INFO] [DESCRIPTION] Ensure LDAP is not enabled.
    2.2.6_disable_ldap        [INFO] Checking Configuration
    2.2.6_disable_ldap        [INFO] Performing audit
    2.2.6_disable_ldap        [ OK ] slapd is absent
    2.2.6_disable_ldap        [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/2.2.6_disable_ldap.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/2.2.7_disable_nfs_rpc.sh
    2.2.7_disable_nfs_rpc     [INFO] Working on 2.2.7_disable_nfs_rpc
    2.2.7_disable_nfs_rpc     [INFO] [DESCRIPTION] Ensure Network File System (nfs) and RPC are not enabled.
    2.2.7_disable_nfs_rpc     [INFO] Checking Configuration
    2.2.7_disable_nfs_rpc     [INFO] Performing audit
    2.2.7_disable_nfs_rpc     [ OK ] rpcbind is absent
    2.2.7_disable_nfs_rpc     [ OK ] nfs-kernel-server is absent
    2.2.7_disable_nfs_rpc     [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/2.2.7_disable_nfs_rpc.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/2.2.8_disable_dns_server.sh
    2.2.8_disable_dns_server  [INFO] Working on 2.2.8_disable_dns_server
    2.2.8_disable_dns_server  [INFO] [DESCRIPTION] Ensure Domain Name System (dns) server is not enabled.
    2.2.8_disable_dns_server  [INFO] Checking Configuration
    2.2.8_disable_dns_server  [INFO] Performing audit
    2.2.8_disable_dns_server  [ OK ] bind9 is absent
    2.2.8_disable_dns_server  [ OK ] unbound is absent
    2.2.8_disable_dns_server  [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/2.2.8_disable_dns_server.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/2.2.9_disable_ftp.sh
    2.2.9_disable_ftp         [INFO] Working on 2.2.9_disable_ftp
    2.2.9_disable_ftp         [INFO] [DESCRIPTION] Ensure File Transfer Protocol (ftp) is not enabled.
    2.2.9_disable_ftp         [INFO] Checking Configuration
    2.2.9_disable_ftp         [INFO] Performing audit
    2.2.9_disable_ftp         [ OK ] ftpd is absent
    2.2.9_disable_ftp         [ OK ] ftpd-ssl is absent
    2.2.9_disable_ftp         [ OK ] heimdal-servers is absent
    2.2.9_disable_ftp         [ OK ] inetutils-ftpd is absent
    2.2.9_disable_ftp         [ OK ] krb5-ftpd is absent
    2.2.9_disable_ftp         [ OK ] muddleftpd is absent
    2.2.9_disable_ftp         [ OK ] proftpd-basic is absent
    2.2.9_disable_ftp         [ OK ] pure-ftpd is absent
    2.2.9_disable_ftp         [ OK ] pure-ftpd-ldap is absent
    2.2.9_disable_ftp         [ OK ] pure-ftpd-mysql is absent
    2.2.9_disable_ftp         [ OK ] pure-ftpd-postgresql is absent
    2.2.9_disable_ftp         [ OK ] twoftpd-run is absent
    2.2.9_disable_ftp         [ OK ] vsftpd is absent
    2.2.9_disable_ftp         [ OK ] wzdftpd is absent
    2.2.9_disable_ftp         [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/2.2.9_disable_ftp.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/2.2.10_disable_http_server.sh
    2.2.10_disable_http_serve [INFO] Working on 2.2.10_disable_http_server
    2.2.10_disable_http_serve [INFO] [DESCRIPTION] Ensure HTTP server is not enabled.
    2.2.10_disable_http_serve [INFO] Checking Configuration
    2.2.10_disable_http_serve [INFO] Performing audit
    2.2.10_disable_http_serve [ OK ] nginx is absent
    2.2.10_disable_http_serve [ OK ] apache2 is absent
    2.2.10_disable_http_serve [ OK ] lighttpd is absent
    2.2.10_disable_http_serve [ OK ] micro-httpd is absent
    2.2.10_disable_http_serve [ OK ] mini-httpd is absent
    2.2.10_disable_http_serve [ OK ] yaws is absent
    2.2.10_disable_http_serve [ OK ] boa is absent
    2.2.10_disable_http_serve [ OK ] bozohttpd is absent
    2.2.10_disable_http_serve [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/2.2.10_disable_http_server.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/2.2.11_disable_imap_pop.sh
    2.2.11_disable_imap_pop   [INFO] Working on 2.2.11_disable_imap_pop
    2.2.11_disable_imap_pop   [INFO] [DESCRIPTION] Ensure IMAP and POP servers are not installed
    2.2.11_disable_imap_pop   [INFO] Checking Configuration
    2.2.11_disable_imap_pop   [INFO] Performing audit
    2.2.11_disable_imap_pop   [ OK ] citadel-server is absent
    2.2.11_disable_imap_pop   [ OK ] courier-imap is absent
    2.2.11_disable_imap_pop   [ OK ] cyrus-imapd-2.4 is absent
    2.2.11_disable_imap_pop   [ OK ] dovecot-imapd is absent
    2.2.11_disable_imap_pop   [ OK ] mailutils-imap4d is absent
    2.2.11_disable_imap_pop   [ OK ] courier-pop is absent
    2.2.11_disable_imap_pop   [ OK ] cyrus-pop3d-2.4 is absent
    2.2.11_disable_imap_pop   [ OK ] dovecot-pop3d is absent
    2.2.11_disable_imap_pop   [ OK ] heimdal-servers is absent
    2.2.11_disable_imap_pop   [ OK ] mailutils-pop3d is absent
    2.2.11_disable_imap_pop   [ OK ] popa3d is absent
    2.2.11_disable_imap_pop   [ OK ] solid-pop3d is absent
    2.2.11_disable_imap_pop   [ OK ] xmail is absent
    2.2.11_disable_imap_pop   [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/2.2.11_disable_imap_pop.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/2.2.12_disable_samba.sh
    2.2.12_disable_samba      [INFO] Working on 2.2.12_disable_samba
    2.2.12_disable_samba      [INFO] [DESCRIPTION] Ensure Samba is not enabled.
    2.2.12_disable_samba      [INFO] Checking Configuration
    2.2.12_disable_samba      [INFO] Performing audit
    2.2.12_disable_samba      [ OK ] samba is absent
    2.2.12_disable_samba      [ OK ] Service smbd is disabled
    2.2.12_disable_samba      [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/2.2.12_disable_samba.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/2.2.13_disable_http_proxy.sh
    2.2.13_disable_http_proxy [INFO] Working on 2.2.13_disable_http_proxy
    2.2.13_disable_http_proxy [INFO] [DESCRIPTION] Ensure HTTP-proxy is not enabled.
    2.2.13_disable_http_proxy [INFO] Checking Configuration
    2.2.13_disable_http_proxy [INFO] Performing audit
    2.2.13_disable_http_proxy [ OK ] squid3 is absent
    2.2.13_disable_http_proxy [ OK ] squid is absent
    2.2.13_disable_http_proxy [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/2.2.13_disable_http_proxy.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/2.2.14_disable_snmp_server.sh
    2.2.14_disable_snmp_serve [INFO] Working on 2.2.14_disable_snmp_server
    2.2.14_disable_snmp_serve [INFO] [DESCRIPTION] Enure SNMP server is not enabled.
    2.2.14_disable_snmp_serve [INFO] Checking Configuration
    2.2.14_disable_snmp_serve [INFO] Performing audit
    2.2.14_disable_snmp_serve [ OK ] snmpd is absent
    2.2.14_disable_snmp_serve [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/2.2.14_disable_snmp_server.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/2.2.15_mta_localhost.sh
    2.2.15_mta_localhost      [INFO] Working on 2.2.15_mta_localhost
    2.2.15_mta_localhost      [INFO] [DESCRIPTION] Configure Mail Transfert Agent for Local-Only Mode.
    2.2.15_mta_localhost      [INFO] Checking Configuration
    2.2.15_mta_localhost      [INFO] Performing audit
    2.2.15_mta_localhost      [INFO] Checking netport ports opened
    2.2.15_mta_localhost      [ OK ] Nothing listens on 25 port, probably unix socket configured
    2.2.15_mta_localhost      [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/2.2.15_mta_localhost.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/2.2.16_disable_rsync.sh
    2.2.16_disable_rsync      [INFO] Working on 2.2.16_disable_rsync
    2.2.16_disable_rsync      [INFO] [DESCRIPTION] Ensure rsync service is not enabled.
    2.2.16_disable_rsync      [INFO] Checking Configuration
    2.2.16_disable_rsync      [INFO] Performing audit
    2.2.16_disable_rsync      [ OK ] rsync is not installed
    2.2.16_disable_rsync      [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/2.2.16_disable_rsync.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/2.2.17_disable_nis.sh
    2.2.17_disable_nis        [INFO] Working on 2.2.17_disable_nis
    2.2.17_disable_nis        [INFO] [DESCRIPTION] Disable NIS Server.
    2.2.17_disable_nis        [INFO] Checking Configuration
    2.2.17_disable_nis        [INFO] Performing audit
    2.2.17_disable_nis        [ OK ] nis is absent
    2.2.17_disable_nis        [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/2.2.17_disable_nis.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/2.3.1_disable_nis.sh
    2.3.1_disable_nis         [INFO] Working on 2.3.1_disable_nis
    2.3.1_disable_nis         [INFO] [DESCRIPTION] Ensure that Network Information Service is not installed. Recommended alternative : LDAP.
    2.3.1_disable_nis         [INFO] Checking Configuration
    2.3.1_disable_nis         [INFO] Performing audit
    2.3.1_disable_nis         [ OK ] nis is absent
    2.3.1_disable_nis         [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/2.3.1_disable_nis.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/2.3.2_disable_rsh_client.sh
    2.3.2_disable_rsh_client  [INFO] Working on 2.3.2_disable_rsh_client
    2.3.2_disable_rsh_client  [INFO] [DESCRIPTION] Ensure rsh client is not installed, Recommended alternative : ssh.
    2.3.2_disable_rsh_client  [INFO] Checking Configuration
    2.3.2_disable_rsh_client  [INFO] Performing audit
    2.3.2_disable_rsh_client  [ OK ] rsh-client is absent
    2.3.2_disable_rsh_client  [ OK ] rsh-redone-client is absent
    2.3.2_disable_rsh_client  [ OK ] heimdal-clients is absent
    2.3.2_disable_rsh_client  [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/2.3.2_disable_rsh_client.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/2.3.3_disable_talk_client.sh
    2.3.3_disable_talk_client [INFO] Working on 2.3.3_disable_talk_client
    2.3.3_disable_talk_client [INFO] [DESCRIPTION] Ensure talk client is not installed.
    2.3.3_disable_talk_client [INFO] Checking Configuration
    2.3.3_disable_talk_client [INFO] Performing audit
    2.3.3_disable_talk_client [ OK ] talk is absent
    2.3.3_disable_talk_client [ OK ] inetutils-talk is absent
    2.3.3_disable_talk_client [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/2.3.3_disable_talk_client.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/2.3.4_disable_telnet_client.sh
    2.3.4_disable_telnet_clie [INFO] Working on 2.3.4_disable_telnet_client
    2.3.4_disable_telnet_clie [INFO] [DESCRIPTION] Ensure telnet client is not installed.
    2.3.4_disable_telnet_clie [INFO] Checking Configuration
    2.3.4_disable_telnet_clie [INFO] Performing audit
    2.3.4_disable_telnet_clie [ OK ] telnet is absent
    2.3.4_disable_telnet_clie [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/2.3.4_disable_telnet_client.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/2.3.5_disable_ldap_client.sh
    2.3.5_disable_ldap_client [INFO] Working on 2.3.5_disable_ldap_client
    2.3.5_disable_ldap_client [INFO] [DESCRIPTION] Ensure ldap client is not installed.
    2.3.5_disable_ldap_client [INFO] Checking Configuration
    2.3.5_disable_ldap_client [INFO] Performing audit
    2.3.5_disable_ldap_client [ OK ] ldap-utils is absent
    2.3.5_disable_ldap_client [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/2.3.5_disable_ldap_client.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/3.1.2_disable_wireless.sh
    3.1.2_disable_wireless    [INFO] Working on 3.1.2_disable_wireless
    3.1.2_disable_wireless    [INFO] [DESCRIPTION] Deactivate wireless interfaces.
    3.1.2_disable_wireless    [INFO] Checking Configuration
    3.1.2_disable_wireless    [INFO] Performing audit
    3.1.2_disable_wireless    [INFO] Not implemented yet
    3.1.2_disable_wireless    [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/3.1.2_disable_wireless.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/3.2.1_disable_send_packet_redirects.sh
    3.2.1_disable_send_packet [INFO] Working on 3.2.1_disable_send_packet_redirects
    3.2.1_disable_send_packet [INFO] [DESCRIPTION] Disable send packet redirects to prevent malicious ICMP corruption.
    3.2.1_disable_send_packet [INFO] Checking Configuration
    3.2.1_disable_send_packet [INFO] Performing audit
    3.2.1_disable_send_packet [ KO ] net.ipv4.conf.all.send_redirects was not set to 0
    3.2.1_disable_send_packet [ KO ] net.ipv4.conf.default.send_redirects was not set to 0
    3.2.1_disable_send_packet [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/3.2.2_disable_ip_forwarding.sh
    3.2.2_disable_ip_forwardi [INFO] Working on 3.2.2_disable_ip_forwarding
    3.2.2_disable_ip_forwardi [INFO] [DESCRIPTION] Disable IP forwarding.
    3.2.2_disable_ip_forwardi [INFO] Checking Configuration
    3.2.2_disable_ip_forwardi [INFO] Performing audit
    3.2.2_disable_ip_forwardi [ OK ] net.ipv4.ip_forward correctly set to 0
    3.2.2_disable_ip_forwardi [ OK ] net.ipv6.conf.all.forwarding correctly set to 0
    3.2.2_disable_ip_forwardi [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/3.2.2_disable_ip_forwarding.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/3.3.1_disable_source_routed_packets.sh
    3.3.1_disable_source_rout [INFO] Working on 3.3.1_disable_source_routed_packets
    3.3.1_disable_source_rout [INFO] [DESCRIPTION] Disable source routed packet acceptance.
    3.3.1_disable_source_rout [INFO] Checking Configuration
    3.3.1_disable_source_rout [INFO] Performing audit
    3.3.1_disable_source_rout [ KO ] net.ipv6.conf.all.disable_ipv6 was not set to 1
    3.3.1_disable_source_rout [ KO ] net.ipv6.conf.default.disable_ipv6 was not set to 1
    3.3.1_disable_source_rout [ KO ] net.ipv6.conf.lo.disable_ipv6 was not set to 1
    3.3.1_disable_source_rout [ KO ] net.ipv6.conf.lo.disable_ipv6 was not set to 1
    3.3.1_disable_source_rout [ KO ] net.ipv6.conf.all.disable_ipv6 was not set to 1
    3.3.1_disable_source_rout [ KO ] net.ipv6.conf.default.disable_ipv6 was not set to 1
    3.3.1_disable_source_rout [ KO ] net.ipv6.conf.lo.disable_ipv6 was not set to 1
    3.3.1_disable_source_rout [ KO ] net.ipv6.conf.lo.disable_ipv6 was not set to 1
    3.3.1_disable_source_rout [ KO ] net.ipv6.conf.all.disable_ipv6 was not set to 1
    3.3.1_disable_source_rout [ KO ] net.ipv6.conf.default.disable_ipv6 was not set to 1
    3.3.1_disable_source_rout [ KO ] net.ipv6.conf.lo.disable_ipv6 was not set to 1
    3.3.1_disable_source_rout [ KO ] net.ipv6.conf.lo.disable_ipv6 was not set to 1
    3.3.1_disable_source_rout [ KO ] net.ipv6.conf.all.disable_ipv6 was not set to 1
    3.3.1_disable_source_rout [ KO ] net.ipv6.conf.default.disable_ipv6 was not set to 1
    3.3.1_disable_source_rout [ KO ] net.ipv6.conf.lo.disable_ipv6 was not set to 1
    3.3.1_disable_source_rout [ KO ] net.ipv6.conf.lo.disable_ipv6 was not set to 1
    3.3.1_disable_source_rout [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/3.3.2_disable_icmp_redirect.sh
    3.3.2_disable_icmp_redire [INFO] Working on 3.3.2_disable_icmp_redirect
    3.3.2_disable_icmp_redire [INFO] [DESCRIPTION] Disable ICMP redirect acceptance to prevent routing table corruption.
    3.3.2_disable_icmp_redire [INFO] Checking Configuration
    3.3.2_disable_icmp_redire [INFO] Performing audit
    3.3.2_disable_icmp_redire [ KO ] net.ipv4.conf.all.accept_redirects was not set to 0
    3.3.2_disable_icmp_redire [ KO ] net.ipv4.conf.default.accept_redirects was not set to 0
    3.3.2_disable_icmp_redire [ KO ] net.ipv6.conf.all.accept_redirects was not set to 0
    3.3.2_disable_icmp_redire [ KO ] net.ipv6.conf.default.accept_redirects was not set to 0
    3.3.2_disable_icmp_redire [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/3.3.3_disable_secure_icmp_redirect.sh
    3.3.3_disable_secure_icmp [INFO] Working on 3.3.3_disable_secure_icmp_redirect
    3.3.3_disable_secure_icmp [INFO] [DESCRIPTION] Disable secure ICMP redirect acceptance to prevent routing tables corruptions.
    3.3.3_disable_secure_icmp [INFO] Checking Configuration
    3.3.3_disable_secure_icmp [INFO] Performing audit
    3.3.3_disable_secure_icmp [ KO ] net.ipv4.conf.all.secure_redirects was not set to 0
    3.3.3_disable_secure_icmp [ KO ] net.ipv4.conf.default.secure_redirects was not set to 0
    3.3.3_disable_secure_icmp [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/3.3.4_log_martian_packets.sh
    3.3.4_log_martian_packets [INFO] Working on 3.3.4_log_martian_packets
    3.3.4_log_martian_packets [INFO] [DESCRIPTION] Log suspicious packets, like spoofed packets.
    3.3.4_log_martian_packets [INFO] Checking Configuration
    3.3.4_log_martian_packets [INFO] Performing audit
    3.3.4_log_martian_packets [ KO ] net.ipv4.conf.all.log_martians was not set to 1
    3.3.4_log_martian_packets [ KO ] net.ipv4.conf.default.log_martians was not set to 1
    3.3.4_log_martian_packets [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/3.3.5_ignore_broadcast_requests.sh
    3.3.5_ignore_broadcast_re [INFO] Working on 3.3.5_ignore_broadcast_requests
    3.3.5_ignore_broadcast_re [INFO] [DESCRIPTION] Ignore broadcast requests to prevent attacks such as Smurf attack.
    3.3.5_ignore_broadcast_re [INFO] Checking Configuration
    3.3.5_ignore_broadcast_re [INFO] Performing audit
    3.3.5_ignore_broadcast_re [ OK ] net.ipv4.icmp_echo_ignore_broadcasts correctly set to 1
    3.3.5_ignore_broadcast_re [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/3.3.5_ignore_broadcast_requests.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/3.3.6_enable_bad_error_message_protection.sh
    3.3.6_enable_bad_error_me [INFO] Working on 3.3.6_enable_bad_error_message_protection
    3.3.6_enable_bad_error_me [INFO] [DESCRIPTION] Enable bad error message protection to prevent logfiles fillup.
    3.3.6_enable_bad_error_me [INFO] Checking Configuration
    3.3.6_enable_bad_error_me [INFO] Performing audit
    3.3.6_enable_bad_error_me [ OK ] net.ipv4.icmp_ignore_bogus_error_responses correctly set to 1
    3.3.6_enable_bad_error_me [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/3.3.6_enable_bad_error_message_protection.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/3.3.7_enable_source_route_validation.sh
    3.3.7_enable_source_route [INFO] Working on 3.3.7_enable_source_route_validation
    3.3.7_enable_source_route [INFO] [DESCRIPTION] Enable RFC-recommended source route validation.
    3.3.7_enable_source_route [INFO] Checking Configuration
    3.3.7_enable_source_route [INFO] Performing audit
    3.3.7_enable_source_route [ KO ] net.ipv4.conf.all.rp_filter was not set to 1
    3.3.7_enable_source_route [ KO ] net.ipv4.conf.default.rp_filter was not set to 1
    3.3.7_enable_source_route [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/3.3.8_enable_tcp_syn_cookies.sh
    3.3.8_enable_tcp_syn_cook [INFO] Working on 3.3.8_enable_tcp_syn_cookies
    3.3.8_enable_tcp_syn_cook [INFO] [DESCRIPTION] Enable TCP-SYN cookie to prevent TCP-SYN flood attack.
    3.3.8_enable_tcp_syn_cook [INFO] Checking Configuration
    3.3.8_enable_tcp_syn_cook [INFO] Performing audit
    3.3.8_enable_tcp_syn_cook [ OK ] net.ipv4.tcp_syncookies correctly set to 1
    3.3.8_enable_tcp_syn_cook [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/3.3.8_enable_tcp_syn_cookies.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/3.3.9_disable_ipv6_router_advertisement.sh
    3.3.9_disable_ipv6_router [INFO] Working on 3.3.9_disable_ipv6_router_advertisement
    3.3.9_disable_ipv6_router [INFO] [DESCRIPTION] Disable IPv6 router advertisements.
    3.3.9_disable_ipv6_router [INFO] Checking Configuration
    3.3.9_disable_ipv6_router [INFO] Performing audit
    3.3.9_disable_ipv6_router [ KO ] net.ipv6.conf.all.disable_ipv6 was not set to 1
    3.3.9_disable_ipv6_router [ KO ] net.ipv6.conf.default.disable_ipv6 was not set to 1
    3.3.9_disable_ipv6_router [ KO ] net.ipv6.conf.lo.disable_ipv6 was not set to 1
    3.3.9_disable_ipv6_router [ KO ] net.ipv6.conf.all.disable_ipv6 was not set to 1
    3.3.9_disable_ipv6_router [ KO ] net.ipv6.conf.default.disable_ipv6 was not set to 1
    3.3.9_disable_ipv6_router [ KO ] net.ipv6.conf.lo.disable_ipv6 was not set to 1
    3.3.9_disable_ipv6_router [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/3.4.1_disable_dccp.sh
    3.4.1_disable_dccp        [INFO] Working on 3.4.1_disable_dccp
    3.4.1_disable_dccp        [INFO] [DESCRIPTION] Disable Datagram Congestion Control Protocol (DCCP).
    3.4.1_disable_dccp        [INFO] Checking Configuration
    3.4.1_disable_dccp        [INFO] Performing audit
    3.4.1_disable_dccp        [ KO ] dccp is enabled!
    3.4.1_disable_dccp        [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/3.4.2_disable_sctp.sh
    3.4.2_disable_sctp        [INFO] Working on 3.4.2_disable_sctp
    3.4.2_disable_sctp        [INFO] [DESCRIPTION] Disable Stream Control Transmission Protocol (SCTP).
    3.4.2_disable_sctp        [INFO] Checking Configuration
    3.4.2_disable_sctp        [INFO] Performing audit
    3.4.2_disable_sctp        [ KO ] sctp is enabled!
    3.4.2_disable_sctp        [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/3.4.3_disable_rds.sh
    3.4.3_disable_rds         [INFO] Working on 3.4.3_disable_rds
    3.4.3_disable_rds         [INFO] [DESCRIPTION] Disable Reliable Datagram Sockets (RDS).
    3.4.3_disable_rds         [INFO] Checking Configuration
    3.4.3_disable_rds         [INFO] Performing audit
    3.4.3_disable_rds         [ KO ] rds is enabled!
    3.4.3_disable_rds         [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/3.4.4_disable_tipc.sh
    3.4.4_disable_tipc        [INFO] Working on 3.4.4_disable_tipc
    3.4.4_disable_tipc        [INFO] [DESCRIPTION] Disable Transperent Inter-Process Communication (TIPC).
    3.4.4_disable_tipc        [INFO] Checking Configuration
    3.4.4_disable_tipc        [INFO] Performing audit
    3.4.4_disable_tipc        [ KO ] tipc is enabled!
    3.4.4_disable_tipc        [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/3.5.1.1_enable_firewall.sh
    3.5.1.1_enable_firewall   [INFO] Working on 3.5.1.1_enable_firewall
    3.5.1.1_enable_firewall   [INFO] [DESCRIPTION] Ensure firewall is active (either nftables or iptables is installed, does not check for its configuration).
    3.5.1.1_enable_firewall   [INFO] Checking Configuration
    3.5.1.1_enable_firewall   [INFO] Performing audit
    3.5.1.1_enable_firewall   [ OK ] iptables provides firewalling feature
    3.5.1.1_enable_firewall   [ OK ] nftables provides firewalling feature
    3.5.1.1_enable_firewall   [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/3.5.1.1_enable_firewall.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/3.5.4.1.1_net_fw_default_policy_drop.sh
    3.5.4.1.1_net_fw_default_ [INFO] Working on 3.5.4.1.1_net_fw_default_policy_drop
    3.5.4.1.1_net_fw_default_ [INFO] [DESCRIPTION] Check iptables firewall default policy for DROP on INPUT and FORWARD.
    3.5.4.1.1_net_fw_default_ [INFO] Checking Configuration
    3.5.4.1.1_net_fw_default_ [INFO] Performing audit
    3.5.4.1.1_net_fw_default_ [ KO ] Policy set to ACCEPT for chain INPUT, should be DROP.
    3.5.4.1.1_net_fw_default_ [ KO ] Policy set to ACCEPT for chain FORWARD, should be DROP.
    3.5.4.1.1_net_fw_default_ [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/4.1.1.1_install_auditd.sh
    4.1.1.1_install_auditd    [INFO] Working on 4.1.1.1_install_auditd
    4.1.1.1_install_auditd    [INFO] [DESCRIPTION] Install auditd.
    4.1.1.1_install_auditd    [INFO] Checking Configuration
    4.1.1.1_install_auditd    [INFO] Performing audit
    4.1.1.1_install_auditd    [ KO ] auditd is not installed!
    4.1.1.1_install_auditd    [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/4.1.1.2_enable_auditd.sh
    4.1.1.2_enable_auditd     [INFO] Working on 4.1.1.2_enable_auditd
    4.1.1.2_enable_auditd     [INFO] [DESCRIPTION] Ensure auditd service is installed and running.
    4.1.1.2_enable_auditd     [INFO] Checking Configuration
    4.1.1.2_enable_auditd     [INFO] Performing audit
    4.1.1.2_enable_auditd     [ KO ] auditd is not installed!
    4.1.1.2_enable_auditd     [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/4.1.1.3_audit_bootloader.sh
    4.1.1.3_audit_bootloader  [INFO] Working on 4.1.1.3_audit_bootloader
    4.1.1.3_audit_bootloader  [INFO] [DESCRIPTION] Enable auditing for processes that start prior to auditd.
    4.1.1.3_audit_bootloader  [INFO] Checking Configuration
    4.1.1.3_audit_bootloader  [INFO] Performing audit
    4.1.1.3_audit_bootloader  [ OK ] /etc/default/grub exists, checking configuration
    4.1.1.3_audit_bootloader  [ KO ] ^GRUB_CMDLINE_LINUX=.*audit=1 is not present in /etc/default/grub
    4.1.1.3_audit_bootloader  [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/4.1.1.4_audit_backlog_limit.sh
    4.1.1.4_audit_backlog_lim [INFO] Working on 4.1.1.4_audit_backlog_limit
    4.1.1.4_audit_backlog_lim [INFO] [DESCRIPTION] Configure audit_backlog_limit to be sufficient.
    4.1.1.4_audit_backlog_lim [INFO] Checking Configuration
    4.1.1.4_audit_backlog_lim [INFO] Performing audit
    4.1.1.4_audit_backlog_lim [ OK ] /etc/default/grub exists, checking configuration
    4.1.1.4_audit_backlog_lim [ KO ] ^GRUB_CMDLINE_LINUX=.*audit_backlog_limit=8192 is not present in /etc/default/grub
    4.1.1.4_audit_backlog_lim [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/4.1.2.1_audit_log_storage.sh
    4.1.2.1_audit_log_storage [INFO] Working on 4.1.2.1_audit_log_storage
    4.1.2.1_audit_log_storage [INFO] [DESCRIPTION] Configure audit log storage size.
    4.1.2.1_audit_log_storage [INFO] Checking Configuration
    4.1.2.1_audit_log_storage [INFO] Performing audit
    4.1.2.1_audit_log_storage [ KO ] /etc/audit/auditd.conf does not exist
    4.1.2.1_audit_log_storage [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/4.1.2.2_halt_when_audit_log_full.sh
    4.1.2.2_halt_when_audit_l [INFO] Working on 4.1.2.2_halt_when_audit_log_full
    4.1.2.2_halt_when_audit_l [INFO] [DESCRIPTION] Disable system on audit log full.
    4.1.2.2_halt_when_audit_l [INFO] Checking Configuration
    4.1.2.2_halt_when_audit_l [INFO] Performing audit
    4.1.2.2_halt_when_audit_l [ KO ] /etc/audit/auditd.conf does not exist
    4.1.2.2_halt_when_audit_l [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/4.1.2.3_keep_all_audit_logs.sh
    4.1.2.3_keep_all_audit_lo [INFO] Working on 4.1.2.3_keep_all_audit_logs
    4.1.2.3_keep_all_audit_lo [INFO] [DESCRIPTION] Keep all auditing information.
    4.1.2.3_keep_all_audit_lo [INFO] Checking Configuration
    4.1.2.3_keep_all_audit_lo [INFO] Performing audit
    4.1.2.3_keep_all_audit_lo [ KO ] /etc/audit/auditd.conf does not exist
    4.1.2.3_keep_all_audit_lo [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/4.1.3_record_date_time_edit.sh
    4.1.3_record_date_time_ed [INFO] Working on 4.1.3_record_date_time_edit
    4.1.3_record_date_time_ed [INFO] [DESCRIPTION] Record events that modify date and time information.
    4.1.3_record_date_time_ed [INFO] Checking Configuration
    4.1.3_record_date_time_ed [INFO] Performing audit
    4.1.3_record_date_time_ed [ KO ] -a always,exit -F arch=b64 -S adjtimex -S settimeofday -k time-change is not present in /etc/audit/audit.rules /etc/audit/rules.d/audit.rules
    4.1.3_record_date_time_ed [ KO ] -a always,exit -F arch=b32 -S adjtimex -S settimeofday -S stime -k time-change is not present in /etc/audit/audit.rules /etc/audit/rules.d/audit.rules
    4.1.3_record_date_time_ed [ KO ] -a always,exit -F arch=b64 -S clock_settime -k time-change is not present in /etc/audit/audit.rules /etc/audit/rules.d/audit.rules
    4.1.3_record_date_time_ed [ KO ] -a always,exit -F arch=b32 -S clock_settime -k time-change is not present in /etc/audit/audit.rules /etc/audit/rules.d/audit.rules
    4.1.3_record_date_time_ed [ KO ] -w /etc/localtime -p wa -k time-change is not present in /etc/audit/audit.rules /etc/audit/rules.d/audit.rules
    4.1.3_record_date_time_ed [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/4.1.4_record_user_group_edit.sh
    4.1.4_record_user_group_e [INFO] Working on 4.1.4_record_user_group_edit
    4.1.4_record_user_group_e [INFO] [DESCRIPTION] Record events that modify user/group information.
    4.1.4_record_user_group_e [INFO] Checking Configuration
    4.1.4_record_user_group_e [INFO] Performing audit
    4.1.4_record_user_group_e [ KO ] -w /etc/group -p wa -k identity is not present in /etc/audit/audit.rules /etc/audit/rules.d/audit.rules
    4.1.4_record_user_group_e [ KO ] -w /etc/passwd -p wa -k identity is not present in /etc/audit/audit.rules /etc/audit/rules.d/audit.rules
    4.1.4_record_user_group_e [ KO ] -w /etc/gshadow -p wa -k identity is not present in /etc/audit/audit.rules /etc/audit/rules.d/audit.rules
    4.1.4_record_user_group_e [ KO ] -w /etc/shadow -p wa -k identity is not present in /etc/audit/audit.rules /etc/audit/rules.d/audit.rules
    4.1.4_record_user_group_e [ KO ] -w /etc/security/opasswd -p wa -k identity is not present in /etc/audit/audit.rules /etc/audit/rules.d/audit.rules
    4.1.4_record_user_group_e [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/4.1.5_record_network_edit.sh
    4.1.5_record_network_edit [INFO] Working on 4.1.5_record_network_edit
    4.1.5_record_network_edit [INFO] [DESCRIPTION] Record events that modify the system's network environment.
    4.1.5_record_network_edit [INFO] Checking Configuration
    4.1.5_record_network_edit [INFO] Performing audit
    4.1.5_record_network_edit [ KO ] -a exit,always -F arch=b64 -S sethostname -S setdomainname -k system-locale is not present in /etc/audit/audit.rules /etc/audit/rules.d/audit.rules
    4.1.5_record_network_edit [ KO ] -a exit,always -F arch=b32 -S sethostname -S setdomainname -k system-locale is not present in /etc/audit/audit.rules /etc/audit/rules.d/audit.rules
    4.1.5_record_network_edit [ KO ] -w /etc/issue -p wa -k system-locale is not present in /etc/audit/audit.rules /etc/audit/rules.d/audit.rules
    4.1.5_record_network_edit [ KO ] -w /etc/issue.net -p wa -k system-locale is not present in /etc/audit/audit.rules /etc/audit/rules.d/audit.rules
    4.1.5_record_network_edit [ KO ] -w /etc/hosts -p wa -k system-locale is not present in /etc/audit/audit.rules /etc/audit/rules.d/audit.rules
    4.1.5_record_network_edit [ KO ] -w /etc/network -p wa -k system-locale is not present in /etc/audit/audit.rules /etc/audit/rules.d/audit.rules
    4.1.5_record_network_edit [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/4.1.6_record_mac_edit.sh
    4.1.6_record_mac_edit     [INFO] Working on 4.1.6_record_mac_edit
    4.1.6_record_mac_edit     [INFO] [DESCRIPTION] Record events that modify the system's mandatory access controls (MAC).
    4.1.6_record_mac_edit     [INFO] Checking Configuration
    4.1.6_record_mac_edit     [INFO] Performing audit
    4.1.6_record_mac_edit     [ KO ] -w /etc/selinux/ -p wa -k MAC-policy is not present in /etc/audit/audit.rules /etc/audit/rules.d/audit.rules
    4.1.6_record_mac_edit     [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/4.1.7_record_login_logout.sh
    4.1.7_record_login_logout [INFO] Working on 4.1.7_record_login_logout
    4.1.7_record_login_logout [INFO] [DESCRIPTION] Collect login and logout events.
    4.1.7_record_login_logout [INFO] Checking Configuration
    4.1.7_record_login_logout [INFO] Performing audit
    4.1.7_record_login_logout [ KO ] -w /var/log/faillog -p wa -k logins is not present in /etc/audit/audit.rules /etc/audit/rules.d/audit.rules
    4.1.7_record_login_logout [ KO ] -w /var/log/lastlog -p wa -k logins is not present in /etc/audit/audit.rules /etc/audit/rules.d/audit.rules
    4.1.7_record_login_logout [ KO ] -w /var/log/tallylog -p wa -k logins is not present in /etc/audit/audit.rules /etc/audit/rules.d/audit.rules
    4.1.7_record_login_logout [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/4.1.8_record_session_init.sh
    4.1.8_record_session_init [INFO] Working on 4.1.8_record_session_init
    4.1.8_record_session_init [INFO] [DESCRIPTION] Collec sessions initiation information.
    4.1.8_record_session_init [INFO] Checking Configuration
    4.1.8_record_session_init [INFO] Performing audit
    4.1.8_record_session_init [ KO ] -w /var/run/utmp -p wa -k session is not present in /etc/audit/audit.rules /etc/audit/rules.d/audit.rules
    4.1.8_record_session_init [ KO ] -w /var/log/wtmp -p wa -k session is not present in /etc/audit/audit.rules /etc/audit/rules.d/audit.rules
    4.1.8_record_session_init [ KO ] -w /var/log/btmp -p wa -k session is not present in /etc/audit/audit.rules /etc/audit/rules.d/audit.rules
    4.1.8_record_session_init [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/4.1.9_record_dac_edit.sh
    4.1.9_record_dac_edit     [INFO] Working on 4.1.9_record_dac_edit
    4.1.9_record_dac_edit     [INFO] [DESCRIPTION] Collect discretionary access control (DAC) permission modification events.
    4.1.9_record_dac_edit     [INFO] Checking Configuration
    4.1.9_record_dac_edit     [INFO] Performing audit
    4.1.9_record_dac_edit     [ KO ] -a always,exit -F arch=b64 -S chmod -S fchmod -S fchmodat -F auid>=1000 -F auid!=4294967295 -k perm_mod is not present in /etc/audit/audit.rules /etc/audit/rules.d/audit.rules
    4.1.9_record_dac_edit     [ KO ] -a always,exit -F arch=b32 -S chmod -S fchmod -S fchmodat -F auid>=1000 -F auid!=4294967295 -k perm_mod is not present in /etc/audit/audit.rules /etc/audit/rules.d/audit.rules
    4.1.9_record_dac_edit     [ KO ] -a always,exit -F arch=b64 -S chown -S fchown -S fchownat -S lchown -F auid>=1000 -F auid!=4294967295 -k perm_mod is not present in /etc/audit/audit.rules /etc/audit/rules.d/audit.rules
    4.1.9_record_dac_edit     [ KO ] -a always,exit -F arch=b32 -S chown -S fchown -S fchownat -S lchown -F auid>=1000 -F auid!=4294967295 -k perm_mod is not present in /etc/audit/audit.rules /etc/audit/rules.d/audit.rules
    4.1.9_record_dac_edit     [ KO ] -a always,exit -F arch=b64 -S setxattr -S lsetxattr -S fsetxattr -S removexattr -S lremovexattr -S fremovexattr -F auid>=1000 -F auid!=4294967295 -k perm_mod is not present in /etc/audit/audit.rules /etc/audit/rules.d/audit.rules
    4.1.9_record_dac_edit     [ KO ] -a always,exit -F arch=b32 -S setxattr -S lsetxattr -S fsetxattr -S removexattr -S lremovexattr -S fremovexattr -F auid>=1000 -F auid!=4294967295 -k perm_mod is not present in /etc/audit/audit.rules /etc/audit/rules.d/audit.rules
    4.1.9_record_dac_edit     [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/4.1.10_record_failed_access_file.sh
    4.1.10_record_failed_acce [INFO] Working on 4.1.10_record_failed_access_file
    4.1.10_record_failed_acce [INFO] [DESCRIPTION] Collect unsuccessful unauthorized access attemps to files.
    4.1.10_record_failed_acce [INFO] Checking Configuration
    4.1.10_record_failed_acce [INFO] Performing audit
    4.1.10_record_failed_acce [ KO ] -a always,exit -F arch=b64 -S creat -S open -S openat -S truncate -S ftruncate -F exit=-EACCES -F auid>=1000 -F auid!=4294967295 -k access is not present in /etc/audit/audit.rules /etc/audit/rules.d/audit.rules
    4.1.10_record_failed_acce [ KO ] -a always,exit -F arch=b32 -S creat -S open -S openat -S truncate -S ftruncate -F exit=-EACCES -F auid>=1000 -F auid!=4294967295 -k access is not present in /etc/audit/audit.rules /etc/audit/rules.d/audit.rules
    4.1.10_record_failed_acce [ KO ] -a always,exit -F arch=b64 -S creat -S open -S openat -S truncate -S ftruncate -F exit=-EPERM -F auid>=1000 -F auid!=4294967295 -k access is not present in /etc/audit/audit.rules /etc/audit/rules.d/audit.rules
    4.1.10_record_failed_acce [ KO ] -a always,exit -F arch=b32 -S creat -S open -S openat -S truncate -S ftruncate -F exit=-EPERM -F auid>=1000 -F auid!=4294967295 -k access is not present in /etc/audit/audit.rules /etc/audit/rules.d/audit.rules
    4.1.10_record_failed_acce [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/4.1.11_record_privileged_commands.sh
    4.1.11_record_privileged_ [INFO] Working on 4.1.11_record_privileged_commands
    4.1.11_record_privileged_ [INFO] [DESCRIPTION] Collect use of privileged commands.
    4.1.11_record_privileged_ [INFO] Checking Configuration
    4.1.11_record_privileged_ [INFO] Performing audit
    4.1.11_record_privileged_ [ KO ] -a always,exit -F path=/usr/sbin/unix_chkpwd -F perm=x -F auid>=1000 -F auid!=4294967295 -k privileged is not present in /etc/audit/audit.rules /etc/audit/rules.d/audit.rules
    4.1.11_record_privileged_ [ KO ] -a always,exit -F path=/usr/bin/chage -F perm=x -F auid>=1000 -F auid!=4294967295 -k privileged is not present in /etc/audit/audit.rules /etc/audit/rules.d/audit.rules
    4.1.11_record_privileged_ [ KO ] -a always,exit -F path=/usr/bin/newgrp -F perm=x -F auid>=1000 -F auid!=4294967295 -k privileged is not present in /etc/audit/audit.rules /etc/audit/rules.d/audit.rules
    4.1.11_record_privileged_ [ KO ] -a always,exit -F path=/usr/bin/expiry -F perm=x -F auid>=1000 -F auid!=4294967295 -k privileged is not present in /etc/audit/audit.rules /etc/audit/rules.d/audit.rules
    4.1.11_record_privileged_ [ KO ] -a always,exit -F path=/usr/bin/ssh-agent -F perm=x -F auid>=1000 -F auid!=4294967295 -k privileged is not present in /etc/audit/audit.rules /etc/audit/rules.d/audit.rules
    4.1.11_record_privileged_ [ KO ] -a always,exit -F path=/usr/bin/chfn -F perm=x -F auid>=1000 -F auid!=4294967295 -k privileged is not present in /etc/audit/audit.rules /etc/audit/rules.d/audit.rules
    4.1.11_record_privileged_ [ KO ] -a always,exit -F path=/usr/bin/gpasswd -F perm=x -F auid>=1000 -F auid!=4294967295 -k privileged is not present in /etc/audit/audit.rules /etc/audit/rules.d/audit.rules
    4.1.11_record_privileged_ [ KO ] -a always,exit -F path=/usr/bin/chsh -F perm=x -F auid>=1000 -F auid!=4294967295 -k privileged is not present in /etc/audit/audit.rules /etc/audit/rules.d/audit.rules
    4.1.11_record_privileged_ [ KO ] -a always,exit -F path=/usr/bin/passwd -F perm=x -F auid>=1000 -F auid!=4294967295 -k privileged is not present in /etc/audit/audit.rules /etc/audit/rules.d/audit.rules
    4.1.11_record_privileged_ [ KO ] -a always,exit -F path=/usr/bin/dotlockfile -F perm=x -F auid>=1000 -F auid!=4294967295 -k privileged is not present in /etc/audit/audit.rules /etc/audit/rules.d/audit.rules
    4.1.11_record_privileged_ [ KO ] -a always,exit -F path=/usr/bin/sudo -F perm=x -F auid>=1000 -F auid!=4294967295 -k privileged is not present in /etc/audit/audit.rules /etc/audit/rules.d/audit.rules
    4.1.11_record_privileged_ [ KO ] -a always,exit -F path=/usr/bin/mount -F perm=x -F auid>=1000 -F auid!=4294967295 -k privileged is not present in /etc/audit/audit.rules /etc/audit/rules.d/audit.rules
    4.1.11_record_privileged_ [ KO ] -a always,exit -F path=/usr/bin/su -F perm=x -F auid>=1000 -F auid!=4294967295 -k privileged is not present in /etc/audit/audit.rules /etc/audit/rules.d/audit.rules
    4.1.11_record_privileged_ [ KO ] -a always,exit -F path=/usr/bin/crontab -F perm=x -F auid>=1000 -F auid!=4294967295 -k privileged is not present in /etc/audit/audit.rules /etc/audit/rules.d/audit.rules
    4.1.11_record_privileged_ [ KO ] -a always,exit -F path=/usr/bin/umount -F perm=x -F auid>=1000 -F auid!=4294967295 -k privileged is not present in /etc/audit/audit.rules /etc/audit/rules.d/audit.rules
    4.1.11_record_privileged_ [ KO ] -a always,exit -F path=/usr/lib/openssh/ssh-keysign -F perm=x -F auid>=1000 -F auid!=4294967295 -k privileged is not present in /etc/audit/audit.rules /etc/audit/rules.d/audit.rules
    4.1.11_record_privileged_ [ KO ] -a always,exit -F path=/usr/lib/dbus-1.0/dbus-daemon-launch-helper -F perm=x -F auid>=1000 -F auid!=4294967295 -k privileged is not present in /etc/audit/audit.rules /etc/audit/rules.d/audit.rules
    4.1.11_record_privileged_ [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/4.1.12_record_successful_mount.sh
    4.1.12_record_successful_ [INFO] Working on 4.1.12_record_successful_mount
    4.1.12_record_successful_ [INFO] [DESCRIPTION] Collect sucessfull file system mounts.
    4.1.12_record_successful_ [INFO] Checking Configuration
    4.1.12_record_successful_ [INFO] Performing audit
    4.1.12_record_successful_ [ KO ] -a always,exit -F arch=b64 -S mount -F auid>=1000 -F auid!=4294967295 -k mounts is not present in /etc/audit/audit.rules /etc/audit/rules.d/audit.rules
    4.1.12_record_successful_ [ KO ] -a always,exit -F arch=b32 -S mount -F auid>=1000 -F auid!=4294967295 -k mounts is not present in /etc/audit/audit.rules /etc/audit/rules.d/audit.rules
    4.1.12_record_successful_ [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/4.1.13_record_file_deletions.sh
    4.1.13_record_file_deleti [INFO] Working on 4.1.13_record_file_deletions
    4.1.13_record_file_deleti [INFO] [DESCRIPTION] Collects file deletion events by users.
    4.1.13_record_file_deleti [INFO] Checking Configuration
    4.1.13_record_file_deleti [INFO] Performing audit
    4.1.13_record_file_deleti [ KO ] -a always,exit -F arch=b64 -S unlink -S unlinkat -S rename -S renameat -F auid>=1000 -F auid!=4294967295 -k delete is not present in /etc/audit/audit.rules /etc/audit/rules.d/audit.rules
    4.1.13_record_file_deleti [ KO ] -a always,exit -F arch=b32 -S unlink -S unlinkat -S rename -S renameat -F auid>=1000 -F auid!=4294967295 -k delete is not present in /etc/audit/audit.rules /etc/audit/rules.d/audit.rules
    4.1.13_record_file_deleti [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/4.1.14_record_sudoers_edit.sh
    4.1.14_record_sudoers_edi [INFO] Working on 4.1.14_record_sudoers_edit
    4.1.14_record_sudoers_edi [INFO] [DESCRIPTION] Collect changes to system administration scopre.
    4.1.14_record_sudoers_edi [INFO] Checking Configuration
    4.1.14_record_sudoers_edi [INFO] Performing audit
    4.1.14_record_sudoers_edi [ KO ] -w /etc/sudoers -p wa -k sudoers is not present in /etc/audit/audit.rules /etc/audit/rules.d/audit.rules
    4.1.14_record_sudoers_edi [ KO ] -w /etc/sudoers.d/ -p wa -k sudoers is not present in /etc/audit/audit.rules /etc/audit/rules.d/audit.rules
    4.1.14_record_sudoers_edi [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/4.1.15_record_sudo_usage.sh
    4.1.15_record_sudo_usage  [INFO] Working on 4.1.15_record_sudo_usage
    4.1.15_record_sudo_usage  [INFO] [DESCRIPTION] Collect system administration actions (sudolog).
    4.1.15_record_sudo_usage  [INFO] Checking Configuration
    4.1.15_record_sudo_usage  [INFO] Performing audit
    4.1.15_record_sudo_usage  [ KO ] -w /var/log/auth.log -p wa -k sudoaction is not present in /etc/audit/audit.rules /etc/audit/rules.d/audit.rules
    4.1.15_record_sudo_usage  [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/4.1.16_record_kernel_modules.sh
    4.1.16_record_kernel_modu [INFO] Working on 4.1.16_record_kernel_modules
    4.1.16_record_kernel_modu [INFO] [DESCRIPTION] Collect kernel module loading and unloading.
    4.1.16_record_kernel_modu [INFO] Checking Configuration
    4.1.16_record_kernel_modu [INFO] Performing audit
    4.1.16_record_kernel_modu [ KO ] -w /sbin/insmod -p x -k modules is not present in /etc/audit/audit.rules /etc/audit/rules.d/audit.rules
    4.1.16_record_kernel_modu [ KO ] -w /sbin/rmmod -p x -k modules is not present in /etc/audit/audit.rules /etc/audit/rules.d/audit.rules
    4.1.16_record_kernel_modu [ KO ] -w /sbin/modprobe -p x -k modules is not present in /etc/audit/audit.rules /etc/audit/rules.d/audit.rules
    4.1.16_record_kernel_modu [ KO ] -a always,exit -F arch=b64 -S init_module -S delete_module -k modules is not present in /etc/audit/audit.rules /etc/audit/rules.d/audit.rules
    4.1.16_record_kernel_modu [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/4.1.17_freeze_auditd_conf.sh
    4.1.17_freeze_auditd_conf [INFO] Working on 4.1.17_freeze_auditd_conf
    4.1.17_freeze_auditd_conf [INFO] [DESCRIPTION] Make the audit configuration immutable.
    4.1.17_freeze_auditd_conf [INFO] Checking Configuration
    4.1.17_freeze_auditd_conf [INFO] Performing audit
    4.1.17_freeze_auditd_conf [ KO ] -e 2 is not present in /etc/audit/audit.rules /etc/audit/rules.d/audit.rules
    4.1.17_freeze_auditd_conf [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/4.2.1.1_install_syslog-ng.sh
    4.2.1.1_install_syslog-ng [INFO] Working on 4.2.1.1_install_syslog-ng
    4.2.1.1_install_syslog-ng [INFO] [DESCRIPTION] Install syslog-ng to manage logs
    4.2.1.1_install_syslog-ng [INFO] Checking Configuration
    4.2.1.1_install_syslog-ng [INFO] Performing audit
    4.2.1.1_install_syslog-ng [ KO ] syslog-ng is not installed!
    4.2.1.1_install_syslog-ng [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/4.2.1.2_enable_syslog-ng.sh
    4.2.1.2_enable_syslog-ng  [INFO] Working on 4.2.1.2_enable_syslog-ng
    4.2.1.2_enable_syslog-ng  [INFO] [DESCRIPTION] Ensure syslog-ng service is activated.
    4.2.1.2_enable_syslog-ng  [INFO] Checking Configuration
    4.2.1.2_enable_syslog-ng  [INFO] Performing audit
    4.2.1.2_enable_syslog-ng  [ KO ] syslog-ng is not installed!
    4.2.1.2_enable_syslog-ng  [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/4.2.1.3_configure_syslog-ng.sh
    4.2.1.3_configure_syslog- [INFO] Working on 4.2.1.3_configure_syslog-ng
    4.2.1.3_configure_syslog- [INFO] [DESCRIPTION] Configure /etc/syslog-ng/syslog-ng.conf .
    4.2.1.3_configure_syslog- [INFO] Checking Configuration
    4.2.1.3_configure_syslog- [INFO] Performing audit
    4.2.1.3_configure_syslog- [INFO] Ensure default and local facilities are preserved on the system
    4.2.1.3_configure_syslog- [INFO] No measure here, please review the file by yourself
    4.2.1.3_configure_syslog- [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/4.2.1.3_configure_syslog-ng.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/4.2.1.4_syslog_ng_logfiles_perm.sh
    4.2.1.4_syslog_ng_logfile [INFO] Working on 4.2.1.4_syslog_ng_logfiles_perm
    4.2.1.4_syslog_ng_logfile [INFO] [DESCRIPTION] Create and set permissions on syslog-ng logfiles.
    4.2.1.4_syslog_ng_logfile [INFO] Checking Configuration
    4.2.1.4_syslog_ng_logfile [INFO] Performing audit
    4.2.1.4_syslog_ng_logfile [ KO ] syslog-ng is not installed!
    4.2.1.4_syslog_ng_logfile [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/4.2.1.5_syslog-ng_remote_host.sh
    4.2.1.5_syslog-ng_remote_ [INFO] Working on 4.2.1.5_syslog-ng_remote_host
    4.2.1.5_syslog-ng_remote_ [INFO] [DESCRIPTION] Configure syslog-ng to send logs to a remote log host.
    4.2.1.5_syslog-ng_remote_ [INFO] Checking Configuration
    4.2.1.5_syslog-ng_remote_ [INFO] Performing audit
    4.2.1.5_syslog-ng_remote_ [ KO ] syslog-ng is not installed!
    4.2.1.5_syslog-ng_remote_ [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/4.2.1.6_remote_syslog-ng_acl.sh
    4.2.1.6_remote_syslog-ng_ [INFO] Working on 4.2.1.6_remote_syslog-ng_acl
    4.2.1.6_remote_syslog-ng_ [INFO] [DESCRIPTION] Configure syslog to accept remote syslog messages only on designated log hosts.
    4.2.1.6_remote_syslog-ng_ [INFO] Checking Configuration
    4.2.1.6_remote_syslog-ng_ [INFO] Performing audit
    4.2.1.6_remote_syslog-ng_ [ KO ] syslog-ng is not installed!
    4.2.1.6_remote_syslog-ng_ [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/4.2.2.1_journald_logs.sh
    4.2.2.1_journald_logs     [INFO] Working on 4.2.2.1_journald_logs
    4.2.2.1_journald_logs     [INFO] [DESCRIPTION] Configure journald to send logs to syslog-ng.
    4.2.2.1_journald_logs     [INFO] Checking Configuration
    4.2.2.1_journald_logs     [INFO] Performing audit
    4.2.2.1_journald_logs     [ OK ] /etc/systemd/journald.conf exists, checking configuration
    4.2.2.1_journald_logs     [ OK ] ^ForwardToSyslog=no is not present in /etc/systemd/journald.conf
    4.2.2.1_journald_logs     [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/4.2.2.1_journald_logs.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/4.2.2.2_journald_compress.sh
    4.2.2.2_journald_compress [INFO] Working on 4.2.2.2_journald_compress
    4.2.2.2_journald_compress [INFO] [DESCRIPTION] Configure journald to send logs to syslog-ng.
    4.2.2.2_journald_compress [INFO] Checking Configuration
    4.2.2.2_journald_compress [INFO] Performing audit
    4.2.2.2_journald_compress [ OK ] /etc/systemd/journald.conf exists, checking configuration
    4.2.2.2_journald_compress [ OK ] ^Compress=no is not present in /etc/systemd/journald.conf
    4.2.2.2_journald_compress [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/4.2.2.2_journald_compress.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/4.2.2.3_journald_write_persistent.sh
    4.2.2.3_journald_write_pe [INFO] Working on 4.2.2.3_journald_write_persistent
    4.2.2.3_journald_write_pe [INFO] [DESCRIPTION] Configure journald to write to a persistent location.
    4.2.2.3_journald_write_pe [INFO] Checking Configuration
    4.2.2.3_journald_write_pe [INFO] Performing audit
    4.2.2.3_journald_write_pe [ OK ] /etc/systemd/journald.conf exists, checking configuration
    4.2.2.3_journald_write_pe [ KO ] ^Storage=persistent is not present in /etc/systemd/journald.conf
    4.2.2.3_journald_write_pe [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/4.2.3_logs_permissions.sh
    4.2.3_logs_permissions    [INFO] Working on 4.2.3_logs_permissions
    4.2.3_logs_permissions    [INFO] [DESCRIPTION] Check permissions on logs (other has no permissions on any files and group does not have write or execute permissions on any file)
    4.2.3_logs_permissions    [INFO] Checking Configuration
    4.2.3_logs_permissions    [INFO] Performing audit
    4.2.3_logs_permissions    [ KO ] /var/log/dpkg.log permissions were not set to 640
    4.2.3_logs_permissions    [ KO ] /var/log/faillog permissions were not set to 640
    4.2.3_logs_permissions    [ OK ] /var/log/auth.log permissions were set to 640
    4.2.3_logs_permissions    [ KO ] /var/log/apt/eipp.log.xz permissions were not set to 640
    4.2.3_logs_permissions    [ OK ] /var/log/apt/term.log permissions were set to 640
    4.2.3_logs_permissions    [ KO ] /var/log/apt/history.log permissions were not set to 640
    4.2.3_logs_permissions    [ OK ] /var/log/kern.log permissions were set to 640
    4.2.3_logs_permissions    [ OK ] /var/log/cron.log permissions were set to 640
    4.2.3_logs_permissions    [ KO ] /var/log/unattended-upgrades/unattended-upgrades-shutdown.log permissions were not set to 640
    4.2.3_logs_permissions    [ OK ] /var/log/user.log permissions were set to 640
    4.2.3_logs_permissions    [ KO ] /var/log/wtmp permissions were not set to 640
    4.2.3_logs_permissions    [ KO ] /var/log/btmp permissions were not set to 640
    4.2.3_logs_permissions    [ OK ] /var/log/journal/03c37850ccc44cb7b56c6604e8096a06/user-1000.journal permissions were set to 640
    4.2.3_logs_permissions    [ OK ] /var/log/journal/03c37850ccc44cb7b56c6604e8096a06/system.journal permissions were set to 640
    4.2.3_logs_permissions    [ OK ] /var/log/syslog permissions were set to 640
    4.2.3_logs_permissions    [ KO ] /var/log/lastlog permissions were not set to 640
    4.2.3_logs_permissions    [ OK ] /var/log/fail2ban.log permissions were set to 640
    4.2.3_logs_permissions    [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/4.3_configure_logrotate.sh
    4.3_configure_logrotate   [INFO] Working on 4.3_configure_logrotate
    4.3_configure_logrotate   [INFO] [DESCRIPTION] Configure logrotate to prevent logfile from growing unmanageable.
    4.3_configure_logrotate   [INFO] Checking Configuration
    4.3_configure_logrotate   [INFO] Performing audit
    4.3_configure_logrotate   [INFO] Ensure logs are properly rotated (especially syslog-ng)
    4.3_configure_logrotate   [INFO] No measure here, please review the files by yourself
    4.3_configure_logrotate   [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/4.3_configure_logrotate.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/4.4_logrotate_permissions.sh
    4.4_logrotate_permissions [INFO] Working on 4.4_logrotate_permissions
    4.4_logrotate_permissions [INFO] [DESCRIPTION] Configure logrotate to assign appropriate permissions.
    4.4_logrotate_permissions [INFO] Checking Configuration
    4.4_logrotate_permissions [INFO] Performing audit
    4.4_logrotate_permissions [ KO ] Logrotate permissions are not configured
    4.4_logrotate_permissions [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/5.1.1_enable_cron.sh
    5.1.1_enable_cron         [INFO] Working on 5.1.1_enable_cron
    5.1.1_enable_cron         [INFO] [DESCRIPTION] Cron package is installed and enabled.
    5.1.1_enable_cron         [INFO] Checking Configuration
    5.1.1_enable_cron         [INFO] Performing audit
    5.1.1_enable_cron         [ OK ] cron is installed
    5.1.1_enable_cron         [ OK ] cron is enabled
    5.1.1_enable_cron         [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/5.1.1_enable_cron.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/5.1.2_crontab_perm_ownership.sh
    5.1.2_crontab_perm_owners [INFO] Working on 5.1.2_crontab_perm_ownership
    5.1.2_crontab_perm_owners [INFO] [DESCRIPTION] User/Group set to root and permissions to 600 on /etc/crontab .
    5.1.2_crontab_perm_owners [INFO] Checking Configuration
    5.1.2_crontab_perm_owners [INFO] Performing audit
    5.1.2_crontab_perm_owners [ OK ] /etc/crontab has correct ownership
    5.1.2_crontab_perm_owners [ KO ] /etc/crontab permissions were not set to 600
    5.1.2_crontab_perm_owners [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/5.1.3_cron_hourly_perm_ownership.sh
    5.1.3_cron_hourly_perm_ow [INFO] Working on 5.1.3_cron_hourly_perm_ownership
    5.1.3_cron_hourly_perm_ow [INFO] [DESCRIPTION] User/Group set to root and permissions to 700 on /etc/cron.hourly .
    5.1.3_cron_hourly_perm_ow [INFO] Checking Configuration
    5.1.3_cron_hourly_perm_ow [INFO] Performing audit
    5.1.3_cron_hourly_perm_ow [ OK ] /etc/cron.hourly has correct ownership
    5.1.3_cron_hourly_perm_ow [ KO ] /etc/cron.hourly permissions were not set to 700
    5.1.3_cron_hourly_perm_ow [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/5.1.4_cron_daily_perm_ownership.sh
    5.1.4_cron_daily_perm_own [INFO] Working on 5.1.4_cron_daily_perm_ownership
    5.1.4_cron_daily_perm_own [INFO] [DESCRIPTION] User/group set to root and permissions to 700 on /etc/cron.daily .
    5.1.4_cron_daily_perm_own [INFO] Checking Configuration
    5.1.4_cron_daily_perm_own [INFO] Performing audit
    5.1.4_cron_daily_perm_own [ OK ] /etc/cron.daily has correct ownership
    5.1.4_cron_daily_perm_own [ KO ] /etc/cron.daily permissions were not set to 700
    5.1.4_cron_daily_perm_own [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/5.1.5_cron_weekly_perm_ownership.sh
    5.1.5_cron_weekly_perm_ow [INFO] Working on 5.1.5_cron_weekly_perm_ownership
    5.1.5_cron_weekly_perm_ow [INFO] [DESCRIPTION] User/group set to root and permissions to 700 on /etc/cron.weekly .
    5.1.5_cron_weekly_perm_ow [INFO] Checking Configuration
    5.1.5_cron_weekly_perm_ow [INFO] Performing audit
    5.1.5_cron_weekly_perm_ow [ OK ] /etc/cron.weekly has correct ownership
    5.1.5_cron_weekly_perm_ow [ KO ] /etc/cron.weekly permissions were not set to 700
    5.1.5_cron_weekly_perm_ow [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/5.1.6_cron_monthly_perm_ownership.sh
    5.1.6_cron_monthly_perm_o [INFO] Working on 5.1.6_cron_monthly_perm_ownership
    5.1.6_cron_monthly_perm_o [INFO] [DESCRIPTION] User/group set to root and permissions to 700 on /etc/cron.monthly .
    5.1.6_cron_monthly_perm_o [INFO] Checking Configuration
    5.1.6_cron_monthly_perm_o [INFO] Performing audit
    5.1.6_cron_monthly_perm_o [ OK ] /etc/cron.monthly has correct ownership
    5.1.6_cron_monthly_perm_o [ KO ] /etc/cron.monthly permissions were not set to 700
    5.1.6_cron_monthly_perm_o [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/5.1.7_cron_d_perm_ownership.sh
    5.1.7_cron_d_perm_ownersh [INFO] Working on 5.1.7_cron_d_perm_ownership
    5.1.7_cron_d_perm_ownersh [INFO] [DESCRIPTION] User/group set to root and permissions to 700 on /etc/cron.d .
    5.1.7_cron_d_perm_ownersh [INFO] Checking Configuration
    5.1.7_cron_d_perm_ownersh [INFO] Performing audit
    5.1.7_cron_d_perm_ownersh [ OK ] /etc/cron.d has correct ownership
    5.1.7_cron_d_perm_ownersh [ KO ] /etc/cron.d permissions were not set to 700
    5.1.7_cron_d_perm_ownersh [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/5.1.8_cron_users.sh
    5.1.8_cron_users          [INFO] Working on 5.1.8_cron_users
    5.1.8_cron_users          [INFO] [DESCRIPTION] Restrict at/cron to authorized users.
    5.1.8_cron_users          [INFO] Checking Configuration
    5.1.8_cron_users          [INFO] Performing audit
    5.1.8_cron_users          [ OK ] /etc/cron.deny is absent
    5.1.8_cron_users          [ OK ] /etc/at.deny is absent
    5.1.8_cron_users          [ KO ] /etc/cron.allow is absent, should exist
    5.1.8_cron_users          [ KO ] /etc/at.allow is absent, should exist
    5.1.8_cron_users          [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/5.2.1_sshd_conf_perm_ownership.sh
    5.2.1_sshd_conf_perm_owne [INFO] Working on 5.2.1_sshd_conf_perm_ownership
    5.2.1_sshd_conf_perm_owne [INFO] [DESCRIPTION] Checking permissions and ownership to root 600 for sshd_config.
    5.2.1_sshd_conf_perm_owne [INFO] Checking Configuration
    5.2.1_sshd_conf_perm_owne [INFO] Performing audit
    5.2.1_sshd_conf_perm_owne [ OK ] /etc/ssh/sshd_config has correct ownership
    5.2.1_sshd_conf_perm_owne [ KO ] /etc/ssh/sshd_config permissions were not set to 600
    5.2.1_sshd_conf_perm_owne [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/5.2.2_ssh_host_private_keys_perm_ownership.sh
    5.2.2_ssh_host_private_ke [INFO] Working on 5.2.2_ssh_host_private_keys_perm_ownership
    5.2.2_ssh_host_private_ke [INFO] [DESCRIPTION] Checking permissions and ownership to root 600 for ssh private keys. 
    5.2.2_ssh_host_private_ke [INFO] Checking Configuration
    5.2.2_ssh_host_private_ke [INFO] Performing audit
    5.2.2_ssh_host_private_ke [ OK ] /etc/ssh/ssh_host_ecdsa_key permissions were set to 600
    5.2.2_ssh_host_private_ke [ OK ] /etc/ssh/ssh_host_rsa_key permissions were set to 600
    5.2.2_ssh_host_private_ke [ OK ] /etc/ssh/ssh_host_ed25519_key permissions were set to 600
    5.2.2_ssh_host_private_ke [ OK ] SSH private keys in /etc/ssh have correct permissions
    5.2.2_ssh_host_private_ke [ OK ] /etc/ssh/ssh_host_ecdsa_key ownership was set to root:root
    5.2.2_ssh_host_private_ke [ OK ] /etc/ssh/ssh_host_rsa_key ownership was set to root:root
    5.2.2_ssh_host_private_ke [ OK ] /etc/ssh/ssh_host_ed25519_key ownership was set to root:root
    5.2.2_ssh_host_private_ke [ OK ] SSH private keys in /etc/ssh have correct ownership
    5.2.2_ssh_host_private_ke [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/5.2.2_ssh_host_private_keys_perm_ownership.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/5.2.3_ssh_host_public_keys_perm_ownership.sh
    5.2.3_ssh_host_public_key [INFO] Working on 5.2.3_ssh_host_public_keys_perm_ownership
    5.2.3_ssh_host_public_key [INFO] [DESCRIPTION] Checking permissions and ownership to root 644 for ssh public keys. 
    5.2.3_ssh_host_public_key [INFO] Checking Configuration
    5.2.3_ssh_host_public_key [INFO] Performing audit
    5.2.3_ssh_host_public_key [ OK ] /etc/ssh/ssh_host_ed25519_key.pub permissions were set to 644
    5.2.3_ssh_host_public_key [ OK ] /etc/ssh/ssh_host_rsa_key.pub permissions were set to 644
    5.2.3_ssh_host_public_key [ OK ] /etc/ssh/ssh_host_ecdsa_key.pub permissions were set to 644
    5.2.3_ssh_host_public_key [ OK ] SSH public keys in /etc/ssh have correct permissions
    5.2.3_ssh_host_public_key [ OK ] /etc/ssh/ssh_host_ed25519_key.pub ownership was set to root:root
    5.2.3_ssh_host_public_key [ OK ] /etc/ssh/ssh_host_rsa_key.pub ownership was set to root:root
    5.2.3_ssh_host_public_key [ OK ] /etc/ssh/ssh_host_ecdsa_key.pub ownership was set to root:root
    5.2.3_ssh_host_public_key [ OK ] SSH public keys in /etc/ssh have correct ownership
    5.2.3_ssh_host_public_key [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/5.2.3_ssh_host_public_keys_perm_ownership.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/5.2.4_sshd_protocol.sh
    5.2.4_sshd_protocol       [INFO] Working on 5.2.4_sshd_protocol
    5.2.4_sshd_protocol       [INFO] [DESCRIPTION] Set secure shell (SSH) protocol to 2.
    5.2.4_sshd_protocol       [INFO] Checking Configuration
    5.2.4_sshd_protocol       [INFO] Performing audit
    5.2.4_sshd_protocol       [ OK ] openssh-server is installed
    5.2.4_sshd_protocol       [ KO ] ^Protocol[[:space:]]*2 is not present in /etc/ssh/sshd_config
    5.2.4_sshd_protocol       [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/5.2.5_sshd_loglevel.sh
    5.2.5_sshd_loglevel       [INFO] Working on 5.2.5_sshd_loglevel
    5.2.5_sshd_loglevel       [INFO] [DESCRIPTION] Set LogLevel to INFO for SSH.
    5.2.5_sshd_loglevel       [INFO] Checking Configuration
    5.2.5_sshd_loglevel       [INFO] Performing audit
    5.2.5_sshd_loglevel       [ OK ] openssh-server is installed
    5.2.5_sshd_loglevel       [ KO ] ^LogLevel[[:space:]]*(INFO|VERBOSE) is not present in /etc/ssh/sshd_config
    5.2.5_sshd_loglevel       [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/5.2.6_disable_x11_forwarding.sh
    5.2.6_disable_x11_forward [INFO] Working on 5.2.6_disable_x11_forwarding
    5.2.6_disable_x11_forward [INFO] [DESCRIPTION] Disable SSH X11 forwarding.
    5.2.6_disable_x11_forward [INFO] Checking Configuration
    5.2.6_disable_x11_forward [INFO] Performing audit
    5.2.6_disable_x11_forward [ OK ] openssh-server is installed
    5.2.6_disable_x11_forward [ KO ] ^X11Forwarding[[:space:]]*no is not present in /etc/ssh/sshd_config
    5.2.6_disable_x11_forward [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/5.2.7_sshd_maxauthtries.sh
    5.2.7_sshd_maxauthtries   [INFO] Working on 5.2.7_sshd_maxauthtries
    5.2.7_sshd_maxauthtries   [INFO] [DESCRIPTION] Set SSH MaxAuthTries to 4.
    5.2.7_sshd_maxauthtries   [INFO] Checking Configuration
    5.2.7_sshd_maxauthtries   [INFO] Performing audit
    5.2.7_sshd_maxauthtries   [ OK ] openssh-server is installed
    5.2.7_sshd_maxauthtries   [ KO ] 10 is higher than recommended 4 for MaxAuthTries
    5.2.7_sshd_maxauthtries   [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/5.2.8_enable_sshd_ignorerhosts.sh
    5.2.8_enable_sshd_ignorer [INFO] Working on 5.2.8_enable_sshd_ignorerhosts
    5.2.8_enable_sshd_ignorer [INFO] [DESCRIPTION] Set SSH IgnoreRhosts to Yes.
    5.2.8_enable_sshd_ignorer [INFO] Checking Configuration
    5.2.8_enable_sshd_ignorer [INFO] Performing audit
    5.2.8_enable_sshd_ignorer [ OK ] openssh-server is installed
    5.2.8_enable_sshd_ignorer [ KO ] ^IgnoreRhosts[[:space:]]*yes is not present in /etc/ssh/sshd_config
    5.2.8_enable_sshd_ignorer [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/5.2.9_disable_sshd_hostbasedauthentication.sh
    5.2.9_disable_sshd_hostba [INFO] Working on 5.2.9_disable_sshd_hostbasedauthentication
    5.2.9_disable_sshd_hostba [INFO] [DESCRIPTION] Set SSH HostbasedAUthentication to No.
    5.2.9_disable_sshd_hostba [INFO] Checking Configuration
    5.2.9_disable_sshd_hostba [INFO] Performing audit
    5.2.9_disable_sshd_hostba [ OK ] openssh-server is installed
    5.2.9_disable_sshd_hostba [ KO ] ^HostbasedAuthentication[[:space:]]*no is not present in /etc/ssh/sshd_config
    5.2.9_disable_sshd_hostba [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/5.2.10_disable_root_login.sh
    5.2.10_disable_root_login [INFO] Working on 5.2.10_disable_root_login
    5.2.10_disable_root_login [INFO] [DESCRIPTION] Disable SSH Root Login.
    5.2.10_disable_root_login [INFO] Checking Configuration
    5.2.10_disable_root_login [INFO] Performing audit
    5.2.10_disable_root_login [ OK ] openssh-server is installed
    5.2.10_disable_root_login [ KO ] ^PermitRootLogin[[:space:]]*no is not present in /etc/ssh/sshd_config
    5.2.10_disable_root_login [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/5.2.11_disable_sshd_permitemptypasswords.sh
    5.2.11_disable_sshd_permi [INFO] Working on 5.2.11_disable_sshd_permitemptypasswords
    5.2.11_disable_sshd_permi [INFO] [DESCRIPTION] Set SSH PermitEmptyPasswords to No in order to disallow SSH login to accounts with empty password strigs.
    5.2.11_disable_sshd_permi [INFO] Checking Configuration
    5.2.11_disable_sshd_permi [INFO] Performing audit
    5.2.11_disable_sshd_permi [ OK ] openssh-server is installed
    5.2.11_disable_sshd_permi [ KO ] ^PermitEmptyPasswords[[:space:]]*no is not present in /etc/ssh/sshd_config
    5.2.11_disable_sshd_permi [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/5.2.12_disable_sshd_setenv.sh
    5.2.12_disable_sshd_seten [INFO] Working on 5.2.12_disable_sshd_setenv
    5.2.12_disable_sshd_seten [INFO] [DESCRIPTION] Do not allow users to set environment options.
    5.2.12_disable_sshd_seten [INFO] Checking Configuration
    5.2.12_disable_sshd_seten [INFO] Performing audit
    5.2.12_disable_sshd_seten [ OK ] openssh-server is installed
    5.2.12_disable_sshd_seten [ KO ] ^PermitUserEnvironment[[:space:]]*no is not present in /etc/ssh/sshd_config
    5.2.12_disable_sshd_seten [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/5.2.13_sshd_ciphers.sh
    5.2.13_sshd_ciphers       [INFO] Working on 5.2.13_sshd_ciphers
    5.2.13_sshd_ciphers       [INFO] [DESCRIPTION] Use only approved ciphers in counter mode (ctr) or Galois counter mode (gcm).
    5.2.13_sshd_ciphers       [INFO] Checking Configuration
    5.2.13_sshd_ciphers       [INFO] Performing audit
    5.2.13_sshd_ciphers       [ OK ] openssh-server is installed
    5.2.13_sshd_ciphers       [ KO ] ^Ciphers[[:space:]]*chacha20-poly1305@openssh.com,aes256-gcm@openssh.com,aes128-gcm@openssh.com,aes256-ctr,aes192-ctr,aes128-ctr is not present in /etc/ssh/sshd_config
    5.2.13_sshd_ciphers       [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/5.2.14_ssh_cry_mac.sh
    5.2.14_ssh_cry_mac        [INFO] Working on 5.2.14_ssh_cry_mac
    5.2.14_ssh_cry_mac        [INFO] [DESCRIPTION] Checking Message Authentication Code ciphers for preferred UMAC and SHA-256|512 with Encrypt-Then-Mac (etm) setting.
    5.2.14_ssh_cry_mac        [INFO] Checking Configuration
    5.2.14_ssh_cry_mac        [INFO] Performing audit
    5.2.14_ssh_cry_mac        [ OK ] openssh-server is installed
    5.2.14_ssh_cry_mac        [ KO ] ^MACs[[:space:]]*hmac-sha2-512-etm@openssh.com,hmac-sha2-256-etm@openssh.com,hmac-sha2-512,hmac-sha2-256 is not present in /etc/ssh/sshd_config
    5.2.14_ssh_cry_mac        [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/5.2.15_ssh_cry_kex.sh
    5.2.15_ssh_cry_kex        [INFO] Working on 5.2.15_ssh_cry_kex
    5.2.15_ssh_cry_kex        [INFO] [DESCRIPTION] Checking key exchange ciphers.
    5.2.15_ssh_cry_kex        [INFO] Checking Configuration
    5.2.15_ssh_cry_kex        [INFO] Performing audit
    5.2.15_ssh_cry_kex        [ OK ] openssh-server is installed
    5.2.15_ssh_cry_kex        [ KO ] ^KexAlgorithms[[:space:]]*curve25519-sha256,curve25519-sha256@libssh.org,diffie-hellman-group14-sha256,diffie-hellman-group16-sha512,diffie-hellman-group18-sha512,ecdh-sha2-nistp521,ecdh-sha2-nistp384,ecdh-sha2-nistp256,diffie-hellman-group-exchange-sha256 is not present in /etc/ssh/sshd_config
    5.2.15_ssh_cry_kex        [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/5.2.16_sshd_idle_timeout.sh
    5.2.16_sshd_idle_timeout  [INFO] Working on 5.2.16_sshd_idle_timeout
    5.2.16_sshd_idle_timeout  [INFO] [DESCRIPTION] Set Idle Timeout Interval for user login.
    5.2.16_sshd_idle_timeout  [INFO] Checking Configuration
    5.2.16_sshd_idle_timeout  [INFO] Performing audit
    5.2.16_sshd_idle_timeout  [ OK ] openssh-server is installed
    5.2.16_sshd_idle_timeout  [ KO ] ^ClientAliveInterval[[:space:]]*300 is not present in /etc/ssh/sshd_config
    5.2.16_sshd_idle_timeout  [ KO ] ^ClientAliveCountMax[[:space:]]*0 is not present in /etc/ssh/sshd_config
    5.2.16_sshd_idle_timeout  [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/5.2.17_sshd_login_grace_time.sh
    5.2.17_sshd_login_grace_t [INFO] Working on 5.2.17_sshd_login_grace_time
    5.2.17_sshd_login_grace_t [INFO] [DESCRIPTION] Set Login Grace Time for user login.
    5.2.17_sshd_login_grace_t [INFO] Checking Configuration
    5.2.17_sshd_login_grace_t [INFO] Performing audit
    5.2.17_sshd_login_grace_t [ OK ] openssh-server is installed
    5.2.17_sshd_login_grace_t [ KO ] ^LoginGraceTime[[:space:]]*60 is not present in /etc/ssh/sshd_config
    5.2.17_sshd_login_grace_t [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/5.2.18_sshd_limit_access.sh
    5.2.18_sshd_limit_access  [INFO] Working on 5.2.18_sshd_limit_access
    5.2.18_sshd_limit_access  [INFO] [DESCRIPTION] Limite access via SSH by (dis)allowing specific users or groups.
    5.2.18_sshd_limit_access  [INFO] Checking Configuration
    5.2.18_sshd_limit_access  [INFO] ALLOWED_USERS is not set, defaults to wildcard
    5.2.18_sshd_limit_access  [INFO] ALLOWED_GROUPS is not set, defaults to wildcard
    5.2.18_sshd_limit_access  [INFO] DENIED_USERS is not set, defaults to nobody
    5.2.18_sshd_limit_access  [INFO] DENIED_GROUPS is not set, defaults to nobody
    5.2.18_sshd_limit_access  [INFO] Performing audit
    5.2.18_sshd_limit_access  [ OK ] openssh-server is installed
    5.2.18_sshd_limit_access  [ KO ] ^AllowUsers[[:space:]]** is not present in /etc/ssh/sshd_config
    5.2.18_sshd_limit_access  [ KO ] ^AllowGroups[[:space:]]** is not present in /etc/ssh/sshd_config
    5.2.18_sshd_limit_access  [ KO ] ^DenyUsers[[:space:]]*nobody is not present in /etc/ssh/sshd_config
    5.2.18_sshd_limit_access  [ KO ] ^DenyGroups[[:space:]]*nobody is not present in /etc/ssh/sshd_config
    5.2.18_sshd_limit_access  [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/5.2.19_ssh_banner.sh
    5.2.19_ssh_banner         [INFO] Working on 5.2.19_ssh_banner
    5.2.19_ssh_banner         [INFO] [DESCRIPTION] Set ssh banner.
    5.2.19_ssh_banner         [INFO] Checking Configuration
    5.2.19_ssh_banner         [INFO] BANNER_FILE is not set, defaults to wildcard
    5.2.19_ssh_banner         [INFO] Performing audit
    5.2.19_ssh_banner         [ OK ] openssh-server is installed
    5.2.19_ssh_banner         [ KO ] ^Banner[[:space:]]* is not present in /etc/ssh/sshd_config
    5.2.19_ssh_banner         [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/5.2.20_enable_ssh_pam.sh
    5.2.20_enable_ssh_pam     [INFO] Working on 5.2.20_enable_ssh_pam
    5.2.20_enable_ssh_pam     [INFO] [DESCRIPTION] Enable SSH PAM.
    5.2.20_enable_ssh_pam     [INFO] Checking Configuration
    5.2.20_enable_ssh_pam     [INFO] Performing audit
    5.2.20_enable_ssh_pam     [ OK ] openssh-server is installed
    5.2.20_enable_ssh_pam     [ OK ] ^usepam[[:space:]]*yes is present in /etc/ssh/sshd_config
    5.2.20_enable_ssh_pam     [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/5.2.20_enable_ssh_pam.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/5.2.21_disable_ssh_allow_tcp_forwarding.sh
    5.2.21_disable_ssh_allow_ [INFO] Working on 5.2.21_disable_ssh_allow_tcp_forwarding
    5.2.21_disable_ssh_allow_ [INFO] [DESCRIPTION] Disable SSH AllowTCPForwarding.
    5.2.21_disable_ssh_allow_ [INFO] Checking Configuration
    5.2.21_disable_ssh_allow_ [INFO] Performing audit
    5.2.21_disable_ssh_allow_ [ OK ] openssh-server is installed
    5.2.21_disable_ssh_allow_ [ KO ] ^AllowTCPForwarding[[:space:]]*no is not present in /etc/ssh/sshd_config
    5.2.21_disable_ssh_allow_ [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/5.2.22_configure_ssh_max_startups.sh
    5.2.22_configure_ssh_max_ [INFO] Working on 5.2.22_configure_ssh_max_startups
    5.2.22_configure_ssh_max_ [INFO] [DESCRIPTION] Configure SSHMaxStartups.
    5.2.22_configure_ssh_max_ [INFO] Checking Configuration
    5.2.22_configure_ssh_max_ [INFO] Performing audit
    5.2.22_configure_ssh_max_ [ OK ] openssh-server is installed
    5.2.22_configure_ssh_max_ [ KO ] ^maxstartups[[:space:]]*10:30:60 is not present in /etc/ssh/sshd_config
    5.2.22_configure_ssh_max_ [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/5.2.23_limit_ssh_max_sessions.sh
    5.2.23_limit_ssh_max_sess [INFO] Working on 5.2.23_limit_ssh_max_sessions
    5.2.23_limit_ssh_max_sess [INFO] [DESCRIPTION] Limit SSH MaxSessions.
    5.2.23_limit_ssh_max_sess [INFO] Checking Configuration
    5.2.23_limit_ssh_max_sess [INFO] Performing audit
    5.2.23_limit_ssh_max_sess [ OK ] openssh-server is installed
    5.2.23_limit_ssh_max_sess [ KO ] ^maxsessions[[:space:]]*10 is not present in /etc/ssh/sshd_config
    5.2.23_limit_ssh_max_sess [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/5.3.1_enable_pwquality.sh
    5.3.1_enable_pwquality    [INFO] Working on 5.3.1_enable_pwquality
    5.3.1_enable_pwquality    [INFO] [DESCRIPTION] Set password creation requirement parameters using pam.cracklib.
    5.3.1_enable_pwquality    [INFO] Checking Configuration
    5.3.1_enable_pwquality    [INFO] Performing audit
    5.3.1_enable_pwquality    [ KO ] libpam-pwquality is not installed!
    5.3.1_enable_pwquality    [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/5.3.2_enable_lockout_failed_password.sh
    5.3.2_enable_lockout_fail [INFO] Working on 5.3.2_enable_lockout_failed_password
    5.3.2_enable_lockout_fail [INFO] [DESCRIPTION] Set lockout for failed password attemps.
    5.3.2_enable_lockout_fail [INFO] Checking Configuration
    5.3.2_enable_lockout_fail [INFO] Performing audit
    5.3.2_enable_lockout_fail [ OK ] libpam-modules-bin is installed
    5.3.2_enable_lockout_fail [ KO ] ^auth[[:space:]]*required[[:space:]]*pam_((tally[2]?)|(faillock))\.so is not present in /etc/pam.d/common-auth
    5.3.2_enable_lockout_fail [ KO ] pam_((tally[2]?)|(faillock))\.so is not present in /etc/pam.d/common-account
    5.3.2_enable_lockout_fail [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/5.3.3_limit_password_reuse.sh
    5.3.3_limit_password_reus [INFO] Working on 5.3.3_limit_password_reuse
    5.3.3_limit_password_reus [INFO] [DESCRIPTION] Limit password reuse.
    5.3.3_limit_password_reus [INFO] Checking Configuration
    5.3.3_limit_password_reus [INFO] Performing audit
    5.3.3_limit_password_reus [ OK ] libpam-modules is installed
    5.3.3_limit_password_reus [ KO ] ^password.*remember is not present in /etc/pam.d/common-password
    5.3.3_limit_password_reus [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/5.3.4_acc_pam_sha512.sh
    5.3.4_acc_pam_sha512      [INFO] Working on 5.3.4_acc_pam_sha512
    5.3.4_acc_pam_sha512      [INFO] [DESCRIPTION] Check that the algorithm declared in PAM for password changes is sha512 (or yescrypt for Debian 11+)
    5.3.4_acc_pam_sha512      [INFO] Checking Configuration
    5.3.4_acc_pam_sha512      [INFO] Performing audit
    5.3.4_acc_pam_sha512      [ OK ] ^\s*password\s.+\s+pam_unix\.so\s+.*(sha512|yescrypt) is present in /etc/pam.d/common-password
    5.3.4_acc_pam_sha512      [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/5.3.4_acc_pam_sha512.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/5.4.1.1_set_password_exp_days.sh
    5.4.1.1_set_password_exp_ [INFO] Working on 5.4.1.1_set_password_exp_days
    5.4.1.1_set_password_exp_ [INFO] [DESCRIPTION] Set password expiration days.
    5.4.1.1_set_password_exp_ [INFO] Checking Configuration
    5.4.1.1_set_password_exp_ [INFO] Performing audit
    5.4.1.1_set_password_exp_ [ OK ] login is installed
    5.4.1.1_set_password_exp_ [ KO ] ^PASS_MAX_DAYS[[:space:]]*90 is not present in /etc/login.defs
    5.4.1.1_set_password_exp_ [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/5.4.1.2_set_password_min_days_change.sh
    5.4.1.2_set_password_min_ [INFO] Working on 5.4.1.2_set_password_min_days_change
    5.4.1.2_set_password_min_ [INFO] [DESCRIPTION] Set password change minimum number of days.
    5.4.1.2_set_password_min_ [INFO] Checking Configuration
    5.4.1.2_set_password_min_ [INFO] Performing audit
    5.4.1.2_set_password_min_ [ OK ] login is installed
    5.4.1.2_set_password_min_ [ KO ] ^PASS_MIN_DAYS[[:space:]]*7 is not present in /etc/login.defs
    5.4.1.2_set_password_min_ [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/5.4.1.3_set_password_exp_warning_days.sh
    5.4.1.3_set_password_exp_ [INFO] Working on 5.4.1.3_set_password_exp_warning_days
    5.4.1.3_set_password_exp_ [INFO] [DESCRIPTION] Set password expiration warning days.
    5.4.1.3_set_password_exp_ [INFO] Checking Configuration
    5.4.1.3_set_password_exp_ [INFO] Performing audit
    5.4.1.3_set_password_exp_ [ OK ] login is installed
    5.4.1.3_set_password_exp_ [ OK ] ^PASS_WARN_AGE[[:space:]]*7 is present in /etc/login.defs
    5.4.1.3_set_password_exp_ [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/5.4.1.3_set_password_exp_warning_days.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/5.4.1.4_lock_inactive_user_account.sh
    5.4.1.4_lock_inactive_use [INFO] Working on 5.4.1.4_lock_inactive_user_account
    5.4.1.4_lock_inactive_use [INFO] [DESCRIPTION] Lock inactive user accounts.
    5.4.1.4_lock_inactive_use [INFO] Checking Configuration
    5.4.1.4_lock_inactive_use [INFO] Performing audit
    5.4.1.4_lock_inactive_use [INFO] Looking at the manual of useradd, it seems that this recommendation does not fill the title
    5.4.1.4_lock_inactive_use [INFO] The number of days after a password expires until the account is permanently disabled.
    5.4.1.4_lock_inactive_use [INFO] Which is not inactive users per se
    5.4.1.4_lock_inactive_use [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/5.4.1.4_lock_inactive_user_account.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/5.4.1.5_last_password_change_past.sh
    5.4.1.5_last_password_cha [INFO] Working on 5.4.1.5_last_password_change_past
    5.4.1.5_last_password_cha [INFO] [DESCRIPTION] Check that user last paswword change date is in the past.
    5.4.1.5_last_password_cha [INFO] Checking Configuration
    5.4.1.5_last_password_cha [INFO] Performing audit
    5.4.1.5_last_password_cha [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/5.4.1.5_last_password_change_past.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/5.4.2_disable_system_accounts.sh
    5.4.2_disable_system_acco [INFO] Working on 5.4.2_disable_system_accounts
    5.4.2_disable_system_acco [INFO] [DESCRIPTION] Disable system accounts, preventing them from interactive login.
    5.4.2_disable_system_acco [INFO] Checking Configuration
    5.4.2_disable_system_acco [INFO] Performing audit
    5.4.2_disable_system_acco [INFO] Checking if admin accounts have a login shell different than /bin/false /usr/sbin/nologin /sbin/nologin
    5.4.2_disable_system_acco [ OK ] All admin accounts deactivated
    5.4.2_disable_system_acco [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/5.4.2_disable_system_accounts.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/5.4.3_default_root_group.sh
    5.4.3_default_root_group  [INFO] Working on 5.4.3_default_root_group
    5.4.3_default_root_group  [INFO] [DESCRIPTION] Set default group for root account to 0.
    5.4.3_default_root_group  [INFO] Checking Configuration
    5.4.3_default_root_group  [INFO] Performing audit
    5.4.3_default_root_group  [ OK ] Root group has GID 0
    5.4.3_default_root_group  [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/5.4.3_default_root_group.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/5.4.4_default_umask.sh
    5.4.4_default_umask       [INFO] Working on 5.4.4_default_umask
    5.4.4_default_umask       [INFO] [DESCRIPTION] Set default mask for users to 077.
    5.4.4_default_umask       [INFO] Checking Configuration
    5.4.4_default_umask       [INFO] Performing audit
    5.4.4_default_umask       [ KO ] umask 077 is not present in /etc/bash.bashrc /etc/profile.d /etc/profile
    5.4.4_default_umask       [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/5.4.5_default_timeout.sh
    5.4.5_default_timeout     [INFO] Working on 5.4.5_default_timeout
    5.4.5_default_timeout     [INFO] [DESCRIPTION] Timeout 600 seconds on tty.
    5.4.5_default_timeout     [INFO] Checking Configuration
    5.4.5_default_timeout     [INFO] Performing audit
    5.4.5_default_timeout     [ KO ] TMOUT= is not present in /etc/bash.bashrc /etc/profile.d /etc/profile
    5.4.5_default_timeout     [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/5.5_secure_tty.sh
    5.5_secure_tty            [INFO] Working on 5.5_secure_tty
    5.5_secure_tty            [INFO] [DESCRIPTION] Restrict root login to system console.
    5.5_secure_tty            [INFO] Checking Configuration
    5.5_secure_tty            [INFO] Performing audit
    5.5_secure_tty            [INFO] Remove terminal entries in /etc/securetty for any consoles that are not in a physically secure location.
    5.5_secure_tty            [INFO] No measure here, please review the file by yourself
    5.5_secure_tty            [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/5.5_secure_tty.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/5.6_restrict_su.sh
    5.6_restrict_su           [INFO] Working on 5.6_restrict_su
    5.6_restrict_su           [INFO] [DESCRIPTION] Restrict access to su command.
    5.6_restrict_su           [INFO] Checking Configuration
    5.6_restrict_su           [INFO] Performing audit
    5.6_restrict_su           [ OK ] login is installed
    5.6_restrict_su           [ KO ] ^auth[[:space:]]*required[[:space:]]*pam_wheel.so is not present in /etc/pam.d/su
    5.6_restrict_su           [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/6.1.2_etc_passwd_permissions.sh
    6.1.2_etc_passwd_permissi [INFO] Working on 6.1.2_etc_passwd_permissions
    6.1.2_etc_passwd_permissi [INFO] [DESCRIPTION] Check 644 permissions and root:root ownership on /etc/passwd
    6.1.2_etc_passwd_permissi [INFO] Checking Configuration
    6.1.2_etc_passwd_permissi [INFO] Performing audit
    6.1.2_etc_passwd_permissi [ OK ] /etc/passwd has correct permissions
    6.1.2_etc_passwd_permissi [ OK ] /etc/passwd has correct ownership
    6.1.2_etc_passwd_permissi [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/6.1.2_etc_passwd_permissions.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/6.1.3_etc_gshadow-_permissions.sh
    6.1.3_etc_gshadow-_permis [INFO] Working on 6.1.3_etc_gshadow-_permissions
    6.1.3_etc_gshadow-_permis [INFO] [DESCRIPTION] Check 640 permissions and root:root ownership on /etc/gshadow-
    6.1.3_etc_gshadow-_permis [INFO] Checking Configuration
    6.1.3_etc_gshadow-_permis [INFO] Performing audit
    6.1.3_etc_gshadow-_permis [ OK ] /etc/gshadow- has correct permissions
    6.1.3_etc_gshadow-_permis [ OK ] /etc/gshadow- has correct ownership
    6.1.3_etc_gshadow-_permis [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/6.1.3_etc_gshadow-_permissions.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/6.1.4_etc_shadow_permissions.sh
    6.1.4_etc_shadow_permissi [INFO] Working on 6.1.4_etc_shadow_permissions
    6.1.4_etc_shadow_permissi [INFO] [DESCRIPTION] Check 640 permissions and root:root ownership on /etc/shadow
    6.1.4_etc_shadow_permissi [INFO] Checking Configuration
    6.1.4_etc_shadow_permissi [INFO] Performing audit
    6.1.4_etc_shadow_permissi [ OK ] /etc/shadow has correct permissions
    6.1.4_etc_shadow_permissi [ OK ] /etc/shadow has correct ownership
    6.1.4_etc_shadow_permissi [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/6.1.4_etc_shadow_permissions.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/6.1.5_etc_group_permissions.sh
    6.1.5_etc_group_permissio [INFO] Working on 6.1.5_etc_group_permissions
    6.1.5_etc_group_permissio [INFO] [DESCRIPTION] Check 644 permissions and root:root ownership on /etc/group
    6.1.5_etc_group_permissio [INFO] Checking Configuration
    6.1.5_etc_group_permissio [INFO] Performing audit
    6.1.5_etc_group_permissio [ OK ] /etc/group has correct permissions
    6.1.5_etc_group_permissio [ OK ] /etc/group has correct ownership
    6.1.5_etc_group_permissio [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/6.1.5_etc_group_permissions.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/6.1.6_etc_passwd-_permissions.sh
    6.1.6_etc_passwd-_permiss [INFO] Working on 6.1.6_etc_passwd-_permissions
    6.1.6_etc_passwd-_permiss [INFO] [DESCRIPTION] Check 600 permissions and root:root ownership on /etc/passwd-
    6.1.6_etc_passwd-_permiss [INFO] Checking Configuration
    6.1.6_etc_passwd-_permiss [INFO] Performing audit
    6.1.6_etc_passwd-_permiss [ OK ] /etc/passwd- has correct permissions
    6.1.6_etc_passwd-_permiss [ OK ] /etc/passwd- has correct ownership
    6.1.6_etc_passwd-_permiss [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/6.1.6_etc_passwd-_permissions.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/6.1.7_etc_shadow-_permissions.sh
    6.1.7_etc_shadow-_permiss [INFO] Working on 6.1.7_etc_shadow-_permissions
    6.1.7_etc_shadow-_permiss [INFO] [DESCRIPTION] Check 600 permissions and root:shadow ownership on /etc/shadow-
    6.1.7_etc_shadow-_permiss [INFO] Checking Configuration
    6.1.7_etc_shadow-_permiss [INFO] Performing audit
    6.1.7_etc_shadow-_permiss [ OK ] /etc/shadow- has correct permissions
    6.1.7_etc_shadow-_permiss [ OK ] /etc/shadow- has correct ownership
    6.1.7_etc_shadow-_permiss [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/6.1.7_etc_shadow-_permissions.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/6.1.8_etc_group-_permissions.sh
    6.1.8_etc_group-_permissi [INFO] Working on 6.1.8_etc_group-_permissions
    6.1.8_etc_group-_permissi [INFO] [DESCRIPTION] Check 600 permissions and root:root ownership on /etc/group-
    6.1.8_etc_group-_permissi [INFO] Checking Configuration
    6.1.8_etc_group-_permissi [INFO] Performing audit
    6.1.8_etc_group-_permissi [ OK ] /etc/group- has correct permissions
    6.1.8_etc_group-_permissi [ OK ] /etc/group- has correct ownership
    6.1.8_etc_group-_permissi [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/6.1.8_etc_group-_permissions.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/6.1.9_etc_gshadow_permissions.sh
    6.1.9_etc_gshadow_permiss [INFO] Working on 6.1.9_etc_gshadow_permissions
    6.1.9_etc_gshadow_permiss [INFO] [DESCRIPTION] Check 640 permissions and root:root ownership on /etc/gshadow
    6.1.9_etc_gshadow_permiss [INFO] Checking Configuration
    6.1.9_etc_gshadow_permiss [INFO] Performing audit
    6.1.9_etc_gshadow_permiss [ OK ] /etc/gshadow has correct permissions
    6.1.9_etc_gshadow_permiss [ OK ] /etc/gshadow has correct ownership
    6.1.9_etc_gshadow_permiss [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/6.1.9_etc_gshadow_permissions.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/6.1.10_find_world_writable_file.sh
    6.1.10_find_world_writabl [INFO] Working on 6.1.10_find_world_writable_file
    6.1.10_find_world_writabl [INFO] [DESCRIPTION] Ensure no world writable files exist
    6.1.10_find_world_writabl [INFO] Checking Configuration
    6.1.10_find_world_writabl [INFO] Performing audit
    6.1.10_find_world_writabl [INFO] Checking if there are world writable files
    6.1.10_find_world_writabl [ OK ] No world writable files found
    6.1.10_find_world_writabl [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/6.1.10_find_world_writable_file.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/6.1.11_find_unowned_files.sh
    6.1.11_find_unowned_files [INFO] Working on 6.1.11_find_unowned_files
    6.1.11_find_unowned_files [INFO] [DESCRIPTION] Ensure no unowned files or directories exist.
    6.1.11_find_unowned_files [INFO] Checking Configuration
    6.1.11_find_unowned_files [INFO] Performing audit
    6.1.11_find_unowned_files [INFO] Checking if there are unowned files
    6.1.11_find_unowned_files [ OK ] No unowned files found
    6.1.11_find_unowned_files [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/6.1.11_find_unowned_files.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/6.1.12_find_ungrouped_files.sh
    6.1.12_find_ungrouped_fil [INFO] Working on 6.1.12_find_ungrouped_files
    6.1.12_find_ungrouped_fil [INFO] [DESCRIPTION] Ensure no ungrouped files or directories exist
    6.1.12_find_ungrouped_fil [INFO] Checking Configuration
    6.1.12_find_ungrouped_fil [INFO] Performing audit
    6.1.12_find_ungrouped_fil [INFO] Checking if there are ungrouped files
    6.1.12_find_ungrouped_fil [ OK ] No ungrouped files found
    6.1.12_find_ungrouped_fil [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/6.1.12_find_ungrouped_files.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/6.1.13_find_suid_files.sh
    6.1.13_find_suid_files    [INFO] Working on 6.1.13_find_suid_files
    6.1.13_find_suid_files    [INFO] [DESCRIPTION] Find SUID system executables.
    6.1.13_find_suid_files    [INFO] Checking Configuration
    6.1.13_find_suid_files    [INFO] Performing audit
    6.1.13_find_suid_files    [INFO] Checking if there are suid files
    6.1.13_find_suid_files    [ KO ] Some suid files are present
    6.1.13_find_suid_files    [ KO ]  /usr/lib/dbus-1.0/dbus-daemon-launch-helper 
    6.1.13_find_suid_files    [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/6.1.14_find_sgid_files.sh
    6.1.14_find_sgid_files    [INFO] Working on 6.1.14_find_sgid_files
    6.1.14_find_sgid_files    [INFO] [DESCRIPTION] Find SGID system executables.
    6.1.14_find_sgid_files    [INFO] Checking Configuration
    6.1.14_find_sgid_files    [INFO] Performing audit
    6.1.14_find_sgid_files    [INFO] Checking if there are sgid files
    6.1.14_find_sgid_files    [ OK ] No unknown sgid files found
    6.1.14_find_sgid_files    [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/6.1.14_find_sgid_files.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/6.2.1_remove_empty_password_field.sh
    6.2.1_remove_empty_passwo [INFO] Working on 6.2.1_remove_empty_password_field
    6.2.1_remove_empty_passwo [INFO] [DESCRIPTION] Ensure password fields are not empty in /etc/shadow.
    6.2.1_remove_empty_passwo [INFO] Checking Configuration
    6.2.1_remove_empty_passwo [INFO] Performing audit
    6.2.1_remove_empty_passwo [INFO] Checking if accounts have an empty password
    6.2.1_remove_empty_passwo [ OK ] All accounts have a password
    6.2.1_remove_empty_passwo [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/6.2.1_remove_empty_password_field.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/6.2.2_remove_legacy_passwd_entries.sh
    6.2.2_remove_legacy_passw [INFO] Working on 6.2.2_remove_legacy_passwd_entries
    6.2.2_remove_legacy_passw [INFO] [DESCRIPTION] Verify no legacy + entries exist in /etc/password file.
    6.2.2_remove_legacy_passw [INFO] Checking Configuration
    6.2.2_remove_legacy_passw [INFO] Performing audit
    6.2.2_remove_legacy_passw [INFO] Checking if accounts have a legacy password entry
    6.2.2_remove_legacy_passw [ OK ] All accounts have a valid password entry format
    6.2.2_remove_legacy_passw [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/6.2.2_remove_legacy_passwd_entries.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/6.2.3_users_homedir_exist.sh
    6.2.3_users_homedir_exist [INFO] Working on 6.2.3_users_homedir_exist
    6.2.3_users_homedir_exist [INFO] [DESCRIPTION] Users are assigned valid home directories.
    6.2.3_users_homedir_exist [INFO] Checking Configuration
    6.2.3_users_homedir_exist [INFO] Performing audit
    6.2.3_users_homedir_exist [ OK ] All home directories exists
    6.2.3_users_homedir_exist [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/6.2.3_users_homedir_exist.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/6.2.4_remove_legacy_shadow_entries.sh
    6.2.4_remove_legacy_shado [INFO] Working on 6.2.4_remove_legacy_shadow_entries
    6.2.4_remove_legacy_shado [INFO] [DESCRIPTION] Verify no legacy + entries exist in /etc/shadow file.
    6.2.4_remove_legacy_shado [INFO] Checking Configuration
    6.2.4_remove_legacy_shado [INFO] Performing audit
    6.2.4_remove_legacy_shado [INFO] Checking if accounts have a legacy password entry
    6.2.4_remove_legacy_shado [ OK ] All accounts have a valid password entry format
    6.2.4_remove_legacy_shado [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/6.2.4_remove_legacy_shadow_entries.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/6.2.5_remove_legacy_group_entries.sh
    6.2.5_remove_legacy_group [INFO] Working on 6.2.5_remove_legacy_group_entries
    6.2.5_remove_legacy_group [INFO] [DESCRIPTION] Verify no legacy + entries exist in /etc/group file.
    6.2.5_remove_legacy_group [INFO] Checking Configuration
    6.2.5_remove_legacy_group [INFO] Performing audit
    6.2.5_remove_legacy_group [INFO] Checking if accounts have a legacy group entry
    6.2.5_remove_legacy_group [ OK ] All accounts have a valid group entry format
    6.2.5_remove_legacy_group [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/6.2.5_remove_legacy_group_entries.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/6.2.6_find_0_uid_non_root_account.sh
    6.2.6_find_0_uid_non_root [INFO] Working on 6.2.6_find_0_uid_non_root_account
    6.2.6_find_0_uid_non_root [INFO] [DESCRIPTION] Verify root is the only UID 0 account.
    6.2.6_find_0_uid_non_root [INFO] Checking Configuration
    6.2.6_find_0_uid_non_root [INFO] Performing audit
    6.2.6_find_0_uid_non_root [INFO] Checking if accounts have uid 0
    6.2.6_find_0_uid_non_root [ OK ] No account with uid 0 appart from root 
    6.2.6_find_0_uid_non_root [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/6.2.6_find_0_uid_non_root_account.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/6.2.7_sanitize_root_path.sh
    6.2.7_sanitize_root_path  [INFO] Working on 6.2.7_sanitize_root_path
    6.2.7_sanitize_root_path  [INFO] [DESCRIPTION] Ensure root path integrity.
    6.2.7_sanitize_root_path  [INFO] Checking Configuration
    6.2.7_sanitize_root_path  [INFO] Performing audit
    6.2.7_sanitize_root_path  [ OK ] root PATH is secure
    6.2.7_sanitize_root_path  [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/6.2.7_sanitize_root_path.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/6.2.8_check_user_dir_perm.sh
    6.2.8_check_user_dir_perm [INFO] Working on 6.2.8_check_user_dir_perm
    6.2.8_check_user_dir_perm [INFO] [DESCRIPTION] Check permissions on user home directories.
    6.2.8_check_user_dir_perm [INFO] Checking Configuration
    6.2.8_check_user_dir_perm [INFO] Performing audit
    6.2.8_check_user_dir_perm [ KO ] Other Read permission set on directory /home/vagrant
    6.2.8_check_user_dir_perm [ KO ] Other Execute permission set on directory /home/vagrant
    6.2.8_check_user_dir_perm [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/6.2.9_users_homedir_ownership.sh
    6.2.9_users_homedir_owner [INFO] Working on 6.2.9_users_homedir_ownership
    6.2.9_users_homedir_owner [INFO] [DESCRIPTION] Ensure users own their home directories
    6.2.9_users_homedir_owner [INFO] Checking Configuration
    6.2.9_users_homedir_owner [INFO] Performing audit
    6.2.9_users_homedir_owner [ OK ] All home directories have correct ownership
    6.2.9_users_homedir_owner [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/6.2.9_users_homedir_ownership.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/6.2.10_check_user_dot_file_perm.sh
    6.2.10_check_user_dot_fil [INFO] Working on 6.2.10_check_user_dot_file_perm
    6.2.10_check_user_dot_fil [INFO] [DESCRIPTION] Check user dot file permissions.
    6.2.10_check_user_dot_fil [INFO] Checking Configuration
    6.2.10_check_user_dot_fil [INFO] Performing audit
    6.2.10_check_user_dot_fil [ OK ] Dot file permission in users directories are correct
    6.2.10_check_user_dot_fil [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/6.2.10_check_user_dot_file_perm.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/6.2.11_find_user_forward_files.sh
    6.2.11_find_user_forward_ [INFO] Working on 6.2.11_find_user_forward_files
    6.2.11_find_user_forward_ [INFO] [DESCRIPTION] There is no user .forward files.
    6.2.11_find_user_forward_ [INFO] Checking Configuration
    6.2.11_find_user_forward_ [INFO] Performing audit
    6.2.11_find_user_forward_ [ OK ] No .forward present in users home directory
    6.2.11_find_user_forward_ [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/6.2.11_find_user_forward_files.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/6.2.12_find_user_netrc_files.sh
    6.2.12_find_user_netrc_fi [INFO] Working on 6.2.12_find_user_netrc_files
    6.2.12_find_user_netrc_fi [INFO] [DESCRIPTION] There is no user .netrc files.
    6.2.12_find_user_netrc_fi [INFO] Checking Configuration
    6.2.12_find_user_netrc_fi [INFO] Performing audit
    6.2.12_find_user_netrc_fi [ OK ] No .netrc present in users home directory
    6.2.12_find_user_netrc_fi [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/6.2.12_find_user_netrc_files.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/6.2.13_set_perm_on_user_netrc.sh
    6.2.13_set_perm_on_user_n [INFO] Working on 6.2.13_set_perm_on_user_netrc
    6.2.13_set_perm_on_user_n [INFO] [DESCRIPTION] Ensure users' .netrc Files are not group or world accessible
    6.2.13_set_perm_on_user_n [INFO] Checking Configuration
    6.2.13_set_perm_on_user_n [INFO] Performing audit
    6.2.13_set_perm_on_user_n [ OK ] permission 600 set on .netrc users files
    6.2.13_set_perm_on_user_n [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/6.2.13_set_perm_on_user_netrc.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/6.2.14_find_user_rhosts_files.sh
    6.2.14_find_user_rhosts_f [INFO] Working on 6.2.14_find_user_rhosts_files
    6.2.14_find_user_rhosts_f [INFO] [DESCRIPTION] No user's .rhosts file.
    6.2.14_find_user_rhosts_f [INFO] Checking Configuration
    6.2.14_find_user_rhosts_f [INFO] Performing audit
    6.2.14_find_user_rhosts_f [ OK ] No .rhosts present in users home directory
    6.2.14_find_user_rhosts_f [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/6.2.14_find_user_rhosts_files.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/6.2.15_find_passwd_group_inconsistencies.sh
    6.2.15_find_passwd_group_ [INFO] Working on 6.2.15_find_passwd_group_inconsistencies
    6.2.15_find_passwd_group_ [INFO] [DESCRIPTION] There is no group in /etc/passwd that is not in /etc/group.
    6.2.15_find_passwd_group_ [INFO] Checking Configuration
    6.2.15_find_passwd_group_ [INFO] Performing audit
    6.2.15_find_passwd_group_ [ OK ] passwd and group Groups are consistent
    6.2.15_find_passwd_group_ [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/6.2.15_find_passwd_group_inconsistencies.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/6.2.16_check_duplicate_uid.sh
    6.2.16_check_duplicate_ui [INFO] Working on 6.2.16_check_duplicate_uid
    6.2.16_check_duplicate_ui [INFO] [DESCRIPTION] Ensure no duplicate UIDs exist
    6.2.16_check_duplicate_ui [INFO] Checking Configuration
    6.2.16_check_duplicate_ui [INFO] Performing audit
    6.2.16_check_duplicate_ui [ OK ] No duplicate UIDs
    6.2.16_check_duplicate_ui [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/6.2.16_check_duplicate_uid.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/6.2.17_check_duplicate_gid.sh
    6.2.17_check_duplicate_gi [INFO] Working on 6.2.17_check_duplicate_gid
    6.2.17_check_duplicate_gi [INFO] [DESCRIPTION] Ensure no duplicate GIDs exist
    6.2.17_check_duplicate_gi [INFO] Checking Configuration
    6.2.17_check_duplicate_gi [INFO] Performing audit
    6.2.17_check_duplicate_gi [ OK ] No duplicate GIDs
    6.2.17_check_duplicate_gi [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/6.2.17_check_duplicate_gid.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/6.2.18_check_duplicate_username.sh
    6.2.18_check_duplicate_us [INFO] Working on 6.2.18_check_duplicate_username
    6.2.18_check_duplicate_us [INFO] [DESCRIPTION] There is no duplicate usernames.
    6.2.18_check_duplicate_us [INFO] Checking Configuration
    6.2.18_check_duplicate_us [INFO] Performing audit
    6.2.18_check_duplicate_us [ OK ] No duplicate usernames
    6.2.18_check_duplicate_us [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/6.2.18_check_duplicate_username.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/6.2.19_check_duplicate_groupname.sh
    6.2.19_check_duplicate_gr [INFO] Working on 6.2.19_check_duplicate_groupname
    6.2.19_check_duplicate_gr [INFO] [DESCRIPTION] There is no duplicate group names.
    6.2.19_check_duplicate_gr [INFO] Checking Configuration
    6.2.19_check_duplicate_gr [INFO] Performing audit
    6.2.19_check_duplicate_gr [ OK ] No duplicate groupnames
    6.2.19_check_duplicate_gr [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/6.2.19_check_duplicate_groupname.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/6.2.20_shadow_group_empty.sh
    6.2.20_shadow_group_empty [INFO] Working on 6.2.20_shadow_group_empty
    6.2.20_shadow_group_empty [INFO] [DESCRIPTION] There is no user in shadow group (that can read /etc/shadow file).
    6.2.20_shadow_group_empty [INFO] Checking Configuration
    6.2.20_shadow_group_empty [INFO] Performing audit
    6.2.20_shadow_group_empty [INFO] shadow group exists
    6.2.20_shadow_group_empty [ OK ] No user belongs to shadow group
    6.2.20_shadow_group_empty [INFO] Checking if a user has 42 as primary group
    6.2.20_shadow_group_empty [ OK ] No user has shadow id as their primary group
    6.2.20_shadow_group_empty [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/6.2.20_shadow_group_empty.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/99.1.1.1_disable_cramfs.sh
    99.1.1.1_disable_cramfs   [INFO] Working on 99.1.1.1_disable_cramfs
    99.1.1.1_disable_cramfs   [INFO] [DESCRIPTION] Disable mounting of cramfs filesystems.
    99.1.1.1_disable_cramfs   [INFO] Checking Configuration
    99.1.1.1_disable_cramfs   [INFO] Performing audit
    99.1.1.1_disable_cramfs   [ OK ] CONFIG_CRAMFS is disabled
    99.1.1.1_disable_cramfs   [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/99.1.1.1_disable_cramfs.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/99.1.1.23_disable_usb_devices.sh
    99.1.1.23_disable_usb_dev [INFO] Working on 99.1.1.23_disable_usb_devices
    99.1.1.23_disable_usb_dev [INFO] [DESCRIPTION] USB devices are disabled.
    99.1.1.23_disable_usb_dev [INFO] Checking Configuration
    99.1.1.23_disable_usb_dev [INFO] Performing audit
    99.1.1.23_disable_usb_dev [ KO ] ACTION=="add", SUBSYSTEMS=="usb", TEST=="authorized_default", ATTR{authorized_default}="0" is not present in /etc/udev/rules.d
    99.1.1.23_disable_usb_dev [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/99.1.3_acc_sudoers_no_all.sh
    99.1.3_acc_sudoers_no_all [INFO] Working on 99.1.3_acc_sudoers_no_all
    99.1.3_acc_sudoers_no_all [INFO] [DESCRIPTION] Checks there are no carte-blanche authorization in sudoers file(s).
    99.1.3_acc_sudoers_no_all [INFO] Checking Configuration
    99.1.3_acc_sudoers_no_all [INFO] Performing audit
    99.1.3_acc_sudoers_no_all [INFO] Will check for 4 files within /etc/sudoers.d
    99.1.3_acc_sudoers_no_all [ OK ] root ALL=(ALL:ALL) ALL is present in /etc/sudoers but was EXCUSED because root is part of exceptions.
    99.1.3_acc_sudoers_no_all [ OK ] %sudo ALL=(ALL:ALL) ALL is present in /etc/sudoers but was EXCUSED because %sudo is part of exceptions.
    99.1.3_acc_sudoers_no_all [ KO ] vagrant ALL=(ALL) NOPASSWD:ALL is present in /etc/sudoers.d/vagrant
    99.1.3_acc_sudoers_no_all [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/99.2.2_disable_telnet_server.sh
    99.2.2_disable_telnet_ser [INFO] Working on 99.2.2_disable_telnet_server
    99.2.2_disable_telnet_ser [INFO] [DESCRIPTION] Ensure telnet server is not enabled. Recommended alternative : sshd (OpenSSH-server).
    99.2.2_disable_telnet_ser [INFO] Checking Configuration
    99.2.2_disable_telnet_ser [INFO] Performing audit
    99.2.2_disable_telnet_ser [ OK ] telnetd is absent
    99.2.2_disable_telnet_ser [ OK ] inetutils-telnetd is absent
    99.2.2_disable_telnet_ser [ OK ] telnetd-ssl is absent
    99.2.2_disable_telnet_ser [ OK ] krb5-telnetd is absent
    99.2.2_disable_telnet_ser [ OK ] heimdal-servers is absent
    99.2.2_disable_telnet_ser [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/99.2.2_disable_telnet_server.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/99.3.3.1_install_tcp_wrapper.sh
    99.3.3.1_install_tcp_wrap [INFO] Working on 99.3.3.1_install_tcp_wrapper
    99.3.3.1_install_tcp_wrap [INFO] [DESCRIPTION] Install TCP wrappers for simple access list management and standardized logging method for services.
    99.3.3.1_install_tcp_wrap [INFO] Checking Configuration
    99.3.3.1_install_tcp_wrap [INFO] Performing audit
    99.3.3.1_install_tcp_wrap [ KO ] tcpd is not installed!
    99.3.3.1_install_tcp_wrap [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/99.3.3.2_hosts_allow.sh
    99.3.3.2_hosts_allow      [INFO] Working on 99.3.3.2_hosts_allow
    99.3.3.2_hosts_allow      [INFO] [DESCRIPTION] Create /etc/hosts.allow .
    99.3.3.2_hosts_allow      [INFO] Checking Configuration
    99.3.3.2_hosts_allow      [INFO] Performing audit
    99.3.3.2_hosts_allow      [ OK ] /etc/hosts.allow exist
    99.3.3.2_hosts_allow      [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/99.3.3.2_hosts_allow.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/99.3.3.3_hosts_deny.sh
    99.3.3.3_hosts_deny       [INFO] Working on 99.3.3.3_hosts_deny
    99.3.3.3_hosts_deny       [INFO] [DESCRIPTION] Create /etc/hosts.deny .
    99.3.3.3_hosts_deny       [INFO] Checking Configuration
    99.3.3.3_hosts_deny       [INFO] Performing audit
    99.3.3.3_hosts_deny       [ OK ] /etc/hosts.deny exists, checking configuration
    99.3.3.3_hosts_deny       [ KO ] ALL: ALL is not present in /etc/hosts.deny, we have to deny everything
    99.3.3.3_hosts_deny       [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/99.3.3.4_hosts_allow_permissions.sh
    99.3.3.4_hosts_allow_perm [INFO] Working on 99.3.3.4_hosts_allow_permissions
    99.3.3.4_hosts_allow_perm [INFO] [DESCRIPTION] Check 644 permissions and root:root ownership on /hosts.allow .
    99.3.3.4_hosts_allow_perm [INFO] Checking Configuration
    99.3.3.4_hosts_allow_perm [INFO] Performing audit
    99.3.3.4_hosts_allow_perm [ OK ] /etc/hosts.allow exist
    99.3.3.4_hosts_allow_perm [ OK ] /etc/hosts.allow has correct permissions
    99.3.3.4_hosts_allow_perm [ OK ] /etc/hosts.allow has correct ownership
    99.3.3.4_hosts_allow_perm [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/99.3.3.4_hosts_allow_permissions.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/99.3.3.5_hosts_deny_permissions.sh
    99.3.3.5_hosts_deny_permi [INFO] Working on 99.3.3.5_hosts_deny_permissions
    99.3.3.5_hosts_deny_permi [INFO] [DESCRIPTION] Check 644 permissions and root:root ownership on /etc/hosts.deny .
    99.3.3.5_hosts_deny_permi [INFO] Checking Configuration
    99.3.3.5_hosts_deny_permi [INFO] Performing audit
    99.3.3.5_hosts_deny_permi [ OK ] /etc/hosts.deny exist
    99.3.3.5_hosts_deny_permi [ OK ] /etc/hosts.deny has correct permissions
    99.3.3.5_hosts_deny_permi [ OK ] /etc/hosts.deny has correct ownership
    99.3.3.5_hosts_deny_permi [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/99.3.3.5_hosts_deny_permissions.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/99.4.0_enable_auditd_kernel.sh
    99.4.0_enable_auditd_kern [INFO] Working on 99.4.0_enable_auditd_kernel
    99.4.0_enable_auditd_kern [INFO] [DESCRIPTION] Ensure CONFIG_AUDIT is enabled in your running kernel.
    99.4.0_enable_auditd_kern [INFO] Checking Configuration
    99.4.0_enable_auditd_kern [INFO] Performing audit
    99.4.0_enable_auditd_kern [ OK ] CONFIG_AUDIT is enabled
    99.4.0_enable_auditd_kern [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/99.4.0_enable_auditd_kernel.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/99.5.2.1_ssh_auth_pubk_only.sh
    99.5.2.1_ssh_auth_pubk_on [INFO] Working on 99.5.2.1_ssh_auth_pubk_only
    99.5.2.1_ssh_auth_pubk_on [INFO] [DESCRIPTION] Ensure that sshd only allows authentication through public key.
    99.5.2.1_ssh_auth_pubk_on [INFO] Checking Configuration
    99.5.2.1_ssh_auth_pubk_on [INFO] Performing audit
    99.5.2.1_ssh_auth_pubk_on [ OK ] openssh-server is installed
    99.5.2.1_ssh_auth_pubk_on [ KO ] ^PubkeyAuthentication[[:space:]]+yes is not present in /etc/ssh/sshd_config
    99.5.2.1_ssh_auth_pubk_on [ OK ] ^PasswordAuthentication[[:space:]]+no is present in /etc/ssh/sshd_config
    99.5.2.1_ssh_auth_pubk_on [ OK ] ^KbdInteractiveAuthentication[[:space:]]+no is present in /etc/ssh/sshd_config
    99.5.2.1_ssh_auth_pubk_on [ KO ] ^KerberosAuthentication[[:space:]]+no is not present in /etc/ssh/sshd_config
    99.5.2.1_ssh_auth_pubk_on [ KO ] ^ChallengeResponseAuthentication[[:space:]]+no is not present in /etc/ssh/sshd_config
    99.5.2.1_ssh_auth_pubk_on [ KO ] ^HostbasedAuthentication[[:space:]]+no is not present in /etc/ssh/sshd_config
    99.5.2.1_ssh_auth_pubk_on [ KO ] ^GSSAPIAuthentication[[:space:]]+no is not present in /etc/ssh/sshd_config
    99.5.2.1_ssh_auth_pubk_on [ KO ] ^GSSAPIKeyExchange[[:space:]]+no is not present in /etc/ssh/sshd_config
    99.5.2.1_ssh_auth_pubk_on [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/99.5.2.2_ssh_cry_rekey.sh
    99.5.2.2_ssh_cry_rekey    [INFO] Working on 99.5.2.2_ssh_cry_rekey
    99.5.2.2_ssh_cry_rekey    [INFO] [DESCRIPTION] Checking rekey limit for time (6 hours) or volume (512Mio) whichever comes first.
    99.5.2.2_ssh_cry_rekey    [INFO] Checking Configuration
    99.5.2.2_ssh_cry_rekey    [INFO] Performing audit
    99.5.2.2_ssh_cry_rekey    [ OK ] openssh-server is installed
    99.5.2.2_ssh_cry_rekey    [ KO ] ^RekeyLimit[[:space:]]*512M\s+6h is not present in /etc/ssh/sshd_config
    99.5.2.2_ssh_cry_rekey    [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/99.5.2.3_ssh_disable_features.sh
    99.5.2.3_ssh_disable_feat [INFO] Working on 99.5.2.3_ssh_disable_features
    99.5.2.3_ssh_disable_feat [INFO] [DESCRIPTION] Check all special features in sshd_config are disabled
    99.5.2.3_ssh_disable_feat [INFO] Checking Configuration
    99.5.2.3_ssh_disable_feat [INFO] Performing audit
    99.5.2.3_ssh_disable_feat [ OK ] openssh-server is installed
    99.5.2.3_ssh_disable_feat [ KO ] ^AllowAgentForwarding[[:space:]]*no is not present in /etc/ssh/sshd_config
    99.5.2.3_ssh_disable_feat [ KO ] ^AllowTcpForwarding[[:space:]]*no is not present in /etc/ssh/sshd_config
    99.5.2.3_ssh_disable_feat [ KO ] ^AllowStreamLocalForwarding[[:space:]]*no is not present in /etc/ssh/sshd_config
    99.5.2.3_ssh_disable_feat [ KO ] ^PermitTunnel[[:space:]]*no is not present in /etc/ssh/sshd_config
    99.5.2.3_ssh_disable_feat [ KO ] ^PermitUserRC[[:space:]]*no is not present in /etc/ssh/sshd_config
    99.5.2.3_ssh_disable_feat [ KO ] ^GatewayPorts[[:space:]]*no is not present in /etc/ssh/sshd_config
    99.5.2.3_ssh_disable_feat [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/99.5.2.4_ssh_keys_from.sh
    99.5.2.4_ssh_keys_from    [INFO] Working on 99.5.2.4_ssh_keys_from
    99.5.2.4_ssh_keys_from    [INFO] [DESCRIPTION] Check <from> field in ssh authorized keys files for users with login shell, and allowed IP if available.
    99.5.2.4_ssh_keys_from    [INFO] Checking Configuration
    99.5.2.4_ssh_keys_from    [INFO] Performing audit
    99.5.2.4_ssh_keys_from    [INFO] User root has a valid shell (/bin/bash).
    99.5.2.4_ssh_keys_from    [INFO] User sync has a valid shell (/bin/sync).
    99.5.2.4_ssh_keys_from    [INFO] User sync has no home directory.
    99.5.2.4_ssh_keys_from    [INFO] User vagrant has a valid shell (/bin/bash).
    99.5.2.4_ssh_keys_from    [ KO ] There are anywhere access keys in /home/vagrant/.ssh/authorized_keys at lines (1).
    99.5.2.4_ssh_keys_from    [ KO ] Check Failed
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/99.5.2.5_ssh_strict_modes.sh
    99.5.2.5_ssh_strict_modes [INFO] Working on 99.5.2.5_ssh_strict_modes
    99.5.2.5_ssh_strict_modes [INFO] [DESCRIPTION] Ensure home directory and ssh sensitive files are verified (not publicly readable) before connecting.
    99.5.2.5_ssh_strict_modes [INFO] Checking Configuration
    99.5.2.5_ssh_strict_modes [INFO] Performing audit
    99.5.2.5_ssh_strict_modes [ OK ] openssh-server is installed
    99.5.2.5_ssh_strict_modes [ OK ] ^StrictModes[[:space:]]*yes is present in /etc/ssh/sshd_config
    99.5.2.5_ssh_strict_modes [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/99.5.2.5_ssh_strict_modes.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/99.5.2.6_ssh_sys_accept_env.sh
    99.5.2.6_ssh_sys_accept_e [INFO] Working on 99.5.2.6_ssh_sys_accept_env
    99.5.2.6_ssh_sys_accept_e [INFO] [DESCRIPTION] Restrict which user's variables are accepted by ssh daemon
    99.5.2.6_ssh_sys_accept_e [INFO] Checking Configuration
    99.5.2.6_ssh_sys_accept_e [INFO] Performing audit
    99.5.2.6_ssh_sys_accept_e [ OK ] openssh-server is installed
    99.5.2.6_ssh_sys_accept_e [ OK ] ^\s*AcceptEnv\s+LANG LC_\* is present in /etc/ssh/sshd_config
    99.5.2.6_ssh_sys_accept_e [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/99.5.2.6_ssh_sys_accept_env.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/99.5.2.7_ssh_sys_no_legacy.sh
    99.5.2.7_ssh_sys_no_legac [INFO] Working on 99.5.2.7_ssh_sys_no_legacy
    99.5.2.7_ssh_sys_no_legac [INFO] [DESCRIPTION] Ensure that legacy services rlogin, rlogind and rcp are disabled and not installed
    99.5.2.7_ssh_sys_no_legac [INFO] Checking Configuration
    99.5.2.7_ssh_sys_no_legac [INFO] Performing audit
    99.5.2.7_ssh_sys_no_legac [INFO] Checking if rlogin is enabled and installed
    99.5.2.7_ssh_sys_no_legac [ OK ] rlogin is disabled
    99.5.2.7_ssh_sys_no_legac [ OK ] rlogin is not installed
    99.5.2.7_ssh_sys_no_legac [INFO] Checking if rlogind is enabled and installed
    99.5.2.7_ssh_sys_no_legac [ OK ] rlogind is disabled
    99.5.2.7_ssh_sys_no_legac [ OK ] rlogind is not installed
    99.5.2.7_ssh_sys_no_legac [INFO] Checking if rcp is enabled and installed
    99.5.2.7_ssh_sys_no_legac [ OK ] rcp is disabled
    99.5.2.7_ssh_sys_no_legac [ OK ] rcp is not installed
    99.5.2.7_ssh_sys_no_legac [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/99.5.2.7_ssh_sys_no_legacy.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/99.5.4.5.1_acc_logindefs_sha512.sh
    99.5.4.5.1_acc_logindefs_ [INFO] Working on 99.5.4.5.1_acc_logindefs_sha512
    99.5.4.5.1_acc_logindefs_ [INFO] [DESCRIPTION] Check that any password that will be created will use sha512crypt (or yescrypt for Debian 11+)
    99.5.4.5.1_acc_logindefs_ [INFO] Checking Configuration
    99.5.4.5.1_acc_logindefs_ [INFO] Performing audit
    99.5.4.5.1_acc_logindefs_ [ OK ] ENCRYPT_METHOD (SHA512|yescrypt|YESCRYPT) is present in /etc/login.defs
    99.5.4.5.1_acc_logindefs_ [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/99.5.4.5.1_acc_logindefs_sha512.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/99.5.4.5.2_acc_shadow_sha512.sh
    99.5.4.5.2_acc_shadow_sha [INFO] Working on 99.5.4.5.2_acc_shadow_sha512
    99.5.4.5.2_acc_shadow_sha [INFO] [DESCRIPTION] Check that passwords in /etc/shadow are sha512crypt (or yescrypt for Debian 11+) hashed and salted
    99.5.4.5.2_acc_shadow_sha [INFO] Checking Configuration
    99.5.4.5.2_acc_shadow_sha [INFO] Performing audit
    99.5.4.5.2_acc_shadow_sha [ OK ] User root has suitable yescrypt hashed password.
    99.5.4.5.2_acc_shadow_sha [ OK ] User systemd-network has a disabled password.
    99.5.4.5.2_acc_shadow_sha [ OK ] User systemd-timesync has a disabled password.
    99.5.4.5.2_acc_shadow_sha [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/99.5.4.5.2_acc_shadow_sha512.cfg
    hardening                 [INFO] Treating /home/vagrant/debian-cis/bin/hardening/99.99_check_distribution.sh
    99.99_check_distribution  [INFO] Working on 99.99_check_distribution
    99.99_check_distribution  [INFO] [DESCRIPTION] Check the distribution and the distribution version
    99.99_check_distribution  [INFO] Checking Configuration
    99.99_check_distribution  [INFO] Performing audit
    99.99_check_distribution  [ OK ] Your distribution is debian and the version is supported
    99.99_check_distribution  [ OK ] Check Passed
    hardening                 [INFO] Status set to enabled in /home/vagrant/debian-cis/etc/conf.d/99.99_check_distribution.cfg
    ################### SUMMARY ###################
        Total Available Checks : 242
            Total Runned Checks : 242
            Total Passed Checks : [ 112/242 ]
            Total Failed Checks : [ 130/242 ]
    Enabled Checks Percentage : 100.00 %
        Conformity Percentage : 46.28 %   

.....

.. Rubric:: Footnotes

.. [1]
   Debian Linux
   https://www.debian.org
