******************************************************************************
Abstract - Notations Conventions - Abstrakt - Notationskonventionen
******************************************************************************

.. sectionauthor:: Pierre Gronau <Pierre.Gronau@ndaal.eu>

..   

.. contents:: Content Abstract - Notations Conventions - Inhalt Abstrakt - Notationskonventionen
   :depth: 3

..

.. Hint::
   - In the event of any discrepancies between the German and English versions
     or any other case of doubt, the German version shall prevail.
   - Bei Abweichungen zwischen der deutschen und der englischen Fassung oder bei
     sonstigen Zweifelsfällen gilt die deutsche Version.

.. _Abstract_Notations_Conventions:

Abstract - Notations Conventions
------------------------------------------------------------------------------

We are offering for your convenience a bilingual version of 
**Abstract - Notations Conventions**:

The following notations, defined in ISO/IEC 9646-7 [#f1]_ , are used for the 
qualifier column of interface information elements:

- **M mandatory** - the capability is required to be supported.
- **optional** - the capability MAY be supported or not.
- **N/A not applicable** - in the given context, it is impossible to use the capability. 
- **CM conditional mandatory** - the capability is required to be supported and is conditional on the support of some condition. This condition SHALL be specified in the Description column.
- **CO conditional optional** - the capability MAY be supported or not and is conditional on the support of some condition. This condition SHALL be specified in the Description column.

The following notation is used for parameters that represent **identifiers**, and for attributes that represent identifiers in information elements and notifications:

- If parameters are referring to an identifier of an actual object, their type is "**Identifier**".
- If an object (information element or notification) contains an attribute that identifies the object, the type of that attribute is "**Identifier**" and the description states that the attribute is the identifier of that particular notification or information element.

EXAMPLE: Identifier "*ResourceId*" of the "NetworkSubnet information element" has type "**Identifier**" and description "*Identifier of this NetworkSubnet information element*".

The key words "unspecified", "undefined", and "implementation-defined" are to be interpreted as described in the rationale for the C99 standard [#f2]_.

Abstrakt - Notationskonventionen
------------------------------------------------------------------------------

Die folgenden Notationen, die in ISO/IEC 9646-7 [#f1]_ definiert sind, werden für die 
Qualifier-Spalte von Schnittstellen-Informationselementen verwendet:

- **M mandatory** - die Fähigkeit MUSS unterstützt werden.
- **optional** - die Fähigkeit KANN unterstützt werden oder nicht.
- **N/A not applicable (nicht anwendbar)** - im gegebenen Kontext ist es unmöglich, die Fähigkeit zu verwenden. 
- **CM conditional mandatory** - die Fähigkeit MUSS unterstützt werden und ist von der Unterstützung einer Bedingung abhängig. Diese Bedingung MUSS in der Spalte Beschreibung angegeben werden.
- **CO conditional optional** - die Fähigkeit KANN unterstützt werden oder nicht und hängt von der Unterstützung einer bestimmten Bedingung ab. Diese Bedingung MUSS in der Spalte "Beschreibung" angegeben werden.

Die folgende Notation wird für Parameter verwendet, die **Identifikatoren** darstellen, sowie für Attribute, die Identifikatoren in Informationselementen und Meldungen darstellen:

- Wenn sich Parameter auf einen Identifikator eines tatsächlichen Objekts beziehen, ist ihr Typ "**Identifikator**".
- Wenn ein Objekt (Informationselement oder Meldung) ein Attribut enthält, das das Objekt identifiziert, ist der Typ dieses Attributs "**Bezeichner**" und die Beschreibung besagt, dass das Attribut der Bezeichner dieser bestimmten Meldung oder dieses Informationselements ist.

BEISPIEL: Der Bezeichner "*ResourceId*" des "NetworkSubnet information element" hat den Typ "**Bezeichner**" und die Beschreibung "*Bezeichner dieses NetworkSubnet information element*".

Die Schlüsselwörter "unspezifiziert", "undefiniert" und "implementierungsdefiniert" sind wie in der Begründung zum C99-Standard [#f2]_ beschrieben zu interpretieren.


.....

.. Seealso:: 
   a) :ref:`Abstract - Minimum Requirement(s) - Abstrakt - Mindestanforderung(en) <Abstract_Minimum_Requirement>`
   #) :ref:`Abstract - Notations Conventions - Abstrakt - Notationskonventionen <Abstract_Notations_Conventions>`
   #) :ref:`Abstract - Verbal Forms - Abstrakt - Verbale Formen <Abstract_Verbal_Forms>`
   #) :ref:`Abstract - Version Information and Compatibility - Abstrakt – Versionsangaben und -kompatibilität <Abstract_Version>`

.....

.. Rubric:: Footnotes

.. [#f1] https://www.iso.org/standard/3084.html ISO/IEC 9646-7
.. [#f2] http://www.open-std.org/jtc1/sc22/wg14/www/C99RationaleV5.10.pdf page 18: Rationale for International Standard— Programming Languages— C

   
