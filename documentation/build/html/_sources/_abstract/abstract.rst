******************************************************************************
Abstract - Abstrakt
******************************************************************************

.. sectionauthor:: Pierre Gronau <Pierre.Gronau@ndaal.eu>

..

.. contents:: Content Abstract - Inhalt Abstrakt
    :depth: 3

.. _Section_Abstract:


We confirm some several words according to *IETF documents*
to specify Requirements.

In the next Chapter we clarify the meaning of Minimum Requirements according
to *Taguchi Gen’ichi Methods*.

Notations, defined in *ISO/IEC 9646-7* are used as well.

::

Wir bestätigen einige Begriffe aus den IETF-Dokumenten,
um Anforderungen zu spezifizieren.

Im nächsten Kapitel klären wir die Bedeutung der Mindestanforderungen
nach der Taguchi Gen'ichi-Methoden.

Es werden auch die in ISO/IEC 9646-7 definierten Bezeichnungen verwendet.

.. toctree::
   :maxdepth: 1
   :glob:
   :caption: Content of Abstract - Inhalt von Abstrakt

   ../_abstract/abstract_verbal_forms.rst
   ../_abstract/abstract_minimum_requirements.rst
   ../_abstract/abstract_notations_conventions.rst
   ../_abstract/abstract_version.rst

