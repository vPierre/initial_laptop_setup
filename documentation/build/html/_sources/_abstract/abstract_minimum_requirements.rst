******************************************************************************
Abstract - Minimum Requirement(s) - Abstrakt - Mindestanforderung(en)
******************************************************************************

.. sectionauthor:: Pierre Gronau <Pierre.Gronau@ndaal.eu>

..

.. contents:: Content Abstract - Minimum Requirement(s) - Inhalt Abstrakt - Mindestanforderung(en)
   :depth: 3

..

.. Hint::
   - In the event of any discrepancies between the German and English versions
     or any other case of doubt, the German version shall prevail.
   - Bei Abweichungen zwischen der deutschen und der englischen Fassung oder bei
     sonstigen Zweifelsfällen gilt die deutsche Version.

.. _Abstract_Minimum_Requirement:

Abstract - Minimum Requirement(s)
------------------------------------------------------------------------------

We are offering for your convenience a bilingual version of 
**Abstract - Minimum Requirement(s)**:

Minimum Requirements are generally defined as the Requirements for technical 
and non-technical systems, products, Services or processes that are required
to achieve predefined minimum objectives (minimum conditions). 
Minimum Requirements in the form of minimum objectives serve, for example, to 
achieve Security, legal certainty and process capability or to define customer 
Requirements that are necessary to achieve customer satisfaction.

::

The loss function according to Taguchi Gen'ichi [1]_ defines any deviation 
from defined target values, even within given tolerances, as monetary loss and 
thus defines the target value as the minimum Requirement (optimum).

::

If only the word "Requirement(s)" is mistakenly indicated in the document, 
this is to be understood and applied each time in the sense of this definition
of minimum Requirement(s).

Abstrakt - Mindestanforderung(en)
------------------------------------------------------------------------------

Als Mindestanforderungen bezeichnet man allgemein die Anforderungen an 
technische wie nichttechnische Systeme, Produkte, Dienstleistungen oder 
Prozesse, die zur Erreichung von vordefinierten Mindestzielen 
(Minimalbedingungen) erforderlich sind. Mindestanforderungen in Form von 
Mindestzielen dienen beispielsweise der Erlan- gung von Sicherheit, 
Rechtssicherheit und Prozessfähigkeit oder zur Definition von 
Kundenanforderungen, die zum Erreichen der Kundenzufriedenheit erforderlich 
sind.

::

Die Verlustfunktion nach Taguchi Gen’ichi definiert jede Abweichung von 
definierten Sollwerten auch innerhalb gegebener Toleranzen als geldwerten 
Verlust und definiert damit den Sollwert als Mindestanforderung (Optimum)

::

Falls irrtümlicherweise im Dokument nur das Wort „Anforderung(en)“ angegeben 
ist, so ist dies jedes Mal im Wort Sinne dieser Definition von 
Mindestanforderung(en) zu verstehen und anzuwenden.

.....

.. Seealso:: 
   a) :ref:`Abstract - Minimum Requirement(s) - Abstrakt - Mindestanforderung(en) <Abstract_Minimum_Requirement>`
   #) :ref:`Abstract - Notations Conventions - Abstrakt - Notationskonventionen <Abstract_Notations_Conventions>`
   #) :ref:`Abstract - Verbal Forms - Abstrakt - Verbale Formen <Abstract_Verbal_Forms>`
   #) :ref:`Abstract - Version Information and Compatibility - Abstrakt – Versionsangaben und -kompatibilität <Abstract_Version>`

.....

.. Rubric:: Footnotes

.. [1]
   https://en.wikipedia.org/wiki/Taguchi_methods Wikipedia *Taguchi Gen'ichi Methods*
