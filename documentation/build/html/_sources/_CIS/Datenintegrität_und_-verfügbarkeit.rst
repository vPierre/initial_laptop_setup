******************************************************************************
CIS Controls - CIS Kontrollen - Datenintegrität und -verfügbarkeit
******************************************************************************

.. sectionauthor:: Pierre Gronau <Pierre.Gronau@ndaal.eu>

..

.. contents:: Inhalt - CIS Kontrollen - Datenintegrität und -verfügbarkeit
    :depth: 3

.. _Section_CIS_Controls_Datenintegrität_und_-verfügbarkeit:

Datenintegrität und -verfügbarkeit
------------------------------------------------------------------------------

Dieses Kapitel beschäftigt sich mit zwei fundamentalen Zielen der Informationssicherheit: Datenintegrität und -verfügbarkeit. Während sich viele vorhergehende Kapitel mit der *Vertraulichkeit* von Daten und dem Schutz vor *unbefugtem Zugriff* befassten, geht es hier darum, sicherzustellen, dass Daten *korrekt*, *vollständig* und *jederzeit verfügbar* sind, wenn sie benötigt werden. Datenintegrität und -verfügbarkeit sind *nicht* direkt als separate Controls in den CIS Controls v8.1 aufgeführt, aber sie sind *implizit* in *mehreren* Controls enthalten und bilden *wesentliche Voraussetzungen* für die *Wirksamkeit* *aller* anderen Sicherheitsmaßnahmen. Ohne *zuverlässige* Daten und *verfügbare* Systeme ist jede Sicherheitsstrategie *unvollständig*.

*Datenintegrität* bedeutet, dass Daten *korrekt*, *vollständig* und *unverändert* sind. Sie müssen *vor unbeabsichtigten* oder *böswilligen Änderungen* geschützt werden. *Unbeabsichtigte Änderungen* können beispielsweise durch *menschliches Versagen*, *Hardwarefehler*, *Softwarefehler* oder *Übertragungsfehler* verursacht werden. *Böswillige Änderungen* können beispielsweise durch *Schadsoftware*, *Hackerangriffe* oder *Insider-Bedrohungen* verursacht werden.

*Datenverfügbarkeit* bedeutet, dass Daten und Systeme *jederzeit verfügbar* sind, wenn sie *benötigt* werden. Sie müssen vor *Ausfällen* und *Störungen* geschützt werden. Ausfälle können beispielsweise durch *Hardwareausfälle*, *Softwarefehler*, *Netzprobleme*, *Stromausfälle* oder *Naturkatastrophen* verursacht werden. Störungen können beispielsweise durch *Denial-of-Service-Angriffe*, *Schadsoftware* oder *menschliches Versagen* verursacht werden.

Stellen Sie sich vor, ein Unternehmen speichert *wichtige Finanzdaten* in einer Datenbank. Wenn die *Datenintegrität* *nicht gewährleistet* ist, könnten die Daten *falsch* oder *unvollständig* sein. Dies könnte zu *falschen Entscheidungen*, *finanziellen Verlusten* oder *rechtlichen Problemen* führen. Wenn die *Datenverfügbarkeit* *nicht gewährleistet* ist, könnte die Datenbank *nicht zugänglich* sein, wenn sie *benötigt* wird. Dies könnte zu *Betriebsunterbrechungen*, *Produktivitätsverlusten* und *Reputationsschäden* führen.

Datenintegrität und -verfügbarkeit sind *nicht nur* für *sensible* oder *geschäftskritische* Daten wichtig, sondern für *alle* Daten. Selbst *scheinbar unwichtige* Daten können *wertvoll* sein, wenn sie *korrekt* und *verfügbar* sind. Beispielsweise könnten *Protokolldateien* *unwichtig* erscheinen, aber sie können *entscheidend* sein, um *Sicherheitsvorfälle* zu *untersuchen* oder *Systemprobleme* zu *diagnostizieren*.

Die Gewährleistung von Datenintegrität und -verfügbarkeit erfordert einen *ganzheitlichen Ansatz*, der *technische*, *organisatorische* und *prozessuale* Maßnahmen umfasst. Es reicht *nicht* aus, *einzelne Tools* oder *Technologien* zu implementieren. Es ist *notwendig*, ein *umfassendes Konzept* zu entwickeln, das alle *Aspekte* der Datenverarbeitung und -speicherung berücksichtigt.

Ein *wichtiger Aspekt* der Datenintegrität ist die *Eingabevalidierung*. Alle Daten, die in ein System *eingegeben* werden, sollten auf *Korrektheit*, *Vollständigkeit* und *Gültigkeit* überprüft werden. Dies kann durch *verschiedene Methoden* erfolgen, beispielsweise durch *Validierungsregeln*, *Datentypüberprüfungen*, *Bereichsprüfungen* und *Plausibilitätsprüfungen*. Die Eingabevalidierung hilft, *Fehler* und *Manipulationen* zu *verhindern*, *bevor* sie in das System gelangen.

Ein *weiterer wichtiger Aspekt* der Datenintegrität ist die *Zugriffskontrolle*. Nur *autorisierte Benutzer* sollten *Zugriff* auf Daten haben und *nur* die *Berechtigungen*, die sie für ihre *Aufgaben* *benötigen* (siehe Kapitel 7 und 8). Dies *minimiert* das Risiko von *unbefugten Änderungen* oder *Löschungen* von Daten. Die Zugriffskontrolle sollte *streng* durchgesetzt und *regelmäßig überprüft* werden.

Die *Protokollierung* von *Datenänderungen* ist ein *weiteres wichtiges Element* der Datenintegrität. Alle *Änderungen* an Daten sollten *protokolliert* werden, einschließlich *wer* die Änderung *vorgenommen* hat, *wann* die Änderung *vorgenommen* wurde und *welche Daten* *geändert* wurden. Diese *Protokolldateien* (siehe Kapitel 10) ermöglichen es, *Änderungen* an Daten *nachzuvollziehen* und *unbefugte* oder *fehlerhafte Änderungen* zu *erkennen*.

Die *Verwendung* von *Prüfsummen* oder *Hashwerten* ist eine *weitere Methode*, um die Datenintegrität zu *gewährleisten*. Eine Prüfsumme oder ein Hashwert ist ein *eindeutiger Wert*, der aus den Daten *berechnet* wird. Wenn sich die Daten *ändern*, ändert sich auch die Prüfsumme oder der Hashwert. Durch die *regelmäßige Überprüfung* der Prüfsummen oder Hashwerte kann festgestellt werden, ob Daten *unbemerkt geändert* wurden.

Die *Sicherung* von Daten (siehe Kapitel 13) ist ein *wesentlicher Bestandteil* der Datenintegrität und -verfügbarkeit. Regelmäßige Sicherungen stellen sicher, dass *Daten* im Falle eines *Verlusts* oder einer *Beschädigung* *wiederhergestellt* werden können. Die Sicherungen sollten *sicher aufbewahrt* und *regelmäßig getestet* werden, um sicherzustellen, dass sie *tatsächlich wiederherstellbar* sind.

Die *Redundanz* von Daten und Systemen ist ein *wichtiger Aspekt* der Datenverfügbarkeit. Redundanz bedeutet, dass *mehrere Kopien* von Daten oder Systemen vorhanden sind, so dass bei *Ausfall* einer Kopie eine *andere Kopie* übernehmen kann. Redundanz kann auf *verschiedenen Ebenen* implementiert werden, beispielsweise durch *gespiegelte Festplatten*, *redundante Server*, *redundante Netzverbindungen* oder *geografisch verteilte Rechenzentren*.

Die *Überwachung* von Systemen und Anwendungen ist *entscheidend*, um *Probleme* mit der Datenintegrität und -verfügbarkeit *frühzeitig* zu *erkennen*. Die Überwachung sollte *kontinuierlich* erfolgen und *verschiedene Aspekte* umfassen, beispielsweise die *Verfügbarkeit* von Systemen und Diensten, die *Auslastung* von Ressourcen (z.B. CPU, Speicher, Festplattenplatz), die *Anzahl* von *Fehlern* und die *Antwortzeiten* von Anwendungen.

Ein *Incident-Response-Plan* (siehe Kapitel 19) ist *unerlässlich*, um auf *Probleme* mit der Datenintegrität und -verfügbarkeit *schnell* und *effektiv* zu *reagieren*. Der Plan sollte *klare Verfahren* für die *Behandlung* von *verschiedenen Arten* von Vorfällen definieren, beispielsweise *Datenverlust*, *Datenbeschädigung*, *Systemausfall* oder *Netzestörung*. Der Plan sollte auch *Verantwortlichkeiten* festlegen und *Kommunikationswege* definieren.

Die *Schulung* der Mitarbeiter ist ein *wichtiger Aspekt* der Datenintegrität und -verfügbarkeit. Die Mitarbeiter sollten über die *Bedeutung* von Datenintegrität und -verfügbarkeit *informiert* werden und *geschult* werden, *wie* sie *Fehler* und *Manipulationen* *vermeiden* und *Probleme* *melden* können.

Die *Verwendung* von *fehlertoleranten Systemen* und *Technologien* kann die Datenverfügbarkeit *erhöhen*. Fehlertolerante Systeme sind so *konzipiert*, dass sie auch bei *Ausfall* einzelner Komponenten *weiterhin funktionieren*. Beispielsweise können *RAID-Systeme (Redundant Array of Independent Disks)* verwendet werden, um Daten auf *mehreren Festplatten* zu speichern, so dass bei *Ausfall* einer Festplatte die Daten *nicht verloren* gehen.

Die *Verwendung* von *Clustering-Technologien* kann die Datenverfügbarkeit *erhöhen*. Ein Cluster ist eine *Gruppe* von *Servern*, die *zusammenarbeiten*, um eine *Anwendung* oder einen *Dienst* bereitzustellen. Wenn ein Server im Cluster *ausfällt*, können die *anderen Server* im Cluster die *Arbeit* übernehmen.

Die *Verwendung* von *Load Balancing* kann die Datenverfügbarkeit *erhöhen*. Load Balancing verteilt die *Arbeitslast* auf *mehrere Server*, um zu *verhindern*, dass ein *einzelner Server* *überlastet* wird. Wenn ein Server *ausfällt* oder *überlastet* ist, kann der *Datenverkehr* auf die *anderen Server* umgeleitet werden.

Die *regelmäßige Überprüfung* und *Wartung* von Systemen und Anwendungen ist *wichtig*, um *Probleme* mit der Datenintegrität und -verfügbarkeit zu *vermeiden*. Die Systeme sollten *regelmäßig* auf *Fehler* überprüft werden, *Sicherheitsupdates* sollten *installiert* werden, und *nicht mehr benötigte Software* und *Daten* sollten *entfernt* werden.

Die *Einhaltung* von *Standards* und *Best Practices* kann dazu beitragen, die Datenintegrität und -verfügbarkeit zu *verbessern*. Es gibt *verschiedene Standards* und *Best Practices* für die Datenverarbeitung und -speicherung, beispielsweise *ISO 27001*, *COBIT* und *ITIL*.

Die *Zusammenarbeit* mit *anderen Abteilungen* und *Rollen* in der Organisation ist *wichtig*, um die Datenintegrität und -verfügbarkeit zu *gewährleisten*. Die *IT-Abteilung* ist für die *technische Implementierung* der Maßnahmen verantwortlich. Die *Sicherheitsabteilung* ist für die *Definition der Sicherheitsrichtlinien* und die *Überwachung der Einhaltung* verantwortlich. Die *Fachabteilungen*, die die Daten und Systeme *nutzen*, müssen in den Prozess *eingebunden* werden, beispielsweise bei der *Definition* der *Anforderungen* an die Datenintegrität und -verfügbarkeit und bei der *Bewertung* der *Auswirkungen* von Vorfällen.

Die Datenintegrität und -verfügbarkeit sind *kontinuierliche Herausforderungen*. Neue Bedrohungen tauchen *ständig* auf, und Technologien entwickeln sich *ständig* weiter. Daher müssen die Maßnahmen zur Gewährleistung der Datenintegrität und -verfügbarkeit *regelmäßig überprüft* und *angepasst* werden.

Datenintegrität und -verfügbarkeit sind *wesentliche Voraussetzungen* für den *erfolgreichen Betrieb* eines Unternehmens. Ohne *zuverlässige* Daten und *verfügbare* Systeme können *Geschäftsprozesse* nicht *ordnungsgemäß* ablaufen, *Entscheidungen* können nicht *fundiert* getroffen werden, und *Kunden* können nicht *zufriedenstellend* bedient werden. Die konsequente Anwendung der hier beschriebenen Prinzipien und Maßnahmen ist ein entscheidender Schritt zur Verbesserung der Cybersicherheit einer Organisation. Ein proaktiver und umfassender Ansatz zur Gewährleistung von Datenintegrität und -verfügbarkeit minimiert das Risiko von Datenverlust, -beschädigung und -ausfällen und trägt dazu bei, die Geschäftskontinuität zu gewährleisten. Durch die Kombination von technischen, organisatorischen und prozessualen Sicherheitsmaßnahmen wird ein robustes Fundament geschaffen, das die Zuverlässigkeit und Verfügbarkeit von Daten und Systemen sicherstellt und somit die Grundlage für den erfolgreichen Geschäftsbetrieb bildet.
