******************************************************************************
CIS Controls - CIS Kontrollen - Kontinuierliches Schwachstellenmanagement
******************************************************************************

.. sectionauthor:: Pierre Gronau <Pierre.Gronau@ndaal.eu>

..

.. contents:: Inhalt - CIS Kontrollen - Kontinuierliches Schwachstellenmanagement
    :depth: 3

.. _Section_CIS_Controls_Einführung:

Kontinuierliches Schwachstellenmanagement
------------------------------------------------------------------------------

Dieses Kapitel behandelt einen fortlaufenden Prozess, der für die Aufrechterhaltung der Systemsicherheit unerlässlich ist: das kontinuierliche Schwachstellenmanagement. CIS Control 7, "Continuous Vulnerability Management", fordert die Entwicklung eines Plans zur kontinuierlichen Bewertung und Verfolgung von Schwachstellen auf *allen* Unternehmensressourcen innerhalb der Infrastruktur der Organisation. Das Ziel ist, Schwachstellen zu beheben und das Zeitfenster für Angreifer zu minimieren. Es geht nicht nur darum, *ob* Schwachstellen gefunden werden, sondern *wann* und *wie schnell* sie behoben werden. Kontinuierliches Schwachstellenmanagement ist ein proaktiver Ansatz, der darauf abzielt, Schwachstellen zu identifizieren und zu beheben, *bevor* sie von Angreifern ausgenutzt werden können. Es ist ein iterativer Prozess, der die ständige Überwachung, Bewertung, Behebung und Überprüfung von Schwachstellen umfasst.

Warum ist kontinuierliches Schwachstellenmanagement so wichtig? Software und Hardware sind selten perfekt. Sie enthalten oft Fehler, sogenannte "Bugs", die zu Sicherheitslücken, den "Schwachstellen", führen können. Diese Schwachstellen können von Angreifern ausgenutzt werden, um in Systeme einzudringen, Daten zu stehlen, Systeme zu beschädigen oder den Betrieb zu stören. Neue Schwachstellen werden *ständig* entdeckt, sei es durch Sicherheitsforscher, Softwarehersteller oder Angreifer selbst. Die Bedrohungslandschaft ist dynamisch und entwickelt sich ständig weiter. Daher reicht es nicht aus, Systeme *einmalig* auf Schwachstellen zu prüfen. Ein kontinuierlicher Prozess ist erforderlich, um sicherzustellen, dass neue Schwachstellen *schnell* erkannt und behoben werden, bevor sie ausgenutzt werden können.

Stellen Sie sich vor, ein Unternehmen führt einmal im Jahr einen Schwachstellenscan durch. In der Zwischenzeit werden jedoch zahlreiche neue Schwachstellen in der verwendeten Software und Hardware entdeckt. Ein Angreifer, der eine dieser Schwachstellen kennt, könnte das Unternehmen möglicherweise monatelang unbemerkt angreifen, bevor der nächste Scan durchgeführt wird. Durch einen *kontinuierlichen* Schwachstellenmanagementprozess, der beispielsweise wöchentliche oder sogar tägliche Scans umfasst, wird das Zeitfenster für Angreifer erheblich reduziert.

Control 7 umfasst sieben Safeguards, die verschiedene Aspekte des kontinuierlichen Schwachstellenmanagements abdecken. Der erste Safeguard, 7.1, fordert die "Establish and Maintain a Vulnerability Management Process" (Erstellung und Pflege eines Schwachstellenmanagementprozesses). Dieser Prozess sollte für Unternehmensressourcen dokumentiert sein. Die Dokumentation sollte jährlich oder bei signifikanten Unternehmensänderungen, die Auswirkungen auf diesen Safeguard haben könnten, überprüft und aktualisiert werden. Ein dokumentierter Prozess stellt sicher, dass das Schwachstellenmanagement *systematisch* und *konsistent* durchgeführt wird.

Der Schwachstellenmanagementprozess sollte die folgenden Schritte umfassen:

1.  **Informationsbeschaffung:** Überwachung öffentlicher und privater Quellen (z.B. Sicherheitsbulletins, Schwachstellendatenbanken, Hersteller-Websites, Threat-Intelligence-Feeds) auf neue Bedrohungs- und Schwachstelleninformationen.
2.  **Identifizierung:** Regelmäßige Durchführung von Schwachstellenscans auf allen Unternehmensressourcen, um Schwachstellen zu identifizieren.
3.  **Bewertung:** Bewertung der identifizierten Schwachstellen hinsichtlich ihres Risikos und ihrer Priorität. Dabei sollten Faktoren wie die Ausnutzbarkeit der Schwachstelle, die potenziellen Auswirkungen eines Angriffs und die Bedeutung der betroffenen Systeme berücksichtigt werden.
4.  **Behebung:** Behebung der Schwachstellen, beispielsweise durch Installation von Patches, Konfigurationsänderungen oder Implementierung von Workarounds.
5.  **Überprüfung:** Überprüfung der Wirksamkeit der Behebungsmaßnahmen, beispielsweise durch erneute Durchführung von Schwachstellenscans.
6. Dokumentation: Der gesamte Prozess von Schwachstellenidentifizierung, -bewertung, -behebung, und -verifizierung sollte nachvollziehbar dokumentiert werden.

Dieser Prozess sollte durch klare Verantwortlichkeiten und Richtlinien unterstützt werden. Es sollte festgelegt werden, *wer* für die Durchführung der einzelnen Schritte verantwortlich ist, *wie oft* Schwachstellenscans durchgeführt werden sollen, *welche* Tools und Methoden verwendet werden sollen und *wie* mit identifizierten Schwachstellen umgegangen werden soll.

Safeguard 7.2, "Establish and Maintain a Remediation Process" (Erstellung und Pflege eines Behebungsprozesses), fordert die Erstellung und Aufrechterhaltung einer risikobasierten Behebungsstrategie, die in einem Behebungsprozess dokumentiert ist, mit monatlichen oder häufigeren Überprüfungen. Nicht alle Schwachstellen sind gleich kritisch. Einige Schwachstellen sind leichter auszunutzen als andere, und einige Systeme sind wichtiger als andere. Ein risikobasierter Behebungsprozess stellt sicher, dass die *kritischsten* Schwachstellen *zuerst* behoben werden.

Der Behebungsprozess sollte die folgenden Aspekte berücksichtigen:

*   **Risikobewertung:** Jede identifizierte Schwachstelle sollte hinsichtlich ihres Risikos bewertet werden. Dabei sollten Faktoren wie die Ausnutzbarkeit der Schwachstelle, die potenziellen Auswirkungen eines Angriffs und die Bedeutung der betroffenen Systeme berücksichtigt werden.
*   **Priorisierung:** Die Schwachstellen sollten basierend auf ihrem Risiko priorisiert werden. Kritische Schwachstellen sollten *sofort* behoben werden, während Schwachstellen mit geringem Risiko möglicherweise später behoben werden können.
*   **Zeitrahmen:** Für jede Schwachstelle sollte ein Zeitrahmen für die Behebung festgelegt werden. Dieser Zeitrahmen sollte sich am Risiko der Schwachstelle orientieren.
*   **Verantwortlichkeiten:** Es sollte festgelegt werden, *wer* für die Behebung der Schwachstelle verantwortlich ist.
*   **Eskalation:** Es sollte ein Eskalationsprozess definiert werden, falls die Schwachstelle nicht innerhalb des festgelegten Zeitrahmens behoben werden kann.

Safeguard 7.3, "Perform Automated Operating System Patch Management" (Durchführung eines automatisierten Betriebssystem-Patch-Managements), fordert die Durchführung von Betriebssystem-Updates auf Unternehmensressourcen durch automatisiertes Patch-Management, monatlich oder häufiger. Softwarehersteller veröffentlichen regelmäßig Updates, sogenannte "Patches", um bekannte Schwachstellen in ihren Produkten zu beheben. Die Installation dieser Patches ist eine der *wichtigsten* Maßnahmen zur Reduzierung des Risikos von Cyberangriffen.

Ein automatisiertes Patch-Management-System stellt sicher, dass Patches *schnell* und *konsistent* auf allen Systemen installiert werden. Das System lädt die Patches automatisch herunter, testet sie (idealerweise in einer Testumgebung) und installiert sie dann auf den Produktionssystemen. Viele Betriebssysteme, wie Windows und macOS, verfügen über integrierte Funktionen für das automatische Patch-Management. Es gibt auch spezialisierte Patch-Management-Tools, die zusätzliche Funktionen bieten, beispielsweise die Unterstützung für Drittanbieter-Software, die zentrale Verwaltung von Patches und die Erstellung von Berichten.

Safeguard 7.4, "Perform Automated Application Patch Management" (Durchführung eines automatisierten Anwendungs-Patch-Managements), erweitert das Konzept des automatisierten Patch-Managements auf *Anwendungen*. Auch Anwendungssoftware, wie Webbrowser, Office-Suiten, PDF-Reader und Mediaplayer, enthält oft Schwachstellen, die durch Patches behoben werden müssen.

Ein automatisiertes Anwendungs-Patch-Management-System stellt sicher, dass Patches für Anwendungen *schnell* und *konsistent* auf allen Systemen installiert werden. Dies kann durch die Verwendung von Patch-Management-Tools erfolgen, die Drittanbieter-Software unterstützen, oder durch die Integration mit Softwareverteilungssystemen.

Safeguard 7.5, "Perform Automated Vulnerability Scans of Internal Enterprise Assets" (Durchführung automatisierter Schwachstellenscans interner Unternehmensressourcen), fordert die Durchführung automatisierter Schwachstellenscans interner Unternehmensressourcen, vierteljährlich oder häufiger. Führen Sie sowohl authentifizierte als auch nicht authentifizierte Scans durch. Schwachstellenscans sind automatisierte Tests, die Systeme und Anwendungen auf *bekannte* Schwachstellen überprüfen. Sie verwenden eine Datenbank bekannter Schwachstellen und vergleichen die Konfiguration und Version der installierten Software mit dieser Datenbank.

Es gibt zwei Arten von Schwachstellenscans:

*   **Nicht authentifizierte Scans:** Diese Scans werden *ohne* Anmeldeinformationen durchgeführt. Sie simulieren einen Angriff von außen und zeigen, welche Schwachstellen ein Angreifer *ohne* Zugriff auf das System ausnutzen könnte.
*   **Authentifizierte Scans:** Diese Scans werden *mit* Anmeldeinformationen durchgeführt. Sie simulieren einen Angriff von innen (z.B. durch einen Insider oder einen Angreifer, der bereits Zugriff auf ein Benutzerkonto erlangt hat) und zeigen, welche Schwachstellen ein Angreifer *mit* Zugriff auf das System ausnutzen könnte.

Die Kombination von nicht authentifizierten und authentifizierten Scans bietet einen umfassenden Überblick über die Schwachstellen in einem System. Die Scans sollten *regelmäßig* durchgeführt werden, beispielsweise wöchentlich oder sogar täglich, um sicherzustellen, dass neue Schwachstellen *schnell* erkannt werden. Es gibt eine Vielzahl von kommerziellen und Open-Source-Schwachstellenscannern auf dem Markt, die unterschiedliche Funktionen und Unterstützung für verschiedene Betriebssysteme und Anwendungen bieten.

Safeguard 7.6, "Perform Automated Vulnerability Scans of Externally-Exposed Enterprise Assets" (Durchführung automatisierter Schwachstellenscans extern zugänglicher Unternehmensressourcen), erweitert das Konzept der Schwachstellenscans auf *extern* zugängliche Systeme, also Systeme, die über das Internet erreichbar sind. Diese Systeme sind einem *erhöhten* Risiko ausgesetzt, da sie von *jedem* Ort der Welt aus angegriffen werden können. Die Scans sollten monatlich, oder häufiger durchgeführt werden.

Safeguard 7.7, "Remediate Detected Vulnerabilities" (Behebung erkannter Schwachstellen), fordert die Behebung erkannter Schwachstellen in der Software durch Prozesse und Tools, monatlich oder häufiger, basierend auf dem Behebungsprozess. Die Behebung von Schwachstellen ist der *entscheidende* Schritt im Schwachstellenmanagementprozess. Die Identifizierung von Schwachstellen ist nur der erste Schritt. Die Schwachstellen müssen auch *behoben* werden, um das Risiko von Cyberangriffen zu reduzieren.

Die Behebung von Schwachstellen kann verschiedene Maßnahmen umfassen:

*   **Installation von Patches:** Die häufigste Methode zur Behebung von Schwachstellen ist die Installation von Patches, die von den Softwareherstellern bereitgestellt werden.
*   **Konfigurationsänderungen:** In einigen Fällen können Schwachstellen durch Änderung der Konfiguration des Systems oder der Anwendung behoben werden, beispielsweise durch Deaktivierung unnötiger Dienste oder Aktivierung sicherer Einstellungen.
*   **Implementierung von Workarounds:** Wenn kein Patch verfügbar ist und eine Konfigurationsänderung nicht möglich ist, kann in einigen Fällen ein Workaround implementiert werden, um die Schwachstelle zu umgehen oder ihre Auswirkungen zu reduzieren.
* **Austausch/Ersatz der Software:** In seltenen Fällen kann es sein, dass die Software nicht repariert werden kann. Hier sollte, wenn möglich, auf alternative Software ausgewichen werden.

Die Behebung von Schwachstellen sollte *priorisiert* werden, basierend auf dem Risiko der Schwachstelle (siehe Safeguard 7.2). Kritische Schwachstellen sollten *sofort* behoben werden, während Schwachstellen mit geringem Risiko möglicherweise später behoben werden können.

Das kontinuierliche Schwachstellenmanagement ist *kein einmaliges Projekt*, sondern ein *fortlaufender Prozess*. Neue Schwachstellen werden *ständig* entdeckt, und die Bedrohungslandschaft entwickelt sich *ständig* weiter. Daher muss der Schwachstellenmanagementprozess *regelmäßig* überprüft und angepasst werden. Dies erfordert ein kontinuierliches Engagement und die Bereitschaft, in die Sicherheit zu investieren.

Das kontinuierliche Schwachstellenmanagement erfordert die *Zusammenarbeit* verschiedener Abteilungen und Rollen in der Organisation. Die IT-Abteilung ist für die technische Implementierung des Schwachstellenmanagements verantwortlich, beispielsweise für die Durchführung von Schwachstellenscans, die Installation von Patches und die Konfiguration von Systemen. Die Sicherheitsabteilung ist für die Definition der Sicherheitsrichtlinien, die Überwachung der Einhaltung und die Bewertung des Risikos von Schwachstellen verantwortlich. Und die Fachabteilungen müssen in den Prozess eingebunden werden, beispielsweise bei der Planung von Wartungsfenstern für die Installation von Patches und bei der Bewertung der Auswirkungen von Schwachstellen auf geschäftskritische Anwendungen.

Ein wichtiger Aspekt des kontinuierlichen Schwachstellenmanagements ist die *Automatisierung*. Viele der Schritte im Schwachstellenmanagementprozess können automatisiert werden, beispielsweise die Informationsbeschaffung, die Durchführung von Schwachstellenscans, die Installation von Patches und die Überprüfung der Wirksamkeit von Behebungsmaßnahmen. Die Automatisierung reduziert den manuellen Aufwand, minimiert das Risiko menschlicher Fehler und ermöglicht eine schnellere Reaktion auf neue Schwachstellen.

Das kontinuierliche Schwachstellenmanagement ist eng mit anderen Sicherheitsmaßnahmen verbunden, beispielsweise mit dem Patch-Management, der Konfigurationsverwaltung, der Netzüberwachung und dem Incident-Response-Management. Ein effektives Schwachstellenmanagement reduziert das Risiko von Sicherheitsvorfällen, aber es kann Sicherheitsvorfälle nicht vollständig verhindern. Daher ist es wichtig, einen Incident-Response-Plan zu haben, um auf Sicherheitsvorfälle *schnell* und *effektiv* reagieren zu können.

Das kontinuierliche Schwachstellenmanagement ist *kein Allheilmittel* gegen Cyberangriffe. Es ist jedoch eine *grundlegende* Sicherheitsmaßnahme, die das Risiko von Cyberangriffen *erheblich* reduzieren kann. Durch die kontinuierliche Überwachung, Bewertung, Behebung und Überprüfung von Schwachstellen können Organisationen ihre Systeme und Anwendungen widerstandsfähiger gegen Angriffe machen und das Zeitfenster für Angreifer minimieren. Die konsequente Anwendung dieser Praktiken bildet eine solide Grundlage für ein proaktives Sicherheitsmanagement und trägt maßgeblich zur Stabilität und Sicherheit der IT-Infrastruktur bei.
