******************************************************************************
CIS Controls - CIS Kontrollen - Durchführung von Penetrationstests
******************************************************************************

.. sectionauthor:: Pierre Gronau <Pierre.Gronau@ndaal.eu>

..

.. contents:: Inhalt - CIS Kontrollen - Durchführung von Penetrationstests
    :depth: 3

.. _Section_CIS_Controls_Durchführung_von_Penetrationstests:

Durchführung von Penetrationstests
------------------------------------------------------------------------------

Dieses Kapitel befasst sich mit einer proaktiven Methode zur Bewertung der Sicherheit von IT-Systemen und Netzen: Penetrationstests, oft auch als "Pentests" bezeichnet. CIS Control 18, "Penetration Testing", fordert die Überprüfung der Effektivität und Widerstandsfähigkeit von Unternehmensressourcen durch Identifizierung und Ausnutzung von Schwachstellen in Kontrollen (Menschen, Prozesse und Technologie) und die Simulation der Ziele und Aktionen eines Angreifers. Es geht *nicht* nur darum, *Schwachstellen* zu *finden*, sondern auch darum, zu *verstehen*, wie ein *realer Angreifer* diese Schwachstellen *ausnutzen* könnte, um *in die Systeme einzudringen*, *Daten zu stehlen* oder *Schaden anzurichten*. Penetrationstests sind *simulierte Angriffe* auf IT-Systeme und Netze, die von *speziell geschulten Experten*, den sogenannten "Penetrationstestern" oder "Ethical Hackern", durchgeführt werden. Sie verwenden die *gleichen Techniken* und *Tools* wie *echte Angreifer*, aber mit dem *Ziel*, die *Sicherheit* zu *verbessern*, *nicht* zu *schaden*.

Warum sind Penetrationstests so wichtig? Schwachstellenscans (siehe Kapitel 9) können *bekannte Schwachstellen* in Systemen und Anwendungen *identifizieren*. Penetrationstests gehen jedoch *einen Schritt weiter*. Sie zeigen, ob und wie diese Schwachstellen *tatsächlich ausgenutzt* werden können, um *in die Systeme einzudringen* und *Schaden anzurichten*. Sie liefern *konkrete Beweise* für die *Auswirkungen* von Schwachstellen und helfen, die *dringendsten Sicherheitsprobleme* zu *priorisieren*. Penetrationstests sind *kein Ersatz* für andere Sicherheitsmaßnahmen, wie *Firewalls*, *Intrusion Detection Systems*, *Antivirensoftware* oder *sichere Konfiguration*. Sie sind eine *ergänzende Maßnahme*, die die *Effektivität* dieser Sicherheitsmaßnahmen *überprüft* und *Schwachstellen* aufdeckt, die von *anderen Maßnahmen* möglicherweise *übersehen* werden.

Stellen Sie sich vor, ein Unternehmen führt *regelmäßig Schwachstellenscans* durch und *installiert alle verfügbaren Sicherheitspatches*. Die Schwachstellenscans zeigen *keine kritischen Schwachstellen* an. Das Unternehmen könnte sich daher *sicher fühlen*. Ein Penetrationstest könnte jedoch zeigen, dass ein Angreifer durch die *Kombination mehrerer scheinbar harmloser Schwachstellen* oder durch die *Ausnutzung einer unbekannten Schwachstelle* dennoch in der Lage ist, *in das Netz einzudringen* und *Zugriff* auf *sensible Daten* zu erlangen. Der Penetrationstest würde dem Unternehmen helfen, diese *Schwachstellen* zu *identifizieren* und zu *beheben*, *bevor* sie von einem *echten Angreifer* ausgenutzt werden.

Penetrationstests sind *nicht nur* für die *Identifizierung* von *technischen Schwachstellen* nützlich, sondern auch für die *Überprüfung* der *Effektivität* von *organisatorischen* und *prozessualen* Sicherheitsmaßnahmen. Beispielsweise könnte ein Penetrationstest zeigen, dass Mitarbeiter *nicht ausreichend geschult* sind, um *Phishing-E-Mails* zu *erkennen*, oder dass der *Incident-Response-Plan* des Unternehmens *nicht effektiv* ist.

Control 18 umfasst fünf Safeguards, die verschiedene Aspekte von Penetrationstests abdecken. Der erste Safeguard, 18.1, fordert die "Establish and Maintain a Penetration Testing Program" (Erstellung und Pflege eines Penetrationstestprogramms). Erstellen und pflegen Sie ein Penetrationstestprogramm, das der Größe, Komplexität, Branche und Reife des Unternehmens entspricht. Zu den Merkmalen des Penetrationstestprogramms gehören Umfang, wie z. B. Netz, Webanwendung, Anwendungsprogrammierschnittstelle (API), gehostete Dienste und physische Präsenzkontrollen; Häufigkeit; Einschränkungen, wie z. B. akzeptable Stunden und ausgeschlossene Angriffstypen; Kontaktinformationen; Behebung, z. B. wie Ergebnisse intern weitergeleitet werden; und retrospektive Anforderungen. Ein *strukturiertes Penetrationstestprogramm* stellt sicher, dass Penetrationstests *regelmäßig*, *systematisch* und *zielgerichtet* durchgeführt werden.

Das Penetrationstestprogramm sollte die folgenden Aspekte umfassen:

*   **Ziele:** Definition der *Ziele* der Penetrationstests, beispielsweise die *Identifizierung* von *Schwachstellen*, die *Überprüfung* der *Effektivität* von *Sicherheitsmaßnahmen*, die *Einhaltung* von *gesetzlichen* oder *regulatorischen Anforderungen* oder die *Sensibilisierung* der Mitarbeiter für *Sicherheitsrisiken*.
*   **Umfang:** Festlegung des *Umfangs* der Penetrationstests, beispielsweise *welche Systeme*, *Anwendungen* oder *Netze* getestet werden sollen. Der Umfang sollte *klar definiert* und *abgegrenzt* sein, um *Missverständnisse* und *unerwünschte Auswirkungen* zu vermeiden.
*   **Methodik:** Festlegung der *Methodik* der Penetrationstests, beispielsweise *welche Techniken* und *Tools* verwendet werden sollen, *welche Arten* von Tests durchgeführt werden sollen (z.B. Black-Box-Tests, White-Box-Tests, Grey-Box-Tests) und *wie* die Ergebnisse *dokumentiert* und *berichtet* werden sollen.
*   **Zeitplan:** Festlegung des *Zeitplans* für die Penetrationstests, beispielsweise *wie oft* die Tests durchgeführt werden sollen (z.B. *jährlich*, *halbjährlich*, *vierteljährlich*) und *wann* die Tests durchgeführt werden sollen (z.B. *außerhalb der Geschäftszeiten*, um *Störungen* des *normalen Betriebs* zu *vermeiden*).
*   **Ressourcen:** Festlegung der *Ressourcen*, die für die Penetrationstests benötigt werden, beispielsweise *Personal*, *Tools*, *Budget* und *Zugriffsrechte*.
*   **Risikobewertung:** *Bewertung* der *Risiken*, die mit den Penetrationstests verbunden sind, beispielsweise das *Risiko* von *Systemausfällen*, *Datenverlust* oder *Betriebsstörungen*. Es sollten *Maßnahmen* ergriffen werden, um diese Risiken zu *minimieren*.
*   **Kommunikation:** Festlegung, *wie* die *Kommunikation* während der Penetrationstests erfolgen soll, beispielsweise *wer* über die Tests *informiert* werden soll, *wie* die *Ergebnisse* *mitgeteilt* werden sollen und *wie* mit *sensiblen Informationen* umgegangen werden soll.
*   **Rechtliche Aspekte:** Berücksichtigung *rechtlicher Aspekte*, beispielsweise die *Einholung* der *Zustimmung* der *betroffenen Parteien* (z.B. Systemadministratoren, Anwendungsentwickler, Benutzer), die *Einhaltung* von *Datenschutzbestimmungen* und die *Regelung* von *Haftungsfragen*.

Das Penetrationstestprogramm sollte *dokumentiert* und allen *relevanten Parteien* *kommuniziert* werden. Es sollte auch *regelmäßig überprüft* und *aktualisiert* werden, um sicherzustellen, dass es den *aktuellen Anforderungen* und *Bedrohungen* entspricht.

Safeguard 18.2, "Perform Periodic External Penetration Tests" (Durchführung periodischer externer Penetrationstests), fordert die Durchführung periodischer externer Penetrationstests basierend auf den Programmanforderungen, mindestens jedoch jährlich. Externe Penetrationstests müssen Unternehmens- und Umgebungsaufklärung umfassen, um verwertbare Informationen zu erkennen. Penetrationstests erfordern spezielle Fähigkeiten und Erfahrungen und müssen von einer qualifizierten Partei durchgeführt werden. Die Tests können als Clear-Box- oder Opaque-Box-Tests durchgeführt werden. *Externe Penetrationstests* simulieren einen Angriff von *außerhalb* des UnternehmensNetzes, beispielsweise über das *Internet*. Sie zielen darauf ab, *Schwachstellen* in den *extern zugänglichen Systemen* und *Anwendungen* des Unternehmens zu *identifizieren*, beispielsweise in *Webservern*, *E-Mail-Servern*, *Firewalls* oder *VPN-Gateways*.

Externe Penetrationstests sollten *regelmäßig*, beispielsweise *jährlich* oder *halbjährlich*, durchgeführt werden. Die *Häufigkeit* der Tests sollte sich an den *spezifischen Risiken* und *Anforderungen* des Unternehmens orientieren.

Externe Penetrationstests sollten *alle Aspekte* der *externen Angriffsfläche* des Unternehmens umfassen, beispielsweise:

*   **Netz-Perimeter:** Überprüfung der *Sicherheit* der *Firewalls*, *Router* und *anderen Netzgeräte*, die das UnternehmensNetz mit dem *Internet* verbinden.
*   **Webanwendungen:** Überprüfung der *Sicherheit* der *Webanwendungen*, die über das *Internet* zugänglich sind, beispielsweise *Websites*, *Webshops*, *Kundenportale* oder *Webdienste*.
*   **Remote-Zugriffsdienste:** Überprüfung der *Sicherheit* der *Dienste*, die den *Remote-Zugriff* auf das UnternehmensNetz ermöglichen, beispielsweise *VPN-Gateways*, *Terminalserver* oder *Remote-Desktop-Dienste*.
*   **E-Mail-Systeme:** Überprüfung der *Sicherheit* der *E-Mail-Systeme*, beispielsweise *Schutz* vor *Phishing*, *Spam* und *Schadsoftware*.
*   **Cloud-Dienste:** Überprüfung der *Sicherheit* der *Cloud-Dienste*, die das Unternehmen nutzt, beispielsweise *Cloud-Speicher*, *SaaS-Anwendungen* oder *IaaS-Plattformen*.

Externe Penetrationstests können in *verschiedenen Formen* durchgeführt werden:

*   **Black-Box-Tests:** Die Penetrationstester haben *keine Vorkenntnisse* über die *Systeme* und *Anwendungen* des Unternehmens. Sie müssen sich *alle Informationen* selbst *beschaffen*, beispielsweise durch *öffentliche Quellen* (z.B. Websites, Suchmaschinen, soziale Medien) oder durch *aktive Aufklärung* (z.B. Port-Scans, Banner-Grabbing).
*   **White-Box-Tests:** Die Penetrationstester haben *vollständige Kenntnisse* über die *Systeme* und *Anwendungen* des Unternehmens, beispielsweise *Zugriff* auf *Quellcode*, *Dokumentationen* und *Konfigurationsinformationen*.
*   **Grey-Box-Tests:** Die Penetrationstester haben *teilweise Kenntnisse* über die *Systeme* und *Anwendungen* des Unternehmens, beispielsweise *Zugriff* auf *Benutzerkonten* oder *Dokumentationen*, aber *keinen Zugriff* auf *Quellcode* oder *detaillierte Konfigurationsinformationen*.

Die Wahl der geeigneten Testform hängt von den *Zielen* des Penetrationstests und den *verfügbaren Ressourcen* ab. Black-Box-Tests sind *realistischer*, da sie einen Angriff von *außen* simulieren, erfordern aber *mehr Zeit* und *Aufwand*. White-Box-Tests sind *effizienter*, da die Penetrationstester *nicht* alle Informationen selbst *beschaffen* müssen, können aber *Schwachstellen übersehen*, die ein *echter Angreifer* ausnutzen könnte. Grey-Box-Tests liegen *zwischen* Black-Box- und White-Box-Tests.

Safeguard 18.3, "Remediate Penetration Test Findings" (Behebung von Ergebnissen aus Penetrationstests), fordert die Behebung von Ergebnissen aus Penetrationstests basierend auf dem dokumentierten Schwachstellenbehebungsprozess des Unternehmens. Dies sollte die Festlegung eines Zeitrahmens und eines Arbeitsaufwands basierend auf den Auswirkungen und der Priorisierung jedes identifizierten Ergebnisses umfassen. Die *Identifizierung* von Schwachstellen durch Penetrationstests ist *nur der erste Schritt*. Die Schwachstellen müssen auch *behoben* werden, um die *Sicherheit* des Unternehmens zu *verbessern*.

Die Ergebnisse des Penetrationstests sollten *sorgfältig analysiert* und *priorisiert* werden. Nicht alle Schwachstellen sind *gleich kritisch*. Einige Schwachstellen sind *leichter auszunutzen* als andere, und einige Schwachstellen haben *größere Auswirkungen* als andere.

Die Behebung der Schwachstellen sollte *gemäß* dem *Schwachstellenmanagementprozess* des Unternehmens erfolgen (siehe Kapitel 9). Für jede Schwachstelle sollte ein *Zeitrahmen* für die Behebung festgelegt werden, der sich an der *Schwere* der Schwachstelle und den *potenziellen Auswirkungen* eines Angriffs orientiert.

Safeguard 18.4, "Validate Security Measures" (Validierung von Sicherheitsmaßnahmen), fordert die Validierung von Sicherheitsmaßnahmen nach jedem Penetrationstest. Modifizieren Sie, falls erforderlich, Regelsätze und Fähigkeiten, um die während des Tests verwendeten Techniken zu erkennen. Penetrationstests sind *nicht nur* dazu da, *Schwachstellen* zu *identifizieren*, sondern auch dazu, die *Effektivität* der *vorhandenen Sicherheitsmaßnahmen* zu *überprüfen*.

Nach einem Penetrationstest sollte *überprüft* werden, ob die *vorhandenen Sicherheitsmaßnahmen* den Angriff *erkannt* und *abgewehrt* haben. Wenn der Angriff *nicht erkannt* wurde, sollten die *Regelsätze* und *Fähigkeiten* der Sicherheitsmaßnahmen *angepasst* werden, um *ähnliche Angriffe* in *Zukunft* zu *erkennen*.

Beispielsweise könnte ein Penetrationstest zeigen, dass ein *Intrusion Detection System (IDS)* einen bestimmten Angriff *nicht erkannt* hat. Die *Regeln* des IDS sollten dann *aktualisiert* werden, um diesen Angriff in *Zukunft* zu *erkennen*. Oder ein Penetrationstest könnte zeigen, dass eine *Firewall* einen bestimmten *Datenverkehr* *nicht blockiert* hat. Die *Konfiguration* der Firewall sollte dann *angepasst* werden, um diesen Datenverkehr in *Zukunft* zu *blockieren*.

Safeguard 18.5, "Perform Periodic Internal Penetration Tests" (Durchführung periodischer interner Penetrationstests), fordert die Durchführung periodischer interner Penetrationstests basierend auf den Programmanforderungen, mindestens jedoch jährlich. Die Tests können als Clear-Box- oder Opaque-Box-Tests durchgeführt werden. *Interne Penetrationstests* simulieren einen Angriff von *innerhalb* des UnternehmensNetzes, beispielsweise durch einen *Insider* (z.B. einen Mitarbeiter, einen Auftragnehmer oder einen Besucher) oder durch einen Angreifer, der bereits *Zugriff* auf *ein System* im Netz erlangt hat (z.B. durch *Phishing* oder *Schadsoftware*).

Interne Penetrationstests zielen darauf ab, *Schwachstellen* in den *internen Systemen* und *Anwendungen* des Unternehmens zu *identifizieren*, beispielsweise in *Servern*, *Workstations*, *Datenbanken*, *Anwendungen* oder *internen Netzdiensten*.

Interne Penetrationstests sollten *regelmäßig*, beispielsweise *jährlich* oder *halbjährlich*, durchgeführt werden. Die *Häufigkeit* der Tests sollte sich an den *spezifischen Risiken* und *Anforderungen* des Unternehmens orientieren.

Interne Penetrationstests können, wie externe Penetrationstests, in *verschiedenen Formen* durchgeführt werden (Black-Box, White-Box, Grey-Box).

Die Durchführung von Penetrationstests ist *keine einmalige Aufgabe*, sondern ein *fortlaufender Prozess*. Neue Bedrohungen tauchen *ständig* auf, und Angreifer entwickeln *ständig* neue Techniken, um Sicherheitsmaßnahmen zu umgehen. Daher müssen Penetrationstests *regelmäßig wiederholt* werden, um sicherzustellen, dass die *Sicherheitsmaßnahmen* des Unternehmens *wirksam* bleiben und *neue Schwachstellen* *frühzeitig erkannt* werden.

Die Durchführung von Penetrationstests erfordert *spezielle Kenntnisse* und *Erfahrungen*. Es ist *nicht empfehlenswert*, Penetrationstests *ohne* die *notwendige Expertise* durchzuführen, da dies zu *Schäden* an den Systemen, *Datenverlust* oder *Betriebsstörungen* führen kann. Es ist ratsam, *qualifizierte Penetrationstester* zu *beauftragen*, entweder *interne Mitarbeiter* mit der *notwendigen Expertise* oder *externe Dienstleister*, die sich auf Penetrationstests *spezialisiert* haben.

Penetrationstests sind ein *wesentlicher Bestandteil* einer *umfassenden Cybersicherheitsstrategie*. Sie helfen, *Schwachstellen* in IT-Systemen und Netzen zu *identifizieren* und zu *beheben*, *bevor* sie von *echten Angreifern* ausgenutzt werden können. Sie *überprüfen* die *Effektivität* der *vorhandenen Sicherheitsmaßnahmen* und liefern *konkrete Beweise* für die *Auswirkungen* von Schwachstellen. Die konsequente Anwendung der hier beschriebenen Prinzipien und Maßnahmen ist ein entscheidender Schritt zur Verbesserung der Cybersicherheit einer Organisation. Ein proaktiver und umfassender Ansatz zu Penetrationstests, zusammen mit einem etablierten Programm und erfahrenen Testern, ermöglicht eine realistische Einschätzung der Sicherheitslage und die gezielte Behebung von Schwachstellen, wodurch das Risiko erfolgreicher Cyberangriffe erheblich reduziert wird.
