******************************************************************************
CIS Controls - CIS Kontrollen - Sicherheit von Anwendungsoftware
******************************************************************************

.. sectionauthor:: Pierre Gronau <Pierre.Gronau@ndaal.eu>

..

.. contents:: Inhalt - CIS Kontrollen - Sicherheit von Anwendungsoftware
    :depth: 3

.. _Section_CIS_Controls_Sicherheit_von_Anwendungsoftware:

Sicherheit von Anwendungsoftware
------------------------------------------------------------------------------

Dieses Kapitel widmet sich der Sicherheit von Anwendungssoftware, ein Bereich, der oft als "Application Security" oder kurz "AppSec" bezeichnet wird. CIS Control 16, "Application Software Security", fordert die Verwaltung des Sicherheitslebenszyklus von intern entwickelter, gehosteter oder erworbener Software. Das Ziel ist, Sicherheitsschwachstellen zu verhindern, zu erkennen und zu beheben, *bevor* sie das Unternehmen beeinträchtigen können. Es geht *nicht* nur darum, *sichere Software* zu *schreiben*, sondern auch darum, *sicherzustellen*, dass *alle* Software, die in der Organisation verwendet wird, *sicher* ist, *unabhängig* davon, ob sie *intern entwickelt*, von *Drittanbietern gekauft* oder als *Service* (wie SaaS) bezogen wurde. Anwendungssicherheit ist ein *kontinuierlicher Prozess*, der den *gesamten Lebenszyklus* der Software umfasst, von der *Planung* und *Entwicklung* über die *Bereitstellung* und den *Betrieb* bis hin zur *Außerbetriebnahme*.

Warum ist die Sicherheit von Anwendungssoftware so wichtig? Anwendungen sind das *Herzstück* der meisten Geschäftsprozesse. Sie verarbeiten *sensible Daten*, steuern *kritische Systeme* und ermöglichen die *Interaktion* mit Kunden und Partnern. Wenn Anwendungen *Schwachstellen* aufweisen, können Angreifer diese Schwachstellen *ausnutzen*, um *Daten zu stehlen*, *Systeme zu beschädigen*, *den Betrieb zu stören* oder *anderen Schaden* anzurichten. Anwendungsschwachstellen sind eine der *häufigsten Ursachen* für *Sicherheitsvorfälle*.

Stellen Sie sich vor, eine Webanwendung, die *Kundendaten* verarbeitet, weist eine *SQL-Injection-Schwachstelle* auf. Ein Angreifer könnte diese Schwachstelle *ausnutzen*, um *unbefugten Zugriff* auf die *Datenbank* zu erlangen und *alle Kundendaten zu stehlen*. Oder stellen Sie sich vor, eine mobile App, die für die *Steuerung* eines *kritischen Systems* verwendet wird, weist eine *Schwachstelle* auf, die es einem Angreifer ermöglicht, *beliebige Befehle* auf dem System auszuführen. Der Angreifer könnte das System *manipulieren*, *beschädigen* oder *lahmlegen*.

Die Sicherheit von Anwendungssoftware ist *nicht nur* ein *technisches Problem*, sondern auch ein *organisatorisches* und *prozessuales* Problem. Es reicht *nicht* aus, *Entwickler* in *sicherer Programmierung* zu *schulen* und *Sicherheitstools* einzusetzen. Es ist auch *notwendig*, *klare Richtlinien* und *Verfahren* für die *Entwicklung*, *Beschaffung* und den *Betrieb* von Software zu *definieren*, *Verantwortlichkeiten* festzulegen und die *Einhaltung* der Sicherheitsanforderungen zu *überwachen*.

Control 16 umfasst vierzehn Safeguards, die verschiedene Aspekte der Anwendungssicherheit abdecken. Der erste Safeguard, 16.1, fordert die "Establish and Maintain a Secure Application Development Process" (Erstellung und Pflege eines sicheren Anwendungsentwicklungsprozesses). Dieser Prozess sollte dokumentiert sein. Darin sollten Punkte wie: Standards für sicheres Anwendungsdesign, sichere Programmierpraktiken, Entwicklerschulungen, Schwachstellenmanagement, Sicherheit von Drittanbietercode und Anwendungssicherheitstestverfahren behandelt werden. Überprüfen und aktualisieren Sie die Dokumentation jährlich oder wenn signifikante Unternehmensänderungen eintreten, die sich auf diesen Safeguard auswirken könnten. Ein *sicherer Anwendungsentwicklungsprozess*, oft auch als "Secure Software Development Lifecycle (SSDLC)" bezeichnet, stellt sicher, dass *Sicherheit* von *Anfang an* in den *gesamten Entwicklungsprozess* *integriert* wird.

Der SSDLC sollte die folgenden Phasen umfassen:

1.  **Planung:** Definition der *Sicherheitsanforderungen* für die Anwendung, basierend auf den *geschäftlichen Anforderungen*, den *relevanten Gesetzen und Vorschriften* und den *potenziellen Bedrohungen* und *Risiken*.
2.  **Design:** Entwurf der *Architektur* und des *Designs* der Anwendung unter *Berücksichtigung* der *Sicherheitsanforderungen*. Dies umfasst beispielsweise die *Auswahl sicherer Technologien*, die *Definition von Authentifizierungs- und Autorisierungsmechanismen* und die *Planung* von *Fehlerbehandlungs- und Protokollierungsfunktionen*.
3.  **Implementierung:** *Schreiben* des *Codes* der Anwendung unter *Verwendung sicherer Programmierpraktiken*. Dies umfasst beispielsweise die *Validierung von Eingabedaten*, die *Vermeidung von Pufferüberläufen* und die *sichere Verwendung von Kryptographie*.
4.  **Testen:** *Testen* der Anwendung auf *Sicherheitsschwachstellen*, beispielsweise durch *Code-Reviews*, *statische Code-Analyse*, *dynamische Code-Analyse* (wie Penetrationstests) und *Fuzzing*.
5.  **Bereitstellung:** *Sichere Bereitstellung* der Anwendung in der *Produktionsumgebung*. Dies umfasst beispielsweise die *Konfiguration* der *Server* und *Netzkomponenten* nach *sicheren Standards*, die *Installation* von *Sicherheitsupdates* und die *Aktivierung* von *Sicherheitsfunktionen*.
6.  **Betrieb und Wartung:** *Überwachung* der Anwendung auf *Sicherheitsvorfälle*, *Installation* von *Sicherheitsupdates* und *Patches* und *regelmäßige Überprüfung* der *Sicherheitskonfiguration*.
7. Außerbetriebnahme: Entfernen der Anwendung, wenn sie nicht mehr verwendet wird, inklusive sicherer Löschung aller relevanter Daten.

Dieser Prozess sollte durch *klare Richtlinien*, *Verfahren*, *Verantwortlichkeiten* und *Tools* unterstützt werden. Es sollte festgelegt werden, *wer* für die *einzelnen Phasen* des Prozesses *verantwortlich* ist, *welche Tools* und *Methoden* verwendet werden sollen und *wie* die *Einhaltung* der *Sicherheitsanforderungen* *überwacht* und *dokumentiert* wird.

Safeguard 16.2, "Establish and Maintain a Process to Accept and Address Software Vulnerabilities" (Erstellung und Pflege eines Prozesses zur Annahme und Behandlung von Software-Schwachstellen), fordert die Erstellung und Aufrechterhaltung eines Prozesses zur Annahme und Behandlung von Berichten über Softwareschwachstellen, einschließlich der Bereitstellung einer Möglichkeit für externe Stellen, diese zu melden. Der Prozess soll Punkte wie eine Richtlinie zum Umgang mit Schwachstellen, die den Meldeprozess, die verantwortliche Stelle für die Bearbeitung von Schwachstellenmeldungen und einen Prozess für die Aufnahme, Zuweisung, Behebung und Prüfung der Behebung beinhaltet, identifizieren. Verwenden Sie im Rahmen des Prozesses ein Schwachstellen-Tracking-System, das Schweregrade und Metriken zur Messung des Zeitpunkts der Identifizierung, Analyse und Behebung von Schwachstellen enthält. Überprüfen und aktualisieren Sie die Dokumentation jährlich oder wenn signifikante Unternehmensänderungen eintreten, die sich auf diesen Safeguard auswirken könnten.

Entwickler von Drittanwendungen sollten dies als eine nach außen gerichtete Richtlinie betrachten, die dazu beiträgt, die Erwartungen externer Stakeholder zu definieren. Keine Software ist *perfekt*. Selbst bei *sorgfältiger Entwicklung* und *ausführlichen Tests* können *Schwachstellen* in der Software *verbleiben* oder *nachträglich entdeckt* werden. Es ist *wichtig*, einen *Prozess* zu haben, um *Schwachstellenmeldungen* von *internen* und *externen* Quellen *entgegenzunehmen*, zu *bewerten* und zu *behandeln*.

Der Prozess sollte die folgenden Schritte umfassen:

1.  **Meldung:** Einrichtung eines *klaren* und *einfachen* Kanals für die *Meldung* von Schwachstellen, beispielsweise eine *E-Mail-Adresse*, ein *Webformular* oder ein *Bug-Bounty-Programm*.
2.  **Annahme:** *Bestätigung* des *Eingangs* der Meldung und *Information* des Melders über die *nächsten Schritte*.
3.  **Bewertung:** *Bewertung* der *Schwere* und der *Auswirkungen* der Schwachstelle.
4.  **Priorisierung:** *Priorisierung* der Behebung der Schwachstelle basierend auf ihrer *Schwere* und ihren *Auswirkungen*.
5.  **Behebung:** *Entwicklung* und *Testen* eines *Patches* oder einer *anderen Lösung* für die Schwachstelle.
6.  **Veröffentlichung:** *Veröffentlichung* des Patches und *Information* der Benutzer über die Schwachstelle und die verfügbare Lösung.
7.  **Überprüfung:** *Überprüfung*, ob die Schwachstelle *tatsächlich behoben* wurde und *keine neuen Schwachstellen* eingeführt wurden.

Dieser Prozess sollte durch ein *Schwachstellen-Tracking-System* unterstützt werden, das die *Verwaltung* von Schwachstellenmeldungen, die *Zuweisung* von Verantwortlichkeiten, die *Verfolgung* des Fortschritts und die *Dokumentation* der Behebungsmaßnahmen ermöglicht.

Safeguard 16.3, "Perform Root Cause Analysis on Security Vulnerabilities" (Durchführung einer Ursachenanalyse bei Sicherheitslücken), fordert die Durchführung einer Ursachenanalyse bei Sicherheitslücken. Bei der Überprüfung von Schwachstellen ist die Ursachenanalyse die Aufgabe, die zugrunde liegenden Probleme, die Schwachstellen im Code verursachen, zu bewerten und es den Entwicklungsteams zu ermöglichen, über die reine Behebung einzelner Schwachstellen hinauszugehen. Es geht *nicht nur* darum, die *Symptome* zu behandeln (d.h. die *einzelne Schwachstelle* zu beheben), sondern auch darum, die *Ursachen* zu verstehen (d.h. *warum* die Schwachstelle *entstanden* ist) und *Maßnahmen* zu ergreifen, um zu *verhindern*, dass *ähnliche Schwachstellen* in *Zukunft* auftreten.

Die Ursachenanalyse kann beispielsweise folgende Fragen beantworten:

*   Wurde die Schwachstelle durch einen *Fehler* in der *Programmierung* verursacht?
*   Wurde die Schwachstelle durch ein *fehlerhaftes Design* verursacht?
*   Wurde die Schwachstelle durch eine *fehlende* oder *unzureichende* *Sicherheitsmaßnahme* verursacht?
*   Wurde die Schwachstelle durch eine *mangelnde Schulung* der Entwickler verursacht?
*   Wurde die Schwachstelle durch einen *fehlerhaften Prozess* verursacht?

Die Ergebnisse der Ursachenanalyse sollten verwendet werden, um den *sicheren Anwendungsentwicklungsprozess* (siehe Safeguard 16.1) zu *verbessern*, beispielsweise durch *Anpassung* der *Programmierrichtlinien*, *Verbesserung* der *Schulungsmaßnahmen* oder *Änderung* der *Testverfahren*.

Safeguard 16.4, "Establish and Manage an Inventory of Third-Party Software Components" (Erstellung und Verwaltung eines Inventars von Drittanbieter-Softwarekomponenten), fordert die Erstellung und Verwaltung eines aktuellen Inventars der in der Entwicklung verwendeten Drittanbieterkomponenten, oft als "Stückliste" bezeichnet, sowie der für die zukünftige Verwendung vorgesehenen Komponenten. Dieses Inventar soll alle Risiken, die jede Drittanbieterkomponente darstellen könnte, enthalten. Bewerten Sie die Liste mindestens monatlich, um Änderungen oder Aktualisierungen dieser Komponenten zu identifizieren und zu überprüfen, ob die Komponente noch unterstützt wird. Moderne Software wird selten *von Grund auf neu* geschrieben. Stattdessen werden häufig *Drittanbieter-Komponenten* verwendet, beispielsweise *Bibliotheken*, *Frameworks* oder *Module*. Diese Komponenten können die *Entwicklung* von Software *beschleunigen* und *vereinfachen*, aber sie können auch *Sicherheitsrisiken* bergen.

Drittanbieter-Komponenten können *Schwachstellen* enthalten, die von Angreifern *ausgenutzt* werden können. Sie können auch *absichtlich* oder *unabsichtlich* *bösartigen Code* enthalten. Es ist daher *wichtig*, einen *Überblick* über *alle* verwendeten Drittanbieter-Komponenten zu haben und ihre *Sicherheit* zu *überwachen*.

Das Inventar der Drittanbieter-Komponenten sollte mindestens die folgenden Informationen enthalten:

*   **Name** der Komponente
*   **Version** der Komponente
*   **Herausgeber** der Komponente
*   **Bezugsquelle** der Komponente (wie Website, Repository)
*   **Lizenz** der Komponente
*   **Bekannte Schwachstellen** in der Komponente
*   **Verfügbare Updates** für die Komponente

Dieses Inventar sollte *regelmäßig aktualisiert* werden, um sicherzustellen, dass es den *aktuellen Stand* der verwendeten Komponenten widerspiegelt. Es sollte auch *automatisiert* werden, beispielsweise durch den Einsatz von *Software Composition Analysis (SCA)*-Tools, die den *Code* der Anwendung *scannen* und *automatisch* die verwendeten Drittanbieter-Komponenten und ihre *Versionen* identifizieren.

Safeguard 16.5, "Use Up-to-Date and Trusted Third-Party Software Components" (Verwendung aktueller und vertrauenswürdiger Drittanbieter-Softwarekomponenten), fordert die Verwendung aktueller und vertrauenswürdiger Drittanbieter-Softwarekomponenten. Wählen Sie nach Möglichkeit etablierte und bewährte Frameworks und Bibliotheken, die eine angemessene Sicherheit bieten. Erwerben Sie diese Komponenten von vertrauenswürdigen Quellen oder bewerten Sie die Software vor der Verwendung auf Schwachstellen. Die Verwendung *aktueller* und *vertrauenswürdiger* Drittanbieter-Komponenten ist *entscheidend*, um das *Risiko* von *Schwachstellen* in der Anwendung zu *minimieren*.

*"Aktuell"* bedeutet, dass die *neueste Version* der Komponente verwendet werden sollte, die alle *verfügbaren Sicherheitsupdates* und *Patches* enthält. *"Vertrauenswürdig"* bedeutet, dass die Komponente von einer *zuverlässigen Quelle* bezogen werden sollte, beispielsweise von der *offiziellen Website* des Herstellers oder von einem *vertrauenswürdigen Repository*.

Es ist auch wichtig, die *Reputation* der Komponente und ihres Herstellers zu *berücksichtigen*. Gibt es *bekannte Sicherheitsprobleme* mit der Komponente? Ist der Hersteller *bekannt* dafür, *schnell* auf *Schwachstellenmeldungen* zu *reagieren* und *Patches* bereitzustellen?

Safeguard 16.6, "Establish and Maintain a Severity Rating System and Process for Application Vulnerabilities" (Erstellung und Pflege eines Schweregrad-Bewertungssystems und -prozesses für Anwendungsschwachstellen), fordert die Erstellung und Pflege eines Schweregrad-Bewertungssystems und -prozesses für Anwendungsschwachstellen, das die Priorisierung der Reihenfolge, in der entdeckte Schwachstellen behoben werden, erleichtert. Dieser Prozess umfasst die Festlegung eines Mindestmaßes an Sicherheitsakzeptanz für die Freigabe von Code oder Anwendungen. Schweregrade bieten eine systematische Methode zur Triage von Schwachstellen, die das Risikomanagement verbessert und dazu beiträgt, dass die schwerwiegendsten Fehler zuerst behoben werden. Überprüfen und aktualisieren Sie das System und den Prozess jährlich.

Nicht alle Schwachstellen sind *gleich kritisch*. Einige Schwachstellen sind *leichter auszunutzen* als andere, und einige Schwachstellen haben *größere Auswirkungen* als andere. Ein *Schweregrad-Bewertungssystem* ermöglicht es, Schwachstellen nach ihrer *Kritikalität* zu *klassifizieren* und die *Behebung* der Schwachstellen entsprechend zu *priorisieren*.

Ein typisches Schweregrad-Bewertungssystem könnte beispielsweise die folgenden Kategorien umfassen:

*   **Kritisch:** Schwachstellen, die *leicht ausgenutzt* werden können und *schwerwiegende Auswirkungen* haben, beispielsweise die *Kompromittierung* des *gesamten Systems* oder den *Verlust sensibler Daten*.
*   **Hoch:** Schwachstellen, die *ausgenutzt* werden können und *erhebliche Auswirkungen* haben, beispielsweise die *Kompromittierung* *einzelner Benutzerkonten* oder die *Offenlegung* *vertraulicher Informationen*.
*   **Mittel:** Schwachstellen, die *schwerer ausgenutzt* werden können oder *geringere Auswirkungen* haben, beispielsweise die *Verursachung* von *Denial-of-Service-Angriffen* oder die *Manipulation* von *nicht sensiblen Daten*.
*   **Niedrig:** Schwachstellen, die *sehr schwer ausgenutzt* werden können oder *minimale Auswirkungen* haben, beispielsweise die *Offenlegung* von *nicht sensiblen Informationen* oder die *Verursachung* von *kleineren Funktionsstörungen*.

Die Bewertung der Schwere einer Schwachstelle sollte *objektiv* und *konsistent* erfolgen. Es gibt *verschiedene Standards* und *Methoden* für die Schweregradbewertung, beispielsweise das *Common Vulnerability Scoring System (CVSS)*.

Safeguard 16.7, "Use Standard Hardening Configuration Templates for Application Infrastructure" (Verwendung von Standard-Härtungskonfigurationsvorlagen für die Anwendungsinfrastruktur), fordert die Verwendung von standardisierten, von der Industrie empfohlenen Härtungskonfigurationsvorlagen für Komponenten der Anwendungsinfrastruktur. Dazu gehören die zugrunde liegenden Server, Datenbanken und Webserver und gelten für Cloud-Container, Platform as a Service (PaaS)-Komponenten und SaaS-Komponenten. Erlauben Sie nicht, dass intern entwickelte Software die Konfigurationshärtung schwächt. Die *sichere Konfiguration* der *Infrastruktur*, auf der die Anwendung läuft, ist *ebenso wichtig* wie die *sichere Entwicklung* der Anwendung selbst.

Die *Infrastruktur* umfasst beispielsweise die *Server*, die *Datenbanken*, die *Webserver*, die *Netzkomponenten* und die *Cloud-Dienste*, die für den *Betrieb* der Anwendung verwendet werden. Diese Komponenten sollten *sicher konfiguriert* werden, um das *Risiko* von *Angriffen* zu *minimieren*.

*"Härtung"* bedeutet, dass *unnötige Dienste* und *Funktionen* *deaktiviert* werden, *Standardpasswörter* *geändert* werden, *Sicherheitsfunktionen* *aktiviert* werden und *Zugriffsrechte* *eingeschränkt* werden. Es gibt *verschiedene Standards* und *Empfehlungen* für die Härtung von Infrastrukturkomponenten, beispielsweise die *CIS Benchmarks*, die *DISA STIGs* und die *Empfehlungen* der *Hersteller*.

Safeguard 16.8, "Separate Production and Non-Production Systems" (Trennung von Produktions- und Nicht-Produktionssystemen), fordert die Aufrechterhaltung getrennter Umgebungen für Produktions- und Nicht-Produktionssysteme. Die *Trennung* von *Produktions-* und *Nicht-Produktionssystemen* (wie Entwicklungs-, Test- und Staging-Systemen) ist eine *grundlegende Sicherheitsmaßnahme*.

*Produktionssysteme* sind die Systeme, die die *aktive Anwendung* hosten und von den *Benutzern* verwendet werden. *Nicht-Produktionssysteme* sind die Systeme, die für die *Entwicklung*, das *Testen* und die *Vorbereitung* der Anwendung verwendet werden.

Wenn *Produktions-* und *Nicht-Produktionssysteme* *nicht getrennt* sind, besteht das *Risiko*, dass *Fehler* oder *Schwachstellen* in *Nicht-Produktionssystemen* die *Sicherheit* der *Produktionssysteme* *beeinträchtigen*. Beispielsweise könnte ein Angreifer, der Zugriff auf ein *Entwicklungssystem* erlangt, möglicherweise *schädlichen Code* in die Anwendung *einschleusen*, der dann auf das *Produktionssystem* übertragen wird.

Die Trennung kann *physisch* erfolgen (wie durch Verwendung *separater Server* und *Netze*) oder *logisch* (wie durch Verwendung *virtueller Maschinen*, *Container* oder *Cloud-Umgebungen*).

Safeguard 16.9, "Train Developers in Application Security Concepts and Secure Coding" (Schulung von Entwicklern in Anwendungssicherheitskonzepten und sicherer Programmierung), fordert, dass alle Mitarbeiter der Softwareentwicklung eine Schulung im Schreiben von sicherem Code für ihre spezifische Entwicklungsumgebung und Verantwortlichkeiten erhalten. Die Schulung kann allgemeine Sicherheitsprinzipien und bewährte Praktiken der Anwendungssicherheit umfassen. Führen Sie die Schulung mindestens jährlich durch und gestalten Sie sie so, dass die Sicherheit innerhalb des Entwicklungsteams gefördert und eine Sicherheitskultur unter den Entwicklern aufgebaut wird. Die *Schulung* der Entwickler in *sicherer Programmierung* ist *entscheidend*, um zu *verhindern*, dass *Schwachstellen* überhaupt erst in den *Code* gelangen.

Die Schulung sollte die folgenden Themen umfassen:

*   **Grundlagen der Anwendungssicherheit:** Bedrohungen, Risiken, Schwachstellen, Angriffe.
*   **Sichere Programmierprinzipien:** Eingabevalidierung, Fehlerbehandlung, sichere Verwendung von Kryptographie, Schutz vor Pufferüberläufen, Schutz vor Injection-Angriffen, Schutz vor Cross-Site-Scripting (XSS), Schutz vor Cross-Site Request Forgery (CSRF), sichere Authentifizierung und Autorisierung, sichere Konfiguration.
*   **Spezifische Schwachstellen:** OWASP Top 10, SANS Top 25.
*   **Sichere Entwicklungswerkzeuge und -methoden:** Code-Reviews, statische Code-Analyse, dynamische Code-Analyse, Fuzzing.
*   **Sicherer Umgang mit Drittanbieter-Komponenten.**
*   **Reaktion auf Sicherheitsvorfälle.**

Die Schulung sollte *regelmäßig* durchgeführt werden, um sicherzustellen, dass die Entwickler über die *neuesten Bedrohungen* und *sicheren Programmierpraktiken* *informiert* sind. Sie sollte auch *praxisorientiert* sein und *Beispiele* für *echte Schwachstellen* und *Angriffe* enthalten.

Safeguard 16.10, "Apply Secure Design Principles in Application Architectures" (Anwendung sicherer Designprinzipien in Anwendungsarchitekturen), fordert die Anwendung sicherer Designprinzipien in Anwendungsarchitekturen. Sichere Designprinzipien umfassen das Konzept der geringsten Rechte und die Durchsetzung von Vermittlung, um jede Operation, die der Benutzer durchführt, zu validieren, wodurch das Konzept "vertraue niemals Benutzereingaben" gefördert wird. Beispiele hierfür sind die Sicherstellung, dass eine explizite Fehlerprüfung für alle Eingaben durchgeführt und dokumentiert wird, einschließlich Größe, Datentyp und akzeptabler Bereiche oder Formate. Sicheres Design bedeutet auch, die Angriffsfläche der Anwendungsinfrastruktur zu minimieren, z. B. durch Deaktivieren ungeschützter Ports und Dienste, Entfernen unnötiger Programme und Dateien und Umbenennen oder Entfernen von Standardkonten. Sicherheit sollte nicht als nachträglicher Gedanke betrachtet werden, sondern von Anfang an integraler Bestandteil des Designs sein.

Safeguard 16.11, "Leverage Vetted Modules or Services for Application Security Components" (Nutzung geprüfter Module oder Dienste für Anwendungssicherheitskomponenten), fordert die Nutzung geprüfter Module oder Dienste für Anwendungssicherheitskomponenten, wie z. B. Identitätsmanagement, Verschlüsselung und Überwachung und Protokollierung. Die Verwendung von Plattformfunktionen in kritischen Sicherheitsfunktionen reduziert den Arbeitsaufwand der Entwickler und minimiert die Wahrscheinlichkeit von Design- oder Implementierungsfehlern. Moderne Betriebssysteme bieten effektive Mechanismen zur Identifizierung, Authentifizierung und Autorisierung und stellen diese Mechanismen Anwendungen zur Verfügung. Verwenden Sie nur standardisierte, aktuell akzeptierte und umfassend überprüfte Verschlüsselungsalgorithmen. Betriebssysteme bieten auch Mechanismen zum Erstellen und Pflegen sicherer Audit-Logs. Anstatt eigene Sicherheitskomponenten zu entwickeln, sollten Entwickler, wann immer möglich, auf bewährte und sichere Lösungen zurückgreifen.

Safeguard 16.12, "Implement Code-Level Security Checks" (Implementierung von Sicherheitsprüfungen auf Code-Ebene), fordert die Anwendung statischer und dynamischer Analysetools innerhalb des Anwendungslebenszyklus, um zu überprüfen, ob sichere Programmierpraktiken eingehalten werden. Die Überprüfung des Codes auf Sicherheitsschwachstellen sollte nicht erst am Ende des Entwicklungsprozesses erfolgen, sondern kontinuierlich während der gesamten Entwicklung. Statische Code-Analyse-Tools untersuchen den *Quellcode* der Anwendung auf *bekannte Schwachstellenmuster*, *ohne* den Code *auszuführen*. Dynamische Code-Analyse-Tools *testen* die Anwendung *während der Laufzeit*, um *Schwachstellen* zu *identifizieren*, die *nicht* durch *statische Analyse* erkannt werden können.

Safeguard 16.13, "Conduct Application Penetration Testing" (Durchführung von Anwendungs-Penetrationstests), fordert die Durchführung von Anwendungs-Penetrationstests. Für kritische Anwendungen ist authentifiziertes Penetration Testing besser geeignet, um Geschäftslogik-Schwachstellen zu finden, als Code-Scanning und automatisiertes Sicherheitstesten. Penetrationstests basieren auf der Fähigkeit des Testers, eine Anwendung manuell als authentifizierter und nicht authentifizierter Benutzer zu manipulieren. Penetrationstests sind *simulierte Angriffe* auf die Anwendung, die von *speziell geschulten Testern* durchgeführt werden. Das Ziel ist, *Schwachstellen* in der Anwendung zu *identifizieren*, *bevor* sie von *echten Angreifern* ausgenutzt werden können.

Safeguard 16.14, "Conduct Threat Modeling" (Durchführung von Bedrohungsmodellierung), fordert die Durchführung von Bedrohungsmodellierung (Threat Modeling). Bedrohungsmodellierung ist der Prozess der Identifizierung und Behebung von Sicherheitsdesignfehlern in Anwendungen innerhalb eines Designs, bevor Code erstellt wird. Sie wird von speziell geschulten Personen durchgeführt, die das Anwendungsdesign bewerten und Sicherheitsrisiken für jeden Einstiegspunkt und jede Zugriffsebene abschätzen. Das Ziel ist, die Anwendung, Architektur und Infrastruktur strukturiert abzubilden, um ihre Schwächen zu verstehen. Bedrohungsmodellierung ist ein *proaktiver Ansatz*, um *Sicherheitsrisiken* in der *Entwurfsphase* der Anwendung zu *identifizieren* und zu *beheben*. Es ist ein *systematischer Prozess*, der die *potenziellen Bedrohungen* für die Anwendung, die *wahrscheinlichen Angriffsvektoren* und die *möglichen Auswirkungen* von Angriffen *analysiert*.

Die Sicherheit von Anwendungssoftware ist ein *kontinuierlicher Prozess*. Neue Bedrohungen tauchen *ständig* auf, und Angreifer entwickeln *ständig* neue Techniken, um Sicherheitsmaßnahmen zu umgehen. Daher müssen die Sicherheitsmaßnahmen für Anwendungen *regelmäßig überprüft* und *angepasst* werden. Die *verwendeten Tools* müssen *aktualisiert* werden, die *Konfigurationen* müssen *überprüft* und *gehärtet* werden, die *Entwicklungsprozesse* müssen *verbessert* werden, und die *Entwickler* müssen *kontinuierlich geschult* werden.

Die Sicherheit von Anwendungssoftware erfordert die *Zusammenarbeit* verschiedener Abteilungen und Rollen in der Organisation. Die *Entwickler* sind für die *Implementierung* der *sicheren Programmierpraktiken* verantwortlich. Die *Tester* sind für die *Überprüfung* der Anwendung auf *Sicherheitsschwachstellen* verantwortlich. Die *Sicherheitsabteilung* ist für die *Definition der Sicherheitsrichtlinien* und die *Überwachung der Einhaltung* verantwortlich. Und die *Geschäftsleitung* muss die *Bedeutung* der Anwendungssicherheit *erkennen* und die *notwendigen Ressourcen* bereitstellen.

Die Sicherheit von Anwendungssoftware ist ein *wesentlicher Bestandteil* einer *umfassenden Cybersicherheitsstrategie*. Sie schützt die *Anwendungen*, die *Herzstücke* der Geschäftsprozesse, vor *Angriffen* und *Störungen*. Die konsequente Anwendung der hier beschriebenen Prinzipien und Maßnahmen ist ein entscheidender Schritt zur Verbesserung der Cybersicherheit einer Organisation. Ein proaktiver und umfassender Ansatz zur Sicherheit von Anwendungssoftware minimiert das Risiko von Sicherheitsvorfällen und trägt dazu bei, die Integrität, Vertraulichkeit und Verfügbarkeit von IT-Systemen und Daten zu gewährleisten. Durch die Integration von Sicherheit in jeden Schritt des Softwareentwicklungszyklus, von der Planung bis zur Wartung, wird sichergestellt, dass Anwendungen robust und widerstandsfähig gegen Cyberbedrohungen sind.
