******************************************************************************
Systemhärtung - Komponenten für den Aufbau einer Linux Systemhärtung
******************************************************************************

.. sectionauthor:: Pierre Gronau <Pierre.Gronau@ndaal.eu>

.. contents:: Inhalt - Sicherheit - Container
   :depth: 4

.. _Section_Komponenten_Aufbau:

Aufbau einer Systemhärtung unter Debian Linux
------------------------------------------------------------------------------

In diesem Abschnitt werden die wesentlichen Komponenten, ihre Aufgaben und 
wie diese installiert werden aufgeführt und beschrieben.


.. figure::
   _static/git-repo2.png
   :scale: 50 %
   :alt: map to buried treasure

   Git Repo

.. _Section_Komponenten_Debian_Linux:

Debian Linux
==============================================================================

.. figure::
   _static/openlogo-nd-100.png
   :align: center
   :scale: 50 %
   :alt: Debian Linux

   Debian Linux


**Debian** [1]_ gilt als eine der stabilsten Linux-Distributionen. Es ist
bekannt für seine umfangreiche Testphase, bevor neue Pakete 
in die stabile Version aufgenommen werden.

Einsatzbereich: Ideal für Server und Desktop-Anwendungen.

Vorteile:

- Große Community und umfangreiche Dokumentation.
- Hohe Stabilität und Sicherheit.

Hier ist eine ausführliche Beschreibung der mindestens benötigten Komponenten
unter Debian Linux 12.9x oder höher:


.. Seealso::

   - :ref:`ADR - ADR-004 - Verwendung von Linux als Automatisierungsplattform für Systemhärtung <Section_ADR_ADR-004>`
   - :ref:`ADR - ADR-005 - Verwendung von Debian für die Systemhärtung von Red Hat 9 und deren Derivate <Section_ADR_ADR-005>`

Werkzeuge für GitOps
==============================================================================

GitOps nutzt verschiedene Werkzeuge, um eine automatisierte und 
versionskontrollierte Infrastrukturverwaltung zu ermöglichen. Zwei zentrale 
Werkzeuge in diesem Kontext sind **Git** und **Git LFS**:

.. figure::
   _static/git-repo2.png
   :align: center
   :scale: 25 %
   :alt: GitOps mit Git Repos

   GitOps mit Git Repos


.. Seealso::

   - :ref:`ADR - ADR-001 - GitOps-Prinzipien <Section_ADR_ADR-001>`

.. _Section_Komponenten_Git:

Werkzeug für GitOps - Git
##############################################################################

.. figure::
   _static/git-logo.png
   :align: center
   :scale: 50 %
   :alt: Werkzeug für GitOps - Git

   Werkzeug für GitOps - Git

Git ist das Fundament von GitOps und bietet folgende Funktionen:

- **Versionskontrolle**: Git protokolliert alle Änderungen am Code und 
  ermöglicht es, frühere Versionen wiederherzustellen.

- **Zusammenarbeit**: Entwickler und Administratoren können über 
  Pull-Requests Änderungen vorschlagen und diskutieren.

- **Single Source of Truth**: Das Git-Repository dient als zentrale, 
  verbindliche Quelle für den Infrastrukturcode.

- **Audit-Trail**: Jede Änderung wird mit Zeitstempel und Autor erfasst, 
  was die Nachverfolgbarkeit erhöht.

.. _Section_Komponenten_Git_LFS:

Werkzeug für GitOps - Git LFS (Large File Storage)
##############################################################################

Git LFS erweitert die Fähigkeiten von Git für große Dateien:

- **Effiziente Speicherung**: Große Dateien werden außerhalb 
  des Git-Repositories gespeichert und durch kleine Pointer-Dateien ersetzt.

- **Lazy Loading**: Große Dateien werden erst bei Bedarf heruntergeladen, 
  was Klonvorgänge beschleunigt.

- **Nahtlose Integration**: Entwickler können wie gewohnt mit Git arbeiten, 
  während Git LFS im Hintergrund die großen Dateien verwaltet.

- **Verbesserte Performance**: Reduziert die Größe des Git-Repositories 
  und beschleunigt Operationen wie Klonen und Pullen.

Diese Werkzeuge ermöglichen zusammen einen GitOps-Workflow, bei dem:

- Infrastruktur als Code im Git-Repository definiert wird.

- Änderungen über Pull-Requests vorgeschlagen und überprüft werden.

- Nach der Genehmigung automatisch die Live-Infrastruktur aktualisiert wird.

- Durch den Einsatz dieser Werkzeuge können Teams ihre Infrastruktur 
  genauso präzise und versioniert verwalten wie ihren Anwendungscode,
  was zu mehr Konsistenz, besserer Zusammenarbeit
  und erhöhter Automatisierung führt.

Werkzeug für GitOps - Git Repo Server
##############################################################################

Sie benötigen noch einen zentralen Git Repo Server wie:

- **GitHub**

   .. figure::
      _static/GitHub_logo.png
      :align: center
      :scale: 50 %
      :alt: Git Repo Server: GitHub

      Git Repo Server: GitHub


- **Gitlab**

   .. figure::
      _static/gitlab-icon-1024x942-f30d1qro.png
      :align: center
      :scale: 50 %
      :alt: Git Repo Server: Gitlab

      Git Repo Server: Gitlab


- **Bitbucket**

   .. figure::
      _static/que-es-bitbucket-1024x617.png
      :align: center
      :scale: 50 %
      :alt: Git Repo Server: Bitbucket

      Git Repo Server: Bitbucket

- etc.

Stellen sie sicher das mindestens bei jedem Commit täglich 
eine Datensicherung erstellt wird.

Git
##############################################################################

.. figure::
   _static/git-logo.png
   :align: center
   :scale: 50 %
   :alt: Werkzeug für GitOps - Git

   Werkzeug für GitOps - Git


**Git** [4]_ ist ein verteiltes Versionskontrollsystem, das die Zusammenarbeit
und Codeverwaltung in Softwareprojekten erleichtert.

*Installation auf Debian:*

bash

``sudo apt-get update``

``sudo apt-get -y install git``

Git LFS (Large File Storage)
##############################################################################

**Git LFS** [5]_ ist eine Erweiterung für Git, die große Dateien effizient
verwaltet, indem sie Dateiinhalte auf einem Remote-Server speichert
und durch Textzeiger im Git-Repository ersetzt.

*Installation auf Debian:*

bash

``sudo apt-get update``

``sudo apt-get -y install git-lfs``

``git lfs install``


GitOps-Prinzipien
##############################################################################

GitOps ist eine Sammlung von Prinzipien für den Betrieb und die Verwaltung 
von Softwaresystemen. Diese Prinzipien sind aus dem modernen Softwarebetrieb 
abgeleitet, basieren aber auf existierenden und verbreiteten Best Practices.

Der Soll-Zustand eines mittels GitOps verwalteten Systems muss folgende 
Eigenschaften erfüllen:

Deklarativ
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Der Soll-Zustand eines durch GitOps verwalteten Systems muss deklarativ 
beschrieben sein.

Versioniert und unveränderlich
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Der Soll-Zustand wird in einer Weise gespeichert, die Unveränderlichkeit 
sowie Versionierung erzwingt und die vollständige Historie erhält.

Automatisch bezogen
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Software-Agenten beziehen den beschriebenen Soll-Zustand automatisch.

Kontinuierlich angeglichen
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Software-Agenten beobachten den tatsächlichen Systemzustand und versuchen 
kontinuierlich, ihn dem Soll-Zustand anzugleichen.

Automation der Systemkonfiguration ohne Aufgabenautomatisierung
==============================================================================

Die Vorgabe hier war eine Systemhärtung ohne Agenten unter Linux.

Ansible
##############################################################################

.. figure::
   _static/Ansible_logo-700x700.png
   :align: center
   :scale: 25 %
   :alt: Ansible

   Ansible


**Ansible** [2]_ ist ein leistungsfähiges Automatisierungstool für
die Konfigurationsverwaltung, Anwendungsbereitstellung und
Aufgabenautomatisierung. Es verwendet eine einfache, YAML-basierte Sprache
zur Beschreibung von Systemkonfigurationen und Automatisierungsaufgaben.

*Installation auf Debian:*

bash

``sudo apt-get update``

``sudo apt-get -y install ansible``

bash
##############################################################################

.. figure::
   _static/bash-logo-by-vd.png
   :align: center
   :scale: 25 %
   :alt: bash

   bash

Die **Bash** (Bourne Again Shell) ist der Standard-Kommandointerpreter 
unter Linux und vielen Unix-Systemen. Sie dient als Benutzerschnittstelle, 
die es ermöglicht, Befehle einzugeben und auszuführen, 
um mit dem Betriebssystem zu interagieren. Die Bash bietet nicht 
nur eine interaktive Umgebung, sondern auch 
eine leistungsfähige Skriptsprache zur Automatisierung von Aufgaben 
und zur Erstellung komplexer Abläufe. Sie verfügt über zahlreiche 
nützliche Funktionen wie Befehlsvervollständigung, eine Befehlshistorie 
und die Möglichkeit, Ein- und Ausgaben umzuleiten46. Darüber hinaus 
ist die Bash abwärtskompatibel zur ursprünglichen Bourne-Shell und 
integriert viele nützliche Eigenschaften anderer Shells 
wie der Korn-Shell und der C-Shell.

VirtualBox für die Virtualisierung
==============================================================================

.. figure::
   _static/Virtualbox_logo.png
   :align: center
   :scale: 25 %
   :alt: VirtualBox für die Virtualisierung

   VirtualBox für die Virtualisierung


**VirtualBox** [9]_ ist eine leistungsstarke Virtualisierungssoftware, die
es ermöglicht, mehrere virtuelle Maschinen auf einem einzelnen Computer
zu betreiben.

*Installation auf Debian:*

bash

``sudo apt-get update``

``sudo apt-get -y install virtualbox``


.. Seealso::

   - :ref:`ADR - ADR-009 - Einsatz von VirtualBox als Virtualisierungslösung am Client <Section_ADR_ADR-009>`

Automation von virtuellen Maschinenumgebungen
==============================================================================

Um die Automation von virtuellen Maschinenumgebungen zu ermöglichen, bedienen 
wir uns der Open Source Lösung von Hash Corp nämlich Vagrant.

Vagrant
##############################################################################

.. figure::
   _static/vagrant-vertical-color.png
   :align: center
   :scale: 75 %
   :alt: Vagrant

   Vagrant


**Vagrant** [10]_ ist ein Werkzeug zur Erstellung und Verwaltung
von virtuellen Maschinenumgebungen. Es vereinfacht den Prozess
der Konfiguration und Bereitstellung von Umgebungen.

*Installation auf Debian:*

bash

``sudo apt-get update``

``sudo apt-get -y install vagrant``

Vagrant Plugin - vagrant-scp
##############################################################################

**Vagrant Plugin - vagrant-scp** [11]_ ist ein Werkzeug zur Erstellung und Verwaltung
von virtuellen Maschinenumgebungen. Es vereinfacht den Prozess
der Konfiguration und Bereitstellung von Umgebungen.

*Installation auf Debian:*

bash

``vagrant plugin install vagrant-scp``

Beispiel

.. code-block:: bash
   :caption: vagrant plugin install vagrant-scp

   vagrant_scp_check=0  # Change this to 1 to check the vagrant_scp plugin
   readonly vagrant_scp_check
   printf "Info: Current vagrant_scp_check: %s\n" "${vagrant_scp_check}"

   # Check if the vagrant-scp plugin is installed
   check_install_vagrant_scp() {
      if [ "${vagrant_scp_check}" -eq 1 ]; then
         if vagrant plugin list | grep -q 'vagrant-scp'; then
               vagrant plugin list
               printf "Info: vagrant-scp is already installed.\n"
               if vagrant plugin update vagrant-scp; then
                  printf "Info: vagrant-scp is updated.\n"
                  vagrant plugin list
               else
                  printf "Error: Failed to update vagrant-scp.\n"
                  return 1
               fi
         else
               if vagrant plugin install vagrant-scp; then
                  vagrant plugin list
                  printf "Info: vagrant-scp has been successfully installed.\n"
               else
                  printf "Error: Failed to install vagrant-scp. Please check your Vagrant setup.\n"
                  return 1
               fi
         fi
      fi

      # Success case
      return 0
   }

check_install_vagrant_scp "${vagrant_scp_check}"

Editor
==============================================================================

Wir regen an, als Ersatz für Visual Studio Code den datensparsamen VSCodium 
zu nutzen. Selbstverständlich steht einem die Wahl eines anderen Editors
frei. Wichtig hierbei das die Übertragung jeglicher Informationen wie Code, 
Einstellungen Geheimnisse, Nutzungsverhalten etc. zwingend unterbunden wird.

Editor VSCodium
##############################################################################

.. figure::
   _static/VSCodium.png
   :align: center
   :scale: 50 %
   :alt: Editor VSCodium

   Editor VSCodium


**VSCodium** [12]_ ist eine freie und quelloffene Version 
von Visual Studio Code (Editor) ohne Microsoft-spezifische Anpassungen 
und Telemetrie.

*Installation auf Debian:*

bash

.. code-block:: bash
   :caption: VSCodium Installation

   sudo apt-get update

   wget -qO - https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/master/pub.gpg | gpg --dearmor | sudo dd of=/usr/share/keyrings/vscodium-archive-keyring.gpg
   echo 'deb [ signed-by=/usr/share/keyrings/vscodium-archive-keyring.gpg ] https://download.vscodium.com/debs vscodium main' | sudo tee /etc/apt/sources.list.d/vscodium.list

   sudo apt-get -y install codium

**VSCodium** steht auch für andere Linux Derivate sowie macOS und Windows 
zur Verfügung.

Optional: Ansible VS Code Erweiterung von Red Hat
##############################################################################

.. figure::
   _static/vscode-ansible.png
   :align: center
   :scale: 25 %
   :alt: Ansible VS Code Erweiterung von Red Hat

   Ansible VS Code Erweiterung von Red Hat


Standardmäßig wird von Red Hat die Übertragung von Telemetriedaten aktiviert:

.. code-block:: text
   :caption: Übertragung von Telemetriedaten aktiviert

   Redhat › Telemetry: Enabled
   Enable usage data and errors to be sent to Red Hat servers


bash

.. code-block:: bash
   :caption: Übertragung von Telemetriedaten deaktivieren

   codium --user-data-dir ~/.config/VSCodium/User --cli-data-dir ~/.config/VSCodium/User --install-extension redhat.vscode-redhat-telemetry && codium --user-data-dir ~/.config/VSCodium/User --cli-data-dir ~/.config/VSCodium/User --config redhat.telemetry.enabled=false

Dieses Kommando führt Folgendes aus:

- Zuerst wird sichergestellt, dass die Red Hat Telemetry-Erweiterung installiert ist.

- Anschließend wird die Einstellung redhat.telemetry.enabled auf false gesetzt.

Die Optionen *--user-data-dir* und *--cli-data-dir* werden verwendet,
um das korrekte Konfigurationsverzeichnis für VSCodium anzugeben.

bash

.. code-block:: bash
   :caption: Neustart von VSCodium erforderlich

   codium --quit && codium


Optional: ansibug Erweiterung von jborean
##############################################################################

Ansible Playbook Debugger als Erweiterung für **VSCodium**
oder Visual Studio Code.

Werkzeuge zur Datenübertragung über verschiedene Netzprotokolle
==============================================================================

Die angeführten Werkzeuge in diesem Kapitel, dienen 
zur sicheren Datenübertragung über verschiedene Netzprotokolle.

wget
##############################################################################

.. figure::
   _static/logo_wget.png
   :align: center
   :scale: 50 %
   :alt: Werkzeuge zur Datenübertragung über verschiedene Netzprotokolle - wget

   Werkzeuge zur Datenübertragung über verschiedene Netzprotokolle - wget


**Wget** [13]_ ist ein leistungsfähiges Kommandozeilenprogramm zum Herunterladen
von Dateien aus dem Internet. Es wurde vom GNU-Projekt entwickelt und ist
auf vielen Unix-basierten Systemen, einschließlich Linux, verfügbar.

Hauptfunktionen

- Unterstützt HTTP-, HTTPS- und FTP-Protokolle sowie den Abruf über HTTP-Proxys
- Kann Links in HTML-, XHTML- und CSS-Seiten verfolgen1
- Ermöglicht das Erstellen lokaler Versionen von entfernten Websites mit vollständiger Wiederherstellung der Verzeichnisstruktur1
- Beachtet den Robot Exclusion Standard (/robots.txt)1
- Kann Links in heruntergeladenen Dateien für Offline-Betrachtung umwandeln


*Installation auf Debian:*

bash

``sudo apt-get update``

``sudo apt-get -y install wget``

Beispiel

wget --secure-protocol=auto --https-only --continue --verbose -N --tries=10 --check-certificate ...

cURL
##############################################################################

.. figure::
   _static/curl-logo.png
   :align: center
   :scale: 20 %
   :alt: Werkzeuge zur Datenübertragung über verschiedene Netzprotokolle - cURL

   Werkzeuge zur Datenübertragung über verschiedene Netzprotokolle - cURL


**cURL** [14]_ (Client URL) ist ein vielseitiges Kommandozeilenwerkzeug
zur Datenübertragung über verschiedene Netzprotokolle. Es wird häufig
für HTTP-Anfragen und API-Tests verwendet.

Hauptfunktionen von cURL:

- Unterstützt zahlreiche Protokolle wie HTTP, HTTPS, FTP
- Ermöglicht GET, POST, PUT, DELETE und andere HTTP-Methoden
- Kann Header und Cookies setzen
- Unterstützt Datei-Downloads und -Uploads

*Installation auf Debian:*

bash

``sudo apt-get update``

``sudo apt-get -y install curl``

Suchen nach Muster - Mustererkennung
==============================================================================

Die hier in diesem Kapitel aufgelisteten Werkzeuge, unterstützen sie Muster 
zu erkennen.

grep
##############################################################################

**grep** [15]_ ist ein Kommandozeilenwerkzeug, das verwendet wird, um in Dateien
nach bestimmten Textmustern zu suchen. Der Name steht für
"global regular expression print". Es ermöglicht die Suche
nach regulären Ausdrücken und gibt die Zeilen aus, die mit dem Suchmuster
übereinstimmen. **grep** ist besonders nützlich für das Filtern von Ausgaben
und das Durchsuchen von Protokolldateien.

*Installation auf Debian:*

bash

``sudo apt-get update``

``sudo apt-get -y install grep``

egrep
##############################################################################

**egrep** [16]_ ist eine erweiterte Version von grep, die die Verwendung
von erweiterten regulären Ausdrücken (ERE) unterstützt. Es ist identisch
mit dem Befehl grep -E. Mit egrep können komplexere Suchmuster verwendet
werden, was es zu einem leistungsstarken Werkzeug für die Textverarbeitung
macht.

*Installation auf Debian:*

bash

``sudo apt-get update``

``sudo apt-get -y install grep``

ripgrep
##############################################################################

**ripgrep** [17]_ (kurz rg) ist ein modernes Suchwerkzeug, das besonders 
schnell ist und eine benutzerfreundliche Syntax bietet. Es kombiniert 
die Funktionalität von grep, ack und ag, indem es rekursiv in Verzeichnissen 
nach Mustern sucht und dabei standardmäßig ignorierte Dateien wie 
*.gitignore* berücksichtigt. **ripgrep** ist bekannt 
für seine hohe Geschwindigkeit und Effizienz bei der Suche in großen Codebasen.

*Installation auf Debian:*

bash

``sudo apt-get update``

``sudo apt-get -y install ripgrep``

Schadsoftwareerkennung
==============================================================================

In diesem Kapitel sind die Werkzeuge aufgelistet, um im Entsteheungsprozess 
bereits Schadsoftware zu erkennen.

ClamAV
##############################################################################

.. figure::
   _static/ClamAV_logo.png
   :align: center
   :scale: 40 %
   :alt: Schadsoftwareerkennung mit ClamAV

   Schadsoftwareerkennung mit ClamAV


**ClamAV** [18]_ ist ein quelloffener Virenscanner, der unter der GPL lizenziert
ist. Er wurde entwickelt, um Trojaner, Viren, Malware und andere schädliche
Software zu erkennen. ClamAV wird häufig zur Überprüfung von E-Mails
auf Viren eingesetzt und ist in der Lage, Viren aller Betriebssysteme
zu erkennen, da die Signaturdatenbank eine Vielzahl von Bedrohungen abdeckt.
ClamAV besteht aus mehreren Komponenten:

- **clamd**: Der Daemon, der von anderen Programmen verwendet wird,
  um Dateien auf Viren zu prüfen.

- **freshclam**: Ein Programm zur Aktualisierung der Virensignaturen.

- **clamscan**: Ein Kommandozeilenwerkzeug zur Überprüfung von Dateien
  und Verzeichnissen auf Viren.

*Installation auf Debian:*

bash

``sudo apt-get update``

``sudo apt-get -y install clamav clamav-daemon``

Verwendung: Um die Virendatenbank manuell zu aktualisieren, verwenden Sie den folgenden Befehl:

bash

``sudo freshclam``

Um eine Datei oder ein Verzeichnis auf Viren zu scannen, verwenden Sie:

bash

``clamscan [Optionen] [Datei/Verzeichnis]``

Beispiele für Optionen:

    --infected: Gibt nur infizierte Dateien aus.
    --remove: Entfernt infizierte Dateien.
    -r: Scannt Unterverzeichnisse.

Um zu testen, ob ClamAV funktioniert, können Sie die EICAR-Testdatei verwenden:

bash

.. code-block:: bash
   :caption: EICAR

    # Define the EICAR test string
    # shellcheck disable=SC2016
    input_string='X5O!P%@AP[4\PZX54(P^)7CC)7}$EICAR-STANDARD-ANTIVIRUS-TEST-FILE!$H+H*'
    readonly input_string

    # Define the EICAR file name
    eicar_file="eicar.txt"
    readonly eicar_file

    # Create a text file with the EICAR test string and ensure it ends with a newline
    printf '%s\n' "${input_string}" > "${eicar_file}"

    if [ ! -f "${eicar_file}" ]; then
        printf "Error: %s not found. Exiting.\n" "${eicar_file}"
        exit 1
    fi

    # Define the EICAR file name
    eicar_printf_file="EICAR_testfile_created_with_printf.txt"
    readonly eicar_printf_file

    printf '%s\n' 'K5B!C%@NC[4\CMK54(C^)7PP)7}$RVPNE-FGNAQNEQ-NAGVIVEHF-GRFG-SVYR!$U+U*' | tr '[A-Za-z]' '[N-ZA-Mn-za-m]' > "${eicar_printf_file}"

    if [ ! -f "${eicar_printf_file}" ]; then
        printf "Error: %s not found. Exiting.\n" "${eicar_printf_file}"
        exit 1
    fi

    clamscan testsig.txt

ClamAV ist ein effektives Werkzeug zur Erkennung und Bekämpfung von Malware 
und bietet eine solide Grundlage für die Sicherheit Ihres Systems.


.. Seealso::

   - :ref:`ADR - ADR-010 - Einsatz von ClamAV zur Erkennung von Schadsoftware <Section_ADR_ADR-010>`

YARA
##############################################################################

;.. figure::
   _static/yaralogo.jpg
   :align: center
   :scale: 50 %
   :alt: Schadsoftwareerkennung mit YARA

   Schadsoftwareerkennung mit YARA


**YARA** [21]_ ist ein quelloffenes Framework zur Mustererkennung, das
von VirusTotal entwickelt wurde. Es wird hauptsächlich zur Identifizierung
und Klassifizierung von Schadsoftware verwendet. Mit YARA können
Sicherheitsforscher Regeln erstellen, die es ermöglichen, Dateien und
Prozesse nach bestimmten Text- oder Binärmuster zu durchsuchen. Die Regeln
sind in einer C-ähnlichen Syntax verfasst und bieten eine flexible
Möglichkeit zur Erkennung von Malware-Familien.

*Installation auf Debian:*

bash

``sudo apt-get update``

``sudo apt-get -y install yara``

Verwendung: Um YARA zu verwenden, müssen Sie Regeln definieren.
Hier ist ein einfaches Beispiel für eine YARA-Regel:

text

.. code-block:: text

    rule ExampleRule {
        strings:
            $a = "example_string"
        condition:
            $a
    }


Diese Regel sucht nach der Zeichenkette **example_string**
in den gescannten Dateien.

YARA-X
##############################################################################

;.. figure::
   ;_static/yara_icon_131858.png
   :align: center
   :scale: 50 %
   :alt: YARA-X

   YARA-X


**YARA-X** [22]_ ist eine erweiterte Version von YARA, die zusätzliche Funktionen und
eine benutzerfreundliche Oberfläche bietet. Es ermöglicht
die einfache Verwaltung und Ausführung von YARA-Regeln sowie die Analyse
von Ergebnissen in einer grafischen Benutzeroberfläche. YARA-X richtet sich
an Sicherheitsanalysten, die eine leistungsfähige Lösung zur Erkennung
von Bedrohungen benötigen.

*Installation auf Debian:*

bash

``cargo install yara-x``

Verwendung: Nach der Installation können Sie YARA-X starten und 
Ihre YARA-Regeln über die Benutzeroberfläche verwalten. Sie können Dateien 
scannen und die Ergebnisse der Regelanwendungen in Echtzeit anzeigen.

Speicher resistente Systemprogrammiersprache
==============================================================================

Annahme: Ein Nutzung von Java als Grundlage von Programmcode ist untersagt.

Rust
##############################################################################

;.. figure::
   ;_static/rust-social-wide-light.svg.png
   :align: center
   :scale: 50 %
   :alt: Rust

   Rust

**Rust** [19]_ ist eine moderne, multiparadigmen-Systemprogrammiersprache,
die von der Open-Source-Community entwickelt wird und unter anderem
von Mozilla Research unterstützt wird. Die Sprache wurde mit dem Ziel
entwickelt, sicher, nebenläufig und performant zu sein. Rust legt
besonderen Wert auf Speichersicherheit und die Vermeidung
von Programmfehlern, die zu Speicherzugriffsfehlern oder
Pufferüberläufen führen können.

*Installation auf Debian:*

bash

``curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh``

Nach der Installation können Sie den Rust-Compiler und Cargo verwenden, um Projekte zu erstellen und zu verwalten.

Paketverwaltung mit Cargo
##############################################################################

.. figure::
   _static/maxresdefault.jpg
   :align: center
   :scale: 25 %
   :alt: Paketverwaltung mit Cargo

   Paketverwaltung mit Cargo

**Cargo** [20]_ ist das offizielle Paketverwaltungssystem und Build-Tool für Rust.
Es ermöglicht Entwicklern, ihre Projekte zu erstellen, Abhängigkeiten
zu verwalten und Pakete einfach zu veröffentlichen. Cargo sorgt dafür,
dass alle benötigten Bibliotheken automatisch heruntergeladen und
in das Projekt integriert werden. *Installation auf Debian:* Cargo wird
normalerweise zusammen mit Rust installiert. Wenn Sie Rust über rustup
installieren, wird Cargo automatisch mitinstalliert. Alternativ können Sie
Cargo auch direkt aus den Paketquellen installieren:

bash

``sudo apt-get update``

``sudo apt-get -y install cargo``

Verwendung: Um ein neues Rust-Projekt mit Cargo zu erstellen, verwenden Sie den folgenden Befehl:

bash

``cargo new projektname``

Um das Projekt zu kompilieren und auszuführen, navigieren Sie in das Projektverzeichnis und verwenden Sie:

bash

``cd projektname``

``cargo run``

Cargo bietet auch viele Funktionen zur Verwaltung von Abhängigkeiten und zur Automatisierung des Build-Prozesses. Diese Beschreibungen bieten eine klare Übersicht über die Funktionen und Installationsanweisungen für Rust und Cargo in deutscher Sprache.


Packer
==============================================================================

;.. figure::
   _static/packer_logo-freelogovectors.net_-640x400.png
   :align: center
   :scale: 50 %
   :alt: Packer

   Packer


**Packer** [23]_ ist ein Open-Source-Tool zur Erstellung
identischer Maschinenimages für mehrere Plattformen
aus einer einzigen Quellkonfiguration.

Hauptmerkmale:

- Automatisierte Erstellung von Maschinenimages
- Unterstützung für verschiedene Cloud-Anbieter und Virtualisierungsplattformen
- Konfiguration über eine einheitliche JSON-Syntax
- Parallele Erstellung von Images für mehrere Plattformen
- Integration mit Configuration-Management-Tools

*Installation auf Debian:*

bash

``curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -``

``sudo apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"``

``sudo apt-get update && sudo apt-get install packer``

Statische Code Analyse
==============================================================================

Die in diesem Kapitel angeführten Werkzeuge dienen 
zur statischen Code Analyse.

Ansible-lint
##############################################################################

.. figure::
   _static/checkov_blue_logo.png
   :align: center
   :scale: 20 %
   :alt: Statische Code Analyse mit Checkov

   Statische Code Analyse mit Checkov

**Ansible-lint** [3]_ ist ein Werkzeug zur Überprüfung von Ansible Playbooks
auf bewährte Praktiken und potenzielle Probleme. Es hilft dabei, konsistente
und qualitativ hochwertige Ansible-Code zu erstellen.

*Installation auf Debian:*

bash

``sudo apt-get update``

``sudo apt-get -y install ansible-lint``

Shellcheck
##############################################################################

;.. figure::
   ;_static/shellcheck_logo.svg.png
   :align: center
   :scale: 20 %
   :alt: Statische Code Analyse mit Checkov

   Statische Code Analyse mit Checkov

**Shellcheck** [6]_ ist ein statisches Analysetool für Shell-Skripte, 
das Warnungen und Vorschläge zur Verbesserung von Shell-Skripten bietet.

*Installation auf Debian:*

bash
 
``sudo apt-get update``

``sudo apt-get -y install shellcheck``

shfmt
##############################################################################

**shfmt** [7]_ ist ein Shell-Formatierer, der Shell-Skripte automatisch 
formatiert und konsistent gestaltet.

*Installation auf Debian:*

bash

``sudo apt-get update``

``sudo apt-get -y install shfmt``

shellharden
##############################################################################

**Shellharden** [8]_ ist ein Open-Source-Tool zur automatischen Verbesserung
der Sicherheit und Zuverlässigkeit von Shell-Skripten.

Hauptmerkmale:

- Automatische Korrektur von unsicheren Shell-Praktiken

- Unterstützung für verschiedene Shell-Dialekte wie Bash und POSIX

- Konfiguration über Kommandozeilenoptionen

- Parallele Verarbeitung mehrerer Skripte

- Integration mit Entwicklungsumgebungen und CI/CD-Pipelines

*Installation auf Debian:*

bash

``sudo apt-get update``

``sudo apt-get -y install cargo``

``cargo install shellharden``

GOSS (Go Server Spec)
##############################################################################

**GOSS (Go Server Spec)** ist ein YAML-basiertes Tool, das als Alternative
zu *Serverspec* dient und zur **Validierung der Konfiguration** eines Servers
verwendet wird. Es wurde entwickelt, um den Prozess des Schreibens
von Tests zu erleichtern, indem es dem Benutzer ermöglicht, Tests
aus dem aktuellen Systemzustand zu generieren.


.. Seealso::

   - :ref:`Überprüfung der Systemhärtung mit GOSS <Section_GOSS>`

.. Seealso::

   - :ref:`ADR - ADR-006 - Verwendung von Goss zur Überprüfung der Systemhärtung bei Red Hat 9 und Derivaten <Section_ADR_ADR-006>`

Checkov
##############################################################################

.. figure::
   _static/checkov_blue_logo.png
   :align: center
   :scale: 20 %
   :alt: Statische Code Analyse mit Checkov

   Statische Code Analyse mit Checkov

**Checkov** [24]_ ist ein statisches Code-Analyse-Tool für
Infrastructure-as-Code (IaC), das Fehlkonfigurationen
in Cloud-Infrastrukturen erkennt und behebt,
bevor sie bereitgestellt werden.

Hauptmerkmale:

- Überprüfung von Terraform, CloudFormation, Kubernetes, Helm und ARM-Vorlagen
- Über 1000 vordefinierte Richtlinien für gängige Fehlkonfigurationen
- Unterstützung für benutzerdefinierte Richtlinien
- Integration in CI/CD-Pipelines
- Generierung von Berichten in verschiedenen Formaten

*Installation auf Debian:*

bash

``sudo apt-get update``

``sudo apt-get -y install python3-pip``

``pip3 install checkov``

Beispiel Jenkins-Pipeline

Hier ist ein Beispiel für eine Jenkins-Pipeline, die Checkov verwendet,
um Ihre IaC-Dateien zu scannen:

groovy

.. code-block:: groovy
   :caption: Jenkins-Pipeline für Checkov

    pipeline {
        agent any

        stages {
            stage('Checkout') {
                steps {
                    // Klonen Sie das Repository mit Ihren IaC-Dateien
                    git branch: 'master', url: 'https://github.com/benutzername/iac-repo.git'
                }
            }
            stage('Checkov') {
                steps {
                    script {
                        // Führen Sie Checkov aus, um die IaC-Dateien zu scannen
                        try {
                            sh 'checkov -d . --output json --output-file-path results.json'
                        } catch (err) {
                            // Fehlerbehandlung, wenn Checkov Probleme findet
                            echo 'Checkov hat Probleme gefunden!'
                            currentBuild.result = 'FAILURE'
                            throw err
                        }
                    }
                }
            }
            stage('Publish Results') {
                steps {
                    // Veröffentlichen Sie die Ergebnisse in Jenkins
                    junit 'results.json'
                }
            }
        }
        options {
            preserveStashes()
            timestamps()
        }
    }


**Erklärung der Pipeline**

- **Checkout-Stage**: Diese Phase klont das Git-Repository, das Ihre IaC-Dateien enthält.

- **Checkov-Stage**: In dieser Phase wird Checkov ausgeführt, um die IaC-Dateien im aktuellen Verzeichnis zu scannen. Die Ergebnisse werden im JSON-Format in der Datei results.json gespeichert. Wenn Checkov Probleme findet, wird die Pipeline als fehlgeschlagen markiert.

- **Publish Results-Stage**: Hier werden die Ergebnisse des Checkov-Scans in Jenkins veröffentlicht, sodass Sie die Testergebnisse einsehen können.

**Integration in CI/CD**

Durch die Integration von Checkov in Ihre Jenkins-Pipeline können Sie sicherstellen, dass alle Änderungen an Ihrer Infrastruktur als Code vor der Bereitstellung auf Sicherheitsprobleme überprüft werden. Dies hilft, potenzielle Schwachstellen frühzeitig zu erkennen und zu beheben.

Optional: Job Control
==============================================================================

Falls optional ein Job Control benötigt wird, greifen wir
auf die bewährte Basis von Jenkins zurück.

Optional: Job Control mit Jenkins
##############################################################################

.. figure::
   _static/jenkins-logo-1.png
   :align: center
   :scale: 25 %
   :alt: Job Control mit Jenkins

   Job Control mit Jenkins

**Jenkins** [25]_ ist ein Open-Source-Automatisierungsserver, 
der hauptsächlich für Continuous Integration und Continuous Delivery (CI/CD) 
verwendet wird. Er ermöglicht Entwicklern, Softwareprojekte zu automatisieren,
indem er Builds, Tests und Bereitstellungen verwaltet. Java ist eine weit 
verbreitete Programmiersprache. Sie wird häufig in der Entwicklung 
von Webanwendungen, mobilen Anwendungen und Unternehmenssoftware eingesetzt.
Jenkins benötigt **Java**.

**Jenkins und seine Funktionen**

- **Automatisierung**: Jenkins automatisiert den gesamten 
  Softwareentwicklungsprozess, von der Codeerstellung bis zur Bereitstellung.

- **Plugins**: Jenkins bietet eine Vielzahl von Plugins, 
  die die Integration mit verschiedenen Tools und Technologien ermöglichen,
  darunter Versionierungssysteme, Testframeworks und Cloud-Dienste.

- **Pipeline**: Mit Jenkins Pipelines können komplexe Build- und
  Deployment-Prozesse definiert und verwaltet werden, was eine bessere 
  Nachverfolgbarkeit und Wiederholbarkeit ermöglicht.

- **Benutzeroberfläche**: Jenkins verfügt über eine benutzerfreundliche 
  Weboberfläche, die es Entwicklern ermöglicht, Builds zu überwachen 
  und zu steuern.

*Installation auf Debian:*

bash

.. code-block:: bash
   :caption: Jenkins Installation
   
    sudo wget -O /usr/share/keyrings/jenkins-keyring.asc \
    https://pkg.jenkins.io/debian-stable/jenkins.io-2023.key
    echo "deb [signed-by=/usr/share/keyrings/jenkins-keyring.asc]" \
    https://pkg.jenkins.io/debian-stable binary/ | sudo tee \
    /etc/apt/sources.list.d/jenkins.list > /dev/null
    sudo apt-get update
    sudo apt-get -y install jenkins


Optional: Java für Job Control mit Jenkins
##############################################################################

.. figure::
   _static/Java_logo.png
   :align: center
   :scale: 25 %
   :alt: Optional: Java für Job Control mit Jenkins

   Optional: Java für Job Control mit Jenkins

**Java** [26]_ ist eine Programmiersprache. Die dort genutzte JVM wird für
Jenkins benötigt.

- **Plattformunabhängigkeit**: Java-Anwendungen können auf jeder Plattform
  ausgeführt werden, die eine Java Virtual Machine (JVM) unterstützt, 
  was die Portabilität erhöht.

- **Objektorientierte Programmierung**: Java unterstützt objektorientierte
  Programmierkonzepte, die die Wiederverwendbarkeit und Wartbarkeit
  von Code fördern.


*Installation auf Debian:* openjdk-17-jre

bash

``sudo apt-get update``

``sudo apt-get -y install fontconfig openjdk-17-jre``

*Installation auf Debian:* openjdk-17-jre

bash

.. code-block:: bash

    wget --secure-protocol=auto --https-only --continue --verbose -N --tries=10 --check-certificate https://download.java.net/openjdk/jdk23/ri/openjdk-23+37_linux-x64_bin.tar.gz                                                     
    
    --2024-12-28 17:58:31--  https://download.java.net/openjdk/jdk23/ri/openjdk-23+37_linux-x64_bin.tar.gz
    Auflösen des Hostnamens download.java.net (download.java.net)… 23.32.100.101
    Verbindungsaufbau zu download.java.net (download.java.net)|23.32.100.101|:443 … verbunden.
    HTTP-Anforderung gesendet, auf Antwort wird gewartet … 200 OK
    Länge: 210705626 (201M) [application/x-gzip]
    Wird in »openjdk-23+37_linux-x64_bin.tar.gz« gespeichert.

    openjdk-23+37_linux-x64_bin.tar.gz                         100%[========================================================================================================================================>] 200,94M  5,40MB/s    in 94s     

    2024-12-28 18:00:07 (2,13 MB/s) - »openjdk-23+37_linux-x64_bin.tar.gz« gespeichert [210705626/210705626]

    wget --secure-protocol=auto --https-only --continue --verbose -N --tries=10 --check-certificate https://download.java.net/openjdk/jdk23/ri/openjdk-23+37_linux-x64_bin.tar.gz.sha256
    
    --2024-12-28 18:00:21--  https://download.java.net/openjdk/jdk23/ri/openjdk-23+37_linux-x64_bin.tar.gz.sha256
    Auflösen des Hostnamens download.java.net (download.java.net)… 23.32.100.101
    Verbindungsaufbau zu download.java.net (download.java.net)|23.32.100.101|:443 … verbunden.
    HTTP-Anforderung gesendet, auf Antwort wird gewartet … 200 OK
    Länge: 64 [text/plain]
    Wird in »openjdk-23+37_linux-x64_bin.tar.gz.sha256« gespeichert.

    openjdk-23+37_linux-x64_bin.tar.gz.sha256                  100%[========================================================================================================================================>]      64  --.-KB/s    in 0s      

    2024-12-28 18:00:22 (1,13 MB/s) - »openjdk-23+37_linux-x64_bin.tar.gz.sha256« gespeichert [64/64]

    cat openjdk-23+37_linux-x64_bin.tar.gz.sha256
    
    08fea92724127c6fa0f2e5ea0b07ff4951ccb1e2f22db3c21eebbd7347152a67%                                                                                                                                                                           ➜  repos sha256 openjdk-23+37_linux-x64_bin.tar.gz.sha256
    
    sha256 openjdk-23+37_linux-x64_bin.tar.gz       
    
    SHA256 (openjdk-23+37_linux-x64_bin.tar.gz) = 08fea92724127c6fa0f2e5ea0b07ff4951ccb1e2f22db3c21eebbd7347152a67

    tar xvf openjdk-23+37_linux-x64_bin.tar.gz

    sudo tee -a  /etc/profile.d/jdk23.sh<<EOF
    export JAVA_HOME=/usr/local/jdk-23
    export PATH=\$PATH:\$JAVA_HOME/bin
    EOF

    sudo source /etc/profile.d/jdk23.sh

    java --version

Optional: Container
==============================================================================

Optional: Podman für Container
##############################################################################

.. figure::
   _static/Podman-logo-orig.png
   :align: center
   :scale: 25 %
   :alt: Optional: Podman für Container

   Optional: Podman für Container

**Podman** [27]_ ist ein Open-Source-Container-Management-Tool, das es 
ermöglicht, Container und Container-Images zu erstellen, zu verwalten und 
auszuführen. Es bietet eine ähnliche Benutzeroberfläche wie Docker, 
ist jedoch daemonlos, was bedeutet, dass es keinen zentralen Daemon benötigt,
um Container zu verwalten. Podman unterstützt die Ausführung von Containern
im Root- und Rootless-Modus, was zusätzliche Sicherheit bietet. Es ist
besonders nützlich für Entwickler und Systemadministratoren,
die eine flexible und sichere Container-Lösung suchen.

*Installation auf Debian:*

bash

``sudo apt-get update``

``sudo apt-get -y install podman``

.. Tip::
   Diese Komponenten bilden eine umfassende Entwicklungsumgebung 
   für Automatisierung, Versionskontrolle und Code-Qualitätssicherung 
   für unser Vorhaben AlmaLinux zu härten.

Hinweise für die Code Schnipsel
------------------------------------------------------------------------------

.. Hint::
   Die angegebenen Befehle sind exemplarisch, ungetestest und verlangen
   einen sicheren und robusten Umgang von qualifiziertem Personal
   in einer geeigneten und gesicherten Umgebung.

Deployment Sicht
------------------------------------------------------------------------------

Hier wird angeführt, wie das alles ausgerollt wird (Deployment Sicht).

.. seealso:: Deployment Sicht

   Diese Einstellungen werden mittels eines bash Skriptes aufgebracht.

   .. figure::
      _static/bash-logo-by-vd.png
      :align: center
      :scale: 25 %
      :alt: bash Skript

      bash Skript

   
   <Platzhalter für den Link zum Repo>

   Diese Einstellungen werden mittels einer Ansible Rolle umgesetzt.

   .. figure::
      _static/Ansible_logo-700x700.png
      :align: center
      :scale: 25 %
      :alt: Ansible Rolle

      Ansible Rolle

   
   <Platzhalter für den Link zum Repo>


.....

.. Rubric:: Footnotes

.. [1]
   Debian Linux
   https://www.debian.org

.. [2]
   Ansible
   https://github.com/ansible/ansible

.. [3]
   ansible-lint
   https://github.com/ansible/ansible-lint

.. [4]
   Git
   https://github.com/git

.. [5]
   Git LFS (Large File Storage)
   https://git-lfs.com/

.. [6]
   Shellcheck
   https://github.com/koalaman/shellcheck

.. [7]
   shfmt
   https://github.com/patrickvane/shfmt

.. [8]
   Shellharden
   https://github.com/anordal/shellharden

.. [9]
   VirtualBox
   https://www.oracle.com/virtualization/technologies/vm/virtualbox.html

.. [10]
   Vagrant
   https://github.com/hashicorp/vagrant

.. [11]
   Vagrant Plugin - vagrant-scp
   https://github.com/invernizzi/vagrant-scp

.. [12]
   VSCodium
   https://github.com/VSCodium/vscodium

.. [13]
   Wget
   https://github.com/mirror/wget

.. [14]
   cURL
   https://github.com/curl/curl

.. [15]
   grep
   https://de.wikipedia.org/wiki/Grep

.. [16]
   egrep
   https://de.wikipedia.org/wiki/Grep

.. [17]
   ripgrep
   https://github.com/BurntSushi/ripgrep

.. [18]
   ClamAV
   https://github.com/Cisco-Talos/clamav

.. [19]
   Rust
   https://github.com/rust-lang/rust

.. [20]
   Cargo
   https://github.com/rust-lang/cargo

.. [21]
   YARA
   https://github.com/VirusTotal/yara

.. [22]
   YARA-X
   https://github.com/VirusTotal/yara-x

.. [23]
   Packer
   https://github.com/hashicorp/packer

.. [24]
   Checkov
   https://github.com/bridgecrewio/checkov

.. [25]
   Jenkins
   https://github.com/jenkinsci/jenkins

.. [26]
   Java - OpenJDK
   https://github.com/openjdk/jdk

.. [27]
   Podman
   https://github.com/containers/podman

.. [28]
   How To Install ansible-lint on Debian 12
   https://installati.one/install-ansible-lint-debian-12/?utm_content=cmp-true

.. [29]
   How To Install ansible-lint on Debian 12
   https://installati.one/install-ansible-lint-debian-12/?utm_content=cmp-true
