******************************************************************************
Abstract - Verbal Forms - Abstrakt - Verbale Formen
******************************************************************************

.. sectionauthor:: Pierre Gronau <Pierre.Gronau@ndaal.eu>


..

.. contents:: Content Abstract - Verbal Forms - Inhalt Abstrakt - Verbale Formen
   :depth: 3

..

.. Hint::
   - In the event of any discrepancies between the German and English versions
     or any other case of doubt, the German version shall prevail.
   - Bei Abweichungen zwischen der deutschen und der englischen Fassung oder bei
     sonstigen Zweifelsfällen gilt die deutsche Version.
  
.. _Abstract_Verbal_Forms:

Abstract - Verbal Forms
------------------------------------------------------------------------------

We are offering for your convenience a bilingual version of 
**Abstract - Verbal Forms**:

In many **IETF documents**, several words, when they are in all capitals as shown below, are used to signify the Requirements in the specification. 
These capitalized words can bring significant clarity and consistency to documents because their meanings are well defined.
This document defines how those words are interpreted in IETF (Internet Engineering Task Force) documents when the words are in all capitals.

- These words can be used as defined here, but using them is not required. Specifically, normative text does not require the use of 
  these key words. They are used for clarity and consistency when that is what's wanted, but a lot of normative text does not use 
  them and is still normative.
- The words have the meanings specified herein only when they are in all capitals.
- When these words are not capitalized, they have their normal English meanings and are not affected by this document.

Authors who follow these guidelines should incorporate this phrase near the beginning of their document:
The key words "**MUST**", "**MUST NOT**", "**REQUIRED**", "**SHALL**", "**SHALL NOT**", "**SHOULD**", "**SHOULD NOT**", "**RECOMMENDED**", 
"**NOT RECOMMENDED**", "**MAY**", and "**OPTIONAL**" in this document are to be interpreted as described in 
**BCP 14** [1]_, **RFC 2119** [2]_ , **RFC 8174** [3]_ when, and only when, they appear in all capitals, as shown here.

.. Hint::
   Note that the force of these words is modified by the Requirement level of the document in which they are used.

1. **MUST**

This word, or the terms "REQUIRED" or "SHALL", mean that the definition is an absolute Requirement of the specification.

2. **MUST NOT**

This phrase, or the phrase "SHALL NOT", mean that the definition is an absolute prohibition of the specification.

3. **SHOULD**

This word, or the adjective "RECOMMENDED", mean that there may exist valid reasons in particular circumstances to ignore a particular item, but the full implications must be understood and carefully weighed before choosing a different course.

4. **SHOULD NOT**

This phrase, or the phrase "NOT RECOMMENDED" mean that there may exist valid reasons in particular circumstances when the particular behavior is acceptable or even useful, but the full implications should be understood and the case carefully weighed before implementing any behavior described with this label.

5. **MAY**

This word, or the adjective "OPTIONAL", mean that an item is truly optional.  One Vendor may choose to include the item because a particular marketplace requires it or because the Vendor feels that it enhances the product while another Vendor may omit the same item. An implementation which does not include a particular option MUST be prepared to interoperate with another implementation which does include the option, though perhaps with reduced functionality. In the same vein an implementation which does include a particular option MUST be prepared to interoperate with another implementation which does not include the option (except, of course, for the feature the option provides.)

6. **Guidance in the use of these Imperatives**

Imperatives of the type defined in this memo must be used with care and sparingly.  In particular, they MUST only be used where it is actually required for interoperation or to limit behavior which has potential for causing harm (e.g., limiting retransmissions). For example, they must not be used to try to impose a particular method on implementors where the method is not required for interoperability.

7. **Security Considerations**

These terms are frequently used to specify behavior with Security implications.  The effects on Security of not implementing a MUST or SHOULD, or doing something the specification says MUST NOT or SHOULD NOT be done, may be very subtle. Document authors should take the time to elaborate the Security implications of not following recommendations or Requirements. as most implementors will not have had the benefit of the experience and discussion that produced the specification.

8. **UPPERCASE usage**

RFC (Requests for Comments) 2119 [2]_ specifies common key words that may be used in protocol specifications.  This document aims to reduce the ambiguity by clarifying that only UPPERCASE usage of the key words have the defined special meanings.

Abstrakt - Verbale Formen
------------------------------------------------------------------------------

In vielen **IETF-Dokumenten** werden mehrere Wörter, wenn sie wie unten gezeigt in Großbuchstaben geschrieben sind, zur Kennzeichnung der Anforderungen in der Spezifikation verwendet. 
Diese großgeschriebenen Wörter können die Klarheit und Konsistenz von Dokumenten erheblich verbessern, da ihre Bedeutung genau definiert ist.
Dieses Dokument definiert, wie diese Wörter in IETF (Internet Engineering Task Force)-Dokumenten zu interpretieren sind, wenn die Wörter in Großbuchstaben geschrieben sind.

- Diese Wörter können so verwendet werden, wie sie hier definiert sind, aber es ist nicht erforderlich, sie zu verwenden. Insbesondere normativer Text erfordert nicht die Verwendung 
  dieser Schlüsselwörter. Sie werden aus Gründen der Klarheit und Konsistenz verwendet, wenn dies erwünscht ist, aber viele normative Texte verwenden sie nicht 
  sie nicht und ist trotzdem normativ.
- Die Wörter haben nur dann die hier angegebene Bedeutung, wenn sie in Großbuchstaben geschrieben sind.
- Wenn diese Wörter nicht in Großbuchstaben geschrieben sind, haben sie ihre normale englische Bedeutung und werden von diesem Dokument nicht beeinflusst.

Autoren, die diese Richtlinien befolgen, sollten diesen Satz am Anfang ihres Dokuments einfügen:

Die Terme “MUST”, “MUST NOT”, “REQUIRED”, “SHALL”, “SHALL NOT”, “SHOULD”, 
“SHOULD NOT”, “RECOMMENDED”, “MAY”, und “OPTIONAL” in diesem Dokument sind, 
wie in **BCP 14** [1]_, **RFC 2119** [2]_ , **RFC 8174** [3]_ beschrieben, 
wenn diese in Großbuchstaben erscheinen, zu interpretieren.

.. Hint::
   Beachten Sie, dass die Bedeutung dieser Wörter von der Anforderungsebene des Dokuments abhängt, in dem sie verwendet werden.

1. **MUSS**

Dieses Wort oder die Begriffe "ERFORDERLICH" oder "MUSS" bedeuten, dass die Definition eine absolute Anforderung der Spezifikation ist.

2. **MUSS NICHT**

Dieses Wort oder der Ausdruck "MUSS NICHT" bedeuten, dass die Definition ein absolutes Verbot der Spezifikation darstellt.

3. **SOLLTE**

Dieses Wort oder das Adjektiv "EMPFOHLEN" bedeutet, dass es unter bestimmten Umständen triftige Gründe geben kann, einen bestimmten Punkt zu ignorieren, dass aber die gesamte Tragweite verstanden und sorgfältig abgewogen werden muss, bevor ein anderer Weg gewählt wird.

4. **SOLLTE NICHT**

Dieser Satz oder der Satz "NICHT EMPFOHLEN" bedeutet, dass es unter bestimmten Umständen triftige Gründe dafür geben kann, dass ein bestimmtes Verhalten akzeptabel oder sogar nützlich ist, dass aber die gesamten Auswirkungen verstanden und der Fall sorgfältig abgewogen werden sollte, bevor ein mit dieser Kennzeichnung beschriebenes Verhalten umgesetzt wird.

5. **MÖGLICH**

Dieses Wort oder das Adjektiv "OPTIONAL" bedeutet, daß ein Element wirklich optional ist.  Ein Anbieter kann sich dafür entscheiden, das Element aufzunehmen, weil ein bestimmter Markt es erfordert oder weil er der Meinung ist, dass es das Produkt verbessert, während ein anderer Anbieter das gleiche Element weglassen kann. Eine Implementierung, die eine bestimmte Option nicht enthält, MUSS darauf vorbereitet sein, mit einer anderen Implementierung zusammenzuarbeiten, die diese Option enthält, wenn auch möglicherweise mit eingeschränkter Funktionalität. Ebenso MUSS eine Implementierung, die eine bestimmte Option enthält, darauf vorbereitet sein, mit einer anderen Implementierung, die diese Option nicht enthält, zusammenzuarbeiten (natürlich mit Ausnahme der Funktion, die die Option bietet).

6. **Hinweise für die Verwendung dieser Imperative**

Imperative des in diesem Memo definierten Typs müssen mit Vorsicht und sparsam verwendet werden.  Insbesondere MÜSSEN sie nur dort verwendet werden, wo sie tatsächlich für die Interoperation erforderlich sind oder um ein Verhalten einzuschränken, das potenziell Schaden anrichten kann (z. B. Begrenzung der erneuten Übertragungen). Sie dürfen zum Beispiel nicht verwendet werden, um den Implementierern eine bestimmte Methode aufzuzwingen, die für die Interoperabilität nicht erforderlich ist.

7. **Sicherheitserwägungen**

Diese Begriffe werden häufig verwendet, um ein Verhalten mit Sicherheitsauswirkungen zu spezifizieren.  Die Auswirkungen auf die Sicherheit, die sich ergeben, wenn ein MUST oder SHOULD nicht implementiert wird oder wenn etwas getan wird, das laut Spezifikation NICHT getan werden MUSS oder SOLL, können sehr subtil sein. Die Autoren der Dokumente sollten sich die Zeit nehmen, die Sicherheitsauswirkungen der Nichtbeachtung von Empfehlungen oder Anforderungen zu erläutern, da die meisten Implementierer nicht in den Genuss der Erfahrungen und Diskussionen gekommen sind, die zur Erstellung der Spezifikation geführt haben.

8. **Verwendung von Großbuchstaben**

RFC (Requests for Comments) 2119 [2]_ spezifiziert allgemeine Schlüsselwörter, die in Protokollspezifikationen verwendet werden können.  Dieses Dokument zielt darauf ab, die Mehrdeutigkeit zu reduzieren, indem es klarstellt, dass nur die Verwendung von Großbuchstaben für die Schlüsselwörter die definierten speziellen Bedeutungen haben.


.....

.. Seealso:: 
   a) :ref:`Abstract - Minimum Requirement(s) - Abstrakt - Mindestanforderung(en) <Abstract_Minimum_Requirement>`
   #) :ref:`Abstract - Notations Conventions - Abstrakt - Notationskonventionen <Abstract_Notations_Conventions>`
   #) :ref:`Abstract - Verbal Forms - Abstrakt - Verbale Formen <Abstract_Verbal_Forms>`
   #) :ref:`Abstract - Version Information and Compatibility - Abstrakt – Versionsangaben und -kompatibilität <Abstract_Version>`

.....

.. Rubric:: Footnotes

.. [1]
   https://tools.ietf.org/html/bcp14 :abbr:`BCP (Best Current Practice)` 14

.. [2]
   https://tools.ietf.org/html/rfc2119 :abbr:`RFC (Requests for Comments)` 2119

.. [3]
   https://tools.ietf.org/html/rfc8174 :abbr:`RFC (Requests for Comments)` 8174
