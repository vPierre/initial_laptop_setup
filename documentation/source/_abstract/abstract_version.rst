**************************************************************************************************
Abstract - Version Information and Compatibility - Abstrakt – Versionsangaben und -kompatibilität
**************************************************************************************************

.. sectionauthor:: Pierre Gronau <Pierre.Gronau@ndaal.eu>
 
..

.. contents:: Content Abstract - Version Information and Compatibility - Inhalt Abstrakt – Versionsangaben und -kompatibilität
   :depth: 3

..

.. Hint::
   - In the event of any discrepancies between the German and English versions
     or any other case of doubt, the German version shall prevail.
   - Bei Abweichungen zwischen der deutschen und der englischen Fassung oder bei
     sonstigen Zweifelsfällen gilt die deutsche Version.
   
.. _Abstract_Version:

Abstract - Version Information and Compatibility
------------------------------------------------------------------------------

We are offering for your convenience a bilingual version of 
**Abstract - Version Information and Compatibility**:

If no **major version** is mentioned the actual version what is **part of
Windows and the Linux distribution** will be used. Therefore the basis for the
major version/release is defined by:

- **Windows Versions**

   - ``Windows 2025``

   - ``Windows 2022``

   - ``Windows 2019``

   - ``Windows 2016``

   - ``Windows 2012 R2``

   - ``Windows 2012``

   - ``Windows 2008 R2``

   - ``Windows 2008``

- **Linux Distributions**
   - **Debian**

      - ``Debian 12``

      - ``Debian 11``

      - ``Debian 10``

   - **Alpine Linux**

      - ``v3.21.0``

      - ``v3.20.0``

      - ``v3.19.0``

   - **Red Hat**

      - ``Red Hat 10``

      - ``Red Hat 9``

      - ``Red Hat 8``

      - ``Red Hat 7``

    - **AlmaLinux**

      - ``AlmaLinux 10``

      - ``AlmaLinux 9``

      - ``AlmaLinux 8``

   - **SUSE**

      - ``SLES 15``

      - ``SLES 12``

      - ``SLES 11``

- **ESXi Versions**

   - ``ESXi 9.0``

   - ``ESXi 8.0 U3``

   - ``ESXi 8.0 U2``

   - ``ESXi 8.0 U1``

   - ``ESXi 8.0``

   - ``ESXi 7.0 U4``

   - ``ESXi 7.0 U3``

   - ``ESXi 7.0 U2``

   - ``ESXi 7.0 U1``

   - ``ESXi 7.0``

   - ``ESXi 6.7 U3``

   - ``ESXi 6.7 U2``

   - ``ESXi 6.7 U1``

   - ``ESXi 6.7``

.. Seealso::
   :ref:`Semantic Versioning Scheme <Document_Information>`

or higher
##############################################################################

In this document, versions of hardware modules, protocols and software are 
listed with an optional suffix "or higher". 
If this explicit reference to a higher version is missing, only bug fixes and
security updates may be installed - no new major versions. With this hint, new
**major versions** MAY also be installed.

Please keep in mind to document which **major versions** will take place.


Abstract - Version information - Examples
##############################################################################

The following are some examples:

- An update from Windows 10 Buid 1809 to Windows 10 Build 1903 (or higher) is 
  considered a major version update. All other Windows updates are considered
  to continue as Windows 10 1809. [2]_

- An update from Windows Server 2012 to Windows Server 2012 R2 (or higher) is
  considered a major version update. All Windows updates available for Windows 
  Server 2012 are considered to continue as Windows Server 2012. [2]_

- An update from ESXi 6.7 U2 to ESXi 6.7 U3 (or later) is considered a new 
  major release, while updates to ESXi670-201904001, ESXi670-201905001, and 
  ESXi670-201906002 are considered bug fixes and security updates,
  respectively. [1]_

Abstrakt – Versionsangaben und -kompatibilität
------------------------------------------------------------------------------

Wenn keine **Hauptversion** angegeben ist, wird die aktuelle Version, die 
**Teil von Windows und der Linux-Distribution** ist, verwendet. Daher wird die
Basis für die Hauptversion/Release wie folgt definiert:

- **Windows Versionen**

   - ``Windows 2025``

   - ``Windows 2022``

   - ``Windows 2019``

   - ``Windows 2016``

   - ``Windows 2012 R2``

   - ``Windows 2012``

   - ``Windows 2008 R2``

   - ``Windows 2008``

- **Linux Distributionen**
   - **Debian**

      - ``Debian 12``

      - ``Debian 11``

      - ``Debian 10``

   - **Alpine Linux**

      - ``v3.21.0``

      - ``v3.20.0``

      - ``v3.19.0``

   - **Red Hat**

      - ``Red Hat 10``

      - ``Red Hat 9``

      - ``Red Hat 8``

      - ``Red Hat 7``

    - **AlmaLinux**

      - ``AlmaLinux 10``

      - ``AlmaLinux 9``

      - ``AlmaLinux 8``

   - **SUSE**

      - ``SLES 15``

      - ``SLES 12``

      - ``SLES 11``

- **ESXi Versionen**

   - ``ESXi 9.0``

   - ``ESXi 8.0 U3``

   - ``ESXi 8.0 U2``

   - ``ESXi 8.0 U1``

   - ``ESXi 8.0``

   - ``ESXi 7.0 U4``

   - ``ESXi 7.0 U3``

   - ``ESXi 7.0 U2``

   - ``ESXi 7.0 U1``

   - ``ESXi 7.0``

   - ``ESXi 6.7 U3``

   - ``ESXi 6.7 U2``

   - ``ESXi 6.7 U1``

   - ``ESXi 6.7``

.. Seealso::
   :ref:`Semantic Versioning Scheme <Document_Information>`

oder höher
##############################################################################

In diesem Dokument werden Versionen von Hardwaremodulen, Protokollen und 
Software mit einem optionalen Suffix „oder höher“ gelistet. Sofern dieser 
explizite Hinweis auf eine höhere Version fehlt, dürfen nur Bugfixes und 
Sicherheitsupdates eingespielt werden – keine neuen Hauptversionen. 

Mit diesem Hinweis dürfen auch neue **Hauptversionen** installiert werden.

Abstrakt – Versionsangaben – Beispiele
##############################################################################  

Nachfolgend sind einige Beispiele aufgeführt:

- Ein Update von Windows 10 Buid 1809 auf Windows 10 Build 1903 (oder höher)
  gilt als Update der Hauptversion. Alle anderen Windows-Updates gelten
  als weiterhin als Windows 10 1809. [2]_

- Ein Update von Windows Server 2012 auf Windows Server 2012 R2 (oder höher)
  gilt als Update der Hauptversion. Alle für Windows Server 2012
  verfügbaren Windows-Updates gelten als weiterhin als
  Windows Server 2012. [2]_

- Ein Update von ESXi 6.7 U2 auf ESXi 6.7 U3 (oder höher) gilt
  als neue Hauptversion, während die Updates auf ESXi670-201904001,
  ESXi670-201905001 und ESXi670-201906002 als Bugfixes bzw.
  Sicherheitsupdates angesehen werden. [1]_
   
.....

.. Rubric:: Footnotes

.. [1]
   Build numbers and versions of VMware ESXi/ESX
   https://knowledge.broadcom.com/external/article/316595/build-numbers-and-versions-of-vmware-esx.html

.. [2]
   Wikipedia article - List of Microsoft Windows versions
   https://en.wikipedia.org/wiki/List_of_Microsoft_Windows_versions

   Wikipedia Artikel - Microsoft Windows
   https://de.wikipedia.org/wiki/Microsoft_Windows

.. Seealso:: 
   a) :ref:`Abstract - Minimum Requirement(s) - Abstrakt - Mindestanforderung(en) <Abstract_Minimum_Requirement>`
   #) :ref:`Abstract - Notations Conventions - Abstrakt - Notationskonventionen <Abstract_Notations_Conventions>`
   #) :ref:`Abstract - Verbal Forms - Abstrakt - Verbale Formen <Abstract_Verbal_Forms>`
   #) :ref:`Abstract - Version Information and Compatibility - Abstrakt – Versionsangaben und -kompatibilität <Abstract_Version>`
