******************************************************************************
Systemhärtung - Systemhärtungsumgebung
******************************************************************************

.. sectionauthor:: Pierre Gronau <Pierre.Gronau@ndaal.eu>

.. contents:: Inhalt - Sicherheit - Container
   :depth: 5


Vorbereitung der Systemhärtungsumgebung
------------------------------------------------------------------------------

Voraussetzungen für die Systemhärtungsumgebung
==============================================================================

- **x86-64** (auch in der Schreibweise „x86_64“) und AMD64 
  (auch in Kleinschreibung: „amd64“) Architektur

- Ausreichende **Entropie** [2]_ 

- Linux Distros

  - Linux **Debian** [1]_ 12.9x  oder höher inkl. aller 
    aktuell verfügbaren Sicherheitsaktualisierungen

  - Linux **AlmaLinux** 9.x inkl. aller aktuell verfügbaren
    Sicherheitsaktualisierungen

  - Linux **Rocky Linux** 9.x inkl. aller aktuell verfügbaren
    Sicherheitsaktualisierungen

- **git** Repo

- **Ansible**

- **ansible-lint**

- **Vagrant**

- **Virtualbox**

- **bash**

- **GOSS**

Wir benötigen nun mindestens folgende **Skripte** und **Schritte** 
um zu einer Systemhärtung zu kommen:

Benötigte VMs
==============================================================================

Mittels **Vagrant** werden mindestens fünf **Virtualbox** VMs produziert:

- Linux **Debian** 12.9x oder höher zum Bootstrapping

- Linux **Debian** 12.9x oder höher als Automation-Controller

- Linux **Debian** 12.9x oder höher als Job-Controller (optional)

- Linux **Debian** 12.9x oder höher als Scanner (optional)

- Testsysteme bestehend aus

  - Linux **AlmaLinux** 9.x

  - Linux **Rocky Linux** 9.x

  - Linux **Oracle Linux** 9.x

Soweit vorhanden werden nur offizielle Images verwendet wie bei
<https://portal.cloud.hashicorp.com/vagrant/discover/almalinux/9>

Prüfung von vorhanden sein von Befehlen
==============================================================================

Prüfung von vorhanden sein von Befehlen mittels einer bash Funktion:

.. code-block:: bash
   :caption: check_command

    # Function to check if a command is available

    check_command() {
    if ! command -v "${1}" &>/dev/null; then
    printf "Error: %s is not installed or not in PATH. Please install it and try again.\n" "${1}"
    return 1
    fi

    # Success case
    return 0
    }

    # Check for required commands

    check_command "git" || exit 1
    check_command "git-lfs" || exit 1
    check_command "wget" || exit 1
    check_command "curl" || exit 1
    check_command "virtualbox" || exit 1
    check_command "vagrant" || exit 1


Ablauf der Systemhärtung
------------------------------------------------------------------------------

- Produzieren der VMs mittels **Vagrant** und **Virtualbox**

- Installiere eine verschlüsselte apt Kommunikation mittels 
  ``apt-transport-https`` auf allen Debian VMs

- Installiere die benötigten Werkzeuge auf Debian 12.9x oder höher

- Härte Debian mittels <https://github.com/ovh/debian-cis> und 
  überprüfe mittels ``bin/hardening.sh --audit-all``

- Hole aus dem git Repo(s) alle benötigten Skripte und Ansible Rollen

- Prüfe alle bash Skripte mittels ``shellcheck`` und ``shfmt`` und 
  beende die Automation bei Findings

- Prüfe alle Ansible Rollen mittels ``ansible-lint`` und beende 
  die Automation bei Findings

- Anwenden der Ansible Rollen auf die Red Hat 9 basierten VMs 
  wie **AlmaLinux**, **Rocky Linux** und **Oracle Linux**

- überprüfe mittels ``Inspect`` die erfolgte Systemhärtung 
  auf Funktionalität und Vollständigkeit

zu den einzelnen Schritten
------------------------------------------------------------------------------

Nachstehend sind die einzelnen Schritte aufgezählt und mit Codebeispielen 
erklärt.

Produzieren der VMs mittels Vagrant und Virtualbox
==============================================================================

Um mit **Vagrant** unter Linux verschiedene Linux-VMs zu erstellen, benötigen Sie
zunächst Vagrant und eine Virtualisierungssoftware wie **VirtualBox**. 
Sie definieren die gewünschten VMs in einer Konfigurationsdatei namens 
„Vagrantfile“, in der Sie die Basis-Boxen, Netze und andere Einstellungen 
festlegen. Hier sind die Schritte, um dies zu erreichen:

1. Voraussetzungen
##############################################################################

- **Installieren Sie Vagrant**: Stellen Sie sicher, dass Vagrant 
  auf Ihrem System installiert ist. Sie können Vagrant 
  von der offiziellen Website herunterladen und installieren.

- **Installieren Sie VirtualBox**: Dies ist 
  die Standard-Virtualisierungssoftware, die mit Vagrant verwendet wird. 
  Laden Sie VirtualBox von der offiziellen Website herunter 
  und installieren Sie es.

2. Erstellen eines neuen Vagrant-Projekts
##############################################################################

**Verzeichnis erstellen**: Erstellen Sie ein neues Verzeichnis 
für Ihr Vagrant-Projekt.

bash

.. code-block:: bash
   :caption: Erstellen eines neuen Vagrant-Projekts

    mkdir mein_vagrant_projekt
    cd mein_vagrant_projekt


**Vagrant initialisieren**: Führen Sie den Befehl ``vagrant init`` aus,
um eine neue Vagrantfile zu erstellen.

bash

.. code-block:: bash
   :caption: Erstellen eines neuen Vagrant-Projekts

    vagrant init


3. Vagrantfile konfigurieren
##############################################################################

**Öffnen Sie die Vagrantdatei**:

Bearbeiten Sie die Vagrantfile mit einem Texteditor (z. B. vim, nano).

bash

.. code-block:: bash
   :caption: Vagrantfile konfigurieren

    vim Vagrantfile


**Definieren Sie die VMs**: Fügen Sie Konfigurationen für jede VM hinzu.
Hier ist ein Beispiel für die Definition von drei VMs (App-Server, DB-Server
und E-Mail-Server):

ruby

.. code-block:: ruby
   :caption: Vagrantfile

   Vagrant.configure("2") do |config|
   config.vm.box_check_update = false

   # App-Server
   config.vm.define "app" do |app|
       app.vm.hostname = "appserver.srg.com"
       app.vm.box = "centos/8"
       app.vm.network :private_network, ip: "10.0.0.101"
   end

   # DB-Server
   config.vm.define "db" do |db|
       db.vm.hostname = "dbserver.srg.com"
       db.vm.box = "centos/8"
       db.vm.network :private_network, ip: "10.0.0.102"
   end

   # Mail-Server
   config.vm.define "mail" do |mail|
       mail.vm.hostname = "mailserver.srg.com"
       mail.vm.box = "centos/8"
       mail.vm.network :private_network, ip: "10.0.0.103"
   end

   config.vm.provider "virtualbox" do |vb|
       vb.gui = false
       vb.memory = "2048"
       vb.cpus = 2
   end
   end


4. VMs starten
##############################################################################

**Vagrant-Umgebung starten**: Führen Sie den Befehl ``vagrant up`` aus,
um alle definierten VMs zu starten.

bash

.. code-block:: bash
   :caption: VMs starten

   vagrant up


5. Überprüfen des Status
##############################################################################

**Status der VMs anzeigen**: Verwenden Sie den Befehl ``vagrant status``, 
um den Status jeder VM zu überprüfen.

bash

.. code-block:: bash
   :caption: Überprüfen des Status

   vagrant status


6. SSH-Zugriff auf die VMs
##############################################################################

**In eine VM einloggen**: Verwenden Sie den Befehl ``vagrant ssh <vm_name>``,
um sich in eine bestimmte VM einzuloggen.

bash

.. code-block:: bash
   :caption: SSH-Zugriff auf die VMs

   vagrant ssh app


7. VMs verwalten
##############################################################################

**Halten Sie eine VM an**: Um eine VM anzuhalten, verwenden Sie den Befehl 
``vagrant halt <vm_name>``.

bash

.. code-block:: bash
   :caption: VMs verwalten - halt

   vagrant halt app


**Zerstören Sie eine VM**: Um eine VM und alle zugehörigen Ressourcen 
zu löschen, verwenden Sie den Befehl ``vagrant destroy <vm_name>``.

bash

.. code-block:: bash
   :caption: VMs verwalten - destroy

   vagrant destroy app


8. Vagrant-Boxen verwalten
##############################################################################

**Boxen hinzufügen**: Um eine neue Box hinzuzufügen, verwenden Sie den Befehl 
``vagrant box add <name>``.

bash

.. code-block:: bash
   :caption: Vagrant-Boxen verwalten

   vagrant box add centos/8

Zusammenfassung
##############################################################################

Mit Vagrant können Sie einfach und effizient mehrere Linux-VMs 
unter Linux erstellen und verwalten. Durch die Verwendung 
einer Vagrantfile können Sie die Konfigurationen für jede VM 
zentral steuern, was die Automatisierung und Reproduzierbarkeit 
Ihrer Entwicklungsumgebungen erheblich verbessert.

Wir nutzen hierfür das bash Skript ``create_VMs_with_Vagrant.sh``:

.. literalinclude::
   ;_scripts/create_VMs_with_Vagrant.sh
   :language: bash
   :force:
   :linenos:
   :caption: create_VMs_with_Vagrant.sh


Das Skript sourced folgende Funktionsdatei 
*functions_for_package_manager.cfg*:

.. literalinclude:: 
   _cfg/functions_for_package_manager.cfg
   :language: text
   :force:
   :linenos:
   :caption: functions_for_package_manager.cfg


Installation der benötigten Werkzeuge auf Debian 12.9x oder höher
==============================================================================

.. code-block:: bash

    # apt update && sudo apt upgrade -y

    array_packages=(
    "apt-transport-https"
    "wget"
    "curl"
    "git"
    "git-lfs"
    "ansible"
    "ansible-lint"
    )

vervollständige "array_packages" ...

.. code-block:: bash

    readonly array_packages
    printf "%sInfo: Package: %s\n" "${array_packages[@]}"


.. literalinclude::
    _cfg/function_install_packages_for_package_manager.cfg
   :language: text
   :force:
   :linenos:
   :caption: function_install_packages_for_package_manager.cfg

.. code-block:: bash

    # Usage example

    install_packages -debian package1 package2 package3

SBOM  (Software Bill of Materials) Erstellung mittels Skript für alle genutzten git Repos
==============================================================================

Eine **SBOM (Software Bill of Materials)** ist ein detailliertes Verzeichnis 
aller Komponenten, Abhängigkeiten und Lizenzen, die in einer Softwareanwendung 
enthalten sind. Sie dient als eine Art "Rezept" für Software und ermöglicht es
Entwicklern, Sicherheits- und Compliance-Risiken besser zu verstehen und 
zu verwalten.

Wichtige Aspekte einer SBOM:
==============================================================================

- **Transparenz**: Eine SBOM bietet Transparenz über die verwendeten 
  Softwarekomponenten, einschließlich Open-Source-Bibliotheken 
  und proprietärer Software.

- **Sicherheitsmanagement**: Durch die Identifizierung aller Komponenten können 
  Sicherheitsanfälligkeiten schneller erkannt und behoben werden. Wenn eine 
  Schwachstelle in einer bestimmten Bibliothek entdeckt wird, kann die SBOM 
  helfen, alle betroffenen Anwendungen zu identifizieren.

- **Compliance**: Eine SBOM unterstützt Unternehmen dabei, die Einhaltung 
  von Lizenzanforderungen und regulatorischen Vorgaben zu gewährleisten, 
  indem sie eine klare Übersicht über die verwendeten Lizenzen bietet.

- **Automatisierung**: SBOMs können in automatisierte Sicherheits- und 
  Compliance-Tools integriert werden, um kontinuierliche Überwachung und 
  Analyse zu ermöglichen.

Fazit
==============================================================================

Insgesamt ist eine **SBOM** ein wichtiges Werkzeug für
die Softwareentwicklung, das dazu beiträgt, die Sicherheit, Transparenz
und Compliance von Softwareanwendungen zu verbessern. Sie gewinnt zunehmend
an Bedeutung, insbesondere in der heutigen Softwarelandschaft,
in der viele Anwendungen auf einer Vielzahl von Drittanbieterkomponenten
basieren.

Software Bill of Materials (SBOM) Quellen
==============================================================================

Essentielle Links zu Deutsch sprachige und Englisch sprachige Quellen.

Englisch sprachige Quellen
##############################################################################

- The ultimate guide to SBOMs - GitLab [3]_
- Briefly Explained: Software Bill of Materials (SBOM) - eco [4]_
- SBOM Resources Library | CISA [5]_
- SBOM Standard Formats: Complete Guide - Scribe Security [6]_
- The Complete Guide to SBOMs - FOSSA [7]_
- Authoritative Guide to SBOM - CycloneDX [10]_
- The SPDX specification is an international open standard (ISO/IEC 5962:2021) [11]_ 
- CycloneDX Bill of Materials Specification (ECMA-424) [12]_ 

Deutsch sprachige Quellen
##############################################################################

- Technische Richtlinie TR-03183: Cyber Resilience Requirements - BSI [8]_
- Empfehlungen für das Management von Software-Bill-of-Materials (SBOM) - BSI [9]_

Beispiel Implementierung mit einem bash Skript
==============================================================================

.. literalinclude::
    _scripts/create_sbom_and_check_for_vulnerabilties.sh
   :language: text
   :force:
   :linenos:
   :caption: create_sbom_and_check_for_vulnerabilties.sh

**Beispielhafte SBOM Ausgabe der Erstellung und Prüfung**

.. code-block:: bash
   :caption: Beispielhafte SBOM Ausgabe der Erstellung und Prüfung

   Info: Current date: 2025-02-05

  Info: Current REPO_PATH: .

  Info: Current RESULTS_FILE: sbom_scan_results_2025-02-05.txt

  Info: Generating SBOM...
  ✔ Indexed file system                                                                                                                                                                                                                   .
  ✔ Cataloged contents                                                                                                                                                     cdb4ee2aea69cc6a83331bbe96dc2caa9a299d21329efb0336fc02a82e1839a8
    ├── ✔ Packages                        [2 packages]
    ├── ✔ File digests                    [8,276 files]
    ├── ✔ File metadata                   [8,485 locations]
    └── ✔ Executables                     [0 executables]
  [0001]  WARN no explicit name and version provided for directory source, deriving artifact ID from the given path (which is not ideal)
  ✔ Indexed file system                                                                                                                                                                                                                   .
  ✔ Cataloged contents                                                                                                                                                     cdb4ee2aea69cc6a83331bbe96dc2caa9a299d21329efb0336fc02a82e1839a8
    ├── ✔ Packages                        [2 packages]
    ├── ✔ File digests                    [8,276 files]
    ├── ✔ File metadata                   [8,485 locations]
    └── ✔ Executables                     [0 executables]
  [0000]  WARN no explicit name and version provided for directory source, deriving artifact ID from the given path (which is not ideal)
  ✔ Indexed file system                                                                                                                                                                                                                   .
  ✔ Cataloged contents                                                                                                                                                     cdb4ee2aea69cc6a83331bbe96dc2caa9a299d21329efb0336fc02a82e1839a8
    ├── ✔ Packages                        [2 packages]
    ├── ✔ File digests                    [8,276 files]
    ├── ✔ File metadata                   [8,485 locations]
    └── ✔ Executables                     [0 executables]
  [0000]  WARN no explicit name and version provided for directory source, deriving artifact ID from the given path (which is not ideal)
  ✔ Indexed file system                                                                                                                                                                                                                   .
  ✔ Cataloged contents                                                                                                                                                     cdb4ee2aea69cc6a83331bbe96dc2caa9a299d21329efb0336fc02a82e1839a8
    ├── ✔ Packages                        [2 packages]
    ├── ✔ File digests                    [8,276 files]
    ├── ✔ File metadata                   [8,485 locations]
    └── ✔ Executables                     [0 executables]
  [0001]  WARN no explicit name and version provided for directory source, deriving artifact ID from the given path (which is not ideal)
  ✔ Indexed file system                                                                                                                                                                                                                   .
  ✔ Cataloged contents                                                                                                                                                     cdb4ee2aea69cc6a83331bbe96dc2caa9a299d21329efb0336fc02a82e1839a8
    ├── ✔ Packages                        [2 packages]
    ├── ✔ File digests                    [8,276 files]
    ├── ✔ File metadata                   [8,485 locations]
    └── ✔ Executables                     [0 executables]
  [0000]  WARN no explicit name and version provided for directory source, deriving artifact ID from the given path (which is not ideal)
  ✔ Indexed file system                                                                                                                                                                                                                   .
  ✔ Cataloged contents                                                                                                                                                     cdb4ee2aea69cc6a83331bbe96dc2caa9a299d21329efb0336fc02a82e1839a8
    ├── ✔ Packages                        [2 packages]
    ├── ✔ File digests                    [8,276 files]
    ├── ✔ File metadata                   [8,485 locations]
    └── ✔ Executables                     [0 executables]
  [0000]  WARN no explicit name and version provided for directory source, deriving artifact ID from the given path (which is not ideal)
  ✔ Indexed file system                                                                                                                                                                                                                   .
  ✔ Cataloged contents                                                                                                                                                     cdb4ee2aea69cc6a83331bbe96dc2caa9a299d21329efb0336fc02a82e1839a8
    ├── ✔ Packages                        [2 packages]
    ├── ✔ File digests                    [8,276 files]
    ├── ✔ File metadata                   [8,485 locations]
    └── ✔ Executables                     [0 executables]
  [0000]  WARN no explicit name and version provided for directory source, deriving artifact ID from the given path (which is not ideal)
  ✔ Indexed file system                                                                                                                                                                                                                   .
  ✔ Cataloged contents                                                                                                                                                     cdb4ee2aea69cc6a83331bbe96dc2caa9a299d21329efb0336fc02a82e1839a8
    ├── ✔ Packages                        [2 packages]
    ├── ✔ File digests                    [8,276 files]
    ├── ✔ File metadata                   [8,485 locations]
    └── ✔ Executables                     [0 executables]
  [0000]  WARN no explicit name and version provided for directory source, deriving artifact ID from the given path (which is not ideal)
  ✔ Indexed file system                                                                                                                                                                                                                   .
  ✔ Cataloged contents                                                                                                                                                     cdb4ee2aea69cc6a83331bbe96dc2caa9a299d21329efb0336fc02a82e1839a8
    ├── ✔ Packages                        [2 packages]
    ├── ✔ File digests                    [8,276 files]
    ├── ✔ File metadata                   [8,485 locations]
    └── ✔ Executables                     [0 executables]
  [0000]  WARN no explicit name and version provided for directory source, deriving artifact ID from the given path (which is not ideal)
  Info: SBOM generated successfully with name sbom.json.
  Info: Scanning for vulnerabilities with Grype...
  Info: === Grype Scan Results ===

  ✔ Scanned for vulnerabilities     [0 vulnerability matches]
    ├── by severity: 0 critical, 0 high, 0 medium, 0 low, 0 negligible
    └── by status:   0 fixed, 0 not-fixed, 0 ignored
  No vulnerabilities found


  Info: Scanning for vulnerabilities with Trivy...
  Info: === Trivy Scan Results ===

  Info: Updating Trivy database...
  Info: Database updated successfully
  2025-02-05T22:21:13+01:00	INFO	[vuln] Vulnerability scanning is enabled
  2025-02-05T22:21:13+01:00	INFO	[misconfig] Misconfiguration scanning is enabled
  2025-02-05T22:21:13+01:00	INFO	[secret] Secret scanning is enabled
  2025-02-05T22:21:13+01:00	INFO	[secret] If your scanning is slow, please try '--scanners vuln' to disable secret scanning
  2025-02-05T22:21:13+01:00	INFO	[secret] Please see also https://aquasecurity.github.io/trivy/v0.59/docs/scanner/secret#recommendation for faster secret detection
  2025-02-05T22:21:59+01:00	INFO	Number of language-specific files	num=0
  2025-02-05T22:21:59+01:00	INFO	Detected config files	num=0
  Info: No vulnerabilities found by Trivy.


  Info: Vulnerability scans completed. Results saved to sbom_scan_results_2025-02-05.txt
  Info: Scan results are available in sbom_scan_results_2025-02-05.txt
  Info: Script completed successfully.

  Info: Cleanup is running ...

  Info: Cleanup finished ...

  Info: script_name1: create_sbom_and_check_for_vulnerabilties.sh
  Info: script_path1: /Users/cloud/repos/ndaal_public_eicar_test_files
  Info: Script path with name: /Users/cloud/repos/ndaal_public_eicar_test_files/create_sbom_and_check_for_vulnerabilties.sh
  Info: Script finished


.....

.. Rubric:: Footnotes

.. [1]
   Debian Linux
   https://www.debian.org

.. [2]
   Wikipedia Artikel Entropie
   https://de.wikipedia.org/wiki/Entropie_(Informationstheorie)

.. [3]
   The ultimate guide to SBOMs - GitLab 
   https://about.gitlab.com/blog/2022/10/25/the-ultimate-guide-to-sboms/

.. [4]
   Briefly Explained: Software Bill of Materials (SBOM) - eco 
   https://international.eco.de/news/briefly-explained-software-bill-of-materials-sbom/

.. [5]
   SBOM Resources Library | CISA 
   https://www.cisa.gov/topics/cyber-threats-and-advisories/sbom/sbomresourceslibrary

.. [6]
   SBOM Standard Formats: Complete Guide - Scribe Security 
   https://scribesecurity.com/sbom/standard-formats/

.. [7]
   The Complete Guide to SBOMs - FOSSA 
   https://fossa.com/learn/sboms

.. [8]
   Technische Richtlinie TR-03183: Cyber Resilience Requirements - BSI 
   https://www.bsi.bund.de/SharedDocs/Downloads/DE/BSI/Publications/TechGuidelines/TR03183/BSI-TR-03183-2.pdf?__blob=publicationFile&v=7
   
.. [9]
   Empfehlungen für das Management von Software-Bill-of-Materials (SBOM) - BSI 
   https://media.defense.gov/2023/Dec/14/2003359097/-1/-1/0/CSI-SCRM-SBOM-MANAGEMENT.PDF
   
.. [10]
   Authoritative Guide to SBOM - CycloneDX 
   https://cyclonedx.org/guides/OWASP_CycloneDX-Authoritative-Guide-to-SBOM-en.pdf

.. [11]
   The SPDX specification is an international open standard (ISO/IEC 5962:2021)
   https://spdx.dev/use/specifications/

.. [12]
   CycloneDX Bill of Materials Specification (ECMA-424)
   https://github.com/CycloneDX/specification
   