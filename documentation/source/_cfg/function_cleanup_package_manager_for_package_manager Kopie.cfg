# Author: Pierre Gronau <Pierre.Gronau@ndaal.eu>
# Copyright 2024
# License: All content is licensed under the terms of the <Apache 2.0>
# Developed on: Debian 12.8x; macOS Sequoia x86 architecture
# Tested on: Debian 12.8x; macOS Sequoia x86 architecture

# Function to clean package manager cache and history
cleanup_package_manager() {
    local package_manager="${1}"

    case "${package_manager}" in
        "-apt"|"-dpkg"|"-debian"|"-ubuntu"|"apt"|"dpkg"|"debian"|"ubuntu")
            printf "Info: Using apt package manager\n"
            printf "Info: Cleaning apt cache and history\n"
            sudo apt-get clean || true
            sudo apt-get autoclean || true
            sudo apt-get autoremove || true
            sudo rm -r -f -v /var/lib/apt/lists/* || true
            ;;
        -apk|apk)
            printf "Info: Using apk package manager\n"
            printf "Info: Cleaning apk cache and history\n"
            sudo apk cache clean || true
            sudo rm -f -r -v /var/cache/apk/* || true
            sudo apk autoremove || true
            sudo apk fix || true
            ;;
        -pacman|-arch|pacman|arch)
            printf "Info: Using pacman package manager\n"
            printf "Info: Updating package database\n"
            sudo pacman -Sy || true
            printf "Info: Removing orphaned packages\n"
            sudo pacman -Rns "$(pacman -Qtdq)" || true
            printf "Info: Cleaning pacman cache\n"
            sudo pacman -Scc --noconfirm || true
            printf "Info: Removing old cached versions\n"
            sudo paccache -r || true
            sudo paccache -rk1 || true
            printf "Info: Cleaning unused sync databases\n"
            sudo pacman -Sc --noconfirm || true
            ;;
        -zypper|-suse|-opensuse|zypper|suse|opensuse)
            printf "Info: Using zypper package manager\n"
            printf "Info: Cleaning zypper cache\n"
            sudo zypper clean -a || true
            printf "Info: Removing old kernels\n"
            sudo zypper purge-kernels -y || true
            printf "Info: Removing orphaned packages\n"
            sudo zypper packages --orphaned | awk '{print $4}' | sudo xargs zypper remove -y || true
            printf "Info: Cleaning up old repository data\n"
            sudo zypper clean --all || true
            printf "Info: Removing unneeded dependencies\n"
            sudo zypper rm -u || true
            ;;
        -brew|-mac|-macos|-osx|brew|mac|macos|osx)
            printf "Info: Using Homebrew package manager\n"
            printf "Info: Cleaning Homebrew cache\n"
            brew cleanup -s || true
            printf "Info: Removing old versions of installed formulae\n"
            brew cleanup --prune=all || true
            printf "Info: Removing stale lock files and outdated downloads\n"
            brew cleanup --prune-prefix || true
            printf "Info: Checking Homebrew for potential problems\n"
            brew doctor || true
            ;;
        -dnf|dnf)
            printf "Info: Using dnf package manager\n"
            printf "Info: Cleaning dnf cache\n"
            sudo dnf clean all || true
            printf "Info: Removing old kernels\n"
            sudo dnf remove "$(dnf repoquery --installonly --latest-limit=-2 -q)" || true
            printf "Info: Removing unused dependencies\n"
            sudo dnf autoremove -y || true
            printf "Info: Cleaning up temporary files\n"
            sudo dnf clean dbcache expire-cache metadata || true
            ;;
        -rpm|rpm)
            printf "Info: Using rpm package manager\n"
            printf "Info: Cleaning rpm cache\n"
            sudo rpm --rebuilddb || true
            printf "Info: Verifying rpm database\n"
            sudo rpm -qa --qf '%{name}-%{version}-%{release}.%{arch}\n' | sort > /tmp/rpm_list
            sudo rpm -Va --nofiles --nodigest | sort > /tmp/rpm_verify
            printf "Info: Cleaning up temporary files\n"
            sudo rm -f -v /var/lib/rpm/__db* || true
            ;;
        -yum|yum)
            printf "Info: Using yum package manager\n"
            printf "Info: Cleaning yum cache\n"
            sudo yum clean all || true
            printf "Info: Removing old kernels\n"
            sudo package-cleanup --oldkernels --count=2 -y || true
            printf "Info: Removing unused dependencies\n"
            sudo yum autoremove -y || true
            printf "Info: Cleaning up temporary files\n"
            sudo yum clean headers packages metadata dbcache || true
            ;;
        -xbps|-void|xbps|void)
            printf "Info: Using xbps package manager\n"
            printf "Info: Cleaning xbps cache\n"
            sudo xbps-remove -O || true
            printf "Info: Removing orphaned packages\n"
            sudo xbps-remove -o || true
            printf "Info: Syncing package database\n"
            sudo xbps-install -S || true
            ;;
        -pisi|-solus|pisi|solus)
            printf "Info: Using pisi package manager\n"
            printf "Info: Cleaning pisi cache\n"
            sudo pisi delete-cache || true
            printf "Info: Removing orphaned packages\n"
            sudo pisi remove-orphans || true
            printf "Info: Checking system consistency\n"
            sudo pisi check || true
            ;;
        -nix|-nixos|nix|nixos)
            printf "Info: Using nix package manager\n"
            printf "Info: Cleaning nix cache\n"
            sudo nix-collect-garbage -d || true
            printf "Info: Optimizing nix store\n"
            sudo nix-store --optimize || true
            printf "Info: Removing old generations\n"
            sudo nix-env --delete-generations old || true
            ;;
        -yay|yay)
            printf "Info: Using yay package manager\n"
            printf "Info: Cleaning yay cache\n"
            yay -Scc --noconfirm || true
            printf "Info: Removing orphaned packages\n"
            yay -Yc --noconfirm || true
            printf "Info: Syncing package database\n"
            yay -Syy || true
            ;;
        -equo|-sabayon|equo|sabayon)
            printf "Info: Using equo package manager\n"
            printf "Info: Cleaning equo cache\n"
            sudo equo cleanup || true
            printf "Info: Removing orphaned packages\n"
            sudo equo depclean || true
            printf "Info: Checking system integrity\n"
            sudo equo rescue check || true
            ;;
        -aurman|aurman)
            printf "Info: Using aurman package manager\n"
            printf "Info: Cleaning aurman cache\n"
            aurman -Scc --noconfirm || true
            printf "Info: Removing orphaned packages\n"
            aurman -Yc --noconfirm || true
            printf "Info: Syncing package database\n"
            aurman -Syy || true
            ;;
        -flatpak|flatpak)
            printf "Info: Using flatpak package manager\n"
            printf "Info: Cleaning flatpak unused runtimes\n"
            flatpak uninstall --unused -y || true
            printf "Info: Removing old flatpak versions\n"
            flatpak uninstall --older-than=6months -y || true
            printf "Info: Cleaning flatpak cache\n"
            flatpak repair --user || true
            ;;
        -snap|snap)
            printf "Info: Using snap package manager\n"
            printf "Info: Cleaning snap cache\n"
            sudo snap set system refresh.retain=2 || true
            sudo snap set system autorefresh.metered=hold || true
            printf "Info: Removing disabled snaps\n"
            snap list --all | awk '/disabled/{print $1, $3}' | while read snapname revision; do sudo snap remove "$snapname" --revision="$revision"; done || true
            ;;
        -cargo|-rust|cargo|rust)
            printf "Info: Using cargo package manager\n"
            printf "Info: Cleaning cargo cache\n"
            cargo cache -a || true
            printf "Info: Removing old cargo registry index\n"
            cargo cache --autoclean || true
            ;;
        -portage|portage)
            printf "Info: Using portage package manager\n"
            printf "Info: Cleaning portage cache\n"
            sudo eclean distfiles || true
            printf "Info: Removing unused dependencies\n"
            sudo emerge --depclean || true
            printf "Info: Cleaning package database\n"
            sudo emaint --fix all || true
            ;;
        -paludis|paludis)
            printf "Info: Using paludis package manager\n"
            printf "Info: Cleaning paludis cache\n"
            sudo paludis --clean || true
            printf "Info: Removing unused packages\n"
            sudo cave purge || true
            ;;
        -conary|conary)
            printf "Info: Using conary package manager\n"
            printf "Info: Cleaning conary cache\n"
            sudo conary remove --old || true
            printf "Info: Syncing repositories\n"
            sudo conary sync || true
            printf "Info: Verifying system integrity\n"
            sudo conary verify || true
            ;;
        -slackpkg|-slackware|slackpkg|slackware)
            printf "Info: Using slackpkg package manager\n"
            printf "Info: Cleaning slackpkg cache\n"
            sudo slackpkg clean-system || true
            printf "Info: Removing old packages\n"
            sudo slackpkg remove-old || true
            printf "Info: Cleaning package lists\n"
            sudo rm -rf /var/lib/slackpkg/* || true
            ;;
        -zeroinstall|zeroinstall)
            printf "Info: Using zeroinstall package manager\n"
            printf "Info: Cleaning zeroinstall cache\n"
            0install store purge || true
            printf "Info: Removing old implementations\n"
            0install store optimise || true
            printf "Info: Updating catalogs\n"
            0install update --all || true
            ;;
        -pip|-pip3|pip|pip3)
            printf "Info: Using pip package manager\n"
            printf "Info: Cleaning pip cache\n"
            pip cache purge || true
            printf "Info: Removing unused dependencies\n"
            pip-autoremove || true
            printf "Info: Upgrading outdated packages\n"
            pip list --outdated --format=freeze | grep -v '^\-e' | cut -d = -f 1 | xargs -n1 pip install -U || true
            ;;
        -h|-help|--help)
            printf "HINT: Select an argument to clean the cache for a specific package manager.\n"
            ;;
        *)
            printf "NOTE: Select an argument to clean the cache for a specific package manager.\n"
            ;;
    esac
    
    printf "Info: Package manager cache cleaned for '%s'.\n" "${package_manager}"

    # Success case
    return 0
}
