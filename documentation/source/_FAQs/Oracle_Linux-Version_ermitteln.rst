******************************************************************************
FAQ - Oracle Linux-Version ermitteln
******************************************************************************

.. sectionauthor:: Pierre Gronau <Pierre.Gronau@ndaal.eu>

.. _Section_FAQ_Oracle_Linux-Version_ermitteln:

.. contents:: Inhalt - Oracle Linux-Version ermitteln
   :depth: 3

Oracle Linux-Version ermitteln
------------------------------------------------------------------------------

Um die Oracle Linux-Version auf Oracle Linux 9 zu ermitteln, können Sie 
folgende Methoden verwenden:

Überprüfen Sie die Datei /etc/oracle-release
==============================================================================

.. code-block:: bash
   :caption: /etc/oracle-release

   cat /etc/oracle-release

Dies zeigt die Oracle Linux-Version an, zum Beispiel:

.. code-block:: text
   :caption: 
   
   "Oracle Linux Server release 9.0"

Verwenden Sie den Befehl lsb_release
==============================================================================

.. code-block:: bash
   :caption: lsb_release

   lsb_release -a

Dieser Befehl liefert detaillierte Informationen über die Distribution, 
einschließlich der Versionsnummer.

Überprüfen Sie die Datei /etc/os-release
==============================================================================

.. code-block:: /etc/os-release
   :caption: lsb_release
   
   cat /etc/os-release

Diese Datei enthält verschiedene Details über das Betriebssystem, 
einschließlich der Versionsinformationen.

Verwenden Sie den Befehl hostnamectl
==============================================================================

.. code-block:: bash
   :caption: hostnamectl

   hostnamectl

Dieser Befehl zeigt System- und Betriebssysteminformationen an, 
einschließlich der Oracle Linux-Version.
Für die genauesten und spezifischsten Oracle Linux-Versionsinformationen 
wird die Datei ``/etc/oracle-release`` empfohlen, da sie speziell 
für Oracle Linux-Distributionen angepasst ist.
