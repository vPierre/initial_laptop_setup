******************************************************************************
FAQ - Ansible - Ansible Collections erstellen
******************************************************************************

.. sectionauthor:: Pierre Gronau <Pierre.Gronau@ndaal.eu>

.. contents:: Inhalt - FAQ - Ansible Collections erstellen
   :depth: 3

.. _Section_FAQs_Ansible_Collections_erstellen:

Ansible Collections erstellen
------------------------------------------------------------------------------

Hier ist eine ausführliche Anleitung, wie Sie mehrere **Ansible-Rollen** [2]_ 
zu einer **Ansible-Collection** zusammenfassen können:


**Ansible Collections** sind eine Möglichkeit, mehrere **Rollen**, **Module**
und **Plugins** in einer strukturierten Form zu bündeln. Dies erleichtert
die Wiederverwendbarkeit und Verteilung von Ansible-Inhalten.
In dieser Anleitung erfahren Sie, wie Sie mehrere Ansible-Rollen
zu einer Collection zusammenfassen können.


Schritt 1: Ansible installieren
==============================================================================


Stellen Sie sicher, dass Ansible auf Ihrem System installiert ist.
Wenn Sie Ansible noch nicht installiert haben, folgen Sie den Anweisungen
in der vorherigen Anleitung zur Installation von Ansible.


Schritt 2: Verzeichnisstruktur für die Collection erstellen
==============================================================================


Erstellen Sie ein Verzeichnis für Ihre Ansible-Collection. Die Struktur
einer Collection sieht wie folgt aus:

my_namespace/ └── my_collection/ ├── docs/ ├── meta/ │ └── runtime.yml ├── roles/ │ ├── role1/ │ └── role2/ ├── plugins/ │ ├── modules/ │ └── ... └── playbooks/


1. **Verzeichnis erstellen**:

   .. code-block:: bash

      mkdir -p ~/ansible-collections/my_namespace/my_collection/{docs,meta,roles,plugins,playbooks}


Schritt 3: Rollen hinzufügen
==============================================================================

Fügen Sie Ihre bestehenden Ansible-Rollen in das `roles`-Verzeichnis
der Collection ein.


1. **Rollen kopieren**:

   Angenommen, Sie haben bereits Rollen in einem anderen Verzeichnis, können Sie diese wie folgt kopieren:

   .. code-block:: bash

      cp -r ~/ansible-project/roles/* ~/ansible-collections/my_namespace/my_collection/roles/


Schritt 4: Metadaten für die Collection erstellen
==============================================================================

Erstellen Sie eine ``meta/runtime.yml``-Datei, um die Metadaten
für Ihre Collection zu definieren.


1. **Metadaten erstellen**:

   .. code-block:: yaml

      ---

      dependencies: []

      galaxy_info:

        author: Ihr Name

        description: Eine Beschreibung Ihrer Collection

        license: MIT

        min_ansible_version: 2.9

        platforms:

          - name: EL

            versions:

              - 7

              - 8

          - name: Ubuntu

            versions:

              - bionic

              - focal

      ```


Schritt 5: Dokumentation hinzufügen
==============================================================================

Fügen Sie Dokumentation für Ihre Collection im `docs`-Verzeichnis hinzu.
Dies kann eine README-Datei oder andere relevante Dokumente sein.


1. **Dokumentation erstellen**:

   .. code-block:: bash

      echo "# My Collection" > ~/ansible-collections/my_namespace/my_collection/docs/README.md


Schritt 6: Collection testen
==============================================================================

Bevor Sie Ihre Collection veröffentlichen, sollten Sie sie testen,
um sicherzustellen, dass alles wie erwartet funktioniert.


1. **Testen der Collection**:

   Sie können die Collection lokal testen, indem Sie ein Playbook erstellen,
   das die Rollen aus Ihrer Collection verwendet. Erstellen Sie ein Playbook
   im `playbooks`-Verzeichnis:

   .. code-block:: yaml

      ---

      - hosts: localhost

        roles:

          - role1

          - role2


2. **Playbook ausführen**:

   .. code-block:: bash

      ansible-playbook -i localhost, ~/ansible-collections/my_namespace/my_collection/playbooks/test_playbook.yml


Schritt 7: Collection packen
==============================================================================

Um Ihre Collection zu verteilen, müssen Sie sie packen. Ansible bietet
den Befehl ``ansible-galaxy`` zum Packen von Collections.


1. **Collection packen**:

   .. code-block:: bash

      cd ~/ansible-collections/my_namespace

      ansible-galaxy collection build my_collection


   Dies erstellt eine ``.tar.gz``-Datei in Ihrem aktuellen Verzeichnis.


Schritt 8: Collection veröffentlichen
==============================================================================

Sie können Ihre Collection auf Ansible Galaxy oder einem anderen Repository
veröffentlichen.


1. **Veröffentlichen auf Ansible Galaxy**:

   Um Ihre Collection auf Ansible Galaxy zu veröffentlichen, müssen Sie sich zuerst anmelden:

   .. code-block:: bash

      ansible-galaxy login


   Dann können Sie die Collection mit dem folgenden Befehl hochladen:

   .. code-block:: bash

      ansible-galaxy collection publish my_collection-*.tar.gz


Fazit
------------------------------------------------------------------------------

Mit diesen Schritten haben Sie gelernt, wie Sie mehrere Ansible-Rollen
zu einer Ansible-Collection zusammenfassen können. Ansible Collections
bieten eine strukturierte Möglichkeit, Ihre Automatisierungsinhalte
zu organisieren und zu verteilen. Sie können die Collection weiter anpassen
und zusätzliche Rollen oder Module hinzufügen,
um Ihre spezifischen Anforderungen zu erfüllen.

Ansible-Struktur und von Ansible-Dateien nach dem Boilerplate-Prinzip
------------------------------------------------------------------------------

Mit dem **bash Skript** ist das Erstellen einer Ansible-Struktur und 
von Ansible-Dateien nach dem Boilerplate-Prinzip möglich. Das Skript erstellt 
die **Ansible Rolle** ``chrony``.

.. literalinclude::
    ../_scripts/create_ansible_structure.sh
   :language: text
   :force:
   :linenos:
   :caption: create_ansible_structure.sh


.. Seealso:: 

   - :ref:`FAQ - Ansible - Ansible Collections erstellen <Section_FAQs_Ansible_Collections_erstellen>`
   - :ref:`FAQ - Ansible - Ansible Rolle unter Debian nutzen <Section_FAQ_Ansible_Rolle_unter_Debian_nutzen>`
   - :ref:`FAQ - Ansible - Best Practices - Bewährte Verfahren <Section_FAQ_Ansible_Best_Practices>`


.....

.. Rubric:: Footnotes

.. [1]
   Debian Linux
   https://www.debian.org

.. [2]
   Ansible
   https://github.com/ansible/ansible

.. [3]
   Good Practices for Ansible - GPA von Red Hat
   https://redhat-cop.github.io/automation-good-practices/
   https://github.com/redhat-cop/automation-good-practices
