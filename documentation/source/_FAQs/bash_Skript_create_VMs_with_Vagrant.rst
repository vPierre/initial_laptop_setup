******************************************************************************
FAQ - bash Skript - create_VMs_with_Vagrant.sh
******************************************************************************

.. sectionauthor:: Pierre Gronau <Pierre.Gronau@ndaal.eu>

.. contents:: Inhalt - FAQ - bash Skript
   :depth: 3

.. _Section_FAQ_bash_Skript_create_VMs_with_Vagrant.sh:

Bash-Skript zum Aufbau der Systemhärtungsumgebung
------------------------------------------------------------------------------

Dieses Bash-Skript [1]_ ist ein umfangreiches und robustes Programm 
zur Erstellung und Verwaltung von Konfigurationsdateien für einen Paketmanager. 
Hier ist eine ausführliche Beschreibung seiner Funktionalität 
und seines Ablaufs:

Initialisierung und Fehlerbehandlung
------------------------------------------------------------------------------

Das Skript beginnt mit der Festlegung strenger Ausführungsoptionen:

- ``set -o errexit``: Beendet das Skript bei Auftreten eines Fehlers.
- ``set -o errtrace``: Ermöglicht die Fehlerbehandlung in Funktionen.
- ``set -o nounset``: Verhindert die Verwendung nicht deklarierter Variablen.
- ``set -o pipefail``: Gibt Fehler in Pipelines weiter.

Eine benutzerdefinierte Fehlerbehandlungsfunktion ``error_handler`` 
wird definiert, die detaillierte Informationen über aufgetretene Fehler 
ausgibt.

Aufräumfunktion
------------------------------------------------------------------------------

Eine ``cleanup``-Funktion wird definiert und als Trap für verschiedene Signale 
(SIGINT, SIGTERM, EXIT) registriert. Diese Funktion entfernt temporäre Dateien
und führt andere Aufräumaufgaben aus.

Betriebssystemerkennung
------------------------------------------------------------------------------

Das Skript erkennt das Betriebssystem (macOS oder Linux) und setzt 
die Variable ``HOMEDIR`` entsprechend.

Variablen und Logging
------------------------------------------------------------------------------

Wichtige Variablen wie ``USERSCRIPT``, ``VERSION``, und ``LOG_FILE`` 
werden definiert.  Eine Logging-Funktion ``log`` wird erstellt, 
die Nachrichten sowohl auf der Konsole als auch in einer Logdatei ausgibt.

Befehlsüberprüfung
------------------------------------------------------------------------------

Die Funktion ``check_command`` überprüft, ob bestimmte erforderliche 
Befehle (wie ``vagrant``, ``VirtualBox``) verfügbar sind.

Verzeichnisoperationen
------------------------------------------------------------------------------

Das bash Skript definiert Quell- und Zielverzeichnisse und erstellt diese 
bei Bedarf mit der Funktion ``create_directory``.

Hauptfunktionalität
------------------------------------------------------------------------------

Dieses Bash-Skript dient zur automatisierten Verwaltung 
von virtuellen Maschinen (VMs) mithilfe von Vagrant. Es bietet Funktionen 
zum Erstellen, Konfigurieren, Überwachen und Sichern von VMs. Das Skript ist 
modular aufgebaut, mit Funktionen für Fehlerbehandlung, Logging und 
Konfigurationsverwaltung.

Funktionalität
------------------------------------------------------------------------------

Das Skript bietet die folgenden Hauptfunktionen:

*   **VM-Erstellung:**  Erstellt neue VMs basierend auf Konfigurationen, die in Arrays definiert sind (z.B. `vms`, `vm_resources`, `vm_networks`). Es verwendet Vagrant, um die VMs aus vordefinierten Boxen zu erstellen und die Hardware-Ressourcen (CPU, RAM, Festplatte) zuzuweisen.

*   **Konfiguration:**
    *   **Netzwerk:** Konfiguriert statische IP-Adressen für jede VM.
    *   **DNS:**  Setzt DNS-Server innerhalb der VMs, um die Namensauflösung zu gewährleisten.
    *   **Sicherheit:**  Härtet die VMs, indem es die SSH-Konfiguration anpasst (Deaktivierung von Root-Login, Passwort-Authentifizierung), eine Firewall (ufw) aktiviert und fail2ban installiert, um Brute-Force-Angriffe zu verhindern.
    *   **Paketmanagement:** Installiert und aktualisiert Softwarepakete mithilfe verschiedener Paketmanager (apt, yum, dnf usw.), je nach Betriebssystem der VM.

*   **Überwachung:**  Überwacht die CPU-, Speicher- und Festplattenauslastung der VMs.  Es sammelt Systemmetriken und speichert sie in Logdateien.

*   **Logging:**  Verwendet ein umfassendes Logging-System, um Informationen, Warnungen und Fehler zu protokollieren. Die Logmeldungen werden farbig dargestellt und in einer Logdatei gespeichert.

*   **Fehlerbehandlung:**  Implementiert eine robuste Fehlerbehandlung mit `set -o errexit` und einem `trap`, der Fehler abfängt und eine `error_handler`-Funktion aufruft. Diese Funktion protokolliert den Fehler, führt Bereinigungsmaßnahmen durch und beendet das Skript.

*   **Konfigurationsmanagement:**  Verwendet Konfigurationsdateien, um Einstellungen wie VM-Namen, Ressourcen, Netzwerkparameter und Paketversionen zu verwalten.

Ablauf
------------------------------------------------------------------------------

Der Ablauf des Skripts lässt sich in folgende Phasen unterteilen:

1.  **Initialisierung:**
    *   **Umgebungsvariablen setzen:** Definiert wichtige Variablen wie Zeitstempel, Skriptname, Verzeichnisse und Standardwerte.
    *   **Optionen parsen:** Verarbeitet Kommandozeilenargumente mit `getopts`, um Optionen wie `--destroy`, `--verbose` und `--log-level` zu verarbeiten.
    *   **Verzeichnisse erstellen:**  Erstellt die notwendigen Verzeichnisse für Logs und Konfigurationen.
    *   **Abhängigkeiten prüfen:**  Stellt sicher, dass alle erforderlichen Befehle (vagrant, virtualbox, git usw.) installiert sind.
    *   **Vagrant Plugin prüfen/installieren:** Prüft ob das Vagrant SCP Plugin installiert ist und installiert es gegebenenfalls.
    *   **Festplattenplatz prüfen:** Überprüft, ob genügend freier Festplattenplatz vorhanden ist.
    *   **Logging initialisieren:**  Erstellt die Logdatei und schreibt Initialisierungsinformationen.
    *   **Konfigurationsdateien einlesen:** Liest Konfigurationsdateien ein.
    *   **Quellcode Dateien prüfen:** Prüft Dateien mit diversen Funktionen auf Gültigkeit und Sicherheit (siehe ``source_file`` Funktion).

2.  **VM-Verwaltung (Hauptteil):**
    *   **VMs zerstören (optional):**  Wenn die Option `--destroy` angegeben wurde, werden alle vorhandenen VMs mit `vagrant destroy` gelöscht.
    *   **VMs erstellen und konfigurieren:** Iteriert über die in `vms` definierten VMs und führt für jede VM folgende Schritte aus:
        *   Erstellt ein Verzeichnis für die VM.
        *   Erstellt eine `Vagrantfile` mit der VM-Konfiguration (Box, Hostname, Netzwerk, Ressourcen).
        *   Führt `vagrant up` aus, um die VM zu erstellen und zu starten.
        *   Verifiziert den VM Status.
        *   Konfiguriert DNS.
        *   Konfiguriert ``systemd-resolved`` falls vorhanden.
        *   Installiert Basis-Pakete.
        *   Führt Sicherheitskonfiguration durch.
        *   Installiert VM-spezifische Pakete.
        *   Führt ein Update aller Pakete auf der VM durch.

3.  **Überwachung:**
    *   Sammelt Metriken (CPU, Speicher, Festplatte) von den VMs.
    *   Speichert die Metriken in Logdateien.

4.  **Abschluss:**
    *   Protokolliert eine Zusammenfassung der ausgeführten Aktionen (Anzahl der erstellten VMs, Fehler usw.).
    *   Führt Bereinigungsmaßnahmen durch (z.B. Backup von Logdateien).
    *   Beendet das Skript mit einem entsprechenden Exit-Code.

Funktionsweise im Detail
------------------------------------------------------------------------------

*   **`error_handler()`:**  Wird bei einem Fehler aufgerufen.  Protokolliert den Fehler, führt `cleanup()` aus und beendet das Skript.

*   **`cleanup()`:**  Führt Bereinigungsarbeiten durch, wie z.B. das Sichern von Logdateien.

*   **`create_vm()`:**  Erstellt eine VM mithilfe von Vagrant.  Generiert die `Vagrantfile` basierend auf den Konfigurationen und führt `vagrant up` aus.

*   **`configure_dns()`:**  Konfiguriert die DNS-Einstellungen innerhalb der VM, indem es die `/etc/resolv.conf` Datei anpasst.

*   **`install_packages()`:**  Installiert Softwarepakete innerhalb der VM mit dem angegebenen Paketmanager.

*   **`configure_security()`:**  Härtet die VM, indem es die SSH-Konfiguration anpasst, eine Firewall aktiviert und fail2ban installiert.

*   **`monitor_vm_resources()`:**  Überwacht die Ressourcen der VM (CPU, Speicher, Festplatte) und protokolliert Warnungen, wenn Schwellenwerte überschritten werden.

*   **`log_message()`:**  Protokolliert Meldungen mit verschiedenen Log-Leveln (ERROR, WARNING, INFO, DEBUG).

*   **`parse_arguments()`:**  Verarbeitet die Kommandozeilenargumente.

*   **`initialize()`:**  Führt die Initialisierungsschritte des Skripts aus.

*    **`source_file()`:** Liest Konfigurationsdateien ein und prüft sie auf Gültigkeit und Sicherheit.

*   **`main()`:**  Die Hauptfunktion, die den Ablauf des Skripts steuert.

Hinweise
------------------------------------------------------------------------------

*   Das Skript verwendet Arrays (`vms`, `vm_resources`, `vm_networks`) zur Konfiguration. Diese Arrays sollten vor der Ausführung des Skripts entsprechend angepasst werden.
*   Das Skript erfordert Vagrant und VirtualBox (oder einen anderen Vagrant-Provider).
*   Das Skript ist für Debian-basierte Systeme optimiert, kann aber mit Anpassungen auch auf anderen Linux-Distributionen verwendet werden.
*   Das Skript verwendet ``set -e``, was bedeutet, dass es beim ersten Fehler abbricht.
*   Die ``source_file`` Funktion verwendet viele Einzelprüfungen um sicherzustellen, dass die einzulesenden Konfigurationsdateien sicher und gültig sind.

Abschluss
------------------------------------------------------------------------------

Zum Schluss ruft das Skript die ``cleanup``-Funktion auf, gibt Informationen 
über den Skriptpfad aus und beendet sich ordnungsgemäß.

Zusammenfassung
------------------------------------------------------------------------------

Dieses Skript ist ein nützliches Werkzeug zur Automatisierung 
der VM-Verwaltung mit Vagrant. Es bietet Funktionen zur Erstellung, 
Konfiguration, Überwachung und Sicherung von VMs und kann an die 
spezifischen Bedürfnisse einer Umgebung angepasst werden.  Die starke 
Fehlerbehandlung und das umfassende Logging helfen bei der Diagnose von Problemen.

Code des bash Skripts create_VMs_with_Vagrant.sh
------------------------------------------------------------------------------

Mit dem **bash Skript** ``create_VMs_with_Vagrant.sh`` wird
die Systemhärtungsumgebung gespawnt:

.. literalinclude::
    ../_scripts/create_VMs_with_Vagrant.sh2
   :language: text
   :force:
   :linenos:
   :force:
   :emphasize-lines: 1,2,20-50
   :caption: create_VMs_with_Vagrant.sh

.. Tip::

   Das bask Skript ist hier zu finden: [2]_ 

.....

.. Rubric:: Footnotes

.. [1]
   bash
   https://www.gnu.org/software/bash/

.. [2]
   bash Skript create_VMs_with_Vagrant.sh
   https://gitlab.com/vPierre/initial_laptop_setup/-/blob/8129db56af8d290a3a325632c6551ec762ed3bb6/scripts/create_VMs_with_Vagrant.sh