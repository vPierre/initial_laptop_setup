************************************************************************************************
FAQ  - Automatische Updates mit unbeaufsichtigten Upgrades
************************************************************************************************

.. sectionauthor:: Pierre Gronau <Pierre.Gronau@ndaal.eu>

.. contents:: Inhalt - Sicherheit - Container
   :depth: 3

.. admonition:: Motivation
   :class: tip

   Automatische Updates mit unbeaufsichtigten Upgrades im Rahmen 
   der Systemhärtung ist aus mehreren Gründen wichtig:
   
   - **Sicherheitsverbesserung**: Regelmäßige Updates schließen 
     bekannte Sicherheitslücken, die von Angreifern ausgenutzt werden 
     könnten. Durch stündliches Patchen wird das Risiko 
     eines erfolgreichen Angriffs erheblich reduziert.

   - **Minimierung von Ausfallzeiten**: Durch die Automatisierung 
     des Patchens während weniger geschäftiger Zeiten können Systemausfälle
     und Unterbrechungen minimiert werden, was die Verfügbarkeit
     der Dienste erhöht.

   - **Konsistenz und Zuverlässigkeit**: Automatisierte Patch-Prozesse
     gewährleisten, dass alle Systeme zeitnah und einheitlich aktualisiert 
     werden, wodurch Inkonsistenzen und potenzielle Schwachstellen 
     vermieden werden.

   - **Compliance-Anforderungen**: Viele Branchen unterliegen strengen 
     Vorschriften, die regelmäßige Sicherheitsupdates vorschreiben. 
     Unbeaufsichtigtes Patchen hilft, diese Anforderungen effizient
     zu erfüllen.

   - **Ressourcenschonung**: Automatisierte Prozesse reduzieren 
     den manuellen Aufwand für IT-Administratoren, sodass sie sich 
     auf strategischere Aufgaben konzentrieren können, anstatt Zeit
     mit der manuellen Aktualisierung von Systemen zu verbringen.

   - **Schnelle Reaktion auf Bedrohungen**: In einer sich schnell
     verändernden Bedrohungslandschaft ermöglicht stündliches Patchen
     eine prompte Reaktion auf neu entdeckte Schwachstellen 
     und Sicherheitsanforderungen.

   Durch die Implementierung eines unbeaufsichtigten stündlichen 
   Patchprozesses wird die Sicherheit und Stabilität der Systeme 
   erheblich verbessert, während gleichzeitig der Verwaltungsaufwand
   für IT-Teams reduziert wird.

Automatische Updates mit unbeaufsichtigten Upgrades unter Debian und Ubuntu
------------------------------------------------------------------------------

Linux-Versionen werden häufig aktualisiert, und Sicherheitsaktualisierungen 
sollten umgehend installiert werden. Debian [1]_ und Ubuntu verwenden 
das APT-System, und obwohl es relativ einfach ist, alle anstehenden 
Aktualisierungen manuell zu installieren, kann es sein, 
dass der Administrator nicht daran denkt oder lieber nachts schläft,
anstatt nach Updates zu suchen. Dieser Artikel bezieht sich hauptsächlich
auf Serverinstallationen.

Das manuelle Aktualisieren und Installieren von Paketen mit apt upgrade ist
auch bei der Verwendung von unattended-upgrades möglich. Wenn ein Upgrade,
das mit ``unattended-upgrades`` gestartet wurde, gerade läuft, wenn **apt**
von der Kommandozeile aus verwendet wird, informiert das apt-System
den Benutzer darüber, dass eine Operation bereits läuft
und der Benutzer warten muss, bis sie beendet ist,
bevor er mit der Eingabe von apt-Befehlen fortfahren kann.

Ein einfacher Weg für vielbeschäftigte Administratoren
==============================================================================

Installieren Sie unbeaufsichtigte Upgrades, überprüfen Sie,
ob sie aktiviert sind, und überwachen Sie ihre Arbeit
anhand von Logdateien.

bash

.. code-block:: bash

    vagrant@debian-automation-controller:~$ sudo apt-get install unattended-upgrades
    Paketlisten werden gelesen… Fertig
    Abhängigkeitsbaum wird aufgebaut… Fertig
    Statusinformationen werden eingelesen… Fertig
    Vorgeschlagene Pakete:
    bsd-mailx default-mta | mail-transport-agent needrestart powermgmt-base python3-gi
    Die folgenden NEUEN Pakete werden installiert:
    unattended-upgrades
    0 aktualisiert, 1 neu installiert, 0 zu entfernen und 0 nicht aktualisiert.
    Es müssen noch 0 B von 63,3 kB an Archiven heruntergeladen werden.
    Nach dieser Operation werden 308 kB Plattenplatz zusätzlich benutzt.
    Vorkonfiguration der Pakete ...
    Vormals nicht ausgewähltes Paket unattended-upgrades wird gewählt.
    (Lese Datenbank ... 30416 Dateien und Verzeichnisse sind derzeit installiert.)
    Vorbereitung zum Entpacken von .../unattended-upgrades_2.9.1+nmu3_all.deb ...
    Entpacken von unattended-upgrades (2.9.1+nmu3) ...
    unattended-upgrades (2.9.1+nmu3) wird eingerichtet ...
    Trigger für man-db (2.11.2-2) werden verarbeitet ...

    sudo dpkg-reconfigure -plow unattended-upgrades

Der dpkg-reconfigure-Befehl zeigt ein Dialogfeld an, in dem Sie gefragt werden,
ob stabile Sicherheitsupdates automatisch heruntergeladen und installiert
werden sollen oder nicht. Überprüfen Sie, ob die Frage mit Ja beantwortet wird.

``unattended-upgrades`` sucht zweimal täglich zu zufälligen Zeiten nach Updates
und installiert stabile Sicherheitsupdates.

Überprüfen Sie, was passiert, indem Sie die Protokolldateien im Verzeichnis
``/var/log/unattended-upgrades/`` und die Datei ``/var/log/dpkg.log`` lesen.

Wo Sie die Dokumentation finden
==============================================================================

Die Konfigurationsdatei ``/etc/apt/apt.conf.d/50unattended-upgrades``
enthält eine Dokumentation in den Kommentaren. In diesem Dokument wird
auf diese Datei von nun an als ``50unattended-upgrades`` verwiesen.

Die Readme-Datei ``/usr/share/doc/unattended-upgrades/README.md.gz``
enthält nützliche Anleitungen. Sie kann mit dem Befehl
``zless`` (verfügbar im Paket gzip) gelesen werden.
Dieses Dokument verweist auf diese Datei von nun an als README.

Das Debian-Wiki hat eine Seite <https://wiki.debian.org/UnattendedUpgrades>.

Die Ubuntu Server Docs Seite hat ein Kapitel „Automatische Updates“
über ``unattended-upgrades``.

Man-Seite von ``unattended-upgrade``, ``man unattended-upgrade``.

Erweiterte Konfiguration
==============================================================================

Wenn Sie den einfachen Weg wählen, werden Sie feststellen, dass nicht
alle Upgrades automatisch installiert werden. Vielleicht möchten Sie
auch mehr Kontrolle darüber haben, was zusätzlich zur Installation
von Aktualisierungen automatisch geschieht.

Die Konfigurationsdatei ``/etc/apt/apt.conf.d/50unattended-upgrades``
enthält eine Dokumentation in den Kommentaren, also lesen Sie die Datei,
um zu sehen, welche Konfigurationen möglich sind. Sehr nützlich ist es,
``unattended-upgrades`` so zu konfigurieren, dass E-Mails verschickt
werden, wenn etwas passiert.

Es ist wichtig zu wissen, dass die Konfigurationsdatei bei der Installation oder dem Upgrade des unattended-upgrades-Pakets erstellt wird. Wenn also unattended-upgrades selbst aktualisiert wird oder auf die nächste Betriebssystemversion umgestellt wird, führen Änderungen in dieser Datei zu Konflikten, die manuell gelöst werden müssen. In der Dokumentationsdatei README wird vorgeschlagen, die Datei 52unattended-upgrades-local zu erstellen, anstatt die ursprüngliche Konfigurationsdatei zu ändern.

Wenn Sie möchten, können Sie die Datei ``/etc/apt/apt.conf.d/20auto-upgrades``
daraufhin überprüfen, ob sie diese Zeilen enthält:

bash

.. code-block:: bash

    APT::Periodic::Update-Package-Lists "1";
    APT::Periodic::Unattended-Upgrade "1";

Wenn Sie den Befehl ``dpkg-reconfigure`` ausgeführt und mit Ja beantwortet haben, sollten die Zeilen vorhanden sein. Die Zeilen steuern die Aktivierung von unattended-upgrades.

E-Mail senden
==============================================================================

Damit das Versenden von E-Mails funktioniert, muss der Host über ein funktionierendes E-Mail-System verfügen, das E-Mails versenden kann. Überprüfen Sie das zuerst.

Aktivieren Sie den E-Mail-Versand von  unattended-upgrades, indem Sie die Konfigurationsdatei 52unattended-upgrades-local bearbeiten (erstellen Sie die Datei, wenn sie nicht im Verzeichnis /etc/apt/apt.conf/ existiert). Kopieren Sie die Zeile

bash

``//Unattended-Upgrade::Mail "";```

aus 50unattended-upgrades, entkommentieren Sie sie und fügen Sie die Ziel-E-Mail-Adresse zwischen den „-Anführungszeichen“ ein.

Es gibt auch eine Einstellung, mit der Sie festlegen können, ob bei unattended-upgrades immer eine E-Mail verschickt wird oder nur bei Fehlern.

.. Hint::
   Aus Sicherheitsgründen empfehlen wir auf die Nutzung von E-Mail komplett zu verzichten - siehe Systemhärtung

bash

.. code-block:: bash
    
    // Set this value to "true" to get emails only on errors. Default
    // is to always send a mail if Unattended-Upgrade::Mail is set
    //Unattended-Upgrade::MailOnlyOnError "false";

Wenn Sie die Einstellung ändern möchten, kopieren Sie die Zeilen nach 52unattended-upgrades-local, entkommentieren Sie die Einstellungszeile und ändern Sie „false“ in „true“.

Das Überprüfen mit

bash

``sudo unattended-upgrade --dry-run -d```

ist eine gute Idee, nachdem Sie die Konfigurationsdatei geändert haben.

Zumindest unter Debian 10, 11 und Ubuntu 22.04 erstellt die Installation
von unattended-upgrades einen symbolischen Link unattended-upgrades
zu unattended-upgrade im Verzeichnis ``/usr/bin/`` und/oder ``/bin/``,
sodass beide Befehle gleich funktionieren.

bash

.. code-block:: bash

    $ ls -lhi /bin/unattended-upgrade*
    11404505 -rwxr-xr-x 1 root root 98K tammi  15  2022 /bin/unattended-upgrade
    11407087 lrwxrwxrwx 1 root root  18 tammi  15  2022 /bin/unattended-upgrades -> unattended-upgrade

Repository hinzufügen
==============================================================================

Das Hinzufügen eines Repositorys, von dem automatisch aktualisiert werden 
soll, erfolgt durch Hinzufügen von Zeilen zu 
*Unattended-Upgrade::Origins-Pattern*. Die Dokumentation dazu finden Sie 
in der Konfigurationsdatei.

Die README weist die Einstellungen in der späteren Konfigurationsdatei 
*52unattended-upgrades-local* an, um die Standardeinstellungen 
zu überschreiben. Ich habe es ausprobiert und festgestellt, dass 
die Einstellung Unattended-Upgrades::Origins-Pattern die Einstellungen 
in der Standarddatei nicht vollständig überschreibt, sondern 
zu den Standardeinstellungen hinzugefügt wird. Es ist also nicht notwendig, 
das komplette Standard Unattended-Upgrade::Origins-Pattern zu kopieren,
es kann hinzugefügt werden.

Zum Beispiel wird das Paket goaccess nicht automatisch aktualisiert, 
wenn es aus dem offiziellen GoAccess-Repository installiert wird. 
Es würde automatisch upgraden, wenn es aus dem normalen Debian-Repository 
installiert würde (es ist in beiden verfügbar). Wenn Sie das 
**GoAccess-Repository** zum Origins-Pattern hinzufügen, führen 
*unattended-upgrades* das Upgrade durch.

Überprüfen Sie die Situation mit:

bash

``sudo apt list --upgradable```

bash

.. code-block:: bash
    
    vagrant@debian-automation-controller:~# LANG=C apt list --upgradable
    Listing... Done
    goaccess/unknown 2:1.6.3-buster amd64 [upgradable from: 2:1.6.2-buster]
    php-tcpdf/buster-backports 6.5.0+dfsg1-1~bpo10+1 all [upgradable from: 6.3.5+dfsg1-1~bpo10+1]
    vagrant@debian-automation-controller:~#

Es gibt also zwei Pakete, die von unattended-upgrades nicht automatisch
aktualisiert wurden. Die Untersuchung von goaccess zeigt, dass es
aus dem offiziellen GoAccess-Repository stammt (und eine ältere Version
aus dem normalen Debian-Repository).

bash

.. code-block:: bash

    vagrant@debian-automation-controller:~# LANG=C apt policy goaccess
    goaccess:
    Installed: 2:1.6.2-buster
    Candidate: 2:1.6.3-buster
    Version table:
        2:1.6.3-buster 500
            500 <https://deb.goaccess.io> buster/main amd64 Packages
    *** 2:1.6.2-buster 100
            100 /var/lib/dpkg/status
        1:1.2-4+b10 500
            500 <http://mirror.hetzner.de/debian/packages> buster/main amd64 Packages
            500 http:// deb.debian.org/debian buster/main amd64 Packages

Wenn Sie *unattended-upgrade --dry-run -d* ausführen, sehen Sie, 
welchen Ursprung die nicht installierten Pakete haben. Das kann dabei 
helfen herauszufinden, was zu Origins-Pattern hinzugefügt werden muss.

Die Untersuchung des Protokolls der letzten unbeaufsichtigten Upgrades zeigt,
welche Ursprünge beim automatischen Upgrade berücksichtigt werden:

Verify

.. code-block:: bash
    
    2022-09-05 08:28:08,955 INFO Checking if system is running on battery is skipped. Please install
    powermgmt-base package to check power status and skip installing updates when the system
    is running on battery.
    2022-09-05 08:28:08,960 INFO Initial blacklist :
    2022-09-05 08:28:08,960 INFO Initial whitelist:
    2022-09-05 08:28:08,960 INFO Starting unattended upgrades script
    2022-09-05 08:28:08,960 INFO Allowed origins are:
    origin=Debian,codename=buster,label=Debian,
    origin=Debian,codename=buster,label=Debian-Security

Das GoAccess-Repository ist noch nicht dabei, also füge ich es jetzt zu
``/etc/apt/apt.conf.d/52unattended-upgrades-local`` hinzu.

Nebenbei bemerkt: Wenn Sie unattended-upgrades auf einem Laptop installiert haben, sollten Sie powermgmt-base installieren. Damit startet unattended-upgrades die Upgrades nicht im Akkubetrieb, wie die Info-Meldung im Log angibt. Auf einem Server, der immer mit Netzstrom betrieben wird, wird diese Info-Meldung nicht mehr angezeigt, wenn Unattended-Upgrade::OnlyOnACPower "false"; zur Konfiguration hinzugefügt wird.

bash

.. code-block:: bash

    Unattended-Upgrade::Origins-Pattern {
    // Taleman added GoAccess 2022-09-05
            "o=GoAccess Repository, n=buster, l=Official GoAccess Repository";
    };
    Unattended-Upgrade::OnlyOnACPower "false";

apt policy zeigte das Feld a für GoAccess nicht an, also habe ich o, n und l verwendet. Infos zu diesen Feldern finden Sie in README.

Die Variablenersetzung wird für ${distro_id}, das die Ausgabe von lsb_release -i enthält, und ${distro_codename}, das die Ausgabe von lsb_release -c enthält, unterstützt. Anstelle von n=buster hätte ich also auch n=${distro_codename} schreiben können.

Es gab zwei Pakete, die nicht automatisch aktualisiert wurden. Das andere, das noch nicht aktualisiert wurde, ist php-tcpdf. Es kann natürlich mit apt upgrade aktualisiert werden, aber es kann auch zu unattended-upgrades hinzugefügt werden, damit es automatisch aktualisiert wird. Die Vorgehensweise ist die gleiche wie bei goaccess.

Überprüfen Sie zuerst die Situation mit apt policy php-tcpdf. Das zeigt, dass es aus dem Debian-Repository-Abschnitt buster-backports installiert ist.

Aus unattended-upgrades.log geht hervor, dass buster-backports nicht zu den erlaubten Ursprüngen gehört. Er ist in der Datei 50unattended-upgrades enthalten, aber auskommentiert. Um ihn zu aktivieren, kopieren Sie die Zeile

bash

``// "o=Debian Backports,a=${distro_codename}-backports,l=Debian Backports";```

nach *52unattended-upgrades-local* in die Origins-Pattern-Einstellung und 
kommentieren Sie sie aus.

Nachdem Sie die Datei bearbeitet haben, überprüfen Sie mit 
*unattended-upgrades --dry-run -d*, ob das hinzugefügte Repository 
nun zu den „Erlaubten Ursprüngen“ gehört und php-tcpdf zu den Paketen gehört, 
die aktualisiert werden.

Zeiten kontrollieren
==============================================================================

unattended-upgrades wird zu zufälligen Zeiten ausgeführt, 
um die Repository-Server zu entlasten. Damit sollen große Lastspitzen
vermieden werden, die auftreten würden, wenn alle Hosts 
zur gleichen Zeit Updates durchführen würden. Überlegen Sie sich gut,
ob Sie dieses Verhalten ändern möchten. Wenn Sie Ihr eigenes Repository 
oder ein Spiegel-Repository betreiben, treffen die Lastspitzen 
Ihren Repository-Server und Sie vermeiden es, die Admins der Repositories
im Internet zu verärgern.

Der systemd steuert, zu welchen Zeiten unattended-upgrades startet. 
Die Datei */lib* */systemd/system/apt-daily.timer* enthält 
einen Timer-Abschnitt, der zweimal täglich mit einer zufälligen Verzögerung 
von 12 Stunden Apt-Downloads startet. Ich schlage vor, dass Sie 
das nicht ändern oder sehr gute Gründe für eine Änderung haben.

Der Rest der Zeitkonfigurationen wird in den Dateien 50unattended-upgrades und 
*52unattended-upgrades-local* vorgenommen.

Die Einstellung Unattended-Upgrade::Update-Days steuert die Wochentage, 
an denen *unattended-upgrades* ausgeführt wird. Die Standardeinstellung 
ist leer, was bedeutet, dass sie jeden Tag ausgeführt wird. Es kann aber 
auch so konfiguriert werden, dass es zum Beispiel nur am Samstag und 
Sonntag ausgeführt wird.

Automatischer Neustart
==============================================================================

Unbeaufsichtigte Aktualisierungen können so eingestellt werden, 
dass sie neu gestartet werden, wenn die installierten Updates 
einen Neustart erfordern. Dies kann entweder sofort oder 
zu einem bestimmten Zeitpunkt geschehen. Diese Konfigurationen 
verwenden Einstellungen:

bash

.. code-block:: bash

    //Unattended-Upgrade::Automatic-Reboot "false";
    //Unattended-Upgrade::Automatic-Reboot-WithUsers "true";
    //Unattended-Upgrade::Automatic-Reboot-Time "02:00";

Die Standardeinstellung ist kein automatischer Neustart. Wenn Sie 
die Einstellung auf „true“ ändern, erfolgt der Neustart sofort
nach der Installation des Upgrades. Automatic-Reboot-Time kann 
so eingestellt werden, dass der Neustart 
zu einem gewünschten Zeitpunkt erfolgt, wenn es installierte Updates 
gibt, die einen Neustart erfordern.

Blacklisting, Whitelisting
==============================================================================

Eine Blacklist verhindert, dass ein Paket, das sonst durch unbeaufsichtigte 
Upgrades aktualisiert werden würde, aktualisiert wird. Die Einstellung 
Package-Blacklist enthält reguläre Ausdrücke. Wenn der Paketname darauf passt,
wird er vom automatischen Upgrade ausgeschlossen.

Ich habe diese Funktion noch nicht genutzt, aber ich denke, dass sie 
bei einer Entwicklungsversion, bei der es sinnvoll ist, das automatische 
Upgrade kritischer Pakete zu verhindern, sinnvoller ist. 
Die Konfigurationsdatei *50unattended-upgrades* enthält Beispiele 
für Pakete, die auf der schwarzen Liste stehen.

In der README steht etwas über die Einstellung Package-Whitelist. 
In der Beschreibung heißt es: „Nur Pakete, die mit den regulären Ausdrücken 
in dieser Liste übereinstimmen, werden für ein Upgrade markiert.“ 
Es gibt keine Beispiele, die verdeutlichen, in welchen Fällen 
es wünschenswert wäre, die Whitelist zu setzen. Nach meinen Experimenten
sieht es so aus, als ob das Hinzufügen von Paketen zur Whitelist bedeutet, 
dass nur diese Pakete automatisch aktualisiert werden und nichts anderes.

Fazit
------------------------------------------------------------------------------

Jetzt wissen Sie, was unattended-upgrades kann und wie Sie es so einrichten 
können, dass es das tut, was Sie wollen.

Es gibt Fälle, in denen unattended-upgrades das Upgrade nicht durchführt,
weil der Befehl *apt upgrade* ein Paket zurückhält. Das passiert, wenn 
das Upgrade ein Paket entfernen oder ein zuvor deinstalliertes Paket 
installieren würde. Um das Upgrade durchzuführen, müssen Sie es selbst
mit dem Befehl *apt full-upgrade* durchführen, der Pakete entfernen 
oder neue, zuvor deinstallierte Pakete installieren kann. Wenn 
*unattended-upgrades* so konfiguriert ist, dass es E-Mails versendet, 
enthält die E-Mail die Zeile:

bash

Packages with upgradable origin but kept back:

Ein letztes Beispiel für *52unattended-upgrades-local* 
von einem meiner Rechner:

bash

.. code-block:: bash

    Unattended-Upgrade::Origins-Pattern {
    // Taleman added 2022-09-05
            "o=GoAccess Repository, n=buster, l=Official GoAccess Repository";
            "o=Debian Backports,a=${distro_codename}-backports,l=Debian Backports";
    "origin=deb.sury.org,archive=${distro_codename}";
    };

    Unattended-Upgrade::Mail "<myemail@domain.tld>";
    Unattended-Upgrade::OnlyOnACPower "false";

Links
------------------------------------------------------------------------------

- Automatische Updates mit unbeaufsichtigten Upgrades unter Debian und Ubuntu [2]_ 

- Debian Updates automatisch installieren [3]_ 

- Configuring Unattended Upgrades on Debian [4]_ 

Deployment Sicht
------------------------------------------------------------------------------

Hier wird angeführt, wie das alles ausgerollt wird (Deployment Sicht).

.. seealso:: Deployment Sicht

   Diese Einstellungen werden mittels eines bash Skriptes aufgebracht.

   .. figure::
      _static/bash-logo-by-vd.png
      :align: center
      :scale: 25 %
      :alt: bash Skript

      bash Skript

   
   <Platzhalter für den Link zum Repo>

   Diese Einstellungen werden mittels einer Ansible Rolle umgesetzt.

   .. figure::
      _static/Ansible_logo-700x700.png
      :align: center
      :scale: 25 %
      :alt: Ansible Rolle

      Ansible Rolle

   
   <Platzhalter für den Link zum Repo>


.....

.. Rubric:: Footnotes

.. [1]
   Debian Linux
   <https://www.debian.org>

.. [2]
   Automatische Updates mit unbeaufsichtigten Upgrades unter Debian und Ubuntu
   <https://www.howtoforge.de/anleitung/automatische-updates-mit-unbeaufsichtigten-upgrades-unter-debian-und-ubuntu/>

.. [3]
   Debian Updates automatisch installieren
   <https://blog.djonz.de/howto/unattended-upgrades/>

.. [4]
   Configuring Unattended Upgrades on Debian
   <https://benheater.com/configuring-unattended-upgrades-on-debian/>
