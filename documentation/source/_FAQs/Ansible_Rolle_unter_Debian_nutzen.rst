******************************************************************************
FAQ - Ansible - Ansible Rolle unter Debian nutzen
******************************************************************************

.. sectionauthor:: Pierre Gronau <Pierre.Gronau@ndaal.eu>

.. contents:: Inhalt - FAQ - Ansible Rolle unter Debian nutzen
   :depth: 3

.. _Section_FAQ_Ansible_Rolle_unter_Debian_nutzen:

Ansible Rolle unter Debian nutzen
------------------------------------------------------------------------------


Um eine **Ansible-Rolle** [2]_ unter **Debian Linux** [1]_ zu nutzen, sind mehrere
Schritte erforderlich. Hier ist eine ausführliche Anleitung,
die die einzelnen Schritte beschreibt.


Schritt 1: Ansible installieren
==============================================================================

Zuerst müssen Sie sicherstellen, dass **Ansible** [2]_ auf Ihrem Debian-System
installiert ist. Sie können Ansible über die Paketverwaltung installieren.


1. **System aktualisieren**:

   .. code-block:: bash

      sudo apt update

      sudo apt upgrade


2. **Ansible installieren**:

   .. code-block:: bash

      sudo apt install ansible


3. **Überprüfen der Installation**:

   Um sicherzustellen, dass Ansible korrekt installiert wurde,
   können Sie die Version überprüfen:

   .. code-block:: bash

      ansible --version


Schritt 2: Verzeichnisstruktur erstellen
==============================================================================

Erstellen Sie ein Verzeichnis für Ihr Ansible-Projekt, in dem Sie
Ihre Rollen und Playbooks organisieren können.


1. **Verzeichnis erstellen**:

   .. code-block:: bash

      mkdir -p ~/ansible-project/roles

      cd ~/ansible-project


Schritt 3: Ansible-Rolle erstellen
==============================================================================

Sie können eine neue Rolle mit dem `ansible-galaxy`-Befehl erstellen.
Dies erstellt die grundlegende Verzeichnisstruktur für die Rolle.


1. **Rolle erstellen**:

   .. code-block:: bash

      ansible-galaxy init my_role


   Dies erstellt ein Verzeichnis `my_role` mit der folgenden Struktur:

my_role/ ├── tasks/ │ └── main.yml ├── handlers/ │ └── main.yml ├── templates/ ├── files/ ├── vars/ │ └── main.yml ├── defaults/ │ └── main.yml └── meta/ └── main.yml


Schritt 4: Ansible-Rolle anpassen
==============================================================================

Passen Sie die Rolle an, indem Sie die entsprechenden Dateien
in den Verzeichnissen `tasks`, `handlers`, `templates`, `files`, `vars`,
`defaults` und `meta` bearbeiten.


1. **Aufgaben definieren**:

Bearbeiten Sie die Datei `tasks/main.yml`, um die Aufgaben zu definieren,
die Ihre Rolle ausführen soll. Beispiel:

.. code-block:: yaml

   ---

   - name: Installiere Apache

     apt:

       name: apache2

       state: present


   - name: Starte Apache

     service:

       name: apache2

       state: started

       enabled: true


Schritt 5: Playbook erstellen
==============================================================================

Erstellen Sie ein Playbook, das die Rolle verwendet. Ein Playbook ist
eine YAML-Datei, die beschreibt, welche Rollen auf welche Hosts
angewendet werden sollen.


1. **Playbook erstellen**:

Erstellen Sie eine Datei namens `site.yml` im Hauptverzeichnis Ihres Ansible-Projekts:

.. code-block:: yaml

   ---

   - hosts: all

     become: yes

     roles:

       - my_role


Schritt 6: Inventar erstellen
==============================================================================

Erstellen Sie eine Inventardatei, die die Hosts definiert, auf denen
die Rolle ausgeführt werden soll.


1. **Inventardatei erstellen**:

Erstellen Sie eine Datei namens `inventory.ini` im Hauptverzeichnis Ihres
Ansible-Projekts:

.. code-block:: ini

   [webserver]

   192.168.1.10


Schritt 7: Ansible-Rolle ausführen
==============================================================================

Jetzt können Sie das Playbook ausführen, um die Rolle
auf den definierten Hosts anzuwenden.


1. **Playbook ausführen**:

.. code-block:: bash

   ansible-playbook -i inventory.ini site.yml


Schritt 8: Überprüfen der Ergebnisse
==============================================================================

Überprüfen Sie die Ausgabe des Playbook-Laufs, um sicherzustellen,
dass die Rolle erfolgreich angewendet wurde. Sie können auch
die Konfiguration des Zielhosts überprüfen, um sicherzustellen,
dass die gewünschten Änderungen vorgenommen wurden.


Schritt 9: Fehlerbehebung
==============================================================================

Falls während der Ausführung Fehler auftreten, überprüfen Sie
die Fehlermeldungen in der Konsole. Sie können auch die `-vvv`-Option
verwenden, um detailliertere Ausgaben zu erhalten:

.. code-block:: bash

ansible-playbook -i inventory.ini site.yml -vvv


Fazit
------------------------------------------------------------------------------

Mit diesen Schritten haben Sie eine Ansible-Rolle unter Debian Linux erstellt
und ausgeführt. Ansible ist ein leistungsstarkes Automatisierungstool,
das Ihnen hilft, Konfigurationen und Bereitstellungen effizient und effektiv
zu verwalten. Sie können die Rolle weiter anpassen und zusätzliche Aufgaben
hinzufügen, um Ihre spezifischen Anforderungen zu erfüllen.

Ansible-Struktur und von Ansible-Dateien nach dem Boilerplate-Prinzip
------------------------------------------------------------------------------

Mit dem **bash Skript** ist das Erstellen einer Ansible-Struktur und 
von Ansible-Dateien nach dem Boilerplate-Prinzip möglich. Das Skript erstellt 
die **Ansible Rolle** ``chrony``.

.. literalinclude::
    ../_scripts/create_ansible_structure.sh
   :language: text
   :force:
   :linenos:
   :caption: create_ansible_structure.sh


.. Seealso::

   - :ref:`FAQ - Ansible - Ansible Collections erstellen <Section_FAQs_Ansible_Collections_erstellen>`
   - :ref:`FAQ - Ansible - Ansible Rolle unter Debian nutzen <Section_FAQ_Ansible_Rolle_unter_Debian_nutzen>`
   - :ref:`FAQ - Ansible - Best Practices - Bewährte Verfahren <Section_FAQ_Ansible_Best_Practices>`


.....

.. Rubric:: Footnotes

.. [1]
   Debian Linux
   https://www.debian.org

.. [2]
   Ansible
   https://github.com/ansible/ansible

.. [3]
   Good Practices for Ansible - GPA von Red Hat
   https://redhat-cop.github.io/automation-good-practices/
   https://github.com/redhat-cop/automation-good-practices
