******************************************************************************
FAQ  - rootless Container
******************************************************************************

.. sectionauthor:: Pierre Gronau <Pierre.Gronau@ndaal.eu>

.. contents:: Inhalt - Sicherheit - Container
   :depth: 3

.. admonition:: Motivation
   :class: tip

   Die Verwendung von Containern im Non-Root-Modus ist
   aus mehreren Gründen wichtig:

   - Sicherheit: Non-Root-Container reduzieren das Risiko
     von Privilege-Escalation-Angriffen und begrenzen potenzielle Schäden
     bei Kompromittierung eines Containers.

   - Compliance: Viele Sicherheitsrichtlinien und Best Practices empfehlen
     oder erfordern den Betrieb von Containern ohne Root-Rechte.

   - Prinzip der geringsten Privilegien: Non-Root-Container folgen
     dem Prinzip, nur die minimal notwendigen Berechtigungen zu vergeben.

   - Verbesserter Schutz des Host-Systems: Bei einem Ausbruch
     aus dem Container hat ein Angreifer weniger Möglichkeiten,
     das Host-System zu kompromittieren.

   - Isolierung: Non-Root-Container bieten eine zusätzliche Isolationsebene
     zwischen den Anwendungen und dem Host-System.

   - Best Practices: Die Verwendung von Non-Root-Containern fördert sichere
     Entwicklungs- und Betriebspraktiken in der Container-Umgebung.

   Durch den Einsatz von Containern im Non-Root-Modus wird
   das Sicherheitsniveau der Container-Infrastruktur erhöht
   und das Risiko von Sicherheitsvorfällen minimiert.

rootless Containers mit Podman auf Debian 12.x und Red Hat 9.x
------------------------------------------------------------------------------

Nachstehend wird exemplarisch beschrieben wie eine Installation
von Podman [27]_ unter Debian 12.8.x [1]_ und RedHat 9.x [2]_ erfolgt,
um eine Containerausführung ohne root Berechtigung.

.. figure::
   _static/Podman-logo-orig.png
   :align: center
   :scale: 25 %
   :alt: Optional: Podman für Container

   Optional: Podman für Container


Debian
==============================================================================

**1. Installation von Podman**

Zuerst müssen Sie Podman [27]_ installieren, falls es noch nicht
installiert ist:

bash

``sudo apt-get update``
``sudo apt-get -y install podman``

**2. Erstellen eines Rootless Benutzers**

Stellen Sie sicher, dass Sie einen normalen Benutzer haben, der die Container
ausführen kann. Wenn Sie noch keinen Benutzer haben, können Sie einen
erstellen:

bash

``sudo adduser containeruser``

**3. Konfigurieren von Podman für Rootless**

Podman unterstützt rootless Container standardmäßig. Stellen Sie sicher,
dass Ihr Benutzer die erforderlichen Umgebungsvariablen hat:

bash

``export XDG_RUNTIME_DIR=/run/user/$(id -u)``

Fügen Sie diese Zeile zu Ihrer ~/.bashrc oder ~/.bash_profile hinzu,
um sie bei jeder Anmeldung zu setzen.

**4. Ausführen eines Apache-Containers**

Jetzt können Sie einen Apache-Container als normaler Benutzer ausführen:

bash

``podman run --rm -d -p 8080:80 httpd``

Sie können dann auf den Apache-Server zugreifen, indem Sie
<http://localhost:8080> in Ihrem Webbrowser aufrufen.

**5. Ausführen eines "Hello World"-Containers**

Um einen einfachen "Hello World"-Container auszuführen, verwenden Sie
das folgende Kommando:

bash

``podman run --rm hello-world``

Dieser Container gibt eine einfache Nachricht aus, die bestätigt,
dass der Container erfolgreich ausgeführt wurde.

Red Hat Linux 9
==============================================================================

**1. Installation von Podman**

Zuerst müssen Sie Podman [27]_ installieren,
falls es noch nicht installiert ist:

bash

``sudo dnf install -y podman``

**2. Erstellen eines Rootless Benutzers**

Stellen Sie sicher, dass Sie einen normalen Benutzer haben, der die Container ausführen kann. Wenn Sie noch keinen Benutzer haben, können Sie einen erstellen:

bash

``sudo adduser containeruser``

**3. Konfigurieren von Podman für Rootless**

Podman unterstützt rootless Container standardmäßig. Stellen Sie sicher, dass Ihr Benutzer die erforderlichen Umgebungsvariablen hat:

bash

``export XDG_RUNTIME_DIR=/run/user/$(id -u)``

Fügen Sie diese Zeile zu Ihrer ~/.bashrc oder ~/.bash_profile hinzu, um sie bei jeder Anmeldung zu setzen.

**4. Ausführen eines Apache-Containers**

Jetzt können Sie einen Apache-Container als normaler Benutzer ausführen:

bash

``podman run --rm -d -p 8080:80 httpd``

Sie können dann auf den Apache-Server zugreifen, indem Sie <http://localhost:8080> in Ihrem Webbrowser aufrufen.

**5. Ausführen eines "Hello World"-Containers**

Um einen einfachen "Hello World"-Container auszuführen, verwenden Sie das folgende Kommando:

bash

``podman run --rm hello-world``

Dieser Container gibt eine einfache Nachricht aus, die bestätigt,
dass der Container erfolgreich ausgeführt wurde.

Fazit
==============================================================================

Die Implementierung von rootless Containern mit Podman auf Debian 12.x und
Red Hat Linux 9.x und seinen Derivaten ist einfach und bietet
eine sichere Möglichkeit, Container zu verwalten. Durch die Ausführung
von Containern ohne Root-Rechte können Sie die Sicherheit Ihres Systems
erhöhen und potenzielle Risiken minimieren.

Mit den oben genannten Schritten können Sie sowohl einen Apache-Server
als auch einen einfachen "Hello World"-Container ausführen.

Reference - Links
------------------------------------------------------------------------------

- Oracle Linux 9 hardening [3]_

Deployment Sicht
------------------------------------------------------------------------------

Hier wird angeführt, wie das alles ausgerollt wird (Deployment Sicht).

.. seealso:: Deployment Sicht

   Diese Einstellungen werden mittels eines bash Skriptes aufgebracht.

   .. figure::
      _static/bash-logo-by-vd.png
      :align: center
      :scale: 25 %
      :alt: bash Skript

      bash Skript

   
   <Platzhalter für den Link zum Repo>

   Diese Einstellungen werden mittels einer Ansible Rolle umgesetzt.

   .. figure::
      _static/Ansible_logo-700x700.png
      :align: center
      :scale: 25 %
      :alt: Ansible Rolle

      Ansible Rolle

   
   <Platzhalter für den Link zum Repo>


.....

.. Rubric:: Footnotes

.. [1]
   Debian Linux
   https://www.debian.org

.. [2]
   Red Hat Linux
   https://redhatofficial.github.io/

.. [3]
   Oracle Linux 9 hardening
   https://docs.oracle.com/en/operating-systems/oracle-linux/9/security/OL9-HARDENING.pdf

.. [27]
   Podman
   https://github.com/containers/podman