******************************************************************************
CIS Controls - CIS Kontrollen - Einführung
******************************************************************************

.. sectionauthor:: Pierre Gronau <Pierre.Gronau@ndaal.eu>

..

.. contents:: Inhalt - CIS Kontrollen - Einführung
    :depth: 3

.. _Section_CIS_Controls_Einführung:

Einführung
------------------------------------------------------------------------------

In einer zunehmend digitalisierten Welt, in der Unternehmen auf IT-Infrastrukturen und Netzwerke angewiesen sind, ist Cybersicherheit von unverzichtbarer Bedeutung. Systeme zu schützen und ihre Verwundbarkeit gegenüber Bedrohungen und Cyberangriffen zu verringern, ist eine Herausforderung, der sich jede Organisation stellen muss. Dieses Buch, 'Einführung in Systemhärtung auf Basis CIS v8.1', widmet sich dieser essentiellen Aufgabe, indem es einen umfassenden Leitfaden zur Systemhärtung auf Grundlage der neuesten Standards und Kontrollen des Center for Internet Security (CIS) bietet.

Das Hauptziel der Systemhärtung besteht darin, den Schutz von IT-Systemen zu verbessern, indem unnötige Schwachstellen eliminiert werden. Dies erfolgt durch die Implementierung bewährter Sicherheitsverfahren und -richtlinien, die im CIS v8.1 Framework zusammengefasst sind. Diese Einführung bietet einen Überblick über die wesentlichen Überlegungen zur Systemhärtung und erklärt, wie die CIS-Kontrollen angewendet werden, um die IT-Sicherheit zu erhöhen.

Zu Beginn dieses Buchs wird auf die Bedeutung der Verwaltung von Unternehmensressourcen und der Notwendigkeit einer genauen Bestandsaufnahme eingegangen, um die Kontrolle über alle verbundenen Geräte zu behalten. Darüber hinaus wird die Bedeutung einer strikten Kontrolle von Software-Assets beleuchtet, um sicherzustellen, dass nur autorisierte Software innerhalb der Unternehmensumgebung läuft. Die Verwaltung von sensiblen Daten spielt ebenfalls eine zentrale Rolle in diesem Security-Framework, einschließlich der Datenverschlüsselung sowohl im Ruhezustand als auch während der Übertragung.

Ein weiterer wesentlicher Aspekt der Systemhärtung ist die sichere Konfiguration von Unternehmensressourcen und -software, um Sicherheitslücken zu schließen, bevor sie ausgenutzt werden können. Die kontinuierliche Überwachung und Bewertung von Netzwerken und Systemen ist ebenso entscheidend für die Aufrechterhaltung einer robusten Sicherheitslage. In diesem Kontext wird das Management von Benutzerkonten sowie die Kontrolle von Zugriffsrechten behandelt, um sicherzustellen, dass nur befugte Personen Zugriff auf sensible Daten und Systeme haben.

Während Sie das Buch durchlesen, werden Sie feststellen, dass es nicht nur um die technischen Maßnahmen geht. Es wird ebenso auf die Bedeutung der Schulung von Mitarbeitern hingewiesen, um ein allgemeines Bewusstsein und Verständnis für Cybersicherheitsrisiken zu schaffen. Ein fundiertes Verständnis von Sicherheitsprotokollen, eine engagierte Sicherheitskultur im Unternehmen und die Implementierung eines Plans für die Reaktion auf Zwischenfälle sind wesentliche Bestandteile einer umfassenden Cybersicherheitsstrategie.

Wir hoffen, dass diese Einführung Ihnen den Weg ebnet, tiefer in die Prinzipien und Praktiken der Systemhärtung einzutauchen und dass Sie durch die Anwendung der in diesem Buch behandelten Techniken den Schutz Ihrer IT-Systeme signifikant verbessern können.


.. toctree::
   :maxdepth: 1
   :glob:
   :caption: CIS Kontrollen - Weiterführende Informationen

   ../_CIS/Einführung_in_die_Systemhärtung.rst
   ../_CIS/Verständnis_der_CIS-Kontrollen.rst
   ../_CIS/Verwaltung_von_Unternehmensressourcen.rst
   ../_CIS/Kontrolle_von_Softwareassets.rst
   ../_CIS/Sch4erutz_sensitiver_Daten.rst
   ../_CIS/Sichere_Konfiguration_von_Unternehmensressourcen.rst
   ../_CIS/Benutzerkontenverwaltung.rst
   ../_CIS/Verwaltung_des_Zugriffsrechts.rst
   ../_CIS/Kontinuierliches_Schwachstellenmanagement.rst
   ../_CIS/Verwaltung_von_Audit-Logs.rst
   ../_CIS/Sicherheitsmaßnahmen_für_E-Mail_und_Web-Browser.rst
   ../_CIS/Abwehrmaßnahmen_gegen_Schadsoftware.rst
   ../_CIS/Strategien_zur_Datenwiederherstellung.rst
   ../_CIS/Überwachung_von_Netz-Infrastrukturen.rst
   ../_CIS/Netzüberwachung_und_-verteidigung.rst
   ../_CIS/Sicherheit_von_Anwendungsoftware.rst
   ../_CIS/Verwaltung_von_Diensteanbietern.rst
   ../_CIS/Vorfallreaktionsmanagement.rst
   ../_CIS/Durchführung_von_Penetrationstests.rst
   ../_CIS/Verzeichnisdienst-Management.rst
   ../_CIS/Datenintegrität_und_-verfügbarkeit.rst
   ../_CIS/Sicherheitsprotokolle_und_Standards.rst
   ../_CIS/Sicherheitsstrategie_und_-planung.rst
   ../_CIS/Zukunft_der_Cybersicherheit.rst