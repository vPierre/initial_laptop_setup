******************************************************************************
CIS Controls - CIS Kontrollen - Verwaltung von Diensteanbietern
******************************************************************************

.. sectionauthor:: Pierre Gronau <Pierre.Gronau@ndaal.eu>

..

.. contents:: Inhalt - CIS Kontrollen - Verwaltung von Diensteanbietern
    :depth: 3

.. _Section_CIS_Controls_Verwaltung von Diensteanbietern:

Verwaltung von Diensteanbietern
------------------------------------------------------------------------------

Dieses Kapitel behandelt einen zunehmend wichtigen Aspekt der Cybersicherheit: die Verwaltung von Dienstleistern, oft auch als "Third-Party Risk Management" (TPRM) bezeichnet. CIS Control 15, "Service Provider Management", fordert die Entwicklung eines Prozesses zur Bewertung von Dienstleistern, die sensible Daten speichern oder für kritische IT-Plattformen oder -Prozesse eines Unternehmens verantwortlich sind. Das Ziel ist, sicherzustellen, dass diese Anbieter die Plattformen und Daten *angemessen schützen*. Es geht *nicht* nur darum, *Verträge* mit Dienstleistern abzuschließen, sondern auch darum, die *Sicherheit* der Dienstleister *kontinuierlich* zu *überwachen* und zu *bewerten*. Die Auslagerung von IT-Diensten an Dritte, sei es in Form von Cloud-Diensten, SaaS-Anwendungen (Software as a Service), Managed Services oder anderen Formen des Outsourcings, ist *weit verbreitet*. Sie bietet *viele Vorteile*, beispielsweise *Kosteneinsparungen*, *Skalierbarkeit*, *Flexibilität* und *Zugang zu spezialisiertem Know-how*. Sie birgt aber auch *erhebliche Sicherheitsrisiken*.

Warum ist die Verwaltung von Diensteanbietern so wichtig? Wenn ein Unternehmen *sensible Daten* an einen Dienstleister *weitergibt* oder *kritische IT-Systeme* an einen Dienstleister *auslagert*, *vertraut* es darauf, dass der Dienstleister die Daten und Systeme *angemessen schützt*. Wenn der Dienstleister jedoch *Sicherheitsmängel* aufweist, kann dies zu *Sicherheitsvorfällen* führen, die das Unternehmen *direkt* betreffen. Ein *Datenleck* beim Dienstleister kann zu einem *Datenverlust* für das Unternehmen führen. Ein *Ausfall* beim Dienstleister kann zu einer *Betriebsunterbrechung* für das Unternehmen führen. Ein *Sicherheitsvorfall* beim Dienstleister kann den *Ruf* des Unternehmens *schädigen*.

Stellen Sie sich vor, ein Unternehmen nutzt einen *Cloud-Speicherdienst*, um *Kundendaten* zu speichern. Wenn der Cloud-Anbieter *gehackt* wird und die Kundendaten *gestohlen* werden, ist das Unternehmen *haftbar*, auch wenn der Sicherheitsvorfall *nicht* direkt vom Unternehmen verursacht wurde. Oder stellen Sie sich vor, ein Unternehmen lagert seine *E-Mail-Server* an einen Dienstleister aus. Wenn der Dienstleister *Sicherheitsupdates* *nicht rechtzeitig* installiert und die E-Mail-Server *kompromittiert* werden, kann dies zu *Phishing-Angriffen* auf die Mitarbeiter des Unternehmens führen oder den *gesamten E-Mail-Verkehr* lahmlegen.

Die Verwaltung von Diensteanbietern ist *nicht nur* ein *technisches Problem*, sondern auch ein *organisatorisches*, *prozessuales* und *rechtliches* Problem. Es reicht *nicht* aus, *technische Sicherheitsmaßnahmen* vom Dienstleister zu *fordern*. Es ist auch *notwendig*, *klare Verträge* abzuschließen, die *Sicherheitsanforderungen* festzulegen, *Verantwortlichkeiten* zu definieren und *Rechte* und *Pflichten* zu regeln. Es ist auch *notwendig*, die *Einhaltung* der Sicherheitsanforderungen durch den Dienstleister *kontinuierlich* zu *überwachen* und zu *bewerten*.

Control 15 umfasst sieben Safeguards, die verschiedene Aspekte der Dienstleisterverwaltung abdecken. Der erste Safeguard, 15.1, fordert die "Establish and Maintain an Inventory of Service Providers" (Erstellung und Pflege eines Inventars der Dienstleister). Das Inventar soll alle bekannten Dienstleister auflisten, Klassifizierungen enthalten und für jeden Dienstleister einen Unternehmenskontakt benennen. Überprüfen und aktualisieren Sie das Inventar jährlich oder wenn signifikante Unternehmensänderungen eintreten, die sich auf diesen Safeguard auswirken könnten. Ein *vollständiges* und *aktuelles* Inventar *aller* Dienstleister, die *sensible Daten* verarbeiten oder *Zugriff* auf *kritische IT-Systeme* haben, ist die *Grundlage* für eine *effektive* Dienstleisterverwaltung.

Das Inventar sollte mindestens die folgenden Informationen enthalten:

*   **Name** des Dienstleisters
*   **Art** des Dienstes (z.B. Cloud-Speicher, SaaS, Managed Security Services)
*   **Beschreibung** des Dienstes und der *verarbeiteten Daten* oder *Systeme*
*   **Klassifizierung** des Dienstleisters (siehe Safeguard 15.3)
*   **Ansprechpartner** im Unternehmen für den Dienstleister
*   **Ansprechpartner** beim Dienstleister
*   **Vertragsbeginn** und **-ende**
*   **Relevante Vertragsdetails** (z.B. Service Level Agreements (SLAs), Sicherheitsanforderungen)

Dieses Inventar sollte *regelmäßig überprüft* und *aktualisiert* werden, um sicherzustellen, dass es den *aktuellen Stand* der Dienstleisterbeziehungen widerspiegelt. Es sollte auch *automatisiert* werden, beispielsweise durch die *Integration* mit dem *Beschaffungsprozess* oder durch den Einsatz von *speziellen TPRM-Tools*.

Safeguard 15.2, "Establish and Maintain a Service Provider Management Policy" (Erstellung und Pflege einer Richtlinie zur Dienstleisterverwaltung), fordert die Erstellung und Pflege einer Richtlinie zur Dienstleisterverwaltung. Stellen Sie sicher, dass die Richtlinie die Klassifizierung, Inventarisierung, Bewertung, Überwachung und Außerbetriebnahme von Dienstleistern behandelt. Überprüfen und aktualisieren Sie die Richtlinie jährlich oder wenn signifikante Unternehmensänderungen eintreten, die sich auf diesen Safeguard auswirken könnten. Eine *klare* und *umfassende* Richtlinie zur Dienstleisterverwaltung legt die *Regeln* und *Verantwortlichkeiten* für den *gesamten Lebenszyklus* der Dienstleisterbeziehung fest, von der *Auswahl* und *Vertragsgestaltung* über die *laufende Überwachung* bis hin zur *Beendigung* der Zusammenarbeit.

Die Richtlinie sollte die folgenden Aspekte umfassen:

*   **Ziele** der Dienstleisterverwaltung (z.B. Schutz sensibler Daten, Sicherstellung der Geschäftskontinuität, Einhaltung gesetzlicher und regulatorischer Anforderungen)
*   **Geltungsbereich** der Richtlinie (d.h. *welche* Arten von Dienstleistern und Diensten sind *betroffen*)
*   **Klassifizierung** von Dienstleistern (siehe Safeguard 15.3)
*   **Anforderungen** an die *Auswahl* von Dienstleistern (z.B. Due-Diligence-Prüfungen, Sicherheitsbewertungen)
*   **Anforderungen** an die *Vertragsgestaltung* mit Dienstleistern (z.B. Festlegung von Sicherheitsanforderungen, SLAs, Haftungsregelungen)
*   **Anforderungen** an die *laufende Überwachung* der Dienstleister (z.B. regelmäßige Sicherheitsüberprüfungen, Überwachung der Einhaltung von SLAs)
*   **Anforderungen** an die *Beendigung* der Zusammenarbeit mit Dienstleistern (z.B. sichere Rückgabe oder Löschung von Daten, Deaktivierung von Zugängen)
*   **Verantwortlichkeiten** für die *Umsetzung* und *Einhaltung* der Richtlinie

Diese Richtlinie sollte *dokumentiert* und allen *relevanten Mitarbeitern* und *Abteilungen* *kommuniziert* werden. Sie sollte auch *regelmäßig überprüft* und *aktualisiert* werden, um sicherzustellen, dass sie den *aktuellen Anforderungen* und *Bedrohungen* entspricht.

Safeguard 15.3, "Classify Service Providers" (Klassifizierung von Dienstleistern), fordert die Klassifizierung von Dienstleistern. Zu den Klassifizierungsüberlegungen können ein oder mehrere Merkmale gehören, wie z. B. Datensensitivität, Datenvolumen, Verfügbarkeitsanforderungen, geltende Vorschriften, inhärentes Risiko und gemindertes Risiko. Aktualisieren und überprüfen Sie Klassifizierungen jährlich oder wenn signifikante Unternehmensänderungen eintreten, die sich auf diesen Safeguard auswirken könnten. Nicht alle Dienstleister sind *gleich kritisch*. Einige Dienstleister verarbeiten *sensiblere Daten* oder haben *Zugriff* auf *kritischere Systeme* als andere. Eine *Klassifizierung* der Dienstleister nach ihrer *Kritikalität* ermöglicht es, die *Sicherheitsmaßnahmen* und die *Überwachung* *risikobasiert* zu *priorisieren*.

Die Klassifizierung könnte beispielsweise die folgenden Kategorien umfassen:

*   **Kritisch:** Dienstleister, die *sensible Daten* verarbeiten (z.B. personenbezogene Daten, Finanzdaten, Gesundheitsdaten) oder *Zugriff* auf *geschäftskritische Systeme* haben (z.B. Produktionssysteme, Finanzsysteme, Systeme, die den Betrieb des Unternehmens steuern).
*   **Hoch:** Dienstleister, die *weniger sensible Daten* verarbeiten oder *Zugriff* auf *weniger kritische Systeme* haben, aber dennoch ein *erhebliches Risiko* darstellen, wenn sie *kompromittiert* werden.
*   **Mittel:** Dienstleister, die *nicht sensible Daten* verarbeiten oder *Zugriff* auf *Systeme* haben, die *nicht geschäftskritisch* sind, aber dennoch ein *gewisses Risiko* darstellen.
*   **Niedrig:** Dienstleister, die *keine sensiblen Daten* verarbeiten und *keinen Zugriff* auf *kritische Systeme* haben und nur ein *minimales Risiko* darstellen.

Die Klassifizierung sollte auf einer *Risikobewertung* basieren, die die *Art* der verarbeiteten Daten, die *Art* der bereitgestellten Dienste, die *potenziellen Auswirkungen* eines Sicherheitsvorfalls und die *vorhandenen Sicherheitsmaßnahmen* des Dienstleisters berücksichtigt.

Safeguard 15.4, "Ensure Service Provider Contracts Include Security Requirements" (Sicherstellen, dass Dienstleisterverträge Sicherheitsanforderungen enthalten), fordert, dass Dienstleisterverträge Sicherheitsanforderungen enthalten. Beispielhafte Anforderungen können Mindestanforderungen an das Sicherheitsprogramm, Benachrichtigung und Reaktion bei Sicherheitsvorfällen und/oder Datenschutzverletzungen, Datenverschlüsselungsanforderungen und Datenentsorgungsverpflichtungen sein. Diese Sicherheitsanforderungen müssen mit der Dienstleister-Verwaltungsrichtlinie des Unternehmens übereinstimmen. Überprüfen Sie Dienstleisterverträge jährlich, um sicherzustellen, dass Verträge keine Sicherheitsanforderungen vermissen lassen. Der *Vertrag* mit dem Dienstleister ist ein *zentrales Instrument*, um die *Sicherheitsanforderungen* *rechtlich verbindlich* festzulegen und die *Verantwortlichkeiten* und *Haftungsfragen* zu regeln.

Der Vertrag sollte *klare* und *detaillierte* Sicherheitsanforderungen enthalten, die auf die *Art* des Dienstes, die *verarbeiteten Daten* und die *Klassifizierung* des Dienstleisters (siehe Safeguard 15.3) *abgestimmt* sind. Zu den typischen Sicherheitsanforderungen gehören:

*   **Mindeststandards** für das *Sicherheitsprogramm* des Dienstleisters (z.B. Implementierung eines Informationssicherheits-Managementsystems (ISMS) nach ISO 27001, Einhaltung der CIS Controls)
*   **Verpflichtung** zur *Einhaltung* aller *relevanten Gesetze* und *Vorschriften* (z.B. DSGVO)
*   **Anforderungen** an die *Datenverschlüsselung* (sowohl *während der Übertragung* als auch *im Ruhezustand*)
*   **Anforderungen** an die *Zugriffskontrolle* (z.B. Multi-Faktor-Authentifizierung, Prinzip der geringsten Rechte)
*   **Anforderungen** an die *Netzesicherheit* (z.B. Firewalls, Intrusion Detection Systems)
*   **Anforderungen** an die *physische Sicherheit* (z.B. Zutrittskontrollen, Videoüberwachung)
*   **Anforderungen** an die *Sicherheitsüberwachung* und *Protokollierung*
*   **Verpflichtung** zur *regelmäßigen Durchführung* von *Sicherheitsüberprüfungen* und *Penetrationstests*
*   **Verpflichtung** zur *unverzüglichen Meldung* von *Sicherheitsvorfällen* und *Datenlecks* an das Unternehmen
*   **Verpflichtung** zur *Zusammenarbeit* mit dem Unternehmen bei der *Untersuchung* und *Behebung* von Sicherheitsvorfällen
*   **Anforderungen** an die *sichere Löschung* oder *Rückgabe* von Daten bei *Beendigung* des Vertrags
*   **Regelungen** zur *Haftung* bei Sicherheitsvorfällen

Der Vertrag sollte auch *Regelungen* für die *Überprüfung* der *Einhaltung* der Sicherheitsanforderungen durch den Dienstleister enthalten, beispielsweise das *Recht* des Unternehmens, *Audits* durchzuführen oder *Sicherheitsberichte* anzufordern.

Safeguard 15.5, "Assess Service Providers" (Bewertung von Dienstleistern), fordert die Bewertung von Dienstleistern in Übereinstimmung mit der Dienstleister-Verwaltungsrichtlinie des Unternehmens. Der Bewertungsumfang kann je nach Klassifizierung(en) variieren und die Überprüfung standardisierter Bewertungsberichte wie Service Organization Control 2 (SOC 2) und Payment Card Industry (PCI) Attestation of Compliance (AoC), angepasste Fragebögen oder andere entsprechend strenge Prozesse umfassen. Dienstleister sind mindestens jährlich oder bei neuen und erneuerten Verträgen neu zu bewerten. Die *Auswahl* des *richtigen* Dienstleisters ist *entscheidend*, um das *Risiko* von Sicherheitsvorfällen zu *minimieren*. Vor der *Vergabe* eines Auftrags an einen Dienstleister sollte eine *sorgfältige Bewertung* der *Sicherheit* des Dienstleisters durchgeführt werden.

Die Bewertung kann *verschiedene Methoden* umfassen:

*   **Anforderung** und *Überprüfung* von *Sicherheitsdokumentationen* des Dienstleisters, beispielsweise *Sicherheitsrichtlinien*, *Verfahrensbeschreibungen*, *Zertifizierungen* (z.B. ISO 27001, SOC 2) und *Penetrationstestberichte*.
*   **Ausfüllen** von *Sicherheitsfragebögen*, die vom Unternehmen *vorgegeben* oder von *Branchenverbänden* oder *Standardisierungsorganisationen* entwickelt wurden (z.B. Shared Assessments Standardized Information Gathering (SIG) Questionnaire).
*   **Durchführung** von *Vor-Ort-Besuchen* oder *virtuellen Audits*, um die *Sicherheitsmaßnahmen* des Dienstleisters *direkt* zu *überprüfen*.
*   **Einholung** von *Referenzen* von *anderen Kunden* des Dienstleisters.
* Durchführung eigener Penetrationstests und Schwachstellenscans.

Die Bewertung sollte *nicht nur* vor der *Vergabe* des Auftrags erfolgen, sondern auch *regelmäßig* während der *gesamten Laufzeit* des Vertrags, beispielsweise *jährlich* oder bei *wesentlichen Änderungen* an den Diensten oder der Sicherheitslage des Dienstleisters.

Safeguard 15.6, "Monitor Service Providers" (Überwachung von Dienstleistern), fordert die Überwachung von Dienstleistern in Übereinstimmung mit der Dienstleister-Verwaltungsrichtlinie des Unternehmens. Die Überwachung kann die periodische Neubewertung der Dienstleister-Compliance, die Überwachung von Dienstleister-Release-Notes und die Dark-Web-Überwachung umfassen. Die *laufende Überwachung* der Dienstleister ist *entscheidend*, um sicherzustellen, dass sie die *vereinbarten Sicherheitsanforderungen* *dauerhaft einhalten* und auf *neue Bedrohungen* und *Schwachstellen* *reagieren*.

Die Überwachung kann *verschiedene Methoden* umfassen:

*   **Regelmäßige Überprüfung** der *Sicherheitsdokumentation* des Dienstleisters, beispielsweise *aktualisierte Sicherheitsrichtlinien*, *neue Zertifizierungen* und *aktuelle Penetrationstestberichte*.
*   **Überwachung** der *Einhaltung* von *Service Level Agreements (SLAs)*, die *sicherheitsrelevante Kennzahlen* enthalten, beispielsweise die *Verfügbarkeit* von Diensten, die *Reaktionszeit* bei Sicherheitsvorfällen und die *Zeit* bis zur *Behebung* von Schwachstellen.
*   **Überwachung** von *Sicherheitswarnungen* und *-meldungen* des Dienstleisters, beispielsweise *Informationen* über *neue Schwachstellen* in den verwendeten Systemen oder *aktuelle Angriffe*.
*   **Überwachung** von *öffentlichen Quellen* und *Dark-Web-Foren* auf *Informationen* über *Sicherheitsprobleme* oder *Datenlecks* beim Dienstleister.
* Durchführung regelmäßiger Sicherheitsaudits und Penetrationstests.

Safeguard 15.7, "Securely Decommission Service Providers" (Sichere Außerbetriebnahme von Dienstleistern), fordert die sichere Außerbetriebnahme von Dienstleistern. Beispielhafte Überlegungen umfassen die Deaktivierung von Benutzer- und Dienstkonten, die Beendigung von Datenflüssen und die sichere Entsorgung von Unternehmensdaten innerhalb der Systeme des Dienstleisters. Die *Beendigung* der Zusammenarbeit mit einem Dienstleister erfordert *ebenso sorgfältige Planung* und *Durchführung* wie die *Auswahl* und *laufende Überwachung*. Es muss sichergestellt werden, dass *alle Daten* des Unternehmens *sicher* vom Dienstleister *entfernt* oder *zurückgegeben* werden, dass *alle Zugänge* des Dienstleisters zu den Systemen des Unternehmens *deaktiviert* werden und dass *keine Sicherheitsrisiken* durch die Beendigung der Zusammenarbeit entstehen.

Die Außerbetriebnahme sollte die folgenden Schritte umfassen:

*   **Deaktivierung** *aller Benutzerkonten* und *Dienstkonten*, die der Dienstleister für den Zugriff auf die Systeme des Unternehmens verwendet hat.
*   **Beendigung** *aller Datenflüsse* zwischen dem Unternehmen und dem Dienstleister, beispielsweise *Deaktivierung* von *VPN-Verbindungen*, *API-Schlüsseln* und *Dateifreigaben*.
*   **Sichere Löschung** oder *Rückgabe* *aller Daten* des Unternehmens, die beim Dienstleister *gespeichert* oder *verarbeitet* wurden. Dies sollte *gemäß* den *vertraglichen Vereinbarungen* und den *geltenden Gesetzen* und *Vorschriften* erfolgen.
*   **Überprüfung**, ob *alle Schritte* der Außerbetriebnahme *erfolgreich durchgeführt* wurden und *keine Sicherheitsrisiken* mehr bestehen.
* **Dokumentation** des gesamten Außerbetriebnahme-Prozesses.

Die Verwaltung von Dienstleistern ist *keine einmalige Aufgabe*, sondern ein *fortlaufender Prozess*. Neue Dienstleister werden *hinzugefügt*, bestehende Dienstleisterbeziehungen werden *geändert* oder *beendet*, und neue *Bedrohungen* und *Risiken* tauchen auf. Daher muss der Prozess der Dienstleisterverwaltung *regelmäßig überprüft* und *angepasst* werden. Das *Inventar* der Dienstleister muss *aktualisiert* werden, die *Klassifizierung* der Dienstleister muss *überprüft* werden, die *Verträge* müssen *angepasst* werden, und die *Sicherheitsbewertungen* und *Überwachungsmaßnahmen* müssen *kontinuierlich durchgeführt* werden.

Die Verwaltung von Dienstleistern erfordert die *Zusammenarbeit* verschiedener Abteilungen und Rollen in der Organisation. Die *IT-Abteilung* ist für die *technische Bewertung* und *Überwachung* der Dienstleister verantwortlich, beispielsweise für die *Überprüfung* der *Sicherheitsarchitektur*, die *Durchführung* von *Penetrationstests* und die *Analyse* von *Protokolldateien*. Die *Sicherheitsabteilung* ist für die *Definition der Sicherheitsanforderungen*, die *Überwachung der Einhaltung* und die *Bewertung* der *Risiken* verantwortlich. Die *Rechtsabteilung* ist für die *Gestaltung* der *Verträge* und die *Klärung* von *Haftungsfragen* verantwortlich. Die *Fachabteilungen*, die die Dienste der Dienstleister *nutzen*, müssen in den Prozess *eingebunden* werden, beispielsweise bei der *Auswahl* der Dienstleister, der *Definition* der *Anforderungen* und der *Bewertung* der *Auswirkungen* von Sicherheitsvorfällen.

Die Verwaltung von Dienstleistern ist ein *wesentlicher Bestandteil* einer *umfassenden Cybersicherheitsstrategie*. Sie schützt das Unternehmen vor den *Risiken*, die mit der *Auslagerung* von IT-Diensten und der *Weitergabe* von Daten an *Dritte* verbunden sind. Die konsequente Anwendung der hier beschriebenen Prinzipien und Maßnahmen ist ein entscheidender Schritt zur Verbesserung der Cybersicherheit einer Organisation. Ein proaktiver und umfassender Ansatz zur Verwaltung von Dienstleistern minimiert das Risiko von Sicherheitsvorfällen und trägt dazu bei, die Integrität, Vertraulichkeit und Verfügbarkeit von IT-Systemen und Daten zu gewährleisten. Durch die systematische Bewertung, Überwachung und Steuerung der Sicherheitsaspekte in den Beziehungen zu Drittanbietern wird ein entscheidender Beitrag zur Gesamtsicherheit des Unternehmens geleistet.
