#!/usr/bin/env bash
# run it with 
# /bin/bash -c "$(curl -fsSL https://gist.githubusercontent.com/gumlooter/98d84a9ca3e349fa4b5000774a41b5fd/raw/_macos_setup.sh\?$(date +%s))"

echo "Make sure you are logged in into AppStore already"
read -p "Press enter to continue"
echo "macOS setup in progress!"

xcode-select --install
sudo softwareupdate --install-rosetta

/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
eval "$(/opt/homebrew/bin/brew shellenv)"

brew update
brew tap homebrew/cask-fonts
brew tap homebrew/autoupdate

PACKAGES=(
    git
    python@3
    go
    node
    htop
    awscli
    bluetoothconnector
    postgresql
    mysql
    sqlite
    wget
    jq
    youtube-dl
    tmux
    font-meslo-lg-nerd-font
    font-fira-code
    mas
    tree
    thefuck
    tldr
    ssh-copy-id
    cocoapods
)
brew install ${PACKAGES[@]}

APPS=(
    iterm2
    slack
    spotify
    docker
    zoom
    dbeaver-community
    alfred
    discord
    steam
    intellij-idea
    sublime-text
    notion
    whatsapp
    charles
    postman
    ngrok
    bartender
    miro
    whatsapp
    the-unarchiver
    google-chrome
    brave-browser
    firefox
    yandex-disk
    nvidia-geforce-now
    kite
    hey
    scroll-reverser
    shadow
    anaconda
    nightowl
    blender
    epic-games
)
brew install --cask ${APPS[@]}

brew cleanup
mkdir -p ~/Library/LaunchAgents
brew autoupdate --start --upgrade --cleanup --enable-notification


APPSTORE=(
    497799835   # Xcode
    441258766   # Magnet
    1191449274  # ToothFairy
    1365531024  # 1Blocker
    937984704   # Amphetamine
    1176895641  # Spark
    1462114288  # Grammarly for Safari
    1495356253  # Tab Suspender
    970502923   # Typeeto
    360593530   # Notability
)
mas install ${APPSTORE[@]}

sudo xcodebuild -license accept


sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k
echo "ZSH_THEME=\"powerlevel10k/powerlevel10k\"" >> ~/.zshrc

sed -i 's/^ZSH_THEME=.*/ZSH_THEME=powerlevel10k\/powerlevel10k/' ~/.zshrc
echo "eval \$(thefuck --alias)" >> ~/.zshrc
eval $(thefuck --alias)
sed -i '.bak' '1s/^/ZSH_DISABLE_COMPFIX=true\'$'\n/g' ~/.zshrc


defaults write com.apple.screensaver askForPassword -int 1
defaults write com.apple.screensaver askForPasswordDelay -int 0
defaults write NSGlobalDomain AppleShowAllExtensions -bool true
defaults write com.apple.driver.AppleBluetoothMultitouch.trackpad Clicking -bool true
defaults -currentHost write NSGlobalDomain com.apple.mouse.tapBehavior -int 1
defaults write com.apple.Finder AppleShowAllFiles true
killall Finder


p10k configure

git config --global user.name "Pierre Gronau"
git config --global user.email pierre.gronau@ndaal.eu


curl https://gist.githubusercontent.com/gumlooter/98d84a9ca3e349fa4b5000774a41b5fd/raw/import_env_vars.sh --output ~/.import_env_vars.sh
chmod +x ~/.import_env_vars.sh
echo "alias import_env=\"source ~/.import_env_vars.sh\"" >> ~/.zshrc
echo 'eval "$(/opt/homebrew/bin/brew shellenv)"' >> ~/.zshrc

echo "Enter your 1password email: "
read user_email
op signin my.1password.com $user_email                          

echo "Done, please restart terminal session"
