#!/usr/bin/env bash
# Author: Pierre Gronau <Pierre.Gronau@ndaal.eu>
# Copyright 2024
# License: All content is licensed under the terms of the <Apache 2.0>
# Developed on: Debian 12.8x; macOS Sequoia x86 architecture
# Tested on: Debian 12.8x; macOS Sequoia x86 architecture
#
set -o errexit
set -o errtrace
set -o nounset
set -o pipefail
# set -o xtrace
# nosemgrep: ifs-tampering
IFS=$'\n\t'

# Error trapping function
error_handler() {
    local line_number="${1}"
    local error_code="${2}"
    local last_command="${BASH_COMMAND}"
    printf "Error: Error occurred in line %s (error code: %s)\n" "${line_number}" "${error_code}"
    printf "Error: Failed command: %s\n" "${last_command}"
}

# Set the error trap
trap 'error_handler ${LINENO} $?' ERR

cleanup() {
    trap - SIGINT SIGTERM ERR EXIT
    printf "%b\n" "\nInfo: Cleanup is running ..."
    # Additional cleanup tasks can be added here if needed
    rm -f -v ./*.tmp
    printf "%b\n" "\nInfo: Cleanup finished ..."
}

trap cleanup SIGINT SIGTERM EXIT

# Function to add configurations to .bashrc
add_to_bashrc() {
    local bashrc_file="${HOME}/.bashrc"
    
    # Check if .bashrc exists
    if [[ ! -f "${bashrc_file}" ]]; then
        printf "Error: .bashrc file does not exist. Creating it now.\n"
        touch "${bashrc_file}"
    fi

    # Add configurations to .bashrc
    {
        printf "# Add RVM to PATH for scripting. Make sure this is the last PATH variable change.\n"
        printf "export PATH=\"\$PATH:\${HOME}/.rvm/bin\"\n"
        printf ". \"\${HOME}/.cargo/env\"\n"
        printf "\n# Custom security configurations\n"
        printf "shopt -s histappend\n"
        printf "PROMPT_COMMAND=\"history -a; history -n\"\n"
        printf "HISTSIZE=10000\n"
        printf "HISTFILESIZE=2000\n"
        printf "chattr +a ~/.bash_history\n"
        printf "set +o emacs\n"
        printf "set +o vi\n"
        printf "export PROMPT_COMMAND='logger -t bash -p user.info \"\$(history 1)\"'\n"
        printf "unset HISTFILE\n"
        printf "unset HISTCONTROL\n"
        printf "umask 077\n"
        #printf "set -o noglob\n"
        printf "export SSH_AUTH_SOCK=/run/user/\$UID/keyring/ssh\n"
        #printf "export PS1=\"\\[\\e[31m\\]\\u@\\h:\\w\\$ \\[\\e[m\\]\"\n"
        printf "set +o history\n"
    } > "${bashrc_file}"

    printf "Info: Security configurations have been added to %s\n" "${bashrc_file}"
}

# Execute the function to add configurations
add_to_bashrc

cleanup

script_name1="$(basename "${0}")"
printf "\nInfo: script_name1: %s\n" "${script_name1}"
script_path1="$(realpath "$(dirname "${0}")")"
printf "Info: script_path1: %s\n" "${script_path1}"
script_path_with_name="${script_path1}/${script_name1}"
printf "Info: Script path with name: %s\n" "${script_path_with_name}"
printf "Info: Script finished\n"
exit 0
