#!/usr/bin/env bash
# Author: Pierre Gronau <Pierre.Gronau@ndaal.eu>
# Copyright 2024, 2025
# License: All content is licensed under the terms of the <Apache 2.0>
# Developed on: Debian 12.x; macOS Sequoia x86 architecture
# Tested on: Debian 12.x; macOS Sequoia x86 architecture
#
set -o errexit
set -o errtrace
set -o nounset
set -o pipefail
# set -o xtrace

# nosemgrep: ifs-tampering
IFS=$'\n\t'

# Error trapping function
# Add this line before the error_handler function
# shellcheck disable=SC2317
error_handler() {
    local line_number="${1}"
    local error_code="${2}"
    local last_command="${BASH_COMMAND}"
    printf "Error: Error occurred in line %s (error code: %s)\n" "${line_number}" "${error_code}"
    printf "Error: Failed command: %s\n" "${last_command}"

    # Success case
    return 0
}

# Set the error trap
trap 'error_handler ${LINENO} $?' ERR

cleanup() {
    trap - SIGINT SIGTERM ERR EXIT
    printf "%b\n" "\nInfo: Cleanup is running ..."
    # Additional cleanup tasks can be added here if needed
    rm -f -v ./*.tmp
    printf "%b\n" "\nInfo: Cleanup finished ..."
}

trap cleanup SIGINT SIGTERM EXIT

# Function to check if a command is available
check_command() {
    if ! command -v "${1}" &>/dev/null; then
        printf "Error: %s is not installed or not in PATH. Please install it and try again.\n" "${1}"
        return 1
    fi

    # Success case
    return 0
}

# Function to display shell version
display_shell_version() {
    local shell_name="${1}"
    local version_option="${2}"

    if check_command "${shell_name}"; then
        printf "Info: Version of %s:\n" "${shell_name}"
        if output="$("${shell_name}" "${version_option}" 2>&1)"; then
            printf "%s\n\n" "${output}" | head -n 1
        else
            printf "Info: Unable to retrieve version information for %s\n\n" "${shell_name}"
        fi
    else
        printf "Warning: %s is not installed or not in PATH.\n\n" "${shell_name}"
    fi

    # Success case
    return 0
}

# Function to clear history for a specific shell
clear_shell_history() {
    local shell_name="${1}"
    local history_file="${2}"

    if check_command "${shell_name}"; then
        printf "Info: Clearing %s history for all users...\n" "${shell_name}"
        for user_home in /home/*; do
            if [ -d "${user_home}" ]; then
                sudo rm -f -v "${user_home}/${history_file}"
                printf "Info: Cleared %s history for user: %s\n" "${shell_name}" "$(basename "${user_home}")"
            fi
        done
        # Also clear for root user
        sudo rm -f -v "/root/${history_file}"
        printf "Info: Cleared %s history for root user\n" "${shell_name}"
    else
        printf "Info: Skipping %s history clearing as it's not installed\n" "${shell_name}"
    fi

    # Success case
    return 0
}

# Display versions and clear history for various shells

# Add bash support
display_shell_version "bash" "--version"
clear_shell_history "bash" ".bash_history"

# Add tcsh support
display_shell_version "tcsh" "--version"
clear_shell_history "tcsh" ".tcsh_history"

# Add fish support
display_shell_version "fish" "--version"
clear_shell_history "fish" ".local/share/fish/fish_history"
clear_shell_history "fish" ".config/fish/fish_history"

# Add ksh support
display_shell_version "ksh" "-c 'echo \$KSH_VERSION'" || true
display_shell_version "ksh" "--version" || true
clear_shell_history "ksh" ".sh_history"

# Add ash support
display_shell_version "ash" "--version"
clear_shell_history "ash" ".ash_history"

# Add crosh support
display_shell_version "crosh" "--version"
clear_shell_history "crosh" ".crosh_history"

# Add csh support
display_shell_version "csh" "--version"
clear_shell_history "csh" ".history"

# Add sh support
display_shell_version "sh" "--version" || true
display_shell_version "sh" "-c 'echo \$BOURNE_VERSION'" || true
clear_shell_history "sh" ".sh_history"

# Add zsh support
display_shell_version "zsh" "--version"
clear_shell_history "zsh" ".zsh_history"

# Add dash support
display_shell_version "dash" "--version"
clear_shell_history "dash" ".dash_history"

# Add nash support
display_shell_version "nash" "--version"
clear_shell_history "nash" ".nash_history"

# Add esh support
display_shell_version "esh" "--version"
clear_shell_history "esh" ".esh_history"

# Add sash support
display_shell_version "sash" "--version"
clear_shell_history "sash" ".sash_history"

# Add Scheme Shell (scsh)
display_shell_version "scsh" "--version"
clear_shell_history "scsh" ".scsh_history"

# Add xonsh support
display_shell_version "xonsh" "--version"
clear_shell_history "xonsh" ".xonsh_history"

# Add elvish support
display_shell_version "elvish" "--version"
clear_shell_history "elvish" ".elvish/history"

# Add Emacs shell (eshell)
# Note: Eshell is part of Emacs, so we check for emacs instead
display_shell_version "emacs" "--version"
clear_shell_history "eshell" ".eshell_history"

# Add Nushell support
display_shell_version "nu" "--version"
clear_shell_history "nu" ".config/nushell/history.txt"

# Add Couchbase Shell support
display_shell_version "cbsh" "--version"
# Adjust the history file path if necessary
clear_shell_history "cbsh" ".cbsh_history"

# Add oksh support
# https://man.freebsd.org/cgi/man.cgi?query=oksh&apropos=0&sektion=1&manpath=freebsd-ports&format=html
display_shell_version "oksh" "--version"
clear_shell_history "oksh" ".sh_history"

# Add pdksh support
# https://linux.die.net/man/1/pdksh
display_shell_version "pdksh" "--version"
clear_shell_history "pdksh" ".sh_history"

# Additional step to clear current bash history
if check_command "bash"; then
    history -c
    printf "Cleared current bash session history\n"
fi

cleanup

script_name1="$(basename "${0}")"
printf "\nInfo: script_name1: %s\n" "${script_name1}"
script_path1="$(realpath "$(dirname "${0}")")"
printf "Info: script_path1: %s\n" "${script_path1}"
script_path_with_name="${script_path1}/${script_name1}"
printf "Info: Script path with name: %s\n" "${script_path_with_name}"
printf "Info: Script finished\n"
exit 0
