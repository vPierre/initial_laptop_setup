#!/usr/bin/env bash

# Author: Pierre Gronau <Pierre.Gronau@ndaal.eu>
# Copyright 2024
# License: All content is licensed under the terms of the <Apache 2.0e>
# Developed on: Debian 12.x; macOS Sequoia x86 architecture
# Tested on: Debian 12.x; macOS Sequoia x86 architecture

set -o errexit
set -o errtrace
set -o nounset
set -o pipefail
# set -o xtrace

# nosemgrep: ifs-tampering
IFS=$'\n\t'

# Error trapping function
# Add this line before the error_handler function
# shellcheck disable=SC2317
error_handler() {
    local line_number="${1}"
    local error_code="${2}"
    local last_command="${BASH_COMMAND}"
    printf "Error: Error occurred in line %s (error code: %s)\n" "${line_number}" "${error_code}"
    printf "Error: Failed command: %s\n" "${last_command}"

    # Success case
    return 0
}

# Set the error trap
trap 'error_handler ${LINENO} $?' ERR

cleanup() {
    trap - SIGINT SIGTERM ERR EXIT
    printf "%b\n" "\nInfo: Cleanup is running ..."
    # Additional cleanup tasks can be added here if needed
    rm -f -v ./*.tmp
    printf "%b\n" "\nInfo: Cleanup finished ..."
}

trap cleanup SIGINT SIGTERM ERR EXIT

VERSION="1.0.0"
readonly VERSION
printf "Info: Current version: %s\n" "${VERSION}"

LOG_FILE="/var/log/vivaldi_cleanup.log"
readonly LOG_FILE
printf "Info: Current version: %s\n" "${LOG_FILE}"

log() {
    printf "%s - %s\n" "$(date '+%Y-%m-%d %H:%M:%S')" "${1}" | tee -a "${LOG_FILE}"
}

check_root() {
    if [ "${EUID}" -ne 0 ]; then
        script_path=$([[ "${0}" = /* ]] && printf "%s" "${0}" || printf "%s" "${PWD}/${0#./}")
        sudo "${script_path}" || {
            printf "Error: Administrator privileges are required.\n"
            exit 1
        }
        exit 0
    fi
}

clear_vivaldi_data() {
    local dir="${1}"
    if [ -d "${dir}" ]; then
        printf "Info: Clearing Vivaldi data in: %s\n" "${dir}"
        rm -r -f -v "${dir:?}"/*
    else
        printf "Warning: No Vivaldi data found in: %s, skipping...\n" "${dir}"
    fi
}

remove_vivaldi_files() {
    local base_dir="${1}"
    local file_pattern="${2}"
    find "${base_dir}" -name "${file_pattern}" -type f -delete
    printf "Info: Removed %s files from %s\n" "${file_pattern}" "${base_dir}"
}

clear_vivaldi_directories() {
    local base_dir="${1}"
    local dir_name="${2}"
    rm -r -f -v "${base_dir:?}/${dir_name:?}"
    printf "Info: Cleared %s directory from %s\n" "${dir_name}" "${base_dir}"
}

process_user_home() {
    local user_home="${1}"

    clear_vivaldi_data "${user_home}/.config/vivaldi"
    clear_vivaldi_data "${user_home}/.var/app/com.vivaldi.Vivaldi"

    vivaldi_files=(
        "Bookmarks*" "Cookies*" "History*" "Login Data*" "Web Data*"
        "Favicons*" "Shortcuts*" "Top Sites*" "Visited Links"
        "DownloadMetadata" "Extension Cookies*" "Media History*"
        "Network Action Predictor*" "Network Persistent State"
        "Preferences" "QuotaManager*" "Reporting and NEL*"
        "SecurePreferences" "SyncData.sqlite3" "Trust Tokens*"
    )

    for file in "${vivaldi_files[@]}"; do
        remove_vivaldi_files "${user_home}/.config/vivaldi" "${file}"
        remove_vivaldi_files "${user_home}/.var/app/com.vivaldi.Vivaldi" "${file}"
    done

    vivaldi_dirs=("Extensions" "File System" "Sessions")

    for dir in "${vivaldi_dirs[@]}"; do
        clear_vivaldi_directories "${user_home}/.config/vivaldi" "${dir}"
        clear_vivaldi_directories "${user_home}/.var/app/com.vivaldi.Vivaldi" "${dir}"
    done
}

main() {
    check_root

    printf "Info: Starting Vivaldi cleanup and privacy enhancement...\n"

    while IFS=: read -r user _ uid _ _ home _; do
        if [ "${uid}" -ge 1000 ] && [ -d "${home}" ]; then
            process_user_home "${home}"
        fi
    done </etc/passwd

    printf "Info: Vivaldi cleanup and privacy enhancement completed.\n"
}

main "$@"

cleanup

script_name1="$(basename "${0}")"
printf "\nInfo: script_name1: %s\n" "${script_name1}"
script_path1="$(realpath "$(dirname "${0}")")"
printf "Info: script_path1: %s\n" "${script_path1}"
script_path_with_name="${script_path1}/${script_name1}"
printf "Info: Script path with name: %s\n" "${script_path_with_name}"
printf "Info: Script finished\n"
exit 0
