#!/usr/bin/env bash
# Author: Pierre Gronau <Pierre.Gronau@ndaal.eu>
# Copyright 2025
# License: All content is licensed under the terms of the <Apache 2.0>
# Developed on: Debian 12.9x; macOS Sequoia x86 architecture
# Tested on: Debian 12.9x; macOS Sequoia x86 architecture
#
set -o errexit
set -o errtrace
set -o nounset
set -o pipefail
# set -o xtrace

# nosemgrep: ifs-tampering
IFS=$'\n\t'

# Error trapping function
# shellcheck disable=SC2317 
error_handler() {
    local line_number="${1}"
    local error_code="${2}"
    local last_command="${BASH_COMMAND}"
    printf "Error: Error occurred in line %s (error code: %s)\n" "${line_number}" "${error_code}"
    printf "Error: Failed command: %s\n" "${last_command}"
    return 0
}

# Set the error trap
trap 'error_handler ${LINENO} $?' ERR

cleanup() {
    trap - SIGINT SIGTERM ERR EXIT
    printf "%b\n" "\nInfo: Cleanup is running ..."
    rm -f -v ./*.tmp
    printf "%b\n" "\nInfo: Cleanup finished ..."
    return 0
}

trap cleanup SIGINT SIGTERM EXIT

# Set up environment
if [[ "$(uname)" == "Darwin" ]]; then
    HOMEDIR="Users"
elif [[ "$(uname)" == "Linux" ]]; then
    HOMEDIR="home"
else
    printf "Error: Unsupported operating system: %s\n" "$(uname)"
    exit 1
fi
readonly HOMEDIR

USERSCRIPT="cloud"
readonly USERSCRIPT
printf "Info: User script: %s\n" "${USERSCRIPT}"

VERSION="0.5.1"
readonly VERSION
printf "Info: Current version: %s\n" "${VERSION}"

TIMESTAMP="$(date +"%Y%m%d_%H%M%S")"
readonly TIMESTAMP

OUTPUT_DIR="/${HOMEDIR}/${USERSCRIPT}/system_analysis_${TIMESTAMP}"
readonly OUTPUT_DIR
mkdir -p "${OUTPUT_DIR}"

PACKAGE_INFO_DIR="${OUTPUT_DIR}/package_info"
readonly PACKAGE_INFO_DIR
mkdir -p "${PACKAGE_INFO_DIR}"

LOG_FILE="${OUTPUT_DIR}/analysis.log"
readonly LOG_FILE
touch "${LOG_FILE}"

# Include the provided list_packages function

# Function to list installed packages
list_packages() {
    local package_manager="${1:-}"

    case "${package_manager}" in
        # APT - https://wiki.debian.org/Apt
        "-apt"|"-dpkg"|"-debian"|"-ubuntu"|"apt"|"dpkg"|"debian"|"ubuntu")
            printf "Info: Using apt package manager\n"
            printf "Info: List installed packages\n"
            apt list --installed
            dpkg -l || true
            ;;
        # APK - https://wiki.alpinelinux.org/wiki/Alpine_Linux_package_management
        "-apk"|"apk")
            printf "Info: Using apk package manager\n"
            printf "Info: List installed packages\n"
            apk info || true
            ;;
        # Pacman - https://wiki.archlinux.org/title/pacman
        "-pacman"|"-arch"|"pacman"|"arch")
            printf "Info: Using pacman package manager\n"
            printf "Info: List installed packages\n"
            pacman -Q || true
            ;;
        # Zypper - https://en.opensuse.org/Portal:Zypper
        "-zypper"|"-suse"|"-opensuse"|"zypper"|"suse"|"opensuse")
            printf "Info: Using zypper package manager\n"
            printf "Info: List installed packages\n"
            zypper packages --installed-only || true
            ;;
        # Homebrew - https://docs.brew.sh
        "-brew"|"-mac"|"-macos"|"-osx"|"brew"|"mac"|"macos"|"osx")
            printf "Info: Using Homebrew package manager\n"
            printf "Info: List installed packages\n"
            brew list || true
            brew list --versions || true
            brew list --versions --multiple || true
            ;;
        # DNF - https://dnf.readthedocs.io
        "-dnf"|"dnf")
            printf "Info: Using dnf package manager\n"
            printf "Info: List installed packages\n"
            dnf list installed || true
            dnf history userinstalled || true
            ;;
        # RPM - https://rpm.org/documentation.html
        "-rpm"|"rpm")
            printf "Info: Using rpm package manager\n"
            printf "Info: List installed packages\n"
            rpm -qa || true
            rpm -q -a --queryformat '%{INSTALLTIME}~%{NAME}~%{VERSION}-%{RELEASE}\\n' || true
            ;;
        # YUM - http://yum.baseurl.org/wiki/Guides.html
        "-yum"|"yum")
            printf "Info: Using yum package manager\n"
            printf "Info: List installed packages\n"
            yum list installed || true
            ;;
        # XBPS - https://docs.voidlinux.org/xbps/index.html
        "-xbps"|"-void"|"xbps"|"void")
            printf "Info: Using xbps package manager\n"
            printf "Info: List installed packages\n"
            xbps-query -l || true
            ;;
        # Pisi - https://wiki.pisilinux.org/wiki/Package_Management
        "-pisi"|"-solus"|"pisi"|"solus")
            printf "Info: Using pisi package manager\n"
            printf "Info: List installed packages\n"
            eopkg list-installed || true
            ;;
        # Nix - https://nixos.org/manual/nix/stable
        "-nix"|"-nixos"|"nix"|"nixos")
            printf "Info: Using nix package manager\n"
            printf "Info: List installed packages\n"
            nix-env -q || true
            ;;
        # Yay - https://github.com/Jguer/yay#usage
        "-yay"|"yay")
            printf "Info: Using yay package manager\n"
            printf "Info: List installed packages\n"
            yay -Q || true
            ;;
        # Equo - https://www.sabayon.org/download
        "-equo"|"-sabayon"|"equo"|"sabayon")
            printf "Info: Using equo package manager\n"
            printf "Info: List installed packages\n"
            equo query list installed || true
            ;;
        # Aurman - https://github.com/polygamma/aurman#usage
        "-aurman"|"aurman")
            printf "Info: Using aurman package manager\n"
            printf "Info: List installed packages\n"
            aurman -Q || true
            ;;
        # Flatpak - https://docs.flatpak.org
        "-flatpak"|"flatpak")
            printf "Info: Using flatpak package manager\n"
            printf "Info: List installed packages\n"
            flatpak list || true
            ;;
        # Snap - https://snapcraft.io/docs
        "-snap"|"snap")
            printf "Info: Using snap package manager\n"
            printf "Info: List installed packages\n"
            snap list --all || true
            ;;
        # Cargo - https://doc.rust-lang.org/cargo/
        "-cargo"|"-rust"|"cargo"|"rust")
            printf "Info: Using cargo package manager\n"
            printf "Info: List installed packages\n"
            cargo install --list || true
            ;;
        # Portage - https://wiki.gentoo.org/wiki/Portage
        "-portage"|"portage")
            printf "Info: Using portage package manager\n"
            printf "Info: List installed packages\n"
            qlist -I || true
            ;;
        # Paludis - https://paludis.exherbo.org/
        "-paludis"|"paludis")
            printf "Info: Using paludis package manager\n"
            printf "Info: List installed packages\n"
            cave show installed-slots || true
            ;;
        # Conary - http://wiki.rpath.com/wiki/Conary
        "-conary"|"conary")
            printf "Info: Using conary package manager\n"
            printf "Info: List installed packages\n"
            conary query || true
            ;;
        # Slackpkg - https://docs.slackware.com/slackware:slackpkg
        "-slackpkg"|"-slackware"|"slackpkg"|"slackware")
            printf "Info: Using slackpkg package manager\n"
            printf "Info: List installed packages\n"
            find /var/log/packages -type f || true
            slackpkg search * | grep -v "uninstalled" || true
            /usr/sbin/slackpkg search * | grep -v "uninstalled" || true
            slackpkg info * || true
            /usr/sbin/slackpkg info * || true
            ;;
        # Zero Install - https://docs.0install.net/
        "-zeroinstall"|"zeroinstall")
            printf "Info: Using zeroinstall package manager\n"
            printf "Info: List installed packages\n"
            0install list || true
            ;;
        # Pip - https://pip.pypa.io/en/stable/
        "-pip"|"-pip3"|"pip"|"pip3")
            printf "Info: Using pip package manager\n"
            printf "Info: List installed packages\n"
            pip3 list || true
            ;;
        # FreeBSD pkg - https://www.freebsd.org/cgi/man.cgi?query=pkg&sektion=8
        "-pkg"|"-freebsd"|"pkg"|"freebsd")
            printf "Info: Using FreeBSD pkg package manager\n"
            printf "Info: List installed packages\n"
            pkg info || true
            ;;
        # OPKG - https://openwrt.org/docs/guide-user/additional-software/opkg
        "-opkg"|"opkg")
            printf "Info: Using opkg package manager\n"
            printf "Info: List installed packages\n"
            opkg list-installed || true
            ;;
        # OpenBSD pkg_add - https://man.openbsd.org/pkg_info.1
        "-pkg_add"|"-openbsd"|"pkg_add"|"openbsd")
            printf "Info: Using OpenBSD pkg_add package manager\n"
            printf "Info: List installed packages\n"
            pkg_info -A || true
            ;;
        # NetBSD pkgin - https://pkgin.net/
        "-pkgin"|"pkgin")
            printf "Info: Using NetBSD pkgin package manager\n"
            printf "Info: List installed packages\n"
            pkgin list || true
            ;;
        # IPKG - https://git.yoctoproject.org/ipkg-utils/
        "-ipkg"|"ipkg")
            printf "Info: Using ipkg package manager\n"
            printf "Info: List installed packages\n"
            ipkg list_installed || true
            ;;
        # Clear Linux swupd - https://www.clearlinux.org/clear-linux-documentation/guides/clear/swupd.html#updates
        "-swupd"|"-clear"|"swupd"|"clear")
            printf "Info: Using Clear Linux swupd package manager\n"
            printf "Info: List installed packages\n"
            swupd bundle-list -all || true
            ;;
        # GNU Guix - https://guix.gnu.org/manual/en/html_node/Invoking-guix-package.html
        "-guix"|"guix")
            printf "Info: Using GNU Guix package manager\n"
            printf "Info: List installed packages\n"
            guix package --list-installed || true
            ;;
        # SliTaz Tazpkg - http://doc.slitaz.org/en:handbook:packages
        "-tazpkg"|"-slitaz"|"tazpkg"|"slitaz")
            printf "Info: Using SliTaz tazpkg package manager\n"
            printf "Info: List installed packages\n"
            tazpkg list || true
            ;;
        # KISS Linux - https://k1ss.org/package-manager
        "-kiss"|"kiss")
            printf "Info: Using KISS Linux package manager\n"
            printf "Info: List installed packages\n"
            kiss list || true
            ;;
        # NPM - https://docs.npmjs.com/
        "-npm"|"npm")
            printf "Info: Using Node.js package manager\n"
            printf "Info: List installed packages\n"
            npm list -g --depth=0 || true
            ;;
        # RubyGems - https://guides.rubygems.org/
        "-gem"|"-ruby"|"gem"|"ruby")
            printf "Info: Using Ruby gem package manager\n"
            printf "Info: List installed packages\n"
            gem list || true
            ;;
        # PHP Composer - https://getcomposer.org/doc/
        "-composer"|"-php"|"composer"|"php")
            printf "Info: Using PHP Composer package manager\n"
            printf "Info: List installed packages\n"
            composer show --installed || true
            ;;
        "-h"|"-help"|"--help")
            printf "Hint: Select some argument to list installed packages using a package manager like -apt, -zypper, -dnf, -rpm, -arch, -xbps, -pisi, -nix, -yay, -equo, -pip, -flatpak, -snap, -brew, -cargo, -pkg, -opkg, -pkg_add, -pkgin, -swupd, -guix, -tazpkg, -kiss, -npm, -gem, -composer etc.\n"
            ;;
        *)
            printf "Warning: Unsupported package manager '%s'. Cannot list packages.\n" "${package_manager}"
            ;;
    esac

    printf "Info: Finished listing packages for '%s'.\n" "${package_manager}"

    # Success case
    return 0
}

log() {
    printf "%s - %s\n" "$(date '+%Y-%m-%d %H:%M:%S')" "${1}" | tee -a "${LOG_FILE}"
}

check_command() {
    if ! command -v "${1}" &>/dev/null; then
        printf "Error: %s is not installed or not in PATH. Please install it and try again.\n" "${1}"
        return 1
    fi

    # Success case
    return 0
}   

# Check for required commands
#for cmd in df mount rg file; do
#    check_command "${cmd}" || exit 1
# done

check_command "rg" || exit 1

log "Starting system analysis..."

# Get mount points
get_mount_points() {
    log "Collecting mount points..."
    df -h | grep -v "tmpfs" > "${OUTPUT_DIR}/mount_points.txt"
    mount | grep -v "tmpfs" >> "${OUTPUT_DIR}/mount_points.txt"
}

# Function to collect package information
collect_package_info() {
    log "Collecting package information from all available package managers..."
    
    # List of all package managers to try
    local package_managers=(
        "apt" "apk" "pacman" "zypper" "brew" "dnf" "rpm" "yum" "xbps" "pisi" 
        "nix" "yay" "equo" "aurman" "flatpak" "snap" "cargo" "portage" "pip" 
        "pip3" "pkg" "opkg" "pkg_add" "pkgin" "ipkg" "swupd" "guix" "tazpkg" 
        "kiss" "npm" "gem" "composer"
    )
    
    for manager in "${package_managers[@]}"; do
        log "Trying package manager: ${manager}"
        {
            printf "=== Package listing for %s ===\n" "${manager}"
            list_packages "${manager}" 2>&1 || true
        } > "${PACKAGE_INFO_DIR}/${manager}_packages.txt"
    done
}

# Analyze executable or library with package version comparison
analyze_file() {
    local file_path="${1}"
    local base_name
    base_name="$(basename "${file_path}")"
    local output_file="${OUTPUT_DIR}/artifact_${base_name}.txt"
    
    {
        printf "File: %s\n" "${file_path}"
        printf "Type: %s\n" "$(file "${file_path}")"
        
        if [[ -x "${file_path}" ]]; then
            printf "Permissions: %s\n" "$(ls -l "${file_path}")"
            printf "SHA256: %s\n" "$(sha256sum "${file_path}" 2>/dev/null || printf "N/A")"
            
            # Try to get version information safely
            if "${file_path}" --version >/dev/null 2>&1; then
                printf "Version information from binary:\n"
                "${file_path}" --version 2>&1 || true
                
                # Compare with package manager version if available
                printf "\nPackage manager version comparison:\n"
                for pkg_file in "${PACKAGE_INFO_DIR}"/*_packages.txt; do
                    if [[ -f "${pkg_file}" ]]; then
                        pkg_manager=$(basename "${pkg_file}" _packages.txt)
                        if grep -i "${base_name}" "${pkg_file}" >/dev/null 2>&1; then
                            printf "Found in %s:\n" "${pkg_manager}"
                            grep -i "${base_name}" "${pkg_file}" || true
                        fi
                    fi
                done
            elif "${file_path}" -V >/dev/null 2>&1; then
                printf "Version information from binary (-V):\n"
                "${file_path}" -V 2>&1 || true
            fi
        fi
        
        printf "\n---\n\n"
    } >> "${output_file}"
}

# Search for executables and libraries
search_artifacts() {
    log "Searching for executables and libraries..."
    local mount_point="${1}"
    
    # Skip certain directories for safety and efficiency
    local exclude_dirs="proc|sys|dev|run|tmp|var/tmp|media|mnt"
    
    if check_command "rg"; then
        log "Starting ripgrep search in ${mount_point}"
        
        # Create a temporary file to store raw rg results
        local rg_results_file="${OUTPUT_DIR}/rg_results_${TIMESTAMP}.txt"
        
        # First, collect all rg results
        rg --files \
           --hidden \
           --no-ignore \
           --no-ignore-vcs \
           --no-ignore-parent \
           --no-require-git \
           -g '!{'"${exclude_dirs}"'}/**' \
           -g '!{.git,node_modules,target}/**' \
           -g '*.{sh,bash,ksh,zsh,fish,tcsh,csh,exe,so,dylib,dll,bin,out}' \
           "${mount_point}" 2>&1 | tee "${rg_results_file}"

        log "Ripgrep search completed. Processing results..."
        
        # Now process each file found
        while IFS= read -r file; do
            if [[ -f "${file}" && -r "${file}" ]]; then
                printf "Processing file: %s\n" "${file}" | tee -a "${LOG_FILE}"
                file_type="$(file -b "${file}")"
                if [[ "${file_type}" =~ (ELF|executable|shared object|shell script) ]]; then
                    printf "Found artifact: %s (Type: %s)\n" "${file}" "${file_type}" | tee -a "${LOG_FILE}"
                    analyze_file "${file}"
                fi
            fi
        done < "${rg_results_file}"
        
        # Save a summary of found files
        {
            printf "=== Ripgrep Search Summary ===\n"
            printf "Search location: %s\n" "${mount_point}"
            printf "Total files found: %s\n" "$(wc -l < "${rg_results_file}")"
            printf "\nComplete file list:\n"
            cat "${rg_results_file}"
        } > "${OUTPUT_DIR}/rg_summary_$(basename "${mount_point}")_${TIMESTAMP}.txt"
        
        # Cleanup temporary file
        rm -f "${rg_results_file}"
    fi
}

# Main execution
get_mount_points
collect_package_info

# Process mount points (modified for macOS format)
while read -r line; do
    if [[ "${line}" =~ ^/dev/ ]]; then  # Only process actual device mounts
        mount_point=$(echo "${line}" | awk '{print $9}')  # For df output
        if [[ -z "${mount_point}" ]]; then
            mount_point=$(echo "${line}" | awk '{print $3}')  # For mount output
        fi
        
        if [[ -n "${mount_point}" ]]; then
            log "Processing mount point: ${mount_point}"
            if [[ "${mount_point}" == "/" ]] || 
               [[ "${mount_point}" == "/System/Volumes/Data" ]] || 
               [[ "${mount_point}" =~ ^/Volumes/ ]]; then
                log "Analyzing mount point: ${mount_point}"
                search_artifacts "${mount_point}"
            fi
        fi
    fi
done < "${OUTPUT_DIR}/mount_points.txt"

cleanup

log "Analysis complete. Results are in ${OUTPUT_DIR}"

script_name1="$(basename "${0}")"
printf "\nInfo: script_name1: %s\n" "${script_name1}"
script_path1="$(realpath "$(dirname "${0}")")"
printf "Info: script_path1: %s\n" "${script_path1}"
script_path_with_name="${script_path1}/${script_name1}"
printf "Info: Script path with name: %s\n" "${script_path_with_name}"
printf "Info: Script finished\n"
exit 0
