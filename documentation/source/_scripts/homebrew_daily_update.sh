#!/usr/bin/env bash
# Author: Pierre Gronau <Pierre.Gronau@ndaal.eu>
# Copyright 2024
# License: All content is licensed under the terms of the <MIT License>
# Developed on: Debian 12.x; macOS Sequoia x86 architecture
# Tested on: Debian 12.x; macOS Sequoia x86 architecture

set -o errexit
set -o errtrace
set -o nounset
set -o pipefail
# set -o xtrace
IFS=$'\n\t'

# Error trapping function
error_handler() {
    local line_number="${1}"
    local error_code="${2}"
    local last_command="${BASH_COMMAND}"
    printf "Error: Error occurred in line %s (error code: %s)\n" "${line_number}" "${error_code}"
    printf "Error: Failed command: %s\n" "${last_command}"
}

# Set the error trap
trap 'error_handler ${LINENO} $?' ERR

cleanup() {
    trap - SIGINT SIGTERM ERR EXIT
    printf "%b\n" "\nInfo: Cleanup is running ..."
    # Additional cleanup tasks can be added here if needed
    rm -f -v ./*.tmp
    printf "%b\n" "\nInfo: Cleanup finished ..."
}

trap cleanup SIGINT SIGTERM ERR EXIT

if [ "${EUID}" -ne 0 ]; then
    script_path=$([[ "${0}" = /* ]] && printf "%s" "${0}" || printf "%s" "${PWD}/${0#./}")
    sudo "${script_path}" || {
        printf "Error: Administrator privileges are required.\n"
        exit 1
    }
    exit 0
fi

export HOME="/home/${SUDO_USER:-${USER}}" # Keep `~` and `$HOME` for user not `/root`.

# Function to log information
log_info() {
    printf "Info: %s\n" "${1}"
}

# Function to log warnings
log_warning() {
    printf "Warning: %s\n" "${1}"
}

# Function to log errors
log_error() {
    printf "Error: %s\n" "${1}" >&2
}

# Function to update environment variable
update_environment_variable() {
    local variable="${1}"
    local value="${2}"
    local declaration_file="/etc/environment"

    if ! [ -f "${declaration_file}" ]; then
        log_info "\"${declaration_file}\" does not exist."
        sudo touch "${declaration_file}"
        log_info "Created ${declaration_file}."
    fi

    if ! grep --quiet "^${variable}=" "${declaration_file}"; then
        printf "%s=%s\n" "${variable}" "${value}" | sudo tee -a "${declaration_file}" > /dev/null
        log_info "Successfully configured ${variable}."
    else
        log_warning "${variable} is already configured."
    fi
}

# ----------------------------------------------------------
# ------------------Disable .NET telemetry------------------
# ----------------------------------------------------------
log_info "--- Disable .NET telemetry"
update_environment_variable "DOTNET_CLI_TELEMETRY_OPTOUT" "1"
# ----------------------------------------------------------

# ----------------------------------------------------------
# ------------------- Clear Wine cache ---------------------
# ----------------------------------------------------------
log_info "--- Clear Wine cache"
rm -r -f -v "${HOME}/.wine/drive_c/windows/temp/"*
rm -r -f -v "${HOME}/.cache/wine/"
# ----------------------------------------------------------

# ----------------------------------------------------------
# ------------------ Clear Winetricks cache ----------------
# ----------------------------------------------------------
log_info "--- Clear Winetricks cache"
if [ -d "${HOME}/.cache/winetricks/" ]; then
    rm -rfv "${HOME}/.cache/winetricks/"*
else
    log_warning "No Winetricks cache directory found, skipping..."
fi
# ----------------------------------------------------------

# ----------------------------------------------------------
# ---------- Clear Visual Studio Code crash reports ---------
# ----------------------------------------------------------
log_info "--- Clear Visual Studio Code crash reports"

# Function to clear crash reports from a specified directory
clear_crash_reports() {
    local dir="${1}"
    if [ -d "${dir}" ]; then
        log_info "Clearing crash reports in: ${dir}"
        rm -rfv "${dir}"*
    else
        log_warning "No crash reports found in: ${dir}, skipping..."
    fi
}

# Clear Crash Reports: Global installation (also Snap with --classic)
clear_crash_reports "${HOME}/.config/Code/Crash Reports/"
clear_crash_reports "${HOME}/.var/app/com.visualstudio.code/config/Code/Crash Reports/"
clear_crash_reports "${HOME}/.config/Code/exthost Crash Reports/"
clear_crash_reports "${HOME}/.var/app/com.visualstudio.code/config/Code/exthost Crash Reports/"
# ----------------------------------------------------------

# ----------------------------------------------------------
# --------------Clear Visual Studio Code logs---------------
# ----------------------------------------------------------
log_info "--- Clear Visual Studio Code logs"

# Function to clear logs from a specified directory
clear_logs() {
    local dir="${1}"
    if [ -d "${dir}" ]; then
        log_info "Clearing logs in: ${dir}"
        rm -r -f -v "${dir}"*
 else
        log_warning "No logs found in: ${dir}, skipping..."
    fi
}

# Clear logs: Global installation (also Snap with --classic)
clear_logs "${HOME}/.config/Code/logs/"
clear_logs "${HOME}/.var/app/com.visualstudio.code/config/Code/logs/"
# ----------------------------------------------------------

# ----------------------------------------------------------
# --------------Clear Azure CLI telemetry data--------------
# ----------------------------------------------------------
log_info "--- Clear Azure CLI telemetry data"

# Function to clear telemetry data from a specified directory
clear_azure_telemetry() {
    local telemetry_dir="${HOME}/.azure/telemetry"
    if [ -d "${telemetry_dir}" ]; then
        log_info "Clearing telemetry directory: ${telemetry_dir}"
        rm -rfv "${telemetry_dir}"
    else
        log_warning "Telemetry directory not found: ${telemetry_dir}, skipping..."
    fi

    # Remove other telemetry-related files
    rm -fv "${HOME}/.azure/telemetry.txt"
    rm -fv "${HOME}/.azure/logs/telemetry.txt"
}

# Call the function to clear Azure CLI telemetry data
clear_azure_telemetry
# ----------------------------------------------------------

# ----------------------------------------------------------
# -------------------Clear Azure CLI logs-------------------
# ----------------------------------------------------------
log_info "--- Clear Azure CLI logs"

# Function to clear logs from a specified directory
clear_azure_logs() {
    local logs_dir="${HOME}/.azure/logs"
    if [ -d "${logs_dir}" ]; then
        log_info "Clearing logs in: ${logs_dir}"
        rm -rfv "${logs_dir}"*
    else
        log_warning "No logs found in: ${logs_dir}, skipping..."
    fi
}

# Call the function to clear Azure CLI logs
clear_azure_logs
# ----------------------------------------------------------

# ----------------------------------------------------------
# ------------------Clear Azure CLI cache-------------------
# ----------------------------------------------------------
log_info "--- Clear Azure CLI cache"

# Check if the Azure CLI is installed
if ! command -v "az" &> /dev/null; then
    log_warning "Skipping because \"az\" is not found."
else
    log_info "Clearing Azure CLI cache..."
    az cache purge
fi
# ----------------------------------------------------------

# ----------------------------------------------------------
# -------------------Clear Firefox cache--------------------
# ----------------------------------------------------------
log_info "--- Clear Firefox cache"

# Function to clear cache from a specified directory
clear_firefox_cache() {
    local cache_dir="${1}"
    if [ -d "${cache_dir}" ]; then
        log_info "Clearing cache in: ${cache_dir}"
        rm -rfv "${cache_dir}"*
    else
        log_warning "No cache found in: ${cache_dir}, skipping..."
    fi
}

# Clear cache: Global installation
clear_firefox_cache "${HOME}/.cache/mozilla/"
clear_firefox_cache "${HOME}/.var/app/org.mozilla.firefox/cache/"
clear_firefox_cache "${HOME}/snap/firefox/common/.cache/"
# ----------------------------------------------------------

# ----------------------------------------------------------
# ---------------Clear Firefox crash reports----------------
# ----------------------------------------------------------
log_info "--- Clear Firefox crash reports"

# Function to clear crash reports from a specified directory
clear_firefox_crash_reports() {
    local crash_dir="${1}"
    if [ -d "${crash_dir}" ]; then
        log_info "Clearing crash reports in: ${crash_dir}"
        rm -rfv "${crash_dir}"*
    else
        log_warning "No crash reports found in: ${crash_dir}, skipping..."
    fi
}

# Clear crash reports: Global installation
clear_firefox_crash_reports "${HOME}/.mozilla/firefox/Crash Reports/"
clear_firefox_crash_reports "${HOME}/.var/app/org.mozilla.firefox/.mozilla/firefox/Crash Reports/"
clear_firefox_crash_reports "${HOME}/snap/firefox/common/.mozilla/firefox/Crash Reports/"
# ----------------------------------------------------------

# ----------------------------------------------------------
# Function to delete files matching a pattern using Python
delete_matching_files() {
    local pattern="${1}"
    if ! command -v "python3" &> /dev/null; then
        log_warning "Skipping because \"python3\" is not found."
        return
    fi

    python3 <<EOF
import glob
import os

pattern = '${pattern}'
expanded_path = os.path.expandvars(os.path.expanduser(pattern))
print(f'Deleting files matching pattern: {expanded_path}')
paths = glob.glob(expanded_path)

if not paths:
    print('Warning: Skipping, no paths found.')

for path in paths:
    if not os.path.isfile(path):
        print(f'Warning: Skipping folder: "{path}".')
        continue
    os.remove(path)
    print(f'Info: Successfully deleted file: "{path}".')

print(f'Info: Successfully deleted {len(paths)} file(s).')
EOF
}

# Delete files matching patterns for crashes and events
delete_matching_files "${ HOME}/.mozilla/firefox/*/crashes/*"
delete_matching_files "${HOME}/.var/app/org.mozilla.firefox/.mozilla/firefox/*/crashes/*"
delete_matching_files "${HOME}/snap/firefox/common/.mozilla/firefox/*/crashes/*"
delete_matching_files "${HOME}/.mozilla/firefox/*/crashes/events/*"
delete_matching_files "${HOME}/.var/app/org.mozilla.firefox/.mozilla/firefox/*/crashes/events/*"
delete_matching_files "${HOME}/snap/firefox/common/.mozilla/firefox/*/crashes/events/*"
# ----------------------------------------------------------

# ----------------------------------------------------------
# -----------------Remove old Snap packages-----------------
# ----------------------------------------------------------
log_info "--- Remove old Snap packages"

# Check if the snap command is available
if ! command -v "snap" &> /dev/null; then
    log_warning "Skipping because \"snap\" is not found."
else
    # List all Snap packages and remove disabled ones
    snap list --all | while read -r name version rev tracking publisher notes; do
        if [[ "${notes}" == *disabled* ]]; then
            log_info "Removing disabled Snap package: ${name} (revision: ${rev})"
            sudo snap remove "${name}" --revision="${rev}"
        fi
    done
fi
# ----------------------------------------------------------

# ----------------------------------------------------------
# -------------Remove orphaned Flatpak runtimes-------------
# ----------------------------------------------------------
log_info "--- Remove orphaned Flatpak runtimes"

# Check if the flatpak command is available
if ! command -v "flatpak" &> /dev/null; then
    log_warning "Skipping because \"flatpak\" is not found."
else
    log_info "Uninstalling unused Flatpak runtimes..."
    flatpak uninstall --unused --noninteractive
    log_info "Orphaned Flatpak runtimes removed successfully."
fi
# ----------------------------------------------------------

# ----------------------------------------------------------
# ---------------Clear obsolete APT packages----------------
# ----------------------------------------------------------
log_info "--- Clear obsolete APT packages"

# Check if the apt-get command is available
if ! command -v "apt-get" &> /dev/null; then
    log_warning "Skipping because \"apt-get\" is not found."
else
    log_info "Running 'apt-get autoclean' to remove obsolete packages..."
    sudo apt-get autoclean
    log_info "Obsolete APT packages cleared successfully."
fi
# ----------------------------------------------------------

# ----------------------------------------------------------
# ---------Clear orphaned APT package dependencies----------
# ----------------------------------------------------------
log_info "--- Clear orphaned APT package dependencies"

# Check if the apt-get command is available
if ! command -v "apt-get" &> /dev/null; then
    log_warning "Skipping because \"apt-get\" is not found."
else
    log_info "Running 'apt-get -y autoremove --purge' to clear orphaned dependencies..."
    sudo apt-get -y autoremove --purge
    log_info "Orphaned APT package dependencies cleared successfully."
fi
# ----------------------------------------------------------

# ----------------------------------------------------------
# -------Disable participation in Popularity Contest--------
# ----------------------------------------------------------
log_info "--- Disable participation in Popularity Contest"

# Configuration file path
config_file="/etc/popularity-contest.conf"

# Check if the configuration file exists
if [ -f "${config_file}" ]; then
    log_info "Modifying configuration file at: ${config_file}"
    sudo sed -i '/PARTICIPATE/c\PARTICIPATE=no' "${config_file}"
    log_info "Participation in Popularity Contest has been disabled."
else
    log_warning "Skipping because configuration file at (${config_file}) is not found. Is popcon installed?"
fi
# ----------------------------------------------------------

# ----------------------------------------------------------
# -----------Disable weekly `pkgstats` submission-----------
# ----------------------------------------------------------
log_info "--- Disable weekly `pkgstats` submission"

# Check if the systemctl command is available
if ! command -v "systemctl" &> /dev/null; then
    log_warning "Skipping because \"systemctl\" is not found."
else
    service="pkgstats.timer"

    # Check if the service exists
    if systemctl list-units --full --all | grep --fixed-strings --quiet "${service}"; then
        # Check if the service is enabled
        if systemctl is-enabled --quiet "${service}"; then
            # Check if the service is active
            if systemctl is-active --quiet "${service}"; then
                log_info "Service ${service} is running now, stopping it..."
                if ! sudo systemctl stop "${service}"; then
                    log_error "Could not stop ${service}."
                else
                    log_info "Successfully stopped ${service}."
                fi
            fi
            
            # Disable the service
            if sudo systemctl disable "${service}"; then
                log_info "Successfully disabled ${service}."
            else
                log_error " Could not disable ${service}."
            fi
        else
            log_info "${service} is already disabled."
        fi
    else
        log_warning "Skipping because ${service} is not found."
    fi
fi
# ----------------------------------------------------------

# ----------------------------------------------------------
# -----------------Disable Firefox application updates-------
# ----------------------------------------------------------
log_info "--- Disable Firefox application updates"

# Function to disable Firefox application updates
disable_firefox_updates() {
    local profile_dir="${1}"
    local prefs_file="${profile_dir}prefs.js"

    if [ ! -f "${prefs_file}" ]; then
        log_warning "Skipping because ${prefs_file} is not found."
        return
    fi

    # Check if the preference already exists
    if grep --quiet "^user_pref(\"app.update.auto\", false);" "${prefs_file}"; then
        log_info "Firefox application updates are already disabled in ${prefs_file}."
    else
        log_info "Disabling Firefox application updates in ${prefs_file}..."
        echo -e "\nuser_pref(\"app.update.auto\", false);" | sudo tee -a "${prefs_file}" > /dev/null
        log_info "Firefox application updates have been disabled in ${prefs_file}."
    fi
}

# Disable Firefox application updates for all profiles
declare -a profile_paths=(
    "${HOME}/.mozilla/firefox/*/"
    "${HOME}/.var/app/org.mozilla.firefox/.mozilla/firefox/*/"
    "${HOME}/snap/firefox/common/.mozilla/firefox/*/"
)

for profile_path in "${profile_paths[@]}"; do
    for profile_dir in "${profile_path}"; do
        if [ -d "${profile_dir}" ]; then
            disable_firefox_updates "${profile_dir}"
        fi
    done
done
# ----------------------------------------------------------

# ----------------------------------------------------------
# -----------------Disable Firefox application updates-------
# ----------------------------------------------------------
log_info "--- Disable Firefox application updates"

# Function to disable Firefox application updates
disable_firefox_updates() {
    local profile_dir="${1}"
    local prefs_file="${profile_dir}prefs.js"

    if [ ! -f "${prefs_file}" ]; then
        log_warning "Skipping because ${prefs_file} is not found."
        return
    fi

    # Check if the preference already exists
    if grep --quiet "^user_pref(\"app.update.auto\", false);" "${prefs_file}"; then
        log_info "Firefox application updates are already disabled in ${prefs_file}."
    else
        log_info "Disabling Firefox application updates in ${prefs_file}..."
        echo -e "\nuser_pref(\"app.update.auto\", false);" | sudo tee -a "${prefs_file}" > /dev/null
        log_info "Firefox application updates have been disabled in ${prefs_file}."
    fi
}

# Disable Firefox application updates for all profiles
declare -a profile_paths=(
    "${HOME}/.mozilla/firefox/*/"
    "${HOME}/.var/app/org.mozilla.firefox/.mozilla/firefox/*/"
    "${HOME}/snap/firefox/common/.mozilla/firefox/*/"
)

for profile_path in "${profile_paths[@]}"; do
    for profile_dir in "${profile_path}"; do
        if [ -d "${profile_dir}" ]; then
            disable_firefox_updates "${profile_dir}"
        fi
    done
done
# ----------------------------------------------------------

# ----------------------------------------------------------
# -----------------Disable Firefox application updates-------
# ----------------------------------------------------------
log_info "--- Disable Firefox application updates"

# Function to disable Firefox application updates
disable_firefox_updates() {
    local profile_dir="${1}"
    local prefs_file="${profile_dir}prefs.js"

    if [ ! -f "${prefs_file}" ]; then
        log_warning "Skipping because ${prefs_file} is not found."
        return
    fi

    # Check if the preference already exists
    if grep --quiet "^user_pref(\"app.update.auto\", false);" "${prefs_file}"; then
        log_info "Firefox application updates are already disabled in ${prefs_file}."
    else
        log_info "Disabling Firefox application updates in ${prefs_file}..."
        echo -e "\nuser_pref(\"app.update.auto\", false);" | sudo tee -a "${prefs_file}" > /dev/null
        log_info "Firefox application updates have been disabled in ${prefs_file}."
    fi
}

# Disable Firefox application updates for all profiles
declare -a profile_paths=(
    "${HOME}/.mozilla/firefox/*/"
    "${HOME}/.var/app/org.mozilla.firefox/.mozilla/firefox/*/"
    "${HOME}/snap/firefox/common/.mozilla/firefox/*/"
)

for profile_path in "${profile_paths[@]}"; do
    for profile_dir in ${profile_path}; do
        if [ -d "${profile_dir}" ]; then
            disable_firefox_updates "${profile_dir}"
        fi
    done
done
# ----------------------------------------------------------

# ----------------------------------------------------------
# ----------------- Clear user-specific cache ---------------
# ----------------------------------------------------------
log_info "--- Clear user-specific cache"

# Function to safely remove contents of a directory if it exists
clear_user_cache() {
    local path="${1}"
    if [ -d "${path}" ]; then
        log_info "Clearing contents of ${path}"
        rm -r -f -v "${path}"/*
    else
        log_warning "Directory ${path} does not exist, skipping."
    fi
}

# Clear user cache
clear_user_cache "${HOME}/.cache"
# ----------------------------------------------------------

# ----------------------------------------------------------
# ----------------- Clear system-wide cache -----------------
# ----------------------------------------------------------
log_info "--- Clear system-wide cache"

# Function to safely remove contents of a directory if it exists
clear_system_cache() {
    local path="${1}"
    if [ -d "${path}" ]; then
        log_info "Clearing contents of ${path}"
        sudo rm -r -f -v "${path}"/*
    else
        log_warning "Directory ${path} does not exist, skipping."
    fi
}

# Clear the system cache in /var/cache
clear_system_cache "/var/cache"
# ----------------------------------------------------------

# ----------------------------------------------------------
# ----------------- Clear Snap application cache ------------
# ----------------------------------------------------------
log_info "--- Clear Snap application cache"

# Function to safely remove contents of a directory if it exists
clear_snap_cache() {
    local path="${1}"
    if [ -d "${path}" ]; then
        log_info "Clearing contents of ${path}"
        sudo rm -r -f -v "${path}"/*
    else
        log_warning "Directory ${path} does not exist, skipping."
    fi
}

# Clear the Snap cache directory
clear_snap_cache "/var/lib/snapd/cache"
# ----------------------------------------------------------

# ----------------------------------------------------------
# ----------------- Clear Flatpak application cache ---------
# ----------------------------------------------------------
log_info "--- Clear Flatpak application cache"

# Function to safely remove contents of a directory if it exists
clear_flatpak_cache() {
    local path="${1}"
    if [ -d "${path}" ]; then
        log_info "Clearing contents of ${path}"
        rm -r -f -v "${path}"/*
    else
        log_warning "Directory ${path} does not exist, skipping."
    fi
}

# Clear the Flatpak application cache
clear_flatpak_cache "${HOME}/.var/app/*/cache"
# ----------------------------------------------------------

# ----------------------------------------------------------
# ----------------- Clear system logs -----------------------
# ----------------------------------------------------------
log_info "--- Clear system logs"

# Check if journalctl is installed
if ! command -v journalctl &> /dev/null; then
    log_warning "Skipping because journalctl is not found."
else
    log_info "Vacuuming journal logs older than 1 second..."
    sudo journalctl --vacuum-time=1s
fi
# ----------------------------------------------------------

# ----------------------------------------------------------
# ----------------- Clear user-specific logs ----------------
# ----------------------------------------------------------
log_info "--- Clear user-specific logs"

# Function to safely remove contents of a directory if it exists
clear_user_logs() {
    local path="${1}"
    if [ -d "${path}" ]; then
        log_info "Clearing contents of ${path}"
        rm -r -f -v "${path}"/*
    else
        log_warning "Directory ${path} does not exist, skipping."
    fi
}

# Clear user logs
clear_user_logs "${HOME}/.local/share/logs"
# ----------------------------------------------------------

# ----------------------------------------------------------
# ----------------- Final cleanup ---------------------------
# ----------------------------------------------------------
log_info "Your privacy and security is now hardened 🎉💪"
log_info "Press any key to exit."
read -r -n 1 -s # ----------------------------------------------------------

# ----------------------------------------------------------
# ----------------- Script path with name -------------------
# ----------------------------------------------------------
script_path_with_name="$(realpath "${0}")"
log_info "Info: Script path with name: ${script_path_with_name}"
log_info "Info: Script finished"
exit 0
