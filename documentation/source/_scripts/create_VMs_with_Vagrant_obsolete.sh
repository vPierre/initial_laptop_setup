#!/usr/bin/env bash

# Author: Pierre Gronau <Pierre.Gronau@ndaal.eu>
# Copyright 2024,2025
# License: All content is licensed under the terms of the <Apache 2.0>
# Developed on: Debian 12.x; macOS Sequoia x86 architecture
# Tested on: Debian 12.x; macOS Sequoia x86 architecture

set -o errexit
set -o errtrace
set -o nounset
set -o pipefail
# set -o xtrace

# nosemgrep: ifs-tampering
IFS=$'\n\t'

# Error trapping function
# Add this line before the error_handler function
# shellcheck disable=SC2317
error_handler() {
    local line_number="${1}"
    local error_code="${2}"
    local last_command="${BASH_COMMAND}"
    printf "Error: Error occurred in line %s (error code: %s)\n" "${line_number}" "${error_code}"
    printf "Error: Failed command: %s\n" "${last_command}"

    # Success case
    return 0
}

# Set the error trap
trap 'error_handler ${LINENO} $?' ERR

cleanup() {
    trap - SIGINT SIGTERM ERR EXIT
    printf "%b\n" "\nInfo: Cleanup is running ..."
    log "Info: Cleanup is running ..."
    # Additional cleanup tasks can be added here if needed
    rm -f -v ./*.tmp
    printf "%b\n" "\nInfo: Cleanup finished ..."
    log "Info: Cleanup finished ..."

    # Success case
    return 0
}

trap cleanup SIGINT SIGTERM ERR EXIT

if [[ "$(uname)" == "Darwin" ]]; then
    HOMEDIR="Users"
elif [[ "$(uname)" == "Linux" ]]; then
    HOMEDIR="home"
else
    printf "Error: Unsupported operating system: %s\n" "$(uname)"
    exit 1
fi
readonly HOMEDIR

# Under Linux you use `home` under macOS `Users`
printf "Info: Home Directory: %s\n" "${HOMEDIR}"

USERSCRIPT="cloud"
# Your user! In which context it SHOULD run
printf "Info: User script: %s\n" "${USERSCRIPT}"

VERSION="1.1.2"
readonly VERSION
printf "Info: Current version: %s\n" "${VERSION}"

LOG_FILE="/${HOMEDIR}/${USERSCRIPT}/create_VMs_with_Vagrant.log"
readonly LOG_FILE 
printf "Info: log file with path: %s\n" "${LOG_FILE}"
if [[ ! -f "${LOG_FILE}" ]]; then
    printf "Info: Log file does not exist. Creating it now.\n"
    touch "${LOG_FILE}" || {
        printf "Error: Failed to create log file at %s\n" "${LOG_FILE}"
        exit 1
    }
else
    printf "Info: Log file already exists at %s\n" "${LOG_FILE}"
fi

log() {
    printf "%s - %s\n" "$(date '+%Y-%m-%d %H:%M:%S')" "${1}" | tee -a "${LOG_FILE}"
}

log "starting script"

print_asterisks_value=79
readonly print_asterisks_value 
printf "Info: Current print_asterisks_value: %s\n" "${print_asterisks_value}"

# Function to print a line of x asterisks
print_asterisks() {
    local x="${1}"

    # Check if x is within the valid range
    if [[ "${x}" -lt 10 || "${x}" -gt 79 ]]; then
        printf "Error: The value of x (%d) must be between 10 and 79.\n" "${x}"
        exit 1
    fi

    # Print x asterisks
    printf "%0.s*" $(seq 1 "${x}")
    printf "\n"

    # Success case
    return 0
}

print_asterisks "${print_asterisks_value}"

DEBUG=0 # Set to 1 to enable debug output, 0 to disable
readonly DEBUG

# Print messages based on the value of DEBUG
if [[ "${DEBUG}" -eq 0 ]]; then
    printf "Info: DEBUG is set to 0. Debug output is disabled.\n"
elif [[ "${DEBUG}" -eq 1 ]]; then
    printf "Info: DEBUG is set to 1. Debug output is enabled.\n"
else
    printf "Error: Invalid value for DEBUG: %s. Please set it to 0 or 1.\n" "${DEBUG}"
fi

print_asterisks "${print_asterisks_value}"

# Set DESTROY variable
DESTROY=1  # Change this to 1 to destroy the VMs
readonly DESTROY

# Print messages based on the value of DESTROY
if [[ "${DESTROY}" -eq 0 ]]; then
    printf "Info: DESTROY is set to 0. No action will be taken on the VMs.\n"
elif [[ "${DESTROY}" -eq 1 ]]; then
    printf "Warning: DESTROY is set to 1. All VMs will be destroyed!\n"
else
    printf "Error: Invalid value for DESTROY: %s. Please set it to 0 or 1.\n" "${DESTROY}"
fi

print_asterisks "${print_asterisks_value}"

expected_user="cloud"
readonly expected_user
printf "Info: Current expected_user: %s\n" "${expected_user}"

expected_group="staff"
readonly expected_group
printf "Info: Current expected_group: %s\n" "${expected_group}"

max_line_length="${2:-1000}"
printf "Info: Current max_line_length: %s\n" "${max_line_length}"

default_encoding="UTF-8"
readonly default_encoding
printf "Info: Current default_encoding: %s\n" "${default_encoding}"

vagrant_scp_check=0  # Change this to 1 to check the vagrant_scp plugin
readonly vagrant_scp_check
printf "Info: Current vagrant_scp_check: %s\n" "${vagrant_scp_check}"

do_copy_files_to_vms=0
readonly do_copy_files_to_vms
printf "Info: Current do_copy_files_to_vms: %s\n" "${do_copy_files_to_vms}"

do_copy_files_from_directory_to_vms=0
readonly do_copy_files_from_directory_to_vms
printf "Info: Current do_copy_files_from_directory_to_vms: %s\n" "${do_copy_files_from_directory_to_vms}"

do_make_files_executable=0
readonly do_make_files_executable
printf "Info: Current do_make_files_executable: %s\n" "${do_make_files_executable}"

package_manager_choice="apt"
printf "Info: Default package_manager_choice: %s\n" "${package_manager_choice}"

vm_name="debian"
printf "Info: Default vm_name: %s\n" "${vm_name}"

do_update_packages=0
readonly do_update_packages
printf "Info: Current do_update_packages: %s\n" "${do_update_packages}"

do_install_packages=0
readonly do_install_packages
printf "Info: Current do_install_packages: %s\n" "${do_install_packages}"

do_cleanup_packages=0
readonly do_cleanup_packages
printf "Info: Current do_cleanup_packages: %s\n" "${do_cleanup_packages}"

print_asterisks "${print_asterisks_value}"

DEFAULT_REQUIRED_SPACE_MB=20000  # Default: 1GB in MB
readonly DEFAULT_REQUIRED_SPACE_MB
printf "Info: Current DEFAULT_REQUIRED_SPACE_MB: %s\n\n" "${DEFAULT_REQUIRED_SPACE_MB}"

# Use configured or default values
REQUIRED_SPACE_MB="${REQUIRED_SPACE_MB:-${DEFAULT_REQUIRED_SPACE_MB}}"
readonly REQUIRED_SPACE_MB
printf "Info: Current REQUIRED_SPACE_MB: %s\n\n" "${REQUIRED_SPACE_MB}"

# Convert MB to KB for df comparison
REQUIRED_SPACE_KB=$((REQUIRED_SPACE_MB * 1024))
readonly REQUIRED_SPACE_KB
printf "Info: Current REQUIRED_SPACE_KB: %s\n\n" "${REQUIRED_SPACE_KB}"

print_asterisks "${print_asterisks_value}"

# Set SSH_CHECK variable
SSH_CHECK=0  # Change this to 1 to SSH into all VMs
readonly SSH_CHECK

# Print messages based on the value of SSH_CHECK
if [[ "${SSH_CHECK}" -eq 0 ]]; then
    printf "Info: SSH_CHECK is set to 0. No SSH actions will be performed on the VMs.\n"
elif [[ "${SSH_CHECK}" -eq 1 ]]; then
    printf "Info: SSH_CHECK is set to 1. SSH will be attempted on all VMs.\n"
else
    printf "Error: Invalid value for SSH_CHECK: %s. Please set it to 0 or 1.\n" "${SSH_CHECK}"
fi

print_asterisks "${print_asterisks_value}"

# Function to check if a command is available
check_command() {
    if ! command -v "${1}" &>/dev/null; then
        printf "Error: %s is not installed or not in PATH. Please install it and try again.\n" "${1}"
        return 1
    fi

    printf "Info: %s is installed.\n" "${1}"

    # Success case
    return 0
}

# Check for required commands
check_command "vagrant" || exit 1
check_command "virtualbox" || exit 1
check_command "awk" || exit 1

printf "Info: The required commands are available.\n"
print_asterisks "${print_asterisks_value}"

create_directory() {
    local dir="${1}"
    printf "Info: Directory will be %s\n" "${dir}"
    if [[ ! -d ${dir} ]]; then
        mkdir -p -v "${dir}"
        printf "Info: Directory %s is created.\n" "${dir}"
        printf "Info: placeholder.txt will be created in %s\n" "${dir}"
        touch "${dir}/placeholder.txt"
        printf "Info: placeholder.txt will be removed from %s\n" "${dir}"
        rm -f -v "${dir}/placeholder.txt"
        printf "Info: placeholder.txt is removed from %s\n" "${dir}"
    else
        printf "Info: Directory %s already exists. No creation needed.\n" "${dir}"
    fi

    # Success case
    return 0
}

# Enhanced disk space checking function
check_disk_space() {
    local available_space
    local required_space="${REQUIRED_SPACE_KB}"

    available_space=$(df -k . | awk 'NR==2 {print $4}')

    printf "Info: Checking disk space. Required: %sMB (%sKB), Available: %sMB\n" \
        "${REQUIRED_SPACE_MB}" "${required_space}" "$((available_space / 1024))"

    if [ "${available_space}" -lt "${required_space}" ]; then
        printf "Error: Insufficient disk space!\n"
        printf "Error: Required: %sMB (%sKB)\n" "${REQUIRED_SPACE_MB}" "${required_space}"
        printf "Error: Available: %sMB (%sKB)\n" "$((available_space / 1024))" "${available_space}"
        exit 1
    fi

    # If we have less than 2x the required space, warn the user
    if [ "${available_space}" -lt "$((required_space * 2))" ]; then
        printf "Info: Warning: Available disk space is less than 2x the required space\n"
        printf "Info: This might cause issues if large files are generated\n"
    fi
}

check_disk_space

# Check if the vagrant-scp plugin is installed
check_install_vagrant_scp() {
    if [ "${vagrant_scp_check}" -eq 1 ]; then
        if vagrant plugin list | grep -q 'vagrant-scp'; then
            vagrant plugin list
            printf "Info: vagrant-scp is already installed.\n"
            if vagrant plugin update vagrant-scp; then
                printf "Info: vagrant-scp is updated.\n"
                vagrant plugin list
            else
                printf "Error: Failed to update vagrant-scp.\n"
                return 1
            fi
        else
            if vagrant plugin install vagrant-scp; then
                vagrant plugin list
                printf "Info: vagrant-scp has been successfully installed.\n"
            else
                printf "Error: Failed to install vagrant-scp. Please check your Vagrant setup.\n"
                return 1
            fi
        fi
    fi

    # Success case
    return 0
}

check_install_vagrant_scp "${vagrant_scp_check}"

print_asterisks "${print_asterisks_value}"

# Declare a readonly source for the vagrant files
VAGRANT_SOURCE_DIR="/${HOMEDIR}/${USERSCRIPT}/"
# VAGRANT_SOURCE_DIR="/path/to/vagrant/source"
readonly VAGRANT_SOURCE_DIR
printf "Info: Current first VAGRANT_SOURCE_DIR: %s\n" "${VAGRANT_SOURCE_DIR}"

DIRECTORY="${VAGRANT_SOURCE_DIR}"
printf "Info: %s\n" "${DIRECTORY}"

create_directory "${DIRECTORY}"

# Define a read-only array of required files for vagrant
declare -r required_files_for_vagrant=(
    "create_VMs_with_Vagrant_and_roboxes.sh"
    "create_VMs_with_Vagrant_and_jborean93.sh"
    "create_VMs_with_Vagrant.sh"
)

printf "Info: required files for all VMs:\n"
printf "Info: required file: %s\n" "${required_files_for_vagrant[@]}"

# Check if the files exist in the VAGRANT_SOURCE_DIR
for file in "${required_files_for_vagrant[@]}"; do
    full_path="${VAGRANT_SOURCE_DIR}${file}"
    
    if [ -e "${full_path}" ]; then
        printf "Info: File exists: %s\n" "${full_path}"
        find "${full_path}" -maxdepth 1 -type f -name "*.sh" -print0 | xargs -0 ls -la --
    else
        printf "Error: File does not exist: %s\n" "${full_path}"
    fi
done

print_asterisks "${print_asterisks_value}"

# Define a read-only array of executable files for vagrant
declare -r executable_files_for_vagrant=(
    "create_VMs_with_Vagrant_and_roboxes.sh"
    "create_VMs_with_Vagrant.sh"
)

printf "Info: executable files for all VMs:\n"
printf "Info: executable file: %s\n" "${required_files_for_vagrant[@]}"

# Check if the files exist in the VAGRANT_SOURCE_DIR
for file in "${executable_files_for_vagrant[@]}"; do
    full_path="${VAGRANT_SOURCE_DIR}${file}"
    
    if [ -e "${full_path}" ]; then
        printf "Info: File exists: %s\n" "${full_path}"
        find "${full_path}" -maxdepth 1 -type f -name "*.sh" -print0 | xargs -0 ls -la --
    else
        printf "Error: File does not exist: %s\n" "${full_path}"
    fi
done

print_asterisks "${print_asterisks_value}"

# Declare a second readonly source for the vagrant files
VAGRANT_SOURCE_DIR_2="/${HOMEDIR}/${USERSCRIPT}/repos/initial_laptop_setup/scripts/"
# VAGRANT_SOURCE_DIR_2="/path/to/vagrant/source"
readonly VAGRANT_SOURCE_DIR_2
printf "Info: Current second VAGRANT_SOURCE_DIR_: %s\n" "${VAGRANT_SOURCE_DIR_2}"

DIRECTORY="${VAGRANT_SOURCE_DIR_2}"
printf "Info: %s\n" "${DIRECTORY}"

create_directory "${DIRECTORY}"

ls -la "${DIRECTORY}"

print_asterisks "${print_asterisks_value}"

# Define VMs and their configurations for Vagrant
declare -A vms=(
    ["debian"]="debian/bookworm64"
    ["debian-automation-controller"]="debian/bookworm64"
    ["debian-job-controller"]="debian/bookworm64"
    ["debian-vulnerability-scanner"]="debian/bookworm64"
    ["Oracle-93"]="roboxes-x64/oracle9"
    ##["ubuntu"]="alvistack/ubuntu-24.10"
    ##["opensuse"]="opensuse/Leap-15.4.x86_64"
    ##["sles11sp4"]="wvera/sles11sp4"
    ##["arch"]="archlinux/archlinux"
    ##["fedora"]="generic/fedora37"
    ["AlmaLinux95"]="almalinux/9"
    ["AlmaLinux94"]="bento/almalinux-9.4"
    ["AlmaLinux93"]="bento/almalinux-9.3"
    ["AlmaLinux92"]="bento/almalinux-9.2"
    ["AlmaLinux91"]="bento/almalinux-9.1"
    ["AlmaLinux90"]="bento/almalinux-9.0"
    ##["AlmaLinux8"]="almalinux/8"
    #["AlmaLinux7"]="almalinux/7"
    ##["centos"]="centos/8"
    ##["centos"]="centos/7"
    #["centos"]="centos/6"
    ##["centos"]="bento/centos-5"
    #["centos"]="centos/4"
    ##["void"]="kstn/voidlinux"
    #["solus"]="solus/4"
    #["nixos"]="boxen/nixos-23.05"
    ##["manjaro"]="GeorgeC/ManjaroLinux"
    ##["Sabayon"]="Sabayon/spinbase-amd64"
    #["macos"]="gobadiah/macos-mojave"
    ##["slackware64-14.2"]="crawford/slackware64-14.2"
    ##["slackware64-14.1"]="ratfactor/slackware"
    ##["slackware64-15"]="normation/slackware-15-64"
)

check_file_exists() {
    local file_path="${1}"
    if [ "${DEBUG}" -eq 1 ]; then
        printf "Debug: Current file_path: %s\n" "${file_path}"
        printf "Debug: Function check_file_exists %s\n"
    fi

    if [[ ! -f "${file_path}" ]]; then
        printf "Error: File '%s' does not exist.\n" "${file_path}" >&2
        return 1
    fi

    # Success case
    return 0
}

check_file_extension() {
    local file_path="${1}"
    if [ "${DEBUG}" -eq 1 ]; then
        printf "Debug: Current file_path: %s\n" "${file_path}"
        printf "Debug: Function check_file_extension %s\n"
    fi    
  
    if [[ "${file_path}" != *.cfg ]]; then
        printf "Error: File '%s' does not have a .cfg extension.\n" "${file_path}" >&2
        return 1
    fi
    
    # Success case
    return 0
}

check_file_readable() {
    local file_path="${1}"
    if [ "${DEBUG}" -eq 1 ]; then
        printf "Debug: Current file_path: %s\n" "${file_path}"
        printf "Debug: Function check_file_readable %s\n"
    fi 
    
    if [[ ! -r "${file_path}" ]]; then
        printf "Error: File '%s' is not readable.\n" "${file_path}" >&2
        return 1
    fi

    if [ "${DEBUG}" -eq 1 ]; then
        cat -v -t -e "${file_path}"
    fi     

    # Success case
    return 0
}

check_file_ownership() {
    local file_path="${1}"
    
    if [ "${DEBUG}" -eq 1 ]; then
        printf "Debug: Current file_path: %s\n" "${file_path}"
        printf "Debug: Function check_file_ownership %s\n"
        printf "Debug: Current expected_user: %s\n" "${expected_user}"
        printf "Debug: Current expected_group: %s\n" "${expected_group}"
    fi
    
    local file_info=$(ls -l "${file_path}")
    local actual_user=$(echo "${file_info}" | awk '{print $3}')
    local actual_group=$(echo "${file_info}" | awk '{print $4}')
    
    if [[ "${actual_user}" != "${expected_user}" ]]; then
        printf "Error: File '%s' is not owned by %s (actual owner: %s).\n" "${file_path}" "${expected_user}" "${actual_user}" >&2
        return 1
    fi
    
    if [[ "${actual_group}" != "${expected_group}" ]]; then
        printf "Error: File '%s' is not owned by group %s (actual group: %s).\n" "${file_path}" "${expected_group}" "${actual_group}" >&2
        return 1
    fi

    # Success case
    return 0
}

check_modification_time() {
    local file_path="${1}"
    if [ "${DEBUG}" -eq 1 ]; then
        printf "Debug: Current file_path: %s\n" "${file_path}"
        printf "Debug: Function check_modification_time %s\n"
    fi
    
    local modification_time=1
    local file_mtime=$(stat -c %Y "${file_path}")
    local current_time=$(date +%s)
    local time_diff=$((current_time - file_mtime))
    local max_time=$((modification_time * 3600))
    if [[ ${time_diff} -gt ${max_time} ]]; then
        printf "Error: File '%s' was not modified within the last %d hours.\n" "${file_path}" "${modification_time}" >&2
        return 1
    fi

    # Success case
    return 0
}

validate_file_hash() {
    local file_path="${1}"
    if [ "${DEBUG}" -eq 1 ]; then
        printf "Debug: Current file_path: %s\n" "${file_path}"
        printf "Debug: Function validate_file_hash %s\n"
    fi

    local hash_file="${file_path}.sha3-512"
    printf "Info: Current hash_file: %s\n" "${hash_file}"
    cat "${hash_file}"
    if [[ ! -f "${hash_file}" ]]; then
        printf "Error: Hash file '%s' does not exist.\n" "${hash_file}" >&2
        return 1
    fi
    local expected_hash=$(cat "${hash_file}")
    printf "Info: expected_hash: %s\n" "${expected_hash}"
    local actual_hash=$(sha512sum "${file_path}" | awk '{print $1}')
    printf "Info: actual_hash: %s\n" "${actual_hash}"
    if [[ "${actual_hash}" != "${expected_hash}" ]]; then
        printf "Error: File '%s' hash does not match expected hash.\n" "${file_path}" >&2
        return 1
    fi

    # Success case
    return 0
}

check_for_symlinks() {
    local file_path="${1}"
    if [ "${DEBUG}" -eq 1 ]; then
        printf "Debug: Current file_path: %s\n" "${file_path}"
        printf "Debug: Function check_for_symlinks %s\n"
    fi

    if [[ -L "${file_path}" ]]; then
        printf "Error: File '%s' is a symlink.\n" "${file_path}" >&2
        return 1
    fi

    # Success case
    return 0
}

validate_file_path() {
    local file_path="${1}"
    if [ "${DEBUG}" -eq 1 ]; then
        printf "Debug: Current file_path: %s\n" "${file_path}"
        printf "Debug: Function validate_file_path %s\n"
    fi

    if [[ "${file_path}" == *".."* ]]; then
        printf "Error: File path '%s' contains directory traversal.\n" "${file_path}" >&2
        return 1
    fi

    # Success case
    return 0
}

check_setuid_setgid() {
    local file_path="${1}"
    if [ "${DEBUG}" -eq 1 ]; then
        printf "Debug: Current file_path: %s\n" "${file_path}"
        printf "Debug: Function check_setuid_setgid\n"
    fi

    local os_type
    os_type=$(uname)

    if [ "${os_type}" = "Darwin" ]; then
        # macOS
        local file_perms
        file_perms=$(ls -l "${file_path}" | cut -d ' ' -f 1)
        if [[ "${file_perms:3:1}" == "s" || "${file_perms:6:1}" == "s" ]]; then
            printf "Error: File '%s' has setuid or setgid bit set.\n" "${file_path}" >&2
            return 1
        fi
    elif [ "${os_type}" = "Linux" ]; then
        # Linux
        local file_perms
        file_perms=$(stat -c '%a' "${file_path}")
        if [[ "${file_perms:0:1}" == "2" || "${file_perms:0:1}" == "4" ]]; then
            printf "Error: File '%s' has setuid or setgid bit set.\n" "${file_path}" >&2
            return 1
        fi
    else
        printf "Error: Unsupported operating system.\n" >&2
        return 1
    fi

    # Success case
    return 0
}

check_file_size() {
    local file_path="${1}"
    if [ "${DEBUG}" -eq 1 ]; then
        printf "Debug: Current file_path: %s\n" "${file_path}"
        printf "Debug: Function check_file_size %s\n"
    fi

    local limit_file_size=75
    local file_size=$(du -k "${file_path}" | cut -f1)
    if [[ ${file_size} -gt ${limit_file_size} ]]; then
        printf "Error: File '%s' exceeds maximum size of %d KB.\n" "${file_path}" "${limit_file_size}" >&2
        return 1
    fi

    # Success case
    return 0
}

validate_file_structure() {
    local file_path="${1}"
    if [ "${DEBUG}" -eq 1 ]; then
        printf "Debug: Current file_path: %s\n" "${file_path}"
        printf "Debug: Function validate_file_structure %s\n"
    fi

    # This is a placeholder function. You need to implement specific checks
    # for your configuration file format.
    printf "Warning: File structure validation not implemented for '%s'.\n" "${file_path}" >&2

    # Success case
    return 0
}

check_whitelist() {
    local file_path="${1}"
    if [ "${DEBUG}" -eq 1 ]; then
        printf "Debug: Current file_path: %s\n" "${file_path}"
        printf "Debug: Function check_whitelist %s\n"
    fi

    local allowed_paths=(
        "/${HOMEDIR}/${USERSCRIPT}/repos/initial_laptop_setup/scripts/"
        "/${HOMEDIR}/${USERSCRIPT}/"
    )
    local is_allowed=false
    for allowed_path in "${allowed_paths[@]}"; do
        if [[ "${file_path}" == "${allowed_path}"* ]]; then
            is_allowed=true
            break
        fi
    done
    if [[ "${is_allowed}" != true ]]; then
        printf "Error: File '%s' is not in the allowed paths.\n" "${file_path}" >&2
        return 1
    fi

    # Success case
    return 0
}

secure_compare() {
    local str1="${1}"
    local str2="${2}"
    if [ "${DEBUG}" -eq 1 ]; then
        printf "Debug: Current file_path: %s\n" "${file_path}"
        printf "Debug: Function secure_compare %s\n"
    fi

    if [[ ${#str1} -ne ${#str2} ]]; then
        return 1
    fi
    local result=0
    for (( i=0; i<${#str1}; i++ )); do
        result=$((result | $(printf '%d' "'${str1:$i:1}") ^ $(printf '%d' "'${str2:$i:1}")))
    done
    return ${result}

    # Success case
    return 0
}

check_file_is_text() {
    local file_path="${1}"
    if [ "${DEBUG}" -eq 1 ]; then
        printf "Debug: Current file_path: %s\n" "${file_path}"
        printf "Debug: Function check_file_is_text %s\n"
    fi

    if ! file -b --mime-type "${file_path}" | grep -q "^text/"; then
        printf "Error: File '%s' is not a text file.\n" "${file_path}" >&2
        return 1
    fi
    
    # Success case
    return 0
}

check_file_encoding() {
    local file_path="${1}"
    local desired_encoding="${2:-UTF-8}"  # Default to UTF-8 if not specified
    
    if [ "${DEBUG}" -eq 1 ]; then
        printf "Debug: Current file_path: %s\n" "${file_path}"
        printf "Debug: Function check_file_encoding\n"
        printf "Debug: Desired encoding: %s\n" "${desired_encoding}"
    fi

    local detected_encoding
    detected_encoding=$(file -i "${file_path}" | awk -F= '{print $2}')
    printf "Info: Detected encoding: %s\n" "${detected_encoding}"

    case "${desired_encoding}" in
        ASCII|ascii)
            if ! file -i "${file_path}" | grep -q "charset=us-ascii"; then
                printf "Error: File '%s' is not ASCII encoded.\n" "${file_path}" >&2
                return 1
            fi
            ;;
        UTF-8|utf-8)
            if ! file -i "${file_path}" | grep -q "charset=utf-8"; then
                printf "Error: File '%s' is not UTF-8 encoded.\n" "${file_path}" >&2
                return 1
            fi
            ;;
        UTF-16|utf-16)
            if ! file -i "${file_path}" | grep -q "charset=utf-16"; then
                printf "Error: File '%s' is not UTF-16 encoded.\n" "${file_path}" >&2
                return 1
            fi
            ;;
        UTF-32|utf-32)
            if ! file -i "${file_path}" | grep -q "charset=utf-32"; then
                printf "Error: File '%s' is not UTF-32 encoded.\n" "${file_path}" >&2
                return 1
            fi
            ;;
        EBCDIC|ebcdic)
            if ! file -i "${file_path}" | grep -q "charset=ebcdic"; then
                printf "Error: File '%s' is not EBCDIC encoded.\n" "${file_path}" >&2
                return 1
            fi
            ;;
        ISO-8859|iso-8859)
            if ! file -i "${file_path}" | grep -q "charset=iso-8859"; then
                printf "Error: File '%s' is not ISO-8859 encoded.\n" "${file_path}" >&2
                return 1
            fi
            ;;
        *)
            printf "Error: Unsupported encoding '%s'.\n" "${desired_encoding}" >&2
            return 1
            ;;
    esac
    
    # Success case
    return 0
}

# Function to check the MIME type and encoding of a file with file
check_file_encoding_with_file() {
    local file="${1}"
    local desired_encoding="${2:-UTF-8}"  # Default to UTF-8 if not specified
    
    if [ "${DEBUG}" -eq 1 ]; then
        printf "Debug: Current file: %s\n" "${file}"
        printf "Debug: Function check_file_encoding_with_file\n"
        printf "Debug: Desired encoding: %s\n" "${desired_encoding}"
    fi

    check_command "file" || return 1

    local mime_info
    mime_info=$(file --mime "${file}")
    printf "Info: MIME info for '%s': %s\n" "${file}" "${mime_info}"

    case "${desired_encoding}" in
    ASCII|ascii)
        if ! echo "${mime_info}" | grep -q "charset=us-ascii"; then
            printf "Error: File '%s' is not ASCII encoded.\n" "${file}" >&2
            return 1
        fi
        ;;
    UTF-8|utf-8)
        if ! echo "${mime_info}" | grep -q "charset=utf-8"; then
            printf "Error: File '%s' is not UTF-8 encoded.\n" "${file}" >&2
            return 1
        fi
        ;;
    UTF-16|utf-16)
        if ! echo "${mime_info}" | grep -q "charset=utf-16"; then
            printf "Error: File '%s' is not UTF-16 encoded.\n" "${file}" >&2
            return 1
        fi
        ;;
    UTF-32|utf-32)
        if ! echo "${mime_info}" | grep -q "charset=utf-32"; then
            printf "Error: File '%s' is not UTF-32 encoded.\n" "${file}" >&2
            return 1
        fi
        ;;
    EBCDIC|ebcdic)
        if ! echo "${mime_info}" | grep -q "charset=ebcdic"; then
            printf "Error: File '%s' is not EBCDIC encoded.\n" "${file}" >&2
            return 1
        fi
        ;;
    ISO-8859|iso-8859)
        if ! echo "${mime_info}" | grep -q "charset=iso-8859"; then
            printf "Error: File '%s' is not ISO-8859 encoded.\n" "${file}" >&2
            return 1
        fi
        ;;
    *)
        printf "Error: Unsupported encoding '%s'.\n" "${desired_encoding}" >&2
        return 1
        ;;
esac


    printf "Info: File '%s' has the desired encoding: %s\n" "${file}" "${desired_encoding}"
    return 0
}

check_line_length() {
    local file_path="${1}"
    if [ "${DEBUG}" -eq 1 ]; then
        printf "Debug: Current file_path: %s\n" "${file_path}"
        printf "Debug: Function check_line_length %s\n"
    fi

    local max_line_length="${2}"
    local long_lines
    long_lines=$(awk -v max="${max_line_length}" 'length > max' "${file_path}")
    if [[ -n "${long_lines}" ]]; then
        printf "Error: File '%s' contains lines longer than %d characters.\n" "${file_path}" "${max_line_length}" >&2
        return 1
    fi
    
    # Success case
    return 0
}

check_ascii_only() {
    local file_path="${1}"
    if [ "${DEBUG}" -eq 1 ]; then
        printf "Debug: Current file_path: %s\n" "${file_path}"
        printf "Debug: Function check_ascii_only %s\n"
    fi

    if LC_ALL=C grep -q '[^[:ascii:]]' "${file_path}"; then
        printf "Error: File '%s' contains non-ASCII characters.\n" "${file_path}" >&2
        return 1
    fi

    if file -i "${file_path}" | grep -q "charset=utf-8"; then
        printf "Warning: File '%s' may contain Unicode characters.\n" "${file_path}" >&2
        return 1
    fi

    # Success case
    return 0
}

check_unicode() {
    local file_path="${1}"
    if [ "${DEBUG}" -eq 1 ]; then
        printf "Debug: Current file_path: %s\n" "${file_path}"
        printf "Debug: Function check_unicode %s\n"
    fi

    # Check if the file exists
    if [[ ! -f "${file_path}" ]]; then
        printf "Error: File '%s' does not exist.\n" "${file_path}"
        return 1
    fi

    # Use grep to check for non-ASCII characters
    if grep -q '[^[:print:]]' "${file_path}"; then
        printf "Unicode character detected in file '%s'. Exiting with Unicode character 🄍.\n" "${file_path}"
        exit 1  # Exit with a non-zero status
    else
        printf "File '%s' contains only ASCII characters. Pass.\n" "${file_path}"
    fi
}

check_unwanted_characters() {
    local file_path="${1}"
    if [ "${DEBUG}" -eq 1 ]; then
        printf "Debug: Current file_path: %s\n" "${file_path}"
        printf "Debug: Function check_unwanted_characters %s\n"
    fi

    local unwanted_chars="[€ßäöüÄÖÜ]" # original "[?!€ßäöüÄÖÜ]"
    if grep -q "${unwanted_chars}" "${file_path}"; then
        printf "Error: File '%s' contains unwanted characters.\n" "${file_path}" >&2
        return 1
    fi
    
    # Success case
    return 0
}

source_file() {
    local file_path="${1}"
    if [ "${DEBUG}" -eq 1 ]; then
        printf "Debug: Current file_path: %s\n" "${file_path}"
        printf "Debug: Function source_file %s\n"
    fi

    check_file_exists "${file_path}" || return 1
    check_file_extension "${file_path}" || return 1
    check_file_readable "${file_path}" || return 1
    
    check_file_ownership "${file_path}" || return 1
    #check_modification_time "${file_path}" || return 1
    validate_file_hash "${file_path}" || return 1
    check_for_symlinks "${file_path}" || return 1
    validate_file_path "${file_path}" || return 1
    check_setuid_setgid "${file_path}" || return 1
    check_file_size "${file_path}" || return 1
    
    #exit 0

    #validate_file_structure "${file_path}" || return 1
    check_whitelist "${file_path}" || return 1
    #secure_compare "${file_path}" || return 1

    check_file_is_text "${file_path}" || return 1
    #check_file_encoding "${file_path}" || return 1

    # Usage example:
    #check_file_encoding "${file_path}" "UTF-8" || return 0
    #check_file_encoding "${file_path}" "UTF-16" || return 0
    #check_file_encoding "${file_path}" "UTF-32" || return 1
    #check_file_encoding "${file_path}" "EBCDIC" || return 1
    #check_file_encoding "${file_path}" "ISO-8859" || return 1
    #check_file_encoding "${file_path}" "ASCII" || return 1

    #check_file_encoding_with_file "${file_path}" "${default_encoding}" || return 1

    #exit 0

    check_line_length "${file_path}" "${max_line_length}" || return 1
    #check_ascii_only "${file_path}" "${max_line_length}" || return 1
    check_unicode "${file_path}" "${max_line_length}" || return 1
    check_unwanted_characters "${file_path}" "${max_line_length}" || return 1

    (
        set -e
        set -f
        set -o noglob
        set -o nounset
        . "${file_path}"
    ) || {
        printf "Error: Failed to source file '%s'.\n" "${file_path}" >&2
        return 1
    }

    printf "Info: Successfully sourced file '%s'.\n" "${file_path}"
    
    # Success case
    return 0
}

source_file "/${HOMEDIR}/${USERSCRIPT}/repos/initial_laptop_setup/scripts/functions_for_package_manager.cfg" 250

print_asterisks "${print_asterisks_value}"

# Function to copy files to VMs
copy_files_to_vms() {
    local full_path
    for vm_name in "${!vms[@]}"; do
        pushd "./vagrant/${vm_name}" 
        #> /dev/null || exit 1
        for file in "${required_files_for_vagrant[@]}"; do
            #printf "vagrant scp \"%s/%s\" \"%s:/tmp/%s\"\n" "${VAGRANT_SOURCE_DIR}" "${file}" "${vm_name}" "${file}"
            full_path="${VAGRANT_SOURCE_DIR}${file}"
            printf "Info: the following file will be processed: %s\n" "${full_path}"

            vagrant scp "${full_path}" "/tmp"

            #vagrant scp "${full_path}" "${vm_name}":"/tmp"

            # Directly check the exit status of the vagrant scp command
            if vagrant scp -v "${full_path}" "${vm_name}:/tmp"; then
                printf "Info: the following file will be processed 2: %s\n" "${full_path}"
                printf "Info: Successfully copied %s to %s\n" "${file}" "${vm_name}"
            else
                printf "Error: Failed to copy %s to %s\n" "${file}" "${vm_name}"
            fi
        done
        
        popd
        #> /dev/null || exit 1

    done

    # Success case
    return 0
}

# Function to remove files from VMs
remove_files_from_vms() {
    for vm_name in "${!vms[@]}"; do
        pushd "./vagrant/${vm_name}" > /dev/null || exit 1
        for file in "${required_files_for_vagrant[@]}"; do
            printf "vagrant ssh \"%s\" -c \"rm -f -v /tmp/%s\"\n" "${vm_name}" "${file}"
            vagrant ssh "${vm_name}" -c "rm -f -v /tmp/${file}"
            if [ $? -eq 0 ]; then
                printf "Info: Successfully removed %s from %s\n" "${file}" "${vm_name}"
            else
                printf "Error: Failed to remove %s from %s\n" "${file}" "${vm_name}"
            fi
        done
        
        popd > /dev/null || exit 1

    done

    # Success case
    return 0
}

# Function to make required files executable on all VMs
make_files_executable() {
    for vm_name in "${!vms[@]}"; do
        pushd "./vagrant/${vm_name}" > /dev/null || exit 1
        for file in "${executable_files_for_vagrant[@]}"; do
            if [[ -f "${VAGRANT_SOURCE_DIR}${file}" ]]; then
                printf "vagrant ssh -c \"chmod +x /tmp/%s\"\n" "${file}"
                vagrant ssh -c "chmod +x /tmp/${file}" && \
                printf "Info: Made %s executable on VM '%s'.\n" "${file}" "${vm_name}"
            else
                printf "Warning: File %s does not exist in source directory.\n" "${file}"
            fi
        done

        popd > /dev/null || exit 1

    done

    # Success case
    return 0
}

# Function to execute the required files on all VMs
execute_required_files() {
    for vm_name in "${!vms[@]}"; do
        pushd "./vagrant/${vm_name}" > /dev/null || exit 1
        for file in "${executable_files_for_vagrant[@]}"; do
            printf "vagrant ssh \"%s\" -c \"sudo /tmp/%s\"\n" "${vm_name}" "${file}"
            vagrant ssh "${vm_name}" -c "sudo /tmp/${file}" && \
            printf "Info: Executed %s on VM '%s'.\n" "${file}" "${vm_name}"
        done

        popd > /dev/null || exit 1

    done

    # Success case
    return 0
}

# Function to copy files from directory to VMs
copy_files_from_directory_to_vms() {
    local full_path="${VAGRANT_SOURCE_DIR_2}"
    for vm_name in "${!vms[@]}"; do
        if ! pushd "./vagrant/${vm_name}" > /dev/null; then
            printf "Error: Could not change to directory ./vagrant/%s\n" "${vm_name}" >&2
            return 1
        fi

        printf "Info: Copying files to %s from %s\n" "${vm_name}" "${full_path}"

        # Copy .sh files
        for file in "${full_path}"*.sh; do
            if [ -f "${file}" ]; then
                if vagrant scp "${file}" "/tmp"; then
                    printf "Info: Copied %s to %s:/tmp\n" "${file}" "${vm_name}"
                else
                    printf "Error: Could not copy %s to %s:/tmp\n" "${file}" "${vm_name}" >&2
                fi
            fi
        done

        # Copy .txt files
        for file in "${full_path}"*.txt; do
            if [ -f "${file}" ]; then
                if vagrant scp "${file}" "/tmp"; then
                    printf "Info: Copied %s to %s:/tmp\n" "${file}" "${vm_name}"
                else
                    printf "Error: Could not copy %s to %s:/tmp\n" "${file}" "${vm_name}" >&2
                fi
            fi
        done

        # Copy .sha3-512 files
        for file in "${full_path}"*.sha3-512; do
            if [ -f "${file}" ]; then
                if vagrant scp "${file}" "/tmp"; then
                    printf "Info: Copied %s to %s:/tmp\n" "${file}" "${vm_name}"
                else
                    printf "Error: Could not copy %s to %s:/tmp\n" "${file}" "${vm_name}" >&2
                fi
            fi
        done

        # Repeat for other file types if needed
        # For example, for .cfg files:
        for file in "${full_path}"*.cfg; do
            if [ -f "${file}" ]; then
                if vagrant scp "${file}" "/tmp"; then
                    printf "Info: Copied %s to %s:/tmp\n" "${file}" "${vm_name}"
                else
                    printf "Error: Could not copy %s to %s:/tmp\n" "${file}" "${vm_name}" >&2
                fi
            fi
        done

        popd > /dev/null || printf "Warning: popd failed\n" >&2

    done

    # Success case
    return 0
}

# Function to SSH into a specific VM
vagrant_ssh() {
    local vm_name="${1}"
    
    # Check if the VM name exists in the array
    if [[ -v vms["${vm_name}"] ]]; then
        printf "Info: Attempting to connect to VM: %s\n" "${vm_name}"
        if vagrant ssh "${vm_name}" -c "exit" > /dev/null 2>&1; then
            printf "Info: Successfully connected to VM: %s\n" "${vm_name}"
        else
            printf "Error: Unable to SSH into VM: %s\n" "${vm_name}"
        fi
    else
        printf "Error: VM '%s' not found.\n" "${vm_name}"
        printf "Info: Available VM through ssh: %s\n" "${!vms[@]}"
    fi

    # Success case
    return 0
}

# Function to create VMs
create_vms() {
    if [ "${DESTROY:-0}" -eq 1 ]; then
        printf "Info: DESTROY is set to 1. Destroying existing VMs...\n"

        for vm_name in "${!vms[@]}"; do
            if [ -d "./vagrant/${vm_name}" ]; then
                pushd "./vagrant/${vm_name}" > /dev/null || exit 1
                vagrant destroy -f || printf "Warning: Failed to destroy VM '%s'.\n" "${vm_name}"
                popd > /dev/null || exit 1
            fi
        done

        printf "Info: All existing VMs destroyed.\n"
    else
        printf "Info: DESTROY is not set to 1. Skipping VM destruction.\n"
    fi
    
    print_asterisks "${print_asterisks_value}"

    printf "Info: Creating VMs...\n"

    for vm_name in "${!vms[@]}"; do
        local box="${vms[${vm_name}]}"
        printf "Info: Setting up VM '%s' with box '%s'\n" "${vm_name}" "${box}"

        mkdir -p -v "./vagrant/${vm_name}"

        if [ "${vm_name}" == "sabayon" ]; then
            cat > "./vagrant/${vm_name}/Vagrantfile" <<EOF
Vagrant.configure("2") do |config|
  config.vm.box = "Sabayon/spinbase-amd64"

  config.vm.provision "shell", inline: <<-SHELL
    if command -v hostnamectl >/dev/null 2>&1; then
      hostnamectl set-hostname 'sabayon'
    else
      echo 'sabayon' > /etc/hostname
      hostname 'sabayon'
    fi
  SHELL
end
EOF
        else
            cat > "./vagrant/${vm_name}/Vagrantfile" <<EOF
Vagrant.configure("2") do |config|
  config.vm.box = "${box}"
  config.vm.hostname = "${vm_name}"
end
EOF
        fi

        pushd "./vagrant/${vm_name}" > /dev/null || exit 1
        vagrant up || { printf "Error: Failed to bring up VM '%s'.\n" "${vm_name}"; exit 1; }
        popd > /dev/null || exit 1

        printf "Info: VM '%s' is up and running.\n" "${vm_name}"
    done

    printf "Info: All VMs created successfully.\n"
    
    # Success case
    return 0
}

# Define packages to install
array_packages=(
    "wget"
    "curl"
    "git"
    "git-lfs"
    "ansible"
    "ansible-lint"
)
readonly array_packages
printf "Info: Packages for all VMs:\n"
printf "Info: Package: %s\n" "${array_packages[@]}"

print_asterisks "${print_asterisks_value}"

array_packages_automation_controller=(
    "apt-transport-https"
    "logrotate"
    "auditd"
    "ansible"
    "ansible-lint"
    "terraform"
)
readonly array_packages_automation_controller
printf "Info: Packages for Automation:\n"
printf "Info: Package: %s\n" "${array_packages_automation_controller[@]}"

print_asterisks "${print_asterisks_value}"

array_packages_job_controller=(
    "apt-transport-https"
    "logrotate"
    "auditd"
    "openjdk"
)
readonly array_packages_job_controller
printf "Info: Packages for Job Controller:\n"
printf "Info: Package: %s\n" "${array_packages_job_controller[@]}"

print_asterisks "${print_asterisks_value}"

install_packages() {
    local package_manager="$1"
    shift
    local packages=("$@")

    if [ "$package_manager" == "apt" ]; then
        export LC_ALL=C.UTF-8
        export LANG=C.UTF-8
        sudo apt update
        sudo apt install -y "${packages[@]}"
    fi
}

main_cleanup_package_manager() {
    for package_manager in "${array_possible_package_manager[@]}"; do
        if check_command "${package_manager}"; then
            printf "Info: %s is installed. Cleaning cache...\n" "${package_manager}"
            cleanup_package_manager "${package_manager}"
            "${package_manager}" -V  || true
        else
            printf "Info: %s is not installed. Skipping.\n" "${package_manager}"
        fi
    done
}

#install_packages brew ${array_packages[*]}

update_vm_packages_with_vagrant() {
    local vm_name="${1}"
    printf "Info: Current vm_name: %s\n" "${vm_name}"
    local package_manager_choice="${2}"
    printf "Info: Current package_manager_choice: %s\n" "${package_manager_choice}"

    # Navigate to the directory containing the Vagrantfile
    cd ~/vagrant/debian-automation-controller

    # Check the status of the VMs
    vagrant status

    # If the VM is not running, start it
    vagrant up

    # Pass the function definition and call it
    vagrant ssh "${vm_name}" -c "$(declare -f update_packages); update_packages ${package_manager_choice}" || {
        printf "Error: Failed to update/install packages in VM '%s'.\n" "${vm_name}"
        exit 1
    }

    printf "Info: Updated packages in VM '%s' using %s.\n" "${vm_name}" "${package_manager_choice}"
    return 0
}


# Main function
main() {
    
    create_vms
    
    print_asterisks "${print_asterisks_value}"

    if [ "${do_copy_files_to_vms}" -eq 1 ]; then
        printf "Info: Starting processing ...\n"
        copy_files_to_vms
    printf "Info: Finalized processing ...\n"
    fi

    if [ "${do_copy_files_from_directory_to_vms}" -eq 1 ]; then
        printf "Info: Starting processing ...\n"
        copy_files_from_directory_to_vms
    printf "Info: Finalized processing ...\n"
    fi

    if [ "${do_make_files_executable}" -eq 1 ]; then
        printf "Info: Starting processing ...\n"
        make_files_executable
    printf "Info: Finalized processing ...\n"
    fi

    for vm_name in "${!vms[@]}"; do
        printf "Info: Processing VM '%s'...\n" "${vm_name}"
        pushd "./vagrant/${vm_name}" 
        #> /dev/null || exit 1
        
        ls -la "/${HOMEDIR}/${USERSCRIPT}/vagrant/${vm_name}"

        #package_manager_choice="apt"
        #printf "Info: Current package_manager_choice: %s\n" "${package_manager_choice}"

        if [[ "${vm_name}" == "pacman" || "${vm_name}" == "arch" ]]; then
            package_manager_choice="pacman"
            printf "Info: Current package_manager_choice: %s\n" "${package_manager_choice}"

            update_vm_packages_with_vagrant "${vm_name}" "${package_manager_choice}"

        elif [ "${vm_name}" == "apt" ] || [ "${vm_name}" == "debian" ]; then
            package_manager_choice="apt"
            printf "Info: Current package_manager_choice: %s\n" "${package_manager_choice}"

            #vagrant ssh -c "sudo apt-get -y install apt-transport-https"

            vagrant ssh "${vm_name}" -c "$(declare -f update_packages); update_packages ${package_manager_choice}"
            #update_vm_packages_with_vagrant "${vm_name}" "${package_manager_choice}"

        elif [ "${vm_name}" == "debian-automation-controller" ]; then
            package_manager_choice="apt"
            printf "Info: Current package_manager_choice: %s\n" "${package_manager_choice}"

            vagrant ssh "${vm_name}" -c "$(declare -f update_packages); update_packages ${package_manager_choice}"
            #update_vm_packages_with_vagrant "${vm_name}" "${package_manager_choice}"

        elif [ "${vm_name}" == "debian-job-controller" ]; then
            package_manager_choice="apt"
            printf "Info: Current package_manager_choice: %s\n" "${package_manager_choice}"

            update_vm_packages_with_vagrant "${vm_name}" "${package_manager_choice}"

        elif [[ "${vm_name}" == "zypper" || "${vm_name}" == "opensuse" || "${vm_name}" == "sles11sp4" ]]; then
            package_manager_choice="zypper"
            printf "Info: Current package_manager_choice: %s\n" "${package_manager_choice}"

            update_vm_packages_with_vagrant "${vm_name}" "${package_manager_choice}"

        # Add more conditions for other package managers as needed

        fi

        
        #if ! update_vm_packages_with_vagrant "${vm_name}" "${package_manager_choice}"; then
        #    popd > /dev/null || exit 1
        #    return 1
        #fi

        # Continue with package installation and other tasks...

        popd > /dev/null || exit 1
        printf "Info: Processing completed for VM '%s'.\n" "${vm_name}"
    done

    remove_files_from_vms

    print_asterisks "${print_asterisks_value}"

    printf "Info: All tasks completed successfully.\n"

    print_asterisks "${print_asterisks_value}"

    # Success case
    return 0
}

main "$@"

vagrant global-status

#main_cleanup_package_manager

cleanup

log "ending script"

script_name1="$(basename "${0}")"
printf "\nInfo: script_name1: %s\n" "${script_name1}"
script_path1="$(realpath "$(dirname "${0}")")"
printf "Info: script_path1: %s\n" "${script_path1}"
script_path_with_name="${script_path1}/${script_name1}"
printf "Info: Script path with name: %s\n" "${script_path_with_name}"
printf "Info: Script finished\n"
exit 0
