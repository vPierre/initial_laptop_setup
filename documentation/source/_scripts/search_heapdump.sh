#!/usr/bin/env bash

# Author: Pierre Gronau <Pierre.Gronau@ndaal.eu>
# Copyright 2024
# License: All content is licensed under the terms of the <Apache 2.0>
# Developed on: Debian 12.x; macOS Sequoia x86 architecture
# Tested on: Debian 12.x; macOS Sequoia x86 architecture

set -o errexit
set -o errtrace
set -o nounset
set -o pipefail
# set -o xtrace
IFS=$'\n\t'

# Log function
log() {
    printf "%s - %s\n" "$(date '+%Y-%m-%d %H:%M:%S')" "${1}" | tee -a "${LOG_FILE}"
}

# Error trapping function
error_handler() {
    local line_number="${1}"
    local error_code="${2}"
    local last_command="${BASH_COMMAND}"
    printf "Error: Error occurred in line %s (error code: %s)\n" "${line_number}" "${error_code}"
    printf "Error: Failed command: %s\n" "${last_command}"
}

# Set the error trap
trap 'error_handler ${LINENO} $?' ERR

cleanup() {
    trap - SIGINT SIGTERM ERR EXIT
    printf "%b\n" "\nInfo: Cleanup is running ..."
    log "Info: Cleanup is running ..."
    # Additional cleanup tasks can be added here if needed
    rm -f -v ./*.tmp
    printf "%b\n" "\nInfo: Cleanup finished ..."
    log "Info: Cleanup finished ..."
}

trap cleanup SIGINT SIGTERM ERR EXIT

# Determine the home directory based on the OS
if [[ "$(uname)" == "Darwin" ]]; then
    HOMEDIR="Users"
elif [[ "$(uname)" == "Linux" ]]; then
    HOMEDIR="home"
else
    printf "Error: Unsupported operating system: %s\n" "$(uname)"
    exit 1
fi

readonly HOMEDIR

# Under Linux you use `home` under macOS `Users`
printf "Info: Home Directory: %s\n" "${HOMEDIR}"

USERSCRIPT="cloud"
readonly USERSCRIPT
printf "Info: User script: %s\n" "${USERSCRIPT}"

VERSION="0.5.0"
readonly VERSION
printf "Info: Current version: %s\n" "${VERSION}"

LOG_FILE="/${HOMEDIR}/${USERSCRIPT}/search_heapdump.log"
readonly LOG_FILE 
printf "Info: log file with path: %s\n" "${LOG_FILE}"
if [[ ! -f "${LOG_FILE}" ]]; then
    printf "Info: Log file does not exist. Creating it now.\n"
    touch "${LOG_FILE}" || {
        printf "Error: Failed to create log file at %s\n" "${LOG_FILE}"
        exit 1
    }
else
    printf "Info: Log file already exists at %s\n" "${LOG_FILE}"
fi

# Function to check if a command is available
check_command() {
    if ! command -v "${1}" &>/dev/null; then
        printf "Error: %s is not installed or not in PATH. Please install it and try again.\n" "${1}"
        return 1
    fi
    return 0
}

# Check for required commands
check_command "rg" || exit 1

# Define the directory to search in and the log file
SEARCH_DIR="${1:-.}"  # Default to current directory if no argument is provided

# Ensure the search directory is a valid directory
if [ ! -d "${SEARCH_DIR}" ]; then
    printf "Error: The specified search directory '%s' is not valid.\n" "${SEARCH_DIR}"
    exit 1
fi

# Search for "enabled" or "disabled" related to actuator heapdump
printf "Info: Searching for 'enabled' or 'disabled' actuator heapdump in directory: %s\n" "${SEARCH_DIR}"

# Perform the search and save results to the log file
if rg -i "heapdump.*(enabled|disabled)" "${SEARCH_DIR}" | tee -a "${LOG_FILE}"; then
    printf "Info: Search completed successfully.\n"
else
    printf "Error: Search encountered an error.\n"
    exit 1
fi

printf "Info: All tasks completed successfully.\n"

cleanup

script_name1="$(basename "${0}")"
printf "\nInfo: script_name1: %s\n" "${script_name1}"
script_path1="$(realpath "$(dirname "${0}")")"
printf "Info: script_path1: %s\n" "${script_path1}"
script_path_with_name="${script_path1}/${script_name1}"
printf "Info: Script path with name: %s\n" "${script_path_with_name}"
printf "Info: Script finished\n"
exit 0
