#!/usr/bin/env bash
# https://privacy.sexy — v0.13.7 — Sun, 29 Dec 2024 18:07:22 GMT
if [ "$EUID" -ne 0 ]; then
  script_path=$([[ "$0" = /* ]] && echo "$0" || echo "$PWD/${0#./}")
  sudo "$script_path" || (
    echo 'Administrator privileges are required.'
    exit 1
  )
  exit 0
fi
export HOME="/home/${SUDO_USER:-${USER}}" # Keep `~` and `$HOME` for user not `/root`.


# ----------------------------------------------------------
# -----------------------Empty trash------------------------
# ----------------------------------------------------------
echo '--- Empty trash'
# Empty global trash
rm -rfv ~/.local/share/Trash/*
sudo rm -rfv /root/.local/share/Trash/*
# Empty Snap trash
rm -rfv ~/snap/*/*/.local/share/Trash/*
# Empty Flatpak trash (apps may not choose to use Portal API)
rm -rfv ~/.var/app/*/data/Trash/*
# ----------------------------------------------------------


# ----------------------------------------------------------
# --------------Clear global temporary folders--------------
# ----------------------------------------------------------
echo '--- Clear global temporary folders'
sudo rm -rfv /tmp/*
sudo rm -rfv /var/tmp/*
# ----------------------------------------------------------


# ----------------------------------------------------------
# --------------------Clear screenshots---------------------
# ----------------------------------------------------------
echo '--- Clear screenshots'
# Clear default directory for GNOME screenshots
rm -rfv ~/Pictures/Screenshots/*
if [ -d ~/Pictures ]; then
  # Clear Ubuntu screenshots
  find ~/Pictures -name 'Screenshot from *.png' | while read -r file_path; do
    rm -fv "$file_path" # E.g. Screenshot from 2022-08-20 02-46-41.png
  done
  # Clear KDE (Spectatle) screenshots
  find ~/Pictures -name 'Screenshot_*' | while read -r file_path; do
    rm -fv "$file_path" # E.g. Screenshot_20220927_205646.png
  done
fi
# Clear ksnip screenshots
find ~ -name 'ksnip_*' | while read -r file_path; do
  rm -fv "$file_path" # E.g. ksnip_20220927-195151.png
done
# ----------------------------------------------------------


# Disable connectivity checks (breaks Captive Portal detection)
echo '--- Disable connectivity checks (breaks Captive Portal detection)'
if ! command -v '/usr/sbin/NetworkManager' &> /dev/null; then
  echo 'Skipping because "/usr/sbin/NetworkManager" is not found.'
else
  file='/etc/NetworkManager/conf.d/20-disable-connectivity-privacy-sexy.conf'
content=$'# Created by privacy.sexy\n[connectivity]\nenabled=false'
directory="${file%/*}"
mkdir -p "$directory"
if [ -f "$file" ]; then
  echo "Skipping, connectivity checks are already disabled through $file."
else
  echo -n "$content" | sudo tee "$file" > /dev/null
  echo 'Successfully disabled connectivity checks.'
fi
if command -v 'nmcli' &> /dev/null; then
  sudo nmcli general reload
  echo 'Successfully reloaded configuration.'
else
  echo 'It will take effect after reboot.'
fi
fi
# ----------------------------------------------------------


# ----------------------------------------------------------
# --------------------Clear bash history--------------------
# ----------------------------------------------------------
echo '--- Clear bash history'
rm -fv ~/.bash_history
sudo rm -fv /root/.bash_history
# ----------------------------------------------------------


# ----------------------------------------------------------
# --------------------Clear Zsh history---------------------
# ----------------------------------------------------------
echo '--- Clear Zsh history'
rm -fv ~/.zsh_history
sudo rm -fv /root/.zsh_history
# ----------------------------------------------------------


# ----------------------------------------------------------
# --------------------Clear tcsh history--------------------
# ----------------------------------------------------------
echo '--- Clear tcsh history'
rm -fv ~/.history
sudo rm -fv /root/.history
# ----------------------------------------------------------


# ----------------------------------------------------------
# --------------------Clear fish history--------------------
# ----------------------------------------------------------
echo '--- Clear fish history'
rm -fv ~/.local/share/fish/fish_history
sudo rm -fv /root/.local/share/fish/fish_history
rm -fv ~/.config/fish/fish_history
sudo rm -fv /root/.config/fish/fish_history
# ----------------------------------------------------------


# ----------------------------------------------------------
# --------------Clear KornShell (ksh) history---------------
# ----------------------------------------------------------
echo '--- Clear KornShell (ksh) history'
rm -fv ~/.sh_history
sudo rm -fv /root/.sh_history
# ----------------------------------------------------------


# ----------------------------------------------------------
# --------------------Clear ash history---------------------
# ----------------------------------------------------------
echo '--- Clear ash history'
rm -fv ~/.ash_history
sudo rm -fv /root/.ash_history
# ----------------------------------------------------------


# ----------------------------------------------------------
# -------------------Clear crosh history--------------------
# ----------------------------------------------------------
echo '--- Clear crosh history'
rm -fv ~/.crosh_history
sudo rm -fv /root/.crosh_history
# ----------------------------------------------------------


# ----------------------------------------------------------
# --------------------Clear Steam cache---------------------
# ----------------------------------------------------------
echo '--- Clear Steam cache'
# Global installation
rm -rfv ~/.local/share/Steam/appcache/*
# Snap
rm -rfv ~/snap/steam/common/.cache/*
rm -rfv ~/snap/steam/common/.local/share/Steam/appcache/*
# Flatpak
rm -rfv ~/.var/app/com.valvesoftware.Steam/cache/*
rm -rfv ~/.var/app/com.valvesoftware.Steam/data/Steam/appcache/*
# ----------------------------------------------------------


# ----------------------------------------------------------
# ------------------Clear Clementine cache------------------
# ----------------------------------------------------------
echo '--- Clear Clementine cache'
# Global installation
rm -rfv ~/.cache/Clementine/*
# Flatpak installation
rm -rfv ~/.var/app/org.clementine_player.Clementine/cache/*
# Snap installation
rm -rfv ~/snap/clementine/common/.cache/*
# ----------------------------------------------------------


# ----------------------------------------------------------
# -------------Clear LibreOffice usage history--------------
# ----------------------------------------------------------
echo '--- Clear LibreOffice usage history'
# Global installation
rm -f ~/.config/libreoffice/4/user/registrymodifications.xcu
# Snap package
rm -fv ~/snap/libreoffice/*/.config/libreoffice/4/user/registrymodifications.xcu
# Flatpak installation
rm -fv ~/.var/app/org.libreoffice.LibreOffice/config/libreoffice/4/user/registrymodifications.xcu
# ----------------------------------------------------------


# ----------------------------------------------------------
# -------------Clear system crash report files--------------
# ----------------------------------------------------------
echo '--- Clear system crash report files'
sudo rm -rfv /var/crash/*
sudo rm -rfv /var/lib/systemd/coredump/
# ----------------------------------------------------------


# ----------------------------------------------------------
# --------------Clear system logs (`journald`)--------------
# ----------------------------------------------------------
echo '--- Clear system logs (`journald`)'
if ! command -v 'journalctl' &> /dev/null; then
  echo 'Skipping because "journalctl" is not found.'
else
  sudo journalctl --vacuum-time=1s
fi
sudo rm -rfv /run/log/journal/*
sudo rm -rfv /var/log/journal/*
# ----------------------------------------------------------


# ----------------------------------------------------------
# -----------Clear Zeitgeist data (activity logs)-----------
# ----------------------------------------------------------
echo '--- Clear Zeitgeist data (activity logs)'
sudo rm -rfv {/root,/home/*}/.local/share/zeitgeist
# ----------------------------------------------------------


# ----------------------------------------------------------
# ------------Clear GTK recently used files list------------
# ----------------------------------------------------------
echo '--- Clear GTK recently used files list'
# From global installations
rm -fv /.recently-used.xbel
rm -fv ~/.local/share/recently-used.xbel*
# From snap packages
rm -fv ~/snap/*/*/.local/share/recently-used.xbel
# From Flatpak packages
rm -fv ~/.var/app/*/data/recently-used.xbel
# ----------------------------------------------------------


# ----------------------------------------------------------
# --------Clear KDE-tracked recently used items list--------
# ----------------------------------------------------------
echo '--- Clear KDE-tracked recently used items list'
# From global installations
rm -rfv ~/.local/share/RecentDocuments/*.desktop
rm -rfv ~/.kde/share/apps/RecentDocuments/*.desktop
rm -rfv ~/.kde4/share/apps/RecentDocuments/*.desktop
# From snap packages
rm -fv ~/snap/*/*/.local/share/*.desktop
# From Flatpak packages
rm -rfv ~/.var/app/*/data/*.desktop
# ----------------------------------------------------------


# ----------------------------------------------------------
# --------------------Clear YUM/RPM data--------------------
# ----------------------------------------------------------
echo '--- Clear YUM/RPM data'
if ! command -v 'yum' &> /dev/null; then
  echo 'Skipping because "yum" is not found.'
else
  yum clean all --enablerepo='*'
fi
# ----------------------------------------------------------


# ----------------------------------------------------------
# --------------------Clear DNF/RPM data--------------------
# ----------------------------------------------------------
echo '--- Clear DNF/RPM data'
if ! command -v 'dnf' &> /dev/null; then
  echo 'Skipping because "dnf" is not found.'
else
  dnf clean all --enablerepo='*'
fi
# ----------------------------------------------------------


# ----------------------------------------------------------
# ----------------Clear user-specific cache-----------------
# ----------------------------------------------------------
echo '--- Clear user-specific cache'
rm -rfv ~/.cache/*
sudo rm -rfv root/.cache/*
# ----------------------------------------------------------


# ----------------------------------------------------------
# -----------------Clear system-wide cache------------------
# ----------------------------------------------------------
echo '--- Clear system-wide cache'
rm -rf /var/cache/*
# ----------------------------------------------------------


# ----------------------------------------------------------
# -------------Clear Flatpak application cache--------------
# ----------------------------------------------------------
echo '--- Clear Flatpak application cache'
rm -rfv ~/.var/app/*/cache/*
# ----------------------------------------------------------


# ----------------------------------------------------------
# ---------------Clear Snap application cache---------------
# ----------------------------------------------------------
echo '--- Clear Snap application cache'
rm -fv ~/snap/*/*/.cache/*
# ----------------------------------------------------------


# ----------------------------------------------------------
# --------------Clear thumbnails (icon cache)---------------
# ----------------------------------------------------------
echo '--- Clear thumbnails (icon cache)'
rm -rfv ~/.thumbnails/*
rm -rfv ~/.cache/thumbnails/*
# ----------------------------------------------------------


# ----------------------------------------------------------
# ------------Clear privacy.sexy script history-------------
# ----------------------------------------------------------
echo '--- Clear privacy.sexy script history'
# Clear directory contents: "$HOME/.config/privacy.sexy/runs"
glob_pattern="$HOME/.config/privacy.sexy/runs/*"
rm -rfv $glob_pattern
# ----------------------------------------------------------


# ----------------------------------------------------------
# -------------Clear privacy.sexy activity logs-------------
# ----------------------------------------------------------
echo '--- Clear privacy.sexy activity logs'
# Clear directory contents: "$HOME/.config/privacy.sexy/logs"
glob_pattern="$HOME/.config/privacy.sexy/logs/*"
rm -rfv $glob_pattern
# ----------------------------------------------------------


# ----------------------------------------------------------
# ---------------------Clear Wine cache---------------------
# ----------------------------------------------------------
echo '--- Clear Wine cache'
# Temporary Windows files for global prefix
rm -rfv ~/.wine/drive_c/windows/temp/*
# Wine cache:
rm -rfv ~/.cache/wine/
# ----------------------------------------------------------


# ----------------------------------------------------------
# ------------------Clear Winetricks cache------------------
# ----------------------------------------------------------
echo '--- Clear Winetricks cache'
rm -rfv ~/.cache/winetricks/
# ----------------------------------------------------------


# ----------------------------------------------------------
# ----------------Clear Thunderbird cookies-----------------
# ----------------------------------------------------------
echo '--- Clear Thunderbird cookies'
# cookies.sqlite: Global installation
rm -fv ~/.thunderbird/*/cookies.sqlite
# cookies.sqlite: Flatpak installation
rm -fv ~/snap/thunderbird/common/.thunderbird/*/cookies.sqlite
# cookies.sqlite: Snap installation
rm -fv ~/.var/app/org.mozilla.Thunderbird/.thunderbird/*/cookies.sqlite
# cookies.sqlite-wal: Global installation
rm -fv ~/.thunderbird/*/cookies.sqlite-wal
# cookies.sqlite-wal: Flatpak installation
rm -fv ~/snap/thunderbird/common/.thunderbird/*/cookies.sqlite-wal
# cookies.sqlite-wal: Snap installation
rm -fv ~/.var/app/org.mozilla.Thunderbird/.thunderbird/*/cookies.sqlite-wal
# cookies.sqlite-journal: Global installation
rm -fv ~/.thunderbird/*/cookies.sqlite-journal
# cookies.sqlite-journal: Flatpak installation
rm -fv ~/snap/thunderbird/common/.thunderbird/*/cookies.sqlite-journal
# cookies.sqlite-journal: Snap installation
rm -fv ~/.var/app/org.mozilla.Thunderbird/.thunderbird/*/cookies.sqlite-journal
# cookies.sqlite-shm: Global installation
rm -fv ~/.thunderbird/*/cookies.sqlite-shm
# cookies.sqlite-shm: Flatpak installation
rm -fv ~/snap/thunderbird/common/.thunderbird/*/cookies.sqlite-shm
# cookies.sqlite-shm: Snap installation
rm -fv ~/.var/app/org.mozilla.Thunderbird/.thunderbird/*/cookies.sqlite-shm
# ----------------------------------------------------------


# Clear Thunderbird session restoration data (open windows and tabs)
echo '--- Clear Thunderbird session restoration data (open windows and tabs)'
# session.json: Global installation
rm -fv ~/.thunderbird/*/session.json
# session.json: Flatpak installation
rm -fv ~/snap/thunderbird/common/.thunderbird/*/session.json
# session.json: Snap installation
rm -fv ~/.var/app/org.mozilla.Thunderbird/.thunderbird/*/session.json
# ----------------------------------------------------------


# ----------------------------------------------------------
# ---------------Clear Thunderbird passwords----------------
# ----------------------------------------------------------
echo '--- Clear Thunderbird passwords'
# logins.json: Global installation
rm -fv ~/.thunderbird/*/logins.json
# logins.json: Flatpak installation
rm -fv ~/snap/thunderbird/common/.thunderbird/*/logins.json
# logins.json: Snap installation
rm -fv ~/.var/app/org.mozilla.Thunderbird/.thunderbird/*/logins.json
# logins-backup.json: Global installation
rm -fv ~/.thunderbird/*/logins-backup.json
# logins-backup.json: Flatpak installation
rm -fv ~/snap/thunderbird/common/.thunderbird/*/logins-backup.json
# logins-backup.json: Snap installation
rm -fv ~/.var/app/org.mozilla.Thunderbird/.thunderbird/*/logins-backup.json
# ----------------------------------------------------------


# ----------------------------------------------------------
# ------------Clear Thunderbird download history------------
# ----------------------------------------------------------
echo '--- Clear Thunderbird download history'
# downloads.rdf: Global installation
rm -fv ~/.thunderbird/*/downloads.rdf
# downloads.rdf: Flatpak installation
rm -fv ~/snap/thunderbird/common/.thunderbird/*/downloads.rdf
# downloads.rdf: Snap installation
rm -fv ~/.var/app/org.mozilla.Thunderbird/.thunderbird/*/downloads.rdf
# downloads.sqlite: Global installation
rm -fv ~/.thunderbird/*/downloads.sqlite
# downloads.sqlite: Flatpak installation
rm -fv ~/snap/thunderbird/common/.thunderbird/*/downloads.sqlite
# downloads.sqlite: Snap installation
rm -fv ~/.var/app/org.mozilla.Thunderbird/.thunderbird/*/downloads.sqlite
# ----------------------------------------------------------


# ----------------------------------------------------------
# --------Clear clicked links history in Thunderbird--------
# ----------------------------------------------------------
echo '--- Clear clicked links history in Thunderbird'
# places.sqlite: Global installation
rm -fv ~/.thunderbird/*/places.sqlite
# places.sqlite: Flatpak installation
rm -fv ~/snap/thunderbird/common/.thunderbird/*/places.sqlite
# places.sqlite: Snap installation
rm -fv ~/.var/app/org.mozilla.Thunderbird/.thunderbird/*/places.sqlite
# places.sqlite-shm: Global installation
rm -fv ~/.thunderbird/*/places.sqlite-shm
# places.sqlite-shm: Flatpak installation
rm -fv ~/snap/thunderbird/common/.thunderbird/*/places.sqlite-shm
# places.sqlite-shm: Snap installation
rm -fv ~/.var/app/org.mozilla.Thunderbird/.thunderbird/*/places.sqlite-shm
# places.sqlite.wal: Global installation
rm -fv ~/.thunderbird/*/places.sqlite.wal
# places.sqlite.wal: Flatpak installation
rm -fv ~/snap/thunderbird/common/.thunderbird/*/places.sqlite.wal
# places.sqlite.wal: Snap installation
rm -fv ~/.var/app/org.mozilla.Thunderbird/.thunderbird/*/places.sqlite.wal
# ----------------------------------------------------------


# ----------------------------------------------------------
# -------------------Clear Python history-------------------
# ----------------------------------------------------------
echo '--- Clear Python history'
rm -fv ~/.python_history
sudo rm -fv /root/.python_history
# ----------------------------------------------------------


# ----------------------------------------------------------
# ---------Clear Thunderbird personal address book----------
# ----------------------------------------------------------
echo '--- Clear Thunderbird personal address book'
# abook.sqlite: Global installation
rm -fv ~/.thunderbird/*/abook.sqlite
# abook.sqlite: Flatpak installation
rm -fv ~/snap/thunderbird/common/.thunderbird/*/abook.sqlite
# abook.sqlite: Snap installation
rm -fv ~/.var/app/org.mozilla.Thunderbird/.thunderbird/*/abook.sqlite
# abook.sqlite-wal: Global installation
rm -fv ~/.thunderbird/*/abook.sqlite-wal
# abook.sqlite-wal: Flatpak installation
rm -fv ~/snap/thunderbird/common/.thunderbird/*/abook.sqlite-wal
# abook.sqlite-wal: Snap installation
rm -fv ~/.var/app/org.mozilla.Thunderbird/.thunderbird/*/abook.sqlite-wal
# abook.mab: Global installation
rm -fv ~/.thunderbird/*/abook.mab
# abook.mab: Flatpak installation
rm -fv ~/snap/thunderbird/common/.thunderbird/*/abook.mab
# abook.mab: Snap installation
rm -fv ~/.var/app/org.mozilla.Thunderbird/.thunderbird/*/abook.mab
# ----------------------------------------------------------


# ----------------------------------------------------------
# ---------Clear Thunderbird collected address book---------
# ----------------------------------------------------------
echo '--- Clear Thunderbird collected address book'
# history.sqlite: Global installation
rm -fv ~/.thunderbird/*/history.sqlite
# history.sqlite: Flatpak installation
rm -fv ~/snap/thunderbird/common/.thunderbird/*/history.sqlite
# history.sqlite: Snap installation
rm -fv ~/.var/app/org.mozilla.Thunderbird/.thunderbird/*/history.sqlite
# history.sqlite-wal: Global installation
rm -fv ~/.thunderbird/*/history.sqlite-wal
# history.sqlite-wal: Flatpak installation
rm -fv ~/snap/thunderbird/common/.thunderbird/*/history.sqlite-wal
# history.sqlite-wal: Snap installation
rm -fv ~/.var/app/org.mozilla.Thunderbird/.thunderbird/*/history.sqlite-wal
# history.mab: Global installation
rm -fv ~/.thunderbird/*/history.mab
# history.mab: Flatpak installation
rm -fv ~/snap/thunderbird/common/.thunderbird/*/history.mab
# history.mab: Snap installation
rm -fv ~/.var/app/org.mozilla.Thunderbird/.thunderbird/*/history.mab
# ----------------------------------------------------------


# ----------------------------------------------------------
# ----------Clear Visual Studio Code crash reports----------
# ----------------------------------------------------------
echo '--- Clear Visual Studio Code crash reports'
# Crash\ Reports: Global installation (also Snap with --classic)
rm -rfv ~/.config/Code/Crash\ Reports/*
# Crash\ Reports: Flatpak installation
rm -rfv ~/.var/app/com.visualstudio.code/config/Code/Crash\ Reports/*
# exthost\ Crash\ Reports: Global installation (also Snap with --classic)
rm -rfv ~/.config/Code/exthost\ Crash\ Reports/*
# exthost\ Crash\ Reports: Flatpak installation
rm -rfv ~/.var/app/com.visualstudio.code/config/Code/exthost\ Crash\ Reports/*
# ----------------------------------------------------------


# ----------------------------------------------------------
# --------------Clear Visual Studio Code cache--------------
# ----------------------------------------------------------
echo '--- Clear Visual Studio Code cache'
# Cache: Global installation (also Snap with --classic)
rm -rfv ~/.config/Code/Cache/*
# Cache: Flatpak installation
rm -rfv ~/.var/app/com.visualstudio.code/config/Code/Cache/*
# CachedData: Global installation (also Snap with --classic)
rm -rfv ~/.config/Code/CachedData/*
# CachedData: Flatpak installation
rm -rfv ~/.var/app/com.visualstudio.code/config/Code/CachedData/*
# Code\ Cache: Global installation (also Snap with --classic)
rm -rfv ~/.config/Code/Code\ Cache/*
# Code\ Cache: Flatpak installation
rm -rfv ~/.var/app/com.visualstudio.code/config/Code/Code\ Cache/*
# GPUCache: Global installation (also Snap with --classic)
rm -rfv ~/.config/Code/GPUCache/*
# GPUCache: Flatpak installation
rm -rfv ~/.var/app/com.visualstudio.code/config/Code/GPUCache/*
# CachedExtensions: Global installation (also Snap with --classic)
rm -rfv ~/.config/Code/CachedExtensions/*
# CachedExtensions: Flatpak installation
rm -rfv ~/.var/app/com.visualstudio.code/config/Code/CachedExtensions/*
# CachedExtensionVSIXs: Global installation (also Snap with --classic)
rm -rfv ~/.config/Code/CachedExtensionVSIXs/*
# CachedExtensionVSIXs: Flatpak installation
rm -rfv ~/.var/app/com.visualstudio.code/config/Code/CachedExtensionVSIXs/*
# ----------------------------------------------------------


# ----------------------------------------------------------
# --------------Clear Visual Studio Code logs---------------
# ----------------------------------------------------------
echo '--- Clear Visual Studio Code logs'
# logs: Global installation (also Snap with --classic)
rm -rfv ~/.config/Code/logs/*
# logs: Flatpak installation
rm -rfv ~/.var/app/com.visualstudio.code/config/Code/logs/*
# ----------------------------------------------------------


# ----------------------------------------------------------
# --------------Clear Azure CLI telemetry data--------------
# ----------------------------------------------------------
echo '--- Clear Azure CLI telemetry data'
rm -rfv ~/.azure/telemetry
rm -fv ~/.azure/telemetry.txt
rm -fv ~/.azure/logs/telemetry.txt
# ----------------------------------------------------------


# ----------------------------------------------------------
# -------------------Clear Azure CLI logs-------------------
# ----------------------------------------------------------
echo '--- Clear Azure CLI logs'
rm -rfv ~/.azure/logs
# ----------------------------------------------------------


# ----------------------------------------------------------
# ------------------Clear Azure CLI cache-------------------
# ----------------------------------------------------------
echo '--- Clear Azure CLI cache'
if ! command -v 'az' &> /dev/null; then
  echo 'Skipping because "az" is not found.'
else
  az cache purge
fi
# ----------------------------------------------------------


# Clear Azure login data (this will log you out of the current session)
echo '--- Clear Azure login data (this will log you out of the current session)'
if ! command -v 'az' &> /dev/null; then
  echo 'Skipping because "az" is not found.'
else
  az logout 2&> /dev/null
az account clear
rm -fv ~/.azure/accessTokens.json
fi
# ----------------------------------------------------------


# ----------------------------------------------------------
# ------------------Clear GNOME Web cache-------------------
# ----------------------------------------------------------
echo '--- Clear GNOME Web cache'
# Global installation
rm -rfv /.cache/epiphany/*
# Flatpak installation
rm -rfv ~/.var/app/org.gnome.Epiphany/cache/*
# Snap installation
rm -rfv ~/~/snap/epiphany/common/.cache/*
# ----------------------------------------------------------


# ----------------------------------------------------------
# -------------Clear GNOME Web browsing history-------------
# ----------------------------------------------------------
echo '--- Clear GNOME Web browsing history'
# ephy-history.db: Global installation
rm -fv ~/.local/share/epiphany/ephy-history.db
# ephy-history.db: Flatpak installation
rm -fv ~/.var/app/org.gnome.Epiphany/data/epiphany/ephy-history.db
# ephy-history.db: Snap installation
rm -fv ~/snap/epiphany/*/.local/share/epiphany/ephy-history.db
# ephy-history.db-shm: Global installation
rm -fv ~/.local/share/epiphany/ephy-history.db-shm
# ephy-history.db-shm: Flatpak installation
rm -fv ~/.var/app/org.gnome.Epiphany/data/epiphany/ephy-history.db-shm
# ephy-history.db-shm: Snap installation
rm -fv ~/snap/epiphany/*/.local/share/epiphany/ephy-history.db-shm
# ephy-history.db-wal: Global installation
rm -fv ~/.local/share/epiphany/ephy-history.db-wal
# ephy-history.db-wal: Flatpak installation
rm -fv ~/.var/app/org.gnome.Epiphany/data/epiphany/ephy-history.db-wal
# ephy-history.db-wal: Snap installation
rm -fv ~/snap/epiphany/*/.local/share/epiphany/ephy-history.db-wal
# ----------------------------------------------------------


# ----------------------------------------------------------
# -----------------Clear GNOME Web cookies------------------
# ----------------------------------------------------------
echo '--- Clear GNOME Web cookies'
# cookies.sqlite: Global installation
rm -fv ~/.local/share/epiphany/cookies.sqlite
# cookies.sqlite: Flatpak installation
rm -fv ~/.var/app/org.gnome.Epiphany/data/epiphany/cookies.sqlite
# cookies.sqlite: Snap installation
rm -fv ~/snap/epiphany/*/.local/share/epiphany/cookies.sqlite
# ----------------------------------------------------------


# ----------------------------------------------------------
# ----------------Clear GNOME Web bookmarks-----------------
# ----------------------------------------------------------
echo '--- Clear GNOME Web bookmarks'
# bookmarks.gvdb: Global installation
rm -fv ~/.local/share/epiphany/bookmarks.gvdb
# bookmarks.gvdb: Flatpak installation
rm -fv ~/.var/app/org.gnome.Epiphany/data/epiphany/bookmarks.gvdb
# bookmarks.gvdb: Snap installation
rm -fv ~/snap/epiphany/*/.local/share/epiphany/bookmarks.gvdb
# ----------------------------------------------------------


# ----------------------------------------------------------
# -------------------Clear Firefox cache--------------------
# ----------------------------------------------------------
echo '--- Clear Firefox cache'
# Global installation
rm -rfv ~/.cache/mozilla/*
# Flatpak installation
rm -rfv ~/.var/app/org.mozilla.firefox/cache/*
# Snap installation
rm -rfv ~/snap/firefox/common/.cache/*
# ----------------------------------------------------------


# ----------------------------------------------------------
# ---------------Clear Firefox crash reports----------------
# ----------------------------------------------------------
echo '--- Clear Firefox crash reports'
# Global installation
rm -fv ~/.mozilla/firefox/Crash\ Reports/*
# Flatpak installation
rm -rfv ~/.var/app/org.mozilla.firefox/.mozilla/firefox/Crash\ Reports/*
# Snap installation
rm -rfv ~/snap/firefox/common/.mozilla/firefox/Crash\ Reports/*
# Delete files matching pattern: "~/.mozilla/firefox/*/crashes/*"
if ! command -v 'python3' &> /dev/null; then
  echo 'Skipping because "python3" is not found.'
else
  python3 <<EOF
import glob
import os
path = '~/.mozilla/firefox/*/crashes/*'
expanded_path = os.path.expandvars(os.path.expanduser(path))
print(f'Deleting files matching pattern: {expanded_path}')
paths = glob.glob(expanded_path)
if not paths:
  print('Skipping, no paths found.')
for path in paths:
  if not os.path.isfile(path):
    print(f'Skipping folder: "{path}".')
    continue
  os.remove(path)
  print(f'Successfully delete file: "{path}".')
print(f'Successfully deleted {len(paths)} file(s).')
EOF
fi
# Delete files matching pattern: "~/.var/app/org.mozilla.firefox/.mozilla/firefox/*/crashes/*"
if ! command -v 'python3' &> /dev/null; then
  echo 'Skipping because "python3" is not found.'
else
  python3 <<EOF
import glob
import os
path = '~/.var/app/org.mozilla.firefox/.mozilla/firefox/*/crashes/*'
expanded_path = os.path.expandvars(os.path.expanduser(path))
print(f'Deleting files matching pattern: {expanded_path}')
paths = glob.glob(expanded_path)
if not paths:
  print('Skipping, no paths found.')
for path in paths:
  if not os.path.isfile(path):
    print(f'Skipping folder: "{path}".')
    continue
  os.remove(path)
  print(f'Successfully delete file: "{path}".')
print(f'Successfully deleted {len(paths)} file(s).')
EOF
fi
# Delete files matching pattern: "~/snap/firefox/common/.mozilla/firefox/*/crashes/*"
if ! command -v 'python3' &> /dev/null; then
  echo 'Skipping because "python3" is not found.'
else
  python3 <<EOF
import glob
import os
path = '~/snap/firefox/common/.mozilla/firefox/*/crashes/*'
expanded_path = os.path.expandvars(os.path.expanduser(path))
print(f'Deleting files matching pattern: {expanded_path}')
paths = glob.glob(expanded_path)
if not paths:
  print('Skipping, no paths found.')
for path in paths:
  if not os.path.isfile(path):
    print(f'Skipping folder: "{path}".')
    continue
  os.remove(path)
  print(f'Successfully delete file: "{path}".')
print(f'Successfully deleted {len(paths)} file(s).')
EOF
fi
# Delete files matching pattern: "~/.mozilla/firefox/*/crashes/events/*"
if ! command -v 'python3' &> /dev/null; then
  echo 'Skipping because "python3" is not found.'
else
  python3 <<EOF
import glob
import os
path = '~/.mozilla/firefox/*/crashes/events/*'
expanded_path = os.path.expandvars(os.path.expanduser(path))
print(f'Deleting files matching pattern: {expanded_path}')
paths = glob.glob(expanded_path)
if not paths:
  print('Skipping, no paths found.')
for path in paths:
  if not os.path.isfile(path):
    print(f'Skipping folder: "{path}".')
    continue
  os.remove(path)
  print(f'Successfully delete file: "{path}".')
print(f'Successfully deleted {len(paths)} file(s).')
EOF
fi
# Delete files matching pattern: "~/.var/app/org.mozilla.firefox/.mozilla/firefox/*/crashes/events/*"
if ! command -v 'python3' &> /dev/null; then
  echo 'Skipping because "python3" is not found.'
else
  python3 <<EOF
import glob
import os
path = '~/.var/app/org.mozilla.firefox/.mozilla/firefox/*/crashes/events/*'
expanded_path = os.path.expandvars(os.path.expanduser(path))
print(f'Deleting files matching pattern: {expanded_path}')
paths = glob.glob(expanded_path)
if not paths:
  print('Skipping, no paths found.')
for path in paths:
  if not os.path.isfile(path):
    print(f'Skipping folder: "{path}".')
    continue
  os.remove(path)
  print(f'Successfully delete file: "{path}".')
print(f'Successfully deleted {len(paths)} file(s).')
EOF
fi
# Delete files matching pattern: "~/snap/firefox/common/.mozilla/firefox/*/crashes/events/*"
if ! command -v 'python3' &> /dev/null; then
  echo 'Skipping because "python3" is not found.'
else
  python3 <<EOF
import glob
import os
path = '~/snap/firefox/common/.mozilla/firefox/*/crashes/events/*'
expanded_path = os.path.expandvars(os.path.expanduser(path))
print(f'Deleting files matching pattern: {expanded_path}')
paths = glob.glob(expanded_path)
if not paths:
  print('Skipping, no paths found.')
for path in paths:
  if not os.path.isfile(path):
    print(f'Skipping folder: "{path}".')
    continue
  os.remove(path)
  print(f'Successfully delete file: "{path}".')
print(f'Successfully deleted {len(paths)} file(s).')
EOF
fi
# ----------------------------------------------------------


# ----------------------------------------------------------
# ------------------Clear Firefox cookies-------------------
# ----------------------------------------------------------
echo '--- Clear Firefox cookies'
# Delete files matching pattern: "~/.mozilla/firefox/*/cookies.sqlite"
if ! command -v 'python3' &> /dev/null; then
  echo 'Skipping because "python3" is not found.'
else
  python3 <<EOF
import glob
import os
path = '~/.mozilla/firefox/*/cookies.sqlite'
expanded_path = os.path.expandvars(os.path.expanduser(path))
print(f'Deleting files matching pattern: {expanded_path}')
paths = glob.glob(expanded_path)
if not paths:
  print('Skipping, no paths found.')
for path in paths:
  if not os.path.isfile(path):
    print(f'Skipping folder: "{path}".')
    continue
  os.remove(path)
  print(f'Successfully delete file: "{path}".')
print(f'Successfully deleted {len(paths)} file(s).')
EOF
fi
# Delete files matching pattern: "~/.var/app/org.mozilla.firefox/.mozilla/firefox/*/cookies.sqlite"
if ! command -v 'python3' &> /dev/null; then
  echo 'Skipping because "python3" is not found.'
else
  python3 <<EOF
import glob
import os
path = '~/.var/app/org.mozilla.firefox/.mozilla/firefox/*/cookies.sqlite'
expanded_path = os.path.expandvars(os.path.expanduser(path))
print(f'Deleting files matching pattern: {expanded_path}')
paths = glob.glob(expanded_path)
if not paths:
  print('Skipping, no paths found.')
for path in paths:
  if not os.path.isfile(path):
    print(f'Skipping folder: "{path}".')
    continue
  os.remove(path)
  print(f'Successfully delete file: "{path}".')
print(f'Successfully deleted {len(paths)} file(s).')
EOF
fi
# Delete files matching pattern: "~/snap/firefox/common/.mozilla/firefox/*/cookies.sqlite"
if ! command -v 'python3' &> /dev/null; then
  echo 'Skipping because "python3" is not found.'
else
  python3 <<EOF
import glob
import os
path = '~/snap/firefox/common/.mozilla/firefox/*/cookies.sqlite'
expanded_path = os.path.expandvars(os.path.expanduser(path))
print(f'Deleting files matching pattern: {expanded_path}')
paths = glob.glob(expanded_path)
if not paths:
  print('Skipping, no paths found.')
for path in paths:
  if not os.path.isfile(path):
    print(f'Skipping folder: "{path}".')
    continue
  os.remove(path)
  print(f'Successfully delete file: "{path}".')
print(f'Successfully deleted {len(paths)} file(s).')
EOF
fi
# ----------------------------------------------------------


# Clear Firefox browsing history (URLs, downloads, bookmarks, visits, etc.)
echo '--- Clear Firefox browsing history (URLs, downloads, bookmarks, visits, etc.)'
# Delete files matching pattern: "~/.mozilla/firefox/*/downloads.rdf"
if ! command -v 'python3' &> /dev/null; then
  echo 'Skipping because "python3" is not found.'
else
  python3 <<EOF
import glob
import os
path = '~/.mozilla/firefox/*/downloads.rdf'
expanded_path = os.path.expandvars(os.path.expanduser(path))
print(f'Deleting files matching pattern: {expanded_path}')
paths = glob.glob(expanded_path)
if not paths:
  print('Skipping, no paths found.')
for path in paths:
  if not os.path.isfile(path):
    print(f'Skipping folder: "{path}".')
    continue
  os.remove(path)
  print(f'Successfully delete file: "{path}".')
print(f'Successfully deleted {len(paths)} file(s).')
EOF
fi
# Delete files matching pattern: "~/.var/app/org.mozilla.firefox/.mozilla/firefox/*/downloads.rdf"
if ! command -v 'python3' &> /dev/null; then
  echo 'Skipping because "python3" is not found.'
else
  python3 <<EOF
import glob
import os
path = '~/.var/app/org.mozilla.firefox/.mozilla/firefox/*/downloads.rdf'
expanded_path = os.path.expandvars(os.path.expanduser(path))
print(f'Deleting files matching pattern: {expanded_path}')
paths = glob.glob(expanded_path)
if not paths:
  print('Skipping, no paths found.')
for path in paths:
  if not os.path.isfile(path):
    print(f'Skipping folder: "{path}".')
    continue
  os.remove(path)
  print(f'Successfully delete file: "{path}".')
print(f'Successfully deleted {len(paths)} file(s).')
EOF
fi
# Delete files matching pattern: "~/snap/firefox/common/.mozilla/firefox/*/downloads.rdf"
if ! command -v 'python3' &> /dev/null; then
  echo 'Skipping because "python3" is not found.'
else
  python3 <<EOF
import glob
import os
path = '~/snap/firefox/common/.mozilla/firefox/*/downloads.rdf'
expanded_path = os.path.expandvars(os.path.expanduser(path))
print(f'Deleting files matching pattern: {expanded_path}')
paths = glob.glob(expanded_path)
if not paths:
  print('Skipping, no paths found.')
for path in paths:
  if not os.path.isfile(path):
    print(f'Skipping folder: "{path}".')
    continue
  os.remove(path)
  print(f'Successfully delete file: "{path}".')
print(f'Successfully deleted {len(paths)} file(s).')
EOF
fi
# Delete files matching pattern: "~/.mozilla/firefox/*/downloads.sqlite"
if ! command -v 'python3' &> /dev/null; then
  echo 'Skipping because "python3" is not found.'
else
  python3 <<EOF
import glob
import os
path = '~/.mozilla/firefox/*/downloads.sqlite'
expanded_path = os.path.expandvars(os.path.expanduser(path))
print(f'Deleting files matching pattern: {expanded_path}')
paths = glob.glob(expanded_path)
if not paths:
  print('Skipping, no paths found.')
for path in paths:
  if not os.path.isfile(path):
    print(f'Skipping folder: "{path}".')
    continue
  os.remove(path)
  print(f'Successfully delete file: "{path}".')
print(f'Successfully deleted {len(paths)} file(s).')
EOF
fi
# Delete files matching pattern: "~/.var/app/org.mozilla.firefox/.mozilla/firefox/*/downloads.sqlite"
if ! command -v 'python3' &> /dev/null; then
  echo 'Skipping because "python3" is not found.'
else
  python3 <<EOF
import glob
import os
path = '~/.var/app/org.mozilla.firefox/.mozilla/firefox/*/downloads.sqlite'
expanded_path = os.path.expandvars(os.path.expanduser(path))
print(f'Deleting files matching pattern: {expanded_path}')
paths = glob.glob(expanded_path)
if not paths:
  print('Skipping, no paths found.')
for path in paths:
  if not os.path.isfile(path):
    print(f'Skipping folder: "{path}".')
    continue
  os.remove(path)
  print(f'Successfully delete file: "{path}".')
print(f'Successfully deleted {len(paths)} file(s).')
EOF
fi
# Delete files matching pattern: "~/snap/firefox/common/.mozilla/firefox/*/downloads.sqlite"
if ! command -v 'python3' &> /dev/null; then
  echo 'Skipping because "python3" is not found.'
else
  python3 <<EOF
import glob
import os
path = '~/snap/firefox/common/.mozilla/firefox/*/downloads.sqlite'
expanded_path = os.path.expandvars(os.path.expanduser(path))
print(f'Deleting files matching pattern: {expanded_path}')
paths = glob.glob(expanded_path)
if not paths:
  print('Skipping, no paths found.')
for path in paths:
  if not os.path.isfile(path):
    print(f'Skipping folder: "{path}".')
    continue
  os.remove(path)
  print(f'Successfully delete file: "{path}".')
print(f'Successfully deleted {len(paths)} file(s).')
EOF
fi
# Delete files matching pattern: "~/.mozilla/firefox/*/places.sqlite"
if ! command -v 'python3' &> /dev/null; then
  echo 'Skipping because "python3" is not found.'
else
  python3 <<EOF
import glob
import os
path = '~/.mozilla/firefox/*/places.sqlite'
expanded_path = os.path.expandvars(os.path.expanduser(path))
print(f'Deleting files matching pattern: {expanded_path}')
paths = glob.glob(expanded_path)
if not paths:
  print('Skipping, no paths found.')
for path in paths:
  if not os.path.isfile(path):
    print(f'Skipping folder: "{path}".')
    continue
  os.remove(path)
  print(f'Successfully delete file: "{path}".')
print(f'Successfully deleted {len(paths)} file(s).')
EOF
fi
# Delete files matching pattern: "~/.var/app/org.mozilla.firefox/.mozilla/firefox/*/places.sqlite"
if ! command -v 'python3' &> /dev/null; then
  echo 'Skipping because "python3" is not found.'
else
  python3 <<EOF
import glob
import os
path = '~/.var/app/org.mozilla.firefox/.mozilla/firefox/*/places.sqlite'
expanded_path = os.path.expandvars(os.path.expanduser(path))
print(f'Deleting files matching pattern: {expanded_path}')
paths = glob.glob(expanded_path)
if not paths:
  print('Skipping, no paths found.')
for path in paths:
  if not os.path.isfile(path):
    print(f'Skipping folder: "{path}".')
    continue
  os.remove(path)
  print(f'Successfully delete file: "{path}".')
print(f'Successfully deleted {len(paths)} file(s).')
EOF
fi
# Delete files matching pattern: "~/snap/firefox/common/.mozilla/firefox/*/places.sqlite"
if ! command -v 'python3' &> /dev/null; then
  echo 'Skipping because "python3" is not found.'
else
  python3 <<EOF
import glob
import os
path = '~/snap/firefox/common/.mozilla/firefox/*/places.sqlite'
expanded_path = os.path.expandvars(os.path.expanduser(path))
print(f'Deleting files matching pattern: {expanded_path}')
paths = glob.glob(expanded_path)
if not paths:
  print('Skipping, no paths found.')
for path in paths:
  if not os.path.isfile(path):
    print(f'Skipping folder: "{path}".')
    continue
  os.remove(path)
  print(f'Successfully delete file: "{path}".')
print(f'Successfully deleted {len(paths)} file(s).')
EOF
fi
# Delete files matching pattern: "~/.mozilla/firefox/*/favicons.sqlite"
if ! command -v 'python3' &> /dev/null; then
  echo 'Skipping because "python3" is not found.'
else
  python3 <<EOF
import glob
import os
path = '~/.mozilla/firefox/*/favicons.sqlite'
expanded_path = os.path.expandvars(os.path.expanduser(path))
print(f'Deleting files matching pattern: {expanded_path}')
paths = glob.glob(expanded_path)
if not paths:
  print('Skipping, no paths found.')
for path in paths:
  if not os.path.isfile(path):
    print(f'Skipping folder: "{path}".')
    continue
  os.remove(path)
  print(f'Successfully delete file: "{path}".')
print(f'Successfully deleted {len(paths)} file(s).')
EOF
fi
# Delete files matching pattern: "~/.var/app/org.mozilla.firefox/.mozilla/firefox/*/favicons.sqlite"
if ! command -v 'python3' &> /dev/null; then
  echo 'Skipping because "python3" is not found.'
else
  python3 <<EOF
import glob
import os
path = '~/.var/app/org.mozilla.firefox/.mozilla/firefox/*/favicons.sqlite'
expanded_path = os.path.expandvars(os.path.expanduser(path))
print(f'Deleting files matching pattern: {expanded_path}')
paths = glob.glob(expanded_path)
if not paths:
  print('Skipping, no paths found.')
for path in paths:
  if not os.path.isfile(path):
    print(f'Skipping folder: "{path}".')
    continue
  os.remove(path)
  print(f'Successfully delete file: "{path}".')
print(f'Successfully deleted {len(paths)} file(s).')
EOF
fi
# Delete files matching pattern: "~/snap/firefox/common/.mozilla/firefox/*/favicons.sqlite"
if ! command -v 'python3' &> /dev/null; then
  echo 'Skipping because "python3" is not found.'
else
  python3 <<EOF
import glob
import os
path = '~/snap/firefox/common/.mozilla/firefox/*/favicons.sqlite'
expanded_path = os.path.expandvars(os.path.expanduser(path))
print(f'Deleting files matching pattern: {expanded_path}')
paths = glob.glob(expanded_path)
if not paths:
  print('Skipping, no paths found.')
for path in paths:
  if not os.path.isfile(path):
    print(f'Skipping folder: "{path}".')
    continue
  os.remove(path)
  print(f'Successfully delete file: "{path}".')
print(f'Successfully deleted {len(paths)} file(s).')
EOF
fi
# ----------------------------------------------------------


# ----------------------------------------------------------
# -------------------Clear Firefox logins-------------------
# ----------------------------------------------------------
echo '--- Clear Firefox logins'
# Delete files matching pattern: "~/.mozilla/firefox/*/logins.json"
if ! command -v 'python3' &> /dev/null; then
  echo 'Skipping because "python3" is not found.'
else
  python3 <<EOF
import glob
import os
path = '~/.mozilla/firefox/*/logins.json'
expanded_path = os.path.expandvars(os.path.expanduser(path))
print(f'Deleting files matching pattern: {expanded_path}')
paths = glob.glob(expanded_path)
if not paths:
  print('Skipping, no paths found.')
for path in paths:
  if not os.path.isfile(path):
    print(f'Skipping folder: "{path}".')
    continue
  os.remove(path)
  print(f'Successfully delete file: "{path}".')
print(f'Successfully deleted {len(paths)} file(s).')
EOF
fi
# Delete files matching pattern: "~/.var/app/org.mozilla.firefox/.mozilla/firefox/*/logins.json"
if ! command -v 'python3' &> /dev/null; then
  echo 'Skipping because "python3" is not found.'
else
  python3 <<EOF
import glob
import os
path = '~/.var/app/org.mozilla.firefox/.mozilla/firefox/*/logins.json'
expanded_path = os.path.expandvars(os.path.expanduser(path))
print(f'Deleting files matching pattern: {expanded_path}')
paths = glob.glob(expanded_path)
if not paths:
  print('Skipping, no paths found.')
for path in paths:
  if not os.path.isfile(path):
    print(f'Skipping folder: "{path}".')
    continue
  os.remove(path)
  print(f'Successfully delete file: "{path}".')
print(f'Successfully deleted {len(paths)} file(s).')
EOF
fi
# Delete files matching pattern: "~/snap/firefox/common/.mozilla/firefox/*/logins.json"
if ! command -v 'python3' &> /dev/null; then
  echo 'Skipping because "python3" is not found.'
else
  python3 <<EOF
import glob
import os
path = '~/snap/firefox/common/.mozilla/firefox/*/logins.json'
expanded_path = os.path.expandvars(os.path.expanduser(path))
print(f'Deleting files matching pattern: {expanded_path}')
paths = glob.glob(expanded_path)
if not paths:
  print('Skipping, no paths found.')
for path in paths:
  if not os.path.isfile(path):
    print(f'Skipping folder: "{path}".')
    continue
  os.remove(path)
  print(f'Successfully delete file: "{path}".')
print(f'Successfully deleted {len(paths)} file(s).')
EOF
fi
# Delete files matching pattern: "~/.mozilla/firefox/*/logins-backup.json"
if ! command -v 'python3' &> /dev/null; then
  echo 'Skipping because "python3" is not found.'
else
  python3 <<EOF
import glob
import os
path = '~/.mozilla/firefox/*/logins-backup.json'
expanded_path = os.path.expandvars(os.path.expanduser(path))
print(f'Deleting files matching pattern: {expanded_path}')
paths = glob.glob(expanded_path)
if not paths:
  print('Skipping, no paths found.')
for path in paths:
  if not os.path.isfile(path):
    print(f'Skipping folder: "{path}".')
    continue
  os.remove(path)
  print(f'Successfully delete file: "{path}".')
print(f'Successfully deleted {len(paths)} file(s).')
EOF
fi
# Delete files matching pattern: "~/.var/app/org.mozilla.firefox/.mozilla/firefox/*/logins-backup.json"
if ! command -v 'python3' &> /dev/null; then
  echo 'Skipping because "python3" is not found.'
else
  python3 <<EOF
import glob
import os
path = '~/.var/app/org.mozilla.firefox/.mozilla/firefox/*/logins-backup.json'
expanded_path = os.path.expandvars(os.path.expanduser(path))
print(f'Deleting files matching pattern: {expanded_path}')
paths = glob.glob(expanded_path)
if not paths:
  print('Skipping, no paths found.')
for path in paths:
  if not os.path.isfile(path):
    print(f'Skipping folder: "{path}".')
    continue
  os.remove(path)
  print(f'Successfully delete file: "{path}".')
print(f'Successfully deleted {len(paths)} file(s).')
EOF
fi
# Delete files matching pattern: "~/snap/firefox/common/.mozilla/firefox/*/logins-backup.json"
if ! command -v 'python3' &> /dev/null; then
  echo 'Skipping because "python3" is not found.'
else
  python3 <<EOF
import glob
import os
path = '~/snap/firefox/common/.mozilla/firefox/*/logins-backup.json'
expanded_path = os.path.expandvars(os.path.expanduser(path))
print(f'Deleting files matching pattern: {expanded_path}')
paths = glob.glob(expanded_path)
if not paths:
  print('Skipping, no paths found.')
for path in paths:
  if not os.path.isfile(path):
    print(f'Skipping folder: "{path}".')
    continue
  os.remove(path)
  print(f'Successfully delete file: "{path}".')
print(f'Successfully deleted {len(paths)} file(s).')
EOF
fi
# Delete files matching pattern: "~/.mozilla/firefox/*/signons.sqlite"
if ! command -v 'python3' &> /dev/null; then
  echo 'Skipping because "python3" is not found.'
else
  python3 <<EOF
import glob
import os
path = '~/.mozilla/firefox/*/signons.sqlite'
expanded_path = os.path.expandvars(os.path.expanduser(path))
print(f'Deleting files matching pattern: {expanded_path}')
paths = glob.glob(expanded_path)
if not paths:
  print('Skipping, no paths found.')
for path in paths:
  if not os.path.isfile(path):
    print(f'Skipping folder: "{path}".')
    continue
  os.remove(path)
  print(f'Successfully delete file: "{path}".')
print(f'Successfully deleted {len(paths)} file(s).')
EOF
fi
# Delete files matching pattern: "~/.var/app/org.mozilla.firefox/.mozilla/firefox/*/signons.sqlite"
if ! command -v 'python3' &> /dev/null; then
  echo 'Skipping because "python3" is not found.'
else
  python3 <<EOF
import glob
import os
path = '~/.var/app/org.mozilla.firefox/.mozilla/firefox/*/signons.sqlite'
expanded_path = os.path.expandvars(os.path.expanduser(path))
print(f'Deleting files matching pattern: {expanded_path}')
paths = glob.glob(expanded_path)
if not paths:
  print('Skipping, no paths found.')
for path in paths:
  if not os.path.isfile(path):
    print(f'Skipping folder: "{path}".')
    continue
  os.remove(path)
  print(f'Successfully delete file: "{path}".')
print(f'Successfully deleted {len(paths)} file(s).')
EOF
fi
# Delete files matching pattern: "~/snap/firefox/common/.mozilla/firefox/*/signons.sqlite"
if ! command -v 'python3' &> /dev/null; then
  echo 'Skipping because "python3" is not found.'
else
  python3 <<EOF
import glob
import os
path = '~/snap/firefox/common/.mozilla/firefox/*/signons.sqlite'
expanded_path = os.path.expandvars(os.path.expanduser(path))
print(f'Deleting files matching pattern: {expanded_path}')
paths = glob.glob(expanded_path)
if not paths:
  print('Skipping, no paths found.')
for path in paths:
  if not os.path.isfile(path):
    print(f'Skipping folder: "{path}".')
    continue
  os.remove(path)
  print(f'Successfully delete file: "{path}".')
print(f'Successfully deleted {len(paths)} file(s).')
EOF
fi
# ----------------------------------------------------------


# ----------------------------------------------------------
# ------------Clear Firefox autocomplete history------------
# ----------------------------------------------------------
echo '--- Clear Firefox autocomplete history'
# Delete files matching pattern: "~/.mozilla/firefox/*/formhistory.sqlite"
if ! command -v 'python3' &> /dev/null; then
  echo 'Skipping because "python3" is not found.'
else
  python3 <<EOF
import glob
import os
path = '~/.mozilla/firefox/*/formhistory.sqlite'
expanded_path = os.path.expandvars(os.path.expanduser(path))
print(f'Deleting files matching pattern: {expanded_path}')
paths = glob.glob(expanded_path)
if not paths:
  print('Skipping, no paths found.')
for path in paths:
  if not os.path.isfile(path):
    print(f'Skipping folder: "{path}".')
    continue
  os.remove(path)
  print(f'Successfully delete file: "{path}".')
print(f'Successfully deleted {len(paths)} file(s).')
EOF
fi
# Delete files matching pattern: "~/.var/app/org.mozilla.firefox/.mozilla/firefox/*/formhistory.sqlite"
if ! command -v 'python3' &> /dev/null; then
  echo 'Skipping because "python3" is not found.'
else
  python3 <<EOF
import glob
import os
path = '~/.var/app/org.mozilla.firefox/.mozilla/firefox/*/formhistory.sqlite'
expanded_path = os.path.expandvars(os.path.expanduser(path))
print(f'Deleting files matching pattern: {expanded_path}')
paths = glob.glob(expanded_path)
if not paths:
  print('Skipping, no paths found.')
for path in paths:
  if not os.path.isfile(path):
    print(f'Skipping folder: "{path}".')
    continue
  os.remove(path)
  print(f'Successfully delete file: "{path}".')
print(f'Successfully deleted {len(paths)} file(s).')
EOF
fi
# Delete files matching pattern: "~/snap/firefox/common/.mozilla/firefox/*/formhistory.sqlite"
if ! command -v 'python3' &> /dev/null; then
  echo 'Skipping because "python3" is not found.'
else
  python3 <<EOF
import glob
import os
path = '~/snap/firefox/common/.mozilla/firefox/*/formhistory.sqlite'
expanded_path = os.path.expandvars(os.path.expanduser(path))
print(f'Deleting files matching pattern: {expanded_path}')
paths = glob.glob(expanded_path)
if not paths:
  print('Skipping, no paths found.')
for path in paths:
  if not os.path.isfile(path):
    print(f'Skipping folder: "{path}".')
    continue
  os.remove(path)
  print(f'Successfully delete file: "{path}".')
print(f'Successfully deleted {len(paths)} file(s).')
EOF
fi
# ----------------------------------------------------------


# ----------------------------------------------------------
# ------Clear Firefox "Multi-Account Containers" data-------
# ----------------------------------------------------------
echo '--- Clear Firefox "Multi-Account Containers" data'
# Delete files matching pattern: "~/.mozilla/firefox/*/containers.json"
if ! command -v 'python3' &> /dev/null; then
  echo 'Skipping because "python3" is not found.'
else
  python3 <<EOF
import glob
import os
path = '~/.mozilla/firefox/*/containers.json'
expanded_path = os.path.expandvars(os.path.expanduser(path))
print(f'Deleting files matching pattern: {expanded_path}')
paths = glob.glob(expanded_path)
if not paths:
  print('Skipping, no paths found.')
for path in paths:
  if not os.path.isfile(path):
    print(f'Skipping folder: "{path}".')
    continue
  os.remove(path)
  print(f'Successfully delete file: "{path}".')
print(f'Successfully deleted {len(paths)} file(s).')
EOF
fi
# Delete files matching pattern: "~/.var/app/org.mozilla.firefox/.mozilla/firefox/*/containers.json"
if ! command -v 'python3' &> /dev/null; then
  echo 'Skipping because "python3" is not found.'
else
  python3 <<EOF
import glob
import os
path = '~/.var/app/org.mozilla.firefox/.mozilla/firefox/*/containers.json'
expanded_path = os.path.expandvars(os.path.expanduser(path))
print(f'Deleting files matching pattern: {expanded_path}')
paths = glob.glob(expanded_path)
if not paths:
  print('Skipping, no paths found.')
for path in paths:
  if not os.path.isfile(path):
    print(f'Skipping folder: "{path}".')
    continue
  os.remove(path)
  print(f'Successfully delete file: "{path}".')
print(f'Successfully deleted {len(paths)} file(s).')
EOF
fi
# Delete files matching pattern: "~/snap/firefox/common/.mozilla/firefox/*/containers.json"
if ! command -v 'python3' &> /dev/null; then
  echo 'Skipping because "python3" is not found.'
else
  python3 <<EOF
import glob
import os
path = '~/snap/firefox/common/.mozilla/firefox/*/containers.json'
expanded_path = os.path.expandvars(os.path.expanduser(path))
print(f'Deleting files matching pattern: {expanded_path}')
paths = glob.glob(expanded_path)
if not paths:
  print('Skipping, no paths found.')
for path in paths:
  if not os.path.isfile(path):
    print(f'Skipping folder: "{path}".')
    continue
  os.remove(path)
  print(f'Successfully delete file: "{path}".')
print(f'Successfully deleted {len(paths)} file(s).')
EOF
fi
# ----------------------------------------------------------


# ----------------------------------------------------------
# ---------Clear Firefox open tabs and windows data---------
# ----------------------------------------------------------
echo '--- Clear Firefox open tabs and windows data'
# Delete files matching pattern: "~/.mozilla/firefox/*/sessionstore.jsonlz4"
if ! command -v 'python3' &> /dev/null; then
  echo 'Skipping because "python3" is not found.'
else
  python3 <<EOF
import glob
import os
path = '~/.mozilla/firefox/*/sessionstore.jsonlz4'
expanded_path = os.path.expandvars(os.path.expanduser(path))
print(f'Deleting files matching pattern: {expanded_path}')
paths = glob.glob(expanded_path)
if not paths:
  print('Skipping, no paths found.')
for path in paths:
  if not os.path.isfile(path):
    print(f'Skipping folder: "{path}".')
    continue
  os.remove(path)
  print(f'Successfully delete file: "{path}".')
print(f'Successfully deleted {len(paths)} file(s).')
EOF
fi
# Delete files matching pattern: "~/.var/app/org.mozilla.firefox/.mozilla/firefox/*/sessionstore.jsonlz4"
if ! command -v 'python3' &> /dev/null; then
  echo 'Skipping because "python3" is not found.'
else
  python3 <<EOF
import glob
import os
path = '~/.var/app/org.mozilla.firefox/.mozilla/firefox/*/sessionstore.jsonlz4'
expanded_path = os.path.expandvars(os.path.expanduser(path))
print(f'Deleting files matching pattern: {expanded_path}')
paths = glob.glob(expanded_path)
if not paths:
  print('Skipping, no paths found.')
for path in paths:
  if not os.path.isfile(path):
    print(f'Skipping folder: "{path}".')
    continue
  os.remove(path)
  print(f'Successfully delete file: "{path}".')
print(f'Successfully deleted {len(paths)} file(s).')
EOF
fi
# Delete files matching pattern: "~/snap/firefox/common/.mozilla/firefox/*/sessionstore.jsonlz4"
if ! command -v 'python3' &> /dev/null; then
  echo 'Skipping because "python3" is not found.'
else
  python3 <<EOF
import glob
import os
path = '~/snap/firefox/common/.mozilla/firefox/*/sessionstore.jsonlz4'
expanded_path = os.path.expandvars(os.path.expanduser(path))
print(f'Deleting files matching pattern: {expanded_path}')
paths = glob.glob(expanded_path)
if not paths:
  print('Skipping, no paths found.')
for path in paths:
  if not os.path.isfile(path):
    print(f'Skipping folder: "{path}".')
    continue
  os.remove(path)
  print(f'Successfully delete file: "{path}".')
print(f'Successfully deleted {len(paths)} file(s).')
EOF
fi
# ----------------------------------------------------------


# ----------------------------------------------------------
# ---------------------Clear Snap cache---------------------
# ----------------------------------------------------------
echo '--- Clear Snap cache'
sudo rm -rfv /var/lib/snapd/cache/*
# ----------------------------------------------------------


# ----------------------------------------------------------
# -----------------Remove old Snap packages-----------------
# ----------------------------------------------------------
echo '--- Remove old Snap packages'
if ! command -v 'snap' &> /dev/null; then
  echo 'Skipping because "snap" is not found.'
else
  snap list --all | while read name version rev tracking publisher notes; do
  if [[ $notes = *disabled* ]]; then
    sudo snap remove "$name" --revision="$rev";
  fi
done
fi
# ----------------------------------------------------------


# ----------------------------------------------------------
# -------------Remove orphaned Flatpak runtimes-------------
# ----------------------------------------------------------
echo '--- Remove orphaned Flatpak runtimes'
if ! command -v 'flatpak' &> /dev/null; then
  echo 'Skipping because "flatpak" is not found.'
else
  flatpak uninstall --unused --noninteractive
fi
# ----------------------------------------------------------


# ----------------------------------------------------------
# -------------------Clear Flatpak cache--------------------
# ----------------------------------------------------------
echo '--- Clear Flatpak cache'
# Temporary cache
sudo rm -rfv /var/tmp/flatpak-cache-*
# New cache
rm -rfv ~/.cache/flatpak/system-cache/*
# Old cache
rm -rfv ~/.local/share/flatpak/system-cache/*
# ----------------------------------------------------------


# ----------------------------------------------------------
# ---------------Clear obsolete APT packages----------------
# ----------------------------------------------------------
echo '--- Clear obsolete APT packages'
if ! command -v 'apt-get' &> /dev/null; then
  echo 'Skipping because "apt-get" is not found.'
else
  sudo apt-get autoclean
fi
# ----------------------------------------------------------


# ----------------------------------------------------------
# ---------------Clear APT package file lists---------------
# ----------------------------------------------------------
echo '--- Clear APT package file lists'
sudo rm -rfv /var/lib/apt/lists/*
# ----------------------------------------------------------


# ----------------------------------------------------------
# ---------Clear orphaned APT package dependencies----------
# ----------------------------------------------------------
echo '--- Clear orphaned APT package dependencies'
if ! command -v 'apt-get' &> /dev/null; then
  echo 'Skipping because "apt-get" is not found.'
else
  sudo apt-get -y autoremove --purge
fi
# ----------------------------------------------------------


# ----------------------------------------------------------
# ---------------Clear cache for APT packages---------------
# ----------------------------------------------------------
echo '--- Clear cache for APT packages'
if ! command -v 'apt-get' &> /dev/null; then
  echo 'Skipping because "apt-get" is not found.'
else
  sudo apt-get clean
fi
# ----------------------------------------------------------


# ----------------------------------------------------------
# --------------Disable Zorin OS census pings---------------
# ----------------------------------------------------------
echo '--- Disable Zorin OS census pings'
if ! command -v 'apt-get' &> /dev/null; then
  echo 'Skipping because "apt-get" is not found.'
else
  apt_package_name='zorin-os-census'
if status="$(dpkg-query -W --showformat='${db:Status-Status}' "$apt_package_name" 2>&1)" \
    && [ "$status" = installed ]; then
  echo "\"$apt_package_name\" is installed and will be uninstalled."
  sudo apt-get purge -y "$apt_package_name"
else
  echo "Skipping, no action needed, \"$apt_package_name\" is not installed."
fi
fi
# ----------------------------------------------------------


# ----------------------------------------------------------
# -------------Remove Zorin OS census unique ID-------------
# ----------------------------------------------------------
echo '--- Remove Zorin OS census unique ID'
sudo rm -fv '/var/lib/zorin-os-census/uuid'
# ----------------------------------------------------------


# Disable online search result collection (collects queries)
echo '--- Disable online search result collection (collects queries)'
if ! command -v 'gsettings' &> /dev/null; then
  echo 'Skipping because "gsettings" is not found.'
else
  gsettings set com.canonical.Unity.Lenses remote-content-search none
fi
# ----------------------------------------------------------


# ----------------------------------------------------------
# --------------Kill Zeitgeist daemon process---------------
# ----------------------------------------------------------
echo '--- Kill Zeitgeist daemon process'
if ! command -v 'zeitgeist-daemon' &> /dev/null; then
  echo 'Skipping because "zeitgeist-daemon" is not found.'
else
  zeitgeist-daemon --quit
fi
# ----------------------------------------------------------


# ----------------------------------------------------------
# --------------Remove Zeitgeist startup entry--------------
# ----------------------------------------------------------
echo '--- Remove Zeitgeist startup entry'
file='/etc/xdg/autostart/zeitgeist-datahub.desktop'
backup_file="${file}.old"
if [ -f "$file" ]; then
  echo "File exists: $file."
  sudo mv "$file" "$backup_file"
  echo "Moved to: $backup_file."
else
  echo "Skipping, no changes needed."
fi
# ----------------------------------------------------------


# ----------------------------------------------------------
# -----------Disable access to Zeitgeist database-----------
# ----------------------------------------------------------
echo '--- Disable access to Zeitgeist database'
file="$HOME/.local/share/zeitgeist/activity.sqlite"
if [ -f "$file" ]; then
  chmod -rw "$file"
  echo "Successfully disabled read/write access to $file."
else
  echo "Skipping, no action needed, file does not exist at $file."
fi
# ----------------------------------------------------------


# ----------------------------------------------------------
# -Remove Zeitgeist package (can break integrated software)-
# ----------------------------------------------------------
echo '--- Remove Zeitgeist package (can break integrated software)'
if ! command -v 'apt-get' &> /dev/null; then
  echo 'Skipping because "apt-get" is not found.'
else
  apt_package_name='zeitgeist-core'
if status="$(dpkg-query -W --showformat='${db:Status-Status}' "$apt_package_name" 2>&1)" \
    && [ "$status" = installed ]; then
  echo "\"$apt_package_name\" is installed and will be uninstalled."
  sudo apt-get purge -y "$apt_package_name"
else
  echo "Skipping, no action needed, \"$apt_package_name\" is not installed."
fi
fi
if ! command -v 'pacman' &> /dev/null; then
  echo 'Skipping because "pacman" is not found.'
else
  pkg_package_name='zeitgeist'
if pacman -Qs "$pkg_package_name" > /dev/null ; then
  echo "\"$pkg_package_name\" is installed and will be uninstalled."
  sudo pacman -Rcns "$pkg_package_name" --noconfirm
else
  echo "The package $pkg_package_name is not installed"
fi
fi
if ! command -v 'dnf' &> /dev/null; then
  echo 'Skipping because "dnf" is not found.'
else
  rpm_package_name='zeitgeist'
sudo dnf autoremove -y --skip-broken "$rpm_package_name"
fi
# ----------------------------------------------------------


# ----------------------------------------------------------
# -------Disable participation in Popularity Contest--------
# ----------------------------------------------------------
echo '--- Disable participation in Popularity Contest'
config_file='/etc/popularity-contest.conf'
if [ -f "$config_file" ]; then
  sudo sed -i '/PARTICIPATE/c\PARTICIPATE=no' "$config_file"
else
  echo "Skipping because configuration file at ($config_file) is not found. Is popcon installed?"
fi
# ----------------------------------------------------------


# ----------------------------------------------------------
# -------Remove Popularity Contest (`popcon`) package-------
# ----------------------------------------------------------
echo '--- Remove Popularity Contest (`popcon`) package'
if ! command -v 'apt-get' &> /dev/null; then
  echo 'Skipping because "apt-get" is not found.'
else
  apt_package_name='popularity-contest'
if status="$(dpkg-query -W --showformat='${db:Status-Status}' "$apt_package_name" 2>&1)" \
    && [ "$status" = installed ]; then
  echo "\"$apt_package_name\" is installed and will be uninstalled."
  sudo apt-get purge -y "$apt_package_name"
else
  echo "Skipping, no action needed, \"$apt_package_name\" is not installed."
fi
fi
# ----------------------------------------------------------


# ----------------------------------------------------------
# -Remove daily cron entry for Popularity Contest (popcon)--
# ----------------------------------------------------------
echo '--- Remove daily cron entry for Popularity Contest (popcon)'
job_name='popularity-contest'
cronjob_path="/etc/cron.daily/$job_name"
if [[ -f "$cronjob_path" ]]; then
  if [[ -x "$cronjob_path" ]]; then
    sudo chmod -x "$cronjob_path"
    echo "Successfully disabled cronjob \"$job_name\"."
  else
    echo "Skipping, cronjob \"$job_name\" is already disabled."
  fi
else
  echo "Skipping, \"$job_name\" cronjob is not found."
fi
# ----------------------------------------------------------


# ----------------------------------------------------------
# ----------------Remove `reportbug` package----------------
# ----------------------------------------------------------
echo '--- Remove `reportbug` package'
if ! command -v 'apt-get' &> /dev/null; then
  echo 'Skipping because "apt-get" is not found.'
else
  apt_package_name='reportbug'
if status="$(dpkg-query -W --showformat='${db:Status-Status}' "$apt_package_name" 2>&1)" \
    && [ "$status" = installed ]; then
  echo "\"$apt_package_name\" is installed and will be uninstalled."
  sudo apt-get purge -y "$apt_package_name"
else
  echo "Skipping, no action needed, \"$apt_package_name\" is not installed."
fi
fi
# ----------------------------------------------------------


# ----------------------------------------------------------
# ----------Remove Python modules for `reportbug`-----------
# ----------------------------------------------------------
echo '--- Remove Python modules for `reportbug`'
if ! command -v 'apt-get' &> /dev/null; then
  echo 'Skipping because "apt-get" is not found.'
else
  apt_package_name='python3-reportbug'
if status="$(dpkg-query -W --showformat='${db:Status-Status}' "$apt_package_name" 2>&1)" \
    && [ "$status" = installed ]; then
  echo "\"$apt_package_name\" is installed and will be uninstalled."
  sudo apt-get purge -y "$apt_package_name"
else
  echo "Skipping, no action needed, \"$apt_package_name\" is not installed."
fi
fi
# ----------------------------------------------------------


# ----------------------------------------------------------
# ----Remove UI for reportbug (`reportbug-gtk` package)-----
# ----------------------------------------------------------
echo '--- Remove UI for reportbug (`reportbug-gtk` package)'
if ! command -v 'apt-get' &> /dev/null; then
  echo 'Skipping because "apt-get" is not found.'
else
  apt_package_name='reportbug-gtk'
if status="$(dpkg-query -W --showformat='${db:Status-Status}' "$apt_package_name" 2>&1)" \
    && [ "$status" = installed ]; then
  echo "\"$apt_package_name\" is installed and will be uninstalled."
  sudo apt-get purge -y "$apt_package_name"
else
  echo "Skipping, no action needed, \"$apt_package_name\" is not installed."
fi
fi
# ----------------------------------------------------------


# ----------------------------------------------------------
# ----------------Remove `pkgstats` package-----------------
# ----------------------------------------------------------
echo '--- Remove `pkgstats` package'
if ! command -v 'pacman' &> /dev/null; then
  echo 'Skipping because "pacman" is not found.'
else
  pkg_package_name='pkgstats'
if pacman -Qs "$pkg_package_name" > /dev/null ; then
  echo "\"$pkg_package_name\" is installed and will be uninstalled."
  sudo pacman -Rcns "$pkg_package_name" --noconfirm
else
  echo "The package $pkg_package_name is not installed"
fi
fi
# ----------------------------------------------------------


# ----------------------------------------------------------
# -----------Disable weekly `pkgstats` submission-----------
# ----------------------------------------------------------
echo '--- Disable weekly `pkgstats` submission'
if ! command -v 'systemctl' &> /dev/null; then
  echo 'Skipping because "systemctl" is not found.'
else
  service='pkgstats.timer'
if systemctl list-units --full -all | grep --fixed-strings --quiet "$service"; then # service exists
  if systemctl is-enabled --quiet "$service"; then
    if systemctl is-active --quiet "$service"; then
      echo "Service $service is running now, stopping it."
      if ! sudo systemctl stop "$service"; then
        >&2 echo "Could not stop $service."
      else
        echo 'Successfully stopped'
      fi
    fi
    if sudo systemctl disable "$service"; then
      echo "Successfully disabled $service."
    else
      >&2 echo "Failed to disable $service."
    fi
  else
    echo "Skipping, $service is already disabled."
  fi
else
  echo "Skipping, $service does not exist."
fi
fi
# ----------------------------------------------------------


# ----------------------------------------------------------
# ---Disable participation in metrics reporting in Ubuntu---
# ----------------------------------------------------------
echo '--- Disable participation in metrics reporting in Ubuntu'
if ! command -v 'ubuntu-report' &> /dev/null; then
  echo 'Skipping because "ubuntu-report" is not found.'
else
  if ubuntu-report -f send no; then
  echo 'Successfully opted out.'
else
  >&2 echo 'Failed to opt out.'
fi
fi
# ----------------------------------------------------------


# ----------------------------------------------------------
# -------Remove Ubuntu Report tool (`ubuntu-report`)--------
# ----------------------------------------------------------
echo '--- Remove Ubuntu Report tool (`ubuntu-report`)'
if ! command -v 'apt-get' &> /dev/null; then
  echo 'Skipping because "apt-get" is not found.'
else
  apt_package_name='ubuntu-report'
if status="$(dpkg-query -W --showformat='${db:Status-Status}' "$apt_package_name" 2>&1)" \
    && [ "$status" = installed ]; then
  echo "\"$apt_package_name\" is installed and will be uninstalled."
  sudo apt-get purge -y "$apt_package_name"
else
  echo "Skipping, no action needed, \"$apt_package_name\" is not installed."
fi
fi
# ----------------------------------------------------------


# ----------------------------------------------------------
# -----------------Remove `apport` package------------------
# ----------------------------------------------------------
echo '--- Remove `apport` package'
if ! command -v 'apt-get' &> /dev/null; then
  echo 'Skipping because "apt-get" is not found.'
else
  apt_package_name='apport'
if status="$(dpkg-query -W --showformat='${db:Status-Status}' "$apt_package_name" 2>&1)" \
    && [ "$status" = installed ]; then
  echo "\"$apt_package_name\" is installed and will be uninstalled."
  sudo apt-get purge -y "$apt_package_name"
else
  echo "Skipping, no action needed, \"$apt_package_name\" is not installed."
fi
fi
# ----------------------------------------------------------


# ----------------------------------------------------------
# ------------------Disable Apport service------------------
# ----------------------------------------------------------
echo '--- Disable Apport service'
if ! command -v 'systemctl' &> /dev/null; then
  echo 'Skipping because "systemctl" is not found.'
else
  service='apport'
if systemctl list-units --full -all | grep --fixed-strings --quiet "$service"; then # service exists
  if systemctl is-enabled --quiet "$service"; then
    if systemctl is-active --quiet "$service"; then
      echo "Service $service is running now, stopping it."
      if ! sudo systemctl stop "$service"; then
        >&2 echo "Could not stop $service."
      else
        echo 'Successfully stopped'
      fi
    fi
    if sudo systemctl disable "$service"; then
      echo "Successfully disabled $service."
    else
      >&2 echo "Failed to disable $service."
    fi
  else
    echo "Skipping, $service is already disabled."
  fi
else
  echo "Skipping, $service does not exist."
fi
fi
# ----------------------------------------------------------


# ----------------------------------------------------------
# --Disable participation in Apport error messaging system--
# ----------------------------------------------------------
echo '--- Disable participation in Apport error messaging system'
if [ -f /etc/default/apport ]; then
  sudo sed -i 's/enabled=1/enabled=0/g' /etc/default/apport
  echo 'Successfully disabled apport.'
else
  echo 'Skipping, apport is not configured to be enabled.'
fi
# ----------------------------------------------------------


# ----------------------------------------------------------
# ----------------Remove `whoopsie` package-----------------
# ----------------------------------------------------------
echo '--- Remove `whoopsie` package'
if ! command -v 'apt-get' &> /dev/null; then
  echo 'Skipping because "apt-get" is not found.'
else
  apt_package_name='whoopsie'
if status="$(dpkg-query -W --showformat='${db:Status-Status}' "$apt_package_name" 2>&1)" \
    && [ "$status" = installed ]; then
  echo "\"$apt_package_name\" is installed and will be uninstalled."
  sudo apt-get purge -y "$apt_package_name"
else
  echo "Skipping, no action needed, \"$apt_package_name\" is not installed."
fi
fi
# ----------------------------------------------------------


# ----------------------------------------------------------
# -----------------Disable Whoopsie service-----------------
# ----------------------------------------------------------
echo '--- Disable Whoopsie service'
if ! command -v 'systemctl' &> /dev/null; then
  echo 'Skipping because "systemctl" is not found.'
else
  service='whoopsie'
if systemctl list-units --full -all | grep --fixed-strings --quiet "$service"; then # service exists
  if systemctl is-enabled --quiet "$service"; then
    if systemctl is-active --quiet "$service"; then
      echo "Service $service is running now, stopping it."
      if ! sudo systemctl stop "$service"; then
        >&2 echo "Could not stop $service."
      else
        echo 'Successfully stopped'
      fi
    fi
    if sudo systemctl disable "$service"; then
      echo "Successfully disabled $service."
    else
      >&2 echo "Failed to disable $service."
    fi
  else
    echo "Skipping, $service is already disabled."
  fi
else
  echo "Skipping, $service does not exist."
fi
fi
# ----------------------------------------------------------


# ----------------------------------------------------------
# -------------Disable crash report submissions-------------
# ----------------------------------------------------------
echo '--- Disable crash report submissions'
if [ -f /etc/default/whoopsie ] ; then
  sudo sed -i 's/report_crashes=true/report_crashes=false/' /etc/default/whoopsie
fi
# ----------------------------------------------------------


echo 'Your privacy and security is now hardened 🎉💪'
echo 'Press any key to exit.'
read -n 1 -s