#!/usr/bin/env bash
# Author: Pierre Gronau <Pierre.Gronau@ndaal.eu>
# Copyright 2024
# License: All content is licensed under the terms of the <MIT License>
# Developed on: Debian 12.x; macOS Sequoia x86 architecture
# Tested on: Debian 12.x; macOS Sequoia x86 architecture
#
# Exit on error. Append "|| true" if you expect an error.
set -o errexit
# This is equivalent to set -e. It causes the script to exit
# immediately if any command exits with a non-zero status.
#
# Exit on error inside any functions or subshells.
set -o errtrace
# This setting ensures that the ERR trap is inherited by shell functions,
# command substitutions, and commands executed in a subshell environment.
#
# Do not allow use of undefined vars. Use ${VAR:-} to use an undefined VAR
set -o nounset
# This is equivalent to set -u. It treats unset variables as
# an error when substituting.
#
# Catch the error in case mysqldump fails (but gzip succeeds) in `mysqldump |gzip`
# https://vaneyckt.io/posts/safer_bash
set -o pipefail
# This setting causes a pipeline to return the exit status of the last command
# in the pipe that returned a non-zero status.
#
# Turn on traces, useful while debugging but commented out by default
# set -o xtrace

# Set $IFS to only newline and tab.
#
# http://www.dwheeler.com/essays/filenames-in-shell.html
# nosemgrep: ifs-tampering
IFS=$'\n\t'

# Error trapping function
error_handler() {
    local line_number="${1}"
    local error_code="${2}"
    local last_command="${BASH_COMMAND}"
    printf "Error: Error occurred in line %s (error code: %s)\n" "${line_number}" "${error_code}"
    printf "Error: Failed command: %s\n" "${last_command}"
}

# Set the error trap
trap "error_handler ${LINENO} \$?" ERR

cleanup() {
    trap - SIGINT SIGTERM ERR EXIT
    printf "%b\n" "\nInfo: Cleanup is running ..."
    # Additional cleanup tasks can be added here if needed
    rm -f -v ./*.tmp
    printf "%b\n" "\nInfo: Cleanup finished ..."
}

trap cleanup SIGINT SIGTERM ERR EXIT

echo "deleting /etc/hosts"
rm -v /etc/hosts

# https://gitlab.com/ndaal_team_developer/ndaal_ml_infra_deploy/-/blob/master/documentation/source/Security.rst#default-dns-server

echo "fetching hosts files from https://github.com/StevenBlack/hosts"
# https://github.com/StevenBlack/hosts
# curl https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts | sudo tee -a /etc/hosts
# curl https://raw.githubusercontent.com/StevenBlack/hosts/master/alternates/fakenews/hosts | sudo tee -a /etc/hosts
# curl https://raw.githubusercontent.com/StevenBlack/hosts/master/alternates/gambling/hosts | sudo tee -a /etc/hosts
# curl https://raw.githubusercontent.com/StevenBlack/hosts/master/alternates/porn/hosts | sudo tee -a /etc/hosts
# curl https://raw.githubusercontent.com/StevenBlack/hosts/master/alternates/social/hosts | sudo tee -a /etc/hosts
curl https://raw.githubusercontent.com/StevenBlack/hosts/master/alternates/fakenews-gambling-porn-social/hosts | sudo tee -a /etc/hosts

echo "fetching hosts files from https://github.com/crazy-max/WindowsSpyBlocker/tree/master/data/hosts"
# https://github.com/crazy-max/WindowsSpyBlocker/tree/master/data/hosts
curl https://raw.githubusercontent.com/crazy-max/WindowsSpyBlocker/master/data/hosts/spy.txt | sudo tee -a /etc/hosts
curl https://raw.githubusercontent.com/crazy-max/WindowsSpyBlocker/master/data/hosts/spy_v6.txt | sudo tee -a /etc/hosts
curl https://raw.githubusercontent.com/crazy-max/WindowsSpyBlocker/master/data/hosts/extra.txt | sudo tee -a /etc/hosts
curl https://raw.githubusercontent.com/crazy-max/WindowsSpyBlocker/master/data/hosts/extra_v6.txt | sudo tee -a /etc/hosts
curl https://raw.githubusercontent.com/crazy-max/WindowsSpyBlocker/master/data/hosts/update.txt | sudo tee -a /etc/hosts
curl https://raw.githubusercontent.com/crazy-max/WindowsSpyBlocker/master/data/hosts/update_v6.txt | sudo tee -a /etc/hosts

echo "setting DNS resolvers/servers for Wi-Fi to 9.9.9.11 149.112.112.11 37.235.1.174 2620:fe::11 2620:fe::fe:11 2001:67c:28a4:: 2002:d596:2a92:1:71:53::"
networksetup -setdnsservers Wi-Fi 9.9.9.11 149.112.112.11 37.235.1.174 2620:fe::11 2620:fe::fe:11 2001:67c:28a4:: 2002:d596:2a92:1:71:53::

cleanup

script_name1="$(basename "${0}")"
printf "\nInfo: script_name1: %s\n" "${script_name1}"
script_path1="$(realpath "$(dirname "${0}")")"
printf "Info: script_path1: %s\n" "${script_path1}"
script_path_with_name="${script_path1}/${script_name1}"
printf "Info: Script path with name: %s\n" "${script_path_with_name}"
printf "Info: Script finished\n"
exit 0
