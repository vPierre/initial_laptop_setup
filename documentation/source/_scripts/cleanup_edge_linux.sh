#!/usr/bin/env bash

# Author: Pierre Gronau <Pierre.Gronau@ndaal.eu>
# Copyright 2024
# License: All content is licensed under the terms of the <Apache 2.0>
# Developed on: Debian 12.x; macOS Sequoia x86 architecture
# Tested on: Debian 12.x; macOS Sequoia x86 architecture

set -o errexit
set -o errtrace
set -o nounset
set -o pipefail

# nosemgrep: ifs-tampering
IFS=$'\n\t'

# Error trapping function
# Add this line before the error_handler function
# shellcheck disable=SC2317
error_handler() {
    local line_number="${1}"
    local error_code="${2}"
    local last_command="${BASH_COMMAND}"
    printf "Error: Error occurred in line %s (error code: %s)\n" "${line_number}" "${error_code}"
    printf "Error: Failed command: %s\n" "${last_command}"

    # Success case
    return 0
}

# Set the error trap
trap 'error_handler ${LINENO} $?' ERR

cleanup() {
    trap - SIGINT SIGTERM ERR EXIT
    printf "%b\n" "\nInfo: Cleanup is running ..."
    # Additional cleanup tasks can be added here if needed
    rm -f -v ./*.tmp
    printf "%b\n" "\nInfo: Cleanup finished ..."
}

trap cleanup SIGINT SIGTERM ERR EXIT

VERSION="1.0.0"
readonly VERSION
printf "Info: Current date: %s\n" "${VERSION}"

LOG_FILE="/var/log/edge_cleanup.log"

# Logging function
log() {
    printf "%s - %s\n" "$(date '+%Y-%m-%d %H:%M:%S')" "${1}" | tee -a "${LOG_FILE}"
}

# Check for root privileges
check_root() {
    if [ "${EUID}" -ne 0 ]; then
        script_path=$([[ "${0}" = /* ]] && printf "%s" "${0}" || printf "%s" "${PWD}/${0#./}")
        sudo "${script_path}" || {
            printf "Error: Administrator privileges are required.\n"
            exit 1
        }
        exit 0
    fi
}

# Clear Edge data
clear_edge_data() {
    local dir="${1}"
    if [ -d "${dir}" ]; then
        printf "Info: Clearing Edge data in: %s\n" "${dir}"
        rm -r -f -v "${dir:?}"/*
    else
        printf "Warning: No Edge data found in: %s, skipping...\n" "${dir}"
    fi
}

# Remove specific Edge files
remove_edge_files() {
    local base_dir="${1}"
    local file_pattern="${2}"
    find "${base_dir}" -name "${file_pattern}" -type f -delete
    printf "Removed %s files from %s\n" "${file_pattern}" "${base_dir}"
}

# Clear Edge directories
clear_edge_directories() {
    local base_dir="${1}"
    local dir_name="${2}"
    rm -r -f -v "${base_dir:?}/${dir_name:?}"
    printf "Cleared %s directory from %s\n" "${dir_name}" "${base_dir}"
}

# Disable Edge telemetry
disable_edge_telemetry() {
    local preferences_file="${1}"
    if [ -f "${preferences_file}" ]; then
        sed -i 's/"metrics_reporting_enabled":true/"metrics_reporting_enabled":false/g' "${preferences_file}"
        sed -i 's/"reporting_enabled":true/"reporting_enabled":false/g' "${preferences_file}"
        printf "Disabled telemetry in Edge preferences file.\n"
    else
        printf "Warning: Edge preferences file not found. Please disable telemetry manually in Edge settings.\n"
    fi
}

# Main function
main() {
    check_root
    
    printf "Starting Microsoft Edge cleanup and privacy enhancement...\n"

    export HOME="/home/${SUDO_USER:-${USER}}"

    # Clear Edge data
    clear_edge_data "${HOME}/.config/microsoft-edge"
    clear_edge_data "${HOME}/.var/app/com.microsoft.Edge"

    # Remove specific Edge files
    edge_files=(
        "Bookmarks*" "Cookies*" "History*" "Login Data*" "Web Data*"
        "Favicons*" "Shortcuts*" "Top Sites*" "Visited Links"
        "DownloadMetadata" "Extension Cookies*" "Media History*"
        "Network Action Predictor*" "Network Persistent State"
        "Preferences" "QuotaManager*" "Reporting and NEL*"
        "SecurePreferences" "SyncData.sqlite3" "Trust Tokens*"
    )

    for file in "${edge_files[@]}"; do
        remove_edge_files "${HOME}/.config/microsoft-edge" "${file}"
        remove_edge_files "${HOME}/.var/app/com.microsoft.Edge" "${file}"
    done

    # Clear Edge directories
    edge_dirs=("Extensions" "File System" "Sessions")

    for dir in "${edge_dirs[@]}"; do
        clear_edge_directories "${HOME}/.config/microsoft-edge" "${dir}"
        clear_edge_directories "${HOME}/.var/app/com.microsoft.Edge" "${dir}"
    done

    # Disable Edge telemetry
    disable_edge_telemetry "${HOME}/.config/microsoft-edge/Default/Preferences"
    disable_edge_telemetry "${HOME}/.var/app/com.microsoft.Edge/config/microsoft-edge/Default/Preferences"

    printf "Microsoft Edge cleanup and privacy enhancement completed.\n"
}

main "$@"

cleanup

script_name1="$(basename "${0}")"
printf "\nInfo: script_name1: %s\n" "${script_name1}"
script_path1="$(realpath "$(dirname "${0}")")"
printf "Info: script_path1: %s\n" "${script_path1}"
script_path_with_name="${script_path1}/${script_name1}"
printf "Info: Script path with name: %s\n" "${script_path_with_name}"
printf "Info: Script finished\n"
exit 0
