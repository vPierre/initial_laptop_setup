#!/usr/bin/env bash
# Author: Pierre Gronau <Pierre.Gronau@ndaal.eu>
# Copyright 2024, 2025
# License: All content is licensed under the terms of the <Apache 2.0>
# Developed on: Debian 12.x; macOS Sequoia x86 architecture
# Tested on: Debian 12.x; macOS Sequoia x86 architecture

# Initialize required variables
SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
SCRIPT_NAME="$(basename "${BASH_SOURCE[0]}")"
SCRIPT_PATH="${SCRIPT_DIR}/${SCRIPT_NAME}"
HOSTNAME="${HOSTNAME:-$(hostname)}"
USER="${USER:-$(whoami)}"
HOMEDIR="${HOME:-}"
LOG_FILE="${LOG_FILE:-/tmp/java_check.log}"
JAVA_HOME="${JAVA_HOME:-}"
TEMP_DIR="${TEMP_DIR:-/tmp}"
SEARCH_DIR="${SEARCH_DIR:-${HOMEDIR}}"

# Set bash strict mode after variable initialization
set -o errexit
set -o errtrace
set -o nounset
set -o pipefail
IFS=$'\n\t'

# Function to check if ripgrep is installed
check_ripgrep() {
    if ! command -v rg >/dev/null 2>&1; then
        log "Error" "ripgrep (rg) is not installed. Please install it first."
        return 1
    fi
    return 0
}

# Function to search for Java-related files
search_java_files() {
    local search_dir="${1:-${SEARCH_DIR}}"
    local temp_file="${TEMP_DIR}/search_results.tmp"
    
    log "Info" "Starting Java file search in directory: ${search_dir}"
    
    # Check if ripgrep is available
    if ! check_ripgrep; then
        return 1
    fi
    
    # Create temp directory if it doesn't exist
    mkdir -p "${TEMP_DIR}"
    
    # Clean up any existing temp file
    rm -f "${temp_file}"
    
    log "Info" "Searching for Java-related files..."
    
    # Perform the search
    if ! rg --no-ignore \
        --ignore-file .gitignore \
        -i \
        --debug \
        --glob "*.class" \
        --glob "*.jar" \
        --glob "*.jsp" \
        --glob "*.jspx" \
        --glob "*.do" \
        --glob "*.action" \
        "${search_dir}" \
        >> "${temp_file}" 2>&1; then
        
        log "Warning" "Search completed with some errors"
    fi
    
    # Check if any files were found
    if [ -s "${temp_file}" ]; then
        log "Info" "Found Java-related files:"
        while IFS= read -r line; do
            log "Info" "Found file: ${line}"
        done < "${temp_file}"
        
        # Count files by type
        log "Info" "File type summary:"
        printf "*.class files: %d\n" "$(grep -c '\.class$' "${temp_file}")" | log "Info"
        printf "*.jar files: %d\n" "$(grep -c '\.jar$' "${temp_file}")" | log "Info"
        printf "*.jsp files: %d\n" "$(grep -c '\.jsp$' "${temp_file}")" | log "Info"
        printf "*.jspx files: %d\n" "$(grep -c '\.jspx$' "${temp_file}")" | log "Info"
        printf "*.do files: %d\n" "$(grep -c '\.do$' "${temp_file}")" | log "Info"
        printf "*.action files: %d\n" "$(grep -c '\.action$' "${temp_file}")" | log "Info"
    else
        log "Warning" "No Java-related files found in ${search_dir}"
    fi
    
    # Cleanup
    rm -f "${temp_file}"
}

[Previous functions remain the same...]

# Main script execution
# Initialize log file
touch "${LOG_FILE}"
log "Info" "Starting Java installation check script"
log "Info" "Log file initialized at: ${LOG_FILE}"
log "Info" "Script running as user: ${USER}"
log "Info" "Current hostname: ${HOSTNAME}"

# Check JAVA_HOME
check_java_home

# Check Java processes
check_java_processes

# Search for Java files
search_java_files "${SEARCH_DIR}"

# Get all detected package managers
mapfile -t package_managers < <(detect_package_managers)

if [ ${#package_managers[@]} -gt 0 ]; then
    log "Info" "Detected package managers: ${package_managers[*]}"
    
    # Check Java installations for each detected package manager
    for pm in "${package_managers[@]}"; do
        log "Info" "Checking package manager: ${pm}"
        check_java_installations "-${pm}"
    done
else
    log "Error" "No supported package managers detected"
fi

# Perform system-wide checks regardless of package managers
if command -v java >/dev/null 2>&1; then
    log "Info" "System-wide Java binary found at: $(command -v java)"
    java -version 2>&1 | while IFS= read -r line; do
        log "Info" "Java version info: ${line}"
    done
fi

# Add a summary of all findings
printf "\n=== Summary of Java Installation Check ===\n"
if [ -f "${LOG_FILE}" ]; then
    printf "Timestamp: %s\n" "$(date '+%Y-%m-%d %H:%M:%S')"
    printf "\nJAVA_HOME Configuration:\n"
    grep "JAVA_HOME" "${LOG_FILE}" | sort -u

    printf "\nInstalled Java Packages:\n"
    grep "Found package:" "${LOG_FILE}" | sort -u

    printf "\nRunning Java Processes:\n"
    grep "Java process:" "${LOG_FILE}" | sort -u

    printf "\nJava Version Information:\n"
    grep "Java version info:" "${LOG_FILE}" | sort -u
else
    printf "No log file found at %s\n" "${LOG_FILE}"
fi

log "Info" "Java installation check script completed"
