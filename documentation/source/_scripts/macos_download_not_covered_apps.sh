#!/usr/bin/env bash
# Author: Pierre Gronau <Pierre.Gronau@ndaal.eu>
# Copyright 2024
# License: All content is licensed under the terms of the <MIT License>
# Developed on: Debian 12.x; macOS Sequoia x86 architecture
# Tested on: Debian 12.x; macOS Sequoia x86 architecture
#
# Exit on error. Append "|| true" if you expect an error.
set -o errexit
# This is equivalent to set -e. It causes the script to exit
# immediately if any command exits with a non-zero status.
#
# Exit on error inside any functions or subshells.
set -o errtrace
# This setting ensures that the ERR trap is inherited by shell functions,
# command substitutions, and commands executed in a subshell environment.
#
# Do not allow use of undefined vars. Use ${VAR:-} to use an undefined VAR
set -o nounset
# This is equivalent to set -u. It treats unset variables as
# an error when substituting.
#
# Catch the error in case mysqldump fails (but gzip succeeds) in `mysqldump |gzip`
# https://vaneyckt.io/posts/safer_bash
set -o pipefail
# This setting causes a pipeline to return the exit status of the last command
# in the pipe that returned a non-zero status.
#
# Turn on traces, useful while debugging but commented out by default
# set -o xtrace

# Set $IFS to only newline and tab.
#
# http://www.dwheeler.com/essays/filenames-in-shell.html
# nosemgrep: ifs-tampering
IFS=$'\n\t'

# Error trapping function
# Add this line before the error_handler function
# shellcheck disable=SC2317
error_handler() {
    local line_number="${1}"
    local error_code="${2}"
    local last_command="${BASH_COMMAND}"
    printf "Error: Error occurred in line %s (error code: %s)\n" "${line_number}" "${error_code}"
    printf "Error: Failed command: %s\n" "${last_command}"

    # Success case
    return 0
}

# Set the error trap
trap "error_handler ${LINENO} \$?" ERR

cleanup() {
    trap - SIGINT SIGTERM ERR EXIT
    printf "%b\n" "\nInfo: Cleanup is running ..."
    # Additional cleanup tasks can be added here if needed
    rm -f -v ./*.tmp
    printf "%b\n" "\nInfo: Cleanup finished ..."
}

trap cleanup SIGINT SIGTERM ERR EXIT

print_asterisks_value=79
readonly print_asterisks_value 
printf "Info: Current print_asterisks_value: %s\n" "${print_asterisks_value}"

# Function to print a line of x asterisks
print_asterisks() {
    local x="${1}"

    # Check if x is within the valid range
    if [[ "${x}" -lt 10 || "${x}" -gt 79 ]]; then
        printf "Error: The value of x (%d) must be between 10 and 79.\n" "${x}"
        exit 1
    fi

    # Print x asterisks
    printf "%0.s*" $(seq 1 "${x}")
    printf "\n"

    # Success case
    return 0
}

printf "\n\n"
print_asterisks "${print_asterisks_value}"

declare -A warnings

warnings["en"]="WARNING: Please do a backup with Time Machine.
WARNING: The installation script is not tested on ARM macOS systems."
warnings["de"]="WARNUNG: Bitte machen Sie ein Backup mit Time Machine.
WARNUNG: Das Installationsskript wurde nicht auf ARM macOS-Systemen getestet."
warnings["es"]="ADVERTENCIA: Por favor, haga una copia de seguridad con Time Machine.
ADVERTENCIA: El script de instalación no ha sido probado en sistemas macOS ARM."
warnings["fr"]="AVERTISSEMENT : Veuillez faire une sauvegarde avec Time Machine.
AVERTISSEMENT : Le script d'installation n'a pas été testé sur les systèmes macOS ARM."
warnings["pt"]="AVISO: Por favor, faça um backup com o Time Machine.
AVISO: O script de instalação não foi testado em sistemas macOS ARM."

# Print warnings in each language
for lang in "${!warnings[@]}"; do
    printf "%s\n" "${warnings[$lang]}"
    printf "\n"
done

print_asterisks "${print_asterisks_value}"
printf "\n\n"

# Define the Downloads directory path
downloads_dir="/Users/$(whoami)/Downloads"
printf "Info: Current Download Directory is: %s\n" "${downloads_dir}"

# Check if the Downloads directory exists
if [[ -d "${downloads_dir}" ]]; then
    # Change to the Downloads directory
    cd "${downloads_dir}"
    printf "Info: Changed Directory to: %s\n" "${downloads_dir}"
else
    # Stay in the current location
    printf "Error: Download Directory does not exist: %s\n" "${downloads_dir}"
fi

wget --secure-protocol=auto --https-only --continue --verbose -N --tries=10 --check-certificate "https://github.com/spyglass-search/spyglass/releases/download/v2024.11.1/Spyglass_24.11.1_universal.dmg"
wget --secure-protocol=auto --https-only --continue --verbose -N --tries=10 --check-certificate "https://cdn.kde.org/ci-builds/graphics/okular/master/macos-x86_64/okular-master-5730-macos-clang-x86_64.dmg"
wget --secure-protocol=auto --https-only --continue --verbose -N --tries=10 --check-certificate "https://mirror.ctan.org/systems/mac/mactex/MacTeX.pkg"

ls -la "${downloads_dir}"/*.dmg
ls -la "${downloads_dir}"/*.pkg

# https://apple.stackexchange.com/questions/113489/unattended-installation-of-pkg-file
# https://github.com/tjluoma/autopkginstall
sudo installer -allowUntrusted -verboseR -pkg "MacTeX.pkg" -target /

printf "Info: Install the application in the Download Directory: %s\n" "${downloads_dir}"

cleanup

script_name1="$(basename "${0}")"
printf "\nInfo: script_name1: %s\n" "${script_name1}"
script_path1="$(realpath "$(dirname "${0}")")"
printf "Info: script_path1: %s\n" "${script_path1}"
script_path_with_name="${script_path1}/${script_name1}"
printf "Info: Script path with name: %s\n" "${script_path_with_name}"
printf "Info: Script finished\n"
exit 0
