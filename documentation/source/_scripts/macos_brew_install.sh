#!/usr/bin/env bash
# Author: Pierre Gronau <Pierre.Gronau@ndaal.eu>
# Copyright 2024
# License: All content is licensed under the terms of the <MIT License>
# Developed on: Debian 12.x; macOS Sequoia x86 architecture
# Tested on: Debian 12.x; macOS Sequoia x86 architecture
#
# Exit on error. Append "|| true" if you expect an error.
set -o errexit
# This is equivalent to set -e. It causes the script to exit
# immediately if any command exits with a non-zero status.
#
# Exit on error inside any functions or subshells.
set -o errtrace
# This setting ensures that the ERR trap is inherited by shell functions,
# command substitutions, and commands executed in a subshell environment.
#
# Do not allow use of undefined vars. Use ${VAR:-} to use an undefined VAR
set -o nounset
# This is equivalent to set -u. It treats unset variables as
# an error when substituting.
#
# Catch the error in case mysqldump fails (but gzip succeeds) in `mysqldump |gzip`
# https://vaneyckt.io/posts/safer_bash
set -o pipefail
# This setting causes a pipeline to return the exit status of the last command
# in the pipe that returned a non-zero status.
#
# Turn on traces, useful while debugging but commented out by default
# set -o xtrace

# Set $IFS to only newline and tab.
#
# http://www.dwheeler.com/essays/filenames-in-shell.html
# nosemgrep: ifs-tampering
IFS=$'\n\t'

# Error trapping function
# Add this line before the error_handler function
# shellcheck disable=SC2317
error_handler() {
    local line_number="${1}"
    local error_code="${2}"
    local last_command="${BASH_COMMAND}"
    printf "Error: Error occurred in line %s (error code: %s)\n" "${line_number}" "${error_code}"
    printf "Error: Failed command: %s\n" "${last_command}"

    # Success case
    return 0
}

# Set the error trap
trap 'error_handler ${LINENO} $?' ERR

cleanup() {
    trap - SIGINT SIGTERM ERR EXIT
    printf "%b\n" "\nInfo: Cleanup is running ..."
    # Additional cleanup tasks can be added here if needed
    rm -f -v ./*.tmp
    printf "%b\n" "\nInfo: Cleanup finished ..."
}

trap cleanup SIGINT SIGTERM ERR EXIT

print_asterisks_value=79
readonly print_asterisks_value 
printf "Info: Current print_asterisks_value: %s\n" "${print_asterisks_value}"

# Function to print a line of x asterisks
print_asterisks() {
    local x="${1}"

    # Check if x is within the valid range
    if [[ "${x}" -lt 10 || "${x}" -gt 79 ]]; then
        printf "Error: The value of x (%d) must be between 10 and 79.\n" "${x}"
        exit 1
    fi

    # Print x asterisks
    printf "%0.s*" $(seq 1 "${x}")
    printf "\n"

    # Success case
    return 0
}

printf "\n\n"
print_asterisks "${print_asterisks_value}"

declare -A warnings

warnings["en"]="WARNING: Please do a backup with Time Machine.
WARNING: The installation script is not tested on ARM macOS systems."
warnings["de"]="WARNUNG: Bitte machen Sie ein Backup mit Time Machine.
WARNUNG: Das Installationsskript wurde nicht auf ARM macOS-Systemen getestet."
warnings["es"]="ADVERTENCIA: Por favor, haga una copia de seguridad con Time Machine.
ADVERTENCIA: El script de instalación no ha sido probado en sistemas macOS ARM."
warnings["fr"]="AVERTISSEMENT : Veuillez faire une sauvegarde avec Time Machine.
AVERTISSEMENT : Le script d'installation n'a pas été testé sur les systèmes macOS ARM."
warnings["pt"]="AVISO: Por favor, faça um backup com o Time Machine.
AVISO: O script de instalação não foi testado em sistemas macOS ARM."

# Print warnings in each language
for lang in "${!warnings[@]}"; do
    printf "%s\n" "${warnings[$lang]}"
    printf "\n"
done

print_asterisks "${print_asterisks_value}"
printf "\n\n"

HOME_ZPROFILE="${HOME}/.zprofile"
UNAME_MACHINE="$(uname -m)"

#THIS SECTION SETS THE FIREWALL

printf "Info: Enabling Firewall\n"
/usr/libexec/ApplicationFirewall/socketfilterfw --setloggingmode on || true
/usr/libexec/ApplicationFirewall/socketfilterfw --setallowsigned on || true
/usr/libexec/ApplicationFirewall/socketfilterfw --setglobalstate on || true
sleep 1

#THIS SECTION DISABLES THE INFRARED RECIEVER

printf "Info: Disabling infrared receiver\n"
defaults write com.apple.driver.AppleIRController DeviceEnabled -bool FALSE || true
sleep 1

#THIS SECTION DISABLED THE BLUETOOTH

printf "Info: Disabling BlueTooth\n"
defaults write /Library/Preferences/com.apple.Bluetooth ControllerPowerState -int -0 || true
sleep 1

#THIS SECTION ENABLED AUTOMATIC UPDATES

printf "Info: Enabling Scheduled updates\n"
softwareupdate --schedule on || true
defaults write /Library/Preferences/com.apple.SoftwareUpdate.plist AutomaticCheckEnabled -bool true || true
defaults write /Library/Preferences/com.apple.SoftwareUpdate.plist AutomaticDownload -bool true || true
defaults write /Library/Preferences/com.apple.commerce.plist AutoUpdateRestartRequired -bool true || true
defaults write /Library/Preferences/com.apple.commerce.plist AutoUpdate -bool true || true
sleep 1

# find the CLI Tools update
printf "Info: Checking for CLI Tool updates...\n"
PROD=$(softwareupdate -l | grep "\*.*Command Line" | head -n 1 | awk -F"*" '{print $2}' | sed -e 's/^ *//' | tr -d '\n') || true
# install CLIE Tools update
if [[ -n "${PROD}" ]]; then
  softwareupdate -i "${PROD}" --verbose || true
fi

# Check for Homebrew, install if not installed
if test ! "$(which brew)"; then
  printf "Info: Installing Homebrew...\n"
  /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"

  printf "Info: Added Homebrew shell to %s.\n" "${HOME_ZPROFILE}"
  printf '# Add Homebrew support\n' >> "${HOME_ZPROFILE}"

  # load shellenv for Apple Silicon
  if [[ "${UNAME_MACHINE}" == "arm64" ]]; then
    printf "eval \"\$(%s)\"\n" "/opt/homebrew/bin/brew shellenv" >> "${HOME_ZPROFILE}"
    eval "$(/opt/homebrew/bin/brew shellenv)"
  fi

  printf "Info: add autocomplete for brew ... \n"
  printf "FPATH=\"%s/share/zsh/site-functions:\${FPATH}\"\n" "$(brew --prefix)" >> "${HOME_ZPROFILE}"
fi

softwareupdate --list || true
xcode-select --install|| true

if [[ "${UNAME_MACHINE}" == "arm64" ]]; then
    printf "Note: that it is very difficult to remove Rosetta 2 once it is installed.\n"
    softwareupdate --install-rosetta --agree-to-license
fi

printf "Info: Installing brew ... \n"
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"

brew analytics off
brew update && brew upgrade && brew cleanup
brew --version

WEBBROWSER=(
    "brave-browser"
    "chromium"
    "firefox"
    "google-chrome"
    "librewolf"
    "opera"
    "tor-browser"
    "waterfox"
    "zen-browser"
    )

printf "Info: brew install \"%s\"\n" "${WEBBROWSER[@]}" || true
brew install "${WEBBROWSER[@]}" || true
brew info "${WEBBROWSER[@]}" || true

# https://www.usebruno.com/compare/bruno-vs-postman

# Define an array of packages to install with double quotes
PACKAGES=(
    "wget"               # Command-line utility for downloading files from the web
    "curl"               # Tool for transferring data with URLs
    "mucommander"        # Dual-pane file manager for easy file management
    "vlc"                # Popular media player supporting various formats
    "thunderbird"        # Email client from Mozilla
    "adium"              # Instant messaging client for macOS
    "cyberduck"          # FTP, SFTP, WebDAV, and cloud storage browser
    "ghidra"             # Software reverse engineering framework developed by NSA
    "mediainfo"          # Tool to display technical information about media files
    "mediathekview"      # Tool to access and download content from German public broadcasters
    "osxfuse"            # FUSE implementation for macOS, allowing custom file systems
    "xee"                # Lightweight image viewer for macOS
    "scribus"            # Desktop publishing software for creating layouts and designs
    "rssowl"             # RSS feed reader for managing and reading feeds
    "epub-to-pdf"        # Tool to convert EPUB files to PDF format
    "libreoffice"        # Full-featured office suite that includes word processing, spreadsheets, etc.
    "microsoft-office"   # Microsoft Office suite (requires a license)
    "microsoft-teams"
    "inkscape"           # Vector graphics editor similar to Adobe Illustrator
    "mumble"             # Open-source VoIP application for gaming and collaboration
    "skim"               # PDF reader and note-taker for macOS with advanced features
    "skitch"             # Screenshot tool that allows annotations and sharing
    "keepassxc"          # Password manager that stores passwords securely
    "cryptomator"        # Client-side encryption solution for cloud storage
    "virtualbox"         # Cross-platform virtualization software for running multiple OSes
    "docker"             # Platform for developing, shipping, and running applications in containers
    "vagrant"            # Tool for building and managing virtualized development environments
    "keka"               # File archiver for macOS supporting various formats (e.g., ZIP, RAR)
    "anydesk"            # Remote desktop application for accessing computers remotely
    "krita"              # Digital painting application designed for artists
    "calibre"            # E-book management software that allows organizing and converting e-books
    "vmware-fusion"      # Virtualization software for running Windows on macOS (requires a license)
    "bibdesk"            # Reference management software for organizing bibliographies 
    "freetube"           # YouTube player that respects user privacy by blocking ads and trackers
    "rustdesk"           # Remote desktop software that is open-source and secure 
    "latest"             # Placeholder; specify a valid package instead of 'latest'
    "shottr"             # Screenshot tool with built-in editing features 
    "gns3"               # Graphical network simulator for designing complex network topologies 
    "htop"               # Interactive process viewer and system monitor 
    "youtube-dl"         # Command-line tool to download videos from YouTube and other sites 
    "links"              # Text-based web browser 
    "tree"               # Command-line utility to display directory structure in a tree format 
    "tmux"               # Terminal multiplexer allowing multiple terminal sessions in one window 
    "utm"                # UTM is a desktop hypervisor for macOS that allows users to run multiple operating systems, including Windows and Linux, on Apple hardware using both virtualization and emulation techniques.
    "qBittorrent"        # qBittorrent is a free, open-source BitTorrent client that offers a user-friendly interface, built-in search engine, and extensive features like bandwidth scheduling and IP filtering for efficient torrent management.
    "veracrypt"          # VeraCrypt is a free disk encryption software that provides enhanced security for data by creating a virtual encrypted disk or encrypting entire partitions, utilizing advanced encryption algorithms to protect sensitive information.
    "freerdp"            # reeRDP is a free, open-source implementation of the Remote Desktop Protocol (RDP) that allows users to connect to Windows desktops from various operating systems, offering features like audio redirection, clipboard sharing, and USB device support.
    "deluge"             # Deluge is a lightweight, cross-platform BitTorrent client that offers a user-friendly interface, plugin support, and advanced features like encryption and bandwidth scheduling for efficient torrent management.
    "diffmerge"          # DiffMerge is a visual file and directory comparison and merging tool that helps users identify and reconcile differences between files, making it useful for developers and content creators working with multiple versions of documents or code.
    "session"            # Session is a decentralized, end-to-end encrypted messaging app that prioritizes user privacy by routing messages through an onion routing network, similar to Tor, and doesn't require phone numbers or other personal information for registration.
    "signal"             # Signal is a secure, open-source messaging application that offers end-to-end encryption for text, voice, and video communications, emphasizing user privacy and security while providing features comparable to mainstream messaging apps.
    "threema"            # Threema is a paid, cross-platform encrypted messaging application developed in Switzerland that prioritizes user privacy by enabling anonymous communication without requiring personal information, while offering features such as end-to-end encryption for messages and calls, multimedia sharing, and group chats, all managed through a unique Threema ID instead of phone numbers or email addresses.
    "onionShare"         # OnionShare is an open-source tool that enables anonymous and secure file sharing, website hosting, and chatting through the Tor network, providing end-to-end encryption and ephemeral .onion addresses for enhanced privacy and security.
    "syncthing"          # Syncthing is a free, open-source file synchronization application that allows users to securely synchronize files between multiple devices in real-time, using a peer-to-peer network and strong encryption to ensure data privacy and integrity.
    "joplin"             # Joplin is an open-source note-taking and to-do application that supports end-to-end encryption, markdown formatting, and synchronization across multiple devices and platforms, allowing users to securely manage and organize their notes, tasks, and attachments.
    "retroshare"         # RetroShare is a free, open-source, decentralized communication platform that offers secure messaging, file sharing, forums, and chat rooms through a friend-to-friend network, utilizing strong encryption and authentication to protect user privacy and data.
    "clamav"             # ClamAV is a free cross platform open source Anti Virus solution
    "double-commander"   # Double Commander is a free cross platform open source file manager with two panels side by side.
    "ripgrep"

    # Podman and related tools (container management)
    "podman"
    "podman-compose"
    "podman-tui"
  
    # Security tools (static analysis, vulnerability scanning)
    "gitleaks"
    "grype"
    "syft"
    "trivy"

    # Networking tools (packet capture, analysis)
    "wireshark"          # Network protocol analyzer for troubleshooting and analysis
    "bomber"
    "tcpdump"

    # Development tools (shell scripting, formatting)
    "shellcheck"
    "shfmt"
    "shellharden"

    # Miscellaneous tools (burning CDs/DVDs, TeX distribution)
    "burn"
    "mactex"

    # Multimedia tools (streaming, recording)
    "obs"
    "blender"

    # Presentation tools (PDF presentation)
    "pdfpc"

    # Reference management tool (citation management)
    "zotero"

    # System tools (macOS system information)
    "mactracker"

    # Networking and security tools (network scanning)
    "nmap"

    # File management tools (file compression, metadata extraction)
    "7zip"
    "exiftool"

  	# Malware analysis tools (pattern matching)
    "yara"
    "yara-x"

    # Talos control tool from Sidero Labs (Kubernetes management)
    "siderolabs/tap/talosctl"

    # Document conversion tool (markdown to various formats)
    #"pandoc" #moved to pip3 install

    # Static analysis tool for code (code quality checks)
    "semgrep'"

    # Disk usage analyzer (disk space usage visualization)
    "dust"

	  # Image manipulation tool (image processing tasks)
    "imagemagick"

    # GPG suite for encryption and signing 
    "gpg-suite"

    "lynis"

)

# Print the installation command for user information
printf "\nInfo: Installing packages with Homebrew:\n\nbrew install \"%s\"\n\n" "${PACKAGES[@]}"

# Install the packages using Homebrew, handling errors gracefully
brew install "${PACKAGES[@]}" || true

# Install any cask packages (GUI applications)
brew install --cask "${PACKAGES[@]}" || true

# Display information about the installed packages
brew info "${PACKAGES[@]}" || true

printf "\nInstallation complete. Please check for any errors above.\n"

DEVELOPMENT=(
    "ansible"
    "ansible-builder"
    "ansible-lint"
    "awscli"
    "bruno"
    "emacs"
    "git"
    "git-lfs"
    "golang"
    "helm"
    "intellij-idea-ce"
    "jaq"
    "kubernetes-cli"
    "neovim"
    "openjdk"
    "pulumi"
    "python3"
    "ruby"
    "shellcheck"
    "shellharden"
    "shfmt"
    "terraform"
    "vim"
    "vscodium"
)

printf "Info: brew install \"%s\"\n" "${DEVELOPMENT[@]}" || true
brew install "${DEVELOPMENT[@]}" || true
brew info "${DEVELOPMENT[@]}" || true

tldr --update || true

printf "Info: For compilers to find ruby you may need to set:\n"

export LDFLAGS="-L/usr/local/opt/ruby/lib"
export CPPFLAGS="-I/usr/local/opt/ruby/include"

printf "Info: For pkg-config to find ruby you may need to set:\n"
export PKG_CONFIG_PATH="/usr/local/opt/ruby/lib/pkgconfig"

# https://developer.hashicorp.com/packer/install
brew tap hashicorp/tap
brew install hashicorp/tap/packer

brew update && brew upgrade && brew cleanup
brew --version

printf "Info: Cleaning Homebrew cache\n"
brew cleanup -s || true
printf "Info: Removing old versions of installed formulae\n"
brew cleanup --prune=all || true
printf "Info: Removing stale lock files and outdated downloads\n"
brew cleanup --prune-prefix || true
printf "Info: Checking Homebrew for potential problems\n"
brew doctor || true

brew list

cleanup

script_name1="$(basename "${0}")"
printf "\nInfo: script_name1: %s\n" "${script_name1}"
script_path1="$(realpath "$(dirname "${0}")")"
printf "Info: script_path1: %s\n" "${script_path1}"
script_path_with_name="${script_path1}/${script_name1}"
printf "Info: Script path with name: %s\n" "${script_path_with_name}"
printf "Info: Script finished\n"
exit 0
