#!/usr/bin/env bash
# Author: Pierre Gronau <Pierre.Gronau@ndaal.eu>
# Copyright 2024, 2025
# License: All content is licensed under the terms of the <Apache 2.0>
# Developed on: Debian 12.8x; macOS Sequoia x86 architecture
# Tested on: Debian 12.8x; macOS Sequoia x86 architecture
#
set -o errexit
set -o errtrace
set -o nounset
set -o pipefail
# set -o xtrace

# nosemgrep: ifs-tampering
IFS=$'\n\t'

# Error trapping function
# Add this line before the error_handler function
# shellcheck disable=SC2317 
error_handler() {
    local line_number="${1}"
    local error_code="${2}"
    local last_command="${BASH_COMMAND}"
    printf "Error: Error occurred in line %s (error code: %s)\n" "${line_number}" "${error_code}"
    printf "Error: Failed command: %s\n" "${last_command}"
}

# Set the error trap
trap 'error_handler ${LINENO} $?' ERR

cleanup() {
    trap - SIGINT SIGTERM ERR EXIT
    printf "%b\n" "\nInfo: Cleanup is running ..."
    # Additional cleanup tasks can be added here if needed
    rm -f -v ./*.tmp
    printf "%b\n" "\nInfo: Cleanup finished ..."
}

trap cleanup SIGINT SIGTERM EXIT

if [[ "$(uname)" == "Darwin" ]]; then
    HOMEDIR="Users"
    readonly HOMEDIR
elif [[ "$(uname)" == "Linux" ]]; then
    HOMEDIR="home"
    readonly HOMEDIR
else
    printf "Error: Unsupported operating system: %s\n" "$(uname)"
    exit 1
fi

# Under Linux you use `home` under macOS `Users`
printf "Info: Home Directory: %s\n" "${HOMEDIR}"

USERSCRIPT="cloud"
# Your user! In which context it SHOULD run
printf "Info: User script: %s\n" "${USERSCRIPT}"

VERSION="0.5.0"
readonly VERSION
printf "Info: Current version: %s\n" "${VERSION}"

LOG_FILE="/${HOMEDIR}/${USERSCRIPT}/prompting.log"
readonly LOG_FILE 
printf "Info: log file with path: %s\n" "${LOG_FILE}"
if [[ ! -f "${LOG_FILE}" ]]; then
    printf "Info: Log file does not exist. Creating it now.\n"
    touch "${LOG_FILE}" || {
        printf "Error: Failed to create log file at %s\n" "${LOG_FILE}"
        exit 1
    }
else
    printf "Info: Log file already exists at %s\n" "${LOG_FILE}"
fi

log() {
    printf "%s - %s\n" "$(date '+%Y-%m-%d %H:%M:%S')" "${1}" | tee -a "${LOG_FILE}"
}

log "starting script ..."

# Function to check if a command is available
check_command() {
    if ! command -v "${1}" &>/dev/null; then
        printf "Error: %s is not installed or not in PATH. Please install it and try again.\n" "${1}"
        return 1
    fi

    # Success case
    return 0
}

# Check for required commands
check_command "vagrant" || exit 1
check_command "ansible-playbook" || exit 1

ANSIBLE_ROLE_DIRECTORY="/${HOMEDIR}/${USERSCRIPT}/repos/ansible/roles"
readonly ANSIBLE_ROLE_DIRECTORY
printf "Info: Current ANSIBLE_ROLE_DIRECTORY: %s\n" "${ANSIBLE_ROLE_DIRECTORY}"

create_directory() {
    local dir="${1}"
    printf "Info: Directory will be %s\n" "${dir}"
    if [[ ! -d ${dir} ]]; then
        mkdir -p -v "${dir}"
        printf "Info: Directory %s is created.\n" "${dir}"
        printf "Info: placeholder.txt will be created in %s\n" "${dir}"
        touch "${dir}/placeholder.txt"
        printf "Info: placeholder.txt will be removed from %s\n" "${dir}"
        rm -f -v "${dir}/placeholder.txt"
        printf "Info: placeholder.txt is removed from %s\n" "${dir}"
    else
        printf "Info: Directory %s already exists. No creation needed.\n" "${dir}"
    fi

    # Success case
    return 0
}

DIRECTORY="${ANSIBLE_ROLE_DIRECTORY}"
printf "Info: %s\n" "${DIRECTORY}"

create_directory "${DIRECTORY}"

# Define an array of Ansible roles
ansible_roles=(
    "common"
    "webserver"
    "database"
    "security"
)


# Function to check if Ansible roles exist
check_ansible_roles() {
    for role in "${ansible_roles[@]}"; do
        if ! vagrant ssh "${vm_name}" -c "test -d ${ANSIBLE_ROLE_DIRECTORY}/${role}"; then
            printf "Error: Ansible role '%s' not found on VM '%s'.\n" "${role}" "${vm_name}"
            return 1
        fi
    done
    printf "Info: All required Ansible roles are available on VM '%s'.\n" "${vm_name}"
    return 0
}

# Function to copy Ansible roles to VM
copy_ansible_roles() {
    for role in "${ansible_roles[@]}"; do
        if ! vagrant ssh "${vm_name}" -c "test -d ${ANSIBLE_ROLE_DIRECTORY}/${role}"; then
            printf "Info: Copying Ansible role '%s' to VM '%s'.\n" "${role}" "${vm_name}"
            vagrant scp "${VAGRANT_SOURCE_DIR}/ansible/roles/${role}" "${vm_name}:${ANSIBLE_ROLE_DIRECTORY}/"
        fi
    done
}

# Modify the execute_ansible_roles_on_vm function to include these checks
execute_ansible_roles_on_vm() {
    local vm="${1}"
    
    if ! check_ansible_roles; then
        printf "Error: Some required Ansible roles are missing on VM '%s'. Copying roles...\n" "${vm}"
        copy_ansible_roles
    fi

    for role in "${ansible_roles[@]}"; do
        printf "Info: Executing Ansible role '%s' on VM '%s'...\n" "${role}" "${vm}"
        if ! vagrant ssh "${vm}" -c "ansible-playbook ${ANSIBLE_ROLE_DIRECTORY}/${role}/playbook.yml"; then
            printf "Error: Failed to execute Ansible role '%s' on VM '%s'.\n" "${role}" "${vm}"
            return 1
        fi
        printf "Info: Successfully executed Ansible role '%s' on VM '%s'.\n" "${role}" "${vm}"
    done

    return 0
}

# Function to run Ansible roles
run_ansible_roles() {
    for role in "${ansible_roles[@]}"; do
        printf "Info: Running Ansible role: %s\n" "${role}"
        if ! ansible-playbook -i localhost, -c local "${role}.yml"; then
            printf "Error: Failed to run Ansible role: %s\n" "${role}"
            return 1
        fi
    done
}

# Main function
main() {
    log "Starting main function..."
    
    # Run Ansible roles
    if ! run_ansible_roles; then
        log "Error: Failed to run Ansible roles"
        return 1
    fi
    
    log "Main function completed successfully"
}

# Run the main function
main

cleanup

log "stopping script ..."

script_name1="$(basename "${0}")"
printf "\nInfo: script_name1: %s\n" "${script_name1}"
script_path1="$(realpath "$(dirname "${0}")")"
printf "Info: script_path1: %s\n" "${script_path1}"
script_path_with_name="${script_path1}/${script_name1}"
printf "Info: Script path with name: %s\n" "${script_path_with_name}"
printf "Info: Script finished\n"
exit 0
