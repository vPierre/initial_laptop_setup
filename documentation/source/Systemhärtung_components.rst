******************************************************************************
Test: Systemhärtung - Behandelte Komponenten in der Linux Systemhärtung
******************************************************************************

.. sectionauthor:: Pierre Gronau <Pierre.Gronau@ndaal.eu>

.. contents:: Inhalt - Behandelte Komponenten in der Linux Systemhärtung
   :depth: 3

.. _yaml-example:

.. warning::

   Dies ist momentan nur ein Platzhalter und wird später gefüllt.


YAML Configuration Example
==========================

Here is an example of a YAML configuration file:

.. literalinclude:: bash.yml
   :language: yaml
   :linenos:

.. literalinclude:: bash.yml
   :language: yaml

.. admonition:: Referenz
   :class: tip

   .. literalinclude:: bash.yml
      :language: yaml  
      
Claude.ai 


.. yaml-admonition:: **/*.yml

.. yaml-admonition:: _components/*.yml

absichtlich Denglish

.. include-yaml-files:: _components/*
   :class: tip



.. include-yaml-files:: _components/sudo.yml
   :class: tip

ddd 

.. literalinclude::
   _components/sudo.yml
   :language: yaml

eigene

.. admonition:: Referenz Test
   :class: tip

   .. literalinclude::
      _components/sudo.yml
      :language: yaml

.. admonition:: Referenz Test 2
   :class: tip

   .. literalinclude::
      _components/*
      :language: yaml

.. literalinclude::
   _components/*
   :language: yaml

bash

.. literalinclude:: bash.yml
   :language: yaml

.. admonition:: Referenz
   :class: tip

   .. literalinclude:: bash.yml
      :language: yaml
