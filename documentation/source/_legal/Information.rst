******************************************************************************
ndaal - General Information - Allgemeine Informationen
******************************************************************************

.. sectionauthor:: Pierre Gronau <Pierre.Gronau@ndaal.eu>

.. meta::
   :keywords: ndaal sphinx documentation generator
   :keywords lang=en: IT Compliance IT Security Automation Common Information General Information
   :keywords lang=de: Allgemeine Informationen

..

.. contents:: Content - General Information - Inhalt Allgemeine Informationen
   :depth: 3

..

.. Hint::
   - In the event of any discrepancies between the German and English versions
     or any other case of doubt, the German version shall prevail.
   - Bei Abweichungen zwischen der deutschen und der englischen Fassung oder bei
     sonstigen Zweifelsfällen gilt die deutsche Version.

General Information - Allgemeine Informationen
------------------------------------------------------------------------------

We are offering for your convenience a bilingual version of 
**General Information**:

General Information
##############################################################################

In a safety audit, the results are based on a self-assessment with the 
responsible persons and the analysis of the relevant systems, which are 
limited in scope and time horizon.
In a technical security analysis, the security-relevant information is
determined directly during the analysis. In principle, here only 
vulnerabilities can be discovered, but no proof of their absence can be
provided.
Despite the greatest possible care, it is therefore possible in both cases
that not all security aspects could be discovered by ndaal. Therefore we 
exclude any liability for existing and not detected risks.
The results of the investigation do not release the customer in any way from
the pursuit of the safety objectives. The customer is in any case responsible
for the implemented measures for the elimination of vulnerabilities and for 
the guarantee their security objectives.

..

Any liability for damage resulting from misuse of the information is excluded.

Allgemeine Informationen
##############################################################################

In einem Sicherheitsaudit basieren die Ergebnisse auf einer Selbsteinschätzung 
mit den Verantwortlichen und der Analyse der relevanten Systeme, die sowohl 
im Umfang als auch im Zeithorizont begrenzt sind.
In einer technischen Sicherheitsanalyse werden die sicherheitsrelevanten 
Informationen direkt während der Analyse ermittelt. Grundsätzlich können hier
nur Schwachstellen entdeckt werden, aber es kann kein Nachweis über deren 
Abwesenheit erbracht werden.
Trotz größtmöglicher Sorgfalt ist es daher in beiden Fällen möglich, dass 
nicht alle Sicherheitsaspekte von der ndaal entdeckt werden konnten. Daher
schließen wir jegliche Haftung für bestehende und nicht erkannte Risiken aus.
Die Ergebnisse der Untersuchung entbinden den Kunden in keiner Weise von der
Verfolgung der Sicherheitsziele. Der Kunde ist in jedem Fall für die 
umgesetzten Maßnahmen zur Beseitigung von Schwachstellen und zur 
Gewährleistung ihrer Sicherheitsziele verantwortlich.

..

Eine Haftung für Schäden aus einem Missbrauch der hierin enthaltenen 
Informationen ist ausgeschlossen.

.....

Opening Clause - Öffnungsklausel
------------------------------------------------------------------------------

We are offering for your convenience a bilingual version of 
**Opening Clause**:

Opening Clause
##############################################################################

It is impossible to take into account all possible variants and factors of 
influencing in advance. The measures and minimum requirements specified in 
this document are generally to be implemented as described. There is only the 
possibility to deviate from them, reasons so require and, at the same time,
supplementary measures are taken. 
These measures SHALL be suitable to reduce the possible risks resulting from
the deviation to an acceptable level. 

..

``Irrefutable`` means that compelling technical or operational reasons are
exist. The mere fact that, for example, a smaller distance between
between data center locations enables more cost-effective operation, 
is not to be regarded as **unavoidable**.

Öffnungsklausel
##############################################################################

Es ist unmöglich, alle in der Realität möglichen Varianten und 
Einflussfaktoren vorwegnehmend zu berücksichtigen. Die in diesem Dokument
genannten Maßnahmen und Mindestanforderungen sind in der Regel wie beschrieben
umzusetzen. Es besteht ausschließlich dann die Möglichkeit davon abzuweichen, 
wenn unabweisbare Gründe das erfordern und zugleich ergänzende Maßnahmen 
ergriffen werden. Diese Maßnahmen MÜSSEN geeignet sein, die sich aus der 
Abweichung eventuell zusätzlichen ergebenden Risiken auf ein akzeptables 
Niveau zu reduzieren.

..

``Unabweisbar`` meint, dass zwingende technische oder betriebliche Gründe
vorliegen. Allein die Tatsache, dass beispielsweise ein geringerer Abstand
zwischen RZ-Standorten einen kostengünstigeren Betrieb ermöglicht, 
ist nicht als **unabweisbar** anzusehen.

.....

Disclaimer - Haftungsausschluß
------------------------------------------------------------------------------

We are offering for your convenience a bilingual version of 
**Disclaimer**:

Disclaimer
##############################################################################

We have checked the contents of this publication for conformity with the 
hardware and software described. 
However, deviations cannot be ruled out, so that we cannot accept any 
liability for the therefore cannot accept any liability for complete
conformity. The information in this are checked regularly, and any necessary
corrections are included in subsequent editions.

Haftungsausschluß
##############################################################################

Wir haben den Inhalt der Druckschrift auf Übereinstimmung mit der 
beschriebenen Hard- und Software geprüft.
Dennoch können Abweichungen nicht ausgeschlossen werden, so dass wir für die
vollständige Übereinstimmung keine Gewähr übernehmen. Die Angaben in dieser
Druckschrift werden regelmäßig überprüft, notwendige Korrekturen sind in den
nachfolgenden Auflagen enthalten.

.....

Rights of Use - Nutzungsrechte
------------------------------------------------------------------------------

We are offering for your convenience a bilingual version of 
**Rights of Use**:

Rights of Use
##############################################################################

The authors or ndaal grant an irrevocable, royalty-free, spatially and 
temporally unrestricted, non-exclusive right of use of the work results to the
end customer, unless otherwise regulated otherwise in the contract.

..

Claims from patents remain unaffected by this.

Nutzungsrechte
##############################################################################

Die Autoren bzw. ndaal räumen ein unwiderrufliches, unentgeltliches, räuumlich
und zeitlich nicht eingeschränktes, nicht ausschließliches Nutzungsrecht
gegenüber den Arbeitsergebnisse dem Endkunden ein soweit im Vertrag nichts
anderes geregelt wurde.

..

Ansprüche aus Patenten bleiben hiervon unberührt.

.....

No duty to update - Keine Pflicht zur Aktualisierung
------------------------------------------------------------------------------

No duty to update
##############################################################################

ndaal assumes no obligation to update any information or forward-looking 
statement contained herein, save for any information we are required to 
disclose by law.

Keine Pflicht zur Aktualisierung
##############################################################################

ndaal übernimmt keine Verpflichtung, die in dieser Meldung enthaltenen 
Informationen und Zukunftsaussagen zu aktualisieren, soweit keine 
gesetzliche Veröffentlichungspflicht besteht.

