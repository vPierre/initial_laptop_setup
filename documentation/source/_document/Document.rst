******************************************************************************
Document Information - Dokument Information
******************************************************************************

.. sectionauthor:: Pierre Gronau <Pierre.Gronau@ndaal.eu>

.. contents:: Content
   :depth: 3

.. _Document_Information:

Summary - Zusammenfassung
------------------------------------------------------------------------------

========================================  =====================================================
**Effective Date**: 9. March 2025         **Document Owner**: Pierre Gronau
========================================  =====================================================

========================================  =====================================================
**Next Review**: 9. March 2026            **Document Owner**: Pierre Gronau
========================================  =====================================================

..

Introduction Semantic Versioning Scheme
------------------------------------------------------------------------------

We are offering for your convenience a bilingual version of 
**Introduction Semantic Versioning Scheme**:

Under the SemVer Versioning Scheme [1]_ , our versioning would take on the 
following format:

.. figure:: _static/SemVer.png
   :alt: SemVer Versioning Scheme
   :width: 75%
   :align: center

   SemVer Versioning Scheme

Where **x**, **y**, and **z** are integers that increase numerically and
indicate **Major**, **Minor**, and **Patch**, respectively.

The SemVer specification assumes that projects using this scheme MUST have a 
public API. Based on this, let’s take a look at each part in more detail 
starting from left to right.

Major
##############################################################################

**The major version should increase when we’ve introduced new functionality 
which breaks our API, i.e., increase this value when we’ve added a 
backward-incompatible change to our project**. When this number is increased,
we must reset the Minor and Patch numbers to ``0``.

For example, if we have a project that is on version ``1.2.5`` and we have 
introduced a breaking change under the SemVer scheme, we must set our new 
version number to ``2.0.0``.

Minor
##############################################################################

**We should increase our minor version when we’ve introduced new functionality 
which changes our API but is backward compatible, i.e., a non-breaking change**. 
We can also opt to change the Minor version if we’ve made substantial changes
to the internal code of our project.

Similarly, when we change the Minor version we should also reset the patch 
version to ``0``. For example, updating the Minor version of a project at 
``2.0.1`` would set it to ``2.1.0``.

Patch
##############################################################################

**Under the SemVer specs, we reserve patch changes for backward-compatible
bug fixes**. A patch change should not involve any changes to the API.

Pre-Release and Build
##############################################################################

After the patch version, there are some optional labels we can add to our
versions, such as a pre-release label or a build number.

For example, to mark a package as a pre-release, we must add a hyphen then
the pre-release label, which can be a dot-separated identifier, e.g., 
``1.0.0-alpha.1`` tells us that this project is a pre-release version of 
``1.0.0 labeled alpha.1``. A pre-release label indicates that this version is 
unstable and has a high risk if we use it. When considering version 
precedence, a pre-release version is always of lower precedence than the
normal version.

If we want to indicate the build of that release, we can add a dot-separated
identifier of the build appended after the patch (or pre-release) with a 
``+`` sign. For example, ``1.0.0-alpha.1+001``. Build meta-data does not factor 
in precedence, so we can consider two versions that only differ in build 
number to be of the same precedence.

Development SemVer
##############################################################################

The SemVer specification reserves the Major version ``0.x.y`` for development
purposes. 
When starting a new project, it makes sense to start initial development at a 
release ``0.1.0`` since it will, of course, include features. SemVer assumes
a development version to be unstable and can change at any time.

.. Tip::
   We will use **SemVer** Versioning Scheme [1]_ under 
   the Attribution 3.0 Unported (CC BY 3.0) License [2]_.

Einführung Semantisches Versionierungsschema
------------------------------------------------------------------------------

Nach dem SemVer Versionierungs Schema [1]_ würde unsere Versionierung das folgende 
Format annehmen:

.. figure:: _static/ndaal_Grafik_pre_release_DE_rz.png
   :alt: SemVer Versionierungs Schema
   :width: 75%
   :align: center

   SemVer Versionierungs Schema


Dabei sind **x**, **y** und **z** ganze Zahlen, die numerisch aufsteigen und
jeweils **Haupt**, **Neben** und **Patch** bezeichnen.

Die SemVer-Spezifikation geht davon aus, dass Projekte, die dieses Schema 
verwenden, über eine öffentliche API haben. Schauen wir uns nun jeden Teil im
Detail an beginnend von links nach rechts.

Zusammenfassung
##############################################################################

Auf Grundlage einer Versionsnummer von MAJOR.MINOR.PATCH werden die einzelnen 
Elemente folgendermaßen erhöht:

#. MAJOR wird erhöht, wenn API-inkompatible Änderungen veröffentlicht werden,
#. MINOR wird erhöht, wenn neue Funktionalitäten, welche kompatibel zur bisherigen API sind, veröffentlicht werden, und
#. PATCH/KORREKTUR wird erhöht, wenn die Änderungen ausschließlich API-kompatible Bugfixes umfassen.

Außerdem sind Bezeichner für Vorveröffentlichungen und Build-Metadaten als Erweiterungen zum MAJOR.MINOR.PATCH Format verfügbar.

Einführung
##############################################################################

In der Welt der Softwareentwicklung existiert ein grauenhafter Ort namens 
“Dependency Hell”. Um so größer ein Projekt wird und je mehr Pakete in die 
Software integriert werden, desto wahrscheinlicher ist es, dass dieser 
fürchterliche Ort eines Tages betreten wird.

In Projekten mit vielen Abhängigkeiten kann das Aktualisieren abhängiger 
Pakete schnell zum Albtraum werden. Sind die Abhängigkeitsspezifikationen des 
Pakets zu strikt, besteht die Gefahr des “Version Lock” (die Unfähigkeit ein 
Paket zu aktualisieren, ohne, dass alle abhängigen Pakete dieses Pakets 
ebenfalls aktualisiert werden müssen). Wenn die Abhängigkeiten des Pakets 
allerdings zu lasch definiert sind, wird sehr wahrscheinlich ein Problem, das
sich “Version Promiscuity” nennt (das Paket gibt vor, mit mehr zukünftigen 
Versionen seiner abhängigen Pakete kompatibel zu sein, als angemessen ist), 
eintreten. Dependency Hell bezeichnet die Situation, in der entweder 
Version Lock oder Version Promiscuity, oder beides den Entwicklungsprozess des
Projekts beeinträchtigt.

Als Lösung für dieses Problem schlage ich ein einfaches Regelwerk vor, welches
definiert wie Versionsnummern gewählt und erhöht werden. Diese Regeln basieren
auf bereits existierenden und weit verbreiteten Verfahren, welche sowohl bei 
der Entwicklung von Closed- als auch von Open-Source Software verwendet 
werden, aber beschränken sich nicht zwingend auf diese. Um dieses System 
nutzen zu können, muss zuerst eine öffentliche API definiert werden. Diese 
kann entweder in Form einer Dokumentation existieren oder durch den Code
selbst erzwungen werden. Egal auf welche Art und Weise die API umgesetzt wird,
es ist wichtig, dass sie übersichtlich und präzise ist. Sobald die öffentliche
API erstellt wurde, werden Änderungen an dieser durch bestimmten Veränderungen
an der Versionsnummer vermittelt. Nimm ein Versionsnummernformat von 
``X.Y.Z (Major.Minor.Patch)`` an. Bei Einführung von Bugfixes, welche die 
öffentliche API nicht beeinflussen, wird die Patch Version erhöht, 
API-kompatible Ergänzungen oder Änderungen erhöhen die Minor Versionsnummer, 
und Änderungen, welche nicht kompatibel zur aktuellen öffentlichen API sind, 
erhöhen die Major Version.

Ich nenne dieses System “Semantic Versioning”. Versionsnummern, die nach 
diesem Schema gewählt und erhöht werden, geben direkten Aufschluss über den 
entsprechenden Code und was sich von einer zur anderen Version verändert hat.

Semantic Versioning Spezifikation (SemVer)
##############################################################################

Die Terme “MUST”, “MUST NOT”, “REQUIRED”, “SHALL”, “SHALL NOT”, “SHOULD”, 
“SHOULD NOT”, “RECOMMENDED”, “MAY”, und “OPTIONAL” in diesem Dokument sind, 
wie in RFC 2119 beschrieben, zu interpretieren.

Software, welche Semantic Versioning nutzt, muss (MUST) eine öffentliche 
API definieren. Die API kann entweder im Code selbst deklariert werden oder 
in einer Dokumentation enthalten sein. Wie auch immer sie umgesetzt wird, es 
ist wichtig, dass sie präzise und ausführlich ist.

Eine gewöhnliche Versionsnummer muss (MUST) dem Format X.Y.Z entsprechen, 
wobei ``X``, ``Y`` und ``Z`` Ganzzahlen größer oder gleich Null sind und 
eine Zahl größer als Null keine führenden Nullen enthalten darf. ``X`` ist die 
Major Version, ``Y`` ist die Minor Version und ``Z`` ist die Patch Version. 
Jedes Element muss (MUST) auf numerische Art und Weise erhöht werden. 
Zum Beispiel: ``1.9.0`` -> ``1.10.0`` -> ``1.11.0``.

Sobald eine Version eines Projektes veröffentlicht wurde, darf (MUST NOT)
der Inhalt dieser Version nicht mehr verändert werden. Eine Änderung am 
Inhalt muss (MUST) als eine neue Version veröffentlicht werden.

Versionsnummern mit einer Major Version von ``0`` (``0.y.z``) sind für die 
initiale Development Phase gedacht. Änderungen können in jeder denkbaren Form
und zu jeder Zeit auftreten. Die öffentliche API sollte nicht als stable betrachtet werden.

Die Version ``1.0.0`` definiert die öffentliche API. Ab dieser 
Veröffentlichung hängt die Art und Weise, wie die Versionsnummer erhöht 
und verändert wird, von der öffentlichen API und den Änderungen, die an ihr 
vollzogen werden, ab.

Die Patch Version Z (x.y.Z | x > 0) muss (MUST) erhöht werden, wenn 
ausschließlich API-kompatible Bugfixes eingeführt werden. Ein Bugfix ist 
als eine interne Änderung, welche ein fehlerhaftes Verhalten korrigiert, 
definiert.

Die Minor Version Y (x.Y.z | x > 0) muss (MUST) erhöht werden, wenn neue 
Funktionalitäten, welche kompatibel zur bisherigen API sind, veröffentlicht 
werden. Sie muss (MUST) außerdem erhöht werden, wenn eine Funktion der 
öffentlichen API als deprecated markiert wird. Wenn umfangreiche Änderungen an 
internem Code eingeführt werden, darf (MAY) die Minor Version ebenfalls erhöht 
werden. Wenn diese Versionsnummer erhöht wird, muss (MUST) die Patch Version
auf Null zurückgesetzt werden.

Die Major Version X (X.y.z | X > 0) muss (MUST) immer dann erhöht werden, 
wenn API-inkompatible Änderungen in die öffentlichen API eingeführt werden. 
Die Änderungen dürfen (MAY) auch Änderungen umfassen, die ansonsten die 
Minor Version oder die Patch Version erhöht hätten. Wenn diese Versionsnummer 
erhöht wird, muss (MUST) sowohl die Minor Version als auch die Patch Version 
auf Null zurückgesetzt werden.

Eine Vorveröffentlichung kann (MAY) gekennzeichnet werden, indem ein 
Bindestrich, gefolgt von dem Vorveröffentlichungs-Bezeichner, dessen 
Elemente durch Punkte voneinander getrennt werden, an die Patch Version 
angehängt wird. Die Elemente des Bezeichners dürfen (MUST) nur aus 
alphanumerischen ASCII Zeichen und dem Bindestrich ([0-9A-Za-z-]) bestehen. 
Sie dürfen (MUST NOT) außerdem nicht leer sein. Wenn ein Element 
ausschließlich aus Ziffern besteht, darf (MUST NOT) es keine führenden 
Nullen enthalten. Eine Vorveröffentlichungs-Version hat einen niedrigeren 
Rang als die entsprechende reguläre Version. Ein 
Vorveröffentlichungs-Bezeichner kennzeichnet, dass die Version als unstable
zu betrachten ist und dass sie unter Umständen nicht den 
Kompatibilitätsanforderungen, die für die entsprechende regulären Version 
bestimmt wurden, entspricht. 
Beispiele: ``1.0.0-alpha``, ``1.0.0-alpha.1``, ``1.0.0-0.3.7``, 
``1.0.0-x.7.z.92``.

Build-Metadaten können (MAY) ausgezeichnet werden, indem ein Plus Symbol, 
gefolgt von den Metadaten, deren Elemente durch Punkte voneinander getrennt 
werden, an die Patch Version oder den Vorveröffentlichungs-Bezeichner 
angehängt wird. Die Elemente der Metadaten dürfen (MUST) nur aus 
alphanumerischen ASCII Zeichen und dem Bindestrich ([0-9A-Za-z-]) bestehen. 
Sie dürfen (MUST NOT) außerdem nicht leer sein. Die Build-Metadaten haben 
keinerlei Einfluss auf den Rang einer Version, sodass zwei Versionen, deren 
Versionsnummern sich nur in den Build-Metadaten unterscheiden, denselben 
Rang einnehmen. 
Beispiele: ``1.0.0-alpha+001``, ``1.0.0+20130313144700``, 
``1.0.0-beta+exp.sha.5114f85``.

Der Rang einer Version bestimmt, welche Versionsnummer einer anderen 
übergeordnet ist, wenn diese bei einer Sortierung miteinander verglichen 
werden. Der Rang wird (MUST) aus der Major, Minor und Patch Version sowie 
dem Vorveröffentlichungs-Bezeichner berechnet (Die Build-Metadaten haben 
keinerlei Einfluss auf den Rang einer Version). Er wird bestimmt, indem der 
erste Unterschied zwischen dem oben aufgeführten Elementen ermittelt wird. 
Dabei wird von links nach rechts, in der oben genannten Reihenfolge 
vorgegangen. Die Major, Minor und Patch Versionen werden numerisch miteinander 
verglichen. 
Beispiel: ``1.0.0`` < ``2.0.0`` < ``2.1.0`` < ``2.1.1.`` 

Beim Vergleich von zwei Versionsnummern, deren Major, Minor und Patch 
Versionen gleich sind, nimmt eine Vorveröffentlichung einen niedrigeren Rang 
als die reguläre Version ein. 
Beispiel: ``1.0.0-alpha`` < ``1.0.0``. 
Sind beide dieser Versionen Vorveröffentlichungen, wird (MUST) der Rang 
ermittelt, indem jedes Element eines Vorveröffentlichungs-Bezeichners 
(durch Punkte voneinander getrennt) mit dem der anderen Version verglichen 
wird bis ein Unterschied festgestellt wird. Auch hierbei wird von links nach 
rechts vorgegangen. Elemente, welche ausschließlich aus Ziffern bestehen, 
werden numerisch miteinander verglichen. Der Vergleich aller anderen Elemente 
erfolgt auf Basis der ASCII-Stellenwerte ihrer Zeichen. Numerische Elemente 
haben immer einen niedrigeren Rang als solche, die auch andere Zeichen 
enthalten. Falls alle Elemente identisch sind, nimmt der Bezeichner mit den 
meisten Elementen den höheren Rang ein. 
Beispiel: ``1.0.0-alpha`` < ``1.0.0-alpha.1`` < ``1.0.0-alpha.beta`` < 
``1.0.0-beta`` < ``1.0.0-beta.2`` < ``1.0.0-beta.11`` < ``1.0.0-rc.1`` < 
``1.0.0``.

Document Information - Dokument Information
------------------------------------------------------------------------------

The Summary of the Document Information is listed here:

Die Zusammenfassung der Dokumentinformationen ist hier aufgeführt:

========================================  =====================================================
**Effective Date**: 9. März 2025          **Document Owner**: Pierre Gronau
========================================  =====================================================

========================================  =====================================================
**Next Review**: 9. März 2026             **Document Owner**: Pierre Gronau
========================================  =====================================================

..

Document History - Dokumentenhistorie
##############################################################################

The whole Document History is listed here:

Die gesamte Dokumentenhistorie ist hier aufgeführt:
..

.. csv-table:: Document History - Dokumentenhistorie
   :class: longtable
   :file: doc_history.csv
   :widths: 15, 15, 25, 15, 15, 15, 15, 15
   :header-rows: 1

General Equal Treatment Act
##############################################################################

We are offering for your convenience a bilingual version of 
**General Equal Treatment Act**:

Consideration of the General Equal Treatment Act (AGG = Allgemeine 
Gleichbehandlungsgesetz). For reasons of practical readability, the following 
personal forms of address are limited to the masculine form of address; 
nevertheless, the information refers to members of all genders, i.e. male, 
female and diverse.

Berücksichtigung des Allgemeinen Gleichbehandlungsgesetz (AGG)
##############################################################################

Aus Gründen der praktischen Lesbarkeit sind die im Folgenden personenbezogenen 
Anreden auf die männliche Anrede beschränkt; nichtsdestoweniger beziehen sich 
die Angaben auf Angehörige aller Geschlechter, also männlich, weiblich und 
divers.

Validity
##############################################################################

We are offering for your convenience a bilingual version of 
**Validity**:

The document must be adapted whenever a significant technical change is made. 
However, at the latest one year after the last review. It is advisable to 
review and adapt this document every six months.

Gültigkeit
##############################################################################

Bei jeder wesentlichen technischen Änderung muss das Dokument angepasst
werden. Längstens jedoch ein Jahr nach der letzten Überprüfung. Es ist 
angezeigt, dieses Dokument einer halbjährlichen Prüfung zu unterziehen und 
anzupassen.

Target group - Qualified personnel
##############################################################################

We are offering for your convenience a bilingual version of 
**Target group - Qualified personnel**:

The product/system described in this documentation MAY only be handled by 
qualified personnel for the respective task in compliance with the 
observing the documentation pertaining to the respective task, 
in particular the safety instructions and warnings contained therein. 
Qualified personnel are, on the basis of their training and experience 
to recognize risks in the handling of these products/systems and to avoid 
possible hazards.

..

Typically, these are IT administrators, IT security experts, 
Automation experts (e.g. Ansible, Terraform und Pulumi), 
employees from data protection.

Zielgruppe - Qualifiziertes Personal
##############################################################################

Das zu dieser Dokumentation zugehörige Produkt/System DARF nur für die 
jeweilige Aufgabenstellung qualifiziertem Personal gehandhabt werden unter 
Beachtung der für die jeweilige Aufgabenstellung zugehörigen Dokumentation, 
insbesondere der darin enthaltenen Sicherheits- und Warnhinweise. 
Qualifiziertes Personal ist auf Grund seiner Ausbildung und Erfahrung 
befähigt, im Umgang mit diesen Produkten/Systemen Risiken zu erkennen und 
mögliche Gefährdungen zu vermeiden.

..

Typischerweise sind dies IT-Administratoren, IT-Sicherheitsexperten, 
Automationsexperten (wie Ansible, Terraform und Pulumi), 
Mitarbeiter aus dem Datenschutz.

Notes on links
##############################################################################

The following links lead to thematically related organizations and provide 
further information. This is not an exclusive listing. We will gladly 
accept further references. Despite careful control of the content, 
we do not assume any liability for the content of external links. 
The operators of the linked pages are solely responsible for their content.

Hinweise zu Links
##############################################################################

Nachfolgende Links führen zu thematisch verwandten Organisationen und bieten 
weiterführende Informationen. Es handelt sich nicht um eine ausschließliche 
Auflistung. Weitere Hinweise nehmen wir gerne auf. Trotz sorgfältiger 
inhaltlicher Kontrolle übernehmen wir keine Haftung für die Inhalte 
externer Links. Für den Inhalt der verlinkten Seiten sind ausschließlich 
deren Betreiber verantwortlich.

.....

.. Rubric:: Footnotes

.. [1]
   https://semver.org/spec/v2.0.0.html

.. [2]
   https://creativecommons.org/licenses/by/3.0/
