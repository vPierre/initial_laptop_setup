==============================================================================
Dokument
==============================================================================


Kontakt
------------------------------------------------------------------------------

===================	===============================================================================
Firma:					**ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG**
===================	===============================================================================
Adresse:					Adolf-Grimme-Allee, 50829 Köln, Deutschland
E-Mail:	        		info@ndaal.eu
Tel:						+49 (0) 221 650 86 200
Tel:						+49 (0) 221 650 86 201 (digitaler Ersthilfe Dienst)
Website:	        		`ndaal <https://www.ndaal.eu>`_
Amtsgericht				Köln, HRA 35474
Komplementärin			XSTeam Beteiligungs GmbH
Geschäftsführer		Carsten Dingendahl
Amtsgericht				Köln, HRB 105499
DUNS Nummer				343064588
===================	===============================================================================


Auf einen Blick
------------------------------------------------------------------------------

+-----------------------------------+-----------------------------------+
| **Geltungsbereich**               | **Aufbewahrungsfrist**            |
|                                   |                                   |
| Dieses Dokument gilt für Sie als  | 1 Jahr(e)                         |
| Mitarbeiter und leitender         |                                   |
| Angestellter sowie als Mitglied   | **Nächster Review-Termin**        |
| der Geschäftsleitung der ndaal    |                                   |
| Gesellschaft für Sicherheit in    | .............                     |
| der Informationstechnik mbH & Co  |                                   |
| KG                                | **Klassifizierung**               |
|                                   |                                   |
| **Gültig ab**                     | Consulting                        |
|                                   |                                   |
| siehe Historie                    | **Vertraulichkeitsstufe**         |
|                                   |                                   |
| **Version**                       | Vertraulich                       |
|                                   |                                   |
| 0.1.x                             |                                   |
|                                   |                                   |
| **Autorisiert vom**               |                                   |
|                                   |                                   |
| siehe Historie                    |                                   |
|                                   |                                   |
| **Autorisiert am**                |                                   |
|                                   |                                   |
| siehe Historie                    |                                   |
+-----------------------------------+-----------------------------------+

Tabelle 1 - Dokumentüberblick

