###### Requirements without Version Specifiers ######`
#nose
#nose-cov
#beautifulsoup4
sphinx-toolbox
jupyter
sphinxcontrib.blockdiag
sphinxcontrib.actdiag
sphinxcontrib.nwdiag
sphinxcontrib.seqdiag
sphinxcontrib-kroki

#sphinx.ext.autodoc
#sphinx.ext.autosummary
sphinx-autopackagesummary
sphinx-autosummary-accessors
#sphinx.ext.autosectionlabel
#sphinx.ext.coverage
#sphinx.ext.doctest
#sphinx.ext.extlinks
#sphinx.ext.graphviz
#sphinx.ext.ifconfig
#sphinx.ext.imgconverter
#sphinx.ext.imgmath
#sphinx.ext.inheritance_diagram
#sphinx.ext.intersphinx
#sphinx.ext.mathjax
sphinxcontrib-napoleon
sphinx_sitemap
#sphinx.ext.todo
#sphinx.ext.viewcode
sphinxcontrib-manpage
sphinxcontrib-serializinghtml
sphinxcontrib-commandoutput
sphinx-jsonschema
sphinxcontrib-htmlhelp
sphinxcontrib-confluencebuilder
sphinxcontrib-applehelp
sphinxcontrib-autoyaml
sphinxcontrib-needs
myst-parser
sphinxcontrib-mermaid
sphinxcontrib.bibtex
sphinx_copybutton
sphinx-gallery
sphinxmark

# themes
sphinxawesome-theme
pydata-sphinx-theme
git+https://github.com/bashtage/sphinx-material.git
renku-sphinx-theme

# fonts
font-roboto

###### Requirements with Version Specifiers ######`
#docopt == 0.6.1             # Version Matching. Must be version 0.6.1
#sphinx >= 4.1.2             # Minimum version 4.1.2
#coverage != 3.5             # Version Exclusion. Anything except version 3.5
#Mopidy-Dirble ~= 1.1        # Compatible release. Same as >= 1.1, == 1.*
sphinx >= 4.1.2             # Minimum version 4.1.2
docutils >= 0.17.1             # Minimum version 0.17.1

