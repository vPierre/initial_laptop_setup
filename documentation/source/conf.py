# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys
from pathlib import Path
from docutils import nodes
from docutils.parsers.rst import Directive, directives
from sphinx.util.docutils import SphinxDirective
import yaml
import glob

# org entries
#import os
#import sys
#from pathlib import Path

#sys.path.append(os.path.abspath("."))
sys.path.append(os.path.abspath('_extensions'))

# -- Project information -----------------------------------------------------

project = "ndaal - Pierre Gronau - Systemhärtung"
copyright = "2024, 2025 Pierre Gronau"
author = "Pierre Gronau"
description = "ndaal - Pierre Gronau - Systemhärtung für Linux Basissysteme"

# The full version, including alpha/beta/rc tags
version = "0.6.7"
release = "0.6.7"


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    # mandantory start
    "sphinxcontrib.blockdiag",
    "sphinxcontrib.actdiag",
    "sphinxcontrib.nwdiag",
    "sphinxcontrib.rackdiag",
    "sphinxcontrib.packetdiag",
    "sphinxcontrib.seqdiag",
    "sphinx.ext.duration",
    "sphinx.ext.doctest",
    "sphinx.ext.autodoc",
    "sphinx.ext.autosummary",
    #    'sphinx_autopackagesummary',
    #    'sphinx-autosummary-accessors',
    #    'sphinx.ext.autosectionlabel',
    "sphinx.ext.coverage",
    "sphinx.ext.doctest",
    "sphinx.ext.extlinks",
    "sphinx.ext.githubpages",
    "sphinx.ext.graphviz",
    "sphinx.ext.ifconfig",
    #'sphinx.ext.imgconverter',
    "sphinx.ext.imgmath",
    "sphinx.ext.inheritance_diagram",
    "sphinx.ext.intersphinx",
    "sphinx.ext.mathjax",
    # end
    # https://www.sphinx-doc.org/en/master/usage/extensions/napoleon.html
    "sphinx.ext.napoleon",
    "sphinx_sitemap",
    # https://www.markdownguide.org/getting-started/
    # https://myst-parser.readthedocs.io/en/latest/sphinx/intro.html
    #    'sphinx.ext.todo',
    #    'sphinx.ext.viewcode',
    #    'recommonmark',
    #'sphinx_markdown_tables',
    #    'sphinxcontrib.swaggerdoc',
    #    'sphinxcontrib.excel_table',
    #    'rusty.includesh',
    "sphinxcontrib.programoutput",
    # needflow uses PlantUML and the Sphinx-extension sphinxcontrib-plantuml for generating the flows.
    "sphinxcontrib.plantuml",
    #    'sphinxcontrib.datatemplates',
    "sphinxcontrib.autoyaml",
    #    'sphinxcontrib.recentpages',
    #    'sphinxcontrib.confluencebuilder',
    # obselete 'nbsphinx',
    # we use instead
    #    'myst_nb',
    #     'sphinx_git',
    #     'variations',
    #     'sphinxcontrib.openapi',
    "sphinxcontrib.details.directive",
    #    'sphinx_needs',
    #    'sphinxcontrib.test_reports',
    # https://hieroglyph.readthedocs.io
    #    'hieroglyph',
    #    'sphinxcontrib.shellcheck',
    #     'frigate.sphinx.ext',
    #     'sphinx_charts.charts',
    #'sphinx-exec-directive',
    #    'sphinx-jsonschema',
    "sphinxawesome_theme",
    #"furo",
    # "sphinxmark",
    #    'sphinxcontrib-fulltoc',
    #    'sphinxcontrib.mermaid',
    #    'sphinxcontrib.kroki',
    #"sphinxcontrib.bibtex",
    #    'sphinx_gallery.load_style',
    #    'sphinx_copybutton',
    # https://pypi.org/project/myst-parser/
    # MyST is a rich and extensible flavor of Markdown meant for technical documentation and publishing.
    "myst_parser",
    #    'sphinxcontrib-applehelp',
    #"sphinxcontrib-manpage",
    #    'sphinxcontrib.dashbuilder',
    #    'sphinx_design',
    "sphinxcontrib.programoutput",
    #"yaml_admonition",
    #"sphinx_licenseinfo",
    "sphinx_termynal",
    "sphinx-prompt",
    "sphinxemoji.sphinxemoji",
    #"sphinx-copybutton",
    #"sphinx-favicon",
    #"sphinxcontrib-httpdomain",
    #"sphinx-notfound-page",
    #"sphinx-version-warning",
    #"sphinx-hoverxref",
    #"sphinx-last-updated-by-git",
    #"sphinxext-opengraph",
    "sphinx_watermark",
]

# Fontpath for nwdiag series (truetype font)
# nwdiag_fontpath = '/usr/share/fonts/truetype/ipafont/ipagp.ttf'
# Fontpath for seqdiag (truetype font)
# seqdiag_fontpath = '/usr/share/fonts/truetype/ipafont/ipagp.ttf'

# Add any paths that contain templates here, relative to this directory.
templates_path = ["_templates"]

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ["_build", "Thumbs.db", ".DS_Store"]

source_suffix = [".rst", ".md"]
numfig = True

# -- Options for HTML output -------------------------------------------------
# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
# html_theme = "alabaster"
html_theme = "sphinxawesome_theme"
#html_theme_options = {
#    'maxdepth': 15,  # Adjust as needed, 3=default
#}
sidebarlogo = "Logo.png"
html_logo = "Logo.png"
# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ["_static"]
master_doc = "index"
latex_engine = "xelatex"
latex_logo = "imgs/Logo.png"
mypreamble = """
            \\usepackage{tikz}
            \\usepackage{lastpage}
            \\usetikzlibrary{calc}
            \\usepackage[sfdefault]{roboto}
            \\usepackage{tikzpagenodes}
            \\usepackage{url}
            \\makeatletter
            \\usepackage{fancyhdr}
            \\usepackage{stfloats}
            \\usepackage{titlesec}
            \\usepackage{xcolor, color, soul}
            \\usepackage{enumitem}

            \\setlistdepth{30}
            \\renewlist{itemize}{itemize}{30}
            \\renewlist{enumerate}{enumerate}{30}
            \\setlist[itemize]{label=$\\cdot$}
            \\setlist[itemize,1]{label=\\textbullet}
            \\setlist[itemize,2]{label=*}
            \\setlist[itemize,3]{label=--}

            \\sethlcolor{black}
            \\newcommand{\\tlpred}{{\\color[HTML]{FF2B2B}\\hl{TLP:RED}{ }}}
            \\newcommand{\\tlpamber}{{\\color[HTML]{FFC000}\\hl{TLP:AMBER}{ }}}
            \\newcommand{\\tlpamberstrict}{{\\color[HTML]{FFC000}\\hl{TLP:AMBER+STRICT}{ }}}
            \\newcommand{\\tlpgreen}{{\\color[HTML]{33FF00}\\hl{TLP:GREEN}{ }}}
            \\newcommand{\\tlpclear}{{\\color[HTML]{FFFFFF}\\hl{TLP:CLEAR}{ }}}

            \\fancypagestyle{plain}{
            \\lhead{}
            \\fancyhead[R]{\\includegraphics{Logo.png}
            \\fancyhead[L]{}}
            \\fancyfoot[C]{\\textbf{\\tlpgreen - freigegebene Entwurfsversion}}}
            \\setlength{\\headheight}{42pt}

            \\makeatletter
            \\titleformat{\\chapter}[frame]
              {\\normalfont}{\\filright\\enspace \\@chapapp~\\thechapter\\enspace\\FrameRule=0.5pt\\FrameSep=2pt}
              {10pt}{\\LARGE\\filright}
            \\titlespacing*{\\chapter}
              {0pt}{0pt}{5pt}
            \\makeatother

            \\ChNumVar{\\Huge\\fontfamily{roboto}}
            \\ChTitleVar{\\LARGE\\fontfamily{roboto}}
            \\ChTitleVar{\\Large\\fontfamily{roboto}}    
            
            \\geometry{
              a4paper,
              left=20mm,
              right=20mm,
              headheight=42pt,
              top=3.5cm,
              bottom=4cm,
              footskip=3cm
            }


            """


# TLP Definitions

tlpred = "\n\n             \\tlpred"
tlpamber = "\n\n             \\tlpamber"
tlpamberstrict = "\n\n             \\tlpamberstrict"
tlpgreen = "\n\n             \\tlpgreen"
tlpclear = "\n\n             \\tlpclear"
html_tlpred = '\n\n             <span style="background-color:#000000;"> <font color="#FF2B2B">TLP:RED</font> </span>'
html_tlpamber = '\n\n             <span style="background-color:#000000;"> <font color="#FFC000">TLP:AMBER</font> </span>'
html_tlpamberstrict = '\n\n             <span style="background-color:#000000;"> <font color="#FFC000">TLP:AMBER+STRICT</font> </span>'
html_tlpgreen = '\n\n             <span style="background-color:#000000;"> <font color="#33FF00">TLP:GREEN</font> </span>'
html_tlpclear = '\n\n             <span style="background-color:#000000;"> <font color="#FFFFFF">TLP:CLEAR</font> </span>'

# Inherit Paths
cover_path = Path("./_imgs/cover.png").resolve()
customer_logo_path = Path("./_imgs/customerlogo.png").resolve()


def create_final_rst() -> None:
    """Create the document's final page.

    :return: None
    :rtype: None
    """
    # pylint: disable=line-too-long
    with open("_finalchapter/final.rst", "w", encoding="utf-8") as final:
        final.write(".. raw:: latex\n\n")
        final.write("    \\chapter*{Contact}\n")
        final.write("    \\phantomsection\n")
        final.write("    \\addcontentsline{toc}{chapter}{Contact}\n")
        final.write("    \\vspace{1.5cm}\n\n")
        final.write(".. image::\n")
        final.write("   /_imgs/contact.png\n")
        final.write("   :width: 86%\n")
        final.write("   :align: right\n\n")
        final.write(".. raw:: latex\n\n")
        final.write(
            "        \\fancyfoot[R]{\\begin{tikzpicture}[remember" " picture,overlay]\n"
        )
        final.write(
            "    \\draw  let \\p1=($(current page footer area.north)-(current"
            " page.south)$),\n"
        )
        final.write("          \\n1={veclen(\\x1,\\y1)} in\n")
        final.write("    node [inner sep=0,outer sep=0,above right]\n")
        final.write(
            "          at (current page.south west){\\includegraphics"
            + "[width=\\paperwidth,height=5in]"
        )
        final.write("{")
        final.write("{}}}".format(cover_path))
        final.write("};\n")
        final.write("    \\end{tikzpicture}}\n\n")
        final.write(".. raw:: latex\n\n")
        final.write("    \\renewcommand{\\headrulewidth}{0pt}\n")
        final.write("    \\fancypagestyle{plain}{\n")
        final.write("    \\lhead{}\n")
        final.write("    \\fancyhead[R]{}\n")
        final.write("    \\fancyhead[L]{}}\n")
        final.write("    \\pagestyle{empty}\n")


def create_tables_rst() -> None:
    """Create the table index RST."""
    # pylint: disable=line-too-long
    tlns = [
        ".. raw:: latex\n\n",
        "    \\begingroup",
        (
            "    \\chapter{\\label{table_index}List of Tables - Verzeichnis"
            " der Tabellen}"
        ),
        "    \\renewcommand{\\chapter}[2]{}",
        "    \\listoftables",
        "    \\endgroup",
    ]
    with open("_finalchapter/tables.rst", "w", encoding="utf-8") as tables:
        for line in tlns:
            tables.write(line + "\n")


def create_figures_rst() -> None:
    """Create the figures index RST."""
    # pylint: disable=line-too-long
    tlns = [
        ".. raw:: latex\n\n",
        "    \\begingroup",
        ("    \\chapter{\\label{table_index}List of Tables}" " der Tabellen}"),
        "    \\renewcommand{\\chapter}[2]{}",
        "    \\listoffigures",
        "    \\endgroup",
    ]
    with open("_finalchapter/figures.rst", "w", encoding="utf-8") as tables:
        for line in tlns:
            tables.write(line + "\n")


def create_finalchapter() -> None:
    """Create the finalchapter directory.."""
    Path("_finalchapter").mkdir(exist_ok=True)
    create_tables_rst()
    create_figures_rst()
    create_final_rst()


if Path("_finalchapter").is_dir() is False:
    create_finalchapter()

rst_epilog = """
.. |tlpred| raw:: latex {}
.. |tlpamber| raw:: latex {}
.. |tlpamberstrict| raw:: latex {}
.. |tlpgreen| raw:: latex {}
.. |tlpclear| raw:: latex {}
.. |htmltlpred| raw:: html {}
.. |htmltlpamber| raw:: html {}
.. |htmltlpamberstrict| raw:: html {}
.. |htmltlpgreen| raw:: html {}
.. |htmltlpclear| raw:: html {}
.. |cover_image| replace:: {}
.. |customer_logo_image| replace:: {}
""".format(
    tlpred,
    tlpamber,
    tlpamberstrict,
    tlpgreen,
    tlpclear,
    html_tlpred,
    html_tlpamber,
    html_tlpamberstrict,
    html_tlpgreen,
    html_tlpclear,
    cover_path,
    customer_logo_path,
)

title = r"""
    \begin{{titlepage}}
    \begin{{tikzpicture}}[remember picture,overlay]
    \draw  let \p1=($(current page.north)-(current page header area.south)$),
          \n1={{veclen(\x1,\y1)}} in
    node [inner sep=0,outer sep=0,below right]
          at (current page.north west){{\includegraphics[width=\paperwidth,height=5in]{{{}}}}};
    \end{{tikzpicture}}\\
    \vspace{{6cm}}\\
    \begin{{tabular*}}{{\textwidth}}{{@{{}}l@{{\extracolsep{{\fill}}}}r@{{}}}}
     \textbf{{\Large{{Systemhärtung für Linux Basissysteme}}}}
    \vspace{{0.7cm}}\\
    \emph{{Pierre Gronau}}\\
    \vspace{{1.7cm}}\\
    \emph{{TLP:GREEN}}\\
    &
     \includegraphics[width=.20\textwidth]{{{}}}
    \end{{tabular*}}
    \end{{titlepage}}
                """.format(
    cover_path, customer_logo_path
)

latex_elements = {
    # 'sphinxsetup': "",
    "sphinxsetup": "VerbatimHighlightColor={RGB}{178,255,102},warningBorderColor={RGB}{220,38,38},cautionBorderColor={RGB}{202,138,4},warningBgColor={RGB}{254,242,242},cautionBgColor={RGB}{254,252,232},noteBorderColor={RGB}{2,132,199},noteBgColor={RGB}{240,249,255},noteTextColor={RGB}{12,74,110},div.note_border-radius=5pt,div.caution_border-radius=5pt,div.warning_border-radius=5pt,div.warning_TeXcolor={RGB}{127,29,29},div.caution_TeXcolor={RGB}{113,63,18}",
    "papersize": "a4paper",
    "releasename": "",
    "figure_align": "htbp",
    "pointsize": "12pt",
    "extraclassoptions": "openany,oneside",
    "preamble": mypreamble,
    # To generate a differently styled title page.
    "maketitle": title,
}

panels_add_fontawesome_latex = True

# -- Options for Epub output ---------------------------------------------------

# Bibliographic Dublin Core info.
# filename
# epub_basename = project.replace(' ', '_') + '_' + language
epub_title = project + " " + version
epub_description = description

# Technically speaking dublincore accepts multiple author and contributor elements, but
# the sphinx builder only accepts one.
epub_author = author
epub_publisher = author
epub_copyright = copyright

# The scheme of the identifier. Typical schemes are ISBN or URL.
epub_scheme = "URL"
# The language of the text. It defaults to the language option
# or 'en' if the language is not set.
epub_language = "en"

# The unique identifier of the text. This can be a ISBN number
# or the project homepage.
# epub_identifier = ''

# A unique identification for the text.
# epub_uid = ''

# A tuple containing the cover image and cover page html template filenames.
# epub_cover = ('_static/Cover.png')

# epub_theme = 'epub2'

# HTML files that should be inserted before the pages created by sphinx.
# The format is a list of tuples containing the path and title.
# epub_pre_files = []

# HTML files shat should be inserted after the pages created by sphinx.
# The format is a list of tuples containing the path and title.
# epub_post_files = []

# A list of files that should not be packed into the epub file.
epub_exclude_files = [
    "_static/opensearch.xml",
    "_static/doctools.js",
    "_static/jquery.js",
    "_static/searchtools.js",
    "_static/underscore.js",
    "_static/basic.css",
    "search.html",
    "_static/websupport.js",
    "_branding",
    "build",
    "_input",
]

# The depth of the table of contents in toc.ncx.
epub_tocdepth = 2

# Allow duplicate toc entries.
epub_tocdup = False

# Choose between 'default' and 'includehidden'.
epub_tocscope = "default"

# Fix unsupported image types using the Pillow.
# epub_fix_images = False

# Scale large images.
epub_max_image_width = 0

# How to display URL addresses: 'footnote', 'no', or 'inline'.
epub_show_urls = "footnote"

# If false, no index is generated.
epub_use_index = True

# -- Options for HTMLHelp output ------------------------------------------

# Output file base name for HTML help builder.
htmlhelp_basename = "%s" % project

bibtex_bibfiles = ["my-references.bib"]

html_sourcelink_suffix = ""

# -- Options for PlantUML ------------------------------------------

sphinxmark_enable = True

# -- Options for bibtex ------------------------------------------

bibtex_bibfiles = ["refs.bib"]

# -- Options for sphinx-needs ------------------------------------------
#
# https://sphinxcontrib-needs.readthedocs.io/en/latest/configuration.html
#
# Default: True
#
needs_include_needs = True

# needs_id_length¶
# This option defines the length of an automated generated ID (the length of the prefix does not count).
#
# Default: 5
#
needs_id_length = 5

#
# needs_types¶
# The option allows the setup of own need types like bugs, user_stories and more.
#
# By default it is set to:

needs_types = [
    dict(
        directive="req", title="Requirement", prefix="R_", color="#BFD8D2", style="node"
    ),
    dict(
        directive="spec",
        title="Specification",
        prefix="S_",
        color="#FEDCD2",
        style="node",
    ),
    dict(
        directive="impl",
        title="Implementation",
        prefix="I_",
        color="#DF744A",
        style="node",
    ),
    dict(
        directive="test", title="Test Case", prefix="T_", color="#DCB239", style="node"
    ),
    # Kept for backwards compatibility
    dict(directive="need", title="Need", prefix="N_", color="#9856a5", style="node"),
]

# needs_types must be a list of dictionaries where each dictionary must contain the following items:
#
# directive: Name of the directive. For instance, you can use “req” via .. req:: in documents
#
# title: Title, used as human readable name in lists
#
# prefix: A prefix for generated IDs, to easily identify that an ID belongs to a specific type. Can also be “”
#
# color: A color as hex value. Used in diagrams and some days maybe in other representations as well. Can also be “”
#
# style: A plantuml node type, like node, artifact, frame, storage or database. See plantuml documentation for more.

needs_extra_options = ["introduced", "updated", "impacts", "main"]

# -- Options for Napoleon ------------------------------------------
#
# https://www.sphinx-doc.org/en/master/usage/extensions/napoleon.html
#
napoleon_google_docstring = True
napoleon_numpy_docstring = True
napoleon_include_init_with_doc = True
napoleon_include_private_with_doc = True
napoleon_include_special_with_doc = True
napoleon_use_admonition_for_examples = False
napoleon_use_admonition_for_notes = False
napoleon_use_admonition_for_references = False
napoleon_use_ivar = False
napoleon_use_param = True
napoleon_use_rtype = True

html_baseurl = "/documentation/"
