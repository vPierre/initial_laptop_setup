CIS Debian Linux 11 Benchmark v1.0.0 Level 2 - Server 
==============================================================================


License 
-----------------------

Please see our terms of use here: https://www.cisecurity.org/cis-securesuite/cis-securesuite-membership-terms-of-use/

Section #: 1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:**  

**Title:** Initial Setup

**Assessment Status:**  

**Description:** Items in this section are advised for all systems, but may be difficult or require extensive preparation after the initial setup of the system.

**Rational Statement:**  

**Impact Statement:**  

**Remediation Procedure:**  

**Audit Procedure:**  

**Additional Information:**  

**CIS Controls:**  

**CIS Safeguards 1 (v8):**  

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:**  

**v8 IG2:**  

**v8 IG3:**  

**CIS Safeguards 1 (v7):**  

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:**  

**v7 IG2:**  

**v7 IG3:**  

**References:**  


Section #: 1.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:**  

**Title:** Filesystem Configuration

**Assessment Status:**  

**Description:** Directories that are used for system-wide functions can be further protected by placing them on separate partitions. This provides protection for resource exhaustion and enables the use of mounting options that are applicable to the directory's intended use. Users' data can be stored on separate partitions and have stricter mount options. A user partition is a filesystem that has been established for use by the users and does not contain software for system operations.

The recommendations in this section are easier to perform during initial system installation. If the system is already installed, it is recommended that a full backup be performed before repartitioning the system.

**Note:** If you are repartitioning a system that has already been installed, make sure the data has been copied over to the new partition, unmount it and then remove the data from the directory that was in the old partition. Otherwise it will still consume space in the old partition that will be masked when the new filesystem is mounted. For example, if a system is in single-user mode with no filesystems mounted and the administrator adds a lot of data to the `/tmp` directory, this data will still consume space in `/` once the `/tmp` filesystem is mounted unless it is removed first.

**Rational Statement:**  

**Impact Statement:**  

**Remediation Procedure:**  

**Audit Procedure:**  

**Additional Information:**  

**CIS Controls:**  

**CIS Safeguards 1 (v8):**  

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:**  

**v8 IG2:**  

**v8 IG3:**  

**CIS Safeguards 1 (v7):**  

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:**  

**v7 IG2:**  

**v7 IG3:**  

**References:**  


Section #: 1.1.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:**  

**Title:** Disable unused filesystems

**Assessment Status:**  

**Description:** A number of uncommon filesystem types are supported under Linux. Removing support for unneeded filesystem types reduces the local attack surface of the system. If a filesystem type is not needed it should be disabled. Native Linux file systems are designed to ensure that built-in security controls function as expected. Non-native filesystems can lead to unexpected consequences to both the security and functionality of the system and should be used with caution. Many filesystems are created for niche use cases and are not maintained and supported as the operating systems are updated and patched. Users of non-native filesystems should ensure that there is attention and ongoing support for them, especially in light of frequent operating system changes.

Standard network connectivity and Internet access to cloud storage may make the use of non-standard filesystem formats to directly attach heterogeneous devices much less attractive.

**Note**: This should not be considered a comprehensive list of filesystems. You may wish to consider additions to those listed here for your environment. For the current available file system modules on the system see `/usr/lib/modules/$(uname -r)/kernel/fs`

#### Start up scripts

Kernel modules loaded directly via `insmod` will ignore what is configured in the relevant `/etc/modprobe.d/*.conf` files. If modules are still being loaded after a reboot whilst having the correctly configured `blacklist` and `install` command, check for `insmod` entries in start up scripts such as `.bashrc`.

You may also want to check `/usr/lib/modprobe.d/`. Please note that this directory should not be used for user defined module loading. Ensure that all such entries resides in `/etc/modprobe.d/*.conf` files.

#### Return values

By using `/bin/false` as the command in disabling a particular module serves two purposes; to convey the meaning of the entry to the user and cause a non-zero return value. The latter can be tested for in scripts. Please note that `insmod` will ignore what is configured in the relevant `/etc/modprobe.d/*.conf` files. The preferred way to load modules is with `modprobe`.

**Rational Statement:**  

**Impact Statement:**  

**Remediation Procedure:**  

**Audit Procedure:**  

**Additional Information:**  

**CIS Controls:**  

**CIS Safeguards 1 (v8):**  

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:**  

**v8 IG2:**  

**v8 IG3:**  

**CIS Safeguards 1 (v7):**  

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:**  

**v7 IG2:**  

**v7 IG3:**  

**References:**  


Section #: 1.1.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.1.2

**Title:** Ensure mounting of squashfs filesystems is disabled

**Assessment Status:** Automated

**Description:** The `squashfs` filesystem type is a compressed read-only Linux filesystem embedded in small footprint systems. A `squashfs` image can be used without having to first decompress the image.

**Rational Statement:** Removing support for unneeded filesystem types reduces the local attack surface of the system. If this filesystem type is not needed, disable it.

**Impact Statement:** As Snap packages utilizes `squashfs` as a compressed filesystem, disabling `squashfs` will cause Snap packages to fail.

`Snap` application packages of software are self-contained and work across a range of Linux distributions. This is unlike traditional Linux package management approaches, like APT or RPM, which require specifically adapted packages per Linux distribution on an application update and delay therefore application deployment from developers to their software's end-user. Snaps themselves have no dependency on any external store ("App store"), can be obtained from any source and can be therefore used for upstream software deployment.

**Remediation Procedure:** Run the following script to disable `squashfs`:



.. code-block:: bash
    #!/usr/bin/env bash

    {
    l_mname="squashfs" # set module name
    # Check if the module exists on the system
    if [ -z "$(modprobe -n -v "$l_mname" 2>&1 | grep -Pi -- "\h*modprobe:\h+FATAL:\h+Module\h+$l_mname\h+not\h+found\h+in\h+directory")" ]; then
    # Remediate loadable
    l_loadable="$(modprobe -n -v "$l_mname")"
    [ "$(wc -l <<< "$l_loadable")" -gt "1" ] && l_loadable="$(grep -P -- "(^\h*install|\b$l_mname)\b" <<< "$l_loadable")"
    if ! grep -Pq -- '^\h*install \/bin\/(true|false)' <<< "$l_loadable"; then
    echo -e " - setting module: \"$l_mname\" to be not loadable"
    echo -e "install $l_mname /bin/false" >> /etc/modprobe.d/"$l_mname".conf
    fi
    # Remediate loaded
    if lsmod | grep "$l_mname" > /dev/null 2> then
    echo -e " - unloading module \"$l_mname\""
    modprobe -r "$l_mname"
    fi
    # Remediate deny list
    if ! modprobe --showconfig | grep -Pq -- "^\h*blacklist\h+$l_mname\b"; then
    echo -e " - deny listing \"$l_mname\""
    echo -e "blacklist $l_mname" >> /etc/modprobe.d/"$l_mname".conf
    fi
    else
    echo -e " - Nothing to remediate\n - Module \"$l_mname\" doesn't exist on the system"
    fi
    }


**Audit Procedure:** Run the following script to verify `squashfs` is disabled:

.. code-block:: bash
    #!/usr/bin/env bash

    {
    l_output="" l_output2=""
    l_mname="squashfs" # set module name
    # Check if the module exists on the system
    if [ -z "$(modprobe -n -v "$l_mname" 2>&1 | grep -Pi -- "\h*modprobe:\h+FATAL:\h+Module\h+$l_mname\h+not\h+found\h+in\h+directory")" ]; then
    # Check how module will be loaded
    l_loadable="$(modprobe -n -v "$l_mname")"
    [ "$(wc -l <<< "$l_loadable")" -gt "1" ] && l_loadable="$(grep -P -- "(^\h*install|\b$l_mname)\b" <<< "$l_loadable")"
    if grep -Pq -- '^\h*install \/bin\/(true|false)' <<< "$l_loadable"; then
    l_output="$l_output\n - module: \"$l_mname\" is not loadable: \"$l_loadable\""
    else
    l_output2="$l_output2\n - module: \"$l_mname\" is loadable: \"$l_loadable\""
    fi
    # Check is the module currently loaded
    if ! lsmod | grep "$l_mname" > /dev/null 2> then
    l_output="$l_output\n - module: \"$l_mname\" is not loaded"
    else
    l_output2="$l_output2\n - module: \"$l_mname\" is loaded"
    fi
    # Check if the module is deny listed
    if modprobe --showconfig | grep -Pq -- "^\h*blacklist\h+$l_mname\b"; then
    l_output="$l_output\n - module: \"$l_mname\" is deny listed in: \"$(grep -Pl -- "^\h*blacklist\h+$l_mname\b" /etc/modprobe.d/*)\""
    else
    l_output2="$l_output2\n - module: \"$l_mname\" is not deny listed"
    fi
    else
    l_output="$l_output\n - Module \"$l_mname\" doesn't exist on the system"
    fi
    # Report results. If no failures output in l_output2, we pass
    if [ -z "$l_output2" ]; then
    echo -e "\n- Audit Result:\n ** PASS **\n$l_output\n"
    else
    echo -e "\n- Audit Result:\n ** FAIL **\n - Reason(s) for audit failure:\n$l_output2\n"
    [ -n "$l_output" ] && echo -e "\n- Correctly set:\n$l_output\n"
    fi
    }


**Additional Information:**  

**CIS Controls:** TITLE:Uninstall or Disable Unnecessary Services on Enterprise Assets and Software CONTROL:v8 4.8 DESCRIPTION:Uninstall or disable unnecessary services on enterprise assets and software, such as an unused file sharing service, web application module, or service function.;TITLE:Ensure Only Approved Ports, Protocols and Services Are Running CONTROL:v7 9.2 DESCRIPTION:Ensure that only network ports, protocols, and services listening on a system with validated business needs, are running on each system.;

**CIS Safeguards 1 (v8):** 4.8

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:**  

**v8 IG2:** X

**v8 IG3:** X

**CIS Safeguards 1 (v7):** 9.2

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:**  

**v7 IG2:** X

**v7 IG3:** X

**References:**  


Section #: 1.1.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.1.3

**Title:** Ensure mounting of udf filesystems is disabled

**Assessment Status:** Automated

**Description:** The `udf` filesystem type is the universal disk format used to implement ISO/IEC 13346 and ECMA-167 specifications. This is an open vendor filesystem type for data storage on a broad range of media. This filesystem type is necessary to support writing DVDs and newer optical disc formats.

**Rational Statement:** Removing support for unneeded filesystem types reduces the local attack surface of the system. If this filesystem type is not needed, disable it.

**Impact Statement:** Microsoft Azure requires the usage of `udf`. 

`udf` should not be disabled on systems run on Microsoft Azure.

**Remediation Procedure:** Run the following script to disable the `udf` filesystem:

.. code-block:: bash
    #!/usr/bin/env bash

    {
    l_mname="udf" # set module name
    # Check if the module exists on the system
    if [ -z "$(modprobe -n -v "$l_mname" 2>&1 | grep -Pi -- "\h*modprobe:\h+FATAL:\h+Module\h+$l_mname\h+not\h+found\h+in\h+directory")" ]; then
    # Remediate loadable
    l_loadable="$(modprobe -n -v "$l_mname")"
    [ "$(wc -l <<< "$l_loadable")" -gt "1" ] && l_loadable="$(grep -P -- "(^\h*install|\b$l_mname)\b" <<< "$l_loadable")"
    if ! grep -Pq -- '^\h*install \/bin\/(true|false)' <<< "$l_loadable"; then
    echo -e " - setting module: \"$l_mname\" to be not loadable"
    echo -e "install $l_mname /bin/false" >> /etc/modprobe.d/"$l_mname".conf
    fi
    # Remediate loaded
    if lsmod | grep "$l_mname" > /dev/null 2> then
    echo -e " - unloading module \"$l_mname\""
    modprobe -r "$l_mname"
    fi
    # Remediate deny list
    if ! modprobe --showconfig | grep -Pq -- "^\h*blacklist\h+$l_mname\b"; then
    echo -e " - deny listing \"$l_mname\""
    echo -e "blacklist $l_mname" >> /etc/modprobe.d/"$l_mname".conf
    fi
    else
    echo -e " - Nothing to remediate\n - Module \"$l_mname\" doesn't exist on the system"
    fi
    }


**Audit Procedure:** Run the following script to verify `udf` is disabled:

.. code-block:: bash
    #!/usr/bin/env bash

    {
    l_output="" l_output2=""
    l_mname="udf" # set module name
    # Check if the module exists on the system
    if [ -z "$(modprobe -n -v "$l_mname" 2>&1 | grep -Pi -- "\h*modprobe:\h+FATAL:\h+Module\h+$l_mname\h+not\h+found\h+in\h+directory")" ]; then
    # Check how module will be loaded
    l_loadable="$(modprobe -n -v "$l_mname")"
    [ "$(wc -l <<< "$l_loadable")" -gt "1" ] && l_loadable="$(grep -P -- "(^\h*install|\b$l_mname)\b" <<< "$l_loadable")"
    if grep -Pq -- '^\h*install \/bin\/(true|false)' <<< "$l_loadable"; then
    l_output="$l_output\n - module: \"$l_mname\" is not loadable: \"$l_loadable\""
    else
    l_output2="$l_output2\n - module: \"$l_mname\" is loadable: \"$l_loadable\""
    fi
    # Check is the module currently loaded
    if ! lsmod | grep "$l_mname" > /dev/null 2> then
    l_output="$l_output\n - module: \"$l_mname\" is not loaded"
    else
    l_output2="$l_output2\n - module: \"$l_mname\" is loaded"
    fi
    # Check if the module is deny listed
    if modprobe --showconfig | grep -Pq -- "^\h*blacklist\h+$l_mname\b"; then
    l_output="$l_output\n - module: \"$l_mname\" is deny listed in: \"$(grep -Pl -- "^\h*blacklist\h+$l_mname\b" /etc/modprobe.d/*)\""
    else
    l_output2="$l_output2\n - module: \"$l_mname\" is not deny listed"
    fi
    else
    l_output="$l_output\n - Module \"$l_mname\" doesn't exist on the system"
    fi
    # Report results. If no failures output in l_output2, we pass
    if [ -z "$l_output2" ]; then
    echo -e "\n- Audit Result:\n ** PASS **\n$l_output\n"
    else
    echo -e "\n- Audit Result:\n ** FAIL **\n - Reason(s) for audit failure:\n$l_output2\n"
    [ -n "$l_output" ] && echo -e "\n- Correctly set:\n$l_output\n"
    fi
    }


**Additional Information:**  

**CIS Controls:** TITLE:Uninstall or Disable Unnecessary Services on Enterprise Assets and Software CONTROL:v8 4.8 DESCRIPTION:Uninstall or disable unnecessary services on enterprise assets and software, such as an unused file sharing service, web application module, or service function.;TITLE:Ensure Only Approved Ports, Protocols and Services Are Running CONTROL:v7 9.2 DESCRIPTION:Ensure that only network ports, protocols, and services listening on a system with validated business needs, are running on each system.;

**CIS Safeguards 1 (v8):** 4.8

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:**  

**v8 IG2:** X

**v8 IG3:** X

**CIS Safeguards 1 (v7):** 9.2

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:**  

**v7 IG2:** X

**v7 IG3:** X

**References:**  


Section #: 1.1.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:**  

**Title:** Configure /tmp

**Assessment Status:**  

**Description:** The /tmp directory is a world-writable directory used for temporary storage by all users and some applications.

**Rational Statement:**  

**Impact Statement:**  

**Remediation Procedure:**  

**Audit Procedure:**  

**Additional Information:**  

**CIS Controls:**  

**CIS Safeguards 1 (v8):**  

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:**  

**v8 IG2:**  

**v8 IG3:**  

**CIS Safeguards 1 (v7):**  

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:**  

**v7 IG2:**  

**v7 IG3:**  

**References:**  


Section #: 1.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:**  

**Title:** Configure /var

**Assessment Status:**  

**Description:** The `/var` directory is used by daemons and other system services to temporarily store dynamic data. Some directories created by these processes may be world-writable.

**Rational Statement:**  

**Impact Statement:**  

**Remediation Procedure:**  

**Audit Procedure:**  

**Additional Information:**  

**CIS Controls:**  

**CIS Safeguards 1 (v8):**  

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:**  

**v8 IG2:**  

**v8 IG3:**  

**CIS Safeguards 1 (v7):**  

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:**  

**v7 IG2:**  

**v7 IG3:**  

**References:**  


Section #: 1.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.3.1

**Title:** Ensure separate partition exists for /var

**Assessment Status:** Automated

**Description:** The `/var` directory is used by daemons and other system services to temporarily store dynamic data. Some directories created by these processes may be world-writable.

**Rational Statement:** The reasoning for mounting `/var` on a separate partition is as follow.

#### Protection from resource exhaustion
The default installation only creates a single `/` partition. Since the `/var` directory may contain world-writable files and directories, there is a risk of resource exhaustion. It will essentially have the whole disk available to fill up and impact the system as a whole. In addition, other operations on the system could fill up the disk unrelated to `/var` and cause unintended behavior across the system as the disk is full. See `man auditd.conf` for details.

#### Fine grained control over the mount
Configuring `/var` as its own file system allows an administrator to set additional mount options such as `noexec/nosuid/nodev`. These options limits an attackers ability to create exploits on the system. Other options allow for specific behaviour. See `man mount` for exact details regarding filesystem-independent and filesystem-specific options.

#### Protection from exploitation
An example of exploiting `/var` may be an attacker establishing a hard-link to a system `setuid` program and wait for it to be updated. Once the program was updated, the hard-link would be broken and the attacker would have his own copy of the program. If the program happened to have a security vulnerability, the attacker could continue to exploit the known flaw.

**Impact Statement:** Resizing filesystems is a common activity in cloud-hosted servers. Separate filesystem partitions may prevent successful resizing, or may require the installation of additional tools solely for the purpose of resizing operations. The use of these additional tools may introduce their own security considerations.

**Remediation Procedure:** For new installations, during installation create a custom partition setup and specify a separate partition for `/var`.

For systems that were previously installed, create a new partition and configure `/etc/fstab` as appropriate.

**Audit Procedure:** Run the following command and verify output shows `/var` is mounted.

Example:


# findmnt --kernel /var

TARGET SOURCE FSTYPE OPTIONS
/var /dev/sdb ext4 rw,relatime,seclabel,data=ordered


**Additional Information:** When modifying `/var` it is advisable to bring the system to emergency mode (so auditd is not running), rename the existing directory, mount the new file system, and migrate the data over before returning to multi-user mode.

**CIS Controls:** TITLE:Configure Data Access Control Lists CONTROL:v8 3.3 DESCRIPTION:Configure data access control lists based on a user’s need to know. Apply data access control lists, also known as access permissions, to local and remote file systems, databases, and applications.;TITLE:Protect Information through Access Control Lists CONTROL:v7 14.6 DESCRIPTION:Protect all information stored on systems with file system, network share, claims, application, or database specific access control lists. These controls will enforce the principle that only authorized individuals should have access to the information based on their need to access the information as a part of their responsibilities.;

**CIS Safeguards 1 (v8):** 3.3

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:** X

**v8 IG2:** X

**v8 IG3:** X

**CIS Safeguards 1 (v7):** 14.6

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:** X

**v7 IG2:** X

**v7 IG3:** X

**References:** AJ Lewis, "LVM HOWTO", http://tldp.org/HOWTO/LVM-HOWTO/


Section #: 1.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:**  

**Title:** Configure /var/tmp

**Assessment Status:**  

**Description:** The `/var/tmp` directory is a world-writable directory used for temporary storage by all users and some applications. Temporary files residing in `/var/tmp` are to be preserved between reboots.

**Rational Statement:**  

**Impact Statement:**  

**Remediation Procedure:**  

**Audit Procedure:**  

**Additional Information:**  

**CIS Controls:**  

**CIS Safeguards 1 (v8):**  

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:**  

**v8 IG2:**  

**v8 IG3:**  

**CIS Safeguards 1 (v7):**  

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:**  

**v7 IG2:**  

**v7 IG3:**  

**References:**  


Section #: 1.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.4.1

**Title:** Ensure separate partition exists for /var/tmp

**Assessment Status:** Automated

**Description:** The `/var/tmp` directory is a world-writable directory used for temporary storage by all users and some applications. Temporary files residing in `/var/tmp` are to be preserved between reboots.

**Rational Statement:** The reasoning for mounting `/var/tmp` on a separate partition is as follows.

#### Protection from resource exhaustion
The default installation only creates a single `/` partition. Since the `/var/tmp` directory may contain world-writable files and directories, there is a risk of resource exhaustion. It will essentially have the whole disk available to fill up and impact the system as a whole. In addition, other operations on the system could fill up the disk unrelated to `/var/tmp` and cause the potential disruption to daemons as the disk is full.

#### Fine grained control over the mount
Configuring `/var/tmp` as its own file system allows an administrator to set additional mount options such as `noexec/nosuid/nodev`. These options limits an attackers ability to create exploits on the system. Other options allow for specific behavior. See `man mount` for exact details regarding filesystem-independent and filesystem-specific options.

#### Protection from exploitation
An example of exploiting `/var/tmp` may be an attacker establishing a hard-link to a system `setuid` program and wait for it to be updated. Once the program was updated, the hard-link would be broken and the attacker would have his own copy of the program. If the program happened to have a security vulnerability, the attacker could continue to exploit the known flaw.

**Impact Statement:** Resizing filesystems is a common activity in cloud-hosted servers. Separate filesystem partitions may prevent successful resizing, or may require the installation of additional tools solely for the purpose of resizing operations. The use of these additional tools may introduce their own security considerations.

**Remediation Procedure:** For new installations, during installation create a custom partition setup and specify a separate partition for `/var/tmp`.

For systems that were previously installed, create a new partition and configure `/etc/fstab` as appropriate.

**Audit Procedure:** Run the following command and verify output shows `/var/tmp` is mounted.

Example:


# findmnt --kernel /var/tmp

TARGET SOURCE FSTYPE OPTIONS
/var/tmp /dev/sdb ext4 rw,relatime,seclabel,data=ordered


**Additional Information:** When modifying `/var/tmp` it is advisable to bring the system to emergency mode (so auditd is not running), rename the existing directory, mount the new file system, and migrate the data over before returning to multi-user mode.

**CIS Controls:** TITLE:Configure Data Access Control Lists CONTROL:v8 3.3 DESCRIPTION:Configure data access control lists based on a user’s need to know. Apply data access control lists, also known as access permissions, to local and remote file systems, databases, and applications.;TITLE:Protect Information through Access Control Lists CONTROL:v7 14.6 DESCRIPTION:Protect all information stored on systems with file system, network share, claims, application, or database specific access control lists. These controls will enforce the principle that only authorized individuals should have access to the information based on their need to access the information as a part of their responsibilities.;

**CIS Safeguards 1 (v8):** 3.3

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:** X

**v8 IG2:** X

**v8 IG3:** X

**CIS Safeguards 1 (v7):** 14.6

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:** X

**v7 IG2:** X

**v7 IG3:** X

**References:** AJ Lewis, "LVM HOWTO", http://tldp.org/HOWTO/LVM-HOWTO/


Section #: 1.1.5
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:**  

**Title:** Configure /var/log

**Assessment Status:**  

**Description:** The `/var/log` directory is used by system services to store log data.

**Rational Statement:**  

**Impact Statement:**  

**Remediation Procedure:**  

**Audit Procedure:**  

**Additional Information:**  

**CIS Controls:**  

**CIS Safeguards 1 (v8):**  

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:**  

**v8 IG2:**  

**v8 IG3:**  

**CIS Safeguards 1 (v7):**  

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:**  

**v7 IG2:**  

**v7 IG3:**  

**References:**  


Section #: 1.1.5
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.5.1

**Title:** Ensure separate partition exists for /var/log

**Assessment Status:** Automated

**Description:** The `/var/log` directory is used by system services to store log data.

**Rational Statement:** The reasoning for mounting `/var/log` on a separate partition is as follows.

#### Protection from resource exhaustion
The default installation only creates a single `/` partition. Since the `/var/log` directory contains log files which can grow quite large, there is a risk of resource exhaustion. It will essentially have the whole disk available to fill up and impact the system as a whole.

#### Fine grained control over the mount
Configuring `/var/log` as its own file system allows an administrator to set additional mount options such as `noexec/nosuid/nodev`. These options limits an attackers ability to create exploits on the system. Other options allow for specific behavior. See `man mount` for exact details regarding filesystem-independent and filesystem-specific options.

#### Protection of log data
As `/var/log` contains log files, care should be taken to ensure the security and integrity of the data and mount point.

**Impact Statement:** Resizing filesystems is a common activity in cloud-hosted servers. Separate filesystem partitions may prevent successful resizing, or may require the installation of additional tools solely for the purpose of resizing operations. The use of these additional tools may introduce their own security considerations.

**Remediation Procedure:** For new installations, during installation create a custom partition setup and specify a separate partition for `/var/log` .

For systems that were previously installed, create a new partition and configure `/etc/fstab` as appropriate.

**Audit Procedure:** Run the following command and verify output shows `/var/log` is mounted:


# findmnt --kernel /var/log

TARGET SOURCE FSTYPE OPTIONS
/var/log /dev/sdb ext4 rw,relatime,seclabel,data=ordered


**Additional Information:** When modifying `/var/log` it is advisable to bring the system to emergency mode (so auditd is not running), rename the existing directory, mount the new file system, and migrate the data over before returning to multiuser mode.

**CIS Controls:** TITLE:Ensure Adequate Audit Log Storage CONTROL:v8 8.3 DESCRIPTION:Ensure that logging destinations maintain adequate storage to comply with the enterprise’s audit log management process.;TITLE:Ensure adequate storage for logs CONTROL:v7 6.4 DESCRIPTION:Ensure that all systems that store logs have adequate storage space for the logs generated.;

**CIS Safeguards 1 (v8):** 8.3

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:** X

**v8 IG2:** X

**v8 IG3:** X

**CIS Safeguards 1 (v7):** 6.4

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:**  

**v7 IG2:** X

**v7 IG3:** X

**References:** AJ Lewis, "LVM HOWTO", http://tldp.org/HOWTO/LVM-HOWTO/


Section #: 1.1.6
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:**  

**Title:** Configure /var/log/audit

**Assessment Status:**  

**Description:** The auditing daemon, `auditd`, stores log data in the `/var/log/audit` directory.

**Rational Statement:**  

**Impact Statement:**  

**Remediation Procedure:**  

**Audit Procedure:**  

**Additional Information:**  

**CIS Controls:**  

**CIS Safeguards 1 (v8):**  

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:**  

**v8 IG2:**  

**v8 IG3:**  

**CIS Safeguards 1 (v7):**  

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:**  

**v7 IG2:**  

**v7 IG3:**  

**References:**  


Section #: 1.1.6
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.6.1

**Title:** Ensure separate partition exists for /var/log/audit

**Assessment Status:** Automated

**Description:** The auditing daemon, `auditd`, stores log data in the `/var/log/audit` directory.

**Rational Statement:** The reasoning for mounting `/var/log/audit` on a separate partition is as follows.

#### Protection from resource exhaustion
The default installation only creates a single `/` partition. Since the `/var/log/audit` directory contains the `audit.log` file which can grow quite large, there is a risk of resource exhaustion. It will essentially have the whole disk available to fill up and impact the system as a whole. In addition, other operations on the system could fill up the disk unrelated to `/var/log/audit` and cause `auditd` to trigger it's `space_left_action` as the disk is full. See `man auditd.conf` for details.

#### Fine grained control over the mount
Configuring `/var/log/audit` as its own file system allows an administrator to set additional mount options such as `noexec/nosuid/nodev`. These options limits an attackers ability to create exploits on the system. Other options allow for specific behavior. See `man mount` for exact details regarding filesystem-independent and filesystem-specific options.

#### Protection of audit data
As `/var/log/audit` contains audit logs, care should be taken to ensure the security and integrity of the data and mount point.

**Impact Statement:** Resizing filesystems is a common activity in cloud-hosted servers. Separate filesystem partitions may prevent successful resizing, or may require the installation of additional tools solely for the purpose of resizing operations. The use of these additional tools may introduce their own security considerations.

**Remediation Procedure:** For new installations, during installation create a custom partition setup and specify a separate partition for `/var/log/audit`.

For systems that were previously installed, create a new partition and configure `/etc/fstab` as appropriate.

**Audit Procedure:** Run the following command and verify output shows `/var/log/audit` is mounted:


# findmnt --kernel /var/log/audit

TARGET SOURCE FSTYPE OPTIONS
/var/log/audit /dev/sdb ext4 rw,relatime,seclabel,data=ordered


**Additional Information:** When modifying `/var/log/audit` it is advisable to bring the system to emergency mode (so auditd is not running), rename the existing directory, mount the new file system, and migrate the data over before returning to multi-user mode.

**CIS Controls:** TITLE:Ensure Adequate Audit Log Storage CONTROL:v8 8.3 DESCRIPTION:Ensure that logging destinations maintain adequate storage to comply with the enterprise’s audit log management process.;TITLE:Ensure adequate storage for logs CONTROL:v7 6.4 DESCRIPTION:Ensure that all systems that store logs have adequate storage space for the logs generated.;

**CIS Safeguards 1 (v8):** 8.3

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:** X

**v8 IG2:** X

**v8 IG3:** X

**CIS Safeguards 1 (v7):** 6.4

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:**  

**v7 IG2:** X

**v7 IG3:** X

**References:** AJ Lewis, "LVM HOWTO", http://tldp.org/HOWTO/LVM-HOWTO/


Section #: 1.1.7
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:**  

**Title:** Configure /home

**Assessment Status:**  

**Description:** Please note that home directories could be mounted anywhere and are not necessarily restricted to `/home` nor restricted to a single location, nor is the name restricted in any way.

Checks can be made by looking in `/etc/passwd`, looking over the mounted file systems with `mount` or querying the relevant database with `getent`.

**Rational Statement:**  

**Impact Statement:**  

**Remediation Procedure:**  

**Audit Procedure:**  

**Additional Information:**  

**CIS Controls:**  

**CIS Safeguards 1 (v8):**  

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:**  

**v8 IG2:**  

**v8 IG3:**  

**CIS Safeguards 1 (v7):**  

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:**  

**v7 IG2:**  

**v7 IG3:**  

**References:**  


Section #: 1.1.7
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.1.7.1

**Title:** Ensure separate partition exists for /home

**Assessment Status:** Automated

**Description:** The `/home` directory is used to support disk storage needs of local users.

**Rational Statement:** The reasoning for mounting `/home` on a separate partition is as follows.

#### Protection from resource exhaustion
The default installation only creates a single `/` partition. Since the `/home` directory contains user generated data, there is a risk of resource exhaustion. It will essentially have the whole disk available to fill up and impact the system as a whole. In addition, other operations on the system could fill up the disk unrelated to `/home` and impact all local users.

#### Fine grained control over the mount
Configuring `/home` as its own file system allows an administrator to set additional mount options such as `noexec/nosuid/nodev`. These options limits an attackers ability to create exploits on the system. In the case of `/home` options such as `usrquota/grpquota` may be considered to limit the impact that users can have on each other with regards to disk resource exhaustion. Other options allow for specific behavior. See `man mount` for exact details regarding filesystem-independent and filesystem-specific options.

#### Protection of user data
As `/home` contains user data, care should be taken to ensure the security and integrity of the data and mount point.

**Impact Statement:** Resizing filesystems is a common activity in cloud-hosted servers. Separate filesystem partitions may prevent successful resizing, or may require the installation of additional tools solely for the purpose of resizing operations. The use of these additional tools may introduce their own security considerations.

**Remediation Procedure:** For new installations, during installation create a custom partition setup and specify a separate partition for `/home`.

For systems that were previously installed, create a new partition and configure `/etc/fstab` as appropriate.

**Audit Procedure:** Run the following command and verify output shows `/home` is mounted:


# findmnt --kernel /home

TARGET SOURCE FSTYPE OPTIONS
/home /dev/sdb ext4 rw,relatime,seclabel


**Additional Information:** When modifying `/home` it is advisable to bring the system to emergency mode (so auditd is not running), rename the existing directory, mount the new file system, and migrate the data over before returning to multi-user mode.

**CIS Controls:** TITLE:Configure Data Access Control Lists CONTROL:v8 3.3 DESCRIPTION:Configure data access control lists based on a user’s need to know. Apply data access control lists, also known as access permissions, to local and remote file systems, databases, and applications.;TITLE:Protect Information through Access Control Lists CONTROL:v7 14.6 DESCRIPTION:Protect all information stored on systems with file system, network share, claims, application, or database specific access control lists. These controls will enforce the principle that only authorized individuals should have access to the information based on their need to access the information as a part of their responsibilities.;

**CIS Safeguards 1 (v8):** 3.3

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:** X

**v8 IG2:** X

**v8 IG3:** X

**CIS Safeguards 1 (v7):** 14.6

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:** X

**v7 IG2:** X

**v7 IG3:** X

**References:** AJ Lewis, "LVM HOWTO", http://tldp.org/HOWTO/LVM-HOWTO/


Section #: 1.1.8
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:**  

**Title:** Configure /dev/shm

**Assessment Status:**  

**Description:**  

**Rational Statement:**  

**Impact Statement:**  

**Remediation Procedure:**  

**Audit Procedure:**  

**Additional Information:**  

**CIS Controls:**  

**CIS Safeguards 1 (v8):**  

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:**  

**v8 IG2:**  

**v8 IG3:**  

**CIS Safeguards 1 (v7):**  

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:**  

**v7 IG2:**  

**v7 IG3:**  

**References:**  


Section #: 1.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:**  

**Title:** Configure Software Updates

**Assessment Status:**  

**Description:** Debian Family Linux distributions use apt to install and update software packages. Patch management procedures may vary widely between enterprises. Large enterprises may choose to install a local updates server that can be used in place of their distributions servers, whereas a single deployment of a system may prefer to get updates directly. Updates can be performed automatically or manually, depending on the site's policy for patch management. Many large enterprises prefer to test patches on a non-production system before rolling out to production.

For the purpose of this benchmark, the requirement is to ensure that a patch management system is configured and maintained. The specifics on patch update procedures are left to the organization.

**Rational Statement:**  

**Impact Statement:**  

**Remediation Procedure:**  

**Audit Procedure:**  

**Additional Information:**  

**CIS Controls:**  

**CIS Safeguards 1 (v8):**  

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:**  

**v8 IG2:**  

**v8 IG3:**  

**CIS Safeguards 1 (v7):**  

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:**  

**v7 IG2:**  

**v7 IG3:**  

**References:**  


Section #: 1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:**  

**Title:** Filesystem Integrity Checking

**Assessment Status:**  

**Description:** AIDE is a file integrity checking tool, similar in nature to Tripwire. While it cannot prevent intrusions, it can detect unauthorized changes to configuration files by alerting when the files are changed. When setting up AIDE, decide internally what the site policy will be concerning integrity checking. Review the AIDE quick start guide and AIDE documentation before proceeding.

**Rational Statement:**  

**Impact Statement:**  

**Remediation Procedure:**  

**Audit Procedure:**  

**Additional Information:**  

**CIS Controls:**  

**CIS Safeguards 1 (v8):**  

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:**  

**v8 IG2:**  

**v8 IG3:**  

**CIS Safeguards 1 (v7):**  

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:**  

**v7 IG2:**  

**v7 IG3:**  

**References:**  


Section #: 1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:**  

**Title:** Secure Boot Settings

**Assessment Status:**  

**Description:** The recommendations in this section focus on securing the bootloader and settings involved in the boot process directly.

**Rational Statement:**  

**Impact Statement:**  

**Remediation Procedure:**  

**Audit Procedure:**  

**Additional Information:**  

**CIS Controls:**  

**CIS Safeguards 1 (v8):**  

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:**  

**v8 IG2:**  

**v8 IG3:**  

**CIS Safeguards 1 (v7):**  

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:**  

**v7 IG2:**  

**v7 IG3:**  

**References:**  


Section #: 1.5
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:**  

**Title:** Additional Process Hardening

**Assessment Status:**  

**Description:**  

**Rational Statement:**  

**Impact Statement:**  

**Remediation Procedure:**  

**Audit Procedure:**  

**Additional Information:**  

**CIS Controls:**  

**CIS Safeguards 1 (v8):**  

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:**  

**v8 IG2:**  

**v8 IG3:**  

**CIS Safeguards 1 (v7):**  

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:**  

**v7 IG2:**  

**v7 IG3:**  

**References:**  


Section #: 1.6
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:**  

**Title:** Mandatory Access Control

**Assessment Status:**  

**Description:** Mandatory Access Control (MAC) provides an additional layer of access restrictions to processes on top of the base Discretionary Access Controls. By restricting how processes can access files and resources on a system the potential impact from vulnerabilities in the processes can be reduced.

_Impact: Mandatory Access Control limits the capabilities of applications and daemons on a system, while this can prevent unauthorized access the configuration of MAC can be complex and difficult to implement correctly preventing legitimate access from occurring._

_Notes:_ 
- _Apparmor is the default MAC provided with Debian systems._
- _Additional Mandatory Access Control systems to include SELinux exist. If a different Mandatory Access Control systems is used, please follow it's vendors guidance for proper implementation in place of the guidance provided in this section_

**Rational Statement:**  

**Impact Statement:**  

**Remediation Procedure:**  

**Audit Procedure:**  

**Additional Information:**  

**CIS Controls:**  

**CIS Safeguards 1 (v8):**  

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:**  

**v8 IG2:**  

**v8 IG3:**  

**CIS Safeguards 1 (v7):**  

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:**  

**v7 IG2:**  

**v7 IG3:**  

**References:**  


Section #: 1.6.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:**  

**Title:** Configure AppArmor

**Assessment Status:**  

**Description:** AppArmor provides a Mandatory Access Control (MAC) system that greatly augments the default Discretionary Access Control (DAC) model. Under AppArmor MAC rules are applied by file paths instead of by security contexts as in other MAC systems. As such it does not require support in the filesystem and can be applied to network mounted filesystems for example. AppArmor security policies define what system resources applications can access and what privileges they can do so with. This automatically limits the damage that the software can do to files accessible by the calling user. The user does not need to take any action to gain this benefit. For an action to occur, both the traditional DAC permissions must be satisfied as well as the AppArmor MAC rules. The action will not be allowed if either one of these models does not permit the action. In this way, AppArmor rules can only make a system's permissions more restrictive and secure.

**References:**

1. AppArmor Documentation: [http://wiki.apparmor.net/index.php/Documentation](http://wiki.apparmor.net/index.php/Documentation)
2. Ubuntu AppArmor Documentation: [https://help.ubuntu.com/community/AppArmor](https://help.ubuntu.com/community/AppArmor)
3. SUSE AppArmor Documentation: [https://www.suse.com/documentation/apparmor/](https://www.suse.com/documentation/apparmor/)

**Rational Statement:**  

**Impact Statement:**  

**Remediation Procedure:**  

**Audit Procedure:**  

**Additional Information:**  

**CIS Controls:**  

**CIS Safeguards 1 (v8):**  

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:**  

**v8 IG2:**  

**v8 IG3:**  

**CIS Safeguards 1 (v7):**  

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:**  

**v7 IG2:**  

**v7 IG3:**  

**References:**  


Section #: 1.6.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.6.1.4

**Title:** Ensure all AppArmor Profiles are enforcing

**Assessment Status:** Automated

**Description:** AppArmor profiles define what resources applications are able to access.

**Rational Statement:** Security configuration requirements vary from site to site. Some sites may mandate a policy that is stricter than the default policy, which is perfectly acceptable. This item is intended to ensure that any policies that exist on the system are activated.

**Impact Statement:**  

**Remediation Procedure:** Run the following command to set all profiles to enforce mode:


# aa-enforce /etc/apparmor.d/*

_Note: Any unconfined processes may need to have a profile created or activated for them and then be restarted_

**Audit Procedure:** Run the following commands and verify that profiles are loaded and are not in complain mode:


# apparmor_status | grep profiles

Review output and ensure that profiles are loaded, and in enforce mode:


34 profiles are loaded.
34 profiles are in enforce mode.
0 profiles are in complain mode.
2 processes have profiles defined.


Run the following command and verify that no processes are unconfined:


apparmor_status | grep processes


Review the output and ensure no processes are unconfined:


2 processes have profiles defined.
2 processes are in enforce mode.
0 processes are in complain mode.
0 processes are unconfined but have a profile defined.


**Additional Information:**  

**CIS Controls:** TITLE:Configure Data Access Control Lists CONTROL:v8 3.3 DESCRIPTION:Configure data access control lists based on a user’s need to know. Apply data access control lists, also known as access permissions, to local and remote file systems, databases, and applications.;TITLE:Protect Information through Access Control Lists CONTROL:v7 14.6 DESCRIPTION:Protect all information stored on systems with file system, network share, claims, application, or database specific access control lists. These controls will enforce the principle that only authorized individuals should have access to the information based on their need to access the information as a part of their responsibilities.;

**CIS Safeguards 1 (v8):** 3.3

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:** X

**v8 IG2:** X

**v8 IG3:** X

**CIS Safeguards 1 (v7):** 14.6

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:** X

**v7 IG2:** X

**v7 IG3:** X

**References:**  


Section #: 1.7
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:**  

**Title:** Command Line Warning Banners

**Assessment Status:**  

**Description:** Presenting a warning message prior to the normal user login may assist in the prosecution of trespassers on the computer system. Changing some of these login banners also has the side effect of hiding OS version information and other detailed system information from attackers attempting to target specific exploits at a system. The `/etc/motd`, `/etc/issue`, and `/etc/issue.net` files govern warning banners for standard command line logins for both local and remote users.

Guidelines published by the US Department of Defense require that warning messages include at least the name of the organization that owns the system, the fact that the system is subject to monitoring and that such monitoring is in compliance with local statutes, and that use of the system implies consent to such monitoring. It is important that the organization's legal counsel review the content of all messages before any system modifications are made, as these warning messages are inherently site-specific. More information (including citations of relevant case law) can be found at [http://www.justice.gov/criminal/cybercrime/ ](http://www.justice.gov/criminal/cybercrime/ )

_Note: The text provided in the remediation actions for these items is intended as an example only. Please edit to include the specific text for your organization as approved by your legal department_

**Rational Statement:**  

**Impact Statement:**  

**Remediation Procedure:**  

**Audit Procedure:**  

**Additional Information:**  

**CIS Controls:**  

**CIS Safeguards 1 (v8):**  

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:**  

**v8 IG2:**  

**v8 IG3:**  

**CIS Safeguards 1 (v7):**  

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:**  

**v7 IG2:**  

**v7 IG3:**  

**References:**  


Section #: 1.8
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:**  

**Title:** GNOME Display Manager

**Assessment Status:**  

**Description:** The GNOME Display Manager (GDM) is a program that manages graphical display servers and handles graphical user logins.

**Note:** If GDM is not installed on the system, this section can be skipped

**Rational Statement:**  

**Impact Statement:**  

**Remediation Procedure:**  

**Audit Procedure:**  

**Additional Information:**  

**CIS Controls:**  

**CIS Safeguards 1 (v8):**  

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:**  

**v8 IG2:**  

**v8 IG3:**  

**CIS Safeguards 1 (v7):**  

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:**  

**v7 IG2:**  

**v7 IG3:**  

**References:**  


Section #: 1.8
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 1.8.1

**Title:** Ensure GNOME Display Manager is removed

**Assessment Status:** Automated

**Description:** The GNOME Display Manager (GDM) is a program that manages graphical display servers and handles graphical user logins.

**Rational Statement:** If a Graphical User Interface (GUI) is not required, it should be removed to reduce the attack surface of the system.

**Impact Statement:** Removing the GNOME Display manager will remove the Graphical User Interface (GUI) from the system.

**Remediation Procedure:** Run the following command to uninstall `gdm3`:



# apt purge gdm3


**Audit Procedure:** Run the following command and verify `gdm3` is not installed:



# dpkg-query -W -f='${binary:Package}\t${Status}\t${db:Status-Status}\n' gdm3

gdm3 unknown ok not-installed not-installed


**Additional Information:**  

**CIS Controls:** TITLE:Uninstall or Disable Unnecessary Services on Enterprise Assets and Software CONTROL:v8 4.8 DESCRIPTION:Uninstall or disable unnecessary services on enterprise assets and software, such as an unused file sharing service, web application module, or service function.;TITLE:Ensure Only Approved Ports, Protocols and Services Are Running CONTROL:v7 9.2 DESCRIPTION:Ensure that only network ports, protocols, and services listening on a system with validated business needs, are running on each system.;

**CIS Safeguards 1 (v8):** 4.8

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:**  

**v8 IG2:** X

**v8 IG3:** X

**CIS Safeguards 1 (v7):** 9.2

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:**  

**v7 IG2:** X

**v7 IG3:** X

**References:**  


Section #: 2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:**  

**Title:** Services

**Assessment Status:**  

**Description:** While applying system updates and patches helps correct known vulnerabilities, one of the best ways to protect the system against as yet unreported vulnerabilities is to disable all services that are not required for normal system operation. This prevents the exploitation of vulnerabilities discovered at a later date. If a service is not enabled, it cannot be exploited. The actions in this section of the document provide guidance on some services which can be safely disabled and under which circumstances, greatly reducing the number of possible threats to the resulting system. Additionally some services which should remain enabled but with secure configuration are covered as well as insecure service clients.

Note: This should not be considered a comprehensive list of insecure services. You may wish to consider additions to those listed here for your environment.

**Rational Statement:**  

**Impact Statement:**  

**Remediation Procedure:**  

**Audit Procedure:**  

**Additional Information:**  

**CIS Controls:**  

**CIS Safeguards 1 (v8):**  

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:**  

**v8 IG2:**  

**v8 IG3:**  

**CIS Safeguards 1 (v7):**  

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:**  

**v7 IG2:**  

**v7 IG3:**  

**References:**  


Section #: 2.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:**  

**Title:** Configure Time Synchronization

**Assessment Status:**  

**Description:** It is recommended that physical systems and virtual guests lacking direct access to the physical host's clock be configured to synchronize their time using a service such as systemd-timesyncd, chrony, or ntp.

**Note:**
- If access to a physical host's clock is available and configured according to site policy, this section can be skipped
- **Only one time synchronization method should be in use on the system**
- Only the section related to the time synchronization method in use on the system should be followed, all other time synchronization recommendations should be skipped
- If access to a physical host's clock is available and configured according to site policy:
 - `systemd-timesyncd` should be stopped and masked
 - `chrony` should be removed from the system
 - `ntp` should be removed from the system

**Rational Statement:**  

**Impact Statement:**  

**Remediation Procedure:**  

**Audit Procedure:**  

**Additional Information:**  

**CIS Controls:**  

**CIS Safeguards 1 (v8):**  

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:**  

**v8 IG2:**  

**v8 IG3:**  

**CIS Safeguards 1 (v7):**  

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:**  

**v7 IG2:**  

**v7 IG3:**  

**References:**  


Section #: 2.1.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:**  

**Title:** Ensure time synchronization is in use

**Assessment Status:**  

**Description:** It is recommended that physical systems and virtual guests lacking direct access to the physical host's clock be configured to synchronize their time using a service such as systemd-timesyncd, chrony, or ntp.

**Rational Statement:**  

**Impact Statement:**  

**Remediation Procedure:**  

**Audit Procedure:**  

**Additional Information:**  

**CIS Controls:**  

**CIS Safeguards 1 (v8):**  

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:**  

**v8 IG2:**  

**v8 IG3:**  

**CIS Safeguards 1 (v7):**  

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:**  

**v7 IG2:**  

**v7 IG3:**  

**References:**  


Section #: 2.1.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:**  

**Title:** Configure chrony

**Assessment Status:**  

**Description:** chrony is a daemon which implements the Network Time Protocol (NTP) and is designed to synchronize system clocks across a variety of systems and use a source that is highly accurate.

chrony can be configured to be a client and/or a server.

More information on chrony can be found at: [http://chrony.tuxfamily.org/](http://chrony.tuxfamily.org/). 

**Note:**
- If ntp or systemd-timesyncd are used, chrony should be removed and this section skipped
- Only one time synchronization method should be in use on the system

**Rational Statement:**  

**Impact Statement:**  

**Remediation Procedure:**  

**Audit Procedure:**  

**Additional Information:**  

**CIS Controls:**  

**CIS Safeguards 1 (v8):**  

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:**  

**v8 IG2:**  

**v8 IG3:**  

**CIS Safeguards 1 (v7):**  

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:**  

**v7 IG2:**  

**v7 IG3:**  

**References:**  


Section #: 2.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:**  

**Title:** Configure systemd-timesyncd

**Assessment Status:**  

**Description:** `systemd-timesyncd` is a daemon that has been added for synchronizing the system clock across the network. It implements an SNTP client. In contrast to NTP implementations such as chrony or the NTP reference server this only implements a client side, and does not bother with the full NTP complexity, focusing only on querying time from one remote server and synchronizing the local clock to it. The daemon runs with minimal privileges, and has been hooked up with networkd to only operate when network connectivity is available. The daemon saves the current clock to disk every time a new NTP sync has been acquired, and uses this to possibly correct the system clock early at bootup, in order to accommodate for systems that lack an RTC such as the Raspberry Pi and embedded devices, and make sure that time monotonically progresses on these systems, even if it is not always correct. To make use of this daemon a new system user and group "systemd-timesync" needs to be created on installation of systemd.

The default configuration is set during compilation, so configuration is only needed when it is necessary to deviate from those defaults. Initially, the main configuration file in /etc/systemd/ contains commented out entries showing the defaults as a guide to the administrator. Local overrides can be created by editing this file or by creating drop-ins, as described below. Using drop-ins for local configuration is recommended over modifications to the main configuration file.

In addition to the "main" configuration file, drop-in configuration snippets are read from `/usr/lib/systemd/*.conf.d/`, `/usr/local/lib/systemd/*.conf.d/`, and `/etc/systemd/*.conf.d/`. Those drop-ins have higher precedence and override the main configuration file. Files in the *.conf.d/ configuration subdirectories are sorted by their filename in lexicographic order, regardless of in which of the subdirectories they reside. When multiple files specify the same option, for options which accept just a single value, the entry in the file sorted last takes precedence, and for options which accept a list of values, entries are collected as they occur in the sorted files.

When packages need to customize the configuration, they can install drop-ins under /usr/. Files in /etc/ are reserved for the local administrator, who may use this logic to override the configuration files installed by vendor packages. Drop-ins have to be used to override package drop-ins, since the main configuration file has lower precedence. It is recommended to prefix all filenames in those subdirectories with a two-digit number and a dash, to simplify the ordering of the files.

To disable a configuration file supplied by the vendor, the recommended way is to place a symlink to /dev/null in the configuration directory in /etc/, with the same filename as the vendor configuration file.

**Note:**
- The recommendations in this section only apply if timesyncd is in use on the system
- The systemd-timesyncd service specifically implements only SNTP. 
 - This minimalistic service will set the system clock for large offsets or slowly adjust it for smaller deltas
 - More complex use cases are not covered by systemd-timesyncd
- **If chrony or ntp are used, systemd-timesyncd should be stopped and masked, and this section skipped**
- **One, and only one, time synchronization method should be in use on the system**

**Rational Statement:**  

**Impact Statement:**  

**Remediation Procedure:**  

**Audit Procedure:**  

**Additional Information:**  

**CIS Controls:**  

**CIS Safeguards 1 (v8):**  

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:**  

**v8 IG2:**  

**v8 IG3:**  

**CIS Safeguards 1 (v7):**  

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:**  

**v7 IG2:**  

**v7 IG3:**  

**References:**  


Section #: 2.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:**  

**Title:** Configure ntp

**Assessment Status:**  

**Description:** `ntp` is a daemon which implements the Network Time Protocol (NTP). It is designed to synchronize system clocks across a variety of systems and use a source that is highly accurate. More information on NTP can be found at [http://www.ntp.org](http://www.ntp.org/). `ntp` can be configured to be a client and/or a server.

**Note:**
- If `chrony` or `systemd-timesyncd` are used, `ntp` should be removed and this section skipped
- This recommendation only applies if ntp is in use on the system
- **Only one time synchronization method should be in use on the system**

**Rational Statement:**  

**Impact Statement:**  

**Remediation Procedure:**  

**Audit Procedure:**  

**Additional Information:**  

**CIS Controls:**  

**CIS Safeguards 1 (v8):**  

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:**  

**v8 IG2:**  

**v8 IG3:**  

**CIS Safeguards 1 (v7):**  

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:**  

**v7 IG2:**  

**v7 IG3:**  

**References:**  


Section #: 2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:**  

**Title:** Special Purpose Services

**Assessment Status:**  

**Description:** This section describes services that are installed on systems that specifically need to run these services. If any of these services are not required, it is recommended that they be deleted from the system to reduce the potential attack surface. If a package is required as a dependency, and the service is not required, the service should be stopped and masked.

The following command can be used to stop and mask the service:

# systemctl --now mask <service_name>


**Rational Statement:**  

**Impact Statement:**  

**Remediation Procedure:**  

**Audit Procedure:**  

**Additional Information:**  

**CIS Controls:**  

**CIS Safeguards 1 (v8):**  

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:**  

**v8 IG2:**  

**v8 IG3:**  

**CIS Safeguards 1 (v7):**  

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:**  

**v7 IG2:**  

**v7 IG3:**  

**References:**  


Section #: 2.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:**  

**Title:** Service Clients

**Assessment Status:**  

**Description:** A number of insecure services exist. While disabling the servers prevents a local attack against these services, it is advised to remove their clients unless they are required.

_Note: This should not be considered a comprehensive list of insecure service clients. You may wish to consider additions to those listed here for your environment._

**Rational Statement:**  

**Impact Statement:**  

**Remediation Procedure:**  

**Audit Procedure:**  

**Additional Information:**  

**CIS Controls:**  

**CIS Safeguards 1 (v8):**  

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:**  

**v8 IG2:**  

**v8 IG3:**  

**CIS Safeguards 1 (v7):**  

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:**  

**v7 IG2:**  

**v7 IG3:**  

**References:**  


Section #: 3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:**  

**Title:** Network Configuration

**Assessment Status:**  

**Description:** This section provides guidance on for securing the network configuration of the system through kernel parameters, access list control, and firewall settings.

**Note:**
- sysctl settings are defined through files in `/usr/lib/sysctl.d/`, `/run/sysctl.d/`, and `/etc/sysctl.d/`.
- Files must have the "`.conf`" extension. 
- Vendors settings live in `/usr/lib/sysctl.d/`
- To override a whole file, create a new file with the same name in `/etc/sysctl.d/` and put new settings there.
- To override only specific settings, add a file with a lexically later name in `/etc/sysctl.d/` and put new settings there.
- The paths where sysctl preload files usually exist
 - `/run/sysctl.d/*.conf`
 - `/etc/sysctl.d/*.conf`
 - `/usr/local/lib/sysctl.d/*.conf`
 - `/usr/lib/sysctl.d/*.conf`
 - `/lib/sysctl.d/*.conf`
 - `/etc/sysctl.conf`
- On systems with Uncomplicated Firewall, additional settings may be configured in `/etc/ufw/sysctl.conf`
 - The settings in `/etc/ufw/sysctl.conf` will override settings in `/etc/sysctl.conf`
 - This behavior can be changed by updating the `IPT_SYSCTL` parameter in `/etc/default/ufw`

**Rational Statement:**  

**Impact Statement:**  

**Remediation Procedure:**  

**Audit Procedure:**  

**Additional Information:**  

**CIS Controls:**  

**CIS Safeguards 1 (v8):**  

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:**  

**v8 IG2:**  

**v8 IG3:**  

**CIS Safeguards 1 (v7):**  

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:**  

**v7 IG2:**  

**v7 IG3:**  

**References:**  


Section #: 3.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:**  

**Title:** Disable unused network protocols and devices

**Assessment Status:**  

**Description:** To reduce the attack surface of a system, unused network protocols and devices should be disabled.

The Linux kernel modules support several network protocols that are not commonly used. If these protocols are not needed, it is recommended that they be disabled in the kernel.

**Note:** This should not be considered a comprehensive list of uncommon network protocols, you may wish to consider additions to those listed here for your environment.

**Rational Statement:**  

**Impact Statement:**  

**Remediation Procedure:**  

**Audit Procedure:**  

**Additional Information:**  

**CIS Controls:**  

**CIS Safeguards 1 (v8):**  

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:**  

**v8 IG2:**  

**v8 IG3:**  

**CIS Safeguards 1 (v7):**  

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:**  

**v7 IG2:**  

**v7 IG3:**  

**References:**  


Section #: 3.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.1.3

**Title:** Ensure DCCP is disabled

**Assessment Status:** Automated

**Description:** The Datagram Congestion Control Protocol (DCCP) is a transport layer protocol that supports streaming media and telephony. DCCP provides a way to gain access to congestion control, without having to do it at the application layer, but does not provide in-sequence delivery.

**Rational Statement:** If the protocol is not required, it is recommended that the drivers not be installed to reduce the potential attack surface.

**Impact Statement:**  

**Remediation Procedure:** Run the following script to disable `dccp`:


.. code-block:: bash
    #!/usr/bin/env bash

    {
    l_mname="dccp" # set module name
    # Check if the module exists on the system
    if [ -z "$(modprobe -n -v "$l_mname" 2>&1 | grep -Pi -- "\h*modprobe:\h+FATAL:\h+Module\h+$l_mname\h+not\h+found\h+in\h+directory")" ]; then
    # Remediate loadable
    l_loadable="$(modprobe -n -v "$l_mname")"
    [ "$(wc -l <<< "$l_loadable")" -gt "1" ] && l_loadable="$(grep -P -- "(^\h*install|\b$l_mname)\b" <<< "$l_loadable")"
    if ! grep -Pq -- '^\h*install \/bin\/(true|false)' <<< "$l_loadable"; then
    echo -e " - setting module: \"$l_mname\" to be not loadable"
    echo -e "install $l_mname /bin/false" >> /etc/modprobe.d/"$l_mname".conf
    fi
    # Remediate loaded
    if lsmod | grep "$l_mname" > /dev/null 2> then
    echo -e " - unloading module \"$l_mname\""
    modprobe -r "$l_mname"
    fi
    # Remediate deny list
    if ! modprobe --showconfig | grep -Pq -- "^\h*blacklist\h+$(tr '-' '_' <<< "$l_mname")\b"; then
    echo -e " - deny listing \"$l_mname\""
    echo -e "blacklist $l_mname" >> /etc/modprobe.d/"$l_mname".conf
    fi
    else
    echo -e " - Nothing to remediate\n - Module \"$l_mname\" doesn't exist on the system"
    fi
    }


**Audit Procedure:** Run the following script to verify `dccp` is disabled:

.. code-block:: bash
    #!/usr/bin/env bash

    {
    l_output="" l_output2=""
    l_mname="dccp" # set module name
    # Check if the module exists on the system
    if [ -z "$(modprobe -n -v "$l_mname" 2>&1 | grep -Pi -- "\h*modprobe:\h+FATAL:\h+Module\h+$l_mname\h+not\h+found\h+in\h+directory")" ]; then
    # Check how module will be loaded
    l_loadable="$(modprobe -n -v "$l_mname")"
    [ "$(wc -l <<< "$l_loadable")" -gt "1" ] && l_loadable="$(grep -P -- "(^\h*install|\b$l_mname)\b" <<< "$l_loadable")"
    if grep -Pq -- '^\h*install \/bin\/(true|false)' <<< "$l_loadable"; then
    l_output="$l_output\n - module: \"$l_mname\" is not loadable: \"$l_loadable\""
    else
    l_output2="$l_output2\n - module: \"$l_mname\" is loadable: \"$l_loadable\""
    fi
    # Check is the module currently loaded
    if ! lsmod | grep "$l_mname" > /dev/null 2> then
    l_output="$l_output\n - module: \"$l_mname\" is not loaded"
    else
    l_output2="$l_output2\n - module: \"$l_mname\" is loaded"
    fi
    # Check if the module is deny listed
    if modprobe --showconfig | grep -Pq -- "^\h*blacklist\h+$(tr '-' '_' <<< "$l_mname")\b"; then
    l_output="$l_output\n - module: \"$l_mname\" is deny listed in: \"$(grep -Pl -- "^\h*blacklist\h+$l_mname\b" /etc/modprobe.d/*)\""
    else
    l_output2="$l_output2\n - module: \"$l_mname\" is not deny listed"
    fi
    else
    l_output="$l_output\n - Module \"$l_mname\" doesn't exist on the system"
    fi
    # Report results. If no failures output in l_output2, we pass
    if [ -z "$l_output2" ]; then
    echo -e "\n- Audit Result:\n ** PASS **\n$l_output\n"
    else
    echo -e "\n- Audit Result:\n ** FAIL **\n - Reason(s) for audit failure:\n$l_output2\n"
    [ -n "$l_output" ] && echo -e "\n- Correctly set:\n$l_output\n"
    fi
    }


**Additional Information:**  

**CIS Controls:** TITLE:Uninstall or Disable Unnecessary Services on Enterprise Assets and Software CONTROL:v8 4.8 DESCRIPTION:Uninstall or disable unnecessary services on enterprise assets and software, such as an unused file sharing service, web application module, or service function.;TITLE:Ensure Only Approved Ports, Protocols and Services Are Running CONTROL:v7 9.2 DESCRIPTION:Ensure that only network ports, protocols, and services listening on a system with validated business needs, are running on each system.;

**CIS Safeguards 1 (v8):** 4.8

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:**  

**v8 IG2:** X

**v8 IG3:** X

**CIS Safeguards 1 (v7):** 9.2

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:**  

**v7 IG2:** X

**v7 IG3:** X

**References:**  


Section #: 3.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.1.4

**Title:** Ensure SCTP is disabled

**Assessment Status:** Automated

**Description:** The Stream Control Transmission Protocol (SCTP) is a transport layer protocol used to support message oriented communication, with several streams of messages in one connection. It serves a similar function as TCP and UDP, incorporating features of both. It is message-oriented like UDP, and ensures reliable in-sequence transport of messages with congestion control like TCP.

**Rational Statement:** If the protocol is not being used, it is recommended that kernel module not be loaded, disabling the service to reduce the potential attack surface.

**Impact Statement:**  

**Remediation Procedure:** Run the following script to disable `sctp`:

.. code-block:: bash
    #!/usr/bin/env bash

    {
    l_mname="sctp" # set module name
    # Check if the module exists on the system
    if [ -z "$(modprobe -n -v "$l_mname" 2>&1 | grep -Pi -- "\h*modprobe:\h+FATAL:\h+Module\h+$l_mname\h+not\h+found\h+in\h+directory")" ]; then
    # Remediate loadable
    l_loadable="$(modprobe -n -v "$l_mname")"
    [ "$(wc -l <<< "$l_loadable")" -gt "1" ] && l_loadable="$(grep -P -- "(^\h*install|\b$l_mname)\b" <<< "$l_loadable")"
    if ! grep -Pq -- '^\h*install \/bin\/(true|false)' <<< "$l_loadable"; then
    echo -e " - setting module: \"$l_mname\" to be not loadable"
    echo -e "install $l_mname /bin/false" >> /etc/modprobe.d/"$l_mname".conf
    fi
    # Remediate loaded
    if lsmod | grep "$l_mname" > /dev/null 2> then
    echo -e " - unloading module \"$l_mname\""
    modprobe -r "$l_mname"
    fi
    # Remediate deny list
    if ! modprobe --showconfig | grep -Pq -- "^\h*blacklist\h+$l_mname\b"; then
    echo -e " - deny listing \"$l_mname\""
    echo -e "blacklist $l_mname" >> /etc/modprobe.d/"$l_mname".conf
    fi
    else
    echo -e " - Nothing to remediate\n - Module \"$l_mname\" doesn't exist on the system"
    fi
    }


**Audit Procedure:** Run the following script to verify `sctp` is disabled:

.. code-block:: bash
    #!/usr/bin/env bash

    {
    l_output="" l_output2=""
    l_mname="sctp" # set module name
    # Check if the module exists on the system
    if [ -z "$(modprobe -n -v "$l_mname" 2>&1 | grep -Pi -- "\h*modprobe:\h+FATAL:\h+Module\h+$l_mname\h+not\h+found\h+in\h+directory")" ]; then
    # Check how module will be loaded
    l_loadable="$(modprobe -n -v "$l_mname")"
    [ "$(wc -l <<< "$l_loadable")" -gt "1" ] && l_loadable="$(grep -P -- "(^\h*install|\b$l_mname)\b" <<< "$l_loadable")"
    if grep -Pq -- '^\h*install \/bin\/(true|false)' <<< "$l_loadable"; then
    l_output="$l_output\n - module: \"$l_mname\" is not loadable: \"$l_loadable\""
    else
    l_output2="$l_output2\n - module: \"$l_mname\" is loadable: \"$l_loadable\""
    fi
    # Check is the module currently loaded
    if ! lsmod | grep "$l_mname" > /dev/null 2> then
    l_output="$l_output\n - module: \"$l_mname\" is not loaded"
    else
    l_output2="$l_output2\n - module: \"$l_mname\" is loaded"
    fi
    # Check if the module is deny listed
    if modprobe --showconfig | grep -Pq -- "^\h*blacklist\h+$l_mname\b"; then
    l_output="$l_output\n - module: \"$l_mname\" is deny listed in: \"$(grep -Pl -- "^\h*blacklist\h+$l_mname\b" /etc/modprobe.d/*)\""
    else
    l_output2="$l_output2\n - module: \"$l_mname\" is not deny listed"
    fi
    else
    l_output="$l_output\n - Module \"$l_mname\" doesn't exist on the system"
    fi
    # Report results. If no failures output in l_output2, we pass
    if [ -z "$l_output2" ]; then
    echo -e "\n- Audit Result:\n ** PASS **\n$l_output\n"
    else
    echo -e "\n- Audit Result:\n ** FAIL **\n - Reason(s) for audit failure:\n$l_output2\n"
    [ -n "$l_output" ] && echo -e "\n- Correctly set:\n$l_output\n"
    fi
    }


**Additional Information:**  

**CIS Controls:** TITLE:Uninstall or Disable Unnecessary Services on Enterprise Assets and Software CONTROL:v8 4.8 DESCRIPTION:Uninstall or disable unnecessary services on enterprise assets and software, such as an unused file sharing service, web application module, or service function.;TITLE:Ensure Only Approved Ports, Protocols and Services Are Running CONTROL:v7 9.2 DESCRIPTION:Ensure that only network ports, protocols, and services listening on a system with validated business needs, are running on each system.;

**CIS Safeguards 1 (v8):** 4.8

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:**  

**v8 IG2:** X

**v8 IG3:** X

**CIS Safeguards 1 (v7):** 9.2

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:**  

**v7 IG2:** X

**v7 IG3:** X

**References:**  


Section #: 3.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.1.5

**Title:** Ensure RDS is disabled

**Assessment Status:** Automated

**Description:** The Reliable Datagram Sockets (RDS) protocol is a transport layer protocol designed to provide low-latency, high-bandwidth communications between cluster nodes. It was developed by the Oracle Corporation.

**Rational Statement:** If the protocol is not being used, it is recommended that kernel module not be loaded, disabling the service to reduce the potential attack surface.

**Impact Statement:**  

**Remediation Procedure:** Run the following script to disable `rds`:

.. code-block:: bash
    #!/usr/bin/env bash

    {
    l_mname="rds" # set module name
    # Check if the module exists on the system
    if [ -z "$(modprobe -n -v "$l_mname" 2>&1 | grep -Pi -- "\h*modprobe:\h+FATAL:\h+Module\h+$l_mname\h+not\h+found\h+in\h+directory")" ]; then
    # Remediate loadable
    l_loadable="$(modprobe -n -v "$l_mname")"
    [ "$(wc -l <<< "$l_loadable")" -gt "1" ] && l_loadable="$(grep -P -- "(^\h*install|\b$l_mname)\b" <<< "$l_loadable")"
    if ! grep -Pq -- '^\h*install \/bin\/(true|false)' <<< "$l_loadable"; then
    echo -e " - setting module: \"$l_mname\" to be not loadable"
    echo -e "install $l_mname /bin/false" >> /etc/modprobe.d/"$l_mname".conf
    fi
    # Remediate loaded
    if lsmod | grep "$l_mname" > /dev/null 2> then
    echo -e " - unloading module \"$l_mname\""
    modprobe -r "$l_mname"
    fi
    # Remediate deny list
    if ! modprobe --showconfig | grep -Pq -- "^\h*blacklist\h+$l_mname\b"; then
    echo -e " - deny listing \"$l_mname\""
    echo -e "blacklist $l_mname" >> /etc/modprobe.d/"$l_mname".conf
    fi
    else
    echo -e " - Nothing to remediate\n - Module \"$l_mname\" doesn't exist on the system"
    fi
    }


**Audit Procedure:** Run the following script to verify `rds` is disabled:

.. code-block:: bash
    #!/usr/bin/env bash

    {
    l_output="" l_output2=""
    l_mname="rds" # set module name
    # Check if the module exists on the system
    if [ -z "$(modprobe -n -v "$l_mname" 2>&1 | grep -Pi -- "\h*modprobe:\h+FATAL:\h+Module\h+$l_mname\h+not\h+found\h+in\h+directory")" ]; then
    # Check how module will be loaded
    l_loadable="$(modprobe -n -v "$l_mname")"
    [ "$(wc -l <<< "$l_loadable")" -gt "1" ] && l_loadable="$(grep -P -- "(^\h*install|\b$l_mname)\b" <<< "$l_loadable")"
    if grep -Pq -- '^\h*install \/bin\/(true|false)' <<< "$l_loadable"; then
    l_output="$l_output\n - module: \"$l_mname\" is not loadable: \"$l_loadable\""
    else
    l_output2="$l_output2\n - module: \"$l_mname\" is loadable: \"$l_loadable\""
    fi
    # Check is the module currently loaded
    if ! lsmod | grep "$l_mname" > /dev/null 2> then
    l_output="$l_output\n - module: \"$l_mname\" is not loaded"
    else
    l_output2="$l_output2\n - module: \"$l_mname\" is loaded"
    fi
    # Check if the module is deny listed
    if modprobe --showconfig | grep -Pq -- "^\h*blacklist\h+$l_mname\b"; then
    l_output="$l_output\n - module: \"$l_mname\" is deny listed in: \"$(grep -Pl -- "^\h*blacklist\h+$l_mname\b" /etc/modprobe.d/*)\""
    else
    l_output2="$l_output2\n - module: \"$l_mname\" is not deny listed"
    fi
    else
    l_output="$l_output\n - Module \"$l_mname\" doesn't exist on the system"
    fi
    # Report results. If no failures output in l_output2, we pass
    if [ -z "$l_output2" ]; then
    echo -e "\n- Audit Result:\n ** PASS **\n$l_output\n"
    else
    echo -e "\n- Audit Result:\n ** FAIL **\n - Reason(s) for audit failure:\n$l_output2\n"
    [ -n "$l_output" ] && echo -e "\n- Correctly set:\n$l_output\n"
    fi
    }


**Additional Information:**  

**CIS Controls:** TITLE:Ensure Only Approved Ports, Protocols and Services Are Running CONTROL:v7 9.2 DESCRIPTION:Ensure that only network ports, protocols, and services listening on a system with validated business needs, are running on each system.;TITLE:Limit Open Ports, Protocols, and Services CONTROL:v6 9.1 DESCRIPTION:Ensure that only ports, protocols, and services with validated business needs are running on each system.;

**CIS Safeguards 1 (v8):**  

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:**  

**v8 IG2:**  

**v8 IG3:**  

**CIS Safeguards 1 (v7):** 9.2

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:**  

**v7 IG2:** X

**v7 IG3:** X

**References:**  


Section #: 3.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 3.1.6

**Title:** Ensure TIPC is disabled

**Assessment Status:** Automated

**Description:** The Transparent Inter-Process Communication (TIPC) protocol is designed to provide communication between cluster nodes.

**Rational Statement:** If the protocol is not being used, it is recommended that kernel module not be loaded, disabling the service to reduce the potential attack surface.

**Impact Statement:**  

**Remediation Procedure:** Run the following script to disable `tipc`:

.. code-block:: bash
    #!/usr/bin/env bash

    {
    l_mname="tipc" # set module name
    # Check if the module exists on the system
    if [ -z "$(modprobe -n -v "$l_mname" 2>&1 | grep -Pi -- "\h*modprobe:\h+FATAL:\h+Module\h+$l_mname\h+not\h+found\h+in\h+directory")" ]; then
    # Remediate loadable
    l_loadable="$(modprobe -n -v "$l_mname")"
    [ "$(wc -l <<< "$l_loadable")" -gt "1" ] && l_loadable="$(grep -P -- "(^\h*install|\b$l_mname)\b" <<< "$l_loadable")"
    if ! grep -Pq -- '^\h*install \/bin\/(true|false)' <<< "$l_loadable"; then
    echo -e " - setting module: \"$l_mname\" to be not loadable"
    echo -e "install $l_mname /bin/false" >> /etc/modprobe.d/"$l_mname".conf
    fi
    # Remediate loaded
    if lsmod | grep "$l_mname" > /dev/null 2> then
    echo -e " - unloading module \"$l_mname\""
    modprobe -r "$l_mname"
    fi
    # Remediate deny list
    if ! modprobe --showconfig | grep -Pq -- "^\h*blacklist\h+$l_mname\b"; then
    echo -e " - deny listing \"$l_mname\""
    echo -e "blacklist $l_mname" >> /etc/modprobe.d/"$l_mname".conf
    fi
    else
    echo -e " - Nothing to remediate\n - Module \"$l_mname\" doesn't exist on the system"
    fi
    }


**Audit Procedure:** Run the following script to verify `tipc` is disabled:

.. code-block:: bash
    #!/usr/bin/env bash

    {
    l_output="" l_output2=""
    l_mname="tipc" # set module name
    # Check if the module exists on the system
    if [ -z "$(modprobe -n -v "$l_mname" 2>&1 | grep -Pi -- "\h*modprobe:\h+FATAL:\h+Module\h+$l_mname\h+not\h+found\h+in\h+directory")" ]; then
    # Check how module will be loaded
    l_loadable="$(modprobe -n -v "$l_mname")"
    [ "$(wc -l <<< "$l_loadable")" -gt "1" ] && l_loadable="$(grep -P -- "(^\h*install|\b$l_mname)\b" <<< "$l_loadable")"
    if grep -Pq -- '^\h*install \/bin\/(true|false)' <<< "$l_loadable"; then
    l_output="$l_output\n - module: \"$l_mname\" is not loadable: \"$l_loadable\""
    else
    l_output2="$l_output2\n - module: \"$l_mname\" is loadable: \"$l_loadable\""
    fi
    # Check is the module currently loaded
    if ! lsmod | grep "$l_mname" > /dev/null 2> then
    l_output="$l_output\n - module: \"$l_mname\" is not loaded"
    else
    l_output2="$l_output2\n - module: \"$l_mname\" is loaded"
    fi
    # Check if the module is deny listed
    if modprobe --showconfig | grep -Pq -- "^\h*blacklist\h+$l_mname\b"; then
    l_output="$l_output\n - module: \"$l_mname\" is deny listed in: \"$(grep -Pl -- "^\h*blacklist\h+$l_mname\b" /etc/modprobe.d/*)\""
    else
    l_output2="$l_output2\n - module: \"$l_mname\" is not deny listed"
    fi
    else
    l_output="$l_output\n - Module \"$l_mname\" doesn't exist on the system"
    fi
    # Report results. If no failures output in l_output2, we pass
    if [ -z "$l_output2" ]; then
    echo -e "\n- Audit Result:\n ** PASS **\n$l_output\n"
    else
    echo -e "\n- Audit Result:\n ** FAIL **\n - Reason(s) for audit failure:\n$l_output2\n"
    [ -n "$l_output" ] && echo -e "\n- Correctly set:\n$l_output\n"
    fi
    }


**Additional Information:**  

**CIS Controls:** TITLE:Ensure Only Approved Ports, Protocols and Services Are Running CONTROL:v7 9.2 DESCRIPTION:Ensure that only network ports, protocols, and services listening on a system with validated business needs, are running on each system.;TITLE:Limit Open Ports, Protocols, and Services CONTROL:v6 9.1 DESCRIPTION:Ensure that only ports, protocols, and services with validated business needs are running on each system.;

**CIS Safeguards 1 (v8):**  

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:**  

**v8 IG2:**  

**v8 IG3:**  

**CIS Safeguards 1 (v7):** 9.2

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:**  

**v7 IG2:** X

**v7 IG3:** X

**References:**  


Section #: 3.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:**  

**Title:** Network Parameters (Host Only)

**Assessment Status:**  

**Description:** The following network parameters are intended for use if the system is to act as a host only. A system is considered host only if the system has a single interface, or has multiple interfaces but will not be configured as a router.

**Note:**

Configuration files are read from directories in `/etc/`, `/run/`, `/usr/local/lib/`, and `/lib/`, in order of precedence. Files must have the the `".conf"` extension. extension. Files in `/etc/` override files with the same name in `/run/`, `/usr/local/lib/`, and `/lib/`. Files in `/run/` override files with the same name under `/usr/`.

All configuration files are sorted by their filename in lexicographic order, regardless of which of the directories they reside in. If multiple files specify the same option, the entry in the file with the lexicographically latest name will take precedence. Thus, the configuration in a certain file may either be replaced completely (by placing a file with the same name in a directory with higher priority), or individual settings might be changed (by specifying additional settings in a file with a different name that is ordered later).

Packages should install their configuration files in `/usr/lib/` (distribution packages) or `/usr/local/lib/` (local installs). Files in `/etc/` are reserved for the local administrator, who may use this logic to override the configuration files installed by vendor packages. It is recommended to prefix all filenames with a two-digit number and a dash, to simplify the ordering of the files.

If the administrator wants to disable a configuration file supplied by the vendor, the recommended way is to place a symlink to `/dev/null` in the configuration directory in `/etc/`, with the same filename as the vendor configuration file. If the vendor configuration file is included in the initrd image, the image has to be regenerated.

**Rational Statement:**  

**Impact Statement:**  

**Remediation Procedure:**  

**Audit Procedure:**  

**Additional Information:**  

**CIS Controls:**  

**CIS Safeguards 1 (v8):**  

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:**  

**v8 IG2:**  

**v8 IG3:**  

**CIS Safeguards 1 (v7):**  

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:**  

**v7 IG2:**  

**v7 IG3:**  

**References:**  


Section #: 3.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:**  

**Title:** Network Parameters (Host and Router)

**Assessment Status:**  

**Description:** The following network parameters are intended for use on both host only and router systems. A system acts as a router if it has at least two interfaces and is configured to perform routing functions.

**Note:**

Configuration files are read from directories in `/etc/`, `/run/`, `/usr/local/lib/`, and `/lib/`, in order of precedence. Files must have the the `".conf"` extension. extension. Files in `/etc/` override files with the same name in `/run/`, `/usr/local/lib/`, and `/lib/`. Files in `/run/` override files with the same name under `/usr/`.

All configuration files are sorted by their filename in lexicographic order, regardless of which of the directories they reside in. If multiple files specify the same option, the entry in the file with the lexicographically latest name will take precedence. Thus, the configuration in a certain file may either be replaced completely (by placing a file with the same name in a directory with higher priority), or individual settings might be changed (by specifying additional settings in a file with a different name that is ordered later).

Packages should install their configuration files in `/usr/lib/` (distribution packages) or `/usr/local/lib/` (local installs). Files in `/etc/` are reserved for the local administrator, who may use this logic to override the configuration files installed by vendor packages. It is recommended to prefix all filenames with a two-digit number and a dash, to simplify the ordering of the files.

If the administrator wants to disable a configuration file supplied by the vendor, the recommended way is to place a symlink to `/dev/null` in the configuration directory in `/etc/`, with the same filename as the vendor configuration file. If the vendor configuration file is included in the initrd image, the image has to be regenerated.

**Rational Statement:**  

**Impact Statement:**  

**Remediation Procedure:**  

**Audit Procedure:**  

**Additional Information:**  

**CIS Controls:**  

**CIS Safeguards 1 (v8):**  

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:**  

**v8 IG2:**  

**v8 IG3:**  

**CIS Safeguards 1 (v7):**  

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:**  

**v7 IG2:**  

**v7 IG3:**  

**References:**  


Section #: 3.5
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:**  

**Title:** Firewall Configuration

**Assessment Status:**  

**Description:** A firewall is a set of rules. When a data packet moves into or out of a protected network space, its contents (in particular, information about its origin, target, and the protocol it plans to use) are tested against the firewall rules to see if it should be allowed through

To provide a Host Based Firewall, the Linux kernel includes support for:
 - Netfilter - A set of hooks inside the Linux kernel that allows kernel modules to register callback functions with the network stack. A registered callback function is then called back for every packet that traverses the respective hook within the network stack. Includes the ip_tables, ip6_tables, arp_tables, and ebtables kernel modules. These modules are some of the significant parts of the Netfilter hook system.
 - nftables - A subsystem of the Linux kernel providing filtering and classification of network packets/datagrams/frames. nftables is supposed to replace certain parts of Netfilter, while keeping and reusing most of it. nftables utilizes the building blocks of the Netfilter infrastructure, such as the existing hooks into the networking stack, connection tracking system, userspace queueing component, and logging subsystem. _Is available in Linux kernels 3.13 and newer_.

In order to configure firewall rules for Netfilter or nftables, a firewall utility needs to be installed. Guidance has been included for the following firewall utilities:
 - UncomplicatedFirewall (ufw) - Provides firewall features by acting as a front-end for the Linux kernel's netfilter framework via the iptables backend. `ufw` supports both IPv4 and IPv6 networks
 - nftables - Includes the nft utility for configuration of the nftables subsystem of the Linux kernel
 - iptables - Includes the iptables, ip6tables, arptables and ebtables utilities for configuration Netfilter and the ip_tables, ip6_tables, arp_tables, and ebtables kernel modules.

_Notes:_
- _Only one method should be used to configure a firewall on the system. Use of more than one method could produce unexpected results_
- _This section is intended only to ensure the resulting firewall rules are in place, not how they are configured_

**Rational Statement:**  

**Impact Statement:**  

**Remediation Procedure:**  

**Audit Procedure:**  

**Additional Information:**  

**CIS Controls:**  

**CIS Safeguards 1 (v8):**  

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:**  

**v8 IG2:**  

**v8 IG3:**  

**CIS Safeguards 1 (v7):**  

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:**  

**v7 IG2:**  

**v7 IG3:**  

**References:**  


Section #: 3.5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:**  

**Title:** Configure UncomplicatedFirewall

**Assessment Status:**  

**Description:** _If nftables or iptables are being used in your environment, please follow the guidance in their respective section and pass-over the guidance in this section._

Uncomplicated Firewall (UFW) is a program for managing a netfilter firewall designed to be easy to use. 
- Uses a command-line interface consisting of a small number of simple commands
- Uses iptables for configuration
- Rules are processed until first matching rule. The first matching rule will be applied.

_Notes:_ 
- _Configuration of a live system's firewall directly over a remote connection will often result in being locked out_
- _Rules should be ordered so that `ALLOW` rules come before `DENY` rules._

**Rational Statement:**  

**Impact Statement:**  

**Remediation Procedure:**  

**Audit Procedure:**  

**Additional Information:**  

**CIS Controls:**  

**CIS Safeguards 1 (v8):**  

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:**  

**v8 IG2:**  

**v8 IG3:**  

**CIS Safeguards 1 (v7):**  

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:**  

**v7 IG2:**  

**v7 IG3:**  

**References:**  


Section #: 3.5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:**  

**Title:** Configure nftables

**Assessment Status:**  

**Description:** _If Uncomplicated Firewall (UFW) or iptables are being used in your environment, please follow the guidance in their respective section and pass-over the guidance in this section._

nftables is a subsystem of the Linux kernel providing filtering and classification of network packets/datagrams/frames and is the successor to iptables. The biggest change with the successor nftables is its simplicity. With iptables, we have to configure every single rule and use the syntax which can be compared with normal commands. With nftables, the simpler syntax, much like BPF (Berkely Packet Filter) means shorter lines and less repetition. Support for nftables should also be compiled into the kernel, together with the related nftables modules. Please ensure that your kernel supports nf_tables before choosing this option.

_Notes:_
- _This section broadly assumes starting with an empty nftables firewall ruleset (established by flushing the rules with nft flush ruleset)._
- _Remediation steps included only affect the live system, you will also need to configure your default firewall configuration to apply on boot._
- _Configuration of a live systems firewall directly over a remote connection will often result in being locked out. It is advised to have a known good firewall configuration set to run on boot and to configure an entire firewall structure in a script that is then run and tested before saving to boot._

The following will implement the firewall rules of this section and open ICMP, IGMP, and port 22(ssh) from anywhere. Opening the ports for ICMP, IGMP, and port 22(ssh) needs to be updated in accordance with local site policy. Allow port 22(ssh) needs to be updated to only allow systems requiring ssh connectivity to connect, as per site policy.

Save the script bellow as `/etc/nftables.rules` 



#!/sbin/nft -f

# This nftables.rules config should be saved as /etc/nftables.rules
# flush nftables rulesset
flush ruleset
# Load nftables ruleset
# nftables config with inet table named filter
table inet filter {
# Base chain for input hook named input (Filters inbound network packets)
chain input {
type filter hook input priority 0; policy drop;

# Ensure loopback traffic is configured
iif "lo" accept
ip saddr 127.0.0.0/8 counter packets 0 bytes 0 drop
ip6 saddr ::1 counter packets 0 bytes 0 drop

# Ensure established connections are configured
ip protocol tcp ct state established accept
ip protocol udp ct state established accept
ip protocol icmp ct state established accept

# Accept port 22(SSH) traffic from anywhere
tcp dport ssh accept

# Accept ICMP and IGMP from anywhere
icmpv6 type { destination-unreachable, packet-too-big, time-exceeded, parameter-problem, mld-listener-query, mld-listener-report, mld-listener-done, nd-router-solicit, nd-router-advert, nd-neighbor-solicit, nd-neighbor-advert, ind-neighbor-solicit, ind-neighbor-advert, mld2-listener-report } accept
icmp type { destination-unreachable, router-advertisement, router-solicitation, time-exceeded, parameter-problem } accept
ip protocol igmp accept
}

# Base chain for hook forward named forward (Filters forwarded network packets)
chain forward {
type filter hook forward priority 0; policy drop;
}

# Base chain for hook output named output (Filters outbount network packets)
chain output {
type filter hook output priority 0; policy drop;
# Ensure outbound and established connections are configured
ip protocol tcp ct state established,related,new accept
ip protocol udp ct state established,related,new accept
ip protocol icmp ct state established,related,new accept
}
}


Run the following command to load the file into nftables


# nft -f /etc/nftables.rules


All changes in the nftables subsections are temporary. 

To make these changes permanent:

Run the following command to create the nftables.rules file

nft list ruleset > /etc/nftables.rules


Add the following line to `/etc/nftables.conf`

include "/etc/nftables.rules"


**Rational Statement:**  

**Impact Statement:**  

**Remediation Procedure:**  

**Audit Procedure:**  

**Additional Information:**  

**CIS Controls:**  

**CIS Safeguards 1 (v8):**  

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:**  

**v8 IG2:**  

**v8 IG3:**  

**CIS Safeguards 1 (v7):**  

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:**  

**v7 IG2:**  

**v7 IG3:**  

**References:**  


Section #: 3.5.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:**  

**Title:** Configure iptables

**Assessment Status:**  

**Description:** _If Uncomplicated Firewall (UFW) or nftables are being used in your environment, please follow the guidance in their respective section and pass-over the guidance in this section._

IPtables is an application that allows a system administrator to configure the IPv4 and IPv6 tables, chains and rules provided by the Linux kernel firewall. While several methods of configuration exist this section is intended only to ensure the resulting IPtables rules are in place, not how they are configured. If IPv6 is in use in your environment, similar settings should be applied to the IP6tables as well.

_Note: Configuration of a live system's firewall directly over a remote connection will often result in being locked out_

**Rational Statement:**  

**Impact Statement:**  

**Remediation Procedure:**  

**Audit Procedure:**  

**Additional Information:**  

**CIS Controls:**  

**CIS Safeguards 1 (v8):**  

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:**  

**v8 IG2:**  

**v8 IG3:**  

**CIS Safeguards 1 (v7):**  

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:**  

**v7 IG2:**  

**v7 IG3:**  

**References:**  


Section #: 3.5.3.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:**  

**Title:** Configure iptables software

**Assessment Status:**  

**Description:** This section provides guidance for installing, enabling, removing, and disabling software packages necessary for using IPTables as the method for configuring and maintaining a Host Based Firewall on the system.

_Note: Using more than one method to configure and maintain a Host Based Firewall can cause unexpected results. If FirewallD or NFTables are being used for configuration and maintenance, this section should be skipped and the guidance in their respective section followed._

**Rational Statement:**  

**Impact Statement:**  

**Remediation Procedure:**  

**Audit Procedure:**  

**Additional Information:**  

**CIS Controls:**  

**CIS Safeguards 1 (v8):**  

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:**  

**v8 IG2:**  

**v8 IG3:**  

**CIS Safeguards 1 (v7):**  

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:**  

**v7 IG2:**  

**v7 IG3:**  

**References:**  


Section #: 3.5.3.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:**  

**Title:** Configure IPv4 iptables

**Assessment Status:**  

**Description:** Iptables is used to set up, maintain, and inspect the tables of IP packet filter rules in the Linux kernel. Several different tables may be defined. Each table contains a number of built-in chains and may also contain user-defined chains.

Each chain is a list of rules which can match a set of packets. Each rule specifies what to do with a packet that matches. This is called a 'target', which may be a jump to a user-defined chain in the same table.

_Note: This section broadly assumes starting with an empty IPtables firewall ruleset (established by flushing the rules with iptables -F). Remediation steps included only affect the live system, you will also need to configure your default firewall configuration to apply on boot. Configuration of a live systems firewall directly over a remote connection will often result in being locked out. It is advised to have a known good firewall configuration set to run on boot and to configure an entire firewall structure in a script that is then run and tested before saving to boot. The following script will implement the firewall rules of this section and open port 22(ssh) from anywhere:_

.. code-block:: bash
    #!/bin/bash

    # Flush IPtables rules
    iptables -F

    # Ensure default deny firewall policy
    iptables -P INPUT DROP
    iptables -P OUTPUT DROP
    iptables -P FORWARD DROP

    # Ensure loopback traffic is configured
    iptables -A INPUT -i lo -j ACCEPT
    iptables -A OUTPUT -o lo -j ACCEPT
    iptables -A INPUT -s 127.0.0.0/8 -j DROP

    # Ensure outbound and established connections are configured
    iptables -A OUTPUT -p tcp -m state --state NEW,ESTABLISHED -j ACCEPT
    iptables -A OUTPUT -p udp -m state --state NEW,ESTABLISHED -j ACCEPT
    iptables -A OUTPUT -p icmp -m state --state NEW,ESTABLISHED -j ACCEPT
    iptables -A INPUT -p tcp -m state --state ESTABLISHED -j ACCEPT
    iptables -A INPUT -p udp -m state --state ESTABLISHED -j ACCEPT
    iptables -A INPUT -p icmp -m state --state ESTABLISHED -j ACCEPT

    # Open inbound ssh(tcp port 22) connections
    iptables -A INPUT -p tcp --dport 22 -m state --state NEW -j ACCEPT


**Rational Statement:**  

**Impact Statement:**  

**Remediation Procedure:**  

**Audit Procedure:**  

**Additional Information:**  

**CIS Controls:**  

**CIS Safeguards 1 (v8):**  

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:**  

**v8 IG2:**  

**v8 IG3:**  

**CIS Safeguards 1 (v7):**  

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:**  

**v7 IG2:**  

**v7 IG3:**  

**References:**  


Section #: 3.5.3.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:**  

**Title:** Configure IPv6  ip6tables

**Assessment Status:**  

**Description:** Ip6tables is used to set up, maintain, and inspect the tables of IPv6 packet filter rules in the Linux kernel. Several different tables may be defined. Each table contains a number of built-in chains and may also contain user-defined chains.
Each chain is a list of rules which can match a set of packets. Each rule specifies what to do with a packet that matches. This is called a `target', which may be a jump to a user-defined chain in the same table. 

If IPv6 in enabled on the system, the ip6tables should be configured.

_Note: This section broadly assumes starting with an empty ip6tables firewall ruleset (established by flushing the rules with ip6tables -F). Remediation steps included only affect the live system, you will also need to configure your default firewall configuration to apply on boot. Configuration of a live systems firewall directly over a remote connection will often result in being locked out. It is advised to have a known good firewall configuration set to run on boot and to configure an entire firewall structure in a script that is then run and tested before saving to boot._

The following script will implement the firewall rules of this section and open port 22(ssh) from anywhere:

.. code-block:: bash
    #!/bin/bash

    # Flush ip6tables rules
    ip6tables -F

    # Ensure default deny firewall policy
    ip6tables -P INPUT DROP
    ip6tables -P OUTPUT DROP
    ip6tables -P FORWARD DROP

    # Ensure loopback traffic is configured
    ip6tables -A INPUT -i lo -j ACCEPT
    ip6tables -A OUTPUT -o lo -j ACCEPT
    ip6tables -A INPUT -s ::1 -j DROP

    # Ensure outbound and established connections are configured
    ip6tables -A OUTPUT -p tcp -m state --state NEW,ESTABLISHED -j ACCEPT
    ip6tables -A OUTPUT -p udp -m state --state NEW,ESTABLISHED -j ACCEPT
    ip6tables -A OUTPUT -p icmp -m state --state NEW,ESTABLISHED -j ACCEPT
    ip6tables -A INPUT -p tcp -m state --state ESTABLISHED -j ACCEPT
    ip6tables -A INPUT -p udp -m state --state ESTABLISHED -j ACCEPT
    ip6tables -A INPUT -p icmp -m state --state ESTABLISHED -j ACCEPT

    # Open inbound ssh(tcp port 22) connections
    ip6tables -A INPUT -p tcp --dport 22 -m state --state NEW -j ACCEPT


**Rational Statement:**  

**Impact Statement:**  

**Remediation Procedure:**  

**Audit Procedure:**  

**Additional Information:**  

**CIS Controls:**  

**CIS Safeguards 1 (v8):**  

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:**  

**v8 IG2:**  

**v8 IG3:**  

**CIS Safeguards 1 (v7):**  

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:**  

**v7 IG2:**  

**v7 IG3:**  

**References:**  


Section #: 4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:**  

**Title:** Logging and Auditing

**Assessment Status:**  

**Description:** The items in this section describe how to configure logging, log monitoring, and auditing, using tools included in most distributions.

It is recommended that `rsyslog` be used for logging (with `logwatch` providing summarization) and `auditd` be used for auditing (with `aureport` providing summarization) to automatically monitor logs for intrusion attempts and other suspicious system behavior.

In addition to the local log files created by the steps in this section, it is also recommended that sites collect copies of their system logs on a secure, centralized log server via an encrypted connection. Not only does centralized logging help sites correlate events that may be occurring on multiple systems, but having a second copy of the system log information may be critical after a system compromise where the attacker has modified the local log files on the affected system(s). If a log correlation system is deployed, configure it to process the logs described in this section.

Because it is often necessary to correlate log information from many different systems (particularly after a security incident) it is recommended that the time be synchronized among systems and devices connected to the local network.

It is important that all logs described in this section be monitored on a regular basis and correlated to determine trends. A seemingly innocuous entry in one log could be more significant when compared to an entry in another log.

_Note on log file permissions:_
_There really isn't a "one size fits all" solution to the permissions on log files. Many sites utilize group permissions so that administrators who are in a defined security group, such as "wheel" do not have to elevate privileges to root in order to read log files. Also, if a third party log aggregation tool is used, it may need to have group permissions to read the log files, which is preferable to having it run setuid to root. Therefore, there are two remediation and audit steps for log file permissions. One is for systems that do not have a secured group method implemented that only permits root to read the log files (`root:root 600`). The other is for sites that do have such a setup and are designated as `root:securegrp 640` where `securegrp` is the defined security group (in some cases `wheel`)._

**Rational Statement:**  

**Impact Statement:**  

**Remediation Procedure:**  

**Audit Procedure:**  

**Additional Information:**  

**CIS Controls:**  

**CIS Safeguards 1 (v8):**  

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:**  

**v8 IG2:**  

**v8 IG3:**  

**CIS Safeguards 1 (v7):**  

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:**  

**v7 IG2:**  

**v7 IG3:**  

**References:**  


Section #: 4.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:**  

**Title:** Configure System Accounting (auditd)

**Assessment Status:**  

**Description:** The Linux Auditing System operates on a set of rules that collects certain types of system activity to facilitate incident investigation, detect unauthorized access or modification of data. By default events will be logged to `/var/log/audit/audit.log`, which can be configured in `/etc/audit/auditd.conf`.

The following types of audit rules can be specified:
 - Control rules: Configuration of the auditing system.
 - File system rules: Allow the auditing of access to a particular file or a directory. Also known as file watches.
 - System call rules: Allow logging of system calls that any specified program makes.

Audit rules can be set:
 - On the command line using the `auditctl` utility. These rules are not persistent across reboots.
 - In `/etc/audit/audit.rules`. These rules have to be merged and loaded before they are active.

**Notes:**
 - For 64 bit systems that have `arch` as a rule parameter, you will need two rules: one for 64 bit and one for 32 bit systems calls. For 32 bit systems, only one rule is needed.
 - If the auditing system is configured to be locked (`-e 2`), a system reboot will be required in order to load any changes.
 - Key names are optional on the rules and will not be used as a compliance auditing. The usage of key names is highly recommended as it facilitates organisation and searching, as such, all remediation steps will have key names supplied.
 - It is best practice to store the rules, in number prepended files, in `/etc/audit/rules.d/`. Rules must end in a `.rules` suffix. This then requires the use of `augenrules` to merge all the rules into `/etc/audit/audit.rules` based on their their alphabetical (lexical) sort order. All benchmark recommendations follow this best practice for remediation, specifically using the prefix of `50` which is centre weighed if all rule sets make use of the number prepending naming convention.
 - Your system may have been customized to change the default `UID_MIN`. All samples output uses `1000`, but this value will not be used in compliance auditing. To confirm the `UID_MIN` for your system, run the following command: 
 


awk '/^\s*UID_MIN/{print $2}' /etc/login.defs

#### Normalization
The Audit system normalizes some entries, so when you look at the sample output keep in mind that:
 - With regards to users whose login UID is not set, the values `-1` / `unset` / `4294967295` are equivalent and normalized to `-1`.
 - When comparing field types and both sides of the comparison is valid fields types, such as`euid!=uid`, then the auditing system may normalize such that the output is `uid!=euid`.
 - Some parts of the rule may be rearranged whilst others are dependant on previous syntax. For example, the following two statements are the same:


-a always,exit -F arch=b64 -S execve -C uid!=euid -F auid!=-1 -F key=user_emulation


and


-a always,exit -F arch=b64 -C euid!=uid -F auid!=unset -S execve -k user_emulation


#### Capacity planning

The recommendations in this section implement auditing policies that not only produces large quantities of logged data, but may also negatively impact system performance. Capacity planning is critical in order not to adversely impact production environments.

 - Disk space. If a significantly large set of events are captured, additional on system or off system storage may need to be allocated. If the logs are not sent to a remote log server, ensure that log rotation is implemented else the disk will fill up and the system will halt. Even when logs are sent to a log server, ensure sufficient disk space to allow caching of logs in the case of temporary network outages.
 - Disk IO. It is not just the amount of data collected that should be considered, but the rate at which logs are generated.
 - CPU overhead. System call rules might incur considerable CPU overhead. Test the systems open/close syscalls per second with and without the rules to gauge the impact of the rules.

**Rational Statement:**  

**Impact Statement:**  

**Remediation Procedure:**  

**Audit Procedure:**  

**Additional Information:**  

**CIS Controls:**  

**CIS Safeguards 1 (v8):**  

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:**  

**v8 IG2:**  

**v8 IG3:**  

**CIS Safeguards 1 (v7):**  

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:**  

**v7 IG2:**  

**v7 IG3:**  

**References:**  


Section #: 4.1.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:**  

**Title:** Ensure auditing is enabled

**Assessment Status:**  

**Description:** The capturing of system events provides system administrators with information to allow them to determine if unauthorized access to their system is occurring.

**Rational Statement:**  

**Impact Statement:**  

**Remediation Procedure:**  

**Audit Procedure:**  

**Additional Information:**  

**CIS Controls:**  

**CIS Safeguards 1 (v8):**  

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:**  

**v8 IG2:**  

**v8 IG3:**  

**CIS Safeguards 1 (v7):**  

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:**  

**v7 IG2:**  

**v7 IG3:**  

**References:**  


Section #: 4.1.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.1.1

**Title:** Ensure auditd is installed

**Assessment Status:** Automated

**Description:** auditd is the userspace component to the Linux Auditing System. It's responsible for writing audit records to the disk

**Rational Statement:** The capturing of system events provides system administrators with information to allow them to determine if unauthorized access to their system is occurring.

**Impact Statement:**  

**Remediation Procedure:** Run the following command to Install auditd


# apt install auditd audispd-plugins


**Audit Procedure:** Run the following command and verify auditd and audispd-plugins are installed:


# dpkg-query -W -f='${binary:Package}\t${Status}\t${db:Status-Status}\n' auditd audispd-plugins

audispd-plugins install ok installed installed
auditd install ok installed installed


**Additional Information:**  

**CIS Controls:** TITLE:Collect Detailed Audit Logs CONTROL:v8 8.5 DESCRIPTION:Configure detailed audit logging for enterprise assets containing sensitive data. Include event source, date, username, timestamp, source addresses, destination addresses, and other useful elements that could assist in a forensic investigation.;TITLE:Activate audit logging CONTROL:v7 6.2 DESCRIPTION:Ensure that local logging has been enabled on all systems and networking devices.;

**CIS Safeguards 1 (v8):** 8.5

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:**  

**v8 IG2:** X

**v8 IG3:** X

**CIS Safeguards 1 (v7):** 6.2

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:** X

**v7 IG2:** X

**v7 IG3:** X

**References:**  


Section #: 4.1.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.1.2

**Title:** Ensure auditd service is enabled and active

**Assessment Status:** Automated

**Description:** Turn on the `auditd` daemon to record system events.

**Rational Statement:** The capturing of system events provides system administrators with information to allow them to determine if unauthorized access to their system is occurring.

**Impact Statement:**  

**Remediation Procedure:** Run the following command to enable and start `auditd`:


# systemctl --now enable auditd


**Audit Procedure:** Run the following command to verify `auditd` is enabled:


# systemctl is-enabled auditd

enabled


Verify result is "enabled".

Run the following command to verify `auditd` is active:


# systemctl is-active auditd

active


Verify result is active

**Additional Information:**  

**CIS Controls:** TITLE:Collect Audit Logs CONTROL:v8 8.2 DESCRIPTION:Collect audit logs. Ensure that logging, per the enterprise’s audit log management process, has been enabled across enterprise assets.;TITLE:Activate audit logging CONTROL:v7 6.2 DESCRIPTION:Ensure that local logging has been enabled on all systems and networking devices.;TITLE:Enable Detailed Logging CONTROL:v7 6.3 DESCRIPTION:Enable system logging to include detailed information such as an event source, date, user, timestamp, source addresses, destination addresses, and other useful elements.;

**CIS Safeguards 1 (v8):** 8.2

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:** X

**v8 IG2:** X

**v8 IG3:** X

**CIS Safeguards 1 (v7):** 6.2

**CIS Safeguards 2 (v7):** 6.3

**CIS Safeguards 3 (v7):**  

**v7 IG1:** X

**v7 IG2:** X

**v7 IG3:** X

**References:**  


Section #: 4.1.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.1.3

**Title:** Ensure auditing for processes that start prior to auditd is enabled

**Assessment Status:** Automated

**Description:** Configure `grub2` so that processes that are capable of being audited can be audited even if they start up prior to `auditd` startup.

**Rational Statement:** Audit events need to be captured on processes that start up prior to `auditd` , so that potential malicious activity cannot go undetected.

**Impact Statement:**  

**Remediation Procedure:** Edit `/etc/default/grub` and add `audit=1` to `GRUB_CMDLINE_LINUX`:

Example:


GRUB_CMDLINE_LINUX="audit=1"


Run the following command to update the `grub2` configuration:

   
# update-grub


**Audit Procedure:** Run the following command:


# find /boot -type f -name 'grub.cfg' -exec grep -Ph -- '^\h*linux' {} + | grep -v 'audit=1'


Nothing should be returned.

**Additional Information:** This recommendation is designed around the grub2 bootloader, if another bootloader is in use in your environment enact equivalent settings.

**CIS Controls:** TITLE:Collect Audit Logs CONTROL:v8 8.2 DESCRIPTION:Collect audit logs. Ensure that logging, per the enterprise’s audit log management process, has been enabled across enterprise assets.;TITLE:Activate audit logging CONTROL:v7 6.2 DESCRIPTION:Ensure that local logging has been enabled on all systems and networking devices.;

**CIS Safeguards 1 (v8):** 8.2

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:** X

**v8 IG2:** X

**v8 IG3:** X

**CIS Safeguards 1 (v7):** 6.2

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:** X

**v7 IG2:** X

**v7 IG3:** X

**References:**  


Section #: 4.1.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.1.4

**Title:** Ensure audit_backlog_limit is sufficient

**Assessment Status:** Automated

**Description:** In the kernel-level audit subsystem, a socket buffer queue is used to hold audit events. Whenever a new audit event is received, it is logged and prepared to be added to this queue. 

The kernel boot parameter `audit_backlog_limit=N`, with `N` representing the amount of messages, will ensure that a queue cannot grow beyond a certain size. If an audit event is logged which would grow the queue beyond this limit, then a failure occurs and is handled according to the system configuration

**Rational Statement:** If an audit event is logged which would grow the queue beyond the `audit_backlog_limit`, then a failure occurs, auditd records will be lost, and potential malicious activity could go undetected.

**Impact Statement:**  

**Remediation Procedure:** Edit `/etc/default/grub` and add `audit_backlog_limit=N` to GRUB_CMDLINE_LINUX. The recommended size for `N` is `8192` or larger.

Example:


GRUB_CMDLINE_LINUX="audit_backlog_limit=8192"


Run the following command to update the `grub2` configuration:


# update-grub


**Audit Procedure:** Run the following command and verify the `audit_backlog_limit=` parameter is set:


# find /boot -type f -name 'grub.cfg' -exec grep -Ph -- '^\h*linux' {} + | grep -Pv 'audit_backlog_limit=\d+\b'


Nothing should be returned.

**Additional Information:**  

**CIS Controls:** TITLE:Collect Audit Logs CONTROL:v8 8.2 DESCRIPTION:Collect audit logs. Ensure that logging, per the enterprise’s audit log management process, has been enabled across enterprise assets.;TITLE:Activate audit logging CONTROL:v7 6.2 DESCRIPTION:Ensure that local logging has been enabled on all systems and networking devices.;TITLE:Enable Detailed Logging CONTROL:v7 6.3 DESCRIPTION:Enable system logging to include detailed information such as an event source, date, user, timestamp, source addresses, destination addresses, and other useful elements.;

**CIS Safeguards 1 (v8):** 8.2

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:** X

**v8 IG2:** X

**v8 IG3:** X

**CIS Safeguards 1 (v7):** 6.2

**CIS Safeguards 2 (v7):** 6.3

**CIS Safeguards 3 (v7):**  

**v7 IG1:** X

**v7 IG2:** X

**v7 IG3:** X

**References:**  


Section #: 4.1.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:**  

**Title:** Configure Data Retention

**Assessment Status:**  

**Description:** When auditing, it is important to carefully configure the storage requirements for audit logs. By default, auditd will max out the log files at 5MB and retain only 4 copies of them. Older versions will be deleted. It is possible on a system that the 20 MBs of audit logs may fill up the system causing loss of audit data. While the recommendations here provide guidance, check your site policy for audit storage requirements.

**Rational Statement:**  

**Impact Statement:**  

**Remediation Procedure:**  

**Audit Procedure:**  

**Additional Information:**  

**CIS Controls:**  

**CIS Safeguards 1 (v8):**  

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:**  

**v8 IG2:**  

**v8 IG3:**  

**CIS Safeguards 1 (v7):**  

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:**  

**v7 IG2:**  

**v7 IG3:**  

**References:**  


Section #: 4.1.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.2.1

**Title:** Ensure audit log storage size is configured

**Assessment Status:** Automated

**Description:** Configure the maximum size of the audit log file. Once the log reaches the maximum size, it will be rotated and a new log file will be started.

**Rational Statement:** It is important that an appropriate size is determined for log files so that they do not impact the system and audit data is not lost.

**Impact Statement:**  

**Remediation Procedure:** Set the following parameter in `/etc/audit/auditd.conf` in accordance with site policy:


max_log_file = <MB>


**Audit Procedure:** Run the following command and ensure output is in compliance with site policy:


# grep -Po -- '^\h*max_log_file\h*=\h*\d+\b' /etc/audit/auditd.conf

max_log_file = <MB>


**Additional Information:** The `max_log_file` parameter is measured in megabytes.

Other methods of log rotation may be appropriate based on site policy. One example is time-based rotation strategies which don't have native support in auditd configurations. Manual audit of custom configurations should be evaluated for effectiveness and completeness.

**CIS Controls:** TITLE:Ensure Adequate Audit Log Storage CONTROL:v8 8.3 DESCRIPTION:Ensure that logging destinations maintain adequate storage to comply with the enterprise’s audit log management process.;TITLE:Ensure adequate storage for logs CONTROL:v7 6.4 DESCRIPTION:Ensure that all systems that store logs have adequate storage space for the logs generated.;

**CIS Safeguards 1 (v8):** 8.3

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:** X

**v8 IG2:** X

**v8 IG3:** X

**CIS Safeguards 1 (v7):** 6.4

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:**  

**v7 IG2:** X

**v7 IG3:** X

**References:**  


Section #: 4.1.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.2.2

**Title:** Ensure audit logs are not automatically deleted

**Assessment Status:** Automated

**Description:** The `max_log_file_action` setting determines how to handle the audit log file reaching the max file size. A value of `keep_logs` will rotate the logs but never delete old logs.

**Rational Statement:** In high security contexts, the benefits of maintaining a long audit history exceed the cost of storing the audit history.

**Impact Statement:**  

**Remediation Procedure:** Set the following parameter in `/etc/audit/auditd.conf:` 


max_log_file_action = keep_logs


**Audit Procedure:** Run the following command and verify output matches:


# grep max_log_file_action /etc/audit/auditd.conf

max_log_file_action = keep_logs


**Additional Information:**  

**CIS Controls:** TITLE:Ensure Adequate Audit Log Storage CONTROL:v8 8.3 DESCRIPTION:Ensure that logging destinations maintain adequate storage to comply with the enterprise’s audit log management process.;TITLE:Ensure adequate storage for logs CONTROL:v7 6.4 DESCRIPTION:Ensure that all systems that store logs have adequate storage space for the logs generated.;

**CIS Safeguards 1 (v8):** 8.3

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:** X

**v8 IG2:** X

**v8 IG3:** X

**CIS Safeguards 1 (v7):** 6.4

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:**  

**v7 IG2:** X

**v7 IG3:** X

**References:**  


Section #: 4.1.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.2.3

**Title:** Ensure system is disabled when audit logs are full

**Assessment Status:** Automated

**Description:** The `auditd` daemon can be configured to halt the system when the audit logs are full.

The admin_space_left_action parameter tells the system what action to take when the system has detected that it is low on disk space. Valid values are ignore, syslog, suspend, single, and halt.
- `ignore`, the audit daemon does nothing
- `Syslog`, the audit daemon will issue a warning to syslog
- `Suspend`, the audit daemon will stop writing records to the disk
- `single`, the audit daemon will put the computer system in single user mode
- `halt`, the audit daemon will shutdown the system

**Rational Statement:** In high security contexts, the risk of detecting unauthorized access or nonrepudiation exceeds the benefit of the system's availability.

**Impact Statement:** If the `admin_space_left_action` parameter is set to `halt` the audit daemon will shutdown the system when the disk partition containing the audit logs becomes full.

**Remediation Procedure:** Set the following parameters in `/etc/audit/auditd.conf:` 


space_left_action = email
action_mail_acct = root


set `admin_space_left_action` to either `halt` **or** `single` in `/etc/audit/auditd.conf`.

_Example:_


admin_space_left_action = halt


**Audit Procedure:** Run the following commands and verify output matches:


# grep space_left_action /etc/audit/auditd.conf

space_left_action = email



# grep action_mail_acct /etc/audit/auditd.conf

action_mail_acct = root


Run the following command and verify the output is either `halt` **or** `single`:


# grep -E 'admin_space_left_action\s*=\s*(halt|single)' /etc/audit/auditd.conf

admin_space_left_action = <halt|single>


**Additional Information:**  

**CIS Controls:** TITLE:Collect Audit Logs CONTROL:v8 8.2 DESCRIPTION:Collect audit logs. Ensure that logging, per the enterprise’s audit log management process, has been enabled across enterprise assets.;TITLE:Ensure Adequate Audit Log Storage CONTROL:v8 8.3 DESCRIPTION:Ensure that logging destinations maintain adequate storage to comply with the enterprise’s audit log management process.;TITLE:Ensure adequate storage for logs CONTROL:v7 6.4 DESCRIPTION:Ensure that all systems that store logs have adequate storage space for the logs generated.;

**CIS Safeguards 1 (v8):** 8.2

**CIS Safeguards 2 (v8):** 8.3

**CIS Safeguards 3 (v8):**  

**v8 IG1:** X

**v8 IG2:** X

**v8 IG3:** X

**CIS Safeguards 1 (v7):** 6.4

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:**  

**v7 IG2:** X

**v7 IG3:** X

**References:**  


Section #: 4.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:**  

**Title:** Configure auditd rules

**Assessment Status:**  

**Description:** The Audit system operates on a set of rules that define what is to be captured in the log files.

The following types of Audit rules can be specified:
 - Control rules: Allow the Audit system's behavior and some of its configuration to be modified.
 - File system rules: Allow the auditing of access to a particular file or a directory. (Also known as file watches)
 - System call rules: Allow logging of system calls that any specified program makes.

Audit rules can be set:
 - on the command line using the auditctl utility. Note that these rules are not persistent across reboots.
 - in a file ending in `.rules` in the `/etc/audit/audit.d/` directory.

**Rational Statement:**  

**Impact Statement:**  

**Remediation Procedure:**  

**Audit Procedure:**  

**Additional Information:**  

**CIS Controls:**  

**CIS Safeguards 1 (v8):**  

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:**  

**v8 IG2:**  

**v8 IG3:**  

**CIS Safeguards 1 (v7):**  

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:**  

**v7 IG2:**  

**v7 IG3:**  

**References:**  


Section #: 4.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.3.1

**Title:** Ensure changes to system administration scope (sudoers) is collected

**Assessment Status:** Automated

**Description:** Monitor scope changes for system administrators. If the system has been properly configured to force system administrators to log in as themselves first and then use the `sudo` command to execute privileged commands, it is possible to monitor changes in scope. The file `/etc/sudoers`, or files in `/etc/sudoers.d`, will be written to when the file(s) or related attributes have changed. The audit records will be tagged with the identifier "scope".

**Rational Statement:** Changes in the `/etc/sudoers` and `/etc/sudoers.d` files can indicate that an unauthorized change has been made to the scope of system administrator activity.

**Impact Statement:**  

**Remediation Procedure:** Edit or create a file in the `/etc/audit/rules.d/` directory, ending in `.rules` extension, with the relevant rules to monitor scope changes for system administrators.

Example:


# printf "
-w /etc/sudoers -p wa -k scope
-w /etc/sudoers.d -p wa -k scope
" >> /etc/audit/rules.d/50-scope.rules


Merge and load the rules into active configuration:


# augenrules --load


Check if reboot is required.


# if [[ $(auditctl -s | grep "enabled") =~ "2" ]]; then printf "Reboot required to load rules\n"; fi


**Audit Procedure:** #### On disk configuration

Run the following command to check the on disk rules:


# awk '/^ *-w/ \
&&/\/etc\/sudoers/ \
&&/ +-p *wa/ \
&&(/ key= *[!-~]* *$/||/ -k *[!-~]* *$/)' /etc/audit/rules.d/*.rules


Verify the output matches:


-w /etc/sudoers -p wa -k scope
-w /etc/sudoers.d -p wa -k scope


#### Running configuration

Run the following command to check loaded rules:


# auditctl -l | awk '/^ *-w/ \
&&/\/etc\/sudoers/ \
&&/ +-p *wa/ \
&&(/ key= *[!-~]* *$/||/ -k *[!-~]* *$/)'


Verify the output matches:


-w /etc/sudoers -p wa -k scope
-w /etc/sudoers.d -p wa -k scope


**Additional Information:** #### Potential reboot required

If the auditing configuration is locked (`-e 2`), then `augenrules` will not warn in any way that rules could not be loaded into the running configuration. A system reboot will be required to load the rules into the running configuration.

#### System call structure

For performance (`man 7 audit.rules`) reasons it is preferable to have all the system calls on one line. However, your configuration may have them on one line each or some other combination. This is important to understand for both the auditing and remediation sections as the examples given are optimized for performance as per the man page.

**CIS Controls:** TITLE:Collect Detailed Audit Logs CONTROL:v8 8.5 DESCRIPTION:Configure detailed audit logging for enterprise assets containing sensitive data. Include event source, date, username, timestamp, source addresses, destination addresses, and other useful elements that could assist in a forensic investigation.;TITLE:Log and Alert on Changes to Administrative Group Membership CONTROL:v7 4.8 DESCRIPTION:Configure systems to issue a log entry and alert when an account is added to or removed from any group assigned administrative privileges.;

**CIS Safeguards 1 (v8):** 8.5

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:**  

**v8 IG2:** X

**v8 IG3:** X

**CIS Safeguards 1 (v7):** 4.8

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:**  

**v7 IG2:** X

**v7 IG3:** X

**References:**  


Section #: 4.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.3.2

**Title:** Ensure actions as another user are always logged

**Assessment Status:** Automated

**Description:** `sudo` provides users with temporary elevated privileges to perform operations, either as the superuser or another user.

**Rational Statement:** Creating an audit log of users with temporary elevated privileges and the operation(s) they performed is essential to reporting. Administrators will want to correlate the events written to the audit trail with the records written to `sudo`'s logfile to verify if unauthorized commands have been executed.

**Impact Statement:**  

**Remediation Procedure:** #### Create audit rules

Edit or create a file in the `/etc/audit/rules.d/` directory, ending in `.rules` extension, with the relevant rules to monitor elevated privileges.

##### 64 Bit systems

Example:


# printf "
-a always,exit -F arch=b64 -C euid!=uid -F auid!=unset -S execve -k user_emulation 
-a always,exit -F arch=b32 -C euid!=uid -F auid!=unset -S execve -k user_emulation
" >> /etc/audit/rules.d/50-user_emulation.rules


#### Load audit rules

Merge and load the rules into active configuration:


# augenrules --load


Check if reboot is required.


# if [[ $(auditctl -s | grep "enabled") =~ "2" ]]; then printf "Reboot required to load rules\n"; fi


##### 32 Bit systems

Follow the same procedures as for 64 bit systems and ignore any entries with `b64`.

**Audit Procedure:** #### 64 Bit systems

***On disk configuration***

Run the following command to check the on disk rules:


# awk '/^ *-a *always,exit/ \
&&/ -F *arch=b[2346]{2}/ \
&&(/ -F *auid!=unset/||/ -F *auid!=-1/||/ -F *auid!=4294967295/) \
&&(/ -C *euid!=uid/||/ -C *uid!=euid/) \
&&/ -S *execve/ \
&&(/ key= *[!-~]* *$/||/ -k *[!-~]* *$/)' /etc/audit/rules.d/*.rules


Verify the output matches:


-a always,exit -F arch=b64 -C euid!=uid -F auid!=unset -S execve -k user_emulation 
-a always,exit -F arch=b32 -C euid!=uid -F auid!=unset -S execve -k user_emulation


***Running configuration***

Run the following command to check loaded rules:


# auditctl -l | awk '/^ *-a *always,exit/ \
&&/ -F *arch=b[2346]{2}/ \
&&(/ -F *auid!=unset/||/ -F *auid!=-1/||/ -F *auid!=4294967295/) \
&&(/ -C *euid!=uid/||/ -C *uid!=euid/) \
&&/ -S *execve/ \
&&(/ key= *[!-~]* *$/||/ -k *[!-~]* *$/)'


Verify the output matches:


-a always,exit -F arch=b64 -S execve -C uid!=euid -F auid!=-1 -F key=user_emulation
-a always,exit -F arch=b32 -S execve -C uid!=euid -F auid!=-1 -F key=user_emulation


#### 32 Bit systems

Follow the same procedures as for 64 bit systems and ignore any entries with `b64`.

**Additional Information:** #### Potential reboot required

If the auditing configuration is locked (`-e 2`), then `augenrules` will not warn in any way that rules could not be loaded into the running configuration. A system reboot will be required to load the rules into the running configuration.

#### System call structure

For performance (`man 7 audit.rules`) reasons it is preferable to have all the system calls on one line. However, your configuration may have them on one line each or some other combination. This is important to understand for both the auditing and remediation sections as the examples given are optimized for performance as per the man page.

**CIS Controls:** TITLE:Collect Detailed Audit Logs CONTROL:v8 8.5 DESCRIPTION:Configure detailed audit logging for enterprise assets containing sensitive data. Include event source, date, username, timestamp, source addresses, destination addresses, and other useful elements that could assist in a forensic investigation.;TITLE:Log and Alert on Unsuccessful Administrative Account Login CONTROL:v7 4.9 DESCRIPTION:Configure systems to issue a log entry and alert on unsuccessful logins to an administrative account.;

**CIS Safeguards 1 (v8):** 8.5

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:**  

**v8 IG2:** X

**v8 IG3:** X

**CIS Safeguards 1 (v7):** 4.9

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:**  

**v7 IG2:** X

**v7 IG3:** X

**References:**  


Section #: 4.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.3.3

**Title:** Ensure events that modify the sudo log file are collected

**Assessment Status:** Automated

**Description:** Monitor the `sudo` log file. If the system has been properly configured to disable the use of the `su` command and force all administrators to have to log in first and then use `sudo` to execute privileged commands, then all administrator commands will be logged to `/var/log/sudo.log` . Any time a command is executed, an audit event will be triggered as the `/var/log/sudo.log` file will be opened for write and the executed administration command will be written to the log.

**Rational Statement:** Changes in `/var/log/sudo.log` indicate that an administrator has executed a command or the log file itself has been tampered with. Administrators will want to correlate the events written to the audit trail with the records written to `/var/log/sudo.log` to verify if unauthorized commands have been executed.

**Impact Statement:**  

**Remediation Procedure:** Edit or create a file in the `/etc/audit/rules.d/` directory, ending in `.rules` extension, with the relevant rules to monitor events that modify the sudo log file.

Example:


# {
SUDO_LOG_FILE=$(grep -r logfile /etc/sudoers* | sed -e 's/.*logfile=//;s/,? .*//' -e 's/"//g')
[ -n "${SUDO_LOG_FILE}" ] && printf "
-w ${SUDO_LOG_FILE} -p wa -k sudo_log_file
" >> /etc/audit/rules.d/50-sudo.rules || printf "ERROR: Variable 'SUDO_LOG_FILE_ESCAPED' is unset.\n"
}


Merge and load the rules into active configuration:


# augenrules --load


Check if reboot is required.


# if [[ $(auditctl -s | grep "enabled") =~ "2" ]]; then printf "Reboot required to load rules\n"; fi


**Audit Procedure:** #### On disk configuration

Run the following command to check the on disk rules:


# {
SUDO_LOG_FILE_ESCAPED=$(grep -r logfile /etc/sudoers* | sed -e 's/.*logfile=//;s/,? .*//' -e 's/"//g' -e 's|/|\\/|g')
[ -n "${SUDO_LOG_FILE_ESCAPED}" ] && awk "/^ *-w/ \
&&/"${SUDO_LOG_FILE_ESCAPED}"/ \
&&/ +-p *wa/ \
&&(/ key= *[!-~]* *$/||/ -k *[!-~]* *$/)" /etc/audit/rules.d/*.rules \
|| printf "ERROR: Variable 'SUDO_LOG_FILE_ESCAPED' is unset.\n"
}


Verify output of matches:


-w /var/log/sudo.log -p wa -k sudo_log_file


#### Running configuration

Run the following command to check loaded rules:


# {
SUDO_LOG_FILE_ESCAPED=$(grep -r logfile /etc/sudoers* | sed -e 's/.*logfile=//;s/,? .*//' -e 's/"//g' -e 's|/|\\/|g')
[ -n "${SUDO_LOG_FILE_ESCAPED}" ] && auditctl -l | awk "/^ *-w/ \
&&/"${SUDO_LOG_FILE_ESCAPED}"/ \
&&/ +-p *wa/ \
&&(/ key= *[!-~]* *$/||/ -k *[!-~]* *$/)" \
|| printf "ERROR: Variable 'SUDO_LOG_FILE_ESCAPED' is unset.\n"
}


Verify output matches:


-w /var/log/sudo.log -p wa -k sudo_log_file


**Additional Information:** #### Potential reboot required

If the auditing configuration is locked (`-e 2`), then `augenrules` will not warn in any way that rules could not be loaded into the running configuration. A system reboot will be required to load the rules into the running configuration.

#### System call structure

For performance (`man 7 audit.rules`) reasons it is preferable to have all the system calls on one line. However, your configuration may have them on one line each or some other combination. This is important to understand for both the auditing and remediation sections as the examples given are optimized for performance as per the man page.

**CIS Controls:** TITLE:Collect Detailed Audit Logs CONTROL:v8 8.5 DESCRIPTION:Configure detailed audit logging for enterprise assets containing sensitive data. Include event source, date, username, timestamp, source addresses, destination addresses, and other useful elements that could assist in a forensic investigation.;TITLE:Log and Alert on Unsuccessful Administrative Account Login CONTROL:v7 4.9 DESCRIPTION:Configure systems to issue a log entry and alert on unsuccessful logins to an administrative account.;

**CIS Safeguards 1 (v8):** 8.5

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:**  

**v8 IG2:** X

**v8 IG3:** X

**CIS Safeguards 1 (v7):** 4.9

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:**  

**v7 IG2:** X

**v7 IG3:** X

**References:**  


Section #: 4.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.3.4

**Title:** Ensure events that modify date and time information are collected

**Assessment Status:** Automated

**Description:** Capture events where the system date and/or time has been modified. The parameters in this section are set to determine if the;
- `adjtimex` - tune kernel clock
- `settimeofday` - set time using `timeval` and `timezone` structures
- `stime` - using seconds since 1/1/1970
- `clock_settime` - allows for the setting of several internal clocks and timers

system calls have been executed. Further, ensure to write an audit record to the configured audit log file upon exit, tagging the records with a unique identifier such as "time-change".

**Rational Statement:** Unexpected changes in system date and/or time could be a sign of malicious activity on the system.

**Impact Statement:**  

**Remediation Procedure:** #### Create audit rules

Edit or create a file in the `/etc/audit/rules.d/` directory, ending in `.rules` extension, with the relevant rules to monitor events that modify date and time information.

##### 64 Bit systems

Example:


# printf "
-a always,exit -F arch=b64 -S adjtimex,settimeofday,clock_settime -k time-change
-a always,exit -F arch=b32 -S adjtimex,settimeofday,clock_settime -k time-change
-w /etc/localtime -p wa -k time-change
" >> /etc/audit/rules.d/50-time-change.rules


#### Load audit rules

Merge and load the rules into active configuration:


# augenrules --load


Check if reboot is required.


# if [[ $(auditctl -s | grep "enabled") =~ "2" ]]; then printf "Reboot required to load rules\n"; fi


##### 32 Bit systems

Follow the same procedures as for 64 bit systems and ignore any entries with `b64`. In addition, add `stime` to the system call audit. Example:


-a always,exit -F arch=b32 -S adjtimex,settimeofday,clock_settime,stime -k time-change


**Audit Procedure:** #### 64 Bit systems

***On disk configuration***

Run the following command to check the on disk rules:


# {
awk '/^ *-a *always,exit/ \
&&/ -F *arch=b[2346]{2}/ \
&&/ -S/ \
&&(/adjtimex/ \
||/settimeofday/ \
||/clock_settime/ ) \
&&(/ key= *[!-~]* *$/||/ -k *[!-~]* *$/)' /etc/audit/rules.d/*.rules

awk '/^ *-w/ \
&&/\/etc\/localtime/ \
&&/ +-p *wa/ \
&&(/ key= *[!-~]* *$/||/ -k *[!-~]* *$/)' /etc/audit/rules.d/*.rules
}


Verify output of matches:


-a always,exit -F arch=b64 -S adjtimex,settimeofday,clock_settime -k time-change
-a always,exit -F arch=b32 -S adjtimex,settimeofday,clock_settime -k time-change
-w /etc/localtime -p wa -k time-change


***Running configuration***

Run the following command to check loaded rules:


# {
auditctl -l | awk '/^ *-a *always,exit/ \
&&/ -F *arch=b[2346]{2}/ \
&&/ -S/ \
&&(/adjtimex/ \
||/settimeofday/ \
||/clock_settime/ ) \
&&(/ key= *[!-~]* *$/||/ -k *[!-~]* *$/)'

auditctl -l | awk '/^ *-w/ \
&&/\/etc\/localtime/ \
&&/ +-p *wa/ \
&&(/ key= *[!-~]* *$/||/ -k *[!-~]* *$/)'
}


Verify the output includes:


-a always,exit -F arch=b64 -S adjtimex,settimeofday,clock_settime -F key=time-change
-a always,exit -F arch=b32 -S adjtimex,settimeofday,clock_settime -F key=time-change
-w /etc/localtime -p wa -k time-change


#### 32 Bit systems

Follow the same procedures as for 64 bit systems and ignore any entries with `b64`.

In addition, also audit for the `stime` system call rule. For example:


-a always,exit -F arch=b32 -S adjtimex,settimeofday,clock_settime,stime -k time-change


**Additional Information:** #### Potential reboot required

If the auditing configuration is locked (`-e 2`), then `augenrules` will not warn in any way that rules could not be loaded into the running configuration. A system reboot will be required to load the rules into the running configuration.

#### System call structure

For performance (`man 7 audit.rules`) reasons it is preferable to have all the system calls on one line. However, your configuration may have them on one line each or some other combination. This is important to understand for both the auditing and remediation sections as the examples given are optimized for performance as per the man page.

**CIS Controls:** TITLE:Collect Detailed Audit Logs CONTROL:v8 8.5 DESCRIPTION:Configure detailed audit logging for enterprise assets containing sensitive data. Include event source, date, username, timestamp, source addresses, destination addresses, and other useful elements that could assist in a forensic investigation.;TITLE:Implement Automated Configuration Monitoring Systems CONTROL:v7 5.5 DESCRIPTION:Utilize a Security Content Automation Protocol (SCAP) compliant configuration monitoring system to verify all security configuration elements, catalog approved exceptions, and alert when unauthorized changes occur.;

**CIS Safeguards 1 (v8):** 8.5

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:**  

**v8 IG2:** X

**v8 IG3:** X

**CIS Safeguards 1 (v7):** 5.5

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:**  

**v7 IG2:** X

**v7 IG3:** X

**References:**  


Section #: 4.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.3.5

**Title:** Ensure events that modify the system's network environment are collected

**Assessment Status:** Automated

**Description:** Record changes to network environment files or system calls. The below parameters monitors the following system calls, and write an audit event on system call exit:
- `sethostname` - set the systems host name
- `setdomainname` - set the systems domain name

The files being monitored are:
- `/etc/issue` and `/etc/issue.net` - messages displayed pre-login
- `/etc/hosts` - file containing host names and associated IP addresses
- `/etc/networks` - symbolic names for networks
- `/etc/network/` - directory containing network interface scripts and configurations files

**Rational Statement:** Monitoring `sethostname` and `setdomainname` will identify potential unauthorized changes to host and domainname of a system. The changing of these names could potentially break security parameters that are set based on those names. The `/etc/hosts` file is monitored for changes that can indicate an unauthorized intruder is trying to change machine associations with IP addresses and trick users and processes into connecting to unintended machines. Monitoring `/etc/issue` and `/etc/issue.net` is important, as intruders could put disinformation into those files and trick users into providing information to the intruder. Monitoring `/etc/network` is important as it can show if network interfaces or scripts are being modified in a way that can lead to the machine becoming unavailable or compromised. All audit records should have a relevant tag associated with them.

**Impact Statement:**  

**Remediation Procedure:** #### Create audit rules

Edit or create a file in the `/etc/audit/rules.d/` directory, ending in `.rules` extension, with the relevant rules to monitor events that modify the system's network environment.

##### 64 Bit systems

Example:


# printf "
-a always,exit -F arch=b64 -S sethostname,setdomainname -k system-locale
-a always,exit -F arch=b32 -S sethostname,setdomainname -k system-locale
-w /etc/issue -p wa -k system-locale
-w /etc/issue.net -p wa -k system-locale
-w /etc/hosts -p wa -k system-locale
-w /etc/networks -p wa -k system-locale
-w /etc/network/ -p wa -k system-locale
" >> /etc/audit/rules.d/50-system_local.rules


#### Load audit rules

Merge and load the rules into active configuration:


# augenrules --load


Check if reboot is required.


# if [[ $(auditctl -s | grep "enabled") =~ "2" ]]; then printf "Reboot required to load rules\n"; fi


##### 32 Bit systems

Follow the same procedures as for 64 bit systems and ignore any entries with `b64`.

**Audit Procedure:** #### 64 Bit systems

***On disk configuration***

Run the following commands to check the on disk rules:


# awk '/^ *-a *always,exit/ \
&&/ -F *arch=b(32|64)/ \
&&/ -S/ \
&&(/sethostname/ \
||/setdomainname/) \
&&(/ key= *[!-~]* *$/||/ -k *[!-~]* *$/)' /etc/audit/rules.d/*.rules

# awk '/^ *-w/ \
&&(/\/etc\/issue/ \
||/\/etc\/issue.net/ \
||/\/etc\/hosts/ \
||/\/etc\/network/) \
&&/ +-p *wa/ \
&&(/ key= *[!-~]* *$/||/ -k *[!-~]* *$/)' /etc/audit/rules.d/*.rules


Verify the output matches:


-a always,exit -F arch=b64 -S sethostname,setdomainname -k system-locale
-a always,exit -F arch=b32 -S sethostname,setdomainname -k system-locale
-w /etc/issue -p wa -k system-locale
-w /etc/issue.net -p wa -k system-locale
-w /etc/hosts -p wa -k system-locale
-w /etc/networks -p wa -k system-locale
-w /etc/network/ -p wa -k system-locale


***Running configuration***

Run the following command to check loaded rules:


# auditctl -l | awk '/^ *-a *always,exit/ \
&&/ -F *arch=b(32|64)/ \
&&/ -S/ \
&&(/sethostname/ \
||/setdomainname/) \
&&(/ key= *[!-~]* *$/||/ -k *[!-~]* *$/)'

# auditctl -l | awk '/^ *-w/ \
&&(/\/etc\/issue/ \
||/\/etc\/issue.net/ \
||/\/etc\/hosts/ \
||/\/etc\/network/) \
&&/ +-p *wa/ \
&&(/ key= *[!-~]* *$/||/ -k *[!-~]* *$/)'


Verify the output includes:


-a always,exit -F arch=b64 -S sethostname,setdomainname -F key=system-locale
-a always,exit -F arch=b32 -S sethostname,setdomainname -F key=system-locale
-w /etc/issue -p wa -k system-locale
-w /etc/issue.net -p wa -k system-locale
-w /etc/hosts -p wa -k system-locale
-w /etc/networks -p wa -k system-locale
-w /etc/network/ -p wa -k system-locale


#### 32 Bit systems

Follow the same procedures as for 64 bit systems and ignore any entries with `b64`

**Additional Information:** #### Potential reboot required

If the auditing configuration is locked (`-e 2`), then `augenrules` will not warn in any way that rules could not be loaded into the running configuration. A system reboot will be required to load the rules into the running configuration.

#### System call structure

For performance (`man 7 audit.rules`) reasons it is preferable to have all the system calls on one line. However, your configuration may have them on one line each or some other combination. This is important to understand for both the auditing and remediation sections as the examples given are optimized for performance as per the man page.

**CIS Controls:** TITLE:Collect Detailed Audit Logs CONTROL:v8 8.5 DESCRIPTION:Configure detailed audit logging for enterprise assets containing sensitive data. Include event source, date, username, timestamp, source addresses, destination addresses, and other useful elements that could assist in a forensic investigation.;TITLE:Implement Automated Configuration Monitoring Systems CONTROL:v7 5.5 DESCRIPTION:Utilize a Security Content Automation Protocol (SCAP) compliant configuration monitoring system to verify all security configuration elements, catalog approved exceptions, and alert when unauthorized changes occur.;

**CIS Safeguards 1 (v8):** 8.5

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:**  

**v8 IG2:** X

**v8 IG3:** X

**CIS Safeguards 1 (v7):** 5.5

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:**  

**v7 IG2:** X

**v7 IG3:** X

**References:**  


Section #: 4.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.3.6

**Title:** Ensure use of privileged commands are collected

**Assessment Status:** Automated

**Description:** Monitor privileged programs, those that have the `setuid` and/or `setgid` bit set on execution, to determine if unprivileged users are running these commands.

**Rational Statement:** Execution of privileged commands by non-privileged users could be an indication of someone trying to gain unauthorized access to the system.

**Impact Statement:** Both the audit and remediation section of this recommendation will traverse all mounted file systems that is not mounted with either `noexec` or `nosuid` mount options. If there are large file systems without these mount options, **such traversal will be significantly detrimental to the performance of the system.**

Before running either the audit or remediation section, inspect the output of the following command to determine exactly which file systems will be traversed:


# findmnt -n -l -k -it $(awk '/nodev/ { print $2 }' /proc/filesystems | paste -sd,) | grep -Pv "noexec|nosuid"


To exclude a particular file system due to adverse performance impacts, update the audit and remediation sections by adding a sufficiently unique string to the `grep` statement. The above command can be used to test the modified exclusions.

**Remediation Procedure:** Edit or create a file in the `/etc/audit/rules.d/` directory, ending in `.rules` extension, with the relevant rules to monitor the use of privileged commands.

Example:


# {
UID_MIN=$(awk '/^\s*UID_MIN/{print $2}' /etc/login.defs)
AUDIT_RULE_FILE="/etc/audit/rules.d/50-privileged.rules"
NEW_DATA=()
for PARTITION in $(findmnt -n -l -k -it $(awk '/nodev/ { print $2 }' /proc/filesystems | paste -sd,) | grep -Pv "noexec|nosuid" | awk '{print $1}'); do
readarray -t DATA < <(find "${PARTITION}" -xdev -perm /6000 -type f | awk -v UID_MIN=${UID_MIN} '{print "-a always,exit -F path=" $1 " -F perm=x -F auid>="UID_MIN" -F auid!=unset -k privileged" }')
for ENTRY in "${DATA[@]}"; do
NEW_DATA+=("${ENTRY}")
done
done
readarray &> /dev/null -t OLD_DATA < "${AUDIT_RULE_FILE}"
COMBINED_DATA=( "${OLD_DATA[@]}" "${NEW_DATA[@]}" )
printf '%s\n' "${COMBINED_DATA[@]}" | sort -u > "${AUDIT_RULE_FILE}"
}


Merge and load the rules into active configuration:


# augenrules --load


Check if reboot is required.


# if [[ $(auditctl -s | grep "enabled") =~ "2" ]]; then printf "Reboot required to load rules\n"; fi


#### Special mount points

If there are any special mount points that are not visible by default from just scanning `/`, change the `PARTITION` variable to the appropriate partition and re-run the remediation.

**Audit Procedure:** #### On disk configuration

Run the following command to check on disk rules:


# for PARTITION in $(findmnt -n -l -k -it $(awk '/nodev/ { print $2 }' /proc/filesystems | paste -sd,) | grep -Pv "noexec|nosuid" | awk '{print $1}'); do
for PRIVILEGED in $(find "${PARTITION}" -xdev -perm /6000 -type f); do
grep -qr "${PRIVILEGED}" /etc/audit/rules.d && printf "OK: '${PRIVILEGED}' found in auditing rules.\n" || printf "Warning: '${PRIVILEGED}' not found in on disk configuration.\n"
done
done


Verify that all output is `OK`.

#### Running configuration

Run the following command to check loaded rules:


# {
RUNNING=$(auditctl -l)
[ -n "${RUNNING}" ] && for PARTITION in $(findmnt -n -l -k -it $(awk '/nodev/ { print $2 }' /proc/filesystems | paste -sd,) | grep -Pv "noexec|nosuid" | awk '{print $1}'); do
for PRIVILEGED in $(find "${PARTITION}" -xdev -perm /6000 -type f); do
printf -- "${RUNNING}" | grep -q "${PRIVILEGED}" && printf "OK: '${PRIVILEGED}' found in auditing rules.\n" || printf "Warning: '${PRIVILEGED}' not found in running configuration.\n"
done
done \
|| printf "ERROR: Variable 'RUNNING' is unset.\n"
}


Verify that all output is `OK`.

#### Special mount points

If there are any special mount points that are not visible by default from `findmnt` as per the above audit, said file systems would have to be manually audited.

**Additional Information:** #### Potential reboot required

If the auditing configuration is locked (`-e 2`), then `augenrules` will not warn in any way that rules could not be loaded into the running configuration. A system reboot will be required to load the rules into the running configuration.

#### System call structure

For performance (`man 7 audit.rules`) reasons it is preferable to have all the system calls on one line. However, your configuration may have them on one line each or some other combination. This is important to understand for both the auditing and remediation sections as the examples given are optimized for performance as per the man page.

**NIST SP 800-53 Rev. 5:**
- AU-3
- AU-3(1)

**CIS Controls:** TITLE:Collect Detailed Audit Logs CONTROL:v8 8.5 DESCRIPTION:Configure detailed audit logging for enterprise assets containing sensitive data. Include event source, date, username, timestamp, source addresses, destination addresses, and other useful elements that could assist in a forensic investigation.;TITLE:Activate audit logging CONTROL:v7 6.2 DESCRIPTION:Ensure that local logging has been enabled on all systems and networking devices.;

**CIS Safeguards 1 (v8):** 8.5

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:**  

**v8 IG2:** X

**v8 IG3:** X

**CIS Safeguards 1 (v7):** 6.2

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:** X

**v7 IG2:** X

**v7 IG3:** X

**References:**  


Section #: 4.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.3.7

**Title:** Ensure unsuccessful file access attempts are collected

**Assessment Status:** Automated

**Description:** Monitor for unsuccessful attempts to access files. The following parameters are associated with system calls that control files:
- creation - `creat`
- opening - `open` , `openat`
- truncation - `truncate` , `ftruncate`

An audit log record will only be written if all of the following criteria is met for the user when trying to access a file:
- a non-privileged user (auid>=UID_MIN)
- is not a Daemon event (auid=4294967295/unset/-1)
- if the system call returned EACCES (permission denied) or EPERM (some other permanent error associated with the specific system call)

**Rational Statement:** Failed attempts to open, create or truncate files could be an indication that an individual or process is trying to gain unauthorized access to the system.

**Impact Statement:**  

**Remediation Procedure:** #### Create audit rules

Edit or create a file in the `/etc/audit/rules.d/` directory, ending in `.rules` extension, with the relevant rules to monitor unsuccessful file access attempts.

##### 64 Bit systems

Example:


# {
UID_MIN=$(awk '/^\s*UID_MIN/{print $2}' /etc/login.defs)
[ -n "${UID_MIN}" ] && printf "
-a always,exit -F arch=b64 -S creat,open,openat,truncate,ftruncate -F exit=-EACCES -F auid>=${UID_MIN} -F auid!=unset -k access
-a always,exit -F arch=b64 -S creat,open,openat,truncate,ftruncate -F exit=-EPERM -F auid>=${UID_MIN} -F auid!=unset -k access
-a always,exit -F arch=b32 -S creat,open,openat,truncate,ftruncate -F exit=-EACCES -F auid>=${UID_MIN} -F auid!=unset -k access
-a always,exit -F arch=b32 -S creat,open,openat,truncate,ftruncate -F exit=-EPERM -F auid>=${UID_MIN} -F auid!=unset -k access
" >> /etc/audit/rules.d/50-access.rules || printf "ERROR: Variable 'UID_MIN' is unset.\n"
}


#### Load audit rules

Merge and load the rules into active configuration:


# augenrules --load


Check if reboot is required.


# if [[ $(auditctl -s | grep "enabled") =~ "2" ]]; then printf "Reboot required to load rules\n"; fi


##### 32 Bit systems

Follow the same procedures as for 64 bit systems and ignore any entries with `b64`.

**Audit Procedure:** #### 64 Bit systems

***On disk configuration***

Run the following command to check the on disk rules:


# {
UID_MIN=$(awk '/^\s*UID_MIN/{print $2}' /etc/login.defs)
[ -n "${UID_MIN}" ] && awk "/^ *-a *always,exit/ \
&&/ -F *arch=b[2346]{2}/ \
&&(/ -F *auid!=unset/||/ -F *auid!=-1/||/ -F *auid!=4294967295/) \
&&/ -F *auid>=${UID_MIN}/ \
&&(/ -F *exit=-EACCES/||/ -F *exit=-EPERM/) \
&&/ -S/ \
&&/creat/ \
&&/open/ \
&&/truncate/ \
&&(/ key= *[!-~]* *$/||/ -k *[!-~]* *$/)" /etc/audit/rules.d/*.rules \
|| printf "ERROR: Variable 'UID_MIN' is unset.\n"
}


Verify the output includes:


-a always,exit -F arch=b64 -S creat,open,openat,truncate,ftruncate -F exit=-EACCES -F auid>=1000 -F auid!=unset -k access
-a always,exit -F arch=b64 -S creat,open,openat,truncate,ftruncate -F exit=-EPERM -F auid>=1000 -F auid!=unset -k access
-a always,exit -F arch=b32 -S creat,open,openat,truncate,ftruncate -F exit=-EACCES -F auid>=1000 -F auid!=unset -k access
-a always,exit -F arch=b32 -S creat,open,openat,truncate,ftruncate -F exit=-EPERM -F auid>=1000 -F auid!=unset -k access


***Running configuration***

Run the following command to check loaded rules:


# {
UID_MIN=$(awk '/^\s*UID_MIN/{print $2}' /etc/login.defs)
[ -n "${UID_MIN}" ] && auditctl -l | awk "/^ *-a *always,exit/ \
&&/ -F *arch=b[2346]{2}/ \
&&(/ -F *auid!=unset/||/ -F *auid!=-1/||/ -F *auid!=4294967295/) \
&&/ -F *auid>=${UID_MIN}/ \
&&(/ -F *exit=-EACCES/||/ -F *exit=-EPERM/) \
&&/ -S/ \
&&/creat/ \
&&/open/ \
&&/truncate/ \
&&(/ key= *[!-~]* *$/||/ -k *[!-~]* *$/)" \
|| printf "ERROR: Variable 'UID_MIN' is unset.\n"
}


Verify the output includes:


-a always,exit -F arch=b64 -S open,truncate,ftruncate,creat,openat -F exit=-EACCES -F auid>=1000 -F auid!=-1 -F key=access
-a always,exit -F arch=b64 -S open,truncate,ftruncate,creat,openat -F exit=-EPERM -F auid>=1000 -F auid!=-1 -F key=access
-a always,exit -F arch=b32 -S open,truncate,ftruncate,creat,openat -F exit=-EACCES -F auid>=1000 -F auid!=-1 -F key=access
-a always,exit -F arch=b32 -S open,truncate,ftruncate,creat,openat -F exit=-EPERM -F auid>=1000 -F auid!=-1 -F key=access


#### 32 Bit systems

Follow the same procedures as for 64 bit systems and ignore any entries with `b64`.

**Additional Information:** #### Potential reboot required

If the auditing configuration is locked (`-e 2`), then `augenrules` will not warn in any way that rules could not be loaded into the running configuration. A system reboot will be required to load the rules into the running configuration.

#### System call structure

For performance (`man 7 audit.rules`) reasons it is preferable to have all the system calls on one line. However, your configuration may have them on one line each or some other combination. This is important to understand for both the auditing and remediation sections as the examples given are optimized for performance as per the man page.

**CIS Controls:** TITLE:Collect Detailed Audit Logs CONTROL:v8 8.5 DESCRIPTION:Configure detailed audit logging for enterprise assets containing sensitive data. Include event source, date, username, timestamp, source addresses, destination addresses, and other useful elements that could assist in a forensic investigation.;TITLE:Enforce Detail Logging for Access or Changes to Sensitive Data CONTROL:v7 14.9 DESCRIPTION:Enforce detailed audit logging for access to sensitive data or changes to sensitive data (utilizing tools such as File Integrity Monitoring or Security Information and Event Monitoring).;

**CIS Safeguards 1 (v8):** 8.5

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:**  

**v8 IG2:** X

**v8 IG3:** X

**CIS Safeguards 1 (v7):** 14.9

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:**  

**v7 IG2:**  

**v7 IG3:** X

**References:**  


Section #: 4.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.3.8

**Title:** Ensure events that modify user/group information are collected

**Assessment Status:** Automated

**Description:** Record events affecting the modification of user or group information, including that of passwords and old passwords if in use.
- `/etc/group` - system groups
- `/etc/passwd` - system users
- `/etc/gshadow` - encrypted password for each group
- `/etc/shadow` - system user passwords
- `/etc/security/opasswd` - storage of old passwords if the relevant PAM module is in use

The parameters in this section will watch the files to see if they have been opened for write or have had attribute changes (e.g. permissions) and tag them with the identifier "identity" in the audit log file.

**Rational Statement:** Unexpected changes to these files could be an indication that the system has been compromised and that an unauthorized user is attempting to hide their activities or compromise additional accounts.

**Impact Statement:**  

**Remediation Procedure:** Edit or create a file in the `/etc/audit/rules.d/` directory, ending in `.rules` extension, with the relevant rules to monitor events that modify user/group information.

Example:


# printf "
-w /etc/group -p wa -k identity
-w /etc/passwd -p wa -k identity
-w /etc/gshadow -p wa -k identity
-w /etc/shadow -p wa -k identity
-w /etc/security/opasswd -p wa -k identity
" >> /etc/audit/rules.d/50-identity.rules


Merge and load the rules into active configuration:


# augenrules --load


Check if reboot is required.


# if [[ $(auditctl -s | grep "enabled") =~ "2" ]]; then printf "Reboot required to load rules\n"; fi


**Audit Procedure:** #### On disk configuration

Run the following command to check the on disk rules:


# awk '/^ *-w/ \
&&(/\/etc\/group/ \
||/\/etc\/passwd/ \
||/\/etc\/gshadow/ \
||/\/etc\/shadow/ \
||/\/etc\/security\/opasswd/) \
&&/ +-p *wa/ \
&&(/ key= *[!-~]* *$/||/ -k *[!-~]* *$/)' /etc/audit/rules.d/*.rules


Verify the output matches:


-w /etc/group -p wa -k identity
-w /etc/passwd -p wa -k identity
-w /etc/gshadow -p wa -k identity
-w /etc/shadow -p wa -k identity
-w /etc/security/opasswd -p wa -k identity


#### Running configuration

Run the following command to check loaded rules:


# auditctl -l | awk '/^ *-w/ \
&&(/\/etc\/group/ \
||/\/etc\/passwd/ \
||/\/etc\/gshadow/ \
||/\/etc\/shadow/ \
||/\/etc\/security\/opasswd/) \
&&/ +-p *wa/ \
&&(/ key= *[!-~]* *$/||/ -k *[!-~]* *$/)'


Verify the output matches:


-w /etc/group -p wa -k identity
-w /etc/passwd -p wa -k identity
-w /etc/gshadow -p wa -k identity
-w /etc/shadow -p wa -k identity
-w /etc/security/opasswd -p wa -k identity


**Additional Information:** #### Potential reboot required

If the auditing configuration is locked (`-e 2`), then `augenrules` will not warn in any way that rules could not be loaded into the running configuration. A system reboot will be required to load the rules into the running configuration.

#### System call structure

For performance (`man 7 audit.rules`) reasons it is preferable to have all the system calls on one line. However, your configuration may have them on one line each or some other combination. This is important to understand for both the auditing and remediation sections as the examples given are optimized for performance as per the man page.

**CIS Controls:** TITLE:Collect Detailed Audit Logs CONTROL:v8 8.5 DESCRIPTION:Configure detailed audit logging for enterprise assets containing sensitive data. Include event source, date, username, timestamp, source addresses, destination addresses, and other useful elements that could assist in a forensic investigation.;TITLE:Log and Alert on Changes to Administrative Group Membership CONTROL:v7 4.8 DESCRIPTION:Configure systems to issue a log entry and alert when an account is added to or removed from any group assigned administrative privileges.;

**CIS Safeguards 1 (v8):** 8.5

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:**  

**v8 IG2:** X

**v8 IG3:** X

**CIS Safeguards 1 (v7):** 4.8

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:**  

**v7 IG2:** X

**v7 IG3:** X

**References:**  


Section #: 4.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.3.9

**Title:** Ensure discretionary access control permission modification events are collected

**Assessment Status:** Automated

**Description:** Monitor changes to file permissions, attributes, ownership and group. The parameters in this section track changes for system calls that affect file permissions and attributes. The following commands and system calls effect the permissions, ownership and various attributes of files.
- `chmod`
- `fchmod`
- `fchmodat`
- `chown`
- `fchown`
- `fchownat`
- `lchown`
- `setxattr`
- `lsetxattr`
- `fsetxattr`
- `removexattr`
- `lremovexattr`
- `fremovexattr`

In all cases, an audit record will only be written for non-system user ids and will ignore Daemon events. All audit records will be tagged with the identifier "perm_mod."

**Rational Statement:** Monitoring for changes in file attributes could alert a system administrator to activity that could indicate intruder activity or policy violation.

**Impact Statement:**  

**Remediation Procedure:** #### Create audit rules

Edit or create a file in the `/etc/audit/rules.d/` directory, ending in `.rules` extension, with the relevant rules to monitor discretionary access control permission modification events.

##### 64 Bit systems

Example:


# {
UID_MIN=$(awk '/^\s*UID_MIN/{print $2}' /etc/login.defs)
[ -n "${UID_MIN}" ] && printf "
-a always,exit -F arch=b64 -S chmod,fchmod,fchmodat -F auid>=${UID_MIN} -F auid!=unset -F key=perm_mod
-a always,exit -F arch=b64 -S chown,fchown,lchown,fchownat -F auid>=${UID_MIN} -F auid!=unset -F key=perm_mod
-a always,exit -F arch=b32 -S chmod,fchmod,fchmodat -F auid>=${UID_MIN} -F auid!=unset -F key=perm_mod
-a always,exit -F arch=b32 -S lchown,fchown,chown,fchownat -F auid>=${UID_MIN} -F auid!=unset -F key=perm_mod
-a always,exit -F arch=b64 -S setxattr,lsetxattr,fsetxattr,removexattr,lremovexattr,fremovexattr -F auid>=${UID_MIN} -F auid!=unset -F key=perm_mod
-a always,exit -F arch=b32 -S setxattr,lsetxattr,fsetxattr,removexattr,lremovexattr,fremovexattr -F auid>=${UID_MIN} -F auid!=unset -F key=perm_mod
" >> /etc/audit/rules.d/50-perm_mod.rules || printf "ERROR: Variable 'UID_MIN' is unset.\n"
}


#### Load audit rules

Merge and load the rules into active configuration:


# augenrules --load


Check if reboot is required.


# if [[ $(auditctl -s | grep "enabled") =~ "2" ]]; then printf "Reboot required to load rules\n"; fi


##### 32 Bit systems

Follow the same procedures as for 64 bit systems and ignore any entries with `b64`.

**Audit Procedure:** #### 64 Bit systems

***On disk configuration***

Run the following command to check the on disk rules:


# {
UID_MIN=$(awk '/^\s*UID_MIN/{print $2}' /etc/login.defs)
[ -n "${UID_MIN}" ] && awk "/^ *-a *always,exit/ \
&&/ -F *arch=b[2346]{2}/ \
&&(/ -F *auid!=unset/||/ -F *auid!=-1/||/ -F *auid!=4294967295/) \
&&/ -S/ \
&&/ -F *auid>=${UID_MIN}/ \
&&(/chmod/||/fchmod/||/fchmodat/ \
||/chown/||/fchown/||/fchownat/||/lchown/ \
||/setxattr/||/lsetxattr/||/fsetxattr/ \
||/removexattr/||/lremovexattr/||/fremovexattr/) \
&&(/ key= *[!-~]* *$/||/ -k *[!-~]* *$/)" /etc/audit/rules.d/*.rules \
|| printf "ERROR: Variable 'UID_MIN' is unset.\n"
}


Verify the output matches:

-a always,exit -F arch=b64 -S chmod,fchmod,fchmodat -F auid>=1000 -F auid!=unset -F key=perm_mod
-a always,exit -F arch=b64 -S chown,fchown,lchown,fchownat -F auid>=1000 -F auid!=unset -F key=perm_mod
-a always,exit -F arch=b32 -S chmod,fchmod,fchmodat -F auid>=1000 -F auid!=unset -F key=perm_mod
-a always,exit -F arch=b32 -S lchown,fchown,chown,fchownat -F auid>=1000 -F auid!=unset -F key=perm_mod
-a always,exit -F arch=b64 -S setxattr,lsetxattr,fsetxattr,removexattr,lremovexattr,fremovexattr -F auid>=1000 -F auid!=unset -F key=perm_mod
-a always,exit -F arch=b32 -S setxattr,lsetxattr,fsetxattr,removexattr,lremovexattr,fremovexattr -F auid>=1000 -F auid!=unset -F key=perm_mod


***Running configuration***

Run the following command to check loaded rules:


# {
UID_MIN=$(awk '/^\s*UID_MIN/{print $2}' /etc/login.defs)
[ -n "${UID_MIN}" ] && auditctl -l | awk "/^ *-a *always,exit/ \
&&/ -F *arch=b[2346]{2}/ \
&&(/ -F *auid!=unset/||/ -F *auid!=-1/||/ -F *auid!=4294967295/) \
&&/ -S/ \
&&/ -F *auid>=${UID_MIN}/ \
&&(/chmod/||/fchmod/||/fchmodat/ \
||/chown/||/fchown/||/fchownat/||/lchown/ \
||/setxattr/||/lsetxattr/||/fsetxattr/ \
||/removexattr/||/lremovexattr/||/fremovexattr/) \
&&(/ key= *[!-~]* *$/||/ -k *[!-~]* *$/)" \
|| printf "ERROR: Variable 'UID_MIN' is unset.\n"
}


Verify the output matches:


-a always,exit -F arch=b64 -S chmod,fchmod,fchmodat -F auid>=1000 -F auid!=-1 -F key=perm_mod
-a always,exit -F arch=b64 -S chown,fchown,lchown,fchownat -F auid>=1000 -F auid!=-1 -F key=perm_mod
-a always,exit -F arch=b32 -S chmod,fchmod,fchmodat -F auid>=1000 -F auid!=-1 -F key=perm_mod
-a always,exit -F arch=b32 -S lchown,fchown,chown,fchownat -F auid>=1000 -F auid!=-1 -F key=perm_mod
-a always,exit -F arch=b64 -S setxattr,lsetxattr,fsetxattr,removexattr,lremovexattr,fremovexattr -F auid>=1000 -F auid!=-1 -F key=perm_mod
-a always,exit -F arch=b32 -S setxattr,lsetxattr,fsetxattr,removexattr,lremovexattr,fremovexattr -F auid>=1000 -F auid!=-1 -F key=perm_mod


#### 32 Bit systems

Follow the same procedures as for 64 bit systems and ignore any entries with `b64`.

**Additional Information:** #### Potential reboot required

If the auditing configuration is locked (`-e 2`), then `augenrules` will not warn in any way that rules could not be loaded into the running configuration. A system reboot will be required to load the rules into the running configuration.

#### System call structure

For performance (`man 7 audit.rules`) reasons it is preferable to have all the system calls on one line. However, your configuration may have them on one line each or some other combination. This is important to understand for both the auditing and remediation sections as the examples given are optimized for performance as per the man page.

**CIS Controls:** TITLE:Collect Detailed Audit Logs CONTROL:v8 8.5 DESCRIPTION:Configure detailed audit logging for enterprise assets containing sensitive data. Include event source, date, username, timestamp, source addresses, destination addresses, and other useful elements that could assist in a forensic investigation.;TITLE:Implement Automated Configuration Monitoring Systems CONTROL:v7 5.5 DESCRIPTION:Utilize a Security Content Automation Protocol (SCAP) compliant configuration monitoring system to verify all security configuration elements, catalog approved exceptions, and alert when unauthorized changes occur.;

**CIS Safeguards 1 (v8):** 8.5

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:**  

**v8 IG2:** X

**v8 IG3:** X

**CIS Safeguards 1 (v7):** 5.5

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:**  

**v7 IG2:** X

**v7 IG3:** X

**References:**  


Section #: 4.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.3.10

**Title:** Ensure successful file system mounts are collected

**Assessment Status:** Automated

**Description:** Monitor the use of the `mount` system call. The `mount` (and `umount` ) system call controls the mounting and unmounting of file systems. The parameters below configure the system to create an audit record when the mount system call is used by a non-privileged user

**Rational Statement:** It is highly unusual for a non privileged user to `mount` file systems to the system. While tracking `mount` commands gives the system administrator evidence that external media may have been mounted (based on a review of the source of the mount and confirming it's an external media type), it does not conclusively indicate that data was exported to the media. System administrators who wish to determine if data were exported, would also have to track successful `open`, `creat` and `truncate` system calls requiring write access to a file under the mount point of the external media file system. This could give a fair indication that a write occurred. The only way to truly prove it, would be to track successful writes to the external media. Tracking write system calls could quickly fill up the audit log and is not recommended. Recommendations on configuration options to track data export to media is beyond the scope of this document.

**Impact Statement:**  

**Remediation Procedure:** #### Create audit rules

Edit or create a file in the `/etc/audit/rules.d/` directory, ending in `.rules` extension, with the relevant rules to monitor successful file system mounts.

##### 64 Bit systems

Example:



# {
UID_MIN=$(awk '/^\s*UID_MIN/{print $2}' /etc/login.defs)
[ -n "${UID_MIN}" ] && printf "
-a always,exit -F arch=b32 -S mount -F auid>=$UID_MIN -F auid!=unset -k mounts
-a always,exit -F arch=b64 -S mount -F auid>=$UID_MIN -F auid!=unset -k mounts
" >> /etc/audit/rules.d/50-mounts.rules || printf "ERROR: Variable 'UID_MIN' is unset.\n"
}


#### Load audit rules

Merge and load the rules into active configuration:


# augenrules --load


Check if reboot is required.


# if [[ $(auditctl -s | grep "enabled") =~ "2" ]]; then printf "Reboot required to load rules\n"; fi


##### 32 Bit systems

Follow the same procedures as for 64 bit systems and ignore any entries with `b64`.

**Audit Procedure:** #### 64 Bit systems

***On disk configuration***

Run the following command to check the on disk rules:


# {
UID_MIN=$(awk '/^\s*UID_MIN/{print $2}' /etc/login.defs)
[ -n "${UID_MIN}" ] && awk "/^ *-a *always,exit/ \
&&/ -F *arch=b[2346]{2}/ \
&&(/ -F *auid!=unset/||/ -F *auid!=-1/||/ -F *auid!=4294967295/) \
&&/ -F *auid>=${UID_MIN}/ \
&&/ -S/ \
&&/mount/ \
&&(/ key= *[!-~]* *$/||/ -k *[!-~]* *$/)" /etc/audit/rules.d/*.rules \
|| printf "ERROR: Variable 'UID_MIN' is unset.\n"
}


Verify the output matches:


-a always,exit -F arch=b64 -S mount -F auid>=1000 -F auid!=unset -k mounts
-a always,exit -F arch=b32 -S mount -F auid>=1000 -F auid!=unset -k mounts


***Running configuration***

Run the following command to check loaded rules:



# {
UID_MIN=$(awk '/^\s*UID_MIN/{print $2}' /etc/login.defs)
[ -n "${UID_MIN}" ] && auditctl -l | awk "/^ *-a *always,exit/ \
&&/ -F *arch=b[2346]{2}/ \
&&(/ -F *auid!=unset/||/ -F *auid!=-1/||/ -F *auid!=4294967295/) \
&&/ -F *auid>=${UID_MIN}/ \
&&/ -S/ \
&&/mount/ \
&&(/ key= *[!-~]* *$/||/ -k *[!-~]* *$/)" \
|| printf "ERROR: Variable 'UID_MIN' is unset.\n"
}


Verify the output matches:


-a always,exit -F arch=b64 -S mount -F auid>=1000 -F auid!=-1 -F key=mounts
-a always,exit -F arch=b32 -S mount -F auid>=1000 -F auid!=-1 -F key=mounts


#### 32 Bit systems

Follow the same procedures as for 64 bit systems and ignore any entries with `b64`.

**Additional Information:** #### Potential reboot required

If the auditing configuration is locked (`-e 2`), then `augenrules` will not warn in any way that rules could not be loaded into the running configuration. A system reboot will be required to load the rules into the running configuration.

#### System call structure

For performance (`man 7 audit.rules`) reasons it is preferable to have all the system calls on one line. However, your configuration may have them on one line each or some other combination. This is important to understand for both the auditing and remediation sections as the examples given are optimized for performance as per the man page.

**NIST SP 800-53 Rev. 5:**
- CM-6

**CIS Controls:** TITLE:Collect Detailed Audit Logs CONTROL:v8 8.5 DESCRIPTION:Configure detailed audit logging for enterprise assets containing sensitive data. Include event source, date, username, timestamp, source addresses, destination addresses, and other useful elements that could assist in a forensic investigation.;TITLE:Enable Detailed Logging CONTROL:v7 6.3 DESCRIPTION:Enable system logging to include detailed information such as an event source, date, user, timestamp, source addresses, destination addresses, and other useful elements.;

**CIS Safeguards 1 (v8):** 8.5

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:**  

**v8 IG2:** X

**v8 IG3:** X

**CIS Safeguards 1 (v7):** 6.3

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:**  

**v7 IG2:** X

**v7 IG3:** X

**References:**  


Section #: 4.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.3.11

**Title:** Ensure session initiation information is collected

**Assessment Status:** Automated

**Description:** Monitor session initiation events. The parameters in this section track changes to the files associated with session events.
- `/var/run/utmp` - tracks all currently logged in users.
- `/var/log/wtmp` - file tracks logins, logouts, shutdown, and reboot events.
- `/var/log/btmp` - keeps track of failed login attempts and can be read by entering the command `/usr/bin/last -f /var/log/btmp`.

All audit records will be tagged with the identifier "session."

**Rational Statement:** Monitoring these files for changes could alert a system administrator to logins occurring at unusual hours, which could indicate intruder activity (i.e. a user logging in at a time when they do not normally log in).

**Impact Statement:**  

**Remediation Procedure:** Edit or create a file in the `/etc/audit/rules.d/` directory, ending in `.rules` extension, with the relevant rules to monitor session initiation information.

Example:



# printf "
-w /var/run/utmp -p wa -k session
-w /var/log/wtmp -p wa -k session
-w /var/log/btmp -p wa -k session
" >> /etc/audit/rules.d/50-session.rules


Merge and load the rules into active configuration:


# augenrules --load


Check if reboot is required.


# if [[ $(auditctl -s | grep "enabled") =~ "2" ]]; then printf "Reboot required to load rules\n"; fi


**Audit Procedure:** #### On disk configuration

Run the following command to check the on disk rules:


# awk '/^ *-w/ \
&&(/\/var\/run\/utmp/ \
||/\/var\/log\/wtmp/ \
||/\/var\/log\/btmp/) \
&&/ +-p *wa/ \
&&(/ key= *[!-~]* *$/||/ -k *[!-~]* *$/)' /etc/audit/rules.d/*.rules


Verify the output matches:


-w /var/run/utmp -p wa -k session
-w /var/log/wtmp -p wa -k session
-w /var/log/btmp -p wa -k session


#### Running configuration

Run the following command to check loaded rules:


# auditctl -l | awk '/^ *-w/ \
&&(/\/var\/run\/utmp/ \
||/\/var\/log\/wtmp/ \
||/\/var\/log\/btmp/) \
&&/ +-p *wa/ \
&&(/ key= *[!-~]* *$/||/ -k *[!-~]* *$/)'


Verify the output matches:


-w /var/run/utmp -p wa -k session
-w /var/log/wtmp -p wa -k session
-w /var/log/btmp -p wa -k session


**Additional Information:** #### Potential reboot required

If the auditing configuration is locked (`-e 2`), then `augenrules` will not warn in any way that rules could not be loaded into the running configuration. A system reboot will be required to load the rules into the running configuration.

#### System call structure

For performance (`man 7 audit.rules`) reasons it is preferable to have all the system calls on one line. However, your configuration may have them on one line each or some other combination. This is important to understand for both the auditing and remediation sections as the examples given are optimized for performance as per the man page.

**NIST SP 800-53 Rev. 5:**
- AU-3
- AU-3(1)

**CIS Controls:** TITLE:Collect Detailed Audit Logs CONTROL:v8 8.5 DESCRIPTION:Configure detailed audit logging for enterprise assets containing sensitive data. Include event source, date, username, timestamp, source addresses, destination addresses, and other useful elements that could assist in a forensic investigation.;TITLE:Log and Alert on Unsuccessful Administrative Account Login CONTROL:v7 4.9 DESCRIPTION:Configure systems to issue a log entry and alert on unsuccessful logins to an administrative account.;TITLE:Alert on Account Login Behavior Deviation CONTROL:v7 16.13 DESCRIPTION:Alert when users deviate from normal login behavior, such as time-of-day, workstation location and duration.;

**CIS Safeguards 1 (v8):** 8.5

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:**  

**v8 IG2:** X

**v8 IG3:** X

**CIS Safeguards 1 (v7):** 4.9

**CIS Safeguards 2 (v7):** 16.13

**CIS Safeguards 3 (v7):**  

**v7 IG1:**  

**v7 IG2:** X

**v7 IG3:** X

**References:**  


Section #: 4.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.3.12

**Title:** Ensure login and logout events are collected

**Assessment Status:** Automated

**Description:** Monitor login and logout events. The parameters below track changes to files associated with login/logout events.
- `/var/log/lastlog` - maintain records of the last time a user successfully logged in. 
- `/var/run/faillock` - directory maintains records of login failures via the `pam_faillock` module.

**Rational Statement:** Monitoring login/logout events could provide a system administrator with information associated with brute force attacks against user logins.

**Impact Statement:**  

**Remediation Procedure:** Edit or create a file in the `/etc/audit/rules.d/` directory, ending in `.rules` extension, with the relevant rules to monitor login and logout events.

Example:


# printf "
-w /var/log/lastlog -p wa -k logins
-w /var/run/faillock -p wa -k logins
" >> /etc/audit/rules.d/50-login.rules


Merge and load the rules into active configuration:


# augenrules --load


Check if reboot is required.


# if [[ $(auditctl -s | grep "enabled") =~ "2" ]]; then printf "Reboot required to load rules\n"; fi


**Audit Procedure:** #### On disk configuration

Run the following command to check the on disk rules:


# awk '/^ *-w/ \
&&(/\/var\/log\/lastlog/ \
||/\/var\/run\/faillock/) \
&&/ +-p *wa/ \
&&(/ key= *[!-~]* *$/||/ -k *[!-~]* *$/)' /etc/audit/rules.d/*.rules


Verify the output matches:


-w /var/log/lastlog -p wa -k logins
-w /var/run/faillock -p wa -k logins


#### Running configuration

Run the following command to check loaded rules:



# auditctl -l | awk '/^ *-w/ \
&&(/\/var\/log\/lastlog/ \
||/\/var\/run\/faillock/) \
&&/ +-p *wa/ \
&&(/ key= *[!-~]* *$/||/ -k *[!-~]* *$/)'


Verify the output matches:


-w /var/log/lastlog -p wa -k logins
-w /var/run/faillock -p wa -k logins


**Additional Information:** #### Potential reboot required

If the auditing configuration is locked (`-e 2`), then `augenrules` will not warn in any way that rules could not be loaded into the running configuration. A system reboot will be required to load the rules into the running configuration.

#### System call structure

For performance (`man 7 audit.rules`) reasons it is preferable to have all the system calls on one line. However, your configuration may have them on one line each or some other combination. This is important to understand for both the auditing and remediation sections as the examples given are optimized for performance as per the man page.

**NIST SP 800-53 Rev. 5:**
- AU-3
- AU-3(1)

**CIS Controls:** TITLE:Collect Detailed Audit Logs CONTROL:v8 8.5 DESCRIPTION:Configure detailed audit logging for enterprise assets containing sensitive data. Include event source, date, username, timestamp, source addresses, destination addresses, and other useful elements that could assist in a forensic investigation.;TITLE:Log and Alert on Unsuccessful Administrative Account Login CONTROL:v7 4.9 DESCRIPTION:Configure systems to issue a log entry and alert on unsuccessful logins to an administrative account.;TITLE:Lock Workstation Sessions After Inactivity CONTROL:v7 16.11 DESCRIPTION:Automatically lock workstation sessions after a standard period of inactivity.;TITLE:Alert on Account Login Behavior Deviation CONTROL:v7 16.13 DESCRIPTION:Alert when users deviate from normal login behavior, such as time-of-day, workstation location and duration.;

**CIS Safeguards 1 (v8):** 8.5

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:**  

**v8 IG2:** X

**v8 IG3:** X

**CIS Safeguards 1 (v7):** 4.9

**CIS Safeguards 2 (v7):** 16.11

**CIS Safeguards 3 (v7):** 16.13

**v7 IG1:** X

**v7 IG2:** X

**v7 IG3:** X

**References:**  


Section #: 4.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.3.13

**Title:** Ensure file deletion events by users are collected

**Assessment Status:** Automated

**Description:** Monitor the use of system calls associated with the deletion or renaming of files and file attributes. This configuration statement sets up monitoring for:
- `unlink` - remove a file
- `unlinkat` - remove a file attribute
- `rename` - rename a file
- `renameat` rename a file attribute
system calls and tags them with the identifier "delete".

**Rational Statement:** Monitoring these calls from non-privileged users could provide a system administrator with evidence that inappropriate removal of files and file attributes associated with protected files is occurring. While this audit option will look at all events, system administrators will want to look for specific privileged files that are being deleted or altered.

**Impact Statement:**  

**Remediation Procedure:** #### Create audit rules

Edit or create a file in the `/etc/audit/rules.d/` directory, ending in `.rules` extension, with the relevant rules to monitor file deletion events by users.

##### 64 Bit systems

Example:


# {
UID_MIN=$(awk '/^\s*UID_MIN/{print $2}' /etc/login.defs)
[ -n "${UID_MIN}" ] && printf "
-a always,exit -F arch=b64 -S rename,unlink,unlinkat,renameat -F auid>=${UID_MIN} -F auid!=unset -F key=delete
-a always,exit -F arch=b32 -S rename,unlink,unlinkat,renameat -F auid>=${UID_MIN} -F auid!=unset -F key=delete
" >> /etc/audit/rules.d/50-delete.rules || printf "ERROR: Variable 'UID_MIN' is unset.\n"
}


#### Load audit rules

Merge and load the rules into active configuration:


# augenrules --load


Check if reboot is required.


# if [[ $(auditctl -s | grep "enabled") =~ "2" ]]; then printf "Reboot required to load rules\n"; fi


##### 32 Bit systems

Follow the same procedures as for 64 bit systems and ignore any entries with `b64`.

**Audit Procedure:** #### 64 Bit systems

***On disk configuration***

Run the following command to check the on disk rules:



# {
UID_MIN=$(awk '/^\s*UID_MIN/{print $2}' /etc/login.defs)
[ -n "${UID_MIN}" ] && awk "/^ *-a *always,exit/ \
&&/ -F *arch=b[2346]{2}/ \
&&(/ -F *auid!=unset/||/ -F *auid!=-1/||/ -F *auid!=4294967295/) \
&&/ -F *auid>=${UID_MIN}/ \
&&/ -S/ \
&&(/unlink/||/rename/||/unlinkat/||/renameat/) \
&&(/ key= *[!-~]* *$/||/ -k *[!-~]* *$/)" /etc/audit/rules.d/*.rules \
|| printf "ERROR: Variable 'UID_MIN' is unset.\n"
}


Verify the output matches:


-a always,exit -F arch=b64 -S unlink,unlinkat,rename,renameat -F auid>=1000 -F auid!=unset -k delete
-a always,exit -F arch=b32 -S unlink,unlinkat,rename,renameat -F auid>=1000 -F auid!=unset -k delete


***Running configuration***

Run the following command to check loaded rules:


# {
UID_MIN=$(awk '/^\s*UID_MIN/{print $2}' /etc/login.defs)
[ -n "${UID_MIN}" ] && auditctl -l | awk "/^ *-a *always,exit/ \
&&/ -F *arch=b[2346]{2}/ \
&&(/ -F *auid!=unset/||/ -F *auid!=-1/||/ -F *auid!=4294967295/) \
&&/ -F *auid>=${UID_MIN}/ \
&&/ -S/ \
&&(/unlink/||/rename/||/unlinkat/||/renameat/) \
&&(/ key= *[!-~]* *$/||/ -k *[!-~]* *$/)" \
|| printf "ERROR: Variable 'UID_MIN' is unset.\n"
}


Verify the output matches:


-a always,exit -F arch=b64 -S rename,unlink,unlinkat,renameat -F auid>=1000 -F auid!=-1 -F key=delete
-a always,exit -F arch=b32 -S unlink,rename,unlinkat,renameat -F auid>=1000 -F auid!=-1 -F key=delete

#### 32 Bit systems

Follow the same procedures as for 64 bit systems and ignore any entries with `b64`.

**Additional Information:** #### Potential reboot required

If the auditing configuration is locked (`-e 2`), then `augenrules` will not warn in any way that rules could not be loaded into the running configuration. A system reboot will be required to load the rules into the running configuration.

#### System call structure

For performance (`man 7 audit.rules`) reasons it is preferable to have all the system calls on one line. However, your configuration may have them on one line each or some other combination. This is important to understand for both the auditing and remediation sections as the examples given are optimized for performance as per the man page.

**CIS Controls:** TITLE:Collect Detailed Audit Logs CONTROL:v8 8.5 DESCRIPTION:Configure detailed audit logging for enterprise assets containing sensitive data. Include event source, date, username, timestamp, source addresses, destination addresses, and other useful elements that could assist in a forensic investigation.;TITLE:Activate audit logging CONTROL:v7 6.2 DESCRIPTION:Ensure that local logging has been enabled on all systems and networking devices.;

**CIS Safeguards 1 (v8):** 8.5

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:**  

**v8 IG2:** X

**v8 IG3:** X

**CIS Safeguards 1 (v7):** 6.2

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:** X

**v7 IG2:** X

**v7 IG3:** X

**References:**  


Section #: 4.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.3.14

**Title:** Ensure events that modify the system's Mandatory Access Controls are collected

**Assessment Status:** Automated

**Description:** Monitor AppArmor, an implementation of mandatory access controls. The parameters below monitor any write access (potential additional, deletion or modification of files in the directory) or attribute changes to the `/etc/apparmor/` and `/etc/apparmor.d/` directories.

**Note:** If a different Mandatory Access Control method is used, changes to the corresponding directories should be audited.

**Rational Statement:** Changes to files in the `/etc/apparmor/` and `/etc/apparmor.d/` directories could indicate that an unauthorized user is attempting to modify access controls and change security contexts, leading to a compromise of the system.

**Impact Statement:**  

**Remediation Procedure:** Edit or create a file in the `/etc/audit/rules.d/` directory, ending in `.rules` extension, with the relevant rules to monitor events that modify the system's Mandatory Access Controls.

Example:


# printf "
-w /etc/apparmor/ -p wa -k MAC-policy
-w /etc/apparmor.d/ -p wa -k MAC-policy
" >> /etc/audit/rules.d/50-MAC-policy.rules


Merge and load the rules into active configuration:


# augenrules --load


Check if reboot is required.


# if [[ $(auditctl -s | grep "enabled") =~ "2" ]]; then printf "Reboot required to load rules\n"; fi


**Audit Procedure:** #### On disk configuration

Run the following command to check the on disk rules:


# awk '/^ *-w/ \
&&(/\/etc\/apparmor/ \
||/\/etc\/apparmor.d/) \
&&/ +-p *wa/ \
&&(/ key= *[!-~]* *$/||/ -k *[!-~]* *$/)' /etc/audit/rules.d/*.rules


Verify the output matches:


-w /etc/apparmor/ -p wa -k MAC-policy
-w /etc/apparmor.d/ -p wa -k MAC-policy


#### Running configuration

Run the following command to check loaded rules:


# auditctl -l | awk '/^ *-w/ \
&&(/\/etc\/apparmor/ \
||/\/etc\/apparmor.d/) \
&&/ +-p *wa/ \
&&(/ key= *[!-~]* *$/||/ -k *[!-~]* *$/)'


Verify the output matches:


-w /etc/apparmor/ -p wa -k MAC-policy
-w /etc/apparmor.d/ -p wa -k MAC-policy


**Additional Information:** #### Potential reboot required

If the auditing configuration is locked (`-e 2`), then `augenrules` will not warn in any way that rules could not be loaded into the running configuration. A system reboot will be required to load the rules into the running configuration.

#### System call structure

For performance (`man 7 audit.rules`) reasons it is preferable to have all the system calls on one line. However, your configuration may have them on one line each or some other combination. This is important to understand for both the auditing and remediation sections as the examples given are optimized for performance as per the man page.

**CIS Controls:** TITLE:Collect Detailed Audit Logs CONTROL:v8 8.5 DESCRIPTION:Configure detailed audit logging for enterprise assets containing sensitive data. Include event source, date, username, timestamp, source addresses, destination addresses, and other useful elements that could assist in a forensic investigation.;TITLE:Implement Automated Configuration Monitoring Systems CONTROL:v7 5.5 DESCRIPTION:Utilize a Security Content Automation Protocol (SCAP) compliant configuration monitoring system to verify all security configuration elements, catalog approved exceptions, and alert when unauthorized changes occur.;

**CIS Safeguards 1 (v8):** 8.5

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:**  

**v8 IG2:** X

**v8 IG3:** X

**CIS Safeguards 1 (v7):** 5.5

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:**  

**v7 IG2:** X

**v7 IG3:** X

**References:**  


Section #: 4.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.3.15

**Title:** Ensure successful and unsuccessful attempts to use the chcon command are recorded

**Assessment Status:** Automated

**Description:** The operating system must generate audit records for successful/unsuccessful uses of the `chcon` command.

**Rational Statement:** The `chcon` command is used to change file security context. Without generating audit records that are specific to the security and mission needs of the organization, it would be difficult to establish, correlate, and investigate the events relating to an incident or identify those responsible for one. 

Audit records can be generated from various components within the information system (e.g., module or policy filter).

**Impact Statement:**  

**Remediation Procedure:** #### Create audit rules

Edit or create a file in the `/etc/audit/rules.d/` directory, ending in `.rules` extension, with the relevant rules to monitor successful and unsuccessful attempts to use the `chcon` command.

##### 64 Bit systems

Example:


# {
UID_MIN=$(awk '/^\s*UID_MIN/{print $2}' /etc/login.defs)
[ -n "${UID_MIN}" ] && printf "
-a always,exit -F path=/usr/bin/chcon -F perm=x -F auid>=${UID_MIN} -F auid!=unset -k perm_chng
" >> /etc/audit/rules.d/50-perm_chng.rules || printf "ERROR: Variable 'UID_MIN' is unset.\n"
}


#### Load audit rules

Merge and load the rules into active configuration:


# augenrules --load


Check if reboot is required.


# if [[ $(auditctl -s | grep "enabled") =~ "2" ]]; then printf "Reboot required to load rules\n"; fi


##### 32 Bit systems

Follow the same procedures as for 64 bit systems and ignore any entries with `b64`.

**Audit Procedure:** #### 64 Bit systems

***On disk configuration***

Run the following command to check the on disk rules:


# {
UID_MIN=$(awk '/^\s*UID_MIN/{print $2}' /etc/login.defs)
[ -n "${UID_MIN}" ] && awk "/^ *-a *always,exit/ \
&&(/ -F *auid!=unset/||/ -F *auid!=-1/||/ -F *auid!=4294967295/) \
&&/ -F *auid>=${UID_MIN}/ \
&&/ -F *perm=x/ \
&&/ -F *path=\/usr\/bin\/chcon/ \
&&(/ key= *[!-~]* *$/||/ -k *[!-~]* *$/)" /etc/audit/rules.d/*.rules \
|| printf "ERROR: Variable 'UID_MIN' is unset.\n"
}


Verify the output matches:


-a always,exit -F path=/usr/bin/chcon -F perm=x -F auid>=1000 -F auid!=unset -k perm_chng


***Running configuration***

Run the following command to check loaded rules:


# {
UID_MIN=$(awk '/^\s*UID_MIN/{print $2}' /etc/login.defs)
[ -n "${UID_MIN}" ] && auditctl -l | awk "/^ *-a *always,exit/ \
&&(/ -F *auid!=unset/||/ -F *auid!=-1/||/ -F *auid!=4294967295/) \
&&/ -F *auid>=${UID_MIN}/ \
&&/ -F *perm=x/ \
&&/ -F *path=\/usr\/bin\/chcon/ \
&&(/ key= *[!-~]* *$/||/ -k *[!-~]* *$/)" \
|| printf "ERROR: Variable 'UID_MIN' is unset.\n"
}


Verify the output matches:


-a always,exit -S all -F path=/usr/bin/chcon -F perm=x -F auid>=1000 -F auid!=-1 -F key=perm_chng


#### 32 Bit systems

Follow the same procedures as for 64 bit systems and ignore any entries with `b64`.

**Additional Information:** #### Potential reboot required

If the auditing configuration is locked (`-e 2`), then `augenrules` will not warn in any way that rules could not be loaded into the running configuration. A system reboot will be required to load the rules into the running configuration.

#### System call structure

For performance (`man 7 audit.rules`) reasons it is preferable to have all the system calls on one line. However, your configuration may have them on one line each or some other combination. This is important to understand for both the auditing and remediation sections as the examples given are optimized for performance as per the man page.

**CIS Controls:** TITLE:Collect Audit Logs CONTROL:v8 8.2 DESCRIPTION:Collect audit logs. Ensure that logging, per the enterprise’s audit log management process, has been enabled across enterprise assets.;TITLE:Activate audit logging CONTROL:v7 6.2 DESCRIPTION:Ensure that local logging has been enabled on all systems and networking devices.;

**CIS Safeguards 1 (v8):** 8.2

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:** X

**v8 IG2:** X

**v8 IG3:** X

**CIS Safeguards 1 (v7):** 6.2

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:** X

**v7 IG2:** X

**v7 IG3:** X

**References:**  


Section #: 4.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.3.16

**Title:** Ensure successful and unsuccessful attempts to use the setfacl command are recorded

**Assessment Status:** Automated

**Description:** The operating system must generate audit records for successful/unsuccessful uses of the `setfacl` command

**Rational Statement:** This utility sets Access Control Lists (ACLs) of files and directories. Without generating audit records that are specific to the security and mission needs of the organization, it would be difficult to establish, correlate, and investigate the events relating to an incident or identify those responsible for one. 

Audit records can be generated from various components within the information system (e.g., module or policy filter).

**Impact Statement:**  

**Remediation Procedure:** #### Create audit rules

Edit or create a file in the `/etc/audit/rules.d/` directory, ending in `.rules` extension, with the relevant rules to monitor successful and unsuccessful attempts to use the `setfacl` command.

##### 64 Bit systems

Example:


# {
UID_MIN=$(awk '/^\s*UID_MIN/{print $2}' /etc/login.defs)
[ -n "${UID_MIN}" ] && printf "
-a always,exit -F path=/usr/bin/setfacl -F perm=x -F auid>=${UID_MIN} -F auid!=unset -k perm_chng
" >> /etc/audit/rules.d/50-perm_chng.rules || printf "ERROR: Variable 'UID_MIN' is unset.\n"
}


#### Load audit rules

Merge and load the rules into active configuration:


# augenrules --load


Check if reboot is required.


# if [[ $(auditctl -s | grep "enabled") =~ "2" ]]; then printf "Reboot required to load rules\n"; fi


##### 32 Bit systems

Follow the same procedures as for 64 bit systems and ignore any entries with `b64`.

**Audit Procedure:** #### 64 Bit systems

***On disk configuration***

Run the following command to check the on disk rules:


# {
UID_MIN=$(awk '/^\s*UID_MIN/{print $2}' /etc/login.defs)
[ -n "${UID_MIN}" ] && awk "/^ *-a *always,exit/ \
&&(/ -F *auid!=unset/||/ -F *auid!=-1/||/ -F *auid!=4294967295/) \
&&/ -F *auid>=${UID_MIN}/ \
&&/ -F *perm=x/ \
&&/ -F *path=\/usr\/bin\/setfacl/ \
&&(/ key= *[!-~]* *$/||/ -k *[!-~]* *$/)" /etc/audit/rules.d/*.rules || printf "ERROR: Variable 'UID_MIN' is unset.\n"
}


Verify the output matches:


-a always,exit -F path=/usr/bin/setfacl -F perm=x -F auid>=1000 -F auid!=unset -k perm_chng


***Running configuration***

Run the following command to check loaded rules:


# {
UID_MIN=$(awk '/^\s*UID_MIN/{print $2}' /etc/login.defs)
[ -n "${UID_MIN}" ] && auditctl -l | awk "/^ *-a *always,exit/ \
&&(/ -F *auid!=unset/||/ -F *auid!=-1/||/ -F *auid!=4294967295/) \
&&/ -F *auid>=${UID_MIN}/ \
&&/ -F *perm=x/ \
&&/ -F *path=\/usr\/bin\/setfacl/ \
&&(/ key= *[!-~]* *$/||/ -k *[!-~]* *$/)" \
|| printf "ERROR: Variable 'UID_MIN' is unset.\n"
}


Verify the output matches:


-a always,exit -S all -F path=/usr/bin/setfacl -F perm=x -F auid>=1000 -F auid!=-1 -F key=perm_chng


#### 32 Bit systems

Follow the same procedures as for 64 bit systems and ignore any entries with `b64`.

**Additional Information:** #### Potential reboot required

If the auditing configuration is locked (`-e 2`), then `augenrules` will not warn in any way that rules could not be loaded into the running configuration. A system reboot will be required to load the rules into the running configuration.

#### System call structure

For performance (`man 7 audit.rules`) reasons it is preferable to have all the system calls on one line. However, your configuration may have them on one line each or some other combination. This is important to understand for both the auditing and remediation sections as the examples given are optimized for performance as per the man page.

**CIS Controls:** TITLE:Collect Audit Logs CONTROL:v8 8.2 DESCRIPTION:Collect audit logs. Ensure that logging, per the enterprise’s audit log management process, has been enabled across enterprise assets.;TITLE:Activate audit logging CONTROL:v7 6.2 DESCRIPTION:Ensure that local logging has been enabled on all systems and networking devices.;

**CIS Safeguards 1 (v8):** 8.2

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:** X

**v8 IG2:** X

**v8 IG3:** X

**CIS Safeguards 1 (v7):** 6.2

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:** X

**v7 IG2:** X

**v7 IG3:** X

**References:**  


Section #: 4.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.3.17

**Title:** Ensure successful and unsuccessful attempts to use the chacl command are recorded

**Assessment Status:** Automated

**Description:** The operating system must generate audit records for successful/unsuccessful uses of the `chacl` command.

`chacl` is an IRIX-compatibility command, and is maintained for those users who are familiar with its use from either XFS or IRIX.

**Rational Statement:** `chacl` changes the ACL(s) for a file or directory. Without generating audit records that are specific to the security and mission needs of the organization, it would be difficult to establish, correlate, and investigate the events relating to an incident or identify those responsible for one. 

Audit records can be generated from various components within the information system (e.g., module or policy filter).

**Impact Statement:**  

**Remediation Procedure:** #### Create audit rules

Edit or create a file in the `/etc/audit/rules.d/` directory, ending in `.rules` extension, with the relevant rules to monitor successful and unsuccessful attempts to use the `chacl` command.

##### 64 Bit systems

Example:


# {
UID_MIN=$(awk '/^\s*UID_MIN/{print $2}' /etc/login.defs)
[ -n "${UID_MIN}" ] && printf "
-a always,exit -F path=/usr/bin/chacl -F perm=x -F auid>=${UID_MIN} -F auid!=unset -k perm_chng
" >> /etc/audit/rules.d/50-perm_chng.rules || printf "ERROR: Variable 'UID_MIN' is unset.\n"
}


#### Load audit rules

Merge and load the rules into active configuration:


# augenrules --load


Check if reboot is required.


# if [[ $(auditctl -s | grep "enabled") =~ "2" ]]; then printf "Reboot required to load rules\n"; fi


##### 32 Bit systems

Follow the same procedures as for 64 bit systems and ignore any entries with `b64`.

**Audit Procedure:** #### 64 Bit systems

***On disk configuration***

Run the following command to check the on disk rules:


# {
UID_MIN=$(awk '/^\s*UID_MIN/{print $2}' /etc/login.defs)
[ -n "${UID_MIN}" ] && awk "/^ *-a *always,exit/ \
&&(/ -F *auid!=unset/||/ -F *auid!=-1/||/ -F *auid!=4294967295/) \
&&/ -F *auid>=${UID_MIN}/ \
&&/ -F *perm=x/ \
&&/ -F *path=\/usr\/bin\/chacl/ \
&&(/ key= *[!-~]* *$/||/ -k *[!-~]* *$/)" /etc/audit/rules.d/*.rules \
|| printf "ERROR: Variable 'UID_MIN' is unset.\n"
}


Verify the output matches:


-a always,exit -F path=/usr/bin/chacl -F perm=x -F auid>=1000 -F auid!=unset -k priv_cmd


***Running configuration***

Run the following command to check loaded rules:


# {
UID_MIN=$(awk '/^\s*UID_MIN/{print $2}' /etc/login.defs)
[ -n "${UID_MIN}" ] && auditctl -l | awk "/^ *-a *always,exit/ \
&&(/ -F *auid!=unset/||/ -F *auid!=-1/||/ -F *auid!=4294967295/) \
&&/ -F *auid>=${UID_MIN}/ \
&&/ -F *perm=x/ \
&&/ -F *path=\/usr\/bin\/chacl/ \
&&(/ key= *[!-~]* *$/||/ -k *[!-~]* *$/)" \
|| printf "ERROR: Variable 'UID_MIN' is unset.\n"
}


Verify the output matches:


-a always,exit -S all -F path=/usr/bin/chacl -F perm=x -F auid>=1000 -F auid!=-1 -F key=priv_cmd


#### 32 Bit systems

Follow the same procedures as for 64 bit systems and ignore any entries with `b64`.

**Additional Information:** #### Potential reboot required

If the auditing configuration is locked (`-e 2`), then `augenrules` will not warn in any way that rules could not be loaded into the running configuration. A system reboot will be required to load the rules into the running configuration.

#### System call structure

For performance (`man 7 audit.rules`) reasons it is preferable to have all the system calls on one line. However, your configuration may have them on one line each or some other combination. This is important to understand for both the auditing and remediation sections as the examples given are optimized for performance as per the man page.

**CIS Controls:** TITLE:Collect Audit Logs CONTROL:v8 8.2 DESCRIPTION:Collect audit logs. Ensure that logging, per the enterprise’s audit log management process, has been enabled across enterprise assets.;TITLE:Activate audit logging CONTROL:v7 6.2 DESCRIPTION:Ensure that local logging has been enabled on all systems and networking devices.;

**CIS Safeguards 1 (v8):** 8.2

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:** X

**v8 IG2:** X

**v8 IG3:** X

**CIS Safeguards 1 (v7):** 6.2

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:** X

**v7 IG2:** X

**v7 IG3:** X

**References:**  


Section #: 4.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.3.18

**Title:** Ensure successful and unsuccessful attempts to use the usermod command are recorded

**Assessment Status:** Automated

**Description:** The operating system must generate audit records for successful/unsuccessful uses of the `usermod` command.

**Rational Statement:** The `usermod` command modifies the system account files to reflect the changes that are specified on the command line. Without generating audit records that are specific to the security and mission needs of the organization, it would be difficult to establish, correlate, and investigate the events relating to an incident or identify those responsible for one. 

Audit records can be generated from various components within the information system (e.g., module or policy filter).

**Impact Statement:**  

**Remediation Procedure:** #### Create audit rules

Edit or create a file in the `/etc/audit/rules.d/` directory, ending in `.rules` extension, with the relevant rules to monitor successful and unsuccessful attempts to use the `usermod` command.

##### 64 Bit systems

Example:


# {
UID_MIN=$(awk '/^\s*UID_MIN/{print $2}' /etc/login.defs)
[ -n "${UID_MIN}" ] && printf "
-a always,exit -F path=/usr/sbin/usermod -F perm=x -F auid>=${UID_MIN} -F auid!=unset -k usermod
" >> /etc/audit/rules.d/50-usermod.rules || printf "ERROR: Variable 'UID_MIN' is unset.\n"
}


#### Load audit rules

Merge and load the rules into active configuration:


# augenrules --load


Check if reboot is required.


# if [[ $(auditctl -s | grep "enabled") =~ "2" ]]; then printf "Reboot required to load rules\n"; fi


##### 32 Bit systems

Follow the same procedures as for 64 bit systems and ignore any entries with `b64`.

**Audit Procedure:** #### 64 Bit systems

***On disk configuration***

Run the following command to check the on disk rules:


# {
UID_MIN=$(awk '/^\s*UID_MIN/{print $2}' /etc/login.defs)
[ -n "${UID_MIN}" ] && awk "/^ *-a *always,exit/ \
&&(/ -F *auid!=unset/||/ -F *auid!=-1/||/ -F *auid!=4294967295/) \
&&/ -F *auid>=${UID_MIN}/ \
&&/ -F *perm=x/ \
&&/ -F *path=\/usr\/sbin\/usermod/ \
&&(/ key= *[!-~]* *$/||/ -k *[!-~]* *$/)" /etc/audit/rules.d/*.rules \
|| printf "ERROR: Variable 'UID_MIN' is unset.\n"
}


Verify the output matches:


-a always,exit -F path=/usr/sbin/usermod -F perm=x -F auid>=1000 -F auid!=unset -k usermod


***Running configuration***

Run the following command to check loaded rules:


# {
UID_MIN=$(awk '/^\s*UID_MIN/{print $2}' /etc/login.defs)
[ -n "${UID_MIN}" ] && auditctl -l | awk "/^ *-a *always,exit/ \
&&(/ -F *auid!=unset/||/ -F *auid!=-1/||/ -F *auid!=4294967295/) \
&&/ -F *auid>=${UID_MIN}/ \
&&/ -F *perm=x/ \
&&/ -F *path=\/usr\/sbin\/usermod/ \
&&(/ key= *[!-~]* *$/||/ -k *[!-~]* *$/)" \
|| printf "ERROR: Variable 'UID_MIN' is unset.\n"
}


Verify the output matches:


-a always,exit -S all -F path=/usr/sbin/usermod -F perm=x -F auid>=1000 -F auid!=-1 -F key=usermod


#### 32 Bit systems

Follow the same procedures as for 64 bit systems and ignore any entries with `b64`.

**Additional Information:** #### Potential reboot required

If the auditing configuration is locked (`-e 2`), then `augenrules` will not warn in any way that rules could not be loaded into the running configuration. A system reboot will be required to load the rules into the running configuration.

#### System call structure

For performance (`man 7 audit.rules`) reasons it is preferable to have all the system calls on one line. However, your configuration may have them on one line each or some other combination. This is important to understand for both the auditing and remediation sections as the examples given are optimized for performance as per the man page.

**CIS Controls:** TITLE:Collect Audit Logs CONTROL:v8 8.2 DESCRIPTION:Collect audit logs. Ensure that logging, per the enterprise’s audit log management process, has been enabled across enterprise assets.;TITLE:Activate audit logging CONTROL:v7 6.2 DESCRIPTION:Ensure that local logging has been enabled on all systems and networking devices.;

**CIS Safeguards 1 (v8):** 8.2

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:** X

**v8 IG2:** X

**v8 IG3:** X

**CIS Safeguards 1 (v7):** 6.2

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:** X

**v7 IG2:** X

**v7 IG3:** X

**References:**  


Section #: 4.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.3.19

**Title:** Ensure kernel module loading unloading and modification is collected

**Assessment Status:** Automated

**Description:** Monitor the loading and unloading of kernel modules. All the loading / listing / dependency checking of modules is done by `kmod` via symbolic links.

The following system calls control loading and unloading of modules:
- `init_module` - load a module
- `finit_module` - load a module (used when the overhead of using cryptographically signed modules to determine the authenticity of a module can be avoided)
- `delete_module` - delete a module
- `create_module` - create a loadable module entry
- `query_module` - query the kernel for various bits pertaining to modules

Any execution of the loading and unloading module programs and system calls will trigger an audit record with an identifier of `modules`.

**Rational Statement:** Monitoring the use of all the various ways to manipulate kernel modules could provide system administrators with evidence that an unauthorized change was made to a kernel module, possibly compromising the security of the system.

**Impact Statement:**  

**Remediation Procedure:** #### Create audit rules

Edit or create a file in the `/etc/audit/rules.d/` directory, ending in `.rules` extension, with the relevant rules to monitor kernel module modification.

##### 64 Bit systems

Example:


# {
UID_MIN=$(awk '/^\s*UID_MIN/{print $2}' /etc/login.defs)
[ -n "${UID_MIN}" ] && printf "
-a always,exit -F arch=b64 -S init_module,finit_module,delete_module,create_module,query_module -F auid>=${UID_MIN} -F auid!=unset -k kernel_modules
-a always,exit -F path=/usr/bin/kmod -F perm=x -F auid>=${UID_MIN} -F auid!=unset -k kernel_modules
" >> /etc/audit/rules.d/50-kernel_modules.rules || printf "ERROR: Variable 'UID_MIN' is unset.\n"
}

#### Load audit rules

Merge and load the rules into active configuration:


# augenrules --load


Check if reboot is required.


# if [[ $(auditctl -s | grep "enabled") =~ "2" ]]; then printf "Reboot required to load rules\n"; fi


**Audit Procedure:** #### 64 Bit systems

***On disk configuration***

Run the following commands to check the on disk rules:


# {
awk '/^ *-a *always,exit/ \
&&/ -F *arch=b[2346]{2}/ \
&&(/ -F auid!=unset/||/ -F auid!=-1/||/ -F auid!=4294967295/) \
&&/ -S/ \
&&(/init_module/ \
||/finit_module/ \
||/delete_module/ \
||/create_module/ \
||/query_module/) \
&&(/ key= *[!-~]* *$/||/ -k *[!-~]* *$/)' /etc/audit/rules.d/*.rules

UID_MIN=$(awk '/^\s*UID_MIN/{print $2}' /etc/login.defs)
[ -n "${UID_MIN}" ] && awk "/^ *-a *always,exit/ \
&&(/ -F *auid!=unset/||/ -F *auid!=-1/||/ -F *auid!=4294967295/) \
&&/ -F *auid>=${UID_MIN}/ \
&&/ -F *perm=x/ \
&&/ -F *path=\/usr\/bin\/kmod/ \
&&(/ key= *[!-~]* *$/||/ -k *[!-~]* *$/)" /etc/audit/rules.d/*.rules \
|| printf "ERROR: Variable 'UID_MIN' is unset.\n"
}


Verify the output matches:


-a always,exit -F arch=b64 -S init_module,finit_module,delete_module,create_module,query_module -F auid>=1000 -F auid!=unset -k kernel_modules
-a always,exit -F path=/usr/bin/kmod -F perm=x -F auid>=1000 -F auid!=unset -k kernel_modules


***Running configuration***

Run the following command to check loaded rules:


# {
auditctl -l | awk '/^ *-a *always,exit/ \
&&/ -F *arch=b[2346]{2}/ \
&&(/ -F auid!=unset/||/ -F auid!=-1/||/ -F auid!=4294967295/) \
&&/ -S/ \
&&(/init_module/ \
||/finit_module/ \
||/delete_module/ \
||/create_module/ \
||/query_module/) \
&&(/ key= *[!-~]* *$/||/ -k *[!-~]* *$/)'

UID_MIN=$(awk '/^\s*UID_MIN/{print $2}' /etc/login.defs)
[ -n "${UID_MIN}" ] && auditctl -l | awk "/^ *-a *always,exit/ \
&&(/ -F *auid!=unset/||/ -F *auid!=-1/||/ -F *auid!=4294967295/) \
&&/ -F *auid>=${UID_MIN}/ \
&&/ -F *perm=x/ \
&&/ -F *path=\/usr\/bin\/kmod/ \
&&(/ key= *[!-~]* *$/||/ -k *[!-~]* *$/)" \
|| printf "ERROR: Variable 'UID_MIN' is unset.\n"
}


Verify the output includes:


-a always,exit -F arch=b64 -S create_module,init_module,delete_module,query_module,finit_module -F auid>=1000 -F auid!=-1 -F key=kernel_modules
-a always,exit -S all -F path=/usr/bin/kmod -F perm=x -F auid>=1000 -F auid!=-1 -F key=kernel_modules


#### Symlink audit

Audit if the symlinks that `kmod` accepts is indeed pointing at it:


# S_LINKS=$(ls -l /usr/sbin/lsmod /usr/sbin/rmmod /usr/sbin/insmod /usr/sbin/modinfo /usr/sbin/modprobe /usr/sbin/depmod | grep -v " -> ../bin/kmod" || true) \
&& if [[ "${S_LINKS}" != "" ]]; then printf "Issue with symlinks: ${S_LINKS}\n"; else printf "OK\n"; fi


Verify the output states `OK`. If there is a symlink pointing to a different location it should be investigated.

**Additional Information:** #### Potential reboot required

If the auditing configuration is locked (`-e 2`), then `augenrules` will not warn in any way that rules could not be loaded into the running configuration. A system reboot will be required to load the rules into the running configuration.

#### System call structure

For performance (`man 7 audit.rules`) reasons it is preferable to have all the system calls on one line. However, your configuration may have them on one line each or some other combination. This is important to understand for both the auditing and remediation sections as the examples given are optimized for performance as per the man page.

**CIS Controls:** TITLE:Collect Detailed Audit Logs CONTROL:v8 8.5 DESCRIPTION:Configure detailed audit logging for enterprise assets containing sensitive data. Include event source, date, username, timestamp, source addresses, destination addresses, and other useful elements that could assist in a forensic investigation.;TITLE:Activate audit logging CONTROL:v7 6.2 DESCRIPTION:Ensure that local logging has been enabled on all systems and networking devices.;

**CIS Safeguards 1 (v8):** 8.5

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:**  

**v8 IG2:** X

**v8 IG3:** X

**CIS Safeguards 1 (v7):** 6.2

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:** X

**v7 IG2:** X

**v7 IG3:** X

**References:**  


Section #: 4.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.3.20

**Title:** Ensure the audit configuration is immutable

**Assessment Status:** Automated

**Description:** Set system audit so that audit rules cannot be modified with `auditctl` . Setting the flag "-e 2" forces audit to be put in immutable mode. Audit changes can only be made on system reboot.

**Note:** This setting will require the system to be rebooted to update the active `auditd` configuration settings.

**Rational Statement:** In immutable mode, unauthorized users cannot execute changes to the audit system to potentially hide malicious activity and then put the audit rules back. Users would most likely notice a system reboot and that could alert administrators of an attempt to make unauthorized audit changes.

**Impact Statement:**  

**Remediation Procedure:** Edit or create the file `/etc/audit/rules.d/99-finalize.rules` and add the line `-e 2` at the end of the file:

_Example:_ 


# printf -- "-e 2
" >> /etc/audit/rules.d/99-finalize.rules


#### Load audit rules

Merge and load the rules into active configuration:


# augenrules --load


Check if reboot is required.


# if [[ $(auditctl -s | grep "enabled") =~ "2" ]]; then printf "Reboot required to load rules\n"; fi


**Audit Procedure:** Run the following command and verify output matches:


# grep -Ph -- '^\h*-e\h+2\b' /etc/audit/rules.d/*.rules | tail -1

-e 2


**Additional Information:** **NIST SP 800-53 Rev. 5:**
- AC-3
- AU-3
- AU-3(1)
- MP-2

**CIS Controls:** TITLE:Configure Data Access Control Lists CONTROL:v8 3.3 DESCRIPTION:Configure data access control lists based on a user’s need to know. Apply data access control lists, also known as access permissions, to local and remote file systems, databases, and applications.;TITLE:Collect Detailed Audit Logs CONTROL:v8 8.5 DESCRIPTION:Configure detailed audit logging for enterprise assets containing sensitive data. Include event source, date, username, timestamp, source addresses, destination addresses, and other useful elements that could assist in a forensic investigation.;TITLE:Activate audit logging CONTROL:v7 6.2 DESCRIPTION:Ensure that local logging has been enabled on all systems and networking devices.;TITLE:Enable Detailed Logging CONTROL:v7 6.3 DESCRIPTION:Enable system logging to include detailed information such as an event source, date, user, timestamp, source addresses, destination addresses, and other useful elements.;

**CIS Safeguards 1 (v8):** 3.3

**CIS Safeguards 2 (v8):** 8.5

**CIS Safeguards 3 (v8):**  

**v8 IG1:** X

**v8 IG2:** X

**v8 IG3:** X

**CIS Safeguards 1 (v7):** 6.2

**CIS Safeguards 2 (v7):** 6.3

**CIS Safeguards 3 (v7):**  

**v7 IG1:** X

**v7 IG2:** X

**v7 IG3:** X

**References:**  


Section #: 4.1.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.3.21

**Title:** Ensure the running and on disk configuration is the same

**Assessment Status:** Manual

**Description:** The Audit system have both on disk and running configuration. It is possible for these configuration settings to differ.

**Note:** Due to the limitations of `augenrules` and `auditctl`, it is not absolutely guaranteed that loading the rule sets via `augenrules --load` will result in all rules being loaded or even that the user will be informed if there was a problem loading the rules.

**Rational Statement:** Configuration differences between what is currently running and what is on disk could cause unexpected problems or may give a false impression of compliance requirements.

**Impact Statement:**  

**Remediation Procedure:** If the rules are not aligned across all three () areas, run the following command to merge and load all rules:



# augenrules --load


Check if reboot is required.


if [[ $(auditctl -s | grep "enabled") =~ "2" ]]; then echo "Reboot required to load rules"; fi


**Audit Procedure:** #### Merged rule sets

Ensure that all rules in `/etc/audit/rules.d` have been merged into `/etc/audit/audit.rules`:


# augenrules --check

/usr/sbin/augenrules: No change


Should there be any drift, run `augenrules --load` to merge and load all rules.

**Additional Information:** #### Potential reboot required

If the auditing configuration is locked (`-e 2`), then `augenrules` will not warn in any way that rules could not be loaded into the running configuration. A system reboot will be required to load the rules into the running configuration.

**CIS Controls:** TITLE:Collect Detailed Audit Logs CONTROL:v8 8.5 DESCRIPTION:Configure detailed audit logging for enterprise assets containing sensitive data. Include event source, date, username, timestamp, source addresses, destination addresses, and other useful elements that could assist in a forensic investigation.;TITLE:Enable Detailed Logging CONTROL:v7 6.3 DESCRIPTION:Enable system logging to include detailed information such as an event source, date, user, timestamp, source addresses, destination addresses, and other useful elements.;

**CIS Safeguards 1 (v8):** 8.5

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:**  

**v8 IG2:** X

**v8 IG3:** X

**CIS Safeguards 1 (v7):** 6.3

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:**  

**v7 IG2:** X

**v7 IG3:** X

**References:**  


Section #: 4.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:**  

**Title:** Configure auditd file access

**Assessment Status:**  

**Description:** Without the capability to restrict which roles and individuals can select which events are audited, unauthorized personnel may be able to prevent the auditing of critical events.

**Rational Statement:**  

**Impact Statement:**  

**Remediation Procedure:**  

**Audit Procedure:**  

**Additional Information:**  

**CIS Controls:**  

**CIS Safeguards 1 (v8):**  

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:**  

**v8 IG2:**  

**v8 IG3:**  

**CIS Safeguards 1 (v7):**  

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:**  

**v7 IG2:**  

**v7 IG3:**  

**References:**  


Section #: 4.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.4.1

**Title:** Ensure audit log files are mode 0640 or less permissive

**Assessment Status:** Automated

**Description:** Audit log files contain information about the system and system activity.

**Rational Statement:** Access to audit records can reveal system and configuration data to attackers, potentially compromising its confidentiality.

**Impact Statement:**  

**Remediation Procedure:** Run the following command to remove more permissive mode than `0640` from audit log files:


# [ -f /etc/audit/auditd.conf ] && find "$(dirname $(awk -F "=" '/^\s*log_file/ {print $2}' /etc/audit/auditd.conf | xargs))" -type f \( ! -perm 600 -a ! -perm 0400 -a ! -perm 0200 -a ! -perm 0000 -a ! -perm 0640 -a ! -perm 0440 -a ! -perm 0040 \) -exec chmod u-x,g-wx,o-rwx {} +


**Audit Procedure:** Run the following command to verify audit log files have mode `0640` or less permissive:


# [ -f /etc/audit/auditd.conf ] && find "$(dirname $(awk -F "=" '/^\s*log_file/ {print $2}' /etc/audit/auditd.conf | xargs))" -type f \( ! -perm 600 -a ! -perm 0400 -a ! -perm 0200 -a ! -perm 0000 -a ! -perm 0640 -a ! -perm 0440 -a ! -perm 0040 \) -exec stat -Lc "%n %#a" {} +


Nothing should be returned

**Additional Information:**  

**CIS Controls:** TITLE:Configure Data Access Control Lists CONTROL:v8 3.3 DESCRIPTION:Configure data access control lists based on a user’s need to know. Apply data access control lists, also known as access permissions, to local and remote file systems, databases, and applications.;TITLE:Protect Information through Access Control Lists CONTROL:v7 14.6 DESCRIPTION:Protect all information stored on systems with file system, network share, claims, application, or database specific access control lists. These controls will enforce the principle that only authorized individuals should have access to the information based on their need to access the information as a part of their responsibilities.;

**CIS Safeguards 1 (v8):** 3.3

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:** X

**v8 IG2:** X

**v8 IG3:** X

**CIS Safeguards 1 (v7):** 14.6

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:** X

**v7 IG2:** X

**v7 IG3:** X

**References:**  


Section #: 4.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.4.2

**Title:** Ensure only authorized users own audit log files

**Assessment Status:** Automated

**Description:** Audit log files contain information about the system and system activity.

**Rational Statement:** Access to audit records can reveal system and configuration data to attackers, potentially compromising its confidentiality.

**Impact Statement:**  

**Remediation Procedure:** Run the following command to configure the audit log files to be owned by the `root` user: 


# [ -f /etc/audit/auditd.conf ] && find "$(dirname $(awk -F "=" '/^\s*log_file/ {print $2}' /etc/audit/auditd.conf | xargs))" -type f ! -user root -exec chown root {} +


**Audit Procedure:** Run the following command to verify audit log files are owned by the `root` user: 


# [ -f /etc/audit/auditd.conf ] && find "$(dirname $(awk -F "=" '/^\s*log_file/ {print $2}' /etc/audit/auditd.conf | xargs))" -type f ! -user root -exec stat -Lc "%n %U" {} +


Nothing should be returned

**Additional Information:**  

**CIS Controls:** TITLE:Configure Data Access Control Lists CONTROL:v8 3.3 DESCRIPTION:Configure data access control lists based on a user’s need to know. Apply data access control lists, also known as access permissions, to local and remote file systems, databases, and applications.;TITLE:Protect Information through Access Control Lists CONTROL:v7 14.6 DESCRIPTION:Protect all information stored on systems with file system, network share, claims, application, or database specific access control lists. These controls will enforce the principle that only authorized individuals should have access to the information based on their need to access the information as a part of their responsibilities.;

**CIS Safeguards 1 (v8):** 3.3

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:** X

**v8 IG2:** X

**v8 IG3:** X

**CIS Safeguards 1 (v7):** 14.6

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:** X

**v7 IG2:** X

**v7 IG3:** X

**References:**  


Section #: 4.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.4.3

**Title:** Ensure only authorized groups are assigned ownership of audit log files

**Assessment Status:** Automated

**Description:** Audit log files contain information about the system and system activity.

**Rational Statement:** Access to audit records can reveal system and configuration data to attackers, potentially compromising its confidentiality.

**Impact Statement:**  

**Remediation Procedure:** Run the following command to configure the audit log files to be owned by `adm` group: 


# find $(dirname $(awk -F"=" '/^\s*log_file/ {print $2}' /etc/audit/auditd.conf | xargs)) -type f \( ! -group adm -a ! -group root \) -exec chgrp adm {} +


Run the following command to configure the audit log files to be owned by the `adm` group:


# chgrp adm /var/log/audit/


Run the following command to set the `log_group` parameter in the audit configuration file to `log_group = adm`:


# sed -ri 's/^\s*#?\s*log_group\s*=\s*\S+(\s*#.*)?.*$/log_group = adm\1/' /etc/audit/auditd.conf


Run the following command to restart the audit daemon to reload the configuration file: 


# systemctl restart auditd


**Audit Procedure:** Run the following command to verify `log_group` parameter is set to either `adm` or `root` in `/etc/audit/auditd.conf`: 


# grep -Piw -- '^\h*log_group\h*=\h*(adm|root)\b' /etc/audit/auditd.conf


Verify the output is:


log_group = adm
-OR-
log_group = root


Using the path of the directory containing the audit logs, determine if the audit log files are owned by the "root" or "adm" group by using the following command: 


# stat -c "%n %G" "$(dirname $(awk -F"=" '/^\s*log_file\s*=\s*/ {print $2}' /etc/audit/auditd.conf | xargs))"/* | grep -Pv '^\h*\H+\h+(adm|root)\b'


Nothing should be returned

**Additional Information:**  

**CIS Controls:** TITLE:Configure Data Access Control Lists CONTROL:v8 3.3 DESCRIPTION:Configure data access control lists based on a user’s need to know. Apply data access control lists, also known as access permissions, to local and remote file systems, databases, and applications.;TITLE:Protect Information through Access Control Lists CONTROL:v7 14.6 DESCRIPTION:Protect all information stored on systems with file system, network share, claims, application, or database specific access control lists. These controls will enforce the principle that only authorized individuals should have access to the information based on their need to access the information as a part of their responsibilities.;

**CIS Safeguards 1 (v8):** 3.3

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:** X

**v8 IG2:** X

**v8 IG3:** X

**CIS Safeguards 1 (v7):** 14.6

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:** X

**v7 IG2:** X

**v7 IG3:** X

**References:**  


Section #: 4.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.4.4

**Title:** Ensure the audit log directory is 0750 or more restrictive

**Assessment Status:** Automated

**Description:** The audit log directory contains audit log files.

**Rational Statement:** Audit information includes all information including: audit records, audit settings and audit reports. This information is needed to successfully audit system activity. This information must be protected from unauthorized modification or deletion. If this information were to be compromised, forensic analysis and discovery of the true source of potentially malicious system activity is impossible to achieve.

**Impact Statement:**  

**Remediation Procedure:** Run the following command to configure the audit log directory to have a mode of "0750" or less permissive: 



# chmod g-w,o-rwx "$(dirname $(awk -F"=" '/^\s*log_file/ {print $2}' /etc/audit/auditd.conf))"


**Audit Procedure:** Run the following command to verify that the audit log directory has a mode of 0750 or less permissive:


# stat -Lc "%n %a" "$(dirname $( awk -F"=" '/^\s*log_file/ {print $2}' /etc/audit/auditd.conf))" | grep -Pv -- '^\h*\H+\h+([0,5,7][0,5]0)'


Nothing should be returned

**Additional Information:**  

**CIS Controls:** TITLE:Configure Data Access Control Lists CONTROL:v8 3.3 DESCRIPTION:Configure data access control lists based on a user’s need to know. Apply data access control lists, also known as access permissions, to local and remote file systems, databases, and applications.;TITLE:Protect Information through Access Control Lists CONTROL:v7 14.6 DESCRIPTION:Protect all information stored on systems with file system, network share, claims, application, or database specific access control lists. These controls will enforce the principle that only authorized individuals should have access to the information based on their need to access the information as a part of their responsibilities.;

**CIS Safeguards 1 (v8):** 3.3

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:** X

**v8 IG2:** X

**v8 IG3:** X

**CIS Safeguards 1 (v7):** 14.6

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:** X

**v7 IG2:** X

**v7 IG3:** X

**References:**  


Section #: 4.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.4.5

**Title:** Ensure audit configuration files are 640 or more restrictive

**Assessment Status:** Automated

**Description:** Audit configuration files control auditd and what events are audited.

**Rational Statement:** Access to the audit configuration files could allow unauthorized personnel to prevent the auditing of critical events. 

Misconfigured audit configuration files may prevent the auditing of critical events or impact the system's performance by overwhelming the audit log. Misconfiguration of the audit configuration files may also make it more difficult to establish and investigate events relating to an incident.

**Impact Statement:**  

**Remediation Procedure:** Run the following command to remove more permissive mode than 0640 from the audit configuration files:


# find /etc/audit/ -type f \( -name '*.conf' -o -name '*.rules' \) -exec chmod u-x,g-wx,o-rwx {} +


**Audit Procedure:** Run the following command to verify that the audit configuration files have mode 640 or more restrictive and are owned by the root user and root group: 


# find /etc/audit/ -type f \( -name '*.conf' -o -name '*.rules' \) -exec stat -Lc "%n %a" {} + | grep -Pv -- '^\h*\H+\h*([0,2,4,6][0,4]0)\h*$'


Nothing should be returned

**Additional Information:**  

**CIS Controls:** TITLE:Configure Data Access Control Lists CONTROL:v8 3.3 DESCRIPTION:Configure data access control lists based on a user’s need to know. Apply data access control lists, also known as access permissions, to local and remote file systems, databases, and applications.;TITLE:Protect Information through Access Control Lists CONTROL:v7 14.6 DESCRIPTION:Protect all information stored on systems with file system, network share, claims, application, or database specific access control lists. These controls will enforce the principle that only authorized individuals should have access to the information based on their need to access the information as a part of their responsibilities.;

**CIS Safeguards 1 (v8):** 3.3

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:** X

**v8 IG2:** X

**v8 IG3:** X

**CIS Safeguards 1 (v7):** 14.6

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:** X

**v7 IG2:** X

**v7 IG3:** X

**References:**  


Section #: 4.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.4.6

**Title:** Ensure audit configuration files are owned by root

**Assessment Status:** Automated

**Description:** Audit configuration files control auditd and what events are audited.

**Rational Statement:** Access to the audit configuration files could allow unauthorized personnel to prevent the auditing of critical events. 

Misconfigured audit configuration files may prevent the auditing of critical events or impact the system's performance by overwhelming the audit log. Misconfiguration of the audit configuration files may also make it more difficult to establish and investigate events relating to an incident.

**Impact Statement:**  

**Remediation Procedure:** Run the following command to change ownership to `root` user:


# find /etc/audit/ -type f \( -name '*.conf' -o -name '*.rules' \) ! -user root -exec chown root {} +


**Audit Procedure:** Run the following command to verify that the audit configuration files have mode 640 or more restrictive and are owned by the root user and root group: 


# find /etc/audit/ -type f \( -name '*.conf' -o -name '*.rules' \) ! -user root


Nothing should be returned

**Additional Information:**  

**CIS Controls:** TITLE:Configure Data Access Control Lists CONTROL:v8 3.3 DESCRIPTION:Configure data access control lists based on a user’s need to know. Apply data access control lists, also known as access permissions, to local and remote file systems, databases, and applications.;TITLE:Protect Information through Access Control Lists CONTROL:v7 14.6 DESCRIPTION:Protect all information stored on systems with file system, network share, claims, application, or database specific access control lists. These controls will enforce the principle that only authorized individuals should have access to the information based on their need to access the information as a part of their responsibilities.;

**CIS Safeguards 1 (v8):** 3.3

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:** X

**v8 IG2:** X

**v8 IG3:** X

**CIS Safeguards 1 (v7):** 14.6

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:** X

**v7 IG2:** X

**v7 IG3:** X

**References:**  


Section #: 4.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.4.7

**Title:** Ensure audit configuration files belong to group root

**Assessment Status:** Automated

**Description:** Audit configuration files control auditd and what events are audited.

**Rational Statement:** Access to the audit configuration files could allow unauthorized personnel to prevent the auditing of critical events. 

Misconfigured audit configuration files may prevent the auditing of critical events or impact the system's performance by overwhelming the audit log. Misconfiguration of the audit configuration files may also make it more difficult to establish and investigate events relating to an incident.

**Impact Statement:**  

**Remediation Procedure:** Run the following command to change group to `root`:


# find /etc/audit/ -type f \( -name '*.conf' -o -name '*.rules' \) ! -group root -exec chgrp root {} +


**Audit Procedure:** Run the following command to verify that the audit configuration files have mode 640 or more restrictive and are owned by the root user and root group: 


# find /etc/audit/ -type f \( -name '*.conf' -o -name '*.rules' \) ! -group root


Nothing should be returned

**Additional Information:**  

**CIS Controls:** TITLE:Configure Data Access Control Lists CONTROL:v8 3.3 DESCRIPTION:Configure data access control lists based on a user’s need to know. Apply data access control lists, also known as access permissions, to local and remote file systems, databases, and applications.;TITLE:Protect Information through Access Control Lists CONTROL:v7 14.6 DESCRIPTION:Protect all information stored on systems with file system, network share, claims, application, or database specific access control lists. These controls will enforce the principle that only authorized individuals should have access to the information based on their need to access the information as a part of their responsibilities.;

**CIS Safeguards 1 (v8):** 3.3

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:** X

**v8 IG2:** X

**v8 IG3:** X

**CIS Safeguards 1 (v7):** 14.6

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:** X

**v7 IG2:** X

**v7 IG3:** X

**References:**  


Section #: 4.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.4.8

**Title:** Ensure audit tools are 755 or more restrictive

**Assessment Status:** Automated

**Description:** Audit tools include, but are not limited to, vendor-provided and open source audit tools needed to successfully view and manipulate audit information system activity and records. Audit tools include custom queries and report generators.

**Rational Statement:** Protecting audit information includes identifying and protecting the tools used to view and manipulate log data. Protecting audit tools is necessary to prevent unauthorized operation on audit information.

**Impact Statement:**  

**Remediation Procedure:** Run the following command to remove more permissive mode from the audit tools: 


# chmod go-w /sbin/auditctl /sbin/aureport /sbin/ausearch /sbin/autrace /sbin/auditd /sbin/augenrules


**Audit Procedure:** Run the following command to verify the audit tools have mode `755` or more restrictive, are owned by the root user and group root: 


# stat -c "%n %a" /sbin/auditctl /sbin/aureport /sbin/ausearch /sbin/autrace /sbin/auditd /sbin/augenrules | grep -Pv -- '^\h*\H+\h+([0-7][0,1,4,5][0,1,4,5])\h*$'


Nothing should be returned

**Additional Information:**  

**CIS Controls:** TITLE:Configure Data Access Control Lists CONTROL:v8 3.3 DESCRIPTION:Configure data access control lists based on a user’s need to know. Apply data access control lists, also known as access permissions, to local and remote file systems, databases, and applications.;TITLE:Protect Information through Access Control Lists CONTROL:v7 14.6 DESCRIPTION:Protect all information stored on systems with file system, network share, claims, application, or database specific access control lists. These controls will enforce the principle that only authorized individuals should have access to the information based on their need to access the information as a part of their responsibilities.;

**CIS Safeguards 1 (v8):** 3.3

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:** X

**v8 IG2:** X

**v8 IG3:** X

**CIS Safeguards 1 (v7):** 14.6

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:** X

**v7 IG2:** X

**v7 IG3:** X

**References:**  


Section #: 4.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.4.9

**Title:** Ensure audit tools are owned by root

**Assessment Status:** Automated

**Description:** Audit tools include, but are not limited to, vendor-provided and open source audit tools needed to successfully view and manipulate audit information system activity and records. Audit tools include custom queries and report generators.

**Rational Statement:** Protecting audit information includes identifying and protecting the tools used to view and manipulate log data. Protecting audit tools is necessary to prevent unauthorized operation on audit information.

**Impact Statement:**  

**Remediation Procedure:** Run the following command to change the owner of the audit tools to the `root` user:


# chown root /sbin/auditctl /sbin/aureport /sbin/ausearch /sbin/autrace /sbin/auditd /sbin/augenrules


**Audit Procedure:** Run the following command to verify the audit tools have mode `755` or more restrictive, are owned by the root user and group root: 


# stat -c "%n %U" /sbin/auditctl /sbin/aureport /sbin/ausearch /sbin/autrace /sbin/auditd /sbin/augenrules | grep -Pv -- '^\h*\H+\h+root\h*$'


Nothing should be returned

**Additional Information:**  

**CIS Controls:** TITLE:Configure Data Access Control Lists CONTROL:v8 3.3 DESCRIPTION:Configure data access control lists based on a user’s need to know. Apply data access control lists, also known as access permissions, to local and remote file systems, databases, and applications.;TITLE:Protect Information through Access Control Lists CONTROL:v7 14.6 DESCRIPTION:Protect all information stored on systems with file system, network share, claims, application, or database specific access control lists. These controls will enforce the principle that only authorized individuals should have access to the information based on their need to access the information as a part of their responsibilities.;

**CIS Safeguards 1 (v8):** 3.3

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:** X

**v8 IG2:** X

**v8 IG3:** X

**CIS Safeguards 1 (v7):** 14.6

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:** X

**v7 IG2:** X

**v7 IG3:** X

**References:**  


Section #: 4.1.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 4.1.4.10

**Title:** Ensure audit tools belong to group root

**Assessment Status:** Automated

**Description:** Audit tools include, but are not limited to, vendor-provided and open source audit tools needed to successfully view and manipulate audit information system activity and records. Audit tools include custom queries and report generators.

**Rational Statement:** Protecting audit information includes identifying and protecting the tools used to view and manipulate log data. Protecting audit tools is necessary to prevent unauthorized operation on audit information.

**Impact Statement:**  

**Remediation Procedure:** Run the following command to remove more permissive mode from the audit tools: 


# chmod go-w /sbin/auditctl /sbin/aureport /sbin/ausearch /sbin/autrace /sbin/auditd /sbin/augenrules


Run the following command to change owner and group of the audit tools to `root` user and group:


# chown root:root /sbin/auditctl /sbin/aureport /sbin/ausearch /sbin/autrace /sbin/auditd /sbin/augenrules


**Audit Procedure:** Run the following command to verify the audit tools have mode `755` or more restrictive, are owned by the root user and group root: 


# stat -c "%n %a %U %G" /sbin/auditctl /sbin/aureport /sbin/ausearch /sbin/autrace /sbin/auditd /sbin/augenrules | grep -Pv -- '^\h*\H+\h+([0-7][0,1,4,5][0,1,4,5])\h+root\h+root\h*$'


Nothing should be returned

**Additional Information:**  

**CIS Controls:** TITLE:Configure Data Access Control Lists CONTROL:v8 3.3 DESCRIPTION:Configure data access control lists based on a user’s need to know. Apply data access control lists, also known as access permissions, to local and remote file systems, databases, and applications.;TITLE:Protect Information through Access Control Lists CONTROL:v7 14.6 DESCRIPTION:Protect all information stored on systems with file system, network share, claims, application, or database specific access control lists. These controls will enforce the principle that only authorized individuals should have access to the information based on their need to access the information as a part of their responsibilities.;

**CIS Safeguards 1 (v8):** 3.3

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:** X

**v8 IG2:** X

**v8 IG3:** X

**CIS Safeguards 1 (v7):** 14.6

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:** X

**v7 IG2:** X

**v7 IG3:** X

**References:**  


Section #: 4.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:**  

**Title:** Configure Logging

**Assessment Status:**  

**Description:** Logging services should be configured to prevent information leaks and to aggregate logs on a remote server so that they can be reviewed in the event of a system compromise. A centralized log server provides a single point of entry for further analysis, monitoring and filtering.

### Security principals for logging
* Ensure transport layer security is implemented between the client and the log server.
* Ensure that logs are rotated as per the environment requirements.
* Ensure all locally generated logs have the appropriate permissions.
* Ensure all security logs are sent to a remote log server.
* Ensure the required events are logged.

### What is covered
This section will cover the minimum best practices for the usage of **either** `rsyslog` **or** `journald`. The recommendations are written such that each is wholly independent of each other and **only one is implemented**.
* If your organization makes use of an enterprise wide logging system completely outside of `rsyslog` or `journald`, then the following recommendations does not directly apply. However, the principals of the recommendations should be followed regardless of what solution is implemented. If the enterprise solution incorporates either of these tools, careful consideration should be given to the following recommendations to determine exactly what applies.
* Should your organization make use of both `rsyslog` and `journald`, take care how the recommendations may or may not apply to you.

### What is not covered
* Enterprise logging systems not utilizing `rsyslog` or `journald`.
As logging is very situational and dependant on the local environment, not everything can be covered here.
* Transport layer security should be applied to all remote logging functionality. Both `rsyslog` and `journald` supports secure transport and should be configured as such.
* The log server. There are a multitude of reasons for a centralized log server (and keeping a short period logging on the local system), but the log server is out of scope for these recommendations.

**Rational Statement:**  

**Impact Statement:**  

**Remediation Procedure:**  

**Audit Procedure:**  

**Additional Information:**  

**CIS Controls:**  

**CIS Safeguards 1 (v8):**  

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:**  

**v8 IG2:**  

**v8 IG3:**  

**CIS Safeguards 1 (v7):**  

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:**  

**v7 IG2:**  

**v7 IG3:**  

**References:**  


Section #: 4.2.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:**  

**Title:** Configure journald

**Assessment Status:**  

**Description:** Included in the systemd suite is a journaling service called `systemd-journald.service` for the collection and storage of logging data. It creates and maintains structured, indexed journals based on logging information that is received from a variety of sources such as: 
- Classic RFC3164 BSD syslog via the /dev/log socket
- STDOUT/STDERR of programs via StandardOutput=journal + StandardError=journal in service files (both of which are default settings)
- Kernel log messages via the /dev/kmsg device node
- Audit records via the kernel’s audit subsystem
- Structured log messages via journald’s native protocol

Any changes made to the `systemd-journald` configuration will require a re-start of `systemd-journald`

**Rational Statement:**  

**Impact Statement:**  

**Remediation Procedure:**  

**Audit Procedure:**  

**Additional Information:**  

**CIS Controls:**  

**CIS Safeguards 1 (v8):**  

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:**  

**v8 IG2:**  

**v8 IG3:**  

**CIS Safeguards 1 (v7):**  

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:**  

**v7 IG2:**  

**v7 IG3:**  

**References:**  


Section #: 4.2.1.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:**  

**Title:** Ensure journald is configured to send logs to a remote log host

**Assessment Status:**  

**Description:**  

**Rational Statement:**  

**Impact Statement:**  

**Remediation Procedure:**  

**Audit Procedure:**  

**Additional Information:**  

**CIS Controls:**  

**CIS Safeguards 1 (v8):**  

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:**  

**v8 IG2:**  

**v8 IG3:**  

**CIS Safeguards 1 (v7):**  

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:**  

**v7 IG2:**  

**v7 IG3:**  

**References:**  


Section #: 4.2.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:**  

**Title:** Configure rsyslog

**Assessment Status:**  

**Description:** The `rsyslog` software package may be used instead of the default `journald` logging mechanism.

**Note:** This section only applies if `rsyslog` is the chosen method for client side logging. Do not apply this section if `journald` is used.

**Rational Statement:**  

**Impact Statement:**  

**Remediation Procedure:**  

**Audit Procedure:**  

**Additional Information:**  

**CIS Controls:**  

**CIS Safeguards 1 (v8):**  

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:**  

**v8 IG2:**  

**v8 IG3:**  

**CIS Safeguards 1 (v7):**  

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:**  

**v7 IG2:**  

**v7 IG3:**  

**References:**  


Section #: 5
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:**  

**Title:** Access, Authentication and Authorization

**Assessment Status:**  

**Description:**  

**Rational Statement:**  

**Impact Statement:**  

**Remediation Procedure:**  

**Audit Procedure:**  

**Additional Information:**  

**CIS Controls:**  

**CIS Safeguards 1 (v8):**  

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:**  

**v8 IG2:**  

**v8 IG3:**  

**CIS Safeguards 1 (v7):**  

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:**  

**v7 IG2:**  

**v7 IG3:**  

**References:**  


Section #: 5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:**  

**Title:** Configure time-based job schedulers

**Assessment Status:**  

**Description:** `cron` is a time-based job scheduler used to schedule jobs, commands or shell scripts, to run periodically at fixed times, dates, or intervals.

`at` provides the ability to execute a command or shell script at a specified date and hour, or after a given interval of time.

_Notes:_
- _Other methods exist for scheduling jobs, such as `systemd timers`. If another method is used, it should be secured in accordance with local site policy_
- _`systemd timers` are systemd unit files whose name ends in `.timer` that control `.service` files or events_
 - _Timers can be used as an alternative to `cron` and `at`_
 - _Timers have built-in support for calendar time events, monotonic time events, and can be run asynchronously_
- _If `cron` and `at` are not installed, this section can be skipped_

**Rational Statement:**  

**Impact Statement:**  

**Remediation Procedure:**  

**Audit Procedure:**  

**Additional Information:**  

**CIS Controls:**  

**CIS Safeguards 1 (v8):**  

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:**  

**v8 IG2:**  

**v8 IG3:**  

**CIS Safeguards 1 (v7):**  

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:**  

**v7 IG2:**  

**v7 IG3:**  

**References:**  


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:**  

**Title:** Configure SSH Server

**Assessment Status:**  

**Description:** SSH is a secure, encrypted replacement for common login services such as `telnet`, `ftp`, `rlogin`, `rsh`, and `rcp`. It is strongly recommended that sites abandon older clear-text login protocols and use SSH to prevent session hijacking and sniffing of sensitive data off the network.

**Note:**

- The recommendations in this section only apply if the SSH daemon is installed on the system, if remote access is **not** required the SSH daemon can be removed and this section skipped.
- Once all configuration changes have been made to `/etc/ssh/sshd_config` or any included configuration files, the `sshd` configuration must be reloaded:

Command to re-load the SSH daemon configuration:


# systemctl reload sshd


Command to remove the SSH daemon:


# apt purge openssh-server


**Rational Statement:**  

**Impact Statement:**  

**Remediation Procedure:**  

**Audit Procedure:**  

**Additional Information:**  

**CIS Controls:**  

**CIS Safeguards 1 (v8):**  

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:**  

**v8 IG2:**  

**v8 IG3:**  

**CIS Safeguards 1 (v7):**  

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:**  

**v7 IG2:**  

**v7 IG3:**  

**References:**  


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.12

**Title:** Ensure SSH X11 forwarding is disabled

**Assessment Status:** Automated

**Description:** The `X11Forwarding` parameter provides the ability to tunnel X11 traffic through the connection to enable remote graphic connections.

**Rational Statement:** Disable X11 forwarding unless there is an operational requirement to use X11 applications directly. There is a small risk that the remote X11 servers of users who are logged in via SSH with X11 forwarding could be compromised by other users on the X11 server. Note that even if X11 forwarding is disabled, users can always install their own forwarders.

**Impact Statement:**  

**Remediation Procedure:** Edit the `/etc/ssh/sshd_config` file to set the parameter as follows:


X11Forwarding no


**Audit Procedure:** Run the following command:


# sshd -T -C user=root -C host="$(hostname)" -C addr="$(grep $(hostname) /etc/hosts | awk '{print $1}')" | grep -i x11forwarding


Verify the output matches:


x11forwarding no


Run the following command:



# grep -Ei '^\s*x11forwarding\s+yes' /etc/ssh/sshd_config


Nothing is returned.

**Additional Information:**  

**CIS Controls:** TITLE:Uninstall or Disable Unnecessary Services on Enterprise Assets and Software CONTROL:v8 4.8 DESCRIPTION:Uninstall or disable unnecessary services on enterprise assets and software, such as an unused file sharing service, web application module, or service function.;TITLE:Ensure Only Approved Ports, Protocols and Services Are Running CONTROL:v7 9.2 DESCRIPTION:Ensure that only network ports, protocols, and services listening on a system with validated business needs, are running on each system.;

**CIS Safeguards 1 (v8):** 4.8

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:**  

**v8 IG2:** X

**v8 IG3:** X

**CIS Safeguards 1 (v7):** 9.2

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:**  

**v7 IG2:** X

**v7 IG3:** X

**References:**  


Section #: 5.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.2.16

**Title:** Ensure SSH AllowTcpForwarding is disabled

**Assessment Status:** Automated

**Description:** SSH port forwarding is a mechanism in SSH for tunneling application ports from the client to the server, or servers to clients. It can be used for adding encryption to legacy applications, going through firewalls, and some system administrators and IT professionals use it for opening backdoors into the internal network from their home machines.

**Rational Statement:** Leaving port forwarding enabled can expose the organization to security risks and backdoors. 

SSH connections are protected with strong encryption. This makes their contents invisible to most deployed network monitoring and traffic filtering solutions. This invisibility carries considerable risk potential if it is used for malicious purposes such as data exfiltration. Cybercriminals or malware could exploit SSH to hide their unauthorized communications, or to exfiltrate stolen data from the target network.

**Impact Statement:** SSH tunnels are widely used in many corporate environments. In some environments the applications themselves may have very limited native support for security. By utilizing tunneling, compliance with SOX, HIPAA, PCI-DSS, and other standards can be achieved without having to modify the applications.

**Remediation Procedure:** Edit the `/etc/ssh/sshd_config` file to set the parameter as follows:


AllowTcpForwarding no


**Audit Procedure:** Run the following command:


# sshd -T -C user=root -C host="$(hostname)" -C addr="$(grep $(hostname) /etc/hosts | awk '{print $1}')" | grep -i allowtcpforwarding


Verify the output matches:


allowtcpforwarding no


Run the following command:


# grep -Ei '^\s*AllowTcpForwarding\s+yes' /etc/ssh/sshd_config


Nothing should be returned.

**Additional Information:**  

**CIS Controls:** TITLE:Establish and Maintain a Secure Configuration Process CONTROL:v8 4.1 DESCRIPTION:Establish and maintain a secure configuration process for enterprise assets (end-user devices, including portable and mobile, non-computing/IoT devices, and servers) and software (operating systems and applications). Review and update documentation annually, or when significant enterprise changes occur that could impact this Safeguard.;TITLE:Ensure Only Approved Ports, Protocols and Services Are Running CONTROL:v7 9.2 DESCRIPTION:Ensure that only network ports, protocols, and services listening on a system with validated business needs, are running on each system.;

**CIS Safeguards 1 (v8):** 4.1

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:** X

**v8 IG2:** X

**v8 IG3:** X

**CIS Safeguards 1 (v7):** 9.2

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:**  

**v7 IG2:** X

**v7 IG3:** X

**References:** https://www.ssh.com/ssh/tunneling/example


Section #: 5.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:**  

**Title:** Configure privilege escalation

**Assessment Status:**  

**Description:** There are various tools which allows a permitted user to execute a command as the superuser or another user, as specified by the security policy.

#### sudo

[sudo documentation](https://www.sudo.ws/)

The invoking user's real (not effective) user ID is used to determine the user name with which to query the security policy.

`sudo` supports a plug-in architecture for security policies and input/output logging. Third parties can develop and distribute their own policy and I/O logging plug-ins to work seamlessly with the `sudo` front end. The default security policy is `sudoers`, which is configured via the file `/etc/sudoers` and any entries in `/etc/sudoers.d`.

#### pkexec

[pkexec documentation](https://www.freedesktop.org/software/polkit/docs/0.105/pkexec.1.html)

`pkexec` allows an authorized user to execute _PROGRAM_ as another user. If _username_ is not specified, then the program will be executed as the administrative super user, `root`.

**Rational Statement:**  

**Impact Statement:**  

**Remediation Procedure:**  

**Audit Procedure:**  

**Additional Information:**  

**CIS Controls:**  

**CIS Safeguards 1 (v8):**  

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:**  

**v8 IG2:**  

**v8 IG3:**  

**CIS Safeguards 1 (v7):**  

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:**  

**v7 IG2:**  

**v7 IG3:**  

**References:**  


Section #: 5.3
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:** 5.3.4

**Title:** Ensure users must provide password for privilege escalation

**Assessment Status:** Automated

**Description:** The operating system must be configured so that users must provide a password for privilege escalation.

**Rational Statement:** Without (re-)authentication, users may access resources or perform tasks for which they do not have authorization. 

When operating systems provide the capability to escalate a functional capability, it is critical the user (re-)authenticate.

**Impact Statement:** This will prevent automated processes from being able to elevate privileges.

**Remediation Procedure:** Based on the outcome of the audit procedure, use `visudo -f <PATH TO FILE>` to edit the relevant sudoers file.

Remove any line with occurrences of `NOPASSWD` tags in the file.

**Audit Procedure:** **Note:** If passwords are not being used for authentication, this is not applicable.

Verify the operating system requires users to supply a password for privilege escalation.

Check the configuration of the `/etc/sudoers` and `/etc/sudoers.d/*` files with the following command:


# grep -r "^[^#].*NOPASSWD" /etc/sudoers*


If any line is found refer to the remediation procedure below.

**Additional Information:**  

**CIS Controls:** TITLE:Restrict Administrator Privileges to Dedicated Administrator Accounts CONTROL:v8 5.4 DESCRIPTION:Restrict administrator privileges to dedicated administrator accounts on enterprise assets. Conduct general computing activities, such as internet browsing, email, and productivity suite use, from the user’s primary, non-privileged account.;TITLE:Ensure the Use of Dedicated Administrative Accounts CONTROL:v7 4.3 DESCRIPTION:Ensure that all users with administrative account access use a dedicated or secondary account for elevated activities. This account should only be used for administrative activities and not internet browsing, email, or similar activities.;

**CIS Safeguards 1 (v8):** 5.4

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:** X

**v8 IG2:** X

**v8 IG3:** X

**CIS Safeguards 1 (v7):** 4.3

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:** X

**v7 IG2:** X

**v7 IG3:** X

**References:**  


Section #: 5.4
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:**  

**Title:** Configure PAM

**Assessment Status:**  

**Description:** PAM (Pluggable Authentication Modules) is a service that implements modular authentication modules on UNIX systems. PAM is implemented as a set of shared objects that are loaded and executed when a program needs to authenticate a user. Files for PAM are typically located in the `/etc/pam.d` directory. PAM must be carefully configured to secure system authentication. While this section covers some of PAM, please consult other PAM resources to fully understand the configuration capabilities.

**Note:** The usage of `pam-auth-update`:
- As of this writing, the management of PAM via `pam-auth-update` does not offer all the required functionality implemented by the benchmark. As such, the usage of `pam-auth-update` is not recommended at present.

**Rational Statement:**  

**Impact Statement:**  

**Remediation Procedure:**  

**Audit Procedure:**  

**Additional Information:**  

**CIS Controls:**  

**CIS Safeguards 1 (v8):**  

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:**  

**v8 IG2:**  

**v8 IG3:**  

**CIS Safeguards 1 (v7):**  

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:**  

**v7 IG2:**  

**v7 IG3:**  

**References:**  


Section #: 5.5
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:**  

**Title:** User Accounts and Environment

**Assessment Status:**  

**Description:** This section provides guidance on setting up secure defaults for system and user accounts and their environment.

**Rational Statement:**  

**Impact Statement:**  

**Remediation Procedure:**  

**Audit Procedure:**  

**Additional Information:**  

**CIS Controls:**  

**CIS Safeguards 1 (v8):**  

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:**  

**v8 IG2:**  

**v8 IG3:**  

**CIS Safeguards 1 (v7):**  

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:**  

**v7 IG2:**  

**v7 IG3:**  

**References:**  


Section #: 5.5.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:**  

**Title:** Set Shadow Password Suite Parameters

**Assessment Status:**  

**Description:** While a majority of the password control parameters have been moved to PAM, some parameters are still available through the shadow password suite. Any changes made to `/etc/login.defs `will only be applied if the `usermod `command is used. If user IDs are added a different way, use the `chage `command to effect changes to individual user IDs.

**Rational Statement:**  

**Impact Statement:**  

**Remediation Procedure:**  

**Audit Procedure:**  

**Additional Information:**  

**CIS Controls:**  

**CIS Safeguards 1 (v8):**  

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:**  

**v8 IG2:**  

**v8 IG3:**  

**CIS Safeguards 1 (v7):**  

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:**  

**v7 IG2:**  

**v7 IG3:**  

**References:**  


Section #: 6
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:**  

**Title:** System Maintenance

**Assessment Status:**  

**Description:** Recommendations in this section are intended as maintenance and are intended to be checked on a frequent basis to ensure system stability. Many recommendations do not have quick remediations and require investigation into the cause and best fix available and may indicate an attempted breach of system security.

**Rational Statement:**  

**Impact Statement:**  

**Remediation Procedure:**  

**Audit Procedure:**  

**Additional Information:**  

**CIS Controls:**  

**CIS Safeguards 1 (v8):**  

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:**  

**v8 IG2:**  

**v8 IG3:**  

**CIS Safeguards 1 (v7):**  

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:**  

**v7 IG2:**  

**v7 IG3:**  

**References:**  


Section #: 6.1
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:**  

**Title:** System File Permissions

**Assessment Status:**  

**Description:** This section provides guidance on securing aspects of system files and directories.

**Rational Statement:**  

**Impact Statement:**  

**Remediation Procedure:**  

**Audit Procedure:**  

**Additional Information:**  

**CIS Controls:**  

**CIS Safeguards 1 (v8):**  

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:**  

**v8 IG2:**  

**v8 IG3:**  

**CIS Safeguards 1 (v7):**  

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:**  

**v7 IG2:**  

**v7 IG3:**  

**References:**  


Section #: 6.2
------------------------------------------------------------------------------------------------------------------------------------------------------

**Recommendation #:**  

**Title:** Local User and Group Settings

**Assessment Status:**  

**Description:** This section provides guidance on securing aspects of the local users and groups.

**Note:** The recommendations in this section check local users and groups. Any users or groups from other sources such as LDAP will not be audited. In a domain environment similar checks should be performed against domain users and groups.

**Rational Statement:**  

**Impact Statement:**  

**Remediation Procedure:**  

**Audit Procedure:**  

**Additional Information:**  

**CIS Controls:**  

**CIS Safeguards 1 (v8):**  

**CIS Safeguards 2 (v8):**  

**CIS Safeguards 3 (v8):**  

**v8 IG1:**  

**v8 IG2:**  

**v8 IG3:**  

**CIS Safeguards 1 (v7):**  

**CIS Safeguards 2 (v7):**  

**CIS Safeguards 3 (v7):**  

**v7 IG1:**  

**v7 IG2:**  

**v7 IG3:**  

**References:**  


