******************************************************************************
Systemhärtung - Einführung
******************************************************************************

.. sectionauthor:: Pierre Gronau <Pierre.Gronau@ndaal.eu>

.. contents:: Inhalt - Sicherheit - Container
   :depth: 4

**Systemhärtung**, auch als System Hardening bekannt,
ist ein umfassender Prozess zur Erhöhung der Sicherheit von ITK-Systemen.
Ziel ist es, die Angriffsfläche zu minimieren
und potenzielle Sicherheitsrisiken zu beseitigen.

Definition und Zweck
------------------------------------------------------------------------------

**Systemhärtung** bezieht sich auf Methoden, Werkzeuge und bewährte Verfahren,
die eingesetzt werden, um die Sicherheit der gesamten IT-Infrastruktur
zu verbessern. Dies umfasst Software, Datensysteme und Hardware. Der Prozess
zielt darauf ab, Systeme widerstandsfähiger gegen Angriffe und
unbefugten Zugriff zu machen.

Hauptaspekte der Systemhärtung
------------------------------------------------------------------------------

- **Reduzierung der Angriffsfläche**: Durch Deaktivierung 
  nicht benötigter Dienste und Entfernung überflüssiger Software.

- **Konfigurationsanpassung**: Änderung von Standardeinstellungen
  zur Erhöhung der Sicherheit.

- **Patch-Management**: Regelmäßige Installation von Sicherheitsupdates.

- **Zugriffskontrolle**: Implementierung starker Authentifizierungsmechanismen
  und Berechtigungskonzepte.

- **Netzsicherheit**: Einsatz von Firewalls und Netzsegmentierung.

Methoden der Systemhärtung
------------------------------------------------------------------------------

- Entfernung oder Deaktivierung nicht erforderlicher Softwarekomponenten

- Verwendung unprivilegierter Benutzerkonten für Server-Prozesse

- Anpassung von Dateisystemrechten

- Einsatz von Verschlüsselungstechnologien

- Implementierung von Mandatory Access Control

Bedeutung der Systemhärtung
------------------------------------------------------------------------------

Bedeutung für Organisationen
==============================================================================

Systemhärtung ist entscheidend für den Schutz sensibler Unternehmensdaten und
-systeme. Sie reduziert das Risiko erfolgreicher Cyberangriffe erheblich und
kann im Extremfall sogar vor einer Insolvenz schützen.

Bedeutung aus Sicht des BSI-Grundschutz
==============================================================================

Der BSI-Grundschutz, auch IT-Grundschutz genannt, ist ein vom Bundesamt 
für Sicherheit in der Informationstechnik (BSI) entwickeltes Konzept 
zur Gewährleistung der Informationssicherheit in Unternehmen, Behörden 
und anderen Organisationen. Der BSI- bzw. IT-Grundschutz bietet 
eine systematische Vorgehensweise zur Umsetzung 
der notwendigen Sicherheitsmaßnahmen.

Das BSI hat passend dazu eine Zuordnungstabelle veröffentlicht, 
die Maßnahmen aus der ISO 27001 auf den Grundschutz abbildet. 
Beim Punkt “A.8.9. / Konfigurationsmanagement” wird ganz klar 
eine Systemhärtung empfohlen. [1]_ , [2]_, [3]_


Fazit
------------------------------------------------------------------------------

.. Hint::
   **Systemhärtung** ist ein **kontinuierlicher Prozess**, der alle Bereiche
   der ITK-Infrastruktur umfasst. Durch die systematische Anwendung
   von Härtungsmaßnahmen können Unternehmen ihre Cybersicherheit signifikant
   verbessern und sich besser gegen potenzielle Bedrohungen schützen.


.....

.. Rubric:: Footnotes

.. [1]
   Gesetze, Regularien & Normen: Deshalb ist Systemhärtung 2024 & 2025 
   ein “Must Have” für Ihr Unternehmen
   https://www.fb-pro.com/systemhaertung-gesetze-regularien-vorgaben/

.. [2]
   BSI Zuordnungstabelle
   https://www.bsi.bund.de/SharedDocs/Downloads/DE/BSI/Grundschutz/IT-GS-Kompendium/Zuordnung_ISO_und_IT_Grundschutz_Edit_6.pdf?__blob=publicationFile&v=2

.. [3]
   Mindeststandard zur Protokollierung und Detektion von Cyberangriffen” des BSI
   BSI-Mindeststandard steht unter anderem unter PD.2.2.03
   (“Umsetzungphase der Protokollierung”):

   “Der Zugriff auf die Protokollierungsinfrastruktur sowie auf die Protokoll-
   und Protokollierungsdaten MUSS restriktiv konfiguriert und überwacht werden.”

   https://www.bsi.bund.de/DE/Themen/Oeffentliche-Verwaltung/Mindeststandards/PDCA/PDCA_node.html
