******************************************************************************
Systemhärtung - Randbedingungen
******************************************************************************

.. sectionauthor:: Pierre Gronau <Pierre.Gronau@ndaal.eu>




.. contents:: Inhalt - Randbedingungen
   :depth: 3

Randbedingungen
------------------------------------------------------------------------------

Dieses Kapitel beschreibt die **Randbedingungen**, die im Wesentlichen 
für die Implementierung und den Betrieb der Systemhärtungslösung 
maßgeblich sind. Diese Randbedingungen bilden den Rahmen 
für alle technischen und organisatorischen Entscheidungen 
und gewährleisten eine konsistente und effektive Umsetzung 
der Sicherheitsmaßnahmen.

Technische Randbedingungen
==============================================================================

Die technischen Randbedingungen definieren die spezifischen Hardware- und 
Softwareanforderungen sowie die technischen Richtlinien für die Systemhärtung.

Betriebssystem und Hardware
##############################################################################

- Es werden ausschließlich Linux-Distributionen betrachtet, die 
  **Red Hat Enterprise Linux 9** vollständig entsprechen. Dies gilt momentan
  für folgende betrachteten **Linux Derivate**:

  - **Rocky Linux**

  - **AlmaLinux**

  - **Oracle Linux**

- Andere Linux-Distributionen oder -Versionen werden nicht unterstützt.

- (virtuelle) Hardware-Architektur: Ausschließlich x86-64 (64-Bit)

Systemhärtung
##############################################################################

- Die Systemhärtung wird nur für ein Linux-Basissystem durchgeführt. Unter 
  einem Basissystem verstehen wir eine Minimalinstallation 
  des Betriebssystems ohne zusätzliche Anwendungen oder Dienste.

- Die Härtungsmaßnahmen orientieren sich am CIS (Center for Internet Security) 
  Benchmark in der aktuellen Ausprägung von 2024.

Konfigurationsmanagement
##############################################################################

- Konfigurationsänderungen dürfen nicht mittels Agenten vorgenommen werden. 
  Dies gewährleistet eine bessere Kontrolle über die Änderungen 
  und reduziert potenzielle Sicherheitsrisiken durch zusätzliche Software 
  auf den Zielsystemen.

- Zur Konfiguration werden verwendet:

  - **Ansible** für die automatisierte Konfigurationsverwaltung
  
  - **Bash-Skripte** für spezifische Aufgaben und Anpassungen


Bash-Skripte
##############################################################################

**Bash-Skripte** werden grundsätzlich mit folgenden Einstellungen ausgeführt:

bash

.. code-block:: bash
   :caption: bash Laufzeiteinstellungen

   # !/usr/bin/env bash

   set -o errexit
   set -o errtrace
   set -o nounset
   set -o pipefail
   # set -o xtrace
   
   # nosemgrep: ifs-tampering
   IFS=$'\n\t'

Diese Einstellungen sind für alle selbst erstellten **bash Skripte**
verbindlich und dienen der Erhöhung der Robustheit und Sicherheit
der Skriptausführung.

Organisatorische Randbedingungen
==============================================================================

- **Automatisierung hat Vorrang:** Dies gewährleistet Konsistenz, reduziert 
  menschliche Fehler und ermöglicht eine effiziente Skalierung 
  der Härtungsmaßnahmen.

- **Die Job-Steuerung basiert auf Jenkins.** Es wird empfohlen, 
  spezifische Jenkins-Plugins für die Versionskontrolle und das Reporting 
  zu verwenden, um den Härtungsprozess optimal zu unterstützen.

Konventionen
==============================================================================

Folgende **Konventionen** MÜSSEN mindestens beachtet werden:

- Alle Konfigurationsänderungen MÜSSEN nachvollziehbar und reproduzierbar sein.

- Die Systemhärtung MUSS den aktuellen Sicherheitsstandards entsprechen.

- Alle Skripte und Konfigurationsdateien SOLLEN ausreichend dokumentiert sein.

- Documentation as Code: Die Dokumentation wird im gleichen Repository 
  wie der Code verwaltet und folgt den gleichen Versionierungs- 
  und Review-Prozessen.

- Alle Skripte und Konfigurationsdateien MÜSSEN versioniert in Git 
  abgelegt sein. Es wird ein Feature-Branch-Modell mit Pull Requests 
  für Code-Reviews empfohlen.

- Grundsätzlich idempotente Skripte: Skripte MÜSSEN so geschrieben sein, 
  dass sie bei mehrmaliger Ausführung das gleiche Ergebnis liefern 
  und keine unbeabsichtigten Nebeneffekte verursachen.

Diese **Konventionen** stellen sicher, dass die **Systemhärtung**
konsistent, nachvollziehbar und wartbar bleibt.
