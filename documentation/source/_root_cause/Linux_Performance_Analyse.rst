******************************************************************************
Wenn mal was schief geht - Linux Performance Analyse
******************************************************************************

.. sectionauthor:: Pierre Gronau <Pierre.Gronau@ndaal.eu>

.. _Section_Linux_Performance_Analyse:

.. contents:: Inhalt - Sicherheit - Container
   :depth: 5

Linux Performance Analyse
------------------------------------------------------------------------------

Wenn es bei einem Linux Server zu einem akuten Performance-Problem kommt, 
bleibt oft nur wenig Zeit das Problem im Detail zu analysieren. 
Brendan Gregg, Computer Performance Analyst und Kernel Engineer, beschreibt
in einem Blog-Posting welche Linux Kommandos zur Performance Analyse 
in den ersten 60 Sekunden von ihm verwendet werden.

Die folgenden Kommandos empfiehlt Brendan Gregg:[1]_ 

.. code-block:: bash
   :caption: Kommandos zur Performance Analyse
   
   uptime
   dmesg | tail
   vmstat 1
   mpstat -P ALL 1
   pidstat 1
   iostat -xz 1
   free -m
   sar -n DEV 1
   sar -n TCP,ETCP 1
   top

Weitere Informationen zu diesen Kommandos finden Sie in folgenden Kapitel:

vmstat: Linux Performance Messungen mit vmstat
==============================================================================

**vmstat** (virtual memory statistics) ist ein wertvolles Überwachungswerkzeug, 
das neben Informationen zum Speicher auch Infos zum Block IO und 
zur CPU Aktivität ausgibt.

Grundlagen von vmstat
##############################################################################

vmstat gibt eine Reihe von Werten aus und wird typischerweise mit zwei 
numerischen Parametern aufgerufen:

* Beispiel: ``vmstat 1 5``
** 1 -> die Werte werden jede Sekunde erneut gemessen und ausgegeben
** 5 -> die Werte werden 5x ausgegeben, danach wird das Programm beendet

Die erste Ausgabezeile enthält dabei die Durchschnittswerte seit 
dem letzten Reboot. Alle weiteren Ausgabezeilen stellen die jeweils 
aktuellen Werte dar. vmstat benötigt keine speziellen Benutzerrechte, 
man kann es also als normaler Benutzer ausführen.

.. code-block:: bash
   :caption: vmstat 1 5
   
   [user@fedora9 ~]$ vmstat 1 5
   procs -----------memory---------- ---swap-- -----io---- --system-- -----cpu------
   r  b   swpd   free   buff  cache   si   so    bi    bo   in   cs us sy id wa st
   3  0      0  44712 110052 623096    0    0    30    28  217  888 13  3 83  1  0
   0  0      0  44408 110052 623096    0    0     0     0   88 1446 31  4 65  0  0
   0  0      0  44524 110052 623096    0    0     0     0   84  872 11  2 87  0  0
   0  0      0  44516 110052 623096    0    0     0     0  149 1429 18  5 77  0  0
   0  0      0  44524 110052 623096    0    0     0     0   60  431 14  1 85  0  0
   [user@fedora9 ~]$


Bedeutung der einzelnen Werte
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* Procs
** r: The number of processes waiting for run time.
** b: The number of processes in uninterruptible sleep.
* Memory
** swpd: the amount of virtual memory used.
** free: the amount of idle memory.
** buff: the amount of memory used as buffers.
** cache: the amount of memory used as cache.
** inact: the amount of inactive memory. (-a option)
** active: the amount of active memory. (-a option)
* Swap
** si: Amount of memory swapped in from disk (/s).
** so: Amount of memory swapped to disk (/s).
* IO
** bi: Blocks received from a block device (blocks/s).
** bo: Blocks sent to a block device (blocks/s).
* System
** in: The number of interrupts per second, including the clock.
** cs: The number of context switches per second.
* CPU
** These are percentages of total CPU time.
** us: Time spent running non-kernel code. (user time, including nice time)
** sy: Time spent running kernel code. (system time)
** id: Time spent idle. Prior to Linux 2.5.41, this includes IO-wait time.
** wa: Time spent waiting for IO. Prior to Linux 2.5.41, included in idle.
** st: Time stolen from a virtual machine. Prior to Linux 2.6.11, unknown.

Beispiele
##############################################################################

Beispiel: User CPU-Last
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

In diesem Beispiel wird mittels lame Encoder<ref>http://lame.sourceforge.net/</ref> 
eine normale Audio-Datei in eine MP3-Datei kodiert. Dieser Vorgang ist sehr 
CPU-intensiv, das zeigt auch das parallel ausgeführte ``vmstat`` mit einer 
user CPU time von 97%:

<pre>
[user@fedora8 ~]$ vmstat 1 5
procs -----------memory---------- ---swap-- -----io---- --system-- -----cpu------
 r  b   swpd   free   buff  cache   si   so    bi    bo   in   cs us sy id wa st
 6  1      0 302380  16356 271852    0    0   561   568   80  590 43  7 43  7  0
 1  0      0 300892  16364 273256    0    0     0    52   79  274 97  3  0  0  0
 2  0      0 299544  16364 274532    0    0     0     0   78  372 97  3  0  0  0
 1  0      0 298292  16368 275780    0    0     0     0   53  255 97  3  0  0  0
 1  0      0 296820  16368 277192    0    0     0     0   77  377 97  3  0  0  0
[user@fedora8 ~]$
</pre>

Beispiel: System CPU-Last
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

In diesem Beispiel wird mittels ``dd`` eine Datei mit zufälligem Inhalt geschrieben:
 [user@fedora9 ~]$ dd if=/dev/urandom of=500MBfile bs=1M count=500

``/dev/urandom``<ref>http://de.wikipedia.org/wiki//dev/random</ref> liefert dabei Zufallszahlen, die vom Kernel berechnet werden. Das führt zu einer hohen CPU-Last (sy - system time). Das parallel dazu ausgeführte ``vmstat`` zeigt hierbei an, dass zwischen 93% und 97% der Zeit die CPU mit Ausfürung von Kernel Code beschäftigt ist (in diesem Falle mit der Erzeugung von Zufallszahlen).

<pre>
[user@fedora9 ~]$ vmstat 1 5
procs -----------memory---------- ---swap-- -----io---- --system-- -----cpu------
 r  b   swpd   free   buff  cache   si   so    bi    bo   in   cs us sy id wa st
 2  0 402944  54000 161912 745324    5   14    54    59  221  867 13  3 82  2  0
 1  0 402944  53232 161916 748396    0    0     0     0   30  213  3 97  0  0  0
 1  0 402944  49752 161920 751452    0    0     0     0   28  290  4 96  0  0  0
 1  0 402944  45804 161924 755564    0    0     0     0   29  188  2 98  0  0  0
 1  0 402944  42568 161936 758608    0    0     0 17456  272  509  7 93  0  0  0
[user@fedora9 ~]$
</pre>

Die Ausführungszeit von System Calls<ref>http://de.wikipedia.org/wiki/Systemaufruf</ref><ref>http://www.softpanorama.org/Internals/unix_system_calls.shtml</ref><ref>http://www.ibm.com/developerworks/linux/library/l-system-calls/</ref> zählt zur system time (sy).

Beispiel: RAM-Engpass (swapping)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

In diesem Beispiel sind zahlreiche Anwendung geöffnet (unter anderem 
VirtualBox mit einem Windows-Gastsystem). Es ist beinahe 
der gesamte Arbeitsspeicher belegt. Nun wird eine weitere Anwendung 
(OpenOffice) gestartet. Der Linux Kernel lagert nun mehrere Speicherseiten 
in den swap auf die Festplatte aus, um RAM für OpenOffice frei zu bekommen. 
Diese Auslagerung von Speicherseiten in den swap ist im parallel ausgeführten 
``vmstat`` in der Spalte so (swap out - memory swapped to disk) ersichtlich:

<pre>
[user@fedora8 ~]$ vmstat 1 5
procs -----------memory---------- ---swap-- -----io---- --system-- -----cpu------
 r  b   swpd   free   buff  cache   si   so    bi    bo   in   cs us sy id wa st
 3  1 244208  10312   1552  62636    4   23    98   249   44  304 28  3 68  1  0
 0  2 244920   6852   1844  67284    0  544  5248   544  236 1655  4  6  0 90  0
 1  2 256556   7468   1892  69356    0 3404  6048  3448  290 2604  5 12  0 83  0
 0  2 263832   8416   1952  71028    0 3788  2792  3788  140 2926 12 14  0 74  0
 0  3 274492   7704   1964  73064    0 4444  2812  5840  295 4201  8 22  0 69  0
[user@fedora8 ~]$
</pre>

Beispiel: hohe IO Leselast
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Hier mittels ``dd`` eine große Datei (z.B. ISO-File) eingelesen und 
nach /dev/null geschrieben:

[user@fedora9 ~]$ dd if=bigfile.iso of=/dev/null bs=1M

Das parallel ausgeführte ``vmstat`` zeigt die erhöhte IO-Leselast (bi Wert):

<pre>
[user@fedora9 ~]$ vmstat 1 5
procs -----------memory---------- ---swap-- -----io---- --system-- -----cpu------
 r  b   swpd   free   buff  cache   si   so    bi    bo   in   cs us sy id wa st
 3  1 465872  36132  82588 1018364    7   17    70   127  214  838 12  3 82  3  0
 0  1 465872  33796  82620 1021820    0    0 34592     0  357  781  6 10  0 84  0
 0  1 465872  36100  82656 1019660    0    0 34340     0  358  723  5  9  0 86  0
 0  1 465872  35744  82688 1020416    0    0 33312     0  345  892  8 11  0 81  0
 0  1 465872  35716  82572 1020948    0    0 34592     0  358  738  7  8  0 85  0
[user@fedora9 ~]$ 
</pre>

Beispiel: hohe IO Schreiblast
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Im Gegensatz dazu wird hier mittels ``dd`` aus /dev/zero gelesen und die Daten in eine Datei geschrieben. Das oflag=dsync bewirkt, dass die Daten sofort auf die Festplatte geschrieben werden (und nicht nur in den [[Linux Page Cache Grundlagen|Page Cache]]):
 [user@fedora9 ~]$ dd if=/dev/zero of=500MBfile bs=1M count=500 oflag=dsync

Das parallel ausgeführte ``vmstat`` zeigt die erhöhte IO-Schreiblast (bo Wert):

<pre>
[user@fedora9 ~]$ vmstat 1 5
procs -----------memory---------- ---swap-- -----io---- --system-- -----cpu------
 r  b   swpd   free   buff  cache   si   so    bi    bo   in   cs us sy id wa st
 1  1      0  35628  14700 1239164    0    0  1740   652  117  601 11  4 66 20  0
 0  1      0  34852  14896 1239788    0    0     0 23096  300  573  3 16  0 81  0
 0  1      0  32780  15080 1241304    0    0     4 21000  344  526  1 13  0 86  0
 0  1      0  36512  15244 1237256    0    0     0 19952  276  394  1 12  0 87  0
 0  1      0  35688  15412 1237180    0    0     0 18904  285  465  1 13  0 86  0
[user@fedora9 ~]$ 
</pre>

Beispiel: CPU wartet auf IO
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Im folgenden Beispiel läuft gerade ein ``updatedb`` Prozess. Das ``updatedb`` Tool ist Teil von [https://fedorahosted.org/mlocate/ mlocate]. Es durchsucht das gesamte Dateisystem und erstellt daraufhin die Datenbank für das ``locate`` Kommando (mit dem dann sehr schnell nach Dateien gesucht werden kann). Da ``updatedb`` alle Dateinamen im gesamten Dateisystem einliest, muss die CPU laufend darauf warten vom IO-System (der Festplatte) Daten zu bekommen. Daher zeigt das parallel ausgeführte ``vmstat`` für die CPU einen hohen wa Wert (waiting for IO):
<pre>
[user@fedora9 ~]$ vmstat 1 5
procs -----------memory---------- ---swap-- -----io---- --system-- -----cpu------
 r  b   swpd   free   buff  cache   si   so    bi    bo   in   cs us sy id wa st
 2  1 403256 602848  17836 400356    5   15    50    50  207  861 13  3 83  1  0
 1  0 403256 601568  18892 400496    0    0  1048   364  337 1903  5  7  0 88  0
 0  1 403256 600816  19640 400568    0    0   748     0  259 1142  6  4  0 90  0
 0  1 403256 600300  20116 400800    0    0   476     0  196  630  8  5  0 87  0
 0  1 403256 599328  20792 400792    0    0   676     0  278 1401  7  5  0 88  0
[user@fedora9 ~]$
</pre>

Weitere vmstat Optionen
##############################################################################

vmstat --help
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

<pre>
[user@fedora9 ~]$ vmstat --help
usage: vmstat [-V] [-n] [delay [count]]
              -V prints version.
              -n causes the headers not to be reprinted regularly.
              -a print inactive/active page stats.
              -d prints disk statistics
              -D prints disk table
              -p prints disk partition statistics
              -s prints vm table
              -m prints slabinfo
              -S unit size
              delay is the delay between updates in seconds. 
              unit size k:1000 K:1024 m:1000000 M:1048576 (default is K)
              count is the number of updates.
[user@fedora9 ~]$
</pre>

vmstat
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

<pre>
[user@fedora9 ~]$ vmstat
procs -----------memory---------- ---swap-- -----io---- --system-- -----cpu------
 r  b   swpd   free   buff  cache   si   so    bi    bo   in   cs us sy id wa st
 2  0  14960  38016   6584 1069284    0    1   506    81  727 1373 12  4 81  3  0
[user@fedora9 ~]$
</pre>

vmstat -V
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

<pre>
[user@fedora9 ~]$ vmstat -V
procps version 3.2.7
[user@fedora9 ~]$
</pre>

vmstat -a
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

<pre>
[user@fedora9 ~]$ vmstat -a
procs -----------memory---------- ---swap-- -----io---- --system-- -----cpu------
 r  b   swpd   free  inact active   si   so    bi    bo   in   cs us sy id wa st
 3  0  14960  38024 988284 461704    0    1   506    81  726 1372 12  4 81  3  0
[user@fedora9 ~]$
</pre>

vmstat -d
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

<pre>
[user@fedora9 ~]$ vmstat -d
disk- ------------reads------------ ------------writes----------- -----IO------
       total merged sectors      ms  total merged sectors      ms    cur    sec
ram0       0      0       0       0      0      0       0       0      0      0
ram1       0      0       0       0      0      0       0       0      0      0
ram2       0      0       0       0      0      0       0       0      0      0
ram3       0      0       0       0      0      0       0       0      0      0
ram4       0      0       0       0      0      0       0       0      0      0
ram5       0      0       0       0      0      0       0       0      0      0
ram6       0      0       0       0      0      0       0       0      0      0
ram7       0      0       0       0      0      0       0       0      0      0
ram8       0      0       0       0      0      0       0       0      0      0
ram9       0      0       0       0      0      0       0       0      0      0
ram10      0      0       0       0      0      0       0       0      0      0
ram11      0      0       0       0      0      0       0       0      0      0
ram12      0      0       0       0      0      0       0       0      0      0
ram13      0      0       0       0      0      0       0       0      0      0
ram14      0      0       0       0      0      0       0       0      0      0
ram15      0      0       0       0      0      0       0       0      0      0
sda   136909  31536 13893867 1197609  58190 219323 2233264 7688807      0    677
sda1   35703   6048 1326394  511477   6728  16136  182984  419232      0    222
sda2      85   1489    2935     653    141   3603   29952    5254      0      1
sda3  101111  23961 12564154  685330  51321 199584 2020328 7264321      0    512
sr0        0      0       0       0      0      0       0       0      0      0
fd0        0      0       0       0      0      0       0       0      0      0
[user@fedora9 ~]
</pre>

vmstat -D
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

<pre>
[user@fedora9 ~]$ vmstat -D
           22 disks 
            0 partitions 
       273820 total reads
        63034 merged reads
     27787446 read sectors
      2395193 milli reading
       116450 writes
       438666 merged writes
      4467248 written sectors
     15377932 milli writing
            0 inprogress IO
         1412 milli spent IO
</pre>

vmstat -p
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``vmstat -p`` funktionierte in Fedora nicht: https://bugzilla.redhat.com/show_bug.cgi?id=485246. Die folgende Ausgabe stammt von einem System mit Ubuntu 9.10:

<pre>
user@ubuntu-9-10:~$ vmstat -p /dev/sda9
sda9          reads   read sectors  writes    requested writes
               23420     411365      24464     530801
user@ubuntu-9-10:~$
</pre>

vmstat -s
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

<pre>
[user@fedora9 ~]$ vmstat -s
      1553972  total memory
      1516180  used memory
       461892  active memory
       988304  inactive memory
        37792  free memory
         6644  buffer memory
      1069388  swap cache
      1052248  total swap
        14960  used swap
      1037288  free swap
       161467 non-nice user cpu ticks
         7586 nice user cpu ticks
        46310 system cpu ticks
      1108919 idle cpu ticks
        46832 IO-wait cpu ticks
         2694 IRQ cpu ticks
         2452 softirq cpu ticks
            0 stolen cpu ticks
      6947021 pages paged in
      1116896 pages paged out
          183 pages swapped in
         3744 pages swapped out
      9985406 interrupts
     18852586 CPU context switches
   1239004323 boot time
        15072 forks
[user@fedora9 ~]$
</pre>

===vmstat -m===
<pre>
[user@fedora9 ~]$ vmstat -m
Cache                       Num  Total   Size  Pages
fuse_request                 11     11    368     11
fuse_inode                    9      9    448      9
rpc_inode_cache               8      8    512      8
nf_conntrack_expect           0      0    168     24
nf_conntrack                 26     80    248     16
dm_uevent                     0      0   2464      3
UDPv6                        22     22    704     11
TCPv6                         6      6   1344      6
kmalloc_dma-512               8      8    512      8
sgpool-128                   12     12   2048      4
scsi_io_context               0      0    104     39
ext3_inode_cache           6822   8360    496      8
ext3_xattr                   85     85     48     85
journal_handle              170    170     24    170
journal_head                 76    219     56     73
revoke_record               256    256     16    256
flow_cache                    0      0     80     51
bsg_cmd                       0      0    288     14
mqueue_inode_cache            7      7    576      7
isofs_inode_cache             0      0    376     10
hugetlbfs_inode_cache        11     11    344     11
dquot                         0      0    128     32
shmem_inode_cache          1058   1071    448      9
xfrm_dst_cache                0      0    320     12
UDP                          19     21    576      7
TCP                          17     24   1216      6
blkdev_queue                 21     21   1080      7
biovec-256                    2      2   3072      2
biovec-128                    5      5   1536      5
biovec-64                     7     10    768      5
sock_inode_cache            619    650    384     10
file_lock_cache              39     39    104     39
Acpi-Operand               2935   2958     40    102
Acpi-Namespace             1700   1700     24    170
Cache                       Num  Total   Size  Pages
taskstats                    25     26    312     13
proc_inode_cache            233    242    360     11
sigqueue                     28     28    144     28
radix_tree_node            7888   8606    296     13
bdev_cache                   24     24    512      8
inode_cache                 370    462    344     11
dentry                     6592  15390    136     30
names_cache                   2      2   4096      2
avc_node                     73     73     56     73
selinux_inode_security     9888  10030     48     85
idr_layer_cache             627    644    144     28
buffer_head                2308   2688     64     64
mm_struct                   659    693    448      9
vm_area_struct            11110  11592     88     46
files_cache                 115    130    384     10
sighand_cache               141    150   1344      6
task_struct                 246    248   3696      2
anon_vma                   4778   5120     16    256
kmalloc-4096                 95    112   4096      8
kmalloc-2048                272    304   2048     16
kmalloc-1024                518    524   1024      4
kmalloc-512                 764    888    512      8
kmalloc-256                 198    208    256     16
kmalloc-128                 629    832    128     32
kmalloc-64                 4322   5568     64     64
kmalloc-32                 1554   1664     32    128
kmalloc-16                 2644   3584     16    256
kmalloc-8                  3561   3584      8    512
kmalloc-192                6349   6930    192     21
kmalloc-96                  885   1176     96     42
[user@fedora9 ~]$ 
</pre>

mpstat: Linux CPU Performance Messungen mit mpstat
==============================================================================

Das Linux Tool <b>mpstat</b> liefert Informationen zur Auslastung 
der einzelnen CPUs eines Systems. Bei Multicore-CPUs wird jeder Core 
als CPU angezeigt. Bei CPUs mit Hyperthreading werden pro physischer CPU 
bzw. physischem Core zwei CPUs angezeigt. mpstat ist bei 
Linux Distributionen meistens im Paket sysstat enthalten (siehe z.B. 
bei Debian http://packages.debian.org/sysstat).

Grundlagen von mpstat
##############################################################################

mpstat wird typischerweise mit der Option ``mpstat -P ALL`` 
aufgerufen. Dadurch werden alle CPUs einzeln aufgelistet. Ein Aufruf 
von ``mpstat -P ALL`` zeigt die Durchschnittswerte 
seit dem letzten Booten:

<pre>
cloud@ubuntu-1204:~$ sudo mpstat -P ALL
Linux 3.2.0-93-generic (ubuntu-1204) 	11/11/2015 	_x86_64_	(4 CPU)

09:38:16 AM  CPU    %usr   %nice    %sys %iowait    %irq   %soft  %steal  %guest   %idle
09:38:16 AM  all    1.94    0.09    0.57    6.95    0.00    0.04    0.00    0.00   90.42
09:38:16 AM    0    1.01    0.20    0.69    3.77    0.00    0.08    0.00    0.00   94.27
09:38:16 AM    1    3.70    0.08    0.67   10.18    0.00    0.07    0.00    0.00   85.30
09:38:16 AM    2    1.72    0.05    0.50   10.28    0.00    0.00    0.00    0.00   87.45
09:38:16 AM    3    1.32    0.04    0.41    3.57    0.00    0.00    0.00    0.00   94.66
cloud@ubuntu-1204:~$
</pre>

Zur Analyse über einen gewissen Zeitraum können zwei weitere Parameter angegeben werden. Bei ``mpstat -P ALL 1 60`` wird jede Sekunde der aktuelle Wert ausgelesen (und dies 60 Mal - d.h. 1 Minute lang). Der letzte Eintrag zeigt dann die Durchschnittswerte über den Messzeitraum. Man kann die Ausgabe von ``mpstat -P ALL 1 60`` bequem in eine Datei schreiben:
<pre>
cloud@ubuntu-1204:~$ sudo mpstat -P ALL 1 60 > mpstatanalyse20151111-1.txt
cloud@ubuntu-1204:~$
</pre>

Hier ein Auszug aus dieser Datei:
<pre>
Linux 3.2.0-93-generic (ubuntu-1204)    11/11/2015      _x86_64_        (4 CPU)

09:35:57 AM  CPU    %usr   %nice    %sys %iowait    %irq   %soft  %steal  %guest   %idle
09:35:58 AM  all    0.00    0.00    0.00   15.19    0.00    0.00    0.00    0.00   84.81
09:35:58 AM    0    0.00    0.00    0.00   18.37    0.00    0.00    0.00    0.00   81.63
09:35:58 AM    1    0.00    0.00    0.99   29.70    0.00    0.99    0.00    0.00   68.32
09:35:58 AM    2    0.00    0.00    1.00   11.00    0.00    0.00    0.00    0.00   88.00
09:35:58 AM    3    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00  100.00

[...]

09:36:56 AM  CPU    %usr   %nice    %sys %iowait    %irq   %soft  %steal  %guest   %idle
09:36:57 AM  all    3.26    0.00    1.50    0.75    0.00    0.00    0.00    0.00   94.49
09:36:57 AM    0    6.06    0.00    0.00    0.00    0.00    0.00    0.00    0.00   93.94
09:36:57 AM    1    0.00    0.00    0.00    2.00    0.00    0.00    0.00    0.00   98.00
09:36:57 AM    2    4.95    0.00    5.94    1.98    0.00    0.00    0.00    0.00   87.13
09:36:57 AM    3    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00  100.00

Average:     CPU    %usr   %nice    %sys %iowait    %irq   %soft  %steal  %guest   %idle
Average:     all    0.48    0.00    0.70   15.71    0.00    0.04    0.00    0.00   83.07
Average:       0    0.66    0.00    1.19   16.44    0.00    0.12    0.00    0.00   81.60
Average:       1    0.29    0.00    0.44   28.94    0.00    0.07    0.00    0.00   70.27
Average:       2    0.53    0.00    0.65   15.27    0.00    0.00    0.00    0.00   83.54
Average:       3    0.42    0.00    0.50    2.27    0.00    0.00    0.00    0.00   96.81
</pre>

Bedeutung der einzelnen Werte
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* <b>CPU</b>: Processor number. The keyword all indicates that statistics are calculated as averages among all processors.
* <b>%usr</b>: Show the percentage of CPU utilization that occurred while executing at the user level (application).
* <b>%nice</b>: Show the percentage of CPU utilization that occurred while executing at the user level with nice priority.
* <b>%sys</b>: Show  the  percentage of CPU utilization that occurred while executing at the system level (kernel). Note that this does not include time spent servicing hardware and software interrupts.
* <b>%iowait</b>: Show the percentage of time that the CPU or CPUs were idle during which the system had an outstanding disk I/O request.
* <b>%irq</b>: Show the percentage of time spent by the CPU or CPUs to service hardware interrupts.
* <b>%soft</b>: Show the percentage of time spent by the CPU or CPUs to service software interrupts.
* <b>%steal</b>: Show  the percentage of time spent in involuntary wait by the virtual CPU or CPUs while the hypervisor was servicing another virtual processor.
* <b>%guest</b>: Show the percentage of time spent by the CPU or CPUs to run a virtual processor.
* <b>%idle</b>: Show the percentage of time that the CPU or CPUs were idle and the system did not have an outstanding disk I/O request.

Analyse
##############################################################################


Auslastung der einzelnen CPUs bzw. Cores
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Bei einem System mit mehreren CPUs oder mehreren Cores ist es sinnvoll diese nach Möglichkeit gleichmäßig auszulasten. Zeigen die Durchschnittswerte von ``mpstat -P ALL`` eine CPU mit sehr hohem %user Anteil, andere CPUs aber mit sehr niedrigem %user Anteil kann durch eine Optimierung der Konfiguration der Anwendungen die Gesamtperformance des Systems womöglich erhöht werden. Eine Datenbank, die nur einen einzelnen Thread bzw. Prozess nutzt kann auch nur einen Core auslasten.

Analysezeitraum
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Da ein ``mpstat -P ALL`` die Durchschnittswerte seit dem letzten Bootvorgang liefert (und somit auch Zeiten mit niedriger Last - etwa Nachts und das Wochenende - beinhaltet), kann z.B. eine zehn Minuten dauernde Messung zu Spitzenzeiten mittels ``mpstat -P ALL 1 600 > /root/mpstatanalyse.txt`` durchgeführt werden. Damit erhält man auch Durchschnittswerte für diese zehn Minuten am Ende der Ausgabe.

iostat: Linux I/O Performance Messungen mit iostat
==============================================================================

Das Monitoring-Tool **iostat** liefert Input/Output Statistiken für Devices, Partitionen und Netz-Dateisysteme (NFS). Daneben gibt es auch CPU Statistiken aus. Iostat ist Teil des sysstat Pakets.

Grundlagen von iostat
##############################################################################

iostat gibt eine Reihe von Werten aus und wird typischerweise mit zwei numerischen Parametern aufgerufen:
* Beispiel: ``iostat 1 5``
** 1 -> die Werte werden jede Sekunde erneut gemessen und ausgegeben
** 5 -> die Werte werden 5x ausgegeben, danach wird das Programm beendet

Die erste Ausgabezeile enthält dabei die Durchschnittswerte seit dem letzten Reboot. Alle weiteren Ausgabezeilen stellen die jeweils aktuellen Werte dar. iostat benötigt keine speziellen Benutzerrechte, man kann es also als normaler Benutzer ausführen.

<pre>
user@ubuntu-9-10:~$ iostat -x 1 5
Linux 2.6.31-20-generic-pae (ubuntu-9-10) 	04/08/2010 	_i686_	(4 CPU)

avg-cpu:  %user   %nice %system %iowait  %steal   %idle
           2.38    0.07    0.66    0.39    0.00   96.51

Device:            tps   Blk_read/s   Blk_wrtn/s   Blk_read   Blk_wrtn
sda               6.93       131.80        85.02    1347350     869177

avg-cpu:  %user   %nice %system %iowait  %steal   %idle
           7.95    0.00    2.65    6.27    0.00   83.13

Device:            tps   Blk_read/s   Blk_wrtn/s   Blk_read   Blk_wrtn
sda             340.00      2720.00         0.00       2720          0

avg-cpu:  %user   %nice %system %iowait  %steal   %idle
           7.19    0.00    4.32    5.76    0.00   82.73

Device:            tps   Blk_read/s   Blk_wrtn/s   Blk_read   Blk_wrtn
sda             440.59      3524.75         0.00       3560          0
[...]
</pre>

Optionen von iostat
##############################################################################

iostat
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Ohne spezielle Parameter sieht die Standardausgabe so aus:
<pre>
user@ubuntu-9-10:~$ iostat 
Linux 2.6.31-20-generic-pae (ubuntu-9-10) 	04/08/2010 	_i686_	(4 CPU)

avg-cpu:  %user   %nice %system %iowait  %steal   %idle
           2.38    0.07    0.66    0.39    0.00   96.50

Device:            tps   Blk_read/s   Blk_wrtn/s   Blk_read   Blk_wrtn
sda               7.19       130.01        87.95    1380622     933921

user@ubuntu-9-10:~$ 
</pre>

iostat -x
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Die Option -x liefert die erweiterte (extended) Statistik:
<pre>
user@ubuntu-9-10:~$ iostat -x
Linux 2.6.31-20-generic-pae (ubuntu-9-10) 	04/08/2010 	_i686_	(4 CPU)

avg-cpu:  %user   %nice %system %iowait  %steal   %idle
           2.38    0.07    0.66    0.39    0.00   96.50

Device:         rrqm/s   wrqm/s     r/s     w/s   rsec/s   wsec/s avgrq-sz avgqu-sz   await  svctm  %util
sda               2.05     8.14    4.31    2.87   129.76    87.85    30.31     0.13   18.47   3.13   2.25

user@ubuntu-9-10:~$ 
</pre>

iostat -d
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Die Option -d zeigt nur Device-Statistiken (keine CPU-Statistiken):
<pre>
user@ubuntu-9-10:~$ iostat -d
Linux 2.6.31-20-generic-pae (ubuntu-9-10) 	04/08/2010 	_i686_	(4 CPU)

Device:            tps   Blk_read/s   Blk_wrtn/s   Blk_read   Blk_wrtn
sda               7.18       129.63        87.83    1380622     935433

user@ubuntu-9-10:~$ 
</pre>

iostat -c
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Die Option -c zeigt nur CPU-Statistiken (keine Device-Statistiken):
<pre>
user@ubuntu-9-10:~$ iostat -c
Linux 2.6.31-20-generic-pae (ubuntu-9-10) 	04/08/2010 	_i686_	(4 CPU)

avg-cpu:  %user   %nice %system %iowait  %steal   %idle
           2.38    0.07    0.66    0.39    0.00   96.50

user@ubuntu-9-10:~$ 
</pre>

Bedeutung der einzelnen Werte
##############################################################################

CPU Utilization Report
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* <b>%user</b>: Show the percentage of CPU utilization that occurred while executing at the user level (application).
* <b>%nice</b>: Show the percentage of CPU utilization that occurred while executing at the user level with nice priority.
* <b>%system</b>: Show the percentage of CPU utilization that occurred while executing at the system level (kernel).
* <b>%iowait</b>: Show the percentage of time that the CPU or CPUs were idle during which the system had an outstanding disk I/O request.
* <b>%steal</b>: Show  the percentage of time spent in involuntary wait by the virtual CPU or CPUs while the hypervisor was servicing another virtual processor.
* <b>%idle</b>: Show the percentage of time that the CPU or CPUs were idle and the system did not have an outstanding disk I/O request.

Device Utilization Report
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* <b>Device</b>: This column gives the device (or partition) name
* <b>tps</b>: Indicate  the  number  of  transfers per second that were issued to the device. A transfer is an I/O request to the device. Multiple logical requests can be combined into a single I/O request to the device. A transfer is of indeterminate size.
* <b>Blk_read/s</b>: Indicate the amount of data read from the device expressed in a number of blocks per second. Blocks are equivalent to  sectors  with kernels 2.4 and later and therefore have a size of 512 bytes. With older kernels, a block is of indeterminate size.
* <b>Blk_wrtn/s</b>: Indicate the amount of data written to the device expressed in a number of blocks per second.
* <b>Blk_read</b>: The total number of blocks read.
* <b>Blk_wrtn</b>: The total number of blocks written.
* <b>kB_read/s</b>: Indicate the amount of data read from the device expressed in kilobytes per second.
* <b>kB_wrtn/s</b>: Indicate the amount of data written to the device expressed in kilobytes per second.
* <b>kB_read</b>: The total number of kilobytes read.
* <b>kB_wrtn</b>: The total number of kilobytes written.
* <b>MB_read/s</b>: Indicate the amount of data read from the device expressed in megabytes per second.
* <b>MB_wrtn/s</b>: Indicate the amount of data written to the device expressed in megabytes per second.
* <b>MB_read</b>: The total number of megabytes read.
* <b>MB_wrtn</b>: The total number of megabytes written.
* <b>rrqm/s</b>: The number of read requests merged per second that were queued to the device.
* <b>wrqm/s</b>: The number of write requests merged per second that were queued to the device.
* <b>r/s</b>: The number of read requests that were issued to the device per second.
* <b>w/s</b>: The number of write requests that were issued to the device per second.
* <b>rsec/s</b>: The number of sectors read from the device per second.
* <b>wsec/s</b>: The number of sectors written to the device per second.
* <b>rkB/s</b>: The number of kilobytes read from the device per second.
* <b>wkB/s</b>: The number of kilobytes written to the device per second.
* <b>rMB/s</b>: The number of megabytes read from the device per second.
* <b>wMB/s</b>: The number of megabytes written to the device per second.
* <b>avgrq-sz</b>: The average size (in sectors) of the requests that were issued to the device.
* <b>avgqu-sz</b>: The average queue length of the requests that were issued to the device.
* <b>await</b>: The average time (in milliseconds) for I/O requests issued to the device to be served. This includes the time spent by the requests in queue and the time spent servicing them.
* <b>svctm</b>: The average service time (in milliseconds) for I/O requests that were issued to the device.
* <b>%util</b>: Percentage of CPU time during which I/O requests were issued to the device (bandwidth utilization for the device). Device saturation occurs when this value is close to 100%.

Network Filesystem Report
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* <b>Filesystem</b>: This  columns  shows  the  hostname  of the NFS server followed by a colon and by the directory name where the network filesystem is mounted.
* <b>rBlk_nor/s</b>: Indicate the number of blocks read by applications via the read(2) system call interface. A block has a size of 512 bytes.
* <b>wBlk_nor/s</b>: Indicate the number of blocks written by applications via the write(2) system call interface.
* <b>rBlk_dir/s</b>: Indicate the number of blocks read from files opened with the O_DIRECT flag.
* <b>wBlk_dir/s</b>: Indicate the number of blocks written to files opened with the O_DIRECT flag.
* <b>rBlk_svr/s</b>: Indicate the number of blocks read from the server by the NFS client via an NFS READ request.
* <b>wBlk_svr/s</b>: Indicate the number of blocks written to the server by the NFS client via an NFS WRITE request.
* <b>rkB_nor/s</b>: Indicate the number of kilobytes read by applications via the read(2) system call interface.
* <b>wkB_nor/s</b>: Indicate the number of kilobytes written by applications via the write(2) system call interface.
* <b>rkB_dir/s</b>: Indicate the number of kilobytes read from files opened with the O_DIRECT flag.
* <b>wkB_dir/s</b>: Indicate the number of kilobytes written to files opened with the O_DIRECT flag.
* <b>rkB_svr/s</b>: Indicate the number of kilobytes read from the server by the NFS client via an NFS READ request.
* <b>wkB_svr/s</b>: Indicate the number of kilobytes written to the server by the NFS client via an NFS WRITE request.
* <b>rMB_nor/s</b>: Indicate the number of megabytes read by applications via the read(2) system call interface.
* <b>wMB_nor/s</b>: Indicate the number of megabytes written by applications via the write(2) system call interface.
* <b>rMB_dir/s</b>: Indicate the number of megabytes read from files opened with the O_DIRECT flag.
* <b>wMB_dir/s</b>: Indicate the number of megabytes written to files opened with the O_DIRECT flag.
* <b>rMB_svr/s</b>: Indicate the number of megabytes read from the server by the NFS client via an NFS READ request.
* <b>wMB_svr/s</b>: Indicate the number of megabytes written to the server by the NFS client via an NFS WRITE request.
* <b>ops/s</b>: Indicate the number of operations that were issued to the filesystem per second.
* <b>rops/s</b>: Indicate the number of 'read' operations that were issued to the filesystem per second.
* <b>wops/s</b>: Indicate the number of 'write' operations that were issued to the filesystem per second.

Weitere Informationen
##############################################################################

* http://www.igvita.com/2009/06/23/measuring-optimizing-io-performance/
* http://mituzas.lt/2009/03/11/iostat/
* http://jcole.us/blog/archives/2007/05/08/on-iostat-disk-latency-iohist-onward/
* http://www.pythian.com/news/247/basic-io-monitoring-on-linux/

free: Linux Page Cache
==============================================================================

Der **Page Cache** unter Linux beschleunigt zahlreiche Lese-Zugriffe von Dateien. Dies geschieht, indem Linux Daten beim erstmaligen Lesen von oder Schreiben auf Datenträgern wie Festplatten zusätzlich in **ungenutzten Bereichen des Arbeitsspeichers** cacht. Werden diese Daten später erneut gelesen, können diese schnell aus dem Arbeitsspeicher gelesen werden. Dieser Artikel liefert wertvolle Hintergrundinformationen zum Page Cache.

Page Cache oder Buffer Cache
##############################################################################

Oft wird für den Page Cache noch der Begriff Buffer Cache verwendet. 
Bei Linux Kerneln bis Version 2.2 gab es sowohl einen Page Cache, als auch 
einen Buffer Cache. Mit Kernel 2.4 wurden diese beiden Caches vereint. 
Heute gibt es nur noch einen Cache, den Page Cache.

<ref>[http://books.google.de/books?id=lZpW6xmXrzoC&pg=PA348&dq=linux+buffer+cache+page+cache&cd=1 Der Buffer-Cache (Kapitel 15.3)] Seite 348, Linux-Kernel Handbuch: Leitfaden zu Design und Implementierung von Kernel 2.6, Robert Love, Addison-Wesley, 2005</ref>

Funktionsweise
##############################################################################

Auslastung
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Unter Linux gibt das Kommando ``free -m`` in der Spalte **cached** an, wieviel MB des Arbeitsspeichers momentan für den Page Cache verwendet werden:
<pre>
[root@testserver ~]# free -m
             total       used       free     shared    buffers     cached
Mem:         15976      15195        781          0        167       9153
-/+ buffers/cache:       5874      10102
Swap:         2000          0       1999
[root@testserver ~]# 
</pre>

Schreiben
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Wenn Daten geschrieben werden, kommen diese zuerst in den Page Cache und werden dort als ''Dirty Pages'' verwaltet. ''Dirty'' deshalb, weil diese Daten zwar im Page Cache liegen, aber erst auf das darunterliegende Speichergerät geschrieben werden müssen. Der Inhalt dieser ''Dirty Pages'' wird regelmäßig (bzw. auch beim Aufruf von Systemcalls wie sync oder fsync) auf das darunterliegende Speichergerät weitergegeben. Dies kann in letzter Instanz etwa ein RAID-Controller oder direkt eine Festplatte sein.

Das folgende Beispiel zeigt die Erstellung einer 10 MByte großen Datei, die zuerst in den Page Cache geschrieben wird. Der Umfang der Dirty Pages steigt dadurch, bis diese in diesem Fall manuell per sync Kommando auf die darunterliegende SSD geschrieben werden:
<pre>
cloud@pc:~$ dd if=/dev/zero of=testfile.txt bs=1M count=10
10+0 records in
10+0 records out
10485760 bytes (10 MB) copied, 0,0121043 s, 866 MB/s
cloud@pc:~$ cat /proc/meminfo | grep Dirty
Dirty:             10260 kB
cloud@pc:~$ sync
cloud@pc:~$ cat /proc/meminfo | grep Dirty
Dirty:                 0 kB
</pre>

==== bis Kernel 2.6.31: pdflush ====
Bis inklusive Linux Kernel 2.6.31 sorgten die pdflush Threads dafür, dass Dirty Pages regelmäßig auf die darunterliegenden Speichergeräte geschrieben wurden.

==== ab Kernel 2.6.32: Per-backing-device based writeback ====
Da pdflush einige Performance-Nachteile hatte, entwickelte Jens Axboe für den [[Neuerungen im Linux Kernel#Kernel 2.6.32|Linux Kernel 2.6.32]] einen neuen effektiveren writeback Mechanismus.<ref>[http://kernelnewbies.org/Linux_2_6_32#head-72c3f91947738f1ea52f9ed21a89876730418a61 Linux 2.6. – 32 Per-backing-device based writeback] (kernelnewbies.org)</ref>

Dabei gibt es nun Threads für jedes Gerät, wie folgendes Beispiel eines Rechners mit einer SSD (/dev/sda) und einer Festplatte (/dev/sdb) zeigt:
<pre>
root@pc:~# ls -l /dev/sda
brw-rw---- 1 root disk 8, 0 2011-09-01 10:36 /dev/sda
root@pc:~# ls -l /dev/sdb
brw-rw---- 1 root disk 8, 16 2011-09-01 10:36 /dev/sdb
root@pc:~# ps -eaf | grep -i flush
root       935     2  0 10:36 ?        00:00:00 [flush-8:0]
root       936     2  0 10:36 ?        00:00:00 [flush-8:16]
</pre>

Lesen
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Nicht nur beim Schreiben, auch beim Lesen von Dateien kommen Dateiblöcke in den Page Cache. Wenn Sie z.B. eine 100 MB Datei zweimal hintereinander lesen, so wird der zweite Zugriff schneller sein, da die Dateiblöcke direkt aus dem Page Cache im Arbeitsspeicher kommen und nicht mehr erneut von der Festplatte gelesen werden müssen. Das folgende Beispiel zeigt, dass nach der Wiedergabe eines gut 200 MB großen Videos die Größe des Page Caches angestiegen ist:

<pre>
user@adminpc:~$ free -m
             total       used       free     shared    buffers     cached
Mem:          3884       1812       2071          0         60       1328
-/+ buffers/cache:        424       3459
Swap:         1956          0       1956
user@adminpc:~$ vlc video.avi
[...]
user@adminpc:~$ free -m
             total       used       free     shared    buffers     cached
Mem:          3884       2056       1827          0         60       1566
-/+ buffers/cache:        429       3454
Swap:         1956          0       1956
user@adminpc:~$
</pre>

Benötigt Linux mehr Arbeitsspeicher für normale Applikationen als aktuell frei ist, werden bereits länger nicht mehr genutzte Bereiche des Page Caches automatisch gelöscht.

Page Cache optimieren
##############################################################################

Das automatische Caching von Dateiblöcken im Page Cache ist meistens sehr vorteilhaft. Manche Daten (etwa Logfiles oder MySQL-Dumps) werden aber oft nach dem Schreiben nicht mehr benötigt. Solche Dateiblöcke belegen also oft unnötig Platz im Page Cache. Andere Dateiblöcke, deren Caching mehr Vorteile bringen würde, fliegen durch die neuen Logfiles oder MySQL aus dem Page Cache hinaus.<ref>[http://www.straylightrun.net/2009/12/03/clearing-the-linux-buffer-cache/ Clearing The Linux Buffer Cache] (blog.straylightrun.net, 03.12.2009)</ref>

Bei Logdateien hilft Ihnen hier etwa ein regelmäßiges Logrotate mit gzip Kompromierung. Wenn eine Logdatei mit beispielsweise 500 MB durch logrotate und gzip auf 10 MB komprimiert wird, wird die ursprüngliche Logdatei und somit auch deren Cache ungültig. 490 MB im Page Cache werden dadurch frei. Die Gefahr, dass eine kontinuierlich wachsenende Logdatei dadurch sinnvollere Dateiblöcke aus dem Page Cache "hinausschiebt" sinkt somit.

Es macht also durchaus Sinn, wenn manche Anwendungen bestimmte Dateien/Dateiblöcke gar nicht cachen würden. Für rsync gibt es dazu auch bereits einen Patch.<ref>[http://insights.oetiker.ch/linux/fadvise.html Improving Linux performance by preserving Buffer Cache State] (insights.oetiker.ch)</ref>

Einzelnachweise
##############################################################################

Weitere Informationen
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* [http://duartes.org/gustavo/blog/post/page-cache-the-affair-between-memory-and-files Page Cache, the Affair Between Memory and Files] (Blog)
* [http://panoskrt.wordpress.com/2009/10/27/linux-buffer-cache-state/ Linux buffer cache state] (Blog)
* [http://www.westnet.com/~gsmith/content/linux-pdflush.htm The Linux Page Cache and pdflush: Theory of Operation and Tuning for Write-Heavy Loads] (Last update 8/08/2007)
* [http://www.linuxinsight.com/proc_sys_vm_drop_caches.html drop_caches] (linuxinsight.com)
* [http://www.linuxinsight.com/ols2005_examining_linux_2_6_page_cache_performance.html Examining Linux 2.6 Page-Cache Performance] (linuxinsight.com)
* [http://en.wikipedia.org/wiki/Page_cache Page cache] (en.wikipedia.org)

sar: Linux Performance Aufzeichnung und Auswertung mit sar
==============================================================================

**sar** dient unter Linux der Aufzeichnung und Auswertung von unterschiedlichen Informationen zur Systemaktivität. Bei Performance-Problemen erlaubt sar auch noch im Nachhinein die Lastwerte der unterschiedlichen Subsysteme (CPU, Memory, Disk, Interrupts, Netz-Interfaces, ...) zu analysieren und so das Problem einzugrenzen.

Aufbau
##############################################################################

sar ist Teil von sysstat.<ref>[http://pagesperso-orange.fr/sebastien.godard/documentation.html sysstat Dokumentation]</ref> sar selbst besteht aus folgenden Teilen:
* [http://pagesperso-orange.fr/sebastien.godard/man_sar.html sar(1)] - zeigt die aufgezeichneten Daten an.
* [http://pagesperso-orange.fr/sebastien.godard/man_sadc.html sadc(8)] - system activity data collector, zeichnet die Daten zur Systemaktivität in einem binären Format auf.
* [http://pagesperso-orange.fr/sebastien.godard/man_sa1.html sa1(8)] - BASH Skript, nutzt sadc im Hintergrund, wird alle 10 Minuten per Cron aufgerufen (über /etc/cron.d/sysstat).
* [http://pagesperso-orange.fr/sebastien.godard/man_sa2.html sa2(8)] - BASH Skript, wird zum Schreiben des täglichen Reports verwendet und per Cron 1x täglich aufgerufen (über /etc/cron.d/sysstat).
* [http://pagesperso-orange.fr/sebastien.godard/man_sadf.html sadf(1)] - zeigt die aufgezeichneten Daten an, bietet im Gegensatz zu sar unterschiedliche Formate (CSV, XML, usw.).

Versionen
##############################################################################

Das sysstat Paket (das u.a. sar enthält) wird laufend weiter entwickelt. 
Bei den Versionsnummern wird folgendes Schema verwendet:

<ref>[http://pagesperso-orange.fr/sebastien.godard/download.html sysstat Download Seite]</ref>

* Versionsnummern x.0 kennzeichnen jeweils eine stabile Version
* Versionsnummern x.1 kennzeichnen jeweils eine Entwickler-Version

Neuere Versionen bieten zumeist zusätzliche Features. Mit Version 7.1.6 
wurde die Funktion von sar -x und sar -X in ein eigenes Kommando 
([http://pagesperso-orange.fr/sebastien.godard/man_pidstat.html pidstat]) 
ausgelagert.<ref>[http://pagesperso-orange.fr/sebastien.godard/old_changelog.html 
sysstat Changelog bis Version 8.0.4], 
[http://pagesperso-orange.fr/sebastien.godard/changelog.html sysstat 
Changelog ab Version 8.1.1]</ref>

Installation
##############################################################################

Wie erwähnt ist sar Teil des sysstat Pakets, das bei zahlreichen 
Distributionen bereits enthalten ist und je nach Distribution entweder 
über ``yum install sysstat`` oder ``apt-get install sysstat`` einfach 
installiert werden kann. Dabei werden zumeist auch gleich die Cronjobs in 
*/etc/cron.d/sysstat* angelegt.

==Funktionsweise der Aufzeichnung==
##############################################################################

sa1 und sa2 speichern die aufgezeichneten Daten im Verzeichnis 
*/var/log/sa/* (RHEL/CentOS) bzw. */var/log/sysstat/* (Debian) ab. Die Daten
werden in der Standardkonfiguration rückwirkend für eine Woche vorgehalten. 
Die Dauer kann auf bis zu 28 Tage erhöht werden (in */etc/sysconfig/sysstat* 
unter RHEL/CentOS bzw. in */etc/sysstat/config* unter Debian 4.0 und 
*/etc/sysstat/sysstat* unter Debian 5.0). Ubuntu 12.04 unterstützt 
die Aufzeichnung auch über 28 Tage, allerdings werden in diesem Fall 
dann mehrere Verzeichnisse verwendet (für jedes Monat ein Verzeichnis). 

**sar Aufzeichnung in Debian aktivieren:** Um die automatische Aufzeichnung unter Debian zu aktivieren, muss zuerst der Parameter "ENABLED" in der Datei /etc/default/sysstat auf "true" gesetzt werden. Dieser ist per Default auf "false" gesetzt.

Hier dazu ein Beispiel:
<pre>
[root@testserver ~]# cat /etc/sysconfig/sysstat
# How long to keep log files (days), maximum is a month
HISTORY=7
[root@testserver ~]# cat /etc/cron.d/sysstat 
# run system activity accounting tool every 10 minutes
*/10 * * * * root /usr/lib64/sa/sa1 1 1
# generate a daily summary of process accounting at 23:53
53 23 * * * root /usr/lib64/sa/sa2 -A

[root@testserver ~]# ls -l /var/log/sa/
total 8952
-rw-r--r--  1 root root 410352 May 18 23:50 sa18
-rw-r--r--  1 root root 410352 May 19 23:50 sa19
-rw-r--r--  1 root root 410352 May 20 23:50 sa20
-rw-r--r--  1 root root 410352 May 21 23:50 sa21
-rw-r--r--  1 root root 410352 May 22 23:50 sa22
-rw-r--r--  1 root root 410352 May 23 23:50 sa23
-rw-r--r--  1 root root 410352 May 24 23:50 sa24
-rw-r--r--  1 root root 410352 May 25 23:50 sa25
-rw-r--r--  1 root root 253712 May 26 14:40 sa26
-rw-r--r--  1 root root 611085 May 17 23:53 sar17
-rw-r--r--  1 root root 611129 May 18 23:53 sar18
-rw-r--r--  1 root root 611127 May 19 23:53 sar19
-rw-r--r--  1 root root 611127 May 20 23:53 sar20
-rw-r--r--  1 root root 611085 May 21 23:53 sar21
-rw-r--r--  1 root root 611100 May 22 23:53 sar22
-rw-r--r--  1 root root 611085 May 23 23:53 sar23
-rw-r--r--  1 root root 611085 May 24 23:53 sar24
-rw-r--r--  1 root root 611125 May 25 23:53 sar25
[root@testserver ~]#
</pre>

Anzeige der aufgezeichneten Daten mit sar
##############################################################################

Eingrenzung des Anzeigezeitraumes
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

sar kann entweder die Daten des aktuellen Tages anzeigen (dazu sind keine speziellen Parameter notwendig), oder die Daten von einem bestimmten Tag (z.B. mit dem Paramter ``-f /var/log/sa/sa21``).

Weiters kann der Abfrage-Zeitraum über folgende Paramter eingegrenzt werden:
* ``-s [ hh:mm:ss ]`` - setzt die Startzeit (z.B. ``-s 07:30:00``), ohne Angabe einer konkreten Zeit wird 08:00:00 als Standard-Startzeit verwendet.
* ``-e [ hh:mm:ss ]`` - setzt die Endzeit (z.B. ``-e 19:30:00``), ohne Angabe einer konkreten Zeit wird 18:00:00 als Standard-Endzeit verwendet.

Parameter für die unterschiedlichen Subsysteme
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Hier sind einige Beispiele zur Anzeige unterschiedlicher Werte (die Werte stammen von einem System mit CentOS 4.7 mit sysstat Version 5.0.5).

====Anzeige der CPU Auslastung: sar -P ALL====

Hier werden die Werte aller CPUs (``-P ALL``) vom 25. des laufenden Monats (``-f /var/log/sa/sa25``) angezeigt.
<pre>
[root@testserver ~]# sar -P ALL -f /var/log/sa/sa25
Linux 2.6.9-023stab048.6-smp (testserver.thomas-krenn.com) 	05/25/09

00:00:01          CPU     %user     %nice   %system   %iowait     %idle
00:10:01          all      9.63      0.00      3.81      6.20     80.36
00:10:01            0      8.69      0.00      3.63      7.18     80.50
00:10:01            1      9.28      0.00      3.96      4.47     82.29
00:10:01            2     10.22      0.00      3.86      6.31     79.62
00:10:01            3      9.84      0.00      3.66      6.91     79.59
00:10:01            4      9.86      0.00      3.94      5.28     80.92
00:10:01            5      9.97      0.00      3.84      6.37     79.81
00:10:01            6      9.90      0.00      4.04      7.21     78.85
00:10:01            7      9.32      0.00      3.54      5.83     81.32
[...]
Average:          all      9.21      0.00      1.68      0.27     88.84
Average:            0      9.03      0.00      1.70      0.31     88.97
Average:            1      9.20      0.00      1.67      0.25     88.88
Average:            2      9.45      0.00      1.70      0.28     88.56
Average:            3      9.84      0.00      1.68      0.26     88.22
Average:            4      9.23      0.00      1.68      0.26     88.84
Average:            5      9.05      0.00      1.68      0.27     89.00
Average:            6      8.90      0.00      1.68      0.29     89.14
Average:            7      8.97      0.00      1.66      0.28     89.10
[root@testserver ~]#
</pre>

====Anzeige der RAM Auslastung: sar -r====
Hier werden die RAM Werte (``-r``) des aktuellen Tages angezeigt. Ein Wert von annähernd 100 für %memused ist dabei durchaus positiv (sofern kein Swappen erforderlich ist). Siehe dazu auch das [[Cache Einstellungen von RAID Controllern und Festplatten#Cache im Betriebssystem|Kapitel Cache im Betriebssystem des Artikels Cache Einstellungen von RAID Controllern und Festplatten]].
<pre>
[root@testserver ~]# sar -r
Linux 2.6.9-023stab048.6-smp (testserver.thomas-krenn.com)   05/26/09

00:00:01    kbmemfree kbmemused  %memused kbbuffers  kbcached kbswpfree kbswpused  %swpused  kbswpcad
00:10:01        67248  16292680     99.59     42320  12502420   2047648       628      0.03         0
00:20:01       129776  16230152     99.21     47020  12527100   2047648       628      0.03         0
[...]
14:50:01       638680  15721248     96.10    158392   6953128   2047648       628      0.03         0
15:00:01       646272  15713656     96.05    159880   6966980   2047648       628      0.03         0
15:10:01        59784  16300144     99.63    132044   6565036   2047648       628      0.03         0
Average:       554290  15805638     96.61    124889  10992802   2047648       628      0.03         0
[root@testserver ~]# 
</pre>

====Anzeige der load average: sar -q====
Hier werden die load average Werte (``-q``) des aktuellen Tages angezeigt. 
<pre>
[root@testserver ~]# sar -q 
Linux 2.6.9-023stab048.6-smp (testserver.thomas-krenn.com) 	05/26/09

00:00:01      runq-sz  plist-sz   ldavg-1   ldavg-5  ldavg-15
00:10:01            0       590      1.62      1.89      1.21
00:20:01           11       574      0.24      0.63      0.86
[...]
15:10:01            7       671      2.36      2.26      2.03
15:20:01            9       651      1.85      1.96      1.99
Average:            7       600      1.02      1.04      1.00
[root@testserver ~]# 
</pre>

====Anzeige der I/O Transfer Rate: sar -b====
Hier werden die I/O Transfer Raten (``-b``) des aktuellen Tages angezeigt. 
<pre>
[root@testserver ~]# sar -b
Linux 2.6.9-023stab048.6-smp (testserver.thomas-krenn.com) 	05/26/09

00:00:01          tps      rtps      wtps   bread/s   bwrtn/s
00:10:01       402.21    197.20    205.00  35133.80  27616.04
00:20:01        66.61     56.36     10.24   2034.96    288.93
[...]
15:20:01        72.43      6.54     65.89    176.29   2720.35
15:30:01        73.28      6.08     67.20    399.04   3516.21
Average:        40.21      5.57     34.64    585.00   1561.78
[root@testserver ~]# 
</pre>
**tps** - Total number of transfers per second that were issued to physical devices. A transfer is an I/O request to a physical device. Multiple logical requests can be combined into a single I/O request to the device. A transfer is of indeterminate size

**rtps** - Total number of read requests per second issued to physical devices

**wtps** - Total number of write requests per second issued to physical devices

**bread/s** - Total amount of data read from the devices in blocks per second. Blocks are equivalent to sectors with 2.4 kernels and newer and therefore have a size of 512 bytes. With older kernels, a block is of indeterminate size.

**bwrtn/s** - Total amount of data written to devices in blocks per second.

====Anzeige der Netz Statistik: sar -n DEV====
Hier werden die Netz Statistik Werte (``-n DEV``) des aktuellen Tages angezeigt. 
<pre>
[root@testserver ~]# sar -n DEV
Linux 2.6.9-023stab048.6-smp (testserver.thomas-krenn.com) 	05/26/09

00:00:01        IFACE   rxpck/s   txpck/s   rxbyt/s   txbyt/s   rxcmp/s   txcmp/s  rxmcst/s
00:10:01           lo      0.82      0.82    177.69    177.69      0.00      0.00      0.00
00:10:01         eth0     31.56     40.21   4537.41  35891.07      0.00      0.00      0.00
00:10:01         eth1   4876.29   9919.22 451754.61 14927283.09      0.00      0.00      0.00
00:10:01       venet0     52.88     44.19  46547.68  15279.15      0.00      0.00      0.00
[...]
15:30:01           lo      0.82      0.82    176.68    176.68      0.00      0.00      0.00
15:30:01         eth0    113.08    155.70  17825.90 188643.77      0.00      0.00      0.00
15:30:01         eth1    634.80   1268.92  58495.76 1888004.02      0.00      0.00      0.00
15:30:01       venet0    189.86    147.08 213164.21  42532.10      0.00      0.00      0.00
Average:           lo      0.82      0.82    177.10    177.10      0.00      0.00      0.00
Average:         eth0     56.42     78.90   9104.66  94428.28      0.00      0.00      0.00
Average:         eth1    281.59    559.55  25919.74 830592.96      0.00      0.00      0.00
Average:       venet0     87.26     64.75  99974.13  14933.05      0.00      0.00      0.00
[root@testserver ~]# 
</pre>

====gesammelte Anzeige aller Werte====
Mit ``sar -A`` können gesammelt alle Werte (CPU, RAM, etc.) angezeigt werden.

Tools zur grafischen Auswertung
##############################################################################

Die folgenden Tools stellen die mit sar aufgezeichneten Werte grafisch dar:
* [http://sourceforge.net/projects/sarface/ sarface]
* kSar: [[Linux Performance Auswertung mit kSar]]
* [https://github.com/sqrt529/sar2png sar2png] (Perl Skript zum Erstellen von PNG Grafiken aus sar Daten, getestet unter GNU/Linux und Solaris 10)
* weitere Tools siehe [http://pagesperso-orange.fr/sebastien.godard/faq.html Sysstat FAQs] Frage 2.11. <i>Do you know a tool which can graphically plot the data collected by sar?</i>

top: Linux-Tool top
==============================================================================

Dieser Artikel beschreibt den Verwendungszweck des Unix-Tools **top** und erläutert wichtige **Filter- und Kommandofunktionen** für die **Prozessüberwachung**.

Information
##############################################################################

top ist ein kleines Unix-Programm, das eine Liste der aktuell laufenden Prozesse des Systems ausgibt und diese - anders als das Tool [[Ps und pstree | ps]] - per default alle 3 Sekunden aktualisiert. Darüber hinaus zeigt das Programm im oberen Feld auch die Auslastung der CPU, den aktuell benutzten und noch freien RAM sowie etliche andere nützliche Informationen an. Sehr wertvoll ist das kleine Programm vor allem für Systemadministratoren, da sich diese so schnell einen genauen Überblick über Benutzer und deren Prozesse verschaffen können.

Installation
##############################################################################

Das Programm top ist standardmäßig in den meisten Linux-Distributionen enthalten.

Beispiel für Ausgabe
##############################################################################

<pre>
top - 14:29:44 up  6:20,  2 users,  load average: 0.24, 0.33, 0.39
Tasks: 200 total,   1 running, 197 sleeping,   2 stopped,   0 zombie
Cpu(s):  0.2%us,  0.3%sy,  0.0%ni, 99.5%id,  0.0%wa,  0.0%hi,  0.0%si,  0.0%st
Mem:   3916992k total,  1615976k used,  2301016k free,   135152k buffers
Swap:  2999288k total,        0k used,  2999288k free,   680008k cached

  PID USER      PR  NI  VIRT  RES  SHR S %CPU %MEM    TIME+  COMMAND            
 1382 root      20   0  163m  67m  21m S    1  1.8  12:24.83 Xorg               
 1617 amarek    20   0  296m  57m  21m S    0  1.5   2:47.17 compiz             
 1868 amarek    20   0  764m  52m  25m S    0  1.4   1:05.92 pidgin             
 2787 amarek    20   0  217m  20m  11m S    0  0.5   0:12.35 gnome-terminal     
 4308 amarek    20   0 19224 1476 1064 R    0  0.0   0:00.27 top                
    1 root      20   0 23812 1976 1276 S    0  0.1   0:01.10 init               
    2 root      20   0     0    0    0 S    0  0.0   0:00.01 kthreadd           
    3 root      RT   0     0    0    0 S    0  0.0   0:00.00 migration/0  
[...]
</pre>

Informationen der Kopfzeilen
##############################################################################

# **Uptime:** <br> Diese Zeile zeigt die Zeitspanne an, seit der das System online ist. Dahinter werden die Load Averages von 1, 5 und 15 Minuten angegeben
# **Tasks:** <br> Hier wird die Anzahl an Prozessen angezeigt, die zum letzten Zeitpunkt der Aktualisierung aktiv waren. Ausserdem wird angezeigt, wieviele Prozesse sich in den Status running, sleeping, stopped oder undead befinden
# **CPU states:** <br> Zeigt die Auslastung der CPU in Prozent an
# **Mem:** <br> Statistik über die Auslastung des Hauptspeichers
# **Swap:** <br> Anzeige der Swap-Auslastung

Interaktive Befehle
##############################################################################

{| {{Prettytable}}
|- style="background-color: #EFEFEF; font-weight: bold;"
! Taste
! Name
! Funktion
|-
| u
| user
| Prozesse eines Benutzers filtern
|-
| k
| kill
| Prozess beenden (PID... + Signalnummer (Std: 15)
|-
| r
| renice
| Prozess Nice−Wert setzen (PID + Nice−Wert)
|-
| d
| delay
| Update−Intervall in Sekunden setzen (default: 3 Sek)
|-
| n
| number
| Maximale Anzahl an angezeigten Prozessen
|-
| N
| Number
| Sortieren nach PID
|-
| P
| Percent
| Sortieren nach CPU-Auslastung
|-
| M
| Memory
| Sortieren nach RAM-Auslastung
|-
| q
| quit
| Beenden von top
|-
| h
| help
| Hilfe anzeigen
|}

== Manueller Refresh der Ausgabe ==
##############################################################################

Durch Drücken der Leertaste kann die Bildschirmausgabe erneuert werden.

== Erweiterte Alternative zu top ==
##############################################################################

Ein an top angelehntes, aber benutzerfreundlicheres Tool ist htop.

Vorteile
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* Graphische Darstellung der CPU-, RAM- und Swap-Auslastung
* Prozess-Signale lassen sich über einfache Tastenkombinationen versenden

Installation von htop
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

htop kann über das gleichnamige Paket installiert werden, z.B. unter Debian:
<pre>
aptitude install htop
</pre>

Weitere Informationen
##############################################################################

* [http://de.wikipedia.org/wiki/Top_%28Unix%29 top](de.wikipedia.org)
* [http://linux.die.net/man/1/top Linux Manpage top] (die.net)
* [http://linux.about.com/od/commands/l/blcmdl1_top.htm Unix Command top] (about.com)

.. Seealso:: 
   - :ref:`Wenn mal was schief geht - Linux Performance Analyse <Section_Linux_Performance_Analyse>`
   - :ref:`Wenn mal was schief geht - Root Cause Analyse <Section_Root_Cause_Analyse>`
   - :ref:`Wenn mal was schief geht - UAC (Unix-like Artifacts Collector) <Section_UAC>`

.....

.. Rubric:: Footnotes

.. [1]
   Netflix Technology Blog: Linux Performance Analysis in 60,000 Milliseconds
   http://techblog.netflix.com/2015/11/linux-performance-analysis-in-60s.html
