#!/usr/bin/env bash
# Author: Pierre Gronau <Pierre.Gronau@ndaal.eu>
# Copyright 2025
# License: All content is licensed under the terms of the <Apache 2.0>
# Developed on: Debian 12.9x; macOS Sequoia x86 architecture
# Tested on: Debian 12.9x; macOS Sequoia x86 architecture
#
set -o errexit
set -o errtrace
set -o nounset
set -o pipefail
# set -o xtrace

# nosemgrep: ifs-tampering
IFS=$'\n\t'

# Error trapping function
# shellcheck disable=SC2317 
error_handler() {
    local line_number="${1}"
    local error_code="${2}"
    local last_command="${BASH_COMMAND}"
    printf "Error: Error occurred in line %s (error code: %s)\n" "${line_number}" "${error_code}"
    printf "Error: Failed command: %s\n" "${last_command}"
}

# Set the error trap
trap 'error_handler ${LINENO} $?' ERR

cleanup() {
    trap - SIGINT SIGTERM ERR EXIT
    printf "%b\n" "\nInfo: Cleanup is running ..."
    # Additional cleanup tasks can be added here if needed
    rm -f -v ./*.tmp
    printf "%b\n" "\nInfo: Cleanup finished ..."
}

trap cleanup SIGINT SIGTERM EXIT

if [[ "$(uname)" == "Darwin" ]]; then
    HOMEDIR="Users"
elif [[ "$(uname)" == "Linux" ]]; then
    HOMEDIR="home"
else
    printf "Error: Unsupported operating system: %s\n" "$(uname)"
    exit 1
fi
readonly HOMEDIR

# Under Linux you use `home` under macOS `Users`
printf "Info: Home Directory: %s\n" "${HOMEDIR}"

USERSCRIPT="cloud"
# Your user! In which context it SHOULD run
printf "Info: User script: %s\n" "${USERSCRIPT}"

VERSION="0.5.0"
readonly VERSION
printf "Info: Current version: %s\n" "${VERSION}"

LOG_FILE="/${HOMEDIR}/${USERSCRIPT}/prompting.log"
readonly LOG_FILE 
printf "Info: log file with path: %s\n" "${LOG_FILE}"
if [[ ! -f "${LOG_FILE}" ]]; then
    printf "Info: Log file does not exist. Creating it now.\n"
    touch "${LOG_FILE}" || {
        printf "Error: Failed to create log file at %s\n" "${LOG_FILE}"
        exit 1
    }
else
    printf "Info: Log file already exists at %s\n" "${LOG_FILE}"
fi

log() {
    printf "%s - %s\n" "$(date '+%Y-%m-%d %H:%M:%S')" "${1}" | tee -a "${LOG_FILE}"
}

log "starting script ..."

# Function to check if a command is available
check_command() {
    if ! command -v "${1}" &>/dev/null; then
        printf "Error: %s is not installed or not in PATH. Please install it and try again.\n" "${1}"
        return 1
    fi

    # Success case
    return 0
}

# Check for required commands
check_command "git" || exit 1
check_command "mktemp" || exit 1
check_command "realpath" || exit 1

# Create a temporary directory in /tmp
TEMP_DIR="$(mktemp -d /tmp/mytempdir.XXXXXX)"
readonly TEMP_DIR
printf "Info: Current TEMP_DIR: %s\n" "${TEMP_DIR}"

# Create a secure array with the following URLs
declare -a REPOS=(
    "https://gitlab.com/vPierre/ndaal_public_ansible_role_auditing_ssh"
    "https://gitlab.com/vPierre/ndaal_public_ansible_role_hardening_ssh"
    "https://gitlab.com/vPierre/ndaal_public_ansible_role_podman_red_hat_9_derivates"
    "https://gitlab.com/vPierre/ndaal_public_ansible_role_auditd_red_hat_9_derivates"
    "https://gitlab.com/vPierre/ndaal_public_ansible_role_journald_red_hat_9_derivates"
    "https://gitlab.com/ndaal_open_source/ndaal_public_ansible_role_gitleaks_red_hat_9_derivates"
    "https://gitlab.com/ndaal_open_source/ndaal_public_ansible_role_git_git-lfs_wget_curl_red_hat_9_derivates"
    "https://gitlab.com/ndaal_open_source/ndaal_public_ansible_role_clamav_red_hat_9_derivates"
    "https://gitlab.com/ndaal_open_source/ndaal_public_ansible_role_yara_red_hat_9_derivates"
    "https://gitlab.com/ndaal_open_source/ndaal_public_ansible_role_yara-x_red_hat_9_derivates"
    "https://gitlab.com/vPierre/ndaal_public_ansible_role_uac_red_hat_9_derivates"
    "https://gitlab.com/ndaal_open_source/ndaal_public_ansible_role_fail2ban_debian_12_derivates"
    "https://gitlab.com/ndaal_open_source/ndaal_public_ansible_role_clamav_debian_12_derivates"
    "https://gitlab.com/ndaal_open_source/ndaal_public_ansible_role_cis_hardening_debian_12_derivates"
    "https://gitlab.com/ndaal_open_source/ndaal_public_ansible_role_aide_red_hat_9_derivates"
)

# Git clone these repos in ${TEMP_DIR}
for REPO in "${REPOS[@]}"; do
    printf "Info: Cloning repository: %s\n" "${REPO}"
    git clone "${REPO}" "${TEMP_DIR}/$(basename "${REPO}")" || true
done

# Create a unique content file for each repo
for REPO in "${REPOS[@]}"; do
    REPO_NAME="$(basename "${REPO}")"
    TOOLS_DIR="${TEMP_DIR}/${REPO_NAME}/tools"
    
    if [[ -d "${TOOLS_DIR}" ]]; then
        printf "Info: Listing contents of tools directory for %s:\n" "${REPO_NAME}"
        ls -1 "${TOOLS_DIR}"

        # Use the first directory found within tools for further processing
        FIRST_DIR="$(find "${TOOLS_DIR}" -mindepth 1 -maxdepth 1 -type d | head -n 1)"
        if [[ -n "${FIRST_DIR}" ]]; then
            printf "Info: First directory found for processing: %s\n" "${FIRST_DIR}"

            # Create a unique content file for this repo
            FILE_NAME="ndaal_public_ansible_role_${REPO_NAME#ndaal_public_ansible_role_}.txt"
            touch "${FILE_NAME}"
            printf "Info: Created file: %s\n" "${FILE_NAME}"

            # Enhanced array of the ansible role directories
            declare -a ANSIBLE_DIRS=(
                "molecule/default"
                "tasks"
                "handlers"
                "files"
                "defaults"
                "templates"
                "meta"
                "vars"
            )

            # Iterate over the ANSIBLE_DIRS array
            for DIR in "${ANSIBLE_DIRS[@]}"; do
                if [[ -d "${FIRST_DIR}/${DIR}" ]]; then
                    echo "${DIR}" >> "${FILE_NAME}"

                    # Check for main.yml file
                    if [[ -f "${FIRST_DIR}/${DIR}/main.yml" ]]; then
                        printf "Contents of main.yml in %s:\n" "${DIR}" >> "${FILE_NAME}"
                        cat "${FIRST_DIR}/${DIR}/main.yml" >> "${FILE_NAME}"
                    fi

                    # Check for YAML files
                    if [[ "${DIR}" == "molecule/default" || "${DIR}" == "tasks" || "${DIR}" == "vars" ]]; then
                        for YAML_FILE in "${FIRST_DIR}/${DIR}"/*.yml "${FIRST_DIR}/${DIR}"/*.yaml; do
                            if [[ -f "${YAML_FILE}" ]]; then
                                printf "Contents of %s:\n" "${YAML_FILE}" >> "${FILE_NAME}"
                                cat "${YAML_FILE}" >> "${FILE_NAME}"
                            fi
                        done
                    fi

                    # Check for Jinja2 template files
                    if [[ "${DIR}" == "templates" ]]; then
                        for J2_FILE in "${FIRST_DIR}/${DIR}"/*.j2; do
                            if [[ -f "${J2_FILE}" ]]; then
                                printf "Contents of %s:\n" "${J2_FILE}" >> "${FILE_NAME}"
                                cat "${J2_FILE}" >> "${FILE_NAME}"
                            fi
                        done
                    fi
                else
                    printf "Warning: Directory %s does not exist in %s\n" "${DIR}" "${FIRST_DIR}"
                fi
            done
        else
            printf "Warning: No directories found in tools for %s\n" "${REPO_NAME}"
        fi
    else
        printf "Warning: Tools directory does not exist in %s\n" "${REPO_NAME}"
    fi
done

# cat ./ndaal_public_*.txt
ls -la ./ndaal_public_*.txt

cleanup

log "stopping script ..."

script_name1="$(basename "${0}")"
printf "\nInfo: script_name1: %s\n" "${script_name1}"
script_path1="$(realpath "$(dirname "${0}")")"
printf "Info: script_path1: %s\n" "${script_path1}"
script_path_with_name="${script_path1}/${script_name1}"
printf "Info: Script path with name: %s\n" "${script_path_with_name}"
printf "Info: Script finished\n"
exit 0
