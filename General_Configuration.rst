.. image:: 
   _static/Logo.png

General Configuration
*********************************************************************

.. contents:: Content
    :depth: 3
    
Patching the current system (Mandatory)
---------------------------------------------------------------------

It is highly recommended to lift the current system state to a fully up-to date and patched system before performing the described hardening measures in this document. It is possible to check the current system for updates through either the AppStore with: Apple menu → App Store → Updates

Or issuing the following command on the command line:

.. code:: Bash

    $ softwareupdate -l 

Auto Updates (Mandatory)
---------------------------------------------------------------------

It is recommended to always have auto updates enabled to deliver latest security and software patches automatically to the system. The following command checks whether this setting is enabled or not:

.. code:: Bash

    $ defaults read /Library/Preferences/com.apple.SoftwareUpdate AutomaticallyInstallMacOSUpdates 1

If automatic updates are enabled, the return value should be 1 as displayed in the output. If the setting is not present, it can be manually set using the following command:

.. code:: Bash

    sudo defaults write /Library/Preferences/com.apple.SoftwareUpdate AutomaticallyInstallMacOSUpdates -int 1

Gatekeeper (Mandatory)
---------------------------------------------------------------------

Gatekeeper will check if the Application is signed with a valid Apple Certificate. Checking Applications for the Signature is possible with:

.. code:: Bash

    spctl –a /Applications/test.app

If you want to add it to Gatekeeper you can use:

.. code:: Bash

    spctl --add test.app

or simply:

right click → open Note: Sometimes it will still deny opening the Application. You can then force the start:
System Preferences → Security & Privacy → General
Open "Application"
Enforcing Code Signing (Mandatory)

Edit the following line in /etc/sysctl.conf:

.. code:: Bash

    vm.cs_force_kill=1  # Kill process if invalidated
    vm.cs_force_hard=1  # Fail operation if invalidated
    vm.cs_all_vnodes=1  # Apply on all Vnodes
    vm.cs_enforcement=1 # Globally apply code signing enforcement

.. Notes:: This change will require to disable System Integrity Protection (Remember to Enable it after Hardening). On a fresh installed system it is possible 
           that sysctl.conf is not existent. If so, you can create it and reboot the system.

Disable Creation of Metadata Files (Mandatory)
---------------------------------------------------------------------

It is recommended to prohibit Mac OS from creating temporary files on remote volumes such as network storage or USB storage. The following commands will prevent this behavior:

.. code:: Bash

    defaults write com.apple.desktopservices DSDontWriteNetworkStores -bool true
    defaults write com.apple.desktopservices DSDontWriteUSBStores -bool true
    Disable saving to iCloud (Mandatory)

The following command prevent Mac OS from saving files directly to any iCloud server:

.. code:: Bash

    sudo defaults write NSGlobalDomain NSDocumentSaveNewDocumentsToCloud -bool false

Disable Diagnostics (Mandatory)
---------------------------------------------------------------------

To avoid sending data to Apple or App Developers, disable the following:

System Preferences → Security & Privacy → Privacy → Diagnostics & Usage
Uncheck “Send diagnostic & usage data to Apple”
Uncheck “Share crash data with app developers”

Disable Handoff (Mandatory)
---------------------------------------------------------------------

Apples Handoff is a feature to keep your workspaces in sync but it does require to send data to Apple which is recommended to be disabled.

System Preferences → General
Uncheck “Allow Handoff between this Mac and your iCloud devices”
Set “Recent Items” to none. You will find this inside of the dropdown box right above the Handoff option

Require Administrator Password (Mandatory)
---------------------------------------------------------------------

It is recommended to always require the administrator password to access system settings.

Screensaver and Un-locking (Mandatory)
---------------------------------------------------------------------

It is recommended to automatically lock your screen when inactive.

System Preferences → Desktop & Screensaver → Screensaver
Adjust the timeframe to your needs but we recommend setting it to 5 minutes.
System Preferences → Security & Privacy → General
Set "Require password immediately after sleep or screen saver begins"

Filename Extensions (Mandatory)
---------------------------------------------------------------------

To be always clear what sort of file you´re processing, it is recommended to turn on displaying Filename extensions.

Open Finder → Settings → Advanced
Set "Show all filename extensions"

Prevent Safari from Opening Known File Types (Mandatory)
If you are using Safari as your main Browser, it is recommended to prevent Safari from opening known files after downloading.

Open Safari → Preferences → General
Unset "Open safe files after downloading"

Set Strict Global Umask (Optional)
---------------------------------------------------------------------

The Strict Global Umask defaults the Permission of any File or Directory that is created by a user in the future. You can adjust the Global Umask with the following command:

.. code:: Bash

    sudo launchctl config system umask 027

..  Notes:: This might break the installation of additional Software that relies on a less restrict umask.

Disable Remote Apple Events (Mandatory)
---------------------------------------------------------------------

It is recommended to disable (Default) the Remote Apple Event service. This service can be used to perform operations on software over the network to another Mac. Hence, it should be always disabled. The following command line checks whether Remote Apple Events is enabled or not:

.. code:: Bash

    sudo systemsetup -getremoteappleevents

Ensure that the output is:

.. code:: Bash

Remote Apple Events: Off

If it is enabled the following command will disable it:

.. code:: Bash

    sudo systemsetup -setremoteappleevents off

Disable Core Dumps (Mandatory)
---------------------------------------------------------------------

To limit Kernel Information leaks, disable the core dumping capability. Edit the following line in /etc/sysctl.conf:

.. code:: Bash

    kern.coredump=0

..  Notes:: This change will require to disable System Integrity Protection (Remember to Enable it after Hardening). On a freshly installed system it is      
            possible that sysctl.conf is not existent. If so you can create it and reboot the system.

Since Kerneldumps can be helpful for debugging you can enable it temporarily with:

.. code:: Bash

    sudo sysctl -w kern.coredump=1

Disable Wake on Lan (Mandatory)
---------------------------------------------------------------------

System Preferences → Energy Saver
Unset "Wake for network access"

Disable Apple File Protocol (AFP) (Mandatory)
---------------------------------------------------------------------

Apples equivalent to SMB. It is disabled by default on OS X 10.11. You can verify this under:

System Preferences → Sharing
Select "File Sharing" → Options
Unset "Share Files and folders using AFP"
Note: It is also possible to disable this via the Command-Line:

.. code:: Bash

    sudo launchctl unload -w /System/Library/LaunchAgents/AppleFileServer.plist

Disable Sharing (Mandatory)
---------------------------------------------------------------------

Sharing is disabled on default. You can verify this here:

System Preferences → Sharing
Unset everything which is not needed

Enable Network Time Synchronization via NTP (Mandatory)
---------------------------------------------------------------------

To ensure that your clock is always correct and not corrupted (e.g. important for Log-Files), use the following Commands:

.. code:: Bash

    sudo systemsetup -setnetworktimeserver "0.de.pool.ntp.org;1.de.pool.ntp.org;2.de.pool.ntp.org"
    sudo systemsetup -setusingnetworktime on
    sudo systemsetup -settimezone "Europe/Berlin"
    echo "restrict default ignore" >> /etc/ntp.conf

    
