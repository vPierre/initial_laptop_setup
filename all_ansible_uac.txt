File: ./tools/ansible/roles/Unix_Artifacts_Collector/vars/main.yml
---
---
uac_dir: "/home/{{ ansible_user }}/uac"
results_dir: "/home/{{ ansible_user }}/uac/uac_results"
---

File: ./tools/ansible/roles/Unix_Artifacts_Collector/tasks/main.yml
---
---
- name: Create a temporary UAC directory
  ansible.builtin.file:
    path: "{{ uac_dir }}"
    state: directory
    mode: '0755'

- name: Create a temporary uac_results directory within uac directory
  ansible.builtin.file:
    path: "{{ results_dir }}"
    state: directory
    mode: '0755'

- name: Download latest UAC release from GitHub
  ansible.builtin.get_url:
    url: "{{ uac_src_url }}"
    dest: "{{ uac_dir }}/uac.tar.gz"
    mode: '0755'

- name: Extract UAC tarball
  ansible.builtin.unarchive:
    src: "{{ uac_dir }}/uac.tar.gz"
    dest: "{{ uac_dir }}"
    remote_src: true

- name: Run full scan with UAC
  ansible.builtin.command:
    cmd: "./uac -p full {{ results_dir }}"
  args:
    chdir: "{{ uac_dir }}/uac-{{ uac_version }}/"
  register: result
  changed_when: "'scan completed' in result.stdout"

- name: Find all files in a folder
  ansible.builtin.find:
    paths: "{{ results_dir }}"
    recurse: true
  register: files_found

- name: Fetch results to Ansible controller
  ansible.builtin.fetch:
    src: "{{ item.path }}"
    dest: "{{ log_dir }}/"
    flat: true
  with_items: "{{ files_found.files }}"
  become: false

- name: Delete complete UAC dir
  ansible.builtin.file:
    path: "{{ uac_dir }}"
    state: absent
---

File: ./tools/ansible/roles/Unix_Artifacts_Collector/meta/main.yml
---
galaxy_info:
  author: Ayesha Shafqat, Pierre Gronau
  description: Ansible role to install UAC tool, run full scan and fetch reports.
  company: ndaal, Cologne
  license: Apache 2.0
  min_ansible_version: "2.9"
  platforms:
    - name: Debian
      versions:
        - all
  galaxy_tags:
    - uac
    - security
    - malware
    - linux
    - debian
    - ubuntu
    - redhat
    - rhel
    - centos
    - almalinux
    - rockylinux
dependencies: []
---

File: ./tools/ansible/roles/Unix_Artifacts_Collector/defaults/main.yml
---
---
uac_version: "3.0.0"
uac_src_url: "https://github.com/tclahr/uac/releases/download/v{{ uac_version }}/uac-{{ uac_version }}.tar.gz"
---

