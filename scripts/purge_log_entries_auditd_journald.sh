#!/usr/bin/env bash
# Author: Pierre Gronau <Pierre.Gronau@ndaal.eu>
# Copyright 2024, 2025
# License: All content is licensed under the terms of the <Apache 2.0>
# Developed on: Debian 12.x; macOS Sequoia x86 architecture
# Tested on: Debian 12.x; macOS Sequoia x86 architecture
#
set -o errexit
set -o errtrace
set -o nounset
set -o pipefail
# set -o xtrace

# nosemgrep: ifs-tampering
IFS=$'\n\t'

# Error trapping function
# Add this line before the error_handler function
# shellcheck disable=SC2317
error_handler() {
    local line_number="${1}"
    local error_code="${2}"
    local last_command="${BASH_COMMAND}"
    printf "Error: Error occurred in line %s (error code: %s)\n" "${line_number}" "${error_code}"
    printf "Error: Failed command: %s\n" "${last_command}"

    # Success case
    return 0
}

# Set the error trap
trap 'error_handler ${LINENO} $?' ERR

cleanup() {
    trap - SIGINT SIGTERM ERR EXIT
    printf "%b\n" "\nInfo: Cleanup is running ..."
    # Additional cleanup tasks can be added here if needed
    rm -f -v ./*.tmp
    printf "%b\n" "\nInfo: Cleanup finished ..."
}

trap cleanup SIGINT SIGTERM EXIT

# Function to check if a command is available
check_command() {
    if ! command -v "${1}" &>/dev/null; then
        printf "Error: %s is not installed or not in PATH. Please install it and try again.\n" "${1}"
        return 1
    fi

    # Success case
    return 0
}

# Check if logrotate is installed and purge all log files
if check_command "logrotate"; then
    printf "Info: Logrotate is installed. Purging all log files...\n"
    if [ -d "/var/log" ]; then
        sudo find /var/log -type f -delete
        printf "Info: All log files in /var/log have been purged.\n"
    else
        printf "Warning: /var/log directory not found. Skipping log file purge.\n"
    fi
else
    printf "Warning: Logrotate is not installed. Skipping log file purge.\n"
fi

# Check if journald is installed and purge all log entries
if check_command "journalctl"; then
    printf "Info: Journald is installed. Purging all log entries...\n"
    sudo journalctl --rotate
    sudo journalctl --vacuum-time=1s
    sudo journalctl --disk-usage
    sudo journalctl --verify
    printf "Info: All journald log entries have been purged.\n"
else
    printf "Warning: Journald is not installed. Skipping log entry purge.\n"
fi

# Check if auditd is installed and purge all log entries
if check_command "auditctl"; then
    printf "Info: Auditd is installed. Purging all log entries...\n"
    sudo auditctl -e 0
    sudo rm -f /var/log/audit/*
    sudo auditctl -e 1
    printf "Info: All auditd log entries have been purged.\n"
else
    printf "Warning: Auditd is not installed. Skipping log entry purge.\n"
fi

cleanup

script_name1="$(basename "${0}")"
printf "\nInfo: script_name1: %s\n" "${script_name1}"
script_path1="$(realpath "$(dirname "${0}")")"
printf "Info: script_path1: %s\n" "${script_path1}"
script_path_with_name="${script_path1}/${script_name1}"
printf "Info: Script path with name: %s\n" "${script_path_with_name}"
printf "Info: Script finished\n"
exit 0
