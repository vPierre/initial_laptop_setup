#!/usr/bin/env bash
# Author: Pierre Gronau <Pierre.Gronau@ndaal.eu>
# Copyright 2024
# License: All content is licensed under the terms of the <MIT License>
# Developed on: Debian 12.x; macOS Sequoia x86 architecture
# Tested on: Debian 12.x; macOS Sequoia x86 architecture
#
set -o errexit
set -o errtrace
set -o nounset
set -o pipefail
# set -o xtrace
# nosemgrep: ifs-tampering
IFS=$'\n\t'

# Error trapping function
error_handler() {
    local line_number="${1}"
    local error_code="${2}"
    local last_command="${BASH_COMMAND}"
    printf "Error: Error occurred in line %s (error code: %s)\n" "${line_number}" "${error_code}"
    printf "Error: Failed command: %s\n" "${last_command}"
}

# Set the error trap
trap 'error_handler ${LINENO} $?' ERR

cleanup() {
    trap - SIGINT SIGTERM ERR EXIT
    printf "%b\n" "\nInfo: Cleanup is running ..."
    # Additional cleanup tasks can be added here if needed
    rm -f -v ./*.tmp
    printf "%b\n" "\nInfo: Cleanup finished ..."
}

trap cleanup SIGINT SIGTERM ERR EXIT

# Function to check if a command is available
check_command() {
    if ! command -v "${1}" &>/dev/null; then
        printf "Error: %s is not installed or not in PATH. Please install it and try again.\n" "${1}"
        return 1
    fi

    # Success case
    return 0
}

# Check for required commands
check_command "git"
#check_command "vscodium"

# Determine the OS and set the settings file path accordingly
if [[ "$OSTYPE" == "darwin"* ]]; then
    # macOS
    settings_file="${HOME}/Library/Application Support/VSCodium/User/settings.json"
else
    # Assume Linux
    settings_file="${HOME}/.config/VSCodium/User/settings.json"
fi

# Check if settings.json exists
if [[ ! -f "${settings_file}" ]]; then
    printf "Error: %s does not exist. Please create it first.\n" "${settings_file}"
    exit 1
fi

# Update settings.json
printf "Info: Updating %s...\n" "${settings_file}"

# Add or update settings in settings.json
jq '. + {
    "better-readability.enable": true,
    "gitlens.enable": true,
    "bracket-pair-colorizer-2.colors": ["Gold", "Orchid", "LightSkyBlue"],
    "markdown-all-in-one.enable": true,
    "markdownlint.enable": true
}' "${settings_file}" > "${settings_file}.tmp" && mv "${settings_file}.tmp" "${settings_file}"

# Install and enable plugins
plugins=(
    #"shd101wyy.markdown-preview-enhanced"
    "eamodio.gitlens"
    "CoenraadS.bracket-pair-colorizer"
    "yzhang.markdown-all-in-one"
    "DavidAnson.vscode-markdownlint"  # Adding markdownlint plugin
)

for plugin in "${plugins[@]}"; do
    printf "Info: Installing plugin %s...\n" "${plugin}"
    vscodium --install-extension "${plugin}" || {
        printf "Error: Failed to install plugin %s.\n" "${plugin}"
        exit 1
    }
done

# Final output
cleanup

script_name1="$(basename "${0}")"
printf "\nInfo: script_name1: %s\n" "${script_name1}"
script_path1="$(realpath "$(dirname "${0}")")"
printf "Info: script_path1: %s\n" "${script_path1}"
script_path_with_name="${script_path1}/${script_name1}"
printf "Info: Script path with name: %s\n" "${script_path_with_name}"
printf "Info: Script finished\n"
exit 0
