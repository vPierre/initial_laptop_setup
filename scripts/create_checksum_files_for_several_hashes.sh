#!/usr/bin/env bash
# Author: Pierre Gronau <Pierre.Gronau@ndaal.eu>
# Copyright 2024
# License: All content is licensed under the terms of the <MIT License>
# Developed on: Debian 12.x; macOS Sequoia x86 architecture
# Tested on: Debian 12.x; macOS Sequoia x86 architecture
#
# Exit on error. Append "|| true" if you expect an error.
set -o errexit
# This is equivalent to set -e. It causes the script to exit
# immediately if any command exits with a non-zero status.
#
# Exit on error inside any functions or subshells.
set -o errtrace
# This setting ensures that the ERR trap is inherited by shell functions,
# command substitutions, and commands executed in a subshell environment.
#
# Do not allow use of undefined vars. Use ${VAR:-} to use an undefined VAR
set -o nounset
# This is equivalent to set -u. It treats unset variables as
# an error when substituting.
#
# Catch the error in case mysqldump fails (but gzip succeeds) in `mysqldump |gzip`
# https://vaneyckt.io/posts/safer_bash
set -o pipefail
# This setting causes a pipeline to return the exit status of the last command
# in the pipe that returned a non-zero status.
#
# Turn on traces, useful while debugging but commented out by default
# set -o xtrace

# Set $IFS to only newline and tab.
#
# http://www.dwheeler.com/essays/filenames-in-shell.html
# nosemgrep: ifs-tampering
IFS=$'\n\t'

# Error trapping function
error_handler() {
    local line_number="${1}"
    local error_code="${2}"
    local last_command="${BASH_COMMAND}"
    printf "Error: Error occurred in line %s (error code: %s)\n" "${line_number}" "${error_code}"
    printf "Error: Failed command: %s\n" "${last_command}"
}

# Set the error trap
trap 'error_handler ${LINENO} $?' ERR

cleanup() {
    trap - SIGINT SIGTERM ERR EXIT
    printf "%b\n" "\nInfo: Cleanup is running ..."
    # Additional cleanup tasks can be added here if needed
    rm -f -v ./*.tmp
    printf "%b\n" "\nInfo: Cleanup finished ..."
}

trap cleanup SIGINT SIGTERM ERR EXIT

create_hash_files() {
    local filename="${1}"
    local algorithms=(blake3 blake2s blake2b sha3-512 sha3-384 sha3-256 sha3-224 sha2-512 sha2-384 sha2-256 sha2-224 sha1 md5 fsb512 fsb384 fsb256 fsb224 fsb160 gost94 groestl512 groestl384 groestl256 groestl224 md4 md2 ripemd320 ripemd256 ripemd160 shabal512 shabal384 shabal256 shabal224 shabal192 sm3 streebog512 streebog256 tiger whirlpool)

    printf "Info: Starting hash generation for file: %s\n" "${filename}"
    printf "Info: Total algorithms to process: %d\n" "${#algorithms[@]}"

    for algo in "${algorithms[@]}"; do
        printf "Info: Processing %s hash for %s... " "${algo}" "${filename}"
        wholesum -a "${algo}" "${filename}" > "${filename}.${algo}"
        
        if [[ -f "${filename}.${algo}" ]]; then
            printf "Info: ✓ Created %s\n" "${filename}.${algo}"
        else
            printf "Error: ✗ Failed to create hash file\n"
        fi
    done

    printf "Info: Hash generation completed for %s\n" "${filename}"
}



create_hash_files "./scripts/macos_rust_install.sh"

cleanup

script_name1="$(basename "${0}")"
printf "\nInfo: script_name1: %s\n" "${script_name1}"
script_path1="$(realpath "$(dirname "${0}")")"
printf "Info: script_path1: %s\n" "${script_path1}"
script_path_with_name="${script_path1}/${script_name1}"
printf "Info: Script path with name: %s\n" "${script_path_with_name}"
printf "Info: Script finished\n"
exit 0
