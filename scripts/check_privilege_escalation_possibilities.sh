#!/usr/bin/env bash
# Author: Pierre Gronau <Pierre.Gronau@ndaal.eu>
# Copyright 2024
# License: All content is licensed under the terms of the <Apache 2.0>
# Developed on: Debian 12.8x; macOS Sequoia x86 architecture
# Tested on: Debian 12.8x; macOS Sequoia x86 architecture
#
set -o errexit
set -o errtrace
set -o nounset
set -o pipefail
# set -o xtrace
# nosemgrep: ifs-tampering
IFS=$'\n\t'

# Error trapping function
# Add this line before the error_handler function
# shellcheck disable=SC2317
error_handler() {
    local line_number="${1}"
    local error_code="${2}"
    local last_command="${BASH_COMMAND}"
    printf "Error: Error occurred in line %s (error code: %s)\n" "${line_number}" "${error_code}"
    printf "Error: Failed command: %s\n" "${last_command}"
}

# Set the error trap
trap 'error_handler ${LINENO} $?' ERR

cleanup() {
    trap - SIGINT SIGTERM ERR EXIT
    printf "%b\n" "\nInfo: Cleanup is running ..."
    # Additional cleanup tasks can be added here if needed
    rm -f -v ./*.tmp
    printf "%b\n" "\nInfo: Cleanup finished ..."
}

trap cleanup SIGINT SIGTERM EXIT

if [[ "$(uname)" == "Darwin" ]]; then
    HOMEDIR="Users"
    readonly HOMEDIR
elif [[ "$(uname)" == "Linux" ]]; then
    HOMEDIR="home"
    readonly HOMEDIR
else
    printf "Error: Unsupported operating system: %s\n" "$(uname)"
    exit 1
fi

printf "Info: Home Directory: %s\n" "${HOMEDIR}"

USERSCRIPT="cloud"
printf "Info: User script: %s\n" "${USERSCRIPT}"

VERSION="0.5.0"
readonly VERSION
printf "Info: Current version: %s\n" "${VERSION}"

LOG_FILE="/${HOMEDIR}/${USERSCRIPT}/prompting.log"
readonly LOG_FILE
printf "Info: log file with path: %s\n" "${LOG_FILE}"

if [[ ! -f "${LOG_FILE}" ]]; then
    printf "Info: Log file does not exist. Creating it now.\n"
    touch "${LOG_FILE}" || {
        printf "Error: Failed to create log file at %s\n" "${LOG_FILE}"
        exit 1
    }
else
    printf "Info: Log file already exists at %s\n" "${LOG_FILE}"
fi

log() {
    printf "%s - %s\n" "$(date '+%Y-%m-%d %H:%M:%S')" "${1}" | tee -a "${LOG_FILE}"
}

log "starting script ..."

# Function to check if a command is available
check_command() {
    if ! command -v "${1}" &>/dev/null; then
        printf "Error: %s is not installed or not in PATH. Please install it and try again.\n" "${1}"
        return 1
    fi
    return 0
}

# Check for required commands
for cmd in "find" "ls" "stat" "dirname" "basename" "realpath"; do
    check_command "${cmd}" || exit 1
done

# Function to safely check file permissions
check_file_perms() {
    local file="${1}"
    if [[ -f "${file}" ]]; then
        printf "[*] Permissions for %s: %s\n" "${file}" "$(stat -c "%A" "${file}" 2>/dev/null || stat -f "%Sp" "${file}")"
    fi
}

# Function to safely find files with specific permissions
safe_find() {
    local search_path="${1}"
    local perm_pattern="${2}"
    local type_pattern="${3}"
    
    find "${search_path}" -type "${type_pattern}" -perm "${perm_pattern}" -ls 2>/dev/null || true
}

# Additional function to check sudo configuration
check_sudo_config() {
    printf "\n[+] Checking sudo configuration...\n"
    if check_command "sudo"; then
        # Check sudo version for vulnerabilities
        sudo -V 2>/dev/null || printf "Unable to get sudo version\n"
        
        # Check if current user has sudo rights
        printf "\n[+] Checking sudo rights for current user...\n"
        sudo -l 2>/dev/null || printf "Unable to list sudo rights\n"
        
        # Check for passwordless sudo entries
        printf "\n[+] Checking for passwordless sudo entries in sudoers...\n"
        if [[ -r "/etc/sudoers" ]]; then
            grep -i "NOPASSWD" "/etc/sudoers" 2>/dev/null || true
        fi
    fi
}

# Function to check for dangerous capabilities
check_capabilities() {
    printf "\n[+] Checking for dangerous capabilities...\n"
    if check_command "getcap"; then
        find "/" -type f -executable -exec getcap {} \; 2>/dev/null || true
    fi
}

# Function to check for weak NFS shares
check_nfs_shares() {
    printf "\n[+] Checking for NFS shares...\n"
    if check_command "showmount"; then
        showmount -e "localhost" 2>/dev/null || true
    fi
    
    if [[ -f "/etc/exports" ]]; then
        printf "\n[+] Checking NFS export configuration...\n"
        grep -v "^#" "/etc/exports" 2>/dev/null || true
    fi
}

# Function to check for docker access
check_docker_access() {
    printf "\n[+] Checking Docker access...\n"
    if check_command "docker"; then
        # Check if user is in docker group
        groups | grep -q "docker" && printf "[!] Warning: Current user is in docker group!\n"
        
        # Try to list containers
        docker ps -a 2>/dev/null && printf "[!] Warning: User can list docker containers!\n"
    fi
}

# Function to check custom PATH hijacking
check_path_hijacking() {
    printf "\n[+] Checking for PATH hijacking opportunities...\n"
    # Check for relative paths in scripts
    find "/" -type f -name "*.sh" -executable 2>/dev/null | while IFS= read -r script; do
        grep -l "^[^#]*command" "${script}" 2>/dev/null || true
    done
}

# Function to check for exploitable services
check_services() {
    printf "\n[+] Checking for potentially exploitable services...\n"
    
    # Check listening ports
    if check_command "netstat"; then
        printf "\n[*] Listening ports:\n"
        netstat -tuln 2>/dev/null || true
    fi
    
    # Check running processes as root
    printf "\n[*] Processes running as root:\n"
    ps aux | grep "^root" 2>/dev/null || true
}

# Function to check for dangerous environment variables
check_env_vars() {
    printf "\n[+] Checking for dangerous environment variables...\n"
    printf "[*] LD_PRELOAD: %s\n" "${LD_PRELOAD:-Not Set}"
    printf "[*] LD_LIBRARY_PATH: %s\n" "${LD_LIBRARY_PATH:-Not Set}"
    printf "[*] PATH: %s\n" "${PATH}"
}

# Function to check for stored credentials
check_stored_credentials() {
    printf "\n[+] Checking for stored credentials...\n"
    
    local cred_files=(
        ".bash_history"
        ".mysql_history"
        ".python_history"
        ".wget-hsts"
        ".gitconfig"
        ".ssh/config"
        ".aws/credentials"
        ".docker/config.json"
    )
    
    for file in "${cred_files[@]}"; do
        if [[ -f "${HOME}/${file}" ]]; then
            printf "[!] Found potential credential file: %s\n" "${HOME}/${file}"
        fi
    done
}

printf "\n================ Enhanced PrivCheck: Privilege Escalation Checker ================\n"

# Check for SUID and SGID binaries
printf "\n[+] Checking for SUID/SGID binaries...\n"
safe_find "/" "6000" "f"

# Check for world-writable files
printf "\n[+] Checking for world-writable files...\n"
safe_find "/" "-0002" "f"

# Check for world-writable directories
printf "\n[+] Checking for world-writable directories...\n"
safe_find "/" "-0002" "d"

# Check sensitive files permissions
printf "\n[+] Checking permissions for sensitive files...\n"
check_file_perms "/etc/passwd"
check_file_perms "/etc/shadow"

# Check for writable cron jobs
printf "\n[+] Checking for writable cron jobs...\n"
if [[ -d "/etc/cron.d" ]]; then
    for cron_file in "/etc/cron.d/"*; do
        if [[ -w "${cron_file}" ]]; then
            printf "[!] Warning: %s is writable!\n" "${cron_file}"
        fi
    done
fi

# Check for writable PATH directories
printf "\n[+] Checking for writable directories in PATH...\n"
while IFS=: read -r dir; do
    if [[ -w "${dir}" ]]; then
        printf "[!] Warning: Writable directory in PATH: %s\n" "${dir}"
    fi
done <<< "${PATH}"

# Check kernel version
printf "\n[+] Current kernel version:\n"
uname -r

# Run enhanced security checks
check_sudo_config
check_capabilities
check_nfs_shares
check_docker_access
check_path_hijacking
check_services
check_env_vars
check_stored_credentials

# Additional specific checks
printf "\n[+] Checking for custom setuid permissions in /usr/local/bin...\n"
safe_find "/usr/local/bin" "4000" "f"

printf "\n[+] Checking /etc/ld.so.conf.d for custom library paths...\n"
if [[ -d "/etc/ld.so.conf.d" ]]; then
    find "/etc/ld.so.conf.d" -type f -exec cat {} \; 2>/dev/null || true
fi

printf "\n[+] Checking for custom PAM configurations...\n"
if [[ -d "/etc/pam.d" ]]; then
    find "/etc/pam.d" -type f -exec cat {} \; 2>/dev/null || true
fi

# Check for unusual mount points
printf "\n[+] Checking for unusual mount points...\n"
mount | grep -v "^proc" 2>/dev/null || true

# Check for unusual network connections
printf "\n[+] Checking for unusual network connections...\n"
if check_command "ss"; then
    ss -tupln 2>/dev/null || true
fi

# Cleanup and exit
cleanup
log "stopping script ..."

script_name1="$(basename "${0}")"
printf "\nInfo: script_name1: %s\n" "${script_name1}"
script_path1="$(realpath "$(dirname "${0}")")"
printf "Info: script_path1: %s\n" "${script_path1}"
script_path_with_name="${script_path1}/${script_name1}"
printf "Info: Script path with name: %s\n" "${script_path_with_name}"
printf "Info: Script finished\n"
exit 0
