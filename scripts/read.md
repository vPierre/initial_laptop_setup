# Instructions

## 1. boot into recovery mode (hold ⌘+r during boot)

When in Recovery Mode disable SIP by opening Terminal and typing: `csrutil disable`

## 2. reboot into normal system:

Run the script in Terminal: `hax.sh`

## 3. boot BACK into recovery

Do whatever you need to do in this mode, but before rebooting please re-enable SIP by typing `csrutil enable` in Terminal:

* Note you'll likely need to redo this each time you upgrade macOS.

## SERIOUS TIPS

- Don't disable anything until you know exactly what it does. 
- Running the script for the first time won't show results until after you reboot.
- READ THE COMMENTS AND COMMENTED LINES OF THE SCRIPT. THIS REMOVES APPS YOU MIGHT ACTUALLY USE SO MAKE SURE YOU COMMENT OUT WHAT YOU USE!!!
- This script is tuned to MY liking, if you straight up run it on your computer without knowing what's happening I AM NOT RESPONISBLE FOR REMOVED APPS OR UNEXPECTED BEHAVIOR!

## What it does

This script unloads a lot of default Apple Agents and Daemons from macOS for performance and privacy (and possibly saves battery on laptops).

- List all jobs in system: `launchctl list`
- Get active services: `launchctl list | grep -v "\-\t0"`
- Find a service: `grep -lR [service] /System/Library/Launch* /Library/Launch* ~/Library/LaunchAgents`
- Count all jobs in system: `launchctl list | wc -l`
- Don't kill tasks with kill -9; they will only be ressurected under a new PID and leak memory.

Use: `launchctl stop com.example.app` instead.

- To permanently disable a job: `launchctl unload -w <task>`
- for more info, `launchctl help`

## Figure out more things yourself :)

Compiled from a bunch of sources - sorry for not caring about credit, if people look at the link hard enough they'll find you there somewhere but if you still care that much find me and punch me or something.

- Use StartPage or man command/website.
- https://apple.stackexchange.com/a/296542