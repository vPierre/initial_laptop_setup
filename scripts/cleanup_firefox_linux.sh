#!/usr/bin/env bash

# Author: Pierre Gronau <Pierre.Gronau@ndaal.eu>
# Copyright 2024
# License: All content is licensed under the terms of the <Apache 2.0>
# Developed on: Debian 12.x; macOS Sequoia x86 architecture
# Tested on: Debian 12.x; macOS Sequoia x86 architecture

set -o errexit
set -o errtrace
set -o nounset
set -o pipefail

# nosemgrep: ifs-tampering
IFS=$'\n\t'

# Error trapping function
# Add this line before the error_handler function
# shellcheck disable=SC2317
error_handler() {
    local line_number="${1}"
    local error_code="${2}"
    local last_command="${BASH_COMMAND}"
    printf "Error: Error occurred in line %s (error code: %s)\n" "${line_number}" "${error_code}"
    printf "Error: Failed command: %s\n" "${last_command}"

    # Success case
    return 0
}

# Set the error trap
trap 'error_handler ${LINENO} $?' ERR

cleanup() {
    trap - SIGINT SIGTERM ERR EXIT
    printf "%b\n" "\nInfo: Cleanup is running ..."
    # Additional cleanup tasks can be added here if needed
    rm -f -v ./*.tmp
    printf "%b\n" "\nInfo: Cleanup finished ..."
}

trap cleanup SIGINT SIGTERM ERR EXIT

VERSION="1.0.0"
readonly VERSION
printf "Info: Current version: %s\n" "${VERSION}"

LOG_FILE="/var/log/firefox_cleanup.log"

# Logging function
log() {
    printf "%s - %s\n" "$(date '+%Y-%m-%d %H:%M:%S')" "${1}" | tee -a "${LOG_FILE}"
}

# Check for root privileges
check_root() {
    if [ "${EUID}" -ne 0 ]; then
        script_path=$([[ "${0}" = /* ]] && printf "%s" "${0}" || printf "%s" "${PWD}/${0#./}")
        sudo "${script_path}" || {
            printf "Error: Administrator privileges are required.\n"
            exit 1
        }
        exit 0
    fi
}

# Clear Firefox data for a specific user directory
clear_firefox_data() {
    local dir="${1}"
    if [ -d "${dir}" ]; then
        printf "Info: Clearing Firefox data in: %s\n" "${dir}"
        rm -r -f -v "${dir:?}"/*
    else
        printf "Warning: No Firefox data found in: %s, skipping...\n" "${dir}"
    fi
}

# Remove specific Firefox files in a user directory
remove_firefox_files() {
    local base_dir="${1}"
    local file_pattern="${2}"
    find "${base_dir}" -name "${file_pattern}" -type f -delete
    printf "Removed %s files from %s\n" "${file_pattern}" "${base_dir}"
}

# Clear Firefox directories in a user directory
clear_firefox_directories() {
    local base_dir="${1}"
    local dir_name="${2}"
    rm -r -f -v "${base_dir:?}/${dir_name:?}"
    printf "Cleared %s directory from %s\n" "${dir_name}" "${base_dir}"
}

# Process each user's home directory for Firefox cleanup
process_user_home() {
    local user_home="${1}"

    # Clear Firefox data for different installation types
    clear_firefox_data "${user_home}/.mozilla/firefox"
    clear_firefox_data "${user_home}/.var/app/org.mozilla.firefox"
    clear_firefox_data "${user_home}/snap/firefox"

    # Remove specific Firefox files
    firefox_files=(
        "addons.*" "bookmarks.sqlite*" "cookies.sqlite*" "downloads.sqlite*"
        "extensions.json" "favicons.sqlite*" "firefox_cookies.sqlite*"
        "formhistory.sqlite*" "key*.db" "logins.json" "permissions.sqlite*"
        "places.sqlite*" "prefs.js" "protections.sqlite*" "search.sqlite*"
        "signon*.*" "signons.sqlite*" "storage-sync*.sqlite*" "webappstore.sqlite*" 
        "downloads.rdf"
    )

    for file in "${firefox_files[@]}"; do
        remove_firefox_files "${user_home}/.mozilla/firefox" "${file}"
        remove_firefox_files "${user_home}/.var/app/org.mozilla.firefox" "${file}"
        remove_firefox_files "${user_home}/snap/firefox" "${file}"
    done

    # Clear Firefox directories
    firefox_dirs=("bookmarkbackups" "sessionstore*" "downloads.rdf")

    for dir in "${firefox_dirs[@]}"; do
        clear_firefox_directories "${user_home}/.mozilla/firefox" "${dir}"
        clear_firefox_directories "${user_home}/.var/app/org.mozilla.firefox" "${dir}"
        clear_firefox_directories "${user_home}/snap/firefox" "${dir}"
    done
}

# Main function to iterate through all user home directories and perform cleanup
main() {
    check_root
    
    printf "Starting Firefox cleanup and privacy enhancement...\n"

    while IFS=: read -r user _ uid _ _ home _; do
        if [ "${uid}" -ge 1000 ] && [ -d "${home}" ]; then  # Check for normal users (UID >= 1000)
            process_user_home "${home}"
        fi
    done < /etc/passwd

    printf "Firefox cleanup and privacy enhancement completed.\n"
}

main "$@"

cleanup

script_name1="$(basename "${0}")"
printf "\nInfo: script_name1: %s\n" "${script_name1}"
script_path1="$(realpath "$(dirname "${0}")")"
printf "Info: script_path1: %s\n" "${script_path1}"
script_path_with_name="${script_path1}/${script_name1}"
printf "Info: Script path with name: %s\n" "${script_path_with_name}"
printf "Info: Script finished\n"
exit 0