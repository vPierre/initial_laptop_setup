******************************************************************************
Systemhärtung - Verzeichnisse
******************************************************************************

.. sectionauthor:: Pierre Gronau <Pierre.Gronau@ndaal.eu>

.. contents:: Inhalt - Sicherheit - Container
   :depth: 3



Prüfung von vorhanden sein von Befehlen
------------------------------------------------------------------------------

# Function to check if a command is available

check_command() {
 if ! command -v "${1}" &>/dev/null; then
  printf "Error: %s is not installed or not in PATH. Please install it and try again.\n" "${1}"
  return 1
 fi

# Success case

 return 0
}

# Check for required commands

check_command "git"
check_command "git-lfs"
check_command "wget"
check_command "curl"
check_command "virtualbox"
check_command "vagrant"

Vorbereitung der Systemhärtungsumgebung
------------------------------------------------------------------------------

Voraussetzungen
==============================================================================

* Linux Debian 12.8x inkl. aller aktuell verfügbaren
Sicherheitsaktualisierungen
* Linux AlmaLinux 9.x inkl. aller aktuell verfügbaren
Sicherheitsaktualisierungen
* Linux Rocky Linux 9.x inkl. aller aktuell verfügbaren
Sicherheitsaktualisierungen
* git Repo
* Ansible
* bash

Wir benötigen nun folgende Skripte und Schritte um zu einer Systemhärtung
zu kommen:

Mittels vagrant werden drei Virtualbox VMs produziert:

* Linux Debian 12.8x
* Linux AlmaLinux 9.x
* Linux Rocky Linux 9.x

Soweit vorhanden werden nur offizielle Images verwendet wie bei
<https://portal.cloud.hashicorp.com/vagrant/discover/almalinux/9>

Ablauf der Systemhärtung
------------------------------------------------------------------------------

* Installiere eine verschlüsselte apt Kommunikation mittels "apt-transport-https" auf der Debian VM
* Installiere die benötigten Werkzeuge auf Debian 12.8x
* Härte Debian mittels <https://github.com/ovh/debian-cis> und überprüfe mittels ```bin/hardening.sh --audit-all"
* Hole aus dem git Repo(s) alle benötigten Skripte und Ansible Rollen
* Prüfe alle bash Skripte mittels ```shellcheck``` und ```shfmt``` und beende die Automation bei Findings
* Prüfe alle Ansible Rollen mittels ```ansible-lint``` und beende die Automation bei Findings
* Anwenden der Ansible Rollen auf die Red Hat 9 basierten VMs wie **AlmaLinux** und **Rocky Linux**
* überprüfe mittels ```Inspect``` die Systemhärtung

zu den einzelnen Schritten
------------------------------------------------------------------------------

Installiere die benötigten Werkzeuge auf Debian 12.8x
==============================================================================

# apt update && sudo apt upgrade -y

array_packages=(
"apt-transport-https"
"wget"
"curl"
"git"
"git-lfs"
"ansible"
"ansible-lint"

)

vervollständige "array_packages" ...

readonly array_packages
printf "%sInfo: Package: %s\n" "${array_packages[@]}"

.. include:: "${TARGET_DIRECTORY}function_install_packages_for_package_manager.cfg"

# Usage example

install_packages -debian package1 package2 package3

Ansible Playbook
------------------------------------------------------------------------------

Das Ansible Playbook besteht mindestens aus folgenden Ansible Rollen:

Ansible Rolle - auditd minimal
==============================================================================

* Install ```auditd``` and test the functionality
* If ```journald``` is installed keep it
* Basis für die **Ansible Rolle** ist: <https://gitlab.com/ndaal_open_source/ndaal_public_auditd>
* reboot

Ansible Rolle - Preparation hardening
==============================================================================

* Placeholder Ansible Role
* reboot

Ansible Rolle - Patching initial
==============================================================================

* Patching
* reboot

.. include:: "${TARGET_DIRECTORY}function_update_packages_for_package_manager.cfg"

# Call the function with the first command-line argument

update_packages "${1}"

Ansible Rolle - CIS hardening
==============================================================================

* System hardening with <https://github.com/ansible-lockdown/RHEL9-CIS> and! <https://github.com/RedHatOfficial/ansible-role-rhel9-cis>
* test the hardening with ```Inspec``` like this <https://github.com/mitre/redhat-enterprise-linux-9-stig-baseline>
* reboot

Ansible Rolle - ssh hardening
==============================================================================

* System hardening with <https://gitlab.com/vPierre/ndaal_public_ansible_role_hardening_ssh>

Ansible Rolle - auditd with ndaal audit udit_best_practices.rules
==============================================================================

* Install ```auditd``` and test the functionality
* Basis für die Ansible Rolle ist:
  <https://gitlab.com/ndaal_open_source/ndaal_public_auditd/-/blob/main/ndaal/audit_best_practices.rules?ref_type=heads>
* reboot

Ansible Rolle - Patching Post hardening
==============================================================================

* Patching

.. include:: "${TARGET_DIRECTORY}function_update_packages_for_package_manager.cfg"

# Call the function with the first command-line argument

update_packages "${1}"

Ansible Rolle - Post hardening
==============================================================================

* Placeholder Ansible Role
* reboot

Ansible Rolle - Post hardening - cleanup
==============================================================================

* Placeholder Ansible Role
* e.g. Cleanup bash history
* reboot

.. include:: "${TARGET_DIRECTORY}function_install_packages_for_package_manager.cfg"

clear_package_traces debian

# !/usr/bin/env bash

# Author: Pierre Gronau <Pierre.Gronau@ndaal.eu>

# Copyright 2024

# License: All content is licensed under the terms of the <Apache 2.0>

# Developed on: Debian 12.x; macOS Sequoia x86 architecture

# Tested on: Debian 12.x; macOS Sequoia x86 architecture

set -o errexit
set -o errtrace
set -o nounset
set -o pipefail

# set -o xtrace

# nosemgrep: ifs-tampering

IFS=$'\n\t'

# Logging function

log() {
    printf "%s - %s\n" "$(date '+%Y-%m-%d %H:%M:%S')" "${1}" | tee -a "${LOG_FILE}"
}

# Error trapping function

# Add this line before the error_handler function

# shellcheck disable=SC2317

error_handler() {
    local line_number="${1}"
    local error_code="${2}"
    local last_command="${BASH_COMMAND}"
    printf "Error: Error occurred in line %s (error code: %s)\n" "${line_number}" "${error_code}"
    printf "Error: Failed command: %s\n" "${last_command}"
    log "Error: Error occurred in line ${line_number} (error code: ${error_code})"
    log "Error: Failed command: ${last_command}"
}

# Set the error trap

trap 'error_handler ${LINENO} $?' ERR

cleanup() {
    trap - SIGINT SIGTERM ERR EXIT
    printf "%b\n" "\nInfo: Cleanup is running ..."
    # Additional cleanup tasks can be added here if needed
    rm -f -v ./*.tmp
    printf "%b\n" "\nInfo: Cleanup finished ..."
    log "Info: Cleanup finished ..."
}

trap cleanup SIGINT SIGTERM ERR EXIT

if [[ "$(uname)" == "Darwin" ]]; then
    HOMEDIR="Users"
    readonly HOMEDIR
elif [[ "$(uname)" == "Linux" ]]; then
    HOMEDIR="home"
    readonly HOMEDIR
else
    printf "Error: Unsupported operating system: %s\n" "$(uname)"
    exit 1
fi

# Under Linux you use `home` under macOS `Users`

printf "Info: Home Directory: %s\n" "${HOMEDIR}"

USERSCRIPT="cloud"

# Your user! In which context it SHOULD run

printf "Info: User script: %s\n" "${USERSCRIPT}"

VERSION="0.1.0"
readonly VERSION
printf "Info: Current version: %s\n" "${VERSION}"

LOG_FILE="/${HOMEDIR}/${USERSCRIPT}/cleanup_package_manager.log"
readonly LOG_FILE
printf "Info: log file with path: %s\n" "${LOG_FILE}"
if [[ ! -f "${LOG_FILE}" ]]; then
    printf "Info: Log file does not exist. Creating it now.\n"
    touch "${LOG_FILE}" || {
        printf "Error: Failed to create log file at %s\n" "${LOG_FILE}"
        exit 1
    }
else
    printf "Info: Log file already exists at %s\n" "${LOG_FILE}"
fi

log "starting script"

print_asterisks_value=79
readonly print_asterisks_value
printf "Info: *** print_asterisks_value: %s\n" "${print_asterisks_value}"

# Function to print a line of x asterisks

print_asterisks() {
    local x="${1}"

    # Check if x is within the valid range
    if [[ "${x}" -lt 10 || "${x}" -gt 79 ]]; then
        printf "Error: The value of x (%d) must be between 10 and 79.\n" "${x}"
        exit 1
    fi

    # Print x asterisks
    printf "%0.s*" $(seq 1 "${x}")
    printf "\n"

    # Success case
    return 0
}

print_asterisks "${print_asterisks_value}"

log "Info: Home Directory: ${HOMEDIR}"
log "Info: User script: ${USERSCRIPT}"
log "Info: Current version: ${VERSION}"
log "Info: log file with path: ${LOG_FILE}"
log "Info: print_asterisks_value ${print_asterisks_value}"

# Function to check if a command is available

check_command() {
    if ! command -v "${1}" &>/dev/null; then
        printf "Error: %s is not installed or not in PATH. Please install it and try again.\n" "${1}"
        return 1
    fi

    # Success case
    return 0
}

# Check for required commands

check_command "git"

# Define possible package managers

array_possible_package_manager=(
    "apt"
    "apk"
    "pacman"
    "zypper"
    "brew"
    "dnf"
    "rpm"
    "yum"
    "xbps"
    "pisi"
    "nix"
    "yay"
    "equo"
    "aurman"
    "flatpak"
    "snap"
    "cargo"
    "portage"
    "paludis"
    "conary"
    "slackpkg"
    "zeroinstall"
    "pip"
)

readonly array_possible_package_manager
printf "Info: Possible Package Managers:\n"
printf "Info: Package Manager is: %s\n" "${array_possible_package_manager[@]}"

# Log the package managers

log "Info: Package Manager list:"
for manager in "${array_possible_package_manager[@]}"; do
    log "Info: Package Manager - ${manager}"
done

printf "Info: Package Managers logged to %s\n" "${LOG_FILE}"

print_asterisks "${print_asterisks_value}"

.. include:: "${TARGET_DIRECTORY}function_cleanup_package_manager_for_package_manager.cfg"

# Main function

main() {
    for package_manager in "${array_possible_package_manager[@]}"; do
        if check_command "${package_manager}"; then
            printf "Info: %s is installed. Cleaning cache...\n" "${package_manager}"
            cleanup_package_manager "${package_manager}"
            "${package_manager}" -V  || true
        else
            printf "Info: %s is not installed. Skipping.\n" "${package_manager}"
        fi
    done
}

main

printf "Info: Script finished\n"

cleanup

log "Info: Script finished\n"

script_name1="$(basename "${0}")"
printf "\nInfo: script_name1: %s\n" "${script_name1}"
script_path1="$(realpath "$(dirname "${0}")")"
printf "Info: script_path1: %s\n" "${script_path1}"
script_path_with_name="${script_path1}/${script_name1}"
printf "Info: Script path with name: %s\n" "${script_path_with_name}"
printf "Info: Script finished\n"
exit 0

export HOME="/home/${SUDO_USER:-${USER}}" # Keep `~` and `$HOME` for user not `/root`.

# ----------------------------------------------------------

# ------------------Disable .NET telemetry------------------

# ----------------------------------------------------------

printf "Info: --- Disable .NET telemetry\n"
variable="DOTNET_CLI_TELEMETRY_OPTOUT"
value="1"
declaration_file="/etc/environment"
if ! [ -f "${declaration_file}" ]; then
  printf "\"%s\" does not exist.\n" "${declaration_file}"
  sudo touch "${declaration_file}"
  printf "Info: Created %s.\n" "${declaration_file}"
fi
assignment_start="${variable}="
assignment="${variable}=${value}"
if ! grep --quiet "^${assignment_start}" "${declaration_file}"; then
  printf "Error: Variable \"%s\" was not configured before.\n" "${variable}"
  printf "\n%s" "${assignment}" | sudo tee -a "${declaration_file}" > /dev/null
  printf "Info: Successfully configured (%s).\n" "${assignment}"
else
  if grep --quiet "^${assignment}$" "${declaration_file}"; then
    printf "Warning: Skipping. Variable \"%s\" is already set to value \"%s\".\n" "${variable}" "${value}"
  else
    if ! sudo sed --in-place "/^${assignment_start}/d" "${declaration_file}"; then
      printf "Error: Failed to delete assignment starting with \"%s\".\n" "${assignment_start}" >&2
    else
      printf "Info: Successfully deleted unexpected assignment of \"%s\".\n" "${variable}"
      if ! printf "\n%s" "${assignment}" | sudo tee -a "${declaration_file}" > /dev/null; then
        printf "Error: Failed to add assignment \"%s\".\n" "${assignment}" >&2
      else
        printf "Info: Successfully reconfigured (%s).\n" "${assignment}"
      fi
    fi
  fi
fi

# ----------------------------------------------------------

# ----------------------------------------------------------

# ------------------- Clear Wine cache ---------------------

# ----------------------------------------------------------

printf "Info: --- Clear Wine cache\n"

# Temporary Windows files for global prefix

rm -r -f -v "${HOME}/.wine/drive_c/windows/temp/"*

# Wine cache

rm -r -f -v "${HOME}/.cache/wine/"

# ----------------------------------------------------------

# ----------------------------------------------------------

# ------------------ Clear Winetricks cache ----------------

# ----------------------------------------------------------

# Check if Winetricks cache directory exists before attempting to clear it

if [ -d "${HOME}/.cache/winetricks/" ]; then
  printf "Info: --- Clearing Winetricks cache...\n"
  rm -rfv "${HOME}/.cache/winetricks/"*
else
  printf "No Winetricks cache directory found, skipping...\n"
fi

# ----------------------------------------------------------

# ----------------------------------------------------------

# ---------- Clear Visual Studio Code crash reports ---------

# ----------------------------------------------------------

# Clear Visual Studio Code crash reports

printf "Info: --- Clear Visual Studio Code crash reports\n"

# Function to clear crash reports from a specified directory

clear_crash_reports() {
  local dir="${1}"
  if [ -d "${dir}" ]; then
    printf "Info: Clearing crash reports in: %s\n" "${dir}"
    rm -rfv "${dir}"*
  else
    printf "Error: No crash reports found in: %s, skipping...\n" "${dir}"
  fi
}

# Clear Crash Reports: Global installation (also Snap with --classic)

clear_crash_reports "${HOME}/.config/Code/Crash Reports/"

# Clear Crash Reports: Flatpak installation

clear_crash_reports "${HOME}/.var/app/com.visualstudio.code/config/Code/Crash Reports/"

# Clear exthost Crash Reports: Global installation (also Snap with --classic)

clear_crash_reports "${HOME}/.config/Code/exthost Crash Reports/"

# Clear exthost Crash Reports: Flatpak installation

clear_crash_reports "${HOME}/.var/app/com.visualstudio.code/config/Code/exthost Crash Reports/"

# ----------------------------------------------------------

# ----------------------------------------------------------

# --------------Clear Visual Studio Code logs---------------

# ----------------------------------------------------------

# Clear Visual Studio Code logs

printf "Info: --- Clear Visual Studio Code logs\n"

# Function to clear logs from a specified directory

clear_logs() {
  local dir="${1}"
  if [ -d "${dir}" ]; then
    printf "Info: Clearing logs in: %s\n" "${dir}"
    rm -r -f -v "${dir}"*
  else
    printf "Error: No logs found in: %s, skipping...\n" "${dir}"
  fi
}

# Clear logs: Global installation (also Snap with --classic)

clear_logs "${HOME}/.config/Code/logs/"

# Clear logs: Flatpak installation

clear_logs "${HOME}/.var/app/com.visualstudio.code/config/Code/logs/"

# ----------------------------------------------------------

# ----------------------------------------------------------

# --------------Clear Azure CLI telemetry data--------------

# ----------------------------------------------------------

# Clear Azure CLI telemetry data

printf "Info: --- Clear Azure CLI telemetry data\n"

# Function to clear telemetry data from a specified directory

clear_azure_telemetry() {
  local telemetry_dir="${HOME}/.azure/telemetry"
  if [ -d "${telemetry_dir}" ]; then
    printf "Info: Clearing telemetry directory: %s\n" "${telemetry_dir}"
    rm -rfv "${telemetry_dir}"
  else
    printf "Error: Telemetry directory not found: %s, skipping...\n" "${telemetry_dir}"
  fi

# Remove other telemetry-related files

  rm -fv "${HOME}/.azure/telemetry.txt"
  rm -fv "${HOME}/.azure/logs/telemetry.txt"
}

# Call the function to clear Azure CLI telemetry data

clear_azure_telemetry

# ----------------------------------------------------------

# ----------------------------------------------------------

# -------------------Clear Azure CLI logs-------------------

# ----------------------------------------------------------

# Clear Azure CLI logs

printf "Info: --- Clear Azure CLI logs\n"

# Function to clear logs from a specified directory

clear_azure_logs() {
  local logs_dir="${HOME}/.azure/logs"
  if [ -d "${logs_dir}" ]; then
    printf "Info: Clearing logs in: %s\n" "${logs_dir}"
    rm -rfv "${logs_dir}"*
  else
    printf "Error: No logs found in: %s, skipping...\n" "${logs_dir}"
  fi
}

# Call the function to clear Azure CLI logs

clear_azure_logs

# ----------------------------------------------------------

# ----------------------------------------------------------

# ------------------Clear Azure CLI cache-------------------

# ----------------------------------------------------------

# Clear Azure CLI cache

printf "Info: --- Clear Azure CLI cache\n"

# Check if the Azure CLI is installed

if ! command -v "az" &> /dev/null; then
  printf "Warning: Skipping because \"az\" is not found.\n"
else
  printf "Info: Clearing Azure CLI cache...\n"
  az cache purge
fi

# ----------------------------------------------------------

# ----------------------------------------------------------

# -------------------Clear Firefox cache--------------------

# ----------------------------------------------------------

# Clear Firefox cache

printf "Info: --- Clear Firefox cache\n"

# Function to clear cache from a specified directory

clear_firefox_cache() {
  local cache_dir="${1}"
  if [ -d "${cache_dir}" ]; then
    printf "Info: Clearing cache in: %s\n" "${cache_dir}"
    rm -rfv "${cache_dir}"*
  else
    printf "Warning: No cache found in: %s, skipping...\n" "${cache_dir}"
  fi
}

# Clear cache: Global installation

clear_firefox_cache "${HOME}/.cache/mozilla/"

# Clear cache: Flatpak installation

clear_firefox_cache "${HOME}/.var/app/org.mozilla.firefox/cache/"

# Clear cache: Snap installation

clear_firefox_cache "${HOME}/snap/firefox/common/.cache/"

# ----------------------------------------------------------

# ----------------------------------------------------------

# ---------------Clear Firefox crash reports----------------

# ----------------------------------------------------------

# Clear Firefox crash reports

printf "Info: --- Clear Firefox crash reports\n"

# Function to clear crash reports from a specified directory

clear_firefox_crash_reports() {
  local crash_dir="${1}"
  if [ -d "${crash_dir}" ]; then
    printf "Info: Clearing crash reports in: %s\n" "${crash_dir}"
    rm -rfv "${crash_dir}"*
  else
    printf "Warning: No crash reports found in: %s, skipping...\n" "${crash_dir}"
  fi
}

# Clear crash reports: Global installation

clear_firefox_crash_reports "${HOME}/.mozilla/firefox/Crash Reports/"

# Clear crash reports: Flatpak installation

clear_firefox_crash_reports "${HOME}/.var/app/org.mozilla.firefox/.mozilla/firefox/Crash Reports/"

# Clear crash reports: Snap installation

clear_firefox_crash_reports "${HOME}/snap/firefox/common/.mozilla/firefox/Crash Reports/"

# Function to delete files matching a pattern using Python

delete_matching_files() {
  local pattern="${1}"
  if ! command -v "python3" &> /dev/null; then
    printf "Warning: Skipping because \"python3\" is not found.\n"
    return
  fi

  python3 <<EOF
import glob
import os

pattern = '${pattern}'
expanded_path = os.path.expandvars(os.path.expanduser(pattern))
print(f'Deleting files matching pattern: {expanded_path}')
paths = glob.glob(expanded_path)

if not paths:
    print('Warning: Skipping, no paths found.')

for path in paths:
    if not os.path.isfile(path):
        print(f'Warning: Skipping folder: "{path}".')
        continue
    os.remove(path)
    print(f'Info: Successfully deleted file: "{path}".')

print(f'Info: Successfully deleted {len(paths)} file(s).')
EOF
}

# Delete files matching patterns for crashes and events

delete_matching_files "${HOME}/.mozilla/firefox/*/crashes/*"
delete_matching_files "${HOME}/.var/app/org.mozilla.firefox/.mozilla/firefox/*/crashes/*"
delete_matching_files "${HOME}/snap/firefox/common/.mozilla/firefox/*/crashes/*"
delete_matching_files "${HOME}/.mozilla/firefox/*/crashes/events/*"
delete_matching_files "${HOME}/.var/app/org.mozilla.firefox/.mozilla/firefox/*/crashes/events/*"
delete_matching_files "${HOME}/snap/firefox/common/.mozilla/firefox/*/crashes/events/*"

# ----------------------------------------------------------

# ----------------------------------------------------------

# -----------------Remove old Snap packages-----------------

# ----------------------------------------------------------

# Remove old Snap packages

printf "Info: --- Remove old Snap packages\n"

# Check if the snap command is available

if ! command -v "snap" &> /dev/null; then
  printf "Warning: Skipping because \"snap\" is not found.\n"
else

# List all Snap packages and remove disabled ones

  snap list --all | while read -r name version rev tracking publisher notes; do
    if [[ "${notes}" == *disabled* ]]; then
      printf "Info: Removing disabled Snap package: %s (revision: %s)\n" "${name}" "${rev}"
      sudo snap remove "${name}" --revision="${rev}"
    fi
  done
fi

# ----------------------------------------------------------

# ----------------------------------------------------------

# -------------Remove orphaned Flatpak runtimes-------------

# ----------------------------------------------------------

# Remove orphaned Flatpak runtimes

printf "Info: --- Remove orphaned Flatpak runtimes\n"

# Check if the flatpak command is available

if ! command -v "flatpak" &> /dev/null; then
  printf "Warning: Skipping because \"flatpak\" is not found.\n"
else
  printf "Info: Uninstalling unused Flatpak runtimes...\n"
  flatpak uninstall --unused --noninteractive
  printf "Info: Orphaned Flatpak runtimes removed successfully.\n"
fi

# ----------------------------------------------------------

# ----------------------------------------------------------

# ---------------Clear obsolete APT packages----------------

# ----------------------------------------------------------

# Clear obsolete APT packages

printf "Info: --- Clear obsolete APT packages\n"

# Check if the apt-get command is available

if ! command -v "apt-get" &> /dev/null; then
  printf "Warning: Skipping because \"apt-get\" is not found.\n"
else
  printf "Info: Running 'apt-get autoclean' to remove obsolete packages...\n"
  sudo apt-get autoclean
  printf "Info: Obsolete APT packages cleared successfully.\n"
fi

# ----------------------------------------------------------

# ----------------------------------------------------------

# ---------Clear orphaned APT package dependencies----------

# ----------------------------------------------------------

# Clear orphaned APT package dependencies

printf "Info: --- Clear orphaned APT package dependencies\n"

# Check if the apt-get command is available

if ! command -v "apt-get" &> /dev/null; then
  printf "Warning: Skipping because \"apt-get\" is not found.\n"
else
  printf "Info: Running 'apt-get -y autoremove --purge' to clear orphaned dependencies...\n"
  sudo apt-get -y autoremove --purge
  printf "Info: Orphaned APT package dependencies cleared successfully.\n"
fi

# ----------------------------------------------------------

# ----------------------------------------------------------

# -------Disable participation in Popularity Contest--------

# ----------------------------------------------------------

# Disable participation in Popularity Contest

printf "Info: --- Disable participation in Popularity Contest\n"

# Configuration file path

config_file="/etc/popularity-contest.conf"

# Check if the configuration file exists

if [ -f "${config_file}" ]; then
  printf "Info: Modifying configuration file at: %s\n" "${config_file}"
  sudo sed -i '/PARTICIPATE/c\PARTICIPATE=no' "${config_file}"
  printf "Info: Participation in Popularity Contest has been disabled.\n"
else
  printf "Warning: Skipping because configuration file at (%s) is not found. Is popcon installed?\n" "${config_file}"
fi

# ----------------------------------------------------------

# ----------------------------------------------------------

# -----------Disable weekly `pkgstats` submission-----------

# ----------------------------------------------------------

# Disable weekly `pkgstats` submission

printf "Info: --- Disable weekly \`pkgstats\` submission\n"

# Check if the systemctl command is available

if ! command -v "systemctl" &> /dev/null; then
  printf "Warning: Skipping because \"systemctl\" is not found.\n"
else
  service="pkgstats.timer"

# Check if the service exists

  if systemctl list-units --full --all | grep --fixed-strings --quiet "${service}"; then
    # Check if the service is enabled
    if systemctl is-enabled --quiet "${service}"; then
      # Check if the service is active
      if systemctl is-active --quiet "${service}"; then
        printf "Info: Service %s is running now, stopping it...\n" "${service}"
        if ! sudo systemctl stop "${service}"; then
          printf "Error: Could not stop %s.\n" "${service}" >&2
        else
          printf "Info: Successfully stopped %s.\n" "${service}"
        fi
      fi

      # Disable the service
      if sudo systemctl disable "${service}"; then
        printf "Info: Successfully disabled %s.\n" "${service}"
      else
        printf "Error: Failed to disable %s.\n" "${service}" >&2
      fi
    else
      printf "Warning: Skipping, %s is already disabled.\n" "${service}"
    fi
  else
    printf "Warning: Skipping, %s does not exist.\n" "${service}"
  fi
fi

# ----------------------------------------------------------

# ----------------------------------------------------------

# ---Disable participation in metrics reporting in Ubuntu---

# ----------------------------------------------------------

# Disable participation in metrics reporting in Ubuntu

printf "Info: --- Disable participation in metrics reporting in Ubuntu\n"

# Check if the ubuntu-report command is available

if ! command -v "ubuntu-report" &> /dev/null; then
  printf "Warning: Skipping because \"ubuntu-report\" is not found.\n"
else

# Opt out of metrics reporting

  if ubuntu-report -f send no; then
    printf "Info: Successfully opted out of metrics reporting.\n"
  else
    printf "Error: Failed to opt out of metrics reporting.\n" >&2
  fi
fi

# ----------------------------------------------------------

# ----------------------------------------------------------

# ------------------Disable Apport service------------------

# ----------------------------------------------------------

# Disable Apport service

printf "Info: --- Disable Apport service\n"

# Check if the systemctl command is available

if ! command -v "systemctl" &> /dev/null; then
  printf "Warning: Skipping because \"systemctl\" is not found.\n"
else
  service="apport"

# Check if the service exists

  if systemctl list-units --full --all | grep --fixed-strings --quiet "${service}"; then
    # Check if the service is enabled
    if systemctl is-enabled --quiet "${service}"; then
      # Check if the service is active
      if systemctl is-active --quiet "${service}"; then
        printf "Info: Service %s is running now, stopping it...\n" "${service}"
        if ! sudo systemctl stop "${service}"; then
          printf "Error: Could not stop %s.\n" "${service}" >&2
        else
          printf "Info: Successfully stopped %s.\n" "${service}"
        fi
      fi

      # Disable the service
      if sudo systemctl disable "${service}"; then
        printf "Info: Successfully disabled %s.\n" "${service}"
      else
        printf "Error: Failed to disable %s.\n" "${service}" >&2
      fi
    else
      printf "Warning: Skipping, %s is already disabled.\n" "${service}"
    fi
  else
    printf "Warning: Skipping, %s does not exist.\n" "${service}"
  fi
fi

# ----------------------------------------------------------

# ----------------------------------------------------------

# --Disable participation in Apport error messaging system--

# ----------------------------------------------------------

# Disable participation in Apport error messaging system

printf "Info: --- Disable participation in Apport error messaging system\n"

# Check if the Apport configuration file exists

if [ -f "/etc/default/apport" ]; then

# Disable Apport by modifying the configuration file

  sudo sed -i 's/enabled=1/enabled=0/g' "/etc/default/apport"
  printf "Info: Successfully disabled Apport error messaging system.\n"
else
  printf "Warning: Skipping, Apport is not configured to be enabled.\n"
fi

# ----------------------------------------------------------

# ----------------------------------------------------------

# -----------------Disable Whoopsie service-----------------

# ----------------------------------------------------------

# Disable Whoopsie service

printf "Info: --- Disable Whoopsie service\n"

# Check if the systemctl command is available

if ! command -v "systemctl" &> /dev/null; then
  printf "Warning: Skipping because \"systemctl\" is not found.\n"
else
  service="whoopsie"

# Check if the service exists

  if systemctl list-units --full --all | grep --fixed-strings --quiet "${service}"; then
    # Check if the service is enabled
    if systemctl is-enabled --quiet "${service}"; then
      # Check if the service is active
      if systemctl is-active --quiet "${service}"; then
        printf "Info: Service %s is running now, stopping it...\n" "${service}"
        if ! sudo systemctl stop "${service}"; then
          printf "Error: Could not stop %s.\n" "${service}" >&2
        else
          printf "Info: Successfully stopped %s.\n" "${service}"
        fi
      fi

      # Disable the service
      if sudo systemctl disable "${service}"; then
        printf "Info: Successfully disabled %s.\n" "${service}"
      else
        printf "Error: Failed to disable %s.\n" "${service}" >&2
      fi
    else
      printf "Warning: Skipping, %s is already disabled.\n" "${service}"
    fi
  else
    printf "Warning: Skipping, %s does not exist.\n" "${service}"
  fi
fi

# ----------------------------------------------------------

# ----------------------------------------------------------

# -------------Disable crash report submissions-------------

# ----------------------------------------------------------

# Disable crash report submissions

printf "Info: --- Disable crash report submissions\n"

# Check if the Whoopsie configuration file exists

if [ -f "/etc/default/whoopsie" ]; then

# Modify the configuration to disable crash reports

  sudo sed -i 's/report_crashes=true/report_crashes=false/' "/etc/default/whoopsie"
  printf "Info: Successfully disabled crash report submissions.\n"
else
  printf "Warning: Skipping, Whoopsie configuration file not found.\n"
fi

# ----------------------------------------------------------

# ----------------------------------------------------------

# -----------Disable Visual Studio Code telemetry-----------

# ----------------------------------------------------------

# Disable Visual Studio Code telemetry

printf "Info: --- Disable Visual Studio Code telemetry\n"

# Check if Python 3 is available

if ! command -v "python3" &> /dev/null; then
  printf "Warning: Skipping because \"python3\" is not found.\n"
else

# Function to update telemetry settings in the settings.json file

  update_telemetry_setting() {
    local property_name="${1}"
    local target_value="${2}"
    local user_name
    user_name="$(whoami)"
    local home_dir="/home/$user_name"
    local settings_files=(
      # Global installation (also Snap that installs with "--classic" flag)
      "${home_dir}/.config/Code/User/settings.json"
      # Flatpak installation
      "${home_dir}/.var/app/com.visualstudio.code/config/Code/User/settings.json"
    )

    for settings_file in "${settings_files[@]}"; do
      if [[ ! -f "${settings_file}" ]]; then
        printf "Warning: Skipping, file does not exist at \"%s\".\n" "${settings_file}"
        continue
      fi
      
      printf "Reading file at \"%s\".\n" "${settings_file}"
      local file_content
      file_content=$(<"${settings_file}")
      
      if [[ -z "${file_content}" ]]; then
        printf "Settings file is empty. Treating it as default empty JSON object.\n"
        file_content='{}'
      fi

      local json_object
      if ! json_object=$(echo "${file_content}" | python3 -c 'import sys, json; print(json.load(sys.stdin))'); then
        printf "Error: Invalid JSON format in the settings file: \"%s\".\n" "${settings_file}" >&2
        continue
      fi

      if [[ -z "${json_object}" || $(echo "${json_object}" | python3 -c "import sys, json; print('${property_name}' not in json.load(sys.stdin))") == "True" ]]; then
        printf "Settings \"%s\" is not configured.\n" "${property_name}"
      else
        local existing_value
        existing_value=$(echo "${json_object}" | python3 -c "import sys, json; print(json.load(sys.stdin)['${property_name}'])")
        
        if [[ "${existing_value}" == "${target_value}" ]]; then
          printf "Warning: Skipping, \"%s\" is already configured as %s.\n" "${property_name}" "${target_value}"
          continue
        fi
        
        printf "Setting \"%s\" has unexpected value %s that will be changed.\n" "${property_name}" "${existing_value}"
      fi

      # Update the JSON object with the new value
      json_object=$(echo "${json_object}" | python3 -c "import sys, json; obj = json.load(sys.stdin); obj['${property_name}'] = ${target_value}; print(json.dumps(obj, indent=2))")
      
      # Write the updated content back to the settings file
      echo "${json_object}" >"${settings_file}"
      printf "Info: Successfully configured \"%s\" to %s.\n" "${property_name}" "${target_value}"
    done
  }

# Update telemetry settings

  update_telemetry_setting 'telemetry.telemetryLevel' '"off"'
  update_telemetry_setting 'telemetry.enableTelemetry' 'false'
  update_telemetry_setting 'telemetry.enableCrashReporter' 'false'
fi

# ----------------------------------------------------------

# Disable online experiments by Microsoft in Visual Studio Code

printf "Info: --- Disable online experiments by Microsoft in Visual Studio Code\n"

# Check if Python 3 is available

if ! command -v "python3" &> /dev/null; then
  printf "Warning: Skipping because \"python3\" is not found.\n"
else
  python3 <<EOF
from pathlib import Path
import os
import json
import sys

property_name = 'workbench.enableExperiments'
target = False  # Target value to disable experiments
home_dir = f'/home/{os.getenv("SUDO_USER", os.getenv("USER"))}'

settings_files = [

# Global installation (also Snap that installs with "--classic" flag)

  f'{home_dir}/.config/Code/User/settings.json',

# Flatpak installation

  f'{home_dir}/.var/app/com.visualstudio.code/config/Code/User/settings.json'
]

for settings_file in settings_files:
  file = Path(settings_file)
  
  if not file.is_file():
    print(f'Warning: Skipping, file does not exist at "{settings_file}".')
    continue
  
  print(f'Reading file at "{settings_file}".')
  
  try:
    file_content = file.read_text()
    if not file_content.strip():
      print('Settings file is empty. Treating it as default empty JSON object.')
      file_content = '{}'

    json_object = json.loads(file_content)
    
    if property_name not in json_object:
      print(f'Settings "{property_name}" is not configured.')
    else:
      existing_value = json_object[property_name]
      if existing_value == target:
        print(f'Warning: Skipping, "{property_name}" is already configured as {target}.')
        continue
      
      print(f'Setting "{property_name}" has unexpected value {existing_value} that will be changed.')
    
    json_object[property_name] = target
    new_content = json.dumps(json_object, indent=2)
    file.write_text(new_content)
    print(f'Info: Successfully configured "{property_name}" to {target}.')
  
  except json.JSONDecodeError:
    print(f'Error: Invalid JSON format in the settings file: "{settings_file}".', file=sys.stderr)
EOF
fi

# ----------------------------------------------------------

# ----------------------------------------------------------

# ------------Enable Firefox tracking protection------------

# ----------------------------------------------------------

# Enable Firefox tracking protection

printf "Info: --- Enable Firefox tracking protection\n"

# Preference name and value

pref_name="privacy.trackingprotection.enabled"
pref_value="true"
printf "Info: Setting preference \"%s\" to \"%s\".\n" "${pref_name}" "${pref_value}"

# Declare an array of possible profile paths

declare -a profile_paths=(
  "${HOME}/.mozilla/firefox/"*"/"
  "${HOME}/.var/app/org.mozilla.firefox/.mozilla/firefox/"*"/"
  "${HOME}/snap/firefox/common/.mozilla/firefox/"*"/"
)

# Initialize a counter for found profiles

declare -i total_profiles_found=0

# Iterate through each profile directory

for profile_dir in "${profile_paths[@]}"; do
  if [ ! -d "${profile_dir}" ]; then
    continue
  fi
  
# Check if the directory is a valid Firefox profile folder

  if [[ ! "$(basename "${profile_dir}")" =~ ^[a-z0-9]{8}\..+ ]]; then
    continue # Not a profile folder
  fi

  ((total_profiles_found++))
  user_js_file="${profile_dir}user.js"
  printf "%s:\n" "${user_js_file}"

# Create user.js file if it doesn't exist

  if [ ! -f "${user_js_file}" ]; then
    touch "${user_js_file}"
    printf "\tCreated new user.js file\n"
  fi

# Prepare preference lines for user.js

  pref_start="user_pref(\"${pref_name}\","
  pref_line="user_pref(\"${pref_name}\", ${pref_value});"

# Check if the preference line already exists and update accordingly

  if ! grep --quiet "^${pref_start}" "${user_js_file}"; then
    printf "\n%s" "${pref_line}" >> "${user_js_file}"
    printf "\tInfo: Successfully added a new preference in %s.\n" "${user_js_file}"
  elif grep --quiet "^${pref_line}$" "${user_js_file}"; then
    printf "\tWarning: Skipping, preference is already set as expected in %s.\n" "${user_js_file}"
  else
    sed --in-place "/^${pref_start}/c\\${pref_line}" "${user_js_file}"
    printf "\tInfo: Successfully replaced the existing incorrect preference in %s.\n" "${user_js_file}"
  fi
done

# Final message based on the number of profiles found

if [ "${total_profiles_found}" -eq 0 ]; then
  printf "No profile folders are found, no changes are made.\n"
else
  printf "Info: Successfully verified preferences in %d profiles.\n" "${total_profiles_found}"
fi

# ----------------------------------------------------------

# Disable WebRTC exposure of your private IP address in Firefox

printf "Info: --- Disable WebRTC exposure of your private IP address in Firefox\n"
pref_name="media.peerconnection.ice.default_address_only"
pref_value="true"
printf "Info: Setting preference \"%s\" to \"%s\".\n" "${pref_name}" "${pref_value}"

# Declare an array of possible profile paths

declare -a profile_paths=(
  "${HOME}/.mozilla/firefox/"*"/"
  "${HOME}/.var/app/org.mozilla.firefox/.mozilla/firefox/"*"/"
  "${HOME}/snap/firefox/common/.mozilla/firefox/"*"/"
)

# Initialize a counter for found profiles

declare -i total_profiles_found=0

# Iterate through each profile directory

for profile_dir in "${profile_paths[@]}"; do
  if [ ! -d "${profile_dir}" ]; then
    continue
  fi
  
# Check if the directory is a valid Firefox profile folder

  if [[ ! "$(basename "${profile_dir}")" =~ ^[a-z0-9]{8}\..+ ]]; then
    continue # Not a profile folder
  fi

  ((total_profiles_found++))
  user_js_file="${profile_dir}user.js"
  printf "%s:\n" "${user_js_file}"

# Create user.js file if it doesn't exist

  if [ ! -f "${user_js_file}" ]; then
    touch "${user_js_file}"
    printf "\tInfo: Created new user.js file\n"
  fi

# Prepare preference lines for user.js

  pref_start="user_pref(\"${pref_name}\","
  pref_line="user_pref(\"${pref_name}\", ${pref_value});"

# Check if the preference line already exists and update accordingly

  if ! grep --quiet "^${pref_start}" "${user_js_file}"; then
    printf "\n%s" "${pref_line}" >> "${user_js_file}"
    printf "\tInfo: Successfully added a new preference in %s.\n" "${user_js_file}"
  elif grep --quiet "^${pref_line}$" "${user_js_file}"; then
    printf "\tWarning: Skipping, preference is already set as expected in %s.\n" "${user_js_file}"
  else
    sed --in-place "/^${pref_start}/c\\${pref_line}" "${user_js_file}"
    printf "\tInfo: Successfully replaced the existing incorrect preference in %s.\n" "${user_js_file}"
  fi
done

# Final message based on the number of profiles found

if [ "${total_profiles_found}" -eq 0 ]; then
  printf "Warning: No profile folders are found, no changes are made.\n"
else
  printf "Info: Successfully verified preferences in %d profiles.\n" "${total_profiles_found}"
fi

# ----------------------------------------------------------

# Disable collection of technical and interaction data in Firefox

printf "Info: --- Disable collection of technical and interaction data in Firefox\n"
pref_name="datareporting.healthreport.uploadEnabled"
pref_value="false"
printf "Info: Setting preference \"%s\" to \"%s\".\n" "${pref_name}" "${pref_value}"

# Reset the total profiles found counter for the second part

total_profiles_found=0

# Iterate through each profile directory again for the second preference

for profile_dir in "${profile_paths[@]}"; do
  if [ ! -d "${profile_dir}" ]; then
    continue
  fi
  
# Check if the directory is a valid Firefox profile folder

  if [[ ! "$(basename "${profile_dir}")" =~ ^[a-z0-9]{8}\..+ ]]; then
    continue # Not a profile folder
  fi

  ((total_profiles_found++))
  user_js_file="${profile_dir}user.js"
  printf "%s:\n" "${user_js_file}"

# Create user.js file if it doesn't exist (again)

  if [ ! -f "${user_js_file}" ]; then
    touch "${user_js_file}"
    printf "\tInfo: Created new user.js file\n"
  fi

# Prepare preference lines for user.js for the second preference

  pref_start="user_pref(\"${pref_name}\","
  pref_line="user_pref(\"${pref_name}\", ${pref_value});"

# Check if the preference line already exists and update accordingly for the second preference

  if ! grep --quiet "^${pref_start}" "${user_js_file}"; then
    printf "\n%s" "${pref_line}" >> "${user_js_file}"
    printf "\tInfo: Successfully added a new preference in %s.\n" "${user_js_file}"
  elif grep --quiet "^${pref_line}$" "${user_js_file}"; then
    printf "\tWarning: Skipping, preference is already set as expected in %s.\n" "${user_js_file}"
  else
    sed --in-place "/^${pref_start}/c\\${pref_line}" "${user_js_file}"
    printf "\tInfo: Successfully replaced the existing incorrect preference in %s.\n" "${user_js_file}"
  fi
done

# Final message based on the number of profiles found for the second part

if [ "${total_profiles_found}" -eq 0 ]; then
  printf "Warning: No profile folders are found, no changes are made.\n"
else
  printf "Info: Successfully verified preferences in %d profiles.\n" "${total_profiles_found}"
fi

# ----------------------------------------------------------

# ----------------------------------------------------------

# -----Disable detailed telemetry collection in Firefox-----

# ----------------------------------------------------------

# Disable detailed telemetry collection in Firefox

printf "Info: --- Disable detailed telemetry collection in Firefox\n"
pref_name="toolkit.telemetry.enabled"
pref_value="false"
printf "Info: Setting preference \"%s\" to \"%s\".\n" "${pref_name}" "${pref_value}"

# Declare an array of possible profile paths

declare -a profile_paths=(
  "${HOME}/.mozilla/firefox/"*"/"
  "${HOME}/.var/app/org.mozilla.firefox/.mozilla/firefox/"*"/"
  "${HOME}/snap/firefox/common/.mozilla/firefox/"*"/"
)

# Initialize a counter for found profiles

declare -i total_profiles_found=0

# Iterate through each profile directory

for profile_dir in "${profile_paths[@]}"; do
  if [ ! -d "${profile_dir}" ]; then
    continue
  fi
  
# Check if the directory is a valid Firefox profile folder

  if [[ ! "$(basename "${profile_dir}")" =~ ^[a-z0-9]{8}\..+ ]]; then
    continue # Not a profile folder
  fi

  ((total_profiles_found++))
  user_js_file="${profile_dir}user.js"
  printf "%s:\n" "${user_js_file}"

# Create user.js file if it doesn't exist

  if [ ! -f "${user_js_file}" ]; then
    touch "${user_js_file}"
    printf "\tInfo: Created new user.js file\n"
  fi

# Prepare preference lines for user.js

  pref_start="user_pref(\"${pref_name}\","
  pref_line="user_pref(\"${pref_name}\", ${pref_value});"

# Check if the preference line already exists and update accordingly

  if ! grep --quiet "^${pref_start}" "${user_js_file}"; then
    printf "\n%s" "${pref_line}" >> "${user_js_file}"
    printf "\tInfo: Successfully added a new preference in %s.\n" "${user_js_file}"
  elif grep --quiet "^${pref_line}$" "${user_js_file}"; then
    printf "\tWarning: Skipping, preference is already set as expected in %s.\n" "${user_js_file}"
  else
    sed --in-place "/^${pref_start}/c\\${pref_line}" "${user_js_file}"
    printf "\tInfo: Successfully replaced the existing incorrect preference in %s.\n" "${user_js_file}"
  fi
done

# Final message based on the number of profiles found

if [ "${total_profiles_found}" -eq 0 ]; then
  printf "Warning: No profile folders are found, no changes are made.\n"
else
  printf "Info: Successfully verified preferences in %d profiles.\n" "${total_profiles_found}"
fi

# ----------------------------------------------------------

# Disable archiving of Firefox telemetry

printf "Info: --- Disable archiving of Firefox telemetry\n"
pref_name="toolkit.telemetry.archive.enabled"
pref_value="false"
printf "Info: Setting preference \"%s\" to \"%s\".\n" "${pref_name}" "${pref_value}"

# Reset the total profiles found counter for the second part

total_profiles_found=0

# Iterate through each profile directory again for the second preference

for profile_dir in "${profile_paths[@]}"; do
  if [ ! -d "${profile_dir}" ]; then
    continue
  fi
  
# Check if the directory is a valid Firefox profile folder

  if [[ ! "$(basename "${profile_dir}")" =~ ^[a-z0-9]{8}\..+ ]]; then
    continue # Not a profile folder
  fi

  ((total_profiles_found++))
  user_js_file="${profile_dir}user.js"
  printf "%s:\n" "${user_js_file}"

# Create user.js file if it doesn't exist (again)

  if [ ! -f "${user_js_file}" ]; then
    touch "${user_js_file}"
    printf "\tInfo: Created new user.js file\n"
  fi

# Prepare preference lines for user.js for the second preference

  pref_start="user_pref(\"${pref_name}\","
  pref_line="user_pref(\"${pref_name}\", ${pref_value});"

# Check if the preference line already exists and update accordingly for the second preference

  if ! grep --quiet "^${pref_start}" "${user_js_file}"; then
    printf "\n%s" "${pref_line}" >> "${user_js_file}"
    printf "\tInfo: Successfully added a new preference in %s.\n" "${user_js_file}"
  elif grep --quiet "^${pref_line}$" "${user_js_file}"; then
    printf "\tWarning: Skipping, preference is already set as expected in %s.\n" "${user_js_file}"
  else
    sed --in-place "/^${pref_start}/c\\${pref_line}" "${user_js_file}"
    printf "\tInfo: Successfully replaced the existing incorrect preference in %s.\n" "${user_js_file}"
  fi
done

# Final message based on the number of profiles found for the second part

if [ "${total_profiles_found}" -eq 0 ]; then
  printf "Warning: No profile folders are found, no changes are made.\n"
else
  printf "Info: Successfully verified preferences in %d profiles.\n" "${total_profiles_found}"
fi

# ----------------------------------------------------------

# ----------------------------------------------------------

# ------------Disable Firefox unified telemetry-------------

# ----------------------------------------------------------

printf "Info: --- Disable Firefox unified telemetry\n"
pref_name="toolkit.telemetry.unified"
pref_value="false"
printf "Info: Setting preference \"%s\" to \"%s\".\n" "${pref_name}" "${pref_value}"
declare -a profile_paths=(
  "${HOME}/.mozilla/firefox/"*"/"
  "${HOME}/.var/app/org.mozilla.firefox/.mozilla/firefox/"*"/"
  "${HOME}/snap/firefox/common/.mozilla/firefox/"*"/"
)
declare -i total_profiles_found=0
for profile_dir in "${profile_paths[@]}"; do
  if [ ! -d "${profile_dir}" ]; then
    continue
  fi
  if [[ ! "$(basename "${profile_dir}")" =~ ^[a-z0-9]{8}\..+ ]]; then
    continue # Not a profile folder
  fi
  ((total_profiles_found++))
  user_js_file="${profile_dir}user.js"
  printf "%s:\n" "${user_js_file}"
  if [ ! -f "${user_js_file}" ]; then
    touch "${user_js_file}"
    printf "\tCreated new user.js file\n"
  fi
  pref_start="user_pref(\"${pref_name}\","
  pref_line="user_pref(\"${pref_name}\", ${pref_value});"
  if ! grep --quiet "^${pref_start}" "${user_js_file}"; then
    printf "\n%s" "${pref_line}" >> "${user_js_file}"
    printf "\tInfo: Successfully added a new preference in %s.\n" "${user_js_file}"
  elif grep --quiet "^${pref_line}$" "${user_js_file}"; then
    printf "\tWarning: Skipping, preference is already set as expected in %s.\n" "${user_js_file}"
  else
    sed --in-place "/^${pref_start}/c\\${pref_line}" "${user_js_file}"
    printf "\tInfo: Successfully replaced the existing incorrect preference in %s.\n" "${user_js_file}"
  fi
done
if [ "${total_profiles_found}" -eq 0 ]; then
  printf "No profile folders are found, no changes are made.\n"
else
  printf "Info: Successfully verified preferences in %d profiles.\n" "${total_profiles_found}"
fi

# ----------------------------------------------------------

# ----------------------------------------------------------

# -------------Clear Firefox telemetry user ID--------------

# ----------------------------------------------------------

# Clear Firefox telemetry user ID

printf "Info: --- Clear Firefox telemetry user ID\n"
pref_name="toolkit.telemetry.cachedClientID"
pref_value="\"\""
printf "Info: Setting preference \"%s\" to %s.\n" "${pref_name}" "${pref_value}"

# Declare an array of possible profile paths

declare -a profile_paths=(
  "${HOME}/.mozilla/firefox/"*"/"
  "${HOME}/.var/app/org.mozilla.firefox/.mozilla/firefox/"*"/"
  "${HOME}/snap/firefox/common/.mozilla/firefox/"*"/"
)

# Initialize a counter for found profiles

declare -i total_profiles_found=0

# Function to update preferences in user.js files

update_preference() {
  local pref_name="${1}"
  local pref_value="${2}"
  
  printf "Info: Setting preference \"%s\" to \"%s\".\n" "${pref_name}" "${pref_value}"

  for profile_dir in "${profile_paths[@]}"; do
    if [ ! -d "${profile_dir}" ]; then
      continue
    fi

    # Check if the directory is a valid Firefox profile folder
    if [[ ! "$(basename "${profile_dir}")" =~ ^[a-z0-9]{8}\..+ ]]; then
      continue # Not a profile folder
    fi

    ((total_profiles_found++))
    user_js_file="${profile_dir}user.js"
    printf "%s:\n" "${user_js_file}"

    # Create user.js file if it doesn't exist
    if [ ! -f "${user_js_file}" ]; then
      touch "${user_js_file}"
      printf "\tInfo: Created new user.js file\n"
    fi

    # Prepare preference lines for user.js
    pref_start="user_pref(\"${pref_name}\","
    pref_line="user_pref(\"${pref_name}\", ${pref_value});"

    # Check if the preference line already exists and update accordingly
    if ! grep --quiet "^${pref_start}" "${user_js_file}"; then
      printf "\n%s" "${pref_line}" >> "${user_js_file}"
      printf "\tInfo: Successfully added a new preference in %s.\n" "${user_js_file}"
    elif grep --quiet "^${pref_line}$" "${user_js_file}"; then
      printf "\tWarning: Skipping, preference is already set as expected in %s.\n" "${user_js_file}"
    else
      sed --in-place "/^${pref_start}/c\\${pref_line}" "${user_js_file}"
      printf "\tInfo: Successfully replaced the existing incorrect preference in %s.\n" "${user_js_file}"
    fi
  done

# Final message based on the number of profiles found

  if [ "${total_profiles_found}" -eq 0 ]; then
    printf "Warning: No profile folders are found, no changes are made.\n"
  else
    printf "Info: Successfully verified preferences in %d profiles.\n" "${total_profiles_found}"
  fi
}

# Update telemetry user ID preference

update_preference "toolkit.telemetry.cachedClientID" "\"\""

# ----------------------------------------------------------

# Disable Firefox Pioneer study monitoring

printf "Info: --- Disable Firefox Pioneer study monitoring\n"
update_preference "toolkit.telemetry.pioneer-new-studies-available" "false"

# ----------------------------------------------------------

# ----------------------------------------------------------

# -------------Clear Firefox pioneer program ID-------------

# ----------------------------------------------------------

# Clear Firefox pioneer program ID

printf "Info: --- Clear Firefox pioneer program ID\n"
pref_name="toolkit.telemetry.pioneerId"
pref_value="\"\""
printf "Info: Setting preference \"%s\" to %s.\n" "${pref_name}" "${pref_value}"

# Declare an array of possible profile paths

declare -a profile_paths=(
  "${HOME}/.mozilla/firefox/"*"/"
  "${HOME}/.var/app/org.mozilla.firefox/.mozilla/firefox/"*"/"
  "${HOME}/snap/firefox/common/.mozilla/firefox/"*"/"
)

# Initialize a counter for found profiles

declare -i total_profiles_found=0

# Function to update preferences in user.js files

update_preference() {
  local pref_name="${1}"
  local pref_value="${2}"

  printf "Info: Setting preference \"%s\" to \"%s\".\n" "${pref_name}" "${pref_value}"

  for profile_dir in "${profile_paths[@]}"; do
    if [ ! -d "${profile_dir}" ]; then
      continue
    fi

    # Check if the directory is a valid Firefox profile folder
    if [[ ! "$(basename "${profile_dir}")" =~ ^[a-z0-9]{8}\..+ ]]; then
      continue # Not a profile folder
    fi

    ((total_profiles_found++))
    user_js_file="${profile_dir}user.js"
    printf "%s:\n" "${user_js_file}"

    # Create user.js file if it doesn't exist
    if [ ! -f "${user_js_file}" ]; then
      touch "${user_js_file}"
      printf "\tInfo: Created new user.js file\n"
    fi

    # Prepare preference lines for user.js
    pref_start="user_pref(\"${pref_name}\","
    pref_line="user_pref(\"${pref_name}\", ${pref_value});"

    # Check if the preference line already exists and update accordingly
    if ! grep --quiet "^${pref_start}" "${user_js_file}"; then
      printf "\n%s" "${pref_line}" >> "${user_js_file}"
      printf "\tInfo: Successfully added a new preference in %s.\n" "${user_js_file}"
    elif grep --quiet "^${pref_line}$" "${user_js_file}"; then
      printf "\tWarning: Skipping, preference is already set as expected in %s.\n" "${user_js_file}"
    else
      sed --in-place "/^${pref_start}/c\\${pref_line}" "${user_js_file}"
      printf "\tInfo: Successfully replaced the existing incorrect preference in %s.\n" "${user_js_file}"
    fi
  done

# Final message based on the number of profiles found

  if [ "${total_profiles_found}" -eq 0 ]; then
    printf "Warning: No profile folders are found, no changes are made.\n"
  else
    printf "Info: Successfully verified preferences in %d profiles.\n" "${total_profiles_found}"
  fi
}

# Update pioneer program ID preference

update_preference "toolkit.telemetry.pioneerId" "\"\""

# ----------------------------------------------------------

# Enable dynamic First-Party Isolation (dFPI)

printf "Info: --- Enable dynamic First-Party Isolation (dFPI)\n"
update_preference "network.cookie.cookieBehavior" "5"

# ----------------------------------------------------------

# ----------------------------------------------------------

# -----------Enable Firefox network partitioning------------

# ----------------------------------------------------------

# Enable Firefox network partitioning

printf "Info: --- Enable Firefox network partitioning\n"
pref_name="privacy.partition.network_state"
pref_value="true"
printf "Info: Setting preference \"%s\" to \"%s\".\n" "${pref_name}" "${pref_value}"

# Declare an array of possible profile paths

declare -a profile_paths=(
  "${HOME}/.mozilla/firefox/"*"/"
  "${HOME}/.var/app/org.mozilla.firefox/.mozilla/firefox/"*"/"
  "${HOME}/snap/firefox/common/.mozilla/firefox/"*"/"
)

# Initialize a counter for found profiles

declare -i total_profiles_found=0

# Function to update preferences in user.js files

update_preference() {
  local pref_name="${1}"
  local pref_value="${2}"

  printf "Info: Setting preference \"%s\" to \"%s\".\n" "${pref_name}" "${pref_value}"

  for profile_dir in "${profile_paths[@]}"; do
    if [ ! -d "${profile_dir}" ]; then
      continue
    fi

    # Check if the directory is a valid Firefox profile folder
    if [[ ! "$(basename "${profile_dir}")" =~ ^[a-z0-9]{8}\..+ ]]; then
      continue # Not a profile folder
    fi

    ((total_profiles_found++))
    user_js_file="${profile_dir}user.js"
    printf "%s:\n" "${user_js_file}"

    # Create user.js file if it doesn't exist
    if [ ! -f "${user_js_file}" ]; then
      touch "${user_js_file}"
      printf "\tInfo: Created new user.js file\n"
    fi

    # Prepare preference lines for user.js
    pref_start="user_pref(\"${pref_name}\","
    pref_line="user_pref(\"${pref_name}\", ${pref_value});"

    # Check if the preference line already exists and update accordingly
    if ! grep --quiet "^${pref_start}" "${user_js_file}"; then
      printf "\n%s" "${pref_line}" >> "${user_js_file}"
      printf "\tInfo: Successfully added a new preference in %s.\n" "${user_js_file}"
    elif grep --quiet "^${pref_line}$" "${user_js_file}"; then
      printf "\tWarning: Skipping, preference is already set as expected in %s.\n" "${user_js_file}"
    else
      sed --in-place "/^${pref_start}/c\\${pref_line}" "${user_js_file}"
      printf "\tInfo: Successfully replaced the existing incorrect preference in %s.\n" "${user_js_file}"
    fi
  done

# Final message based on the number of profiles found

  if [ "${total_profiles_found}" -eq 0 ]; then
    printf "Warning: No profile folders are found, no changes are made.\n"
  else
    printf "Info: Successfully verified preferences in %d profiles.\n" "${total_profiles_found}"
  fi
}

# Update network partitioning preference

update_preference "privacy.partition.network_state" "true"

# ----------------------------------------------------------

# Minimize Firefox telemetry logging verbosity

printf "Info: --- Minimize Firefox telemetry logging verbosity\n"
update_preference "toolkit.telemetry.log.level" "\"Fatal\""

# ----------------------------------------------------------

# ----------------------------------------------------------

# -----------Disable Firefox telemetry log output-----------

# ----------------------------------------------------------

printf "Info: --- Disable Firefox telemetry log output\n"
pref_name="toolkit.telemetry.log.dump"
pref_value="\"Fatal\""
printf "Info: Setting preference \"%s\" to %s.\n" "${pref_name}" "${pref_value}"
declare -a profile_paths=(
  "${HOME}/.mozilla/firefox/"*"/"
  "${HOME}/.var/app/org.mozilla.firefox/.mozilla/firefox/"*"/"
  "${HOME}/snap/firefox/common/.mozilla/firefox/"*"/"
)
declare -i total_profiles_found=0
for profile_dir in "${profile_paths[@]}"; do
  if [ ! -d "${profile_dir}" ]; then
    continue
  fi
  if [[ ! "$(basename "${profile_dir}")" =~ ^[a-z0-9]{8}\..+ ]]; then
    continue # Not a profile folder
  fi
  ((total_profiles_found++))
  user_js_file="${profile_dir}user.js"
  printf "%s:\n" "${user_js_file}"
  if [ ! -f "${user_js_file}" ]; then
    touch "${user_js_file}"
    printf "\tCreated new user.js file\n"
  fi
  pref_start="user_pref(\"${pref_name}\","
  pref_line="user_pref(\"${pref_name}\", ${pref_value});"
  if ! grep --quiet "^${pref_start}" "${user_js_file}"; then
    printf "\n%s" "${pref_line}" >> "${user_js_file}"
    printf "\tInfo: Successfully added a new preference in %s.\n" "${user_js_file}"
  elif grep --quiet "^${pref_line}$" "${user_js_file}"; then
    printf "\tWarning: Skipping, preference is already set as expected in %s.\n" "${user_js_file}"
  else
    sed --in-place "/^${pref_start}/c\\${pref_line}" "${user_js_file}"
    printf "\tInfo: Successfully replaced the existing incorrect preference in %s.\n" "${user_js_file}"
  fi
done
if [ "${total_profiles_found}" -eq 0 ]; then
  printf "No profile folders are found, no changes are made.\n"
else
  printf "Info: Successfully verified preferences in %d profiles.\n" "${total_profiles_found}"
fi

# ----------------------------------------------------------

# ----------------------------------------------------------

# --------Disable pings to Firefox telemetry server---------

# ----------------------------------------------------------

# Disable pings to Firefox telemetry server

printf "Info: --- Disable pings to Firefox telemetry server\n"
pref_name="toolkit.telemetry.server"
pref_value="\"\""
printf "Info: Setting preference \"%s\" to %s.\n" "${pref_name}" "${pref_value}"

# Declare an array of possible profile paths

declare -a profile_paths=(
  "${HOME}/.mozilla/firefox/"*"/"
  "${HOME}/.var/app/org.mozilla.firefox/.mozilla/firefox/"*"/"
  "${HOME}/snap/firefox/common/.mozilla/firefox/"*"/"
)

# Initialize a counter for found profiles

declare -i total_profiles_found=0

# Function to update preferences in user.js files

update_preference() {
  local pref_name="${1}"
  local pref_value="${2}"

  printf "Info: Setting preference \"%s\" to %s.\n" "${pref_name}" "${pref_value}"

  for profile_dir in "${profile_paths[@]}"; do
    if [ ! -d "${profile_dir}" ]; then
      continue
    fi

    # Check if the directory is a valid Firefox profile folder
    if [[ ! "$(basename "${profile_dir}")" =~ ^[a-z0-9]{8}\..+ ]]; then
      continue # Not a profile folder
    fi

    ((total_profiles_found++))
    user_js_file="${profile_dir}user.js"
    printf "%s:\n" "${user_js_file}"

    # Create user.js file if it doesn't exist
    if [ ! -f "${user_js_file}" ]; then
      touch "${user_js_file}"
      printf "\tInfo: Created new user.js file\n"
    fi

    # Prepare preference lines for user.js
    pref_start="user_pref(\"${pref_name}\","
    pref_line="user_pref(\"${pref_name}\", ${pref_value});"

    # Check if the preference line already exists and update accordingly
    if ! grep --quiet "^${pref_start}" "${user_js_file}"; then
      printf "\n%s" "${pref_line}" >> "${user_js_file}"
      printf "\tInfo: Successfully added a new preference in %s.\n" "${user_js_file}"
    elif grep --quiet "^${pref_line}$" "${user_js_file}"; then
      printf "\tWarning: Skipping, preference is already set as expected in %s.\n" "${user_js_file}"
    else
      sed --in-place "/^${pref_start}/c\\${pref_line}" "${user_js_file}"
      printf "\tInfo: Successfully replaced the existing incorrect preference in %s.\n" "${user_js_file}"
    fi
  done

# Final message based on the number of profiles found

  if [ "${total_profiles_found}" -eq 0 ]; then
    printf "Warning: No profile folders are found, no changes are made.\n"
  else
    printf "Info: Successfully verified preferences in %d profiles.\n" "${total_profiles_found}"
  fi
}

# Update telemetry server preference

update_preference "toolkit.telemetry.server" "\"\""

# ----------------------------------------------------------

# Disable Firefox shutdown ping

printf "Info: --- Disable Firefox shutdown ping\n"
update_preference "toolkit.telemetry.shutdownPingSender.enabled" "false"

# ----------------------------------------------------------

# -------Minimize Firefox telemetry logging verbosity-------

# ----------------------------------------------------------

printf "Info: --- Minimize Firefox telemetry logging verbosity\n"
pref_name="toolkit.telemetry.log.level"
pref_value="\"Fatal\""
printf "Info: Setting preference \"%s\" to %s.\n" "${pref_name}" "${pref_value}"
declare -a profile_paths=(
  "${HOME}/.mozilla/firefox/"*"/"
  "${HOME}/.var/app/org.mozilla.firefox/.mozilla/firefox/"*"/"
  "${HOME}/snap/firefox/common/.mozilla/firefox/"*"/"
)
declare -i total_profiles_found=0
for profile_dir in "${profile_paths[@]}"; do
  if [ ! -d "${profile_dir}" ]; then
    continue
  fi
  if [[ ! "$(basename "${profile_dir}")" =~ ^[a-z0-9]{8}\..+ ]]; then
    continue # Not a profile folder
  fi
  ((total_profiles_found++))
  user_js_file="${profile_dir}user.js"
  printf "%s:\n" "${user_js_file}"
  if [ ! -f "${user_js_file}" ]; then
    touch "${user_js_file}"
    printf "\tCreated new user.js file\n"
  fi
  pref_start="user_pref(\"${pref_name}\","
  pref_line="user_pref(\"${pref_name}\", ${pref_value});"
  if ! grep --quiet "^${pref_start}" "${user_js_file}"; then
    printf "\n%s" "${pref_line}" >> "${user_js_file}"
    printf "\tInfo: Successfully added a new preference in %s.\n" "${user_js_file}"
  elif grep --quiet "^${pref_line}$" "${user_js_file}"; then
    printf "\tWarning: Skipping, preference is already set as expected in %s.\n" "${user_js_file}"
  else
    sed --in-place "/^${pref_start}/c\\${pref_line}" "${user_js_file}"
    printf "\tInfo: Successfully replaced the existing incorrect preference in %s.\n" "${user_js_file}"
  fi
done
if [ "${total_profiles_found}" -eq 0 ]; then
  printf "No profile folders are found, no changes are made.\n"
else
  printf "Info: Successfully verified preferences in %d profiles.\n" "${total_profiles_found}"
fi

# ----------------------------------------------------------

# ----------------------------------------------------------

# ---------------Disable Firefox update ping----------------

# ----------------------------------------------------------

printf "Info: --- Disable Firefox update ping\n"
pref_name="toolkit.telemetry.updatePing.enabled"
pref_value="false"
printf "Info: Setting preference \"%s\" to %s.\n" "${pref_name}" "${pref_value}"
declare -a profile_paths=(
  "${HOME}/.mozilla/firefox/"*"/"
  "${HOME}/.var/app/org.mozilla.firefox/.mozilla/firefox/"*"/"
  "${HOME}/snap/firefox/common/.mozilla/firefox/"*"/"
)
declare -i total_profiles_found=0
for profile_dir in "${profile_paths[@]}"; do
  if [ ! -d "${profile_dir}" ]; then
    continue
  fi
  if [[ ! "$(basename "${profile_dir}")" =~ ^[a-z0-9]{8}\..+ ]]; then
    continue # Not a profile folder
  fi
  ((total_profiles_found++))
  user_js_file="${profile_dir}user.js"
  printf "%s:\n" "${user_js_file}"
  if [ ! -f "${user_js_file}" ]; then
    touch "${user_js_file}"
    printf "\tCreated new user.js file\n"
  fi
  pref_start="user_pref(\"${pref_name}\","
  pref_line="user_pref(\"${pref_name}\", ${pref_value});"
  if ! grep --quiet "^${pref_start}" "${user_js_file}"; then
    printf "\n%s" "${pref_line}" >> "${user_js_file}"
    printf "\tInfo: Successfully added a new preference in %s.\n" "${user_js_file}"
  elif grep --quiet "^${pref_line}$" "${user_js_file}"; then
    printf "\tWarning: Skipping, preference is already set as expected in %s.\n" "${user_js_file}"
  else
    sed --in-place "/^${pref_start}/c\\${pref_line}" "${user_js_file}"
    printf "\tInfo: Successfully replaced the existing incorrect preference in %s.\n" "${user_js_file}"
  fi
done
if [ "${total_profiles_found}" -eq 0 ]; then
  printf "Warning: No profile folders are found, no changes are made.\n"
else
  printf "Info: Successfully verified preferences in %d profiles.\n" "${total_profiles_found}"
fi

# ----------------------------------------------------------

# ----------------Disable Firefox prio ping-----------------

# ----------------------------------------------------------

printf "Info: --- Disable Firefox prio ping\n"
pref_name="toolkit.telemetry.prioping.enabled"
pref_value="false"
printf "Info: Setting preference \"%s\" to %s.\n" "${pref_name}" "${pref_value}"
declare -a profile_paths=(
  "${HOME}/.mozilla/firefox/"*"/"
  "${HOME}/.var/app/org.mozilla.firefox/.mozilla/firefox/"*"/"
  "${HOME}/snap/firefox/common/.mozilla/firefox/"*"/"
)
declare -i total_profiles_found=0
for profile_dir in "${profile_paths[@]}"; do
  if [ ! -d "${profile_dir}" ]; then
    continue
  fi
  if [[ ! "$(basename "${profile_dir}")" =~ ^[a-z0-9]{8}\..+ ]]; then
    continue # Not a profile folder
  fi
  ((total_profiles_found++))
  user_js_file="${profile_dir}user.js"
  printf "%s:\n" "${user_js_file}"
  if [ ! -f "${user_js_file}" ]; then
    touch "${user_js_file}"
    printf "\tInfo: Created new user.js file\n"
  fi
  pref_start="user_pref(\"${pref_name}\","
  pref_line="user_pref(\"${pref_name}\", ${pref_value});"
  if ! grep --quiet "^${pref_start}" "${user_js_file}"; then
    printf "\n%s" "${pref_line}" >> "${user_js_file}"
    printf "\tInfo: Successfully added a new preference in %s.\n" "${user_js_file}"
  elif grep --quiet "^${pref_line}$" "${user_js_file}"; then
    printf "\tWarning: Skipping, preference is already set as expected in %s.\n" "${user_js_file}"
  else
    sed --in-place "/^${pref_start}/c\\${pref_line}" "${user_js_file}"
    printf "\tInfo: Successfully replaced the existing incorrect preference in %s.\n" "${user_js_file}"
  fi
done
if [ "${total_profiles_found}" -eq 0 ]; then
  printf "Warning: No profile folders are found, no changes are made.\n"
else
  printf "Info: Successfully verified preferences in %d profiles.\n" "${total_profiles_found}"
fi

printf "Info: Your privacy and security is now hardened 🎉💪\n"
printf "Info: Press any key to exit.\n"
read -r -n 1 -s

cleanup

script_name1="$(basename "${0}")"
printf "\nInfo: script_name1: %s\n" "${script_name1}"
script_path1="$(realpath "$(dirname "${0}")")"
printf "Info: script_path1: %s\n" "${script_path1}"
script_path_with_name="${script_path1}/${script_name1}"
printf "Info: Script path with name: %s\n" "${script_path_with_name}"
printf "Info: Script finished\n"
exit 0

# ----------------------------------------------------------

# ----------------------- Empty trash ----------------------

# ----------------------------------------------------------

# Empty trash

printf "Info: --- Empty trash\n"

# Function to safely remove files from a directory if it exists

empty_trash() {
  local path="${1}"
  if [ -d "$path" ]; then
    printf "Info: Emptying trash at %s\n" "$path"
    rm -r -f -v "${path}"/*
  else
    printf "Warning: Directory %s does not exist, skipping.\n" "$path"
  fi
}

# Empty user trash

empty_trash "${HOME}/.local/share/Trash"

# Empty root trash (requires sudo)

if [ -d "/root/.local/share/Trash" ]; then
  printf "Info: Emptying root trash\n"
  sudo rm -r -f -v /root/.local/share/Trash/*
else
  printf "Warning: Root directory /root/.local/share/Trash does not exist, skipping.\n"
fi

# Empty Snap trash

empty_trash "${HOME}/snap/*/.local/share/Trash"

# Empty Flatpak trash (apps may not choose to use Portal API)

empty_trash "${HOME}/.var/app/*/data/Trash"

# ----------------------------------------------------------

# ----------------------------------------------------------

# -------------- Clear global temporary folders ------------

# ----------------------------------------------------------

# Clear global temporary folders

printf "Info: --- Clear global temporary folders\n"

# Function to safely remove files from a directory if it exists

# Function to safely remove contents of a directory if it exists

clear_temp_folder() {
  local path="${1}"
  if [ -d "${path}" ]; then
    printf "Info: Clearing contents of %s\n" "${path}"
    sudo rm -r -f -v "${path}"/*
  else
    printf "Warning: Directory %s does not exist, skipping.\n" "${path}"
  fi
}

# Clear /tmp folder

clear_temp_folder "/tmp"

# Clear /var/tmp folder

clear_temp_folder "/var/tmp"

# ----------------------------------------------------------

# ----------------------------------------------------------

# -------------------- Clear screenshots -------------------

# ----------------------------------------------------------

printf "Info: --- Clear screenshots\n"

# Clear default directory for GNOME screenshots

rm -r -f -v "${HOME}/Pictures/Screenshots/"*
if [ -d "${HOME}/Pictures" ]; then

# Clear Ubuntu screenshots

  find "${HOME}/Pictures" -name 'Screenshot from *.png' | while read -r file_path; do
    rm -f -v "$file_path" # E.g. Screenshot from 2022-08-20 02-46-41.png
  done

# Clear KDE (Spectacle) screenshots

  find "${HOME}/Pictures" -name 'Screenshot_*' | while read -r file_path; do
    rm -f -v "$file_path" # E.g. Screenshot_20220927_205646.png
  done
fi

# Clear ksnip screenshots

find "${HOME}" -name 'ksnip_*' | while read -r file_path; do
  rm -f -v "$file_path" # E.g. ksnip_20220927-195151.png
done

# ----------------------------------------------------------

# Disable connectivity checks (breaks Captive Portal detection)

printf "Info: --- Disable connectivity checks (breaks Captive Portal detection)\n"
if ! command -v '/usr/sbin/NetworkManager' &> /dev/null; then
  printf "Warning: Skipping because \"/usr/sbin/NetworkManager\" is not found.\n"
else
  file='/etc/NetworkManager/conf.d/20-disable-connectivity-ndaal-cologne.conf'
  content=$'# Created by ndaal.cologne\n[connectivity]\nenabled=false'
  directory="${file%/*}"
  mkdir -p "${directory}"
  if [ -f "$file" ]; then
    printf "Warning: Skipping, connectivity checks are already disabled through %s.\n" "$file"
  else
    printf "%s" "$content" | sudo tee "$file" > /dev/null
    printf "Info: Successfully disabled connectivity checks.\n"
  fi
  if command -v 'nmcli' &> /dev/null; then
    sudo nmcli general reload
    printf "Info: Successfully reloaded configuration.\n"
  else
    printf "Info: It will take effect after reboot.\n"
  fi
fi

# ----------------------------------------------------------

# ----------------------------------------------------------

# --Disable Python history for future interactive commands--

# ----------------------------------------------------------

printf "Info: --- Disable Python history for future interactive commands\n"
history_file="${HOME}/.python_history"
if [ ! -f "${history_file}" ]; then
  touch "${history_file}"
  printf "Info: Created %s.\n" "${history_file}"
fi
sudo chattr +i "$(realpath "${history_file}")" # realpath in case of symlink

# ----------------------------------------------------------

# ----------------------------------------------------------

# ------------Disable PowerShell Core telemetry-------------

# ----------------------------------------------------------

printf "Info: --- Disable PowerShell Core telemetry\n"
variable="POWERSHELL_TELEMETRY_OPTOUT"
value="1"
declaration_file="/etc/environment"
if ! [ -f "${declaration_file}" ]; then
  printf "\"%s\" does not exist.\n" "${declaration_file}"
  sudo touch "${declaration_file}"
  printf "Info: Created %s.\n" "${declaration_file}"
fi
assignment_start="${variable}="
assignment="${variable}=${value}"
if ! grep --quiet "^${assignment_start}" "${declaration_file}"; then
  printf "Info: Variable \"%s\" was not configured before.\n" "$variable"
  printf "\n%s" "$assignment" | sudo tee -a "${declaration_file}" > /dev/null
  printf "Info: Successfully configured (%s).\n" "$assignment"
else
  if grep --quiet "^${assignment}$" "${declaration_file}"; then
    printf "Warning: Skipping. Variable \"%s\" is already set to value \"%s\".\n" "$variable" "$value"
  else
    if ! sudo sed --in-place "/^${assignment_start}/d" "${declaration_file}"; then
      >&2 printf "Error: Failed to delete assignment starting with \"%s\".\n" "$assignment_start"
    else
      printf "Info: Successfully deleted unexpected assignment of \"%s\".\n" "$variable"
      if ! printf "\n%s" "$assignment" | sudo tee -a "${declaration_file}" > /dev/null; then
        >&2 printf "Error: Failed to add assignment \"%s\".\n" "$assignment"
      else
        printf "Info: Successfully reconfigured (%s).\n" "$assignment"
      fi
    fi
  fi
fi

# ----------------------------------------------------------

# -------------------- Clear bash history ------------------

# ----------------------------------------------------------

# Clear bash history

printf "Info: --- Clear bash history\n"

# Function to safely remove a file if it exists

clear_bash_history() {
  local path="${1}"
  if [ -f "${path}" ]; then
    printf "Info: Clearing bash history at %s\n" "${path}"
    rm -f -v "${path}"
  else
    printf "Warning: File %s does not exist, skipping.\n" "${path}"
  fi
}

# Clear user bash history

clear_bash_history "${HOME}/.bash_history"

# Clear root bash history (requires sudo)

if [ -f "/root/.bash_history" ]; then
  printf "Info: Clearing root bash history\n"
  sudo rm -f -v "/root/.bash_history"
else
  printf "Warning: Root file /root/.bash_history does not exist, skipping.\n"
fi

# ----------------------------------------------------------

# ----------------------------------------------------------

# -------------------- Clear Zsh history -------------------

# ----------------------------------------------------------

# Clear Zsh history

printf "Info: --- Clear Zsh history\n"

# Function to safely remove a file if it exists

clear_zsh_history() {
  local path="${1}"
  if [ -f "${path}" ]; then
    printf "Info: Clearing Zsh history at %s\n" "${path}"
    rm -f -v "${path}"
  else
    printf "Warning: File %s does not exist, skipping.\n" "${path}"
  fi
}

# Clear user Zsh history

clear_zsh_history "${HOME}/.zsh_history"

# Clear root Zsh history (requires sudo)

if [ -f "/root/.zsh_history" ]; then
  printf "Info: Clearing root Zsh history\n"
  sudo rm -f -v "/root/.zsh_history"
else
  printf "Warning: Root file /root/.zsh_history does not exist, skipping.\n"
fi

# ----------------------------------------------------------

# ----------------------------------------------------------

# -------------------- Clear tcsh history ------------------

# ----------------------------------------------------------

# Clear tcsh history

printf "Info: --- Clear tcsh history\n"

# Function to safely remove a file if it exists

clear_tcsh_history() {
  local path="${1}"
  if [ -f "${path}" ]; then
    printf "Info: Clearing tcsh history at %s\n" "${path}"
    rm -f -v "${path}"
  else
    printf "Warning: File %s does not exist, skipping.\n" "${path}"
  fi
}

# Clear user tcsh history

clear_tcsh_history "${HOME}/.history"

# Clear root tcsh history (requires sudo)

if [ -f "/root/.history" ]; then
  printf "Info: Clearing root tcsh history\n"
  sudo rm -f -v "/root/.history"
else
  printf "Warning: Root file /root/.history does not exist, skipping.\n"
fi

# ----------------------------------------------------------

# ----------------------------------------------------------

# -------------------- Clear fish history ------------------

# ----------------------------------------------------------

# Clear fish history

printf "Info: --- Clear fish history\n"

# Function to safely remove a file if it exists

clear_fish_history() {
  local path="${1}"
  if [ -f "${path}" ]; then
    printf "Info: Clearing fish history at %s\n" "${path}"
    rm -f -v "${path}"
  else
    printf "Warning: File %s does not exist, skipping.\n" "${path}"
  fi
}

# Clear user fish history from both possible locations

clear_fish_history "${HOME}/.local/share/fish/fish_history"
clear_fish_history "${HOME}/.config/fish/fish_history"

# Clear root fish history (requires sudo)

if [ -f "/root/.local/share/fish/fish_history" ]; then
  printf "Info: Clearing root fish history at /root/.local/share/fish/fish_history\n"
  sudo rm -f -v "/root/.local/share/fish/fish_history"
else
  printf "Warning: Root file /root/.local/share/fish/fish_history does not exist, skipping.\n"
fi

if [ -f "/root/.config/fish/fish_history" ]; then
  printf "Info: Clearing root fish history at /root/.config/fish/fish_history\n"
  sudo rm -f -v "/root/.config/fish/fish_history"
else
  printf "Warning: Root file /root/.config/fish/fish_history does not exist, skipping.\n"
fi

# ----------------------------------------------------------

# ----------------------------------------------------------

# -------------- Clear KornShell (ksh) history -------------

# ----------------------------------------------------------

# Clear KornShell (ksh) history

printf "Info: --- Clear KornShell (ksh) history\n"

# Function to safely remove a file if it exists

clear_ksh_history() {
  local path="${1}"
  if [ -f "${path}" ]; then
    printf "Info: Clearing ksh history at %s\n" "${path}"
    rm -f -v "${path}"
  else
    printf "Warning: File %s does not exist, skipping.\n" "${path}"
  fi
}

# Clear user ksh history

clear_ksh_history "${HOME}/.sh_history"

# Clear root ksh history (requires sudo)

if [ -f "/root/.sh_history" ]; then
  printf "Info: Clearing root ksh history at /root/.sh_history\n"
  sudo rm -f -v "/root/.sh_history"
else
  printf "Warning: Root file /root/.sh_history does not exist, skipping.\n"
fi

# ----------------------------------------------------------

# ----------------------------------------------------------

# -------------------- Clear ash history --------------------

# ----------------------------------------------------------

# Clear ash history

printf "Info: --- Clear ash history\n"

# Function to safely remove a file if it exists

clear_ash_history() {
  local path="${1}"
  if [ -f "${path}" ]; then
    printf "Info: Clearing ash history at %s\n" "${path}"
    rm -f -v "${path}"
  else
    printf "Warning: File %s does not exist, skipping.\n" "${path}"
  fi
}

# Clear user ash history

clear_ash_history "${HOME}/.ash_history"

# Clear root ash history (requires sudo)

if [ -f "/root/.ash_history" ]; then
  printf "Info: Clearing root ash history at /root/.ash_history\n"
  sudo rm -f -v "/root/.ash_history"
else
  printf "Warning: Root file /root/.ash_history does not exist, skipping.\n"
fi

# ----------------------------------------------------------

# ----------------------------------------------------------

# ------------------- Clear crosh history -------------------

# ----------------------------------------------------------

# Clear crosh history

printf "Info: --- Clear crosh history\n"

# Function to safely remove a file if it exists

clear_crosh_history() {
  local path="${1}"
  if [ -f "${path}" ]; then
    printf "Info: Clearing crosh history at %s\n" "${path}"
    rm -f -v "${path}"
  else
    printf "Warning: File %s does not exist, skipping.\n" "${path}"
  fi
}

# Clear user crosh history

clear_crosh_history "${HOME}/.crosh_history"

# Clear root crosh history (requires sudo)

if [ -f "/root/.crosh_history" ]; then
  printf "Info: Clearing root crosh history at /root/.crosh_history\n"
  sudo rm -f -v "/root/.crosh_history"
else
  printf "Warning: Root file /root/.crosh_history does not exist, skipping.\n"
fi

# ----------------------------------------------------------

printf "Info: Your privacy and security is now hardened 🎉💪\n"
printf "Info: Press any key to exit.\n"
read -r -n 1 -s

cleanup

script_name1="$(basename "${0}")"
printf "\nInfo: script_name1: %s\n" "${script_name1}"
script_path1="$(realpath "$(dirname "${0}")")"
printf "Info: script_path1: %s\n" "${script_path1}"
script_path_with_name="${script_path1}/${script_name1}"
printf "Info: Script path with name: %s\n" "${script_path_with_name}"
printf "Info: Script finished\n"
exit 0

Ansible Rolle - ssh-audit
==============================================================================

* Install auditd and test the functionality
* Basis für die Ansible Rolle ist:
  <https://gitlab.com/vPierre/ndaal_public_ansible_role_auditing_ssh>
* reboot

Updating

printf "%s\n" "${array_packages[@]}"

case "${1:-}" in
    -debian|-ubuntu|debian|ubuntu)
        sudo apt update && sudo apt upgrade -y
        ;;
    -arch|arch|pacman)
        sudo pacman -Syu
        ;;
    -mac|-brew|mac|brew)
        brew update && brew upgrade
        ;;
    -zypper|zypper)
        sudo zypper update
        ;;
    -dnf|dnf)
        sudo dnf upgrade --refresh
        ;;
    -rpm|rpm)
        sudo rpm -Uvh *.rpm
        ;;
    -yum|yum)
        sudo yum update
        ;;
    -xbps|xbps)
        sudo xbps-install -Su
        ;;
    -pisi|pisi)
        sudo pisi upgrade
        ;;
    -nix|nix)
        nix-channel --update && nix-env -u
        ;;
    -yay|yay)
        yay -Syu
        ;;
    -equo|equo)
        sudo equo update && sudo equo upgrade
        ;;
    -pip|-pip3|pip|pip3)
        pip freeze --local | awk -F= '{print "pip install -U "$1}' | sh
        pip3 freeze --local | grep -v '^\-e' | cut -d = -f 1  | xargs -n1 pip3 install -U --break-system-packages
        ;;
    -aurman)
        aurman -Syu
        ;;
    -flatpak)
        flatpak update
        ;;
    -snap)
        sudo snap refresh
        ;;
    -h|-help)
        printf "HINT: Select an argument to use a package manager and update all packages. Options include -zypper, -dnf, -rpm, -arch, -xbps, -pisi, -nix, -yay, -equo, -pip, -brew, -flatpak, -snap, etc.\n"
        ;;
    *)
        printf "NOTE: Select an argument to use a package manager and update all packages. Options include -zypper, -dnf, -rpm, -arch, -xbps, -pisi, -nix, -yay, -equo, -pip, -flatpak, -snap, etc.\n"
        ;;
esac

Ansible Rolle - documentation with UAC
==============================================================================

* Placeholder Ansible Rolle
* reboot

Automatische Updates mit unbeaufsichtigten Upgrades unter Debian und Ubuntu
------------------------------------------------------------------------------

<https://www.howtoforge.de/anleitung/automatische-updates-mit-unbeaufsichtigten-upgrades-unter-debian-und-ubuntu/>

Linux-Versionen werden häufig aktualisiert, und Sicherheitsaktualisierungen sollten umgehend installiert werden. Debian und Ubuntu verwenden das APT-System, und obwohl es relativ einfach ist, alle anstehenden Aktualisierungen manuell zu installieren, kann es sein, dass der Administrator nicht daran denkt oder lieber nachts schläft, anstatt nach Updates zu suchen. Dieser Artikel bezieht sich hauptsächlich auf Serverinstallationen.

Das manuelle Aktualisieren und Installieren von Paketen mit apt upgrade ist auch bei der Verwendung von unattended-upgrades möglich. Wenn ein Upgrade, das mit unattended-upgrades gestartet wurde, gerade läuft, wenn apt von der Kommandozeile aus verwendet wird, informiert das apt-System den Benutzer darüber, dass eine Operation bereits läuft und der Benutzer warten muss, bis sie beendet ist, bevor er mit der Eingabe von apt-Befehlen fortfahren kann.

Ein einfacher Weg für vielbeschäftigte Administratoren
Installieren Sie unbeaufsichtigte Upgrades, überprüfen Sie, ob sie aktiviert sind, und überwachen Sie ihre Arbeit anhand von Logdateien.

bash

vagrant@debian-automation-controller:~$ sudo apt-get install unattended-upgrades
Paketlisten werden gelesen… Fertig
Abhängigkeitsbaum wird aufgebaut… Fertig
Statusinformationen werden eingelesen… Fertig
Vorgeschlagene Pakete:
  bsd-mailx default-mta | mail-transport-agent needrestart powermgmt-base python3-gi
Die folgenden NEUEN Pakete werden installiert:
  unattended-upgrades
0 aktualisiert, 1 neu installiert, 0 zu entfernen und 0 nicht aktualisiert.
Es müssen noch 0 B von 63,3 kB an Archiven heruntergeladen werden.
Nach dieser Operation werden 308 kB Plattenplatz zusätzlich benutzt.
Vorkonfiguration der Pakete ...
Vormals nicht ausgewähltes Paket unattended-upgrades wird gewählt.
(Lese Datenbank ... 30416 Dateien und Verzeichnisse sind derzeit installiert.)
Vorbereitung zum Entpacken von .../unattended-upgrades_2.9.1+nmu3_all.deb ...
Entpacken von unattended-upgrades (2.9.1+nmu3) ...
unattended-upgrades (2.9.1+nmu3) wird eingerichtet ...
Trigger für man-db (2.11.2-2) werden verarbeitet ...

sudo dpkg-reconfigure -plow unattended-upgrades

Der dpkg-reconfigure-Befehl zeigt ein Dialogfeld an, in dem Sie gefragt werden, ob stabile Sicherheitsupdates automatisch heruntergeladen und installiert werden sollen oder nicht. Überprüfen Sie, ob die Frage mit Ja beantwortet wird.

unattended-upgrades sucht zweimal täglich zu zufälligen Zeiten nach Updates und installiert stabile Sicherheitsupdates.

Überprüfen Sie, was passiert, indem Sie die Protokolldateien im Verzeichnis /var/log/unattended-upgrades/ und die Datei /var/log/dpkg.log lesen.

Wo Sie die Dokumentation finden
==============================================================================

Die Konfigurationsdatei /etc/apt/apt.conf.d/50unattended-upgrades enthält eine Dokumentation in den Kommentaren. In diesem Dokument wird auf diese Datei von nun an als 50unattended-upgrades verwiesen.

Die Readme-Datei /usr/share/doc/unattended-upgrades/README.md.gz enthält nützliche Anleitungen. Sie kann mit dem Befehl zless (verfügbar im Paket gzip) gelesen werden. Dieses Dokument verweist auf diese Datei von nun an als README.

Das Debian-Wiki hat eine Seite <https://wiki.debian.org/UnattendedUpgrades>.

Die Ubuntu Server Docs Seite hat ein Kapitel „Automatische Updates“ über unattended-upgrades.

Man-Seite von unattended-upgrade, man unattended-upgrade.

Erweiterte Konfiguration
==============================================================================

Wenn Sie den einfachen Weg wählen, werden Sie feststellen, dass nicht alle Upgrades automatisch installiert werden. Vielleicht möchten Sie auch mehr Kontrolle darüber haben, was zusätzlich zur Installation von Aktualisierungen automatisch geschieht.

Die Konfigurationsdatei /etc/apt/apt.conf.d/50unattended-upgrades enthält eine Dokumentation in den Kommentaren, also lesen Sie die Datei, um zu sehen, welche Konfigurationen möglich sind. Sehr nützlich ist es, unattended-upgrades so zu konfigurieren, dass E-Mails verschickt werden, wenn etwas passiert.

Es ist wichtig zu wissen, dass die Konfigurationsdatei bei der Installation oder dem Upgrade des unattended-upgrades-Pakets erstellt wird. Wenn also unattended-upgrades selbst aktualisiert wird oder auf die nächste Betriebssystemversion umgestellt wird, führen Änderungen in dieser Datei zu Konflikten, die manuell gelöst werden müssen. In der Dokumentationsdatei README wird vorgeschlagen, die Datei 52unattended-upgrades-local zu erstellen, anstatt die ursprüngliche Konfigurationsdatei zu ändern.

Wenn Sie möchten, können Sie die Datei /etc/apt/apt.conf.d/20auto-upgrades daraufhin überprüfen, ob sie diese Zeilen enthält:

bash

APT::Periodic::Update-Package-Lists "1";
APT::Periodic::Unattended-Upgrade "1";

Wenn Sie den Befehl dpkg-reconfigure ausgeführt und mit Ja beantwortet haben, sollten die Zeilen vorhanden sein. Die Zeilen steuern die Aktivierung von unattended-upgrades.

E-Mail senden
==============================================================================

Damit das Versenden von E-Mails funktioniert, muss der Host über ein funktionierendes E-Mail-System verfügen, das E-Mails versenden kann. Überprüfen Sie das zuerst.

Aktivieren Sie den E-Mail-Versand von  unattended-upgrades, indem Sie die Konfigurationsdatei 52unattended-upgrades-local bearbeiten (erstellen Sie die Datei, wenn sie nicht im Verzeichnis /etc/apt/apt.conf/ existiert). Kopieren Sie die Zeile

bash

//Unattended-Upgrade::Mail "";
aus 50unattended-upgrades, entkommentieren Sie sie und fügen Sie die Ziel-E-Mail-Adresse zwischen den „-Anführungszeichen“ ein.

Es gibt auch eine Einstellung, mit der Sie festlegen können, ob bei unattended-upgrades immer eine E-Mail verschickt wird oder nur bei Fehlern.

Hinweis
Aus Sicherheitsgründen empfehlen wir auf die Nutzung von E-Mail komplett zu verzichten - siehe Systemhärtung

bash

// Set this value to "true" to get emails only on errors. Default
// is to always send a mail if Unattended-Upgrade::Mail is set
//Unattended-Upgrade::MailOnlyOnError "false";

Wenn Sie die Einstellung ändern möchten, kopieren Sie die Zeilen nach 52unattended-upgrades-local, entkommentieren Sie die Einstellungszeile und ändern Sie „false“ in „true“.

Das Überprüfen mit

bash

sudo unattended-upgrade --dry-run -d

ist eine gute Idee, nachdem Sie die Konfigurationsdatei geändert haben.

Zumindest unter Debian 10, 11 und Ubuntu 22.04 erstellt die Installation von unattended-upgrades einen symbolischen Link unattended-upgrades zu unattended-upgrade im Verzeichnis /usr/bin/ und/oder /bin/, sodass beide Befehle gleich funktionieren.

bash

$ ls -lhi /bin/unattended-upgrade*
11404505 -rwxr-xr-x 1 root root 98K tammi  15  2022 /bin/unattended-upgrade
11407087 lrwxrwxrwx 1 root root  18 tammi  15  2022 /bin/unattended-upgrades -> unattended-upgrade

Repository hinzufügen
==============================================================================

Das Hinzufügen eines Repositorys, von dem automatisch aktualisiert werden soll, erfolgt durch Hinzufügen von Zeilen zu Unattended-Upgrade::Origins-Pattern. Die Dokumentation dazu finden Sie in der Konfigurationsdatei.

Die README weist die Einstellungen in der späteren Konfigurationsdatei 52unattended-upgrades-local an, um die Standardeinstellungen zu überschreiben. Ich habe es ausprobiert und festgestellt, dass die Einstellung Unattended-Upgrades::Origins-Pattern die Einstellungen in der Standarddatei nicht vollständig überschreibt, sondern zu den Standardeinstellungen hinzugefügt wird. Es ist also nicht notwendig, das komplette Standard Unattended-Upgrade::Origins-Pattern zu kopieren, es kann hinzugefügt werden.

Zum Beispiel wird das Paket goaccess nicht automatisch aktualisiert, wenn es aus dem offiziellen GoAccess-Repository installiert wird. Es würde automatisch upgraden, wenn es aus dem normalen Debian-Repository installiert würde (es ist in beiden verfügbar). Wenn Sie das GoAccess-Repository zum Origins-Pattern hinzufügen, führen unattended-upgrades das Upgrade durch.

Überprüfen Sie die Situation mit:

bash

sudo apt list --upgradable

bash

vagrant@debian-automation-controller:~# LANG=C apt list --upgradable
Listing... Done
goaccess/unknown 2:1.6.3-buster amd64 [upgradable from: 2:1.6.2-buster]
php-tcpdf/buster-backports 6.5.0+dfsg1-1~bpo10+1 all [upgradable from: 6.3.5+dfsg1-1~bpo10+1]
vagrant@debian-automation-controller:~#

Es gibt also zwei Pakete, die von unattended-upgrades nicht automatisch aktualisiert wurden. Die Untersuchung von goaccess zeigt, dass es aus dem offiziellen GoAccess-Repository stammt (und eine ältere Version aus dem normalen Debian-Repository).

bash

vagrant@debian-automation-controller:~# LANG=C apt policy goaccess
goaccess:
  Installed: 2:1.6.2-buster
  Candidate: 2:1.6.3-buster
  Version table:
     2:1.6.3-buster 500
        500 <https://deb.goaccess.io> buster/main amd64 Packages
 *** 2:1.6.2-buster 100
        100 /var/lib/dpkg/status
     1:1.2-4+b10 500
        500 <http://mirror.hetzner.de/debian/packages> buster/main amd64 Packages
        500 http:// deb.debian.org/debian buster/main amd64 Packages
Wenn Sie unattended-upgrade --dry-run -d ausführen, sehen Sie, welchen Ursprung die nicht installierten Pakete haben. Das kann dabei helfen herauszufinden, was zu Origins-Pattern hinzugefügt werden muss.

Die Untersuchung des Protokolls der letzten unbeaufsichtigten Upgrades zeigt, welche Ursprünge beim automatischen Upgrade berücksichtigt werden:

Verify

2022-09-05 08:28:08,955 INFO Checking if system is running on battery is skipped. Please install
    powermgmt-base package to check power status and skip installing updates when the system
    is running on battery.
2022-09-05 08:28:08,960 INFO Initial blacklist :
2022-09-05 08:28:08,960 INFO Initial whitelist:
2022-09-05 08:28:08,960 INFO Starting unattended upgrades script
2022-09-05 08:28:08,960 INFO Allowed origins are:
origin=Debian,codename=buster,label=Debian,
origin=Debian,codename=buster,label=Debian-Security
Das GoAccess-Repository ist noch nicht dabei, also füge ich es jetzt zu /etc/apt/apt.conf.d/52unattended-upgrades-local hinzu.

Nebenbei bemerkt: Wenn Sie unattended-upgrades auf einem Laptop installiert haben, sollten Sie powermgmt-base installieren. Damit startet unattended-upgrades die Upgrades nicht im Akkubetrieb, wie die Info-Meldung im Log angibt. Auf einem Server, der immer mit Netzstrom betrieben wird, wird diese Info-Meldung nicht mehr angezeigt, wenn Unattended-Upgrade::OnlyOnACPower "false"; zur Konfiguration hinzugefügt wird.

bash

Unattended-Upgrade::Origins-Pattern {
// Taleman added GoAccess 2022-09-05
        "o=GoAccess Repository, n=buster, l=Official GoAccess Repository";
};
Unattended-Upgrade::OnlyOnACPower "false";

apt policy zeigte das Feld a für GoAccess nicht an, also habe ich o, n und l verwendet. Infos zu diesen Feldern finden Sie in README.

Die Variablenersetzung wird für ${distro_id}, das die Ausgabe von lsb_release -i enthält, und ${distro_codename}, das die Ausgabe von lsb_release -c enthält, unterstützt. Anstelle von n=buster hätte ich also auch n=${distro_codename} schreiben können.

Es gab zwei Pakete, die nicht automatisch aktualisiert wurden. Das andere, das noch nicht aktualisiert wurde, ist php-tcpdf. Es kann natürlich mit apt upgrade aktualisiert werden, aber es kann auch zu unattended-upgrades hinzugefügt werden, damit es automatisch aktualisiert wird. Die Vorgehensweise ist die gleiche wie bei goaccess.

Überprüfen Sie zuerst die Situation mit apt policy php-tcpdf. Das zeigt, dass es aus dem Debian-Repository-Abschnitt buster-backports installiert ist.

Aus unattended-upgrades.log geht hervor, dass buster-backports nicht zu den erlaubten Ursprüngen gehört. Er ist in der Datei 50unattended-upgrades enthalten, aber auskommentiert. Um ihn zu aktivieren, kopieren Sie die Zeile

bash

// "o=Debian Backports,a=${distro_codename}-backports,l=Debian Backports";
nach 52unattended-upgrades-local in die Origins-Pattern-Einstellung und kommentieren Sie sie aus.

Nachdem Sie die Datei bearbeitet haben, überprüfen Sie mit unattended-upgrades --dry-run -d, ob das hinzugefügte Repository nun zu den „Erlaubten Ursprüngen“ gehört und php-tcpdf zu den Paketen gehört, die aktualisiert werden.

Zeiten kontrollieren
==============================================================================

unattended-upgrades wird zu zufälligen Zeiten ausgeführt, um die Repository-Server zu entlasten. Damit sollen große Lastspitzen vermieden werden, die auftreten würden, wenn alle Hosts zur gleichen Zeit Updates durchführen würden. Überlegen Sie sich gut, ob Sie dieses Verhalten ändern möchten. Wenn Sie Ihr eigenes Repository oder ein Spiegel-Repository betreiben, treffen die Lastspitzen Ihren Repository-Server und Sie vermeiden es, die Admins der Repositories im Internet zu verärgern.

Der systemd steuert, zu welchen Zeiten unattended-upgrades startet. Die Datei /lib /systemd/system/apt-daily.timer enthält einen Timer-Abschnitt, der zweimal täglich mit einer zufälligen Verzögerung von 12 Stunden Apt-Downloads startet. Ich schlage vor, dass Sie das nicht ändern oder sehr gute Gründe für eine Änderung haben.

Der Rest der Zeitkonfigurationen wird in den Dateien 50unattended-upgrades und 52unattended-upgrades-local vorgenommen.

Die Einstellung Unattended-Upgrade::Update-Days steuert die Wochentage, an denen unattended-upgrades ausgeführt wird. Die Standardeinstellung ist leer, was bedeutet, dass sie jeden Tag ausgeführt wird. Es kann aber auch so konfiguriert werden, dass es zum Beispiel nur am Samstag und Sonntag ausgeführt wird.

Automatischer Neustart
==============================================================================

Unbeaufsichtigte Aktualisierungen können so eingestellt werden, dass sie neu gestartet werden, wenn die installierten Updates einen Neustart erfordern. Dies kann entweder sofort oder zu einem bestimmten Zeitpunkt geschehen. Diese Konfigurationen verwenden Einstellungen:

bash

//Unattended-Upgrade::Automatic-Reboot "false";
//Unattended-Upgrade::Automatic-Reboot-WithUsers "true";
//Unattended-Upgrade::Automatic-Reboot-Time "02:00";

Die Standardeinstellung ist kein automatischer Neustart. Wenn Sie die Einstellung auf „true“ ändern, erfolgt der Neustart sofort nach der Installation des Upgrades. Automatic-Reboot-Time kann so eingestellt werden, dass der Neustart zu einem gewünschten Zeitpunkt erfolgt, wenn es installierte Updates gibt, die einen Neustart erfordern.

Blacklisting, Whitelisting
==============================================================================

Eine Blacklist verhindert, dass ein Paket, das sonst durch unbeaufsichtigte Upgrades aktualisiert werden würde, aktualisiert wird. Die Einstellung Package-Blacklist enthält reguläre Ausdrücke. Wenn der Paketname darauf passt, wird er vom automatischen Upgrade ausgeschlossen.

Ich habe diese Funktion noch nicht genutzt, aber ich denke, dass sie bei einer Entwicklungsversion, bei der es sinnvoll ist, das automatische Upgrade kritischer Pakete zu verhindern, sinnvoller ist. Die Konfigurationsdatei 50unattended-upgrades enthält Beispiele für Pakete, die auf der schwarzen Liste stehen.

In der README steht etwas über die Einstellung Package-Whitelist. In der Beschreibung heißt es: „Nur Pakete, die mit den regulären Ausdrücken in dieser Liste übereinstimmen, werden für ein Upgrade markiert.“ Es gibt keine Beispiele, die verdeutlichen, in welchen Fällen es wünschenswert wäre, die Whitelist zu setzen. Nach meinen Experimenten sieht es so aus, als ob das Hinzufügen von Paketen zur Whitelist bedeutet, dass nur diese Pakete automatisch aktualisiert werden und nichts anderes.

Fazit
==============================================================================

Jetzt wissen Sie, was unattended-upgrades kann und wie Sie es so einrichten können, dass es das tut, was Sie wollen.

Es gibt Fälle, in denen unattended-upgrades das Upgrade nicht durchführt, weil der Befehl apt upgrade ein Paket zurückhält. Das passiert, wenn das Upgrade ein Paket entfernen oder ein zuvor deinstalliertes Paket installieren würde. Um das Upgrade durchzuführen, müssen Sie es selbst mit dem Befehl apt full-upgrade durchführen, der Pakete entfernen oder neue, zuvor deinstallierte Pakete installieren kann. Wenn unattended-upgrades so konfiguriert ist, dass es E-Mails versendet, enthält die E-Mail die Zeile:

bash

Packages with upgradable origin but kept back:

Ein letztes Beispiel für 52unattended-upgrades-local von einem meiner Rechner:

bash

Unattended-Upgrade::Origins-Pattern {
// Taleman added 2022-09-05
        "o=GoAccess Repository, n=buster, l=Official GoAccess Repository";
        "o=Debian Backports,a=${distro_codename}-backports,l=Debian Backports";
 "origin=deb.sury.org,archive=${distro_codename}";
};

Unattended-Upgrade::Mail "<myemail@domain.tld>";
Unattended-Upgrade::OnlyOnACPower "false";

Links
==============================================================================

* <https://www.howtoforge.de/anleitung/automatische-updates-mit-unbeaufsichtigten-upgrades-unter-debian-und-ubuntu/>
* <https://blog.djonz.de/howto/unattended-upgrades/>
* <https://benheater.com/configuring-unattended-upgrades-on-debian/>

.bashrc
------------------------------------------------------------------------------

1. Verlaufskonfiguration hinzufügen
==============================================================================

Verhindern, dass der Verlauf überschrieben wird: Stellen Sie sicher, dass der Befehlshistorie angehängt wird, anstatt überschrieben zu werden. Dies hilft, eine vollständige Historie der ausgeführten Befehle zu behalten.

bash

shopt -s histappend
PROMPT_COMMAND="history -a; history -n"

Verlaufgröße festlegen: Begrenzen Sie die Größe der Verlaufdatei, um übermäßigen Speicherplatz für Befehle zu vermeiden.

bash

HISTSIZE=1000
HISTFILESIZE=2000

2. Verlaufdatei schützen
==============================================================================

Verlaufdatei schreibgeschützt machen: Dies verhindert, dass Benutzer ihren Verlauf löschen.

bash

chattr +a ~/.bash_history

3. Unerwünschte Funktionen deaktivieren
==============================================================================

Befehlsergänzung deaktivieren: Dies kann verhindern, dass Befehle versehentlich ausgeführt werden.

bash

set +o emacs
set +o vi

4. Befehle in Syslog protokollieren
==============================================================================

Befehle in Syslog protokollieren: Ändern Sie die Shell, um alle Befehle in Syslog zu protokollieren, um eine bessere Überwachung zu ermöglichen.

bash

export PROMPT_COMMAND='logger -t bash -p user.info "$(history 1)"'

5. Umgebungsvariablen einschränken
==============================================================================

Gefährliche Variablen unsetten: Verhindern Sie die Verwendung potenziell schädlicher Umgebungsvariablen.

bash

unset HISTFILE
unset HISTCONTROL

6. Sicheren umask festlegen
==============================================================================

Einen sicheren umask festlegen: Dies definiert die Standardberechtigungen für neu erstellte Dateien.

bash

umask 077

7. Sourcing anderer rc-Dateien deaktivieren
==============================================================================

Verhindern, dass andere Konfigurationsdateien geladen werden: Dies kann helfen, unerwünschte Konfigurationen zu vermeiden.

bash

set -o noglob

8. Sichere Shell-Optionen verwenden
==============================================================================

Sichere Shell-Optionen festlegen: Wenn Sie SSH verwenden, stellen Sie sicher, dass bestimmte Optionen für die Sicherheit festgelegt sind.

bash

export SSH_AUTH_SOCK=/run/user/$UID/keyring/ssh

9. Benutzerdefinierte Eingabeaufforderung für Sicherheitsbewusstsein
==============================================================================

Eine benutzerdefinierte Eingabeaufforderung festlegen: Dies kann die Benutzer daran erinnern, vorsichtig zu sein.

bash

export PS1="\[\e[31m\]\u@\h:\w\$ \[\e[m\]"

10. Verlauf für sensible Sitzungen deaktivieren
==============================================================================

Verlauf vorübergehend deaktivieren: Für sensible Operationen können Sie den Verlauf deaktivieren.

bash

set +o history

# Sensible Befehle hier

set -o history

Fazit
==============================================================================

Die Implementierung dieser Konfigurationen in Ihrer .bashrc-Datei kann die Sicherheit Ihrer Shell-Umgebung erheblich verbessern. Denken Sie immer daran, Änderungen in einer sicheren Umgebung zu überprüfen und zu testen, bevor Sie sie auf Produktionssystemen bereitstellen.

Setzen von locale
------------------------------------------------------------------------------

perl: warning: Please check that your locale settings: LANGUAGE = (unset), LC_ALL = (unset), LC_CTYPE = "de_DE.UTF-8", LANG = "C.UTF-8" are supported and installed on your system. perl: warning: Falling back to a fallback locale ("C.UTF-8").

Die Warnung, die Sie sehen, deutet darauf hin, dass die Locale-Einstellungen auf Ihrem System nicht korrekt konfiguriert sind oder dass die angegebenen Locale nicht installiert sind. Dies kann zu Problemen bei der Ausführung von Perl-Skripten oder anderen Programmen führen, die auf Locale-Einstellungen angewiesen sind.

Hier sind einige Schritte, um das Problem zu beheben:

1. Überprüfen Sie die installierten Locales
==============================================================================

Zuerst sollten Sie überprüfen, welche Locales auf Ihrem System installiert sind. Führen Sie den folgenden Befehl aus:

bash

locale -a

Dieser Befehl listet alle verfügbaren Locales auf. Stellen Sie sicher, dass de_DE.UTF-8 in der Liste enthalten ist.

2. Installieren Sie die fehlenden Locales
==============================================================================

Wenn de_DE.UTF-8 nicht in der Liste der verfügbaren Locales erscheint, können Sie es installieren. Führen Sie dazu die folgenden Schritte aus:

Auf Debian/Ubuntu-basierten Systemen:
Öffnen Sie ein Terminal.

Führen Sie den folgenden Befehl aus, um die Locale zu generieren:

bash

sudo locale-gen de_DE.UTF-8

Aktualisieren Sie die Locale-Einstellungen:

bash

sudo update-locale

Auf Red Hat/CentOS-basierten Systemen:

Öffnen Sie ein Terminal.

Fügen Sie die Locale zu /etc/locale.gen hinzu (falls nicht vorhanden):

bash

echo "de_DE.UTF-8 UTF-8" | sudo tee -a /etc/locale.gen
Generieren Sie die Locale:

bash

sudo locale-gen

3. Setzen Sie die Locale-Umgebungsvariablen
==============================================================================

Um sicherzustellen, dass die Locale-Umgebungsvariablen korrekt gesetzt sind, können Sie die folgenden Zeilen zu Ihrer ~/.bashrc oder ~/.profile Datei hinzufügen:

bash

export LANG=de_DE.UTF-8
export LC_ALL=de_DE.UTF-8

Nach dem Hinzufügen dieser Zeilen müssen Sie die Datei neu laden oder sich ab- und wieder anmelden:

bash
source ~/.bashrc

4. Überprüfen Sie die Locale-Einstellungen
==============================================================================

Führen Sie den folgenden Befehl aus, um zu überprüfen, ob die Locale-Einstellungen korrekt gesetzt sind:

bash

locale

LANG=de_DE.UTF-8
LANGUAGE=
LC_CTYPE="de_DE.UTF-8"
LC_NUMERIC="de_DE.UTF-8"
LC_TIME="de_DE.UTF-8"
LC_COLLATE="de_DE.UTF-8"
LC_MONETARY="de_DE.UTF-8"
LC_MESSAGES="de_DE.UTF-8"
LC_PAPER="de_DE.UTF-8"
LC_NAME="de_DE.UTF-8"
LC_ADDRESS="de_DE.UTF-8"
LC_TELEPHONE="de_DE.UTF-8"
LC_MEASUREMENT="de_DE.UTF-8"
LC_IDENTIFICATION="de_DE.UTF-8"
LC_ALL=de_DE.UTF-8

Die Ausgabe sollte nun de_DE.UTF-8 für LANG und LC_ALL anzeigen.

Wenn sie US Englisch wünschen ersetzen sie bitte ```de_DE```mit ```en_US```.

Zusammenfassung
==============================================================================

Durch die Installation der fehlenden Locale und das Setzen der entsprechenden Umgebungsvariablen sollten die Perl-Warnungen verschwinden. Wenn das Problem weiterhin besteht, überprüfen Sie die Konfiguration und stellen Sie sicher, dass alle Schritte korrekt ausgeführt wurden.

Links
==============================================================================

* <https://de.wikipedia.org/wiki/Locale>
* <https://saimana.com/list-of-country-locale-code/>
* <https://simplelocalize.io/data/locales/>

rootless Containers mit Podman auf Debian 12 und Red Hat 9
------------------------------------------------------------------------------

Debian
==============================================================================

**1. Installation von Podman**

Zuerst müssen Sie Podman installieren, falls es noch nicht installiert ist:

bash

```sudo apt-get update```
```sudo apt-get -y install podman```

**2. Erstellen eines Rootless Benutzers**

Stellen Sie sicher, dass Sie einen normalen Benutzer haben, der die Container ausführen kann. Wenn Sie noch keinen Benutzer haben, können Sie einen erstellen:

bash

```sudo adduser containeruser```

**3. Konfigurieren von Podman für Rootless**

Podman unterstützt rootless Container standardmäßig. Stellen Sie sicher, dass Ihr Benutzer die erforderlichen Umgebungsvariablen hat:

bash

```export XDG_RUNTIME_DIR=/run/user/$(id -u)```

Fügen Sie diese Zeile zu Ihrer ~/.bashrc oder ~/.bash_profile hinzu, um sie bei jeder Anmeldung zu setzen.

**4. Ausführen eines Apache-Containers**

Jetzt können Sie einen Apache-Container als normaler Benutzer ausführen:

bash

```podman run --rm -d -p 8080:80 httpd```

Sie können dann auf den Apache-Server zugreifen, indem Sie <http://localhost:8080> in Ihrem Webbrowser aufrufen.

**5. Ausführen eines "Hello World"-Containers**

Um einen einfachen "Hello World"-Container auszuführen, verwenden Sie das folgende Kommando:

bash

```podman run --rm hello-world```

Dieser Container gibt eine einfache Nachricht aus, die bestätigt, dass der Container erfolgreich ausgeführt wurde.

Red Hat Linux 9
==============================================================================

**1. Installation von Podman**

Zuerst müssen Sie Podman installieren, falls es noch nicht installiert ist:

bash

```sudo dnf install -y podman```

**2. Erstellen eines Rootless Benutzers**

Stellen Sie sicher, dass Sie einen normalen Benutzer haben, der die Container ausführen kann. Wenn Sie noch keinen Benutzer haben, können Sie einen erstellen:

bash

```sudo adduser containeruser```

**3. Konfigurieren von Podman für Rootless**

Podman unterstützt rootless Container standardmäßig. Stellen Sie sicher, dass Ihr Benutzer die erforderlichen Umgebungsvariablen hat:

bash

```export XDG_RUNTIME_DIR=/run/user/$(id -u)```

Fügen Sie diese Zeile zu Ihrer ~/.bashrc oder ~/.bash_profile hinzu, um sie bei jeder Anmeldung zu setzen.

**4. Ausführen eines Apache-Containers**

Jetzt können Sie einen Apache-Container als normaler Benutzer ausführen:

bash

```podman run --rm -d -p 8080:80 httpd```

Sie können dann auf den Apache-Server zugreifen, indem Sie <http://localhost:8080> in Ihrem Webbrowser aufrufen.

**5. Ausführen eines "Hello World"-Containers**

Um einen einfachen "Hello World"-Container auszuführen, verwenden Sie das folgende Kommando:

bash

```podman run --rm hello-world```

Dieser Container gibt eine einfache Nachricht aus, die bestätigt, dass der Container erfolgreich ausgeführt wurde.

Fazit
==============================================================================

Die Implementierung von rootless Containern mit Podman auf Debian und Red Hat Linux 9 ist einfach und bietet eine sichere Möglichkeit, Container zu verwalten. Durch die Ausführung von Containern ohne Root-Rechte können Sie die Sicherheit Ihres Systems erhöhen und potenzielle Risiken minimieren. Mit den oben genannten Schritten können Sie sowohl einen Apache-Server als auch einen einfachen "Hello World"-Container ausführen.

Reference - Links
------------------------------------------------------------------------------

* <https://docs.oracle.com/en/operating-systems/oracle-linux/9/security/OL9-HARDENING.pdf>
