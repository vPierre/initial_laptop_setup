#!/usr/bin/env bash
# Author: Pierre Gronau <Pierre.Gronau@ndaal.eu>
# Copyright 2024, 2025
# License: All content is licensed under the terms of the <MIT License>
# Developed on: Debian 12.x; macOS Sequoia x86 architecture
# Tested on: Debian 12.x; macOS Sequoia x86 architecture
#
# Exit on error. Append "|| true" if you expect an error.
set -o errexit
# This is equivalent to set -e. It causes the script to exit
# immediately if any command exits with a non-zero status.
#
# Exit on error inside any functions or subshells.
set -o errtrace
# This setting ensures that the ERR trap is inherited by shell functions,
# command substitutions, and commands executed in a subshell environment.
#
# Do not allow use of undefined vars. Use ${VAR:-} to use an undefined VAR
set -o nounset
# This is equivalent to set -u. It treats unset variables as
# an error when substituting.
#
# Catch the error in case mysqldump fails (but gzip succeeds) in `mysqldump |gzip`
# https://vaneyckt.io/posts/safer_bash
set -o pipefail
# This setting causes a pipeline to return the exit status of the last command
# in the pipe that returned a non-zero status.
#
# Turn on traces, useful while debugging but commented out by default
# set -o xtrace

# Set $IFS to only newline and tab.
#
# http://www.dwheeler.com/essays/filenames-in-shell.html
# nosemgrep: ifs-tampering
IFS=$'\n\t'

# Error trapping function
# Add this line before the error_handler function
# shellcheck disable=SC2317
error_handler() {
    local line_number="${1}"
    local error_code="${2}"
    local last_command="${BASH_COMMAND}"
    printf "Error: Error occurred in line %s (error code: %s)\n" "${line_number}" "${error_code}"
    printf "Error: Failed command: %s\n" "${last_command}"

    # Success case
    return 0
}

# Set the error trap
trap "error_handler ${LINENO} \$?" ERR

cleanup() {
    trap - SIGINT SIGTERM ERR EXIT
    printf "%b\n" "\nInfo: Cleanup is running ..."
    # Additional cleanup tasks can be added here if needed
    rm -f -v ./*.tmp
    printf "%b\n" "\nInfo: Cleanup finished ..."
}

trap cleanup SIGINT SIGTERM ERR EXIT

declare -A warnings

warnings["en"]="WARNING: Please do a backup with Time Machine.
WARNING: The installation script is not tested on ARM macOS systems."
warnings["de"]="WARNUNG: Bitte machen Sie ein Backup mit Time Machine.
WARNUNG: Das Installationsskript wurde nicht auf ARM macOS-Systemen getestet."
warnings["es"]="ADVERTENCIA: Por favor, haga una copia de seguridad con Time Machine.
ADVERTENCIA: El script de instalación no ha sido probado en sistemas macOS ARM."
warnings["fr"]="AVERTISSEMENT : Veuillez faire une sauvegarde avec Time Machine.
AVERTISSEMENT : Le script d'installation n'a pas été testé sur les systèmes macOS ARM."
warnings["pt"]="AVISO: Por favor, faça um backup com o Time Machine.
AVISO: O script de instalação não foi testado em sistemas macOS ARM."

# Print warnings in each language
for lang in "${!warnings[@]}"; do
    printf "%s\n" "${warnings[$lang]}"
    printf "\n"
done

wget --secure-protocol=auto --https-only --continue --verbose -N --tries=10 --check-certificate "https://gitlab.com/vPierre/initial_laptop_setup/-/raw/main/scripts/hardening_dns.sh"
wget --secure-protocol=auto --https-only --continue --verbose -N --tries=10 --check-certificate "https://gitlab.com/vPierre/initial_laptop_setup/-/raw/main/scripts/macos_brew_install.sh"
wget --secure-protocol=auto --https-only --continue --verbose -N --tries=10 --check-certificate "https://gitlab.com/vPierre/initial_laptop_setup/-/raw/main/scripts/macos_pip3_install.sh"
wget --secure-protocol=auto --https-only --continue --verbose -N --tries=10 --check-certificate "https://gitlab.com/vPierre/initial_laptop_setup/-/raw/main/scripts/macos_rust_install.sh"
wget --secure-protocol=auto --https-only --continue --verbose -N --tries=10 --check-certificate "https://gitlab.com/vPierre/initial_laptop_setup/-/raw/main/scripts/hardening_script.sh"
wget --secure-protocol=auto --https-only --continue --verbose -N --tries=10 --check-certificate "https://gitlab.com/vPierre/initial_laptop_setup/-/raw/main/scripts/hardening_privacy.sh"
wget --secure-protocol=auto --https-only --continue --verbose -N --tries=10 --check-certificate "https://gitlab.com/vPierre/initial_laptop_setup/-/raw/main/scripts/macos_download_not_covered_apps.sh"
wget --secure-protocol=auto --https-only --continue --verbose -N --tries=10 --check-certificate "https://gitlab.com/vPierre/initial_laptop_setup/-/raw/main/scripts/macos_privacy_settings.sh"
wget --secure-protocol=auto --https-only --continue --verbose -N --tries=10 --check-certificate "https://raw.githubusercontent.com/ataumo/macos_hardening/refs/heads/main/puppy.sh"

git clone https://github.com/ataumo/macos_hardening

"./macos_brew_install.sh"
"./macos_pip3_install.sh"
"./macos_rust_install.sh"
"./macos_download_not_covered_apps.sh"
"./hardening_script.sh"
"./hardening_privacy.sh"
"./macos_privacy_settings.sh"
"./macos_hardening/puppy.sh -H"
"./hardening_dns"

softwareupdate --install -a

sudo lynis audit system

#THIS SECTION REBOOTS THE MACHINE SO THE SETTINGS CAN TAKE EFFECT - IT ADDS A KEYPRESS AS A PAUSE BEFORE CONTINUING WITH THE REBOOT
read -r -p "[I] The machine will now reboot to enable the hardening to take effect. Press ENTER to continue..."
sudo reboot

cleanup

script_name1="$(basename "${0}")"
printf "\nInfo: script_name1: %s\n" "${script_name1}"
script_path1="$(realpath "$(dirname "${0}")")"
printf "Info: script_path1: %s\n" "${script_path1}"
script_path_with_name="${script_path1}/${script_name1}"
printf "Info: Script path with name: %s\n" "${script_path_with_name}"
printf "Info: Script finished\n"
exit 0
