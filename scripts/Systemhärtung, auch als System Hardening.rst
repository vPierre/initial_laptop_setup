******************************************************************************
Systemhärtung - Systemhärtung mittels Ansible und bash Skripte
******************************************************************************

.. sectionauthor:: Pierre Gronau <Pierre.Gronau@ndaal.eu>

.. contents:: Inhalt - Sicherheit - Container
   :depth: 5


Systemhärtung mittels Ansible Playbooks, Rollen und bash Skripte
------------------------------------------------------------------------------

Das Ansible Playbook besteht mindestens aus folgenden Ansible Rollen:

Vorbereitung Systemhärtung
==============================================================================

Die Vorbereitung für die Systemhärtung erfolgt mit nachstehenden 
Bash-Skripten und Ansible-Rollen:

Ansible Rolle - auditd minimal
##############################################################################

* Install ``auditd`` and test the functionality
* If ``journald`` is installed keep it
* Basis für die **Ansible Rolle** ist: <https://gitlab.com/ndaal_open_source/ndaal_public_auditd>
* Neustart 

Ansible Rolle - Preparation hardening
##############################################################################

* Platzhalter Ansible Role
* Neustart 

Durchführung der Systemhärtung
==============================================================================

Die Durchführung der Systemhärtung erfolgt mit nachstehenden 
Bash-Skripten und Ansible-Rollen:

Ansible Rolle - Patching initial
##############################################################################

* Patching
* Neustart 

.. literalinclude::
    _cfg/function_update_packages_for_package_manager.cfg
   :language: text
   :force:
   :linenos:
   :caption: function_update_packages_for_package_manager.cfg


# Call the function with the first command-line argument

update_packages "${1}"

Durchführung der Systemhärtung - Ansible Rolle - CIS hardening
##############################################################################

* System hardening with <https://github.com/ansible-lockdown/RHEL9-CIS> and! <https://github.com/RedHatOfficial/ansible-role-rhel9-cis>
* test the hardening with ``Inspec`` like this <https://github.com/mitre/redhat-enterprise-linux-9-stig-baseline>
* Neustart 

Durchführung der Systemhärtung - Ansible Rolle - ssh hardening
##############################################################################

* System hardening with <https://gitlab.com/vPierre/ndaal_public_ansible_role_hardening_ssh>

Durchführung der Systemhärtung - Ansible Rolle - auditd with ndaal audit udit_best_practices.rules
##############################################################################

* Install ``auditd`` and test the functionality
* Basis für die Ansible Rolle ist:
  <https://gitlab.com/ndaal_open_source/ndaal_public_auditd/-/blob/main/ndaal/audit_best_practices.rules?ref_type=heads>
* Neustart 

Aufräumen der Systemhärtung
==============================================================================

Das Aufräumen der Systemhärtung erfolgt mit nachstehenden 
Bash-Skripten und Ansible-Rollen:

Aufräumen der Systemhärtung - Ansible Rolle - Patching
##############################################################################

* Patching

.. literalinclude:: 
   _cfg/function_update_packages_for_package_manager.cfg
   :language: text
   :force:
   :linenos:
   :caption: function_update_packages_for_package_manager.cfg


# Call the function with the first command-line argument

update_packages "${1}"

Aufräumen der Systemhärtung - Ansible Rolle - Post hardening
##############################################################################

* Platzhalter Ansible Role
* Neustart 

Aufräumen der Systemhärtung - Ansible Rolle - cleanup
##############################################################################

* Platzhalter Ansible Role
* e.g. Cleanup bash history
* Neustart 

Aufräumen der Systemhärtung - function_install_packages_for_package_manager.cfg
##############################################################################

.. literalinclude::
   _cfg/function_install_packages_for_package_manager.cfg
   :language: text
   :force:
   :linenos:
   :caption: function_install_packages_for_package_manager.cfg


``clear_package_traces debian``

Aufräumen der Systemhärtung - cleanup_package_manager_linux.sh
##############################################################################

.. literalinclude:: 
   _scripts/cleanup_package_manager_linux.sh
   :language: bash
   :force:
   :linenos:
   :caption: cleanup_package_manager_linux.sh


export HOME="/home/${SUDO_USER:-${USER}}" # Keep `~` and `$HOME` for user not `/root`.

Aufräumen der Systemhärtung - cleanup_hardening_linux.sh
##############################################################################

.. literalinclude::
   _scripts/cleanup_hardening_linux.sh
   :language: bash
   :force:
   :linenos:
   :caption: cleanup_hardening_linux.sh


Aufräumen der Systemhärtung - remove_unwished_files.sh
##############################################################################

.. literalinclude::
   _scripts/remove_unwished_files.sh
   :language: bash
   :force:
   :linenos:
   :caption: remove_unwished_files.sh


Aufräumen der Systemhärtung - Web Browser traces bereinigen
##############################################################################

Exemplarisches löschen von Web Browser traces für Firefox:

.. literalinclude::
   _scripts/cleanup_firefox_linux.sh
   :language: bash
   :force:
   :linenos:
   :caption: cleanup_firefox_linux.sh

Weitere Skripte stehen zur Verfügung:

- cleanup_Browsh_linux.sh

- cleanup_Links_linux.sh

- cleanup_brave_linux.sh

- cleanup_epiphany_linux.sh

- cleanup_konqueror_linux.sh

- cleanup_opera_linux.sh

- cleanup_vivaldi_linux.sh

- cleanup_ELinks_linux.sh

- cleanup_Netrik_linux.sh

- cleanup_chrome_linux.sh

- cleanup_falkon_linux.sh

- cleanup_lynx _linux.sh

- cleanup_package_manager_linux.sh

- cleanup_w3m_linux.sh

- cleanup_LibreWolf_linux.sh

- cleanup_Pale-Moon_linux.sh

- cleanup_chromium_linux.sh

- cleanup_firefox_linux.sh

- cleanup_midori_linux.sh

- cleanup_slimjet_linux.sh

- cleanup_waterfox_linux.sh

- cleanup_Links2_linux.sh

- cleanup_SeaMonkey_linux.sh

- cleanup_edge_linux.sh

- cleanup_hardening_linux.sh

- cleanup_min_linux.sh

- cleanup_tor-browser_linux.sh


Aufräumen der Systemhärtung - Shell Historie bereinigen
##############################################################################

Mit diesem bash Skript werden von diversen Shells wie **bash**, **dash** 
und **ksh** die Shell Historie bereinigt. Nachstehend die Liste 
der unterstützten Shells:

- **bash**

- **tcsh**

- **fish**

- **ksh**

- **ash**

- **crosh**

- **csh**

- **sh**

- **zsh**

- **dash**

- **nash**

- **esh**

- **sash**

- **scsh**

- **eshell**

- **xonsh**

- **elvish**


.. literalinclude:: 
   _scripts/cleanup_shell_history.sh
   :language: bash
   :force:
   :linenos:
   :caption: cleanup_shell_history.sh

Aufräumen der Systemhärtung - Log Dateien bereinigen
##############################################################################

Mit diesem bash Skript werden die Log Dateien wie **/var/log/** bereinigt.

.. literalinclude::
   ;_scripts/hardening_privacy_new.sh
   :emphasize-lines: 1,2,20-50
   :language: bash
   :force:
   :linenos:
   :caption: hardening_privacy_new.sh


Prüfung der Systemhärtung
==============================================================================

Die Prüfung der Systemhärtung erfolgt mit nachstehenden 
Bash-Skripten und Ansible-Rollen, um festzustellen 
ob die die Härtungsmaßnahmen erfolgreich waren:

Prüfung der Systemhärtung - Ansible Rolle - ssh-audit
##############################################################################

* Install ssh-audit and test the functionality
* Basis für die Ansible Rolle ist:
  <https://gitlab.com/vPierre/ndaal_public_ansible_role_auditing_ssh>
* Neustart 

Updating

printf "%s\n" "${array_packages[@]}"

.. literalinclude:: 
    _cfg/function_update_packages_for_package_manager.cfg
   :language: text
   :force:
   :linenos:
   :caption: function_update_packages_for_package_manager.cfg


.. code-block:: yaml

    ---
    - name: Get current date
    ansible.builtin.set_fact:
        current_date: "{{ ansible_date_time.date }}"

    - name: Install pre-requisites based on the OS Family
    ansible.builtin.include_tasks: "{{ ansible_os_family }}.yml"
    tags:
        - always

.. code-block:: yaml

    ---
    - name: Debian/Ubuntu Family | Update apt cache
    ansible.builtin.apt:
        update_cache: true
    become: true

    - name: Debian/Ubuntu Family | Install required packages
    ansible.builtin.apt:
        name:
        - python3-pip
        - python3-setuptools
        state: present
    become: true

    - name: Debian/Ubuntu Family | Install ssh-audit
    ansible.builtin.pip:
        name: ssh-audit
        executable: pip3
        state: present
    become: true
    when: ansible_os_family == 'Debian' and ansible_distribution_major_version != '12'

    # Installation with pipx package manager for Debian 12
    - name: Debian/Ubuntu Family | Install pipx
    ansible.builtin.apt:
        name: pipx
        state: present
    become: true
    when: ansible_os_family == 'Debian' and ansible_distribution_major_version == '12'

    - name: Debian/Ubuntu Family | Install ssh-audit
    community.general.pipx:
        name: ssh-audit
        state: present
    when: ansible_os_family == 'Debian' and ansible_distribution_major_version == '12'

    - name: Debian/Ubuntu Family | Ensure pipx path setup
    ansible.builtin.command: pipx ensurepath
    changed_when: false
    when: ansible_os_family == 'Debian' and ansible_distribution_major_version == '12'

    - name: Copy custom SSH policy file
    ansible.builtin.template:
        src: policy.txt.j2
        dest: /tmp/policy.txt

    - name: Debian/Ubuntu Family | Run policy tests
    environment:
        PATH: "{{ ansible_env.PATH }}:{{ ansible_env.HOME }}/.local/bin"
    ansible.builtin.shell: ssh-audit -P /tmp/policy.txt localhost -p {{ ssh_audit_port }}
    register: debian_audit_results
    become: true
    changed_when: false

    - name: Debian/Ubuntu Family | Parse SSH Audit Results
    ansible.builtin.set_fact:
        debian_audit_passed: "{{ 'Passed' in debian_audit_results.stdout }}"

    - name: Debian/Ubuntu Family | Assert SSH Audit Results
    ansible.builtin.assert:
        that:
        - debian_audit_passed
        success_msg: "Debian SSH Audit passed."
        fail_msg: "Debian SSH Audit failed."


.. literalinclude:: 
   _scripts/execute_Ansible_Roles.sh
   :language: bash
   :force:
   :linenos:
   :caption: execute_Ansible_Roles.sh

Prüfung der Systemhärtung - Ansible Rolle - Yara
##############################################################################

Installiere Yara und teste die Funktionalität

.. code-block:: yaml

    ---
    - name: Get current date
    ansible.builtin.set_fact:
        current_date: "{{ ansible_date_time.date }}"

    - name: Install pre-requisites based on the OS Family
    ansible.builtin.include_tasks: "{{ ansible_os_family }}.yml"
    tags:
        - always

.. code-block:: yaml

    ---
    - name: Debian/Ubuntu Family | Update cache & Full system update
    ansible.builtin.apt:
        update_cache: true
        upgrade: dist
        cache_valid_time: 3600
        force_apt_get: true
    become: true

    - name: Ubuntu/Debian Family | Install YARA
    ansible.builtin.apt:
        name: yara
        state: present
    become: true

    - name: Ubuntu/Debian Family | Create Rules Directory
    ansible.builtin.file:
        path: "{{ linux_dest_dir }}"
        state: directory
        mode: "0755"
        owner: "{{ ansible_user }}"
        group: "{{ ansible_user }}"
        recurse: true
    become: false

    - name: Ubuntu/Debian Family | Download directory from Github as tar.gz
    ansible.builtin.get_url:
        url: "{{ rules_repo_tar }}"
        dest: "{{ linux_dest_dir }}/repo.tar.gz"
        mode: "0755"
        owner: "{{ ansible_user }}"
        group: "{{ ansible_user }}"
    become: false

    - name: Ubuntu/Debian Family | Unzip the downloaded file
    ansible.builtin.unarchive:
        src: "{{ linux_dest_dir }}/repo.tar.gz"
        dest: "{{ linux_dest_dir }}"
        remote_src: true
        owner: "{{ ansible_user }}"
        group: "{{ ansible_user }}"
    become: false

    - name: Clean up on remote host
    ansible.builtin.file:
        path: "{{ linux_dest_dir }}/repo.tar.gz"
        state: absent

.. code-block:: yaml

    ---
    - name: Redhat Family | Install YUM package manager
    ansible.builtin.yum:
        name:
        - yum
        - git
        state: present

    - name: Redhat Family | Install EPEL repo
    ansible.builtin.include_tasks: epel_repo.yml
    tags:
        - always

    - name: Redhat Family | Install RPM packages for RHEL_7 distributions
    ansible.builtin.include_tasks: install_rpms.yml
    when: ansible_distribution_major_version >= '7'and ansible_distribution_major_version < '8'

    - name: Redhat Family | Install DNF package manager
    ansible.builtin.yum:
        name: dnf
        state: present
    when: ansible_distribution == 'RedHat' and ansible_distribution_major_version >= '8'

    - name: RedHat Family | Check directories
    ansible.builtin.stat:
        path: "{{ linux_dest_dir }}"
    register: directory_data

    - name: RedHat Family | Create directory if it doesn't already exist
    ansible.builtin.file:
        path: "{{ linux_dest_dir }}"
        state: directory
        owner: "{{ ansible_user_id }}"
        mode: "0755"
    when: not directory_data.stat.exists

    - name: Redhat Family | Get compiled source code of YARA for RHEL_8 distributions
    ansible.builtin.include_tasks: compiled_yara.yml
    when: ansible_distribution == 'RedHat' and ansible_distribution_major_version >= '8'

    - name: Redhat Family | Get YARA version
    ansible.builtin.command:
        cmd: /usr/local/bin/yara --version
    register: yara_version_output
    ignore_errors: true

    - name: Redhat Family | Fix YARA library error
    ansible.builtin.command: sh -c 'echo "/usr/local/lib" >> /etc/ld.so.conf && ldconfig'
    changed_when: yara_version_output.rc != 0
    when: yara_version_output.rc != 0
    become: true

    - name: Redhat Family | Get YARA version
    ansible.builtin.command: /usr/local/bin/yara --version

    - name: RedHat Family | Download rules repository as tar.gz
    ansible.builtin.get_url:
        url: "{{ rules_repo_tar }}"
        dest: "{{ linux_dest_dir }}/repo_rules.tar.gz"
        mode: "0755"

    - name: RedHat Family | Unzip rules
    ansible.builtin.unarchive:
        src: "{{ linux_dest_dir }}/repo_rules.tar.gz"
        dest: "{{ linux_dest_dir }}"
        remote_src: true

Prüfung der Systemhärtung - Ansible Rolle - Dokumentation mit UAC
##############################################################################

* Platzhalter Ansible Rolle
* Neustart 

.....

.. Rubric:: Footnotes

.. [1]
   Debian Linux
   https://www.debian.org

.. [2]
   Wikipedia Artikel Entropie
   https://de.wikipedia.org/wiki/Entropie_(Informationstheorie)
