#!/usr/bin/env bash

# Author: Pierre Gronau <Pierre.Gronau@ndaal.eu>
# Copyright 2024
# License: All content is licensed under the terms of the <Apache 2.0>
# Developed on: Debian 12.x; macOS Sequoia x86 architecture
# Tested on: Debian 12.x; macOS Sequoia x86 architecture

set -o errexit
set -o errtrace
set -o nounset
set -o pipefail
# set -o xtrace
# nosemgrep: ifs-tampering
IFS=$'\n\t'

# Error trapping function
# Add this line before the error_handler function
# shellcheck disable=SC2317
error_handler() {
    local line_number="${1}"
    local error_code="${2}"
    local last_command="${BASH_COMMAND}"
    printf "Error: Error occurred in line %s (error code: %s)\n" "${line_number}" "${error_code}"
    printf "Error: Failed command: %s\n" "${last_command}"
}

# Set the error trap
trap 'error_handler ${LINENO} $?' ERR

cleanup() {
    trap - SIGINT SIGTERM ERR EXIT
    printf "%b\n" "\nInfo: Cleanup is running ..."
    log "Info: Cleanup is running ..."
    # Additional cleanup tasks can be added here if needed
    rm -f -v ./*.tmp
    printf "%b\n" "\nInfo: Cleanup finished ..."
    log "Info: Cleanup finished ..."
}

trap cleanup SIGINT SIGTERM ERR EXIT

if [[ "$(uname)" == "Darwin" ]]; then
    HOMEDIR="Users"
    readonly HOMEDIR
elif [[ "$(uname)" == "Linux" ]]; then
    HOMEDIR="home"
    readonly HOMEDIR
else
    printf "Error: Unsupported operating system: %s\n" "$(uname)"
    exit 1
fi

# Under Linux you use `home` under macOS `Users`
printf "Info: Home Directory: %s\n" "${HOMEDIR}"

USERSCRIPT="cloud"
# Your user! In which context it SHOULD run
printf "Info: User script: %s\n" "${USERSCRIPT}"

VERSION="1.0.0"
readonly VERSION
printf "Info: Current version: %s\n" "${VERSION}"

LOG_FILE="/${HOMEDIR}/${USERSCRIPT}/vagrant_setup.log"
readonly LOG_FILE 
printf "Info: log file with path: %s\n" "${LOG_FILE}"
if [[ ! -f "${LOG_FILE}" ]]; then
    printf "Info: Log file does not exist. Creating it now.\n"
    touch "${LOG_FILE}" || {
        printf "Error: Failed to create log file at %s\n" "${LOG_FILE}"
        exit 1
    }
else
    printf "Info: Log file already exists at %s\n" "${LOG_FILE}"
fi

log() {
    printf "%s - %s\n" "$(date '+%Y-%m-%d %H:%M:%S')" "${1}" | tee -a "${LOG_FILE}"
}

log "starting script"

print_asterisks_value=79
readonly print_asterisks_value 
printf "Info: Current print_asterisks_value: %s\n" "${print_asterisks_value}"

# Function to print a line of x asterisks
print_asterisks() {
    local x="${1}"

    # Check if x is within the valid range
    if [[ "${x}" -lt 10 || "${x}" -gt 79 ]]; then
        printf "Error: The value of x (%d) must be between 10 and 79.\n" "${x}"
        exit 1
    fi

    # Print x asterisks
    printf "%0.s*" $(seq 1 "${x}")
    printf "\n"

    # Success case
    return 0
}

print_asterisks "${print_asterisks_value}"

DEBUG=1 # Set to 1 to enable debug output, 0 to disable
readonly VERSION

# Print messages based on the value of DEBUG
if [[ "${DEBUG}" -eq 0 ]]; then
    printf "Info: DEBUG is set to 0. Debug output is disabled.\n"
elif [[ "${DEBUG}" -eq 1 ]]; then
    printf "Info: DEBUG is set to 1. Debug output is enabled.\n"
else
    printf "Error: Invalid value for DEBUG: %s. Please set it to 0 or 1.\n" "${DEBUG}"
fi

# Set DESTROY variable
DESTROY=1  # Change this to 1 to destroy the VMs
readonly DESTROY

# Print messages based on the value of DESTROY
if [[ "${DESTROY}" -eq 0 ]]; then
    printf "Info: DESTROY is set to 0. No action will be taken on the VMs.\n"
elif [[ "${DESTROY}" -eq 1 ]]; then
    printf "Warning: DESTROY is set to 1. All VMs will be destroyed!\n"
else
    printf "Error: Invalid value for DESTROY: %s. Please set it to 0 or 1.\n" "${DESTROY}"
fi

print_asterisks "${print_asterisks_value}"

expected_user="cloud"
readonly expected_user
printf "Info: Current expected_user: %s\n" "${expected_user}"
expected_group="staff"
readonly expected_group
printf "Info: Current expected_group: %s\n" "${expected_group}"

max_line_length="${2:-1000}"
printf "Info: Current max_line_length: %s\n" "${max_line_length}"

default_encoding="UTF-8"
readonly default_encoding
printf "Info: Current default_encoding: %s\n" "${default_encoding}"

vagrant_scp_check=0  # Change this to 1 to check the vagrant_scp plugin
readonly vagrant_scp_check
printf "Info: Current vagrant_scp_check: %s\n" "${vagrant_scp_check}"

do_copy_files_to_vms=0
readonly do_copy_files_to_vms
printf "Info: Current do_copy_files_to_vms: %s\n" "${do_copy_files_to_vms}"

do_copy_files_from_directory_to_vms=0
readonly do_copy_files_from_directory_to_vms
printf "Info: Current do_copy_files_from_directory_to_vms: %s\n" "${do_copy_files_from_directory_to_vms}"

do_make_files_executable=0
readonly do_make_files_executable
printf "Info: Current do_make_files_executable: %s\n" "${do_make_files_executable}"

package_manager_choice="apt"
printf "Info: Default package_manager_choice: %s\n" "${package_manager_choice}"

vm_name="debian"
printf "Info: Default vm_name: %s\n" "${vm_name}"

do_update_packages=0
readonly do_update_packages
printf "Info: Current do_update_packages: %s\n" "${do_update_packages}"

do_install_packages=0
readonly do_install_packages
printf "Info: Current do_install_packages: %s\n" "${do_install_packages}"

do_cleanup_packages=0
readonly do_cleanup_packages
printf "Info: Current do_cleanup_packages: %s\n" "${do_cleanup_packages}"

print_asterisks "${print_asterisks_value}"

# Set SSH_CHECK variable
SSH_CHECK=0  # Change this to 1 to SSH into all VMs
readonly SSH_CHECK

# Print messages based on the value of SSH_CHECK
if [[ "${SSH_CHECK}" -eq 0 ]]; then
    printf "Info: SSH_CHECK is set to 0. No SSH actions will be performed on the VMs.\n"
elif [[ "${SSH_CHECK}" -eq 1 ]]; then
    printf "Info: SSH_CHECK is set to 1. SSH will be attempted on all VMs.\n"
else
    printf "Error: Invalid value for SSH_CHECK: %s. Please set it to 0 or 1.\n" "${SSH_CHECK}"
fi

log() {
    printf "%s - %s\n" "$(date '+%Y-%m-%d %H:%M:%S')" "${1}" | tee -a "${LOG_FILE}"
}

# Function to check if a command is available
check_command() {
    if ! command -v "${1}" &>/dev/null; then
        printf "Error: %s is not installed or not in PATH. Please install it and try again.\n" "${1}"
        return 1
    fi

    # Success case
    return 0
}

# Check for required commands
check_command "vagrant"
check_command "virtualbox"

# Define VMs and their configurations
declare -A vms=(
    ["debian"]="debian/bookworm64"
    ["debian-automation-controller"]="debian/bookworm64"
    ##["debian-job-controller"]="debian/bookworm64"
    #["roboxes-alma8"]="roboxes/alma8"
    #["roboxes-alma9"]="roboxes/alma9"
    #["roboxes-arch"]="roboxes/arch"
    #["roboxes-centos6"]="roboxes/centos6"
    #["roboxes-centos7"]="roboxes/centos7"
    #["roboxes-centos8"]="roboxes/centos8"
    #["roboxes-centos8s"]="roboxes/centos8s"
    #["roboxes-centos9s"]="roboxes/centos9s"
    #["roboxes-debian8"]="roboxes/debian8"
    #["roboxes-debian9"]="roboxes/debian9"
    #["roboxes-debian10"]="roboxes/debian10"
    #["roboxes-debian11"]="roboxes/debian11"    
    #["roboxes-debian12"]="roboxes/debian12"
)

# Function to SSH into a specific VM
vagrant_ssh() {
    local vm_name="${1}"
    
    # Check if the VM name exists in the array
    if [[ -v vms["${vm_name}"] ]]; then
        printf "Info: Attempting to connect to VM: %s\n" "${vm_name}"
        if vagrant ssh "${vm_name}" -c "exit" > /dev/null 2>&1; then
            printf "Info: Successfully connected to VM: %s\n" "${vm_name}"
        else
            printf "Error: Unable to SSH into VM: %s\n" "${vm_name}"
        fi
    else
        printf "Error: VM '%s' not found.\n" "${vm_name}"
        printf "Info: Available VM through ssh: %s\n" "${!vms[@]}"
    fi

    # Success case
    return 0
}

# Function to update packages on a VM based on its package manager
update_packages() {
    local package_manager="${1:-}"

    case "${package_manager}" in
        -apt|-dpkg|-debian|-ubuntu|apt|dpkg|debian|ubuntu)
            printf "Info: Using apt package manager\n"
            printf "Info: Updating system\n" 
            sudo apt update
            sudo apt upgrade -y 
            ;;
        -pacman|-arch|pacman|arch)
            printf "Info: Using pacman package manager\n"
            printf "Info: Updating system\n"
            sudo pacman -Syu --noconfirm 
            ;;
        -zypper|-suse|-opensuse|zypper|suse|opensuse)
            printf "Info: Using zypper package manager\n"
            printf "Info: Updating system\n"
            sudo zypper lr
            sudo zypper mr -d repo-backports-update
            sudo zypper mr -d repo-update-non-oss
            sudo zypper lr            
            sudo zypper ref
            sudo zypper update -y 
            ;;
        -brew|-mac|-macos|-osx|brew|mac|macos|osx)
            printf "Info: Using Homebrew package manager\n"
            printf "Info: Updating system\n"
            brew update
            brew upgrade
            ;;
        -dnf|dnf)
            printf "Info: Using dnf package manager\n"
            printf "Info: Updating system\n"
            sudo dnf upgrade --refresh -y 
            ;;
        -rpm|rpm)
            printf "Info: Using rpm package manager\n"
            printf "Info: Updating system\n"
            sudo yum update -y
            ;;
        -yum|yum)
            printf "Info: Using yum package manager\n"
            printf "Info: Updating system\n"
            sudo yum update -y
            ;;
        -xbps|-void|xbps|void)
            printf "Info: Using xbps package manager\n"
            printf "Info: Updating system\n"
            sudo xbps-install -Su
            ;;
        -pisi|-solus|pisi|solus)
            printf "Info: Using pisi package manager\n"
            printf "Info: Updating system\n"
            sudo eopkg upgrade -y
            ;;
        -nix|-nixos|nix|nixos)
            printf "Info: Using nix package manager\n"
            printf "Info: Updating system\n"
            sudo nix-channel --update
            sudo nix-env -u '*'
            ;;
        -yay|yay)
            printf "Info: Using yay package manager\n"
            printf "Info: Updating system\n"
            yay -Syu --noconfirm ;;
        -equo|-sabayon|equo|sabayon)
            printf "Info: Using equo package manager\n"
            printf "Info: Updating system\n"
            sudo equo update -y
            sudo equo upgrade -y
            ;;
        -aurman|aurman)
            printf "Info: Using aurman package manager\n"
            printf "Info: Updating system\n"
            aurman -Syu --noconfirm ;;
        -flatpak|flatpak)
            printf "Info: Using flatpak package manager\n"
            printf "Info: Updating system\n"
            flatpak update -y ;;
        -snap|snap)
            printf "Info: Using snap package manager\n"
            printf "Info: Updating system\n"
            sudo snap refresh ;;
        -cargo|-rust|cargo|rust)
            printf "Info: Using cargo package manager\n"
            printf "Info: Updating system\n"
            cargo install cargo-update
            cargo install $(cargo install --list | grep -E '^[a-z0-9_-]+ v[0-9.]+:$' | cut -f1 -d' ' | xargs)
            ;;
        -portage|portage)
            printf "Info: Using portage package manager\n"
            printf "Info: Updating system\n"
            sudo emerge -uDN @world --quiet-build=y
            ;;
        -paludis|paludis)
            printf "Info: Using paludis package manager\n"
            printf "Info: Updating system\n"
            sudo cave resolve world -cx
            ;;
        -conary|conary)
            printf "Info: Using conary package manager\n"
            printf "Info: Updating system\n"
            sudo conary updateall --yes
            ;;
        -slackpkg|-slackware|slackpkg|slackware)
            printf "Info: Using slackpkg package manager\n"
            printf "Info: Updating system\n"
            sudo slackpkg update
            sudo slackpkg upgrade-all -batch=on -default_answer=y
            ;;
        -zeroinstall|zeroinstall)
            printf "Info: Using zeroinstall package manager\n"
            printf "Info: Updating system\n"
            0install update --all
            ;;
        -pip|-pip3|pip|pip3)
            printf "Info: Using pip package manager\n"
            printf "Info: Upgrading pip\n"
            pip3 install --upgrade pip || true
            printf "Info: Installing/upgrading extractcode\n"
            pip3 install --upgrade 'extractcode[full]' || true
            ;;
        -h|-help)
            printf "HINT: Select some argument to use a package manager and install the requirements like -apt, -zypper, -dnf, -rpm, -arch, -xbps, -pisi, -nix, -yay, -equo, -pip, -flatpak, -snap, -brew, -cargo etc.\n"
            ;;
        *)
            printf "Warning: Unsupported package manager '%s'. Skipping update.\n" "${package_manager}" 
            ;;
    esac

    printf "Info: Packages updated using '%s'.\n" "${package_manager}"
    
    # Success case
    return 0
}

# Function to create VMs
create_vms() {
    if [ "${DESTROY:-0}" -eq 1 ]; then
        printf "Info: DESTROY is set to 1. Destroying existing VMs...\n"

        for vm_name in "${!vms[@]}"; do
            if [ -d "./vagrant/${vm_name}" ]; then
                pushd "./vagrant/${vm_name}" > /dev/null || exit 1
                vagrant destroy -f || printf "Warning: Failed to destroy VM '%s'.\n" "${vm_name}"
                popd > /dev/null || exit 1
            fi
        done

        printf "Info: All existing VMs destroyed.\n"
    else
        printf "Info: DESTROY is not set to 1. Skipping VM destruction.\n"
    fi

    printf "Info: Creating VMs...\n"

    for vm_name in "${!vms[@]}"; do
        local box="${vms[${vm_name}]}"
        printf "Info: Setting up VM '%s' with box '%s'\n" "${vm_name}" "${box}"

        mkdir -p "./vagrant/${vm_name}"

        if [ "${vm_name}" == "sabayon" ]; then
            cat > "./vagrant/${vm_name}/Vagrantfile" <<EOF
Vagrant.configure("2") do |config|
  config.vm.box = "Sabayon/spinbase-amd64"

  config.vm.provision "shell", inline: <<-SHELL
    if command -v hostnamectl >/dev/null 2>&1; then
      hostnamectl set-hostname 'sabayon'
    else
      echo 'sabayon' > /etc/hostname
      hostname 'sabayon'
    fi
  SHELL
end
EOF
        else
            cat > "./vagrant/${vm_name}/Vagrantfile" <<EOF
Vagrant.configure("2") do |config|
  config.vm.box = "${box}"
  config.vm.hostname = "${vm_name}"
end
EOF
        fi

        pushd "./vagrant/${vm_name}" > /dev/null || exit 1
        vagrant up || { printf "Error: Failed to bring up VM '%s'.\n" "${vm_name}"; exit 1; }
        popd > /dev/null || exit 1

        printf "Info: VM '%s' is up and running.\n" "${vm_name}"
    done

    printf "Info: All VMs created successfully.\n"
    
    # Success case
    return 0
}

# Define packages to install
array_packages=(
    "wget"
    "curl"
    "git"
    "git-lfs"
    "ansible"
    "ansible-lint"
)
readonly array_packages
printf "Info: Packages for all VMs:\n"
printf "Info: Package: %s\n" "${array_packages[@]}"

array_packages_automation_controller=(
    "apt-transport-https"
    "ansible"
    "ansible-lint"
    "terraform"
)
readonly array_packages_automation_controller
printf "Info: Packages for Automation:\n"
printf "Info: Package: %s\n" "${array_packages_automation_controller[@]}"

array_packages_job_controller=(
    "apt-transport-https"
    "openjdk"
)
readonly array_packages_job_controller
printf "Info: Packages for Job Controller:\n"
printf "Info: Package: %s\n" "${array_packages_job_controller[@]}"

# Function to install packages
install_packages() {
    local package_manager="${1}"
    shift
    local packages=("$@")

    case "${package_manager}" in
        -apt|-dpkg|-debian|-ubuntu|apt|dpkg|debian|ubuntu)
            printf "Info: Using apt package manager\n"
            printf "Info: Installing packages ...\n"
            export LC_ALL=C.UTF-8
            export LANG=C.UTF-8
            sudo apt-get install -y "${packages[@]}" || true
            ;;
        -pacman|-arch|pacman|arch)
            printf "Info: Using pacman package manager\n"
            printf "Info: Installing packages ...\n"
            sudo pacman -Sy "${packages[@]}" || true
            ;;
        -zypper|-suse|-opensuse|zypper|suse|opensuse)
            printf "Info: Using zypper package manager\n"
            printf "Info: Installing packages ...\n"
            sudo zypper install -y "${packages[@]}" || true
            ;; 
        -brew|-mac|-macos|-osx|brew|mac|macos|osx)
            printf "Info: Using Homebrew package manager\n"
            printf "Info: Installing packages ...\n"
            brew install "${array_packages[@]}" || true
            ;;
        -dnf|dnf)
            printf "Info: Using dnf package manager\n"
            printf "Info: Installing packages ...\n"
            sudo dnf install -y "${packages[@]}" || true
            ;;    
        -rpm|rpm)
            printf "Info: Using rpm package manager\n"
            printf "Info: Installing packages ...\n"
            rpm -ivh "${packages[@]}" || true
            ;; 
        -yum|yum)
            printf "Info: Using yum package manager\n"
            printf "Info: Installing packages ...\n"
            sudo yum install -y "${packages[@]}" || true
            ;;
        -xbps|-void|xbps|void)
            printf "Info: Using xbps package manager\n"
            printf "Info: Installing packages ...\n"
            sudo xbps-install -Sy "${packages[@]}" || true
            ;;
        -pisi|-solus|pisi|solus)
            printf "Info: Using pisi package manager\n"
            printf "Info: Installing packages ...\n"
            sudo pisi install "${packages[@]}" || true
            ;;
        -nix|-nixos|nix|nixos)
            printf "Info: Using nix package manager\n"
            printf "Info: Installing packages ...\n"
            sudo nix-env -i "${packages[@]}" || true
            ;;
        -yay|yay)
            printf "Info: Using yay package manager\n"
            printf "Info: Installing packages ...\n"
            sudo yay -S "${packages[@]}" || true
            ;;
        -equo|-sabayon|equo|sabayon)
            printf "Info: Using equo package manager\n"
            printf "Info: Installing packages ...\n"
            sudo equo i "${packages[@]}" || true
            ;;
        -pip|-pip3|pip|pip3)
            printf "Info: Using pip package manager\n"
            printf "Info: Installing packages ...\n"
            pip3 install "${packages[@]}" || true
            ;;
        -aurman|aurman)
            printf "Info: Using aurman package manager\n"
            printf "Info: Installing packages ...\n"
            aurman --sync --noedit --noconfirm "${packages[@]}" || true
            ;;
        -flatpak|flatpak)
            printf "Info: Using flatpak package manager\n"
            printf "Info: Installing packages ...\n"
            flatpak install flathub -y "${packages[@]}" || true
            ;;
        -snap|snap)
            printf "Info: Using snap package manager\n"
            printf "Info: Installing packages ...\n"
            sudo snap install "${packages[@]}" || true
            ;;
        -cargo|-rust|cargo|rust)
            printf "Info: Using cargo package manager\n"
            printf "Info: Installing packages ...\n"
            cargo install "${packages[@]}" || true
            ;;
        -portage|portage)
            printf "Info: Using portage package manager\n"
            printf "Info: Installing packages ...\n"
            sudo emerge -v "${packages[@]}" || true
            ;;
        -paludis|paludis)
            printf "Info: Using paludis package manager\n"
            printf "Info: Installing packages ...\n"
            sudo cave resolve "${packages[@]}" -x || true
            ;;
        -conary|conary)
            printf "Info: Using conary package manager\n"
            printf "Info: Installing packages ...\n"
            sudo conary update "${packages[@]}" || true
            ;;
        -slackpkg|-slackware|slackpkg|slackware)
            printf "Info: Using slackpkg package manager\n"
            printf "Info: Installing packages ...\n"
            sudo slackpkg install -y "${packages[@]}" || true
            ;;
        -zeroinstall|zeroinstall)
            printf "Info: Using zeroinstall package manager\n"
            printf "Info: Installing packages ...\n"
            0install add "${packages[@]}" || true
            ;;
        -pip|-pip3|pip|pip3)
            printf "Info: Using pip package manager\n"
            printf "Info: Installing packages ...\n"
            pip3 install "${packages[@]}" || true
            ;;
        -h|-help)
            printf "HINT: Select some argument to use a package manager and install the requirements like -apt, -zypper, -dnf, -rpm, -arch, -xbps, -pisi, -nix, -yay, -equo, -pip, -flatpak, -snap, -brew, -cargo etc.\n"
            ;;
        *)
            printf "NOTE: Select some argument to use a package manager and install the requirements like -apt, -zypper, -dnf, -rpm, -arch, -xbps, -pisi, -nix, -yay, -equo, -pip, -flatpak, -snap, -brew, cargo, etc.\n"
            ;;
    esac
    
    # Success case
    return 0
}

restart_vm_and_check_uptime() {
    # Restart the VM
    printf "Info: Restarting the VM...\n"
    vagrant ssh -c "sudo init 6" || {
        printf "Error: Failed to restart the VM.\n"
        exit 1
    }

    # Wait for a few seconds to allow the VM to restart
    sleep 10  # Adjust the sleep duration as needed

    # Check uptime after restart
    printf "Info: Checking uptime for the VM...\n"
    vagrant ssh -c "sudo uptime" || {
        printf "Error: Failed to check uptime for the VM.\n"
        exit 1
    }

    # Success case
    return 0
}


# Main function
main() {
    create_vms

    #vagrant global-status

    for vm_name in "${!vms[@]}"; do
        printf "Info: Processing VM '%s'...\n" "${vm_name}"

        pushd "./vagrant/${vm_name}"
        #> /dev/null || exit 1

        # Check SSH for each VM in the array if SSH_CHECK is set to 1
        if [[ "${SSH_CHECK}" -eq 1 ]]; then
            for vm in "${!vms[@]}"; do
                printf "Info: Checking VM: %s\n" "${vm}"  # Indicate which VM is being checked
                vagrant_ssh "${vm}"
            done
        else
            printf "Info: SSH_CHECK is not set to 1. Skipping SSH checks.\n"
        fi

    if [ "${vm_name}" == "debian" ]; then
        package_manager_choice="apt"
        printf "Info: Current package_manager_choice: %s\n" "${package_manager_choice}"

        printf "Info: install apt-transport-https: %s\n"
        vagrant ssh -c "sudo apt-get update && sudo apt-get -y install apt-transport-https"

        printf "Info: Current vm_name: %s\n" "${vm_name}"
        printf "Info: update_packages: %s\n"
        vagrant ssh -c "$(declare -f update_packages); update_packages ${package_manager_choice}" || {
            printf "Error: Failed to update/install packages in VM '%s'.\n" "${vm_name}"
            exit 1
        }

        #vagrant ssh -c "sudo init 6"

        printf "Info: Current vm_name: %s\n" "${vm_name}"
        vagrant ssh -c "$(declare -f install_packages); install_packages ${package_manager_choice} ${array_packages[*]}; install_packages ${package_manager_choice} ${array_packages_automation_controller[*]}"


    elif [ "${vm_name}" == "debian-automation-controller" ]; then
        package_manager_choice="apt"
        printf "Info: Current package_manager_choice: %s\n" "${package_manager_choice}"

        printf "Info: install apt-transport-https: %s\n"
        vagrant ssh -c "sudo apt-get update && sudo apt-get -y install apt-transport-https"

        # Example usage
        restart_vm_and_check_uptime

        #vagrant ssh -c "sudo init 6"

        #vagrant ssh -c "sudo uptime"

        printf "Info: Current vm_name: %s\n" "${vm_name}"
        printf "Info: install_packages: %s\n"
        vagrant ssh -c "$(declare -f install_packages); install_packages ${package_manager_choice} ${array_packages[*]}; install_packages ${package_manager_choice} ${array_packages_automation_controller[*]}"

        #printf "Info: update_packages: %s\n" 
        #vagrant ssh -c "$(declare -f update_packages); update_packages ${package_manager_choice}" || {
        #    printf "Error: Failed to update/install packages in VM '%s'.\n" "${vm_name}"
        #    exit 1

        #}
    elif [ "${vm_name}" == "debian-job-controller" ]; then
        package_manager_choice="apt"
        printf "Info: Current package_manager_choice: %s\n" "${package_manager_choice}"

        vagrant ssh -c "sudo apt-get update && sudo apt-get -y install apt-transport-https"

        vagrant ssh -c "$(declare -f update_packages); update_packages ${package_manager_choice}" || {
            printf "Error: Failed to update/install packages in VM '%s'.\n" "${vm_name}"
            exit 1
        
        }
    elif [ "${vm_name}" == "ubuntu" ]; then
        package_manager_choice="apt"
        printf "Info: Current package_manager_choice: %s\n" "${package_manager_choice}"
        # For Ubuntu, install specific packages
        vagrant ssh -c "$(declare -f update_packages install_packages); update_packages ${package_manager_choice}; install_packages ${package_manager_choice} ${array_packages[*]}" || {
            printf "Error: Failed to update/install packages in VM '%s'.\n" "${vm_name}"
            exit 1
        }
    elif [[ "${vm_name}" == "pacman" || "${vm_name}" == "arch" ]]; then
        vagrant ssh -c "$(declare -f update_packages install_packages); update_packages pacman; install_packages pacman ${array_packages[*]}" || {
            printf "Error: Failed to update/install packages in VM '%s'.\n" "${vm_name}"
            exit 1
        }
    elif [[ "${vm_name}" == "zypper" || "${vm_name}" == "opensuse" || "${vm_name}" == "sles11sp4" ]]; then
        vagrant ssh -c "$(declare -f update_packages install_packages); update_packages zypper; install_packages zypper ${array_packages[*]}" || {
            printf "Error: Failed to update/install packages in VM '%s'.\n" "${vm_name}"
            exit 1
        }
    elif [[ "${vm_name}" == "brew" || "${vm_name}" == "macos" ]]; then
        vagrant ssh -c "$(declare -f update_packages install_packages); update_packages brew; install_packages brew ${array_packages[*]}" || {
            printf "Error: Failed to update/install packages in VM '%s'.\n" "${vm_name}"
            exit 1
        }
    elif [[ "${vm_name}" == "dnf" || "${vm_name}" == "almalinux8" || "${vm_name}" == "almalinux7" || "${vm_name}" == "almalinux" ]]; then
        # For AlmaLinux, install specific packages
        vagrant ssh -c "$(declare -f update_packages install_packages); update_packages dnf; install_packages dnf ${array_packages[*]}" || {
            printf "Error: Failed to update/install packages in VM '%s'.\n" "${vm_name}"
            exit 1
        }
    elif [[ "${vm_name}" == "rpm" || "${vm_name}" == "redhat" ]]; then
        vagrant ssh -c "$(declare -f update_packages install_packages); update_packages rpm; install_packages rpm ${array_packages[*]}" || {
            printf "Error: Failed to update/install packages in VM '%s'.\n" "${vm_name}"
            exit 1
        }
    elif [[ "${vm_name}" == "yum" || "${vm_name}" == "redhat2" ]]; then
        vagrant ssh -c "$(declare -f update_packages install_packages); update_packages yum; install_packages yum ${array_packages[*]}" || {
            printf "Error: Failed to update/install packages in VM '%s'.\n" "${vm_name}"
            exit 1
        }
    elif [[ "${vm_name}" == "xbps" || "${vm_name}" == "void" ]]; then
        vagrant ssh -c "$(declare -f update_packages install_packages); update_packages xbps; install_packages xbps ${array_packages[*]}" || {
            printf "Error: Failed to update/install packages in VM '%s'.\n" "${vm_name}"
            exit 1
        }
    elif [[ "${vm_name}" == "pisi" || "${vm_name}" == "solus" ]]; then
        vagrant ssh -c "$(declare -f update_packages install_packages); update_packages pisi; install_packages pisi ${array_packages[*]}" || {
            printf "Error: Failed to update/install packages in VM '%s'.\n" "${vm_name}"
            exit 1
        }
    elif [[ "${vm_name}" == "nix" || "${vm_name}" == "nixos" ]]; then
        vagrant ssh -c "$(declare -f update_packages install_packages); update_packages nix; install_packages nix ${array_packages[*]}" || {
            printf "Error: Failed to update/install packages in VM '%s'.\n" "${vm_name}"
            exit 1
        }
    elif [[ "${vm_name}" == "yay" || "${vm_name}" == "yay2" ]]; then
        vagrant ssh -c "$(declare -f update_packages install_packages); update_packages yay; install_packages yay ${array_packages[*]}" || {
            printf "Error: Failed to update/install packages in VM '%s'.\n" "${vm_name}"
            exit 1
        }
    elif [[ "${vm_name}" == "equo" || "${vm_name}" == "Sabayon" ]]; then
        vagrant ssh -c "$(declare -f update_packages install_packages); update_packages equo; install_packages equo ${array_packages[*]}" || {
            printf "Error: Failed to update/install packages in VM '%s'.\n" "${vm_name}"
            exit 1
        }
    elif [[ "${vm_name}" == "aurman" || "${vm_name}" == "aurman2" ]]; then
        vagrant ssh -c "$(declare -f update_packages install_packages); update_packages aurman; install_packages aurman ${array_packages[*]}" || {
            printf "Error: Failed to update/install packages in VM '%s'.\n" "${vm_name}"
            exit 1
        }
    elif [[ "${vm_name}" == "flatpak" || "${vm_name}" == "flatpak2" ]]; then
        vagrant ssh -c "$(declare -f update_packages install_packages); update_packages flatpak; install_packages flatpak ${array_packages[*]}" || {
            printf "Error: Failed to update/install packages in VM '%s'.\n" "${vm_name}"
            exit 1
        }
    elif [[ "${vm_name}" == "snap" || "${vm_name}" == "snap2" ]]; then
        vagrant ssh -c "$(declare -f update_packages install_packages); update_packages snap; install_packages snap ${array_packages[*]}" || {
            printf "Error: Failed to update/install packages in VM '%s'.\n" "${vm_name}"
            exit 1
        }
    elif [[ "${vm_name}" == "cargo" || "${vm_name}" == "rust" ]]; then
        vagrant ssh -c "$(declare -f update_packages install_packages); update_packages cargo; install_packages cargo ${array_packages[*]}" || {
            printf "Error: Failed to update/install packages in VM '%s'.\n" "${vm_name}"
            exit 1
        }
    elif [[ "${vm_name}" == "portage" || "${vm_name}" == "Gentoo" ]]; then
        vagrant ssh -c "$(declare -f update_packages install_packages); update_packages portage; install_packages port age ${array_packages[*]}" || {
            printf "Error: Failed to update/install packages in VM '%s'.\n" "${vm_name}"
            exit 1
        }
    elif [[ "${vm_name}" == "paludis" || "${vm_name}" == "Gentoo2" ]]; then
        vagrant ssh -c "$(declare -f update_packages install_packages); update_packages paludis; install_packages paludis ${array_packages[*]}" || {
            printf "Error: Failed to update/install packages in VM '%s'.\n" "${vm_name}"
            exit 1
        }
    elif [[ "${vm_name}" == "conary" || "${vm_name}" == "conary" ]]; then
        vagrant ssh -c "$(declare -f update_packages install_packages); update_packages conary; install_packages conary ${array_packages[*]}" || {
            printf "Error: Failed to update/install packages in VM '%s'.\n" "${vm_name}"
            exit 1
        }
    elif [[ "${vm_name}" == "slackpkg" || "${vm_name}" == "Slackware" ]]; then
        vagrant ssh -c "$(declare -f update_packages install_packages); update_packages slackpkg; install_packages slackpkg ${array_packages[*]}" || {
            printf "Error: Failed to update/install packages in VM '%s'.\n" "${vm_name}"
            exit 1
        }
    elif [[ "${vm_name}" == "zeroinstall" || "${vm_name}" == "zeroinstall2" ]]; then
        vagrant ssh -c "$(declare -f update_packages install_packages); update_packages zeroinstall; install_packages zeroinstall ${array_packages[*]}" || {
            printf "Error: Failed to update/install packages in VM '%s'.\n" "${vm_name}"
            exit 1
        }
    else
        # Skip package installation for other VMs
        printf "Info: Skipping package installation for VM '%s'.\n" "${vm_name}"
    fi
        popd > /dev/null || exit 1

        printf "Info: Processing completed for VM '%s'.\n" "${vm_name}"
    done

    printf "Info: All tasks completed successfully.\n"
    
    # Success case
    return 0
}

main "$@"

cleanup

script_name1="$(basename "${0}")"
printf "\nInfo: script_name1: %s\n" "${script_name1}"
script_path1="$(realpath "$(dirname "${0}")")"
printf "Info: script_path1: %s\n" "${script_path1}"
script_path_with_name="${script_path1}/${script_name1}"
printf "Info: Script path with name: %s\n" "${script_path_with_name}"
printf "Info: Script finished\n"
exit 0
