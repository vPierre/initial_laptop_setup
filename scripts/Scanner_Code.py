# requirements.txt
Flask==2.3.2
python-magic==0.4.27
pyclamd==0.4.0
yara-python==4.3.1
requests==2.31.0
celery==5.3.4
redis==4.5.4
python-dotenv==1.0.0
pytest==7.3.1
structlog==23.1.0

# config.py
import os
from dotenv import load_dotenv

load_dotenv()

class Config:
    UPLOAD_FOLDER = os.getenv('UPLOAD_FOLDER', '/tmp/uploads')
    MAX_CONTENT_LENGTH = 16 * 1024 * 1024  # 16MB
    CLAMAV_SOCKET = os.getenv('CLAMAV_SOCKET', 'unix:///var/run/clamav/clamd.ctl')
    REDIS_URL = os.getenv('REDIS_URL', 'redis://localhost:6379/0')
    YARA_RULES_PATH = os.getenv('YARA_RULES_PATH', '/etc/yara/rules')
    SECRET_KEY = os.getenv('SECRET_KEY', 'development_secret')

# scanner.py
import os
import magic
import yara
import pyclamd
import structlog

class MalwareScanner:
    def __init__(self, config):
        self.logger = structlog.get_logger()
        self.config = config
        self.clamd_client = self._init_clamav()
        self.yara_rules = self._load_yara_rules()

    def _init_clamav(self):
        try:
            cd = pyclamd.ClamdNetworkSocket()
            cd.ping()
            return cd
        except Exception as e:
            self.logger.error(f"ClamAV initialization failed: {e}")
            return None

    def _load_yara_rules(self):
        try:
            rules_path = self.config.YARA_RULES_PATH
            yara_rules = {}
            for root, _, files in os.walk(rules_path):
                for file in files:
                    if file.endswith('.yar') or file.endswith('.yara'):
                        rule_path = os.path.join(root, file)
                        rule_name = os.path.splitext(file)[0]
                        yara_rules[rule_name] = yara.compile(rule_path)
            return yara_rules
        except Exception as e:
            self.logger.error(f"Yara rules loading failed: {e}")
            return {}

    def scan_file(self, file_path):
        results = {
            'clamav': self._scan_with_clamav(file_path),
            'yara': self._scan_with_yara(file_path),
            'file_type': self._detect_file_type(file_path)
        }
        return results

    def _scan_with_clamav(self, file_path):
        if not self.clamd_client:
            return {'status': 'error', 'message': 'ClamAV not available'}
        
        try:
            scan_result = self.clamd_client.scan_file(file_path)
            return {
                'status': 'infected' if scan_result else 'clean',
                'details': scan_result
            }
        except Exception as e:
            return {'status': 'error', 'message': str(e)}

    def _scan_with_yara(self, file_path):
        results = []
        try:
            with open(file_path, 'rb') as f:
                file_data = f.read()
                for rule_name, rule in self.yara_rules.items():
                    matches = rule.match(data=file_data)
                    if matches:
                        results.append({
                            'rule_name': rule_name,
                            'matches': [str(match) for match in matches]
                        })
            return results
        except Exception as e:
            return [{'status': 'error', 'message': str(e)}]

    def _detect_file_type(self, file_path):
        try:
            return magic.from_file(file_path)
        except Exception as e:
            return f"File type detection error: {e}"

# tasks.py (Celery)
from celery import Celery
from scanner import MalwareScanner
from config import Config

celery_app = Celery('malware_scanner', broker=Config.REDIS_URL)

@celery_app.task
def scan_file_task(file_path):
    scanner = MalwareScanner(Config)
    return scanner.scan_file(file_path)

# app.py (Flask API)
import os
from flask import Flask, request, jsonify
from werkzeug.utils import secure_filename
from tasks import scan_file_task
from config import Config

app = Flask(__name__)
app.config.from_object(Config)

@app.route('/scan', methods=['POST'])
def upload_and_scan():
    if 'file' not in request.files:
        return jsonify({'error': 'No file uploaded'}), 400
    
    file = request.files['file']
    if file.filename == '':
        return jsonify({'error': 'No selected file'}), 400
    
    filename = secure_filename(file.filename)
    file_path = os.path.join(app.config['UPLOAD_FOLDER'], filename)
    file.save(file_path)
    
    task = scan_file_task.delay(file_path)
    return jsonify({
        'task_id': task.id,
        'status': 'Scanning in progress'
    }), 202

@app.route('/result/<task_id>', methods=['GET'])
def get_scan_result(task_id):
    task = scan_file_task.AsyncResult(task_id)
    if task.ready():
        return jsonify(task.result)
    return jsonify({'status': 'Processing'}), 202

if __name__ == '__main__':
    app.run(debug=True)

# Dockerfile
FROM python:3.9-slim

RUN apt-get update && apt-get install -y \
    clamav \
    clamav-daemon \
    libmagic1 \
    wget \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /app

COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt

COPY . .

CMD ["gunicorn", "-b", "0.0.0.0:5000", "app:app"]
