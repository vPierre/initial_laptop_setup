#!/usr/bin/env bash

# Author: Pierre Gronau <Pierre.Gronau@ndaal.eu>
# Copyright 2024, 2025
# License: All content is licensed under the terms of the <Apache 2.0>
# Developed on: Debian 12.x; macOS Sequoia x86 architecture
# Tested on: Debian 12.x; macOS Sequoia x86 architecture

set -o errexit
set -o errtrace
set -o nounset
set -o pipefail
# set -o xtrace
# nosemgrep: ifs-tampering
IFS=$'\n\t'

# Error trapping function
# Add this line before the error_handler function
# shellcheck disable=SC2317
error_handler() {
    local line_number="${1}"
    local error_code="${2}"
    local last_command="${BASH_COMMAND}"
    printf "Error: Error occurred in line %s (error code: %s)\n" "${line_number}" "${error_code}"
    printf "Error: Failed command: %s\n" "${last_command}"
}

# Set the error trap
trap 'error_handler ${LINENO} $?' ERR

cleanup() {
    trap - SIGINT SIGTERM ERR EXIT
    printf "%b\n" "\nInfo: Cleanup is running ..."
    # Additional cleanup tasks can be added here if needed
    rm -f -v ./*.tmp
    printf "%b\n" "\nInfo: Cleanup finished ..."
}

trap cleanup SIGINT SIGTERM EXIT

if [[ "$(uname)" == "Darwin" ]]; then
    HOMEDIR="Users"
elif [[ "$(uname)" == "Linux" ]]; then
    HOMEDIR="home"
else
    printf "Error: Unsupported operating system: %s\n" "$(uname)"
    exit 1
fi
readonly HOMEDIR

# Under Linux you use `home` under macOS `Users`
printf "Info: Home Directory: %s\n" "${HOMEDIR}"

USERSCRIPT="cloud"
# Your user! In which context it SHOULD run
printf "Info: User script: %s\n" "${USERSCRIPT}"

VERSION="1.0.1"
readonly VERSION
printf "Info: Current version: %s\n" "${VERSION}"

LOG_FILE="/${HOMEDIR}/${USERSCRIPT}/remove_unwished_files.log"
readonly LOG_FILE
printf "Info: log file with path: %s\n" "${LOG_FILE}"
if [[ ! -f "${LOG_FILE}" ]]; then
    printf "Info: Log file does not exist. Creating it now.\n"
    touch "${LOG_FILE}" || {
        printf "Error: Failed to create log file at %s\n" "${LOG_FILE}"
        exit 1
    }
else
    printf "Info: Log file already exists at %s\n" "${LOG_FILE}"
fi

log() {
    printf "%s - %s\n" "$(date '+%Y-%m-%d %H:%M:%S')" "${1}" | tee -a "${LOG_FILE}"
}

log "starting script ..."

print_asterisks_value=79
readonly print_asterisks_value
printf "Info: *** print_asterisks_value: %s\n" "${print_asterisks_value}"

# Function to print a line of x asterisks
print_asterisks() {
    local x="${1}"

    # Check if x is within the valid range
    if [[ "${x}" -lt 10 || "${x}" -gt 79 ]]; then
        printf "Error: The value of x (%d) must be between 10 and 79.\n" "${x}"
        exit 1
    fi

    # Print x asterisks
    printf "%0.s*" $(seq 1 "${x}")
    printf "\n"

    # Success case
    return 0
}

print_asterisks "${print_asterisks_value}"

DESTINATION="/${HOMEDIR}/${USERSCRIPT}/repos/initial_laptop_setup/"
readonly DESTINATION
printf "Info: Destination Directory: %s\n" "${DESTINATION}"

# Function to check if a command is available
check_command() {
    if ! command -v "${1}" &>/dev/null; then
        printf "Error: %s is not installed or not in PATH. Please install it and try again.\n" "${1}"
        return 1
    fi

    # Success case
    return 0
}

# Check for required commands
check_command "find" || exit 1

# Define the array of unwanted file extensions globally
unwanted_file_extensions=(
    "thumbs.db"
    ".bak"
    ".bak.*"
    ".backup"
    "~"
    ".tmp"
    ".temp"
    ".DS_Store"
    ".crdownload"
    ".part"
    ".cache"
    ".dmp"
    ".download"
    ".partial"
    ".swp"
    ".log"
    ".old"
    ".$$$"
    ".wbk"
    ".xlk"
    ".~lock"
    ".lck"
    ".err"
    ".chk"
    ".sik"
    ".crash"
    ".temp$"
    ".bup"
    ".save"
    ".swn"
    ".swo"
)

Function_Remove_Unwished_Files() {
    printf "\nInfo: Remove Unwished Files\n"
    printf "Info: Currently, these file types are considered temporary:\n"
    printf "Info: %s\n" "${unwanted_file_extensions[*]}"
    printf "Warning: Some of these files may be important for recovery or debugging.\n"
    printf "Info: Always ensure you have backups before performing any mass deletion of files.\n\n"

    local total_removed=0
    for ext in "${unwanted_file_extensions[@]}"; do
        printf "Info: Removing files with extension/name: %s\n" "${ext}"
        local count=$(find . -name "*${ext}" -type f -print -delete | wc -l)
        total_removed=$((total_removed + count))
        printf "Info: Removed %d file(s) with extension/name %s\n" "${count}" "${ext}"
    done

    printf "\nInfo: Removal process completed. Total files removed: %d\n" "${total_removed}"
    printf "Info: Please review any error messages carefully.\n"

    # Success case
    return 0
}

Function_List_Unwished_Files() {
    printf "\nInfo: Listing Unwished Files\n"
    printf "Info: Currently, files with these extensions are considered temporary:\n"
    printf "Info: %s\n" "${unwanted_file_extensions[*]}"
    printf "Info: These files may be important for recovery or debugging.\n\n"

    local total_found=0
    for ext in "${unwanted_file_extensions[@]}"; do
        printf "Info: Listing files with extension/name: %s\n" "${ext}"
        local found_files=$(find . -name "${ext}" -type f)
        local count=$(echo "${found_files}" | grep -c '^')
        total_found=$((total_found + count))
        if [ ${count} -gt 0 ]; then
            printf "Info: Found %d file(s) with extension/name %s:\n" "${count}" "${ext}"
            echo "${found_files}"
        else
            printf "Info: No files found with extension/name %s\n" "${ext}"
        fi
        printf "\n"
    done

    printf "\nInfo: Listing process completed. Total files found: %d\n" "${total_found}"
    printf "Info: Review the list carefully before taking any action.\n"

    # Success case
    return 0
}

cd "${DESTINATION}" || exit
printf "Info: Changed to the desired Directory: %s\n" "${DESTINATION}"

printf "Info: All tasks completed successfully.\n"

Function_List_Unwished_Files
Function_Remove_Unwished_Files
Function_List_Unwished_Files

cleanup

log "stopping script ..."


script_name1="$(basename "${0}")"
printf "\nInfo: script_name1: %s\n" "${script_name1}"
script_path1="$(realpath "$(dirname "${0}")")"
printf "Info: script_path1: %s\n" "${script_path1}"
script_path_with_name="${script_path1}/${script_name1}"
printf "Info: Script path with name: %s\n" "${script_path_with_name}"
printf "Info: Script finished\n"
exit 0
