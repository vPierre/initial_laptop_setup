#!/usr/bin/env bash
# Author: Pierre Gronau <Pierre.Gronau@ndaal.eu>
# Copyright 2024
# License: All content is licensed under the terms of the <Apache 2.0>
# Developed on: Debian 12.8x; macOS Sequoia x86 architecture
# Tested on: Debian 12.8x; macOS Sequoia x86 architecture
#
set -o errexit
set -o errtrace
set -o nounset
set -o pipefail
# set -o xtrace
# nosemgrep: ifs-tampering
IFS=$'\n\t'

# Error trapping function
error_handler() {
    local line_number="${1}"
    local error_code="${2}"
    local last_command="${BASH_COMMAND}"
    printf "Error: Error occurred in line %s (error code: %s)\n" "${line_number}" "${error_code}"
    printf "Error: Failed command: %s\n" "${last_command}"
}

# Set the error trap
trap 'error_handler ${LINENO} $?' ERR

cleanup() {
    trap - SIGINT SIGTERM ERR EXIT
    printf "%b\n" "\nInfo: Cleanup is running ..."
    # Additional cleanup tasks can be added here if needed
    rm -f -v ./*.tmp
    printf "%b\n" "\nInfo: Cleanup finished ..."
}

trap cleanup SIGINT SIGTERM EXIT

if [[ "$(uname)" == "Darwin" ]]; then
    HOMEDIR="Users"
    readonly HOMEDIR
elif [[ "$(uname)" == "Linux" ]]; then
    HOMEDIR="home"
    readonly HOMEDIR
else
    printf "Error: Unsupported operating system: %s\n" "$(uname)"
    exit 1
fi

# Under Linux you use `home` under macOS `Users`
printf "Info: Home Directory: %s\n" "${HOMEDIR}"

USERSCRIPT="cloud"
# Your user! In which context it SHOULD run
printf "Info: User script: %s\n" "${USERSCRIPT}"

VERSION="0.1.0"
readonly VERSION
printf "Info: Current version: %s\n" "${VERSION}"

LOG_FILE="/${HOMEDIR}/${USERSCRIPT}/cleanup_package_manager.log"
readonly LOG_FILE 
printf "Info: log file with path: %s\n" "${LOG_FILE}"

if [[ ! -f "${LOG_FILE}" ]]; then
    printf "Info: Log file does not exist. Creating it now.\n"
    touch "${LOG_FILE}" || {
        printf "Error: Failed to create log file at %s\n" "${LOG_FILE}"
        exit 1
    }
else
    printf "Info: Log file already exists at %s\n" "${LOG_FILE}"
fi

# Function to check if a command is available
check_command() {
    if ! command -v "${1}" &>/dev/null; then
        printf "Error: %s is not installed or not in PATH. Please install it and try again.\n" "${1}"
        return 1
    fi

    # Success case
    return 0
}

# Check for required commands
check_command "packer"
check_command "vagrant"
check_command "curl"

packer version

# Create a directory for the Packer template
template_dir="./vagrant-debian-box"
mkdir -p "${template_dir}"
cd "${template_dir}"

mkdir -p http

#cp -f -p -v "/${HOMEDIR}/${USERSCRIPT}/repos/initial_laptop_setup/scripts/preseed.cfg" "/${HOMEDIR}/${USERSCRIPT}/repos/initial_laptop_setup/scripts/vagrant-debian-box"
cp -f -p -v "/${HOMEDIR}/${USERSCRIPT}/repos/initial_laptop_setup/scripts/preseed.cfg" "/${HOMEDIR}/${USERSCRIPT}/repos/initial_laptop_setup/scripts/vagrant-debian-box/http"

#packer init debian.pkr.hcl

# Create a Packer template file
cat <<EOF > "debian.pkr.hcl"
variable "debian_version" {
  type    = string
  default = "12.8.0"
}

source "virtualbox-iso" "debian" {
  iso_url            = "https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/debian-12.8.0-amd64-netinst.iso"
  iso_checksum       = "sha256:04396d12b0f377958a070c38a923c227832fa3b3e18ddc013936ecf492e9fbb3"
  ssh_username       = "vagrant"
  ssh_password       = "vagrant"
  shutdown_command   = "shutdown now"
  disk_size          = 8192
  communicator       = "ssh"
  boot_command = [
        "<wait>",
        "install auto=true priority=critical preseed/url=http://{{ .HTTPIP }}:{{ .HTTPPort }}/preseed.cfg<wait>",
        "<enter>"
  http_directory = "http"  # Directory where preseed.cfg will be served
}

build {
  sources = ["source.virtualbox-iso.debian"]

  provisioner "shell" {
    inline = [
      "apt-get update",
      "apt-get install -y build-essential",
      "apt-get install -y virtualbox-guest-utils"
    ]
  }
}
EOF

cp -f -p -v "/${HOMEDIR}/${USERSCRIPT}/repos/initial_laptop_setup/scripts/debian.pkr.hcl" "/${HOMEDIR}/${USERSCRIPT}/repos/initial_laptop_setup/scripts/vagrant-debian-box"

# Validate the Packer template
printf "Info: Validating Packer template...\n"
packer validate "debian.pkr.hcl"

# Build the Vagrant box
printf "Info: Building the Vagrant box...\n"
packer build "debian.pkr.hcl"

# Add the box to Vagrant
vagrant box add "debian-box" "output-vagrant/package.box"

# Initialize and start the Vagrant box
vagrant init "debian-box"
vagrant up

# Cleanup and exit
cleanup

script_name1="$(basename "${0}")"
printf "\nInfo: script_name1: %s\n" "${script_name1}"
script_path1="$(realpath "$(dirname "${0}")")"
printf "Info: script_path1: %s\n" "${script_path1}"
script_path_with_name="${script_path1}/${script_name1}"
printf "Info: Script path with name: %s\n" "${script_path_with_name}"
printf "Info: Script finished\n"
exit 0
