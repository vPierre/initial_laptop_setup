#!/usr/bin/env bash

# Author: Pierre Gronau <Pierre.Gronau@ndaal.eu>
# Copyright 2024, 2025
# License: All content is licensed under the terms of the <Apache 2.0>
# Developed on: Debian 12.x; macOS Sequoia x86 architecture
# Tested on: Debian 12.x; macOS Sequoia x86 architecture

set -o errexit
set -o errtrace
set -o nounset
set -o pipefail
# set -o xtrace
# nosemgrep: ifs-tampering
IFS=$'\n\t'

# Error trapping function
# Add this line before the error_handler function
# shellcheck disable=SC2317
error_handler() {
    local line_number="${1}"
    local error_code="${2}"
    local last_command="${BASH_COMMAND}"
    printf "Error: Error occurred in line %s (error code: %s)\n" "${line_number}" "${error_code}"
    printf "Error: Failed command: %s\n" "${last_command}"

    # Success case
    return 0
}

# Set the error trap
trap 'error_handler ${LINENO} $?' ERR

cleanup() {
    trap - SIGINT SIGTERM ERR EXIT
    printf "%b\n" "\nInfo: Cleanup is running ..."
    log "Info: Cleanup is running ..."
    # Additional cleanup tasks can be added here if needed
    rm -f -v ./*.tmp
    printf "%b\n" "\nInfo: Cleanup finished ..."
    log "Info: Cleanup finished ..."
}

trap cleanup SIGINT SIGTERM ERR EXIT

if [[ "$(uname)" == "Darwin" ]]; then
    HOMEDIR="Users"
    readonly HOMEDIR
elif [[ "$(uname)" == "Linux" ]]; then
    HOMEDIR="home"
    readonly HOMEDIR
else
    printf "Error: Unsupported operating system: %s\n" "$(uname)"
    exit 1
fi

# Under Linux you use `home` under macOS `Users`
printf "Info: Home Directory: %s\n" "${HOMEDIR}"

USERSCRIPT="cloud"
# Your user! In which context it SHOULD run
printf "Info: User script: %s\n" "${USERSCRIPT}"

VERSION="1.0.0"
readonly VERSION
printf "Info: Current version: %s\n" "${VERSION}"

LOG_FILE="/${HOMEDIR}/${USERSCRIPT}/vagrant_setup.log"
readonly LOG_FILE 
printf "Info: log file with path: %s\n" "${LOG_FILE}"
if [[ ! -f "${LOG_FILE}" ]]; then
    printf "Info: Log file does not exist. Creating it now.\n"
    touch "${LOG_FILE}" || {
        printf "Error: Failed to create log file at %s\n" "${LOG_FILE}"
        exit 1
    }
else
    printf "Info: Log file already exists at %s\n" "${LOG_FILE}"
fi

log() {
    printf "%s - %s\n" "$(date '+%Y-%m-%d %H:%M:%S')" "${1}" | tee -a "${LOG_FILE}"
}

log "starting script"

print_asterisks_value=79
readonly print_asterisks_value 
printf "Info: Current print_asterisks_value: %s\n" "${print_asterisks_value}"

# Function to print a line of x asterisks
print_asterisks() {
    local x="${1}"

    # Check if x is within the valid range
    if [[ "${x}" -lt 10 || "${x}" -gt 79 ]]; then
        printf "Error: The value of x (%d) must be between 10 and 79.\n" "${x}"
        exit 1
    fi

    # Print x asterisks
    printf "%0.s*" $(seq 1 "${x}")
    printf "\n"

    # Success case
    return 0
}

DEBUG=1 # Set to 1 to enable debug output, 0 to disable
readonly VERSION

# Print messages based on the value of DEBUG
if [[ "${DEBUG}" -eq 0 ]]; then
    printf "Info: DEBUG is set to 0. Debug output is disabled.\n"
elif [[ "${DEBUG}" -eq 1 ]]; then
    printf "Info: DEBUG is set to 1. Debug output is enabled.\n"
else
    printf "Error: Invalid value for DEBUG: %s. Please set it to 0 or 1.\n" "${DEBUG}"
fi

# Set DESTROY variable
DESTROY=1  # Change this to 1 to destroy the VMs
readonly DESTROY

# Print messages based on the value of DESTROY
if [[ "${DESTROY}" -eq 0 ]]; then
    printf "Info: DESTROY is set to 0. No action will be taken on the VMs.\n"
elif [[ "${DESTROY}" -eq 1 ]]; then
    printf "Warning: DESTROY is set to 1. All VMs will be destroyed!\n"
else
    printf "Error: Invalid value for DESTROY: %s. Please set it to 0 or 1.\n" "${DESTROY}"
fi

print_asterisks "${print_asterisks_value}"

expected_user="cloud"
readonly expected_user
printf "Info: Current expected_user: %s\n" "${expected_user}"
expected_group="staff"
readonly expected_group
printf "Info: Current expected_group: %s\n" "${expected_group}"

max_line_length="${2:-1000}"
printf "Info: Current max_line_length: %s\n" "${max_line_length}"

default_encoding="UTF-8"
readonly default_encoding
printf "Info: Current default_encoding: %s\n" "${default_encoding}"

vagrant_scp_check=0  # Change this to 1 to check the vagrant_scp plugin
readonly vagrant_scp_check
printf "Info: Current vagrant_scp_check: %s\n" "${vagrant_scp_check}"

do_copy_files_to_vms=0
readonly do_copy_files_to_vms
printf "Info: Current do_copy_files_to_vms: %s\n" "${do_copy_files_to_vms}"

do_copy_files_from_directory_to_vms=0
readonly do_copy_files_from_directory_to_vms
printf "Info: Current do_copy_files_from_directory_to_vms: %s\n" "${do_copy_files_from_directory_to_vms}"

do_make_files_executable=0
readonly do_make_files_executable
printf "Info: Current do_make_files_executable: %s\n" "${do_make_files_executable}"

package_manager_choice="apt"
printf "Info: Default package_manager_choice: %s\n" "${package_manager_choice}"

vm_name="debian"
printf "Info: Default vm_name: %s\n" "${vm_name}"

do_update_packages=0
readonly do_update_packages
printf "Info: Current do_update_packages: %s\n" "${do_update_packages}"

do_install_packages=0
readonly do_install_packages
printf "Info: Current do_install_packages: %s\n" "${do_install_packages}"

do_cleanup_packages=0
readonly do_cleanup_packages
printf "Info: Current do_cleanup_packages: %s\n" "${do_cleanup_packages}"

print_asterisks "${print_asterisks_value}"

# Set SSH_CHECK variable
SSH_CHECK=1  # Change this to 1 to SSH into all VMs
readonly SSH_CHECK

# Print messages based on the value of SSH_CHECK
if [[ "${SSH_CHECK}" -eq 0 ]]; then
    printf "Info: SSH_CHECK is set to 0. No SSH actions will be performed on the VMs.\n"
elif [[ "${SSH_CHECK}" -eq 1 ]]; then
    printf "Info: SSH_CHECK is set to 1. SSH will be attempted on all VMs.\n"
else
    printf "Error: Invalid value for SSH_CHECK: %s. Please set it to 0 or 1.\n" "${SSH_CHECK}"
fi

log() {
    printf "%s - %s\n" "$(date '+%Y-%m-%d %H:%M:%S')" "${1}" | tee -a "${LOG_FILE}"
}

# Function to check if a command is available
check_command() {
    if ! command -v "${1}" &>/dev/null; then
        printf "Error: %s is not installed or not in PATH. Please install it and try again.\n" "${1}"
        return 1
    fi

    # Success case
    return 0
}

# Check for required commands
check_command "vagrant" || exit 1
check_command "virtualbox" || exit 1

# Define VMs and their configurations
declare -A vms=(
    ["debian"]="debian/bookworm64"
    ##["debian-automation-controller"]="debian/bookworm64"
    ##["debian-job-controller"]="debian/bookworm64"
    ##["WindowsServer2016"]="jborean93/WindowsServer2016"
    ##["WindowsServer2019"]="jborean93/WindowsServer2019"
    ##["WindowsServer2022"]="jborean93/WindowsServer2022"
    ##["WindowsServer2012R"]="jborean93/WindowsServer2012R2"
    ##["WindowsServer2012"]="jborean93/WindowsServer2012"
    ##["WindowsServer2008R2"]="jborean93/WindowsServer2008R2"
    ##["WindowsServer2008-x64"]="jborean93/WindowsServer2008-x64"
    ##["WindowsServer2008-x86"]="jborean93/WindowsServer2008-x86"

)
#functions_for_package_manager.cfg

#export LANG=en_US.UTF-8
#export LC_ALL=en_US.UTF-8

check_file_exists() {
    local file_path="${1}"
    if [ "${DEBUG}" -eq 1 ]; then
        printf "Info: Current file_path: %s\n" "${file_path}"
        printf "Info: Function check_file_exists %s\n"
    fi

    if [[ ! -f "${file_path}" ]]; then
        printf "Error: File '%s' does not exist.\n" "${file_path}" >&2
        return 1
    fi

    # Success case
    return 0
}

check_file_extension() {
    local file_path="${1}"
    if [ "${DEBUG}" -eq 1 ]; then
        printf "Info: Current file_path: %s\n" "${file_path}"
        printf "Info: Function check_file_extension %s\n"
    fi    
  
    if [[ "${file_path}" != *.cfg ]]; then
        printf "Error: File '%s' does not have a .cfg extension.\n" "${file_path}" >&2
        return 1
    fi
    
    # Success case
    return 0
}

check_file_readable() {
    local file_path="${1}"
    if [ "${DEBUG}" -eq 1 ]; then
        printf "Info: Current file_path: %s\n" "${file_path}"
        printf "Info: Function check_file_readable %s\n"
    fi 
    
    if [[ ! -r "${file_path}" ]]; then
        printf "Error: File '%s' is not readable.\n" "${file_path}" >&2
        return 1
    fi
    
    # Success case
    return 0
}

check_file_ownership() {
    local file_path="${1}"
    if [ "${DEBUG}" -eq 1 ]; then
        printf "Info: Current file_path: %s\n" "${file_path}"
        printf "Info: Function check_file_ownership %s\n"
    fi
    
    local expected_owner=$(whoami)
    local actual_owner=$(stat -c '%U' "${file_path}")
    if [[ "${actual_owner}" != "${expected_owner}" ]]; then
        printf "Error: File '%s' is not owned by %s.\n" "${file_path}" "${expected_owner}" >&2
        return 1
    fi

    # Success case
    return 0
}

check_modification_time() {
    local file_path="${1}"
    if [ "${DEBUG}" -eq 1 ]; then
        printf "Info: Current file_path: %s\n" "${file_path}"
        printf "Info: Function check_modification_time %s\n"
    fi
    
    local modification_time=1
    local file_mtime=$(stat -c %Y "${file_path}")
    local current_time=$(date +%s)
    local time_diff=$((current_time - file_mtime))
    local max_time=$((modification_time * 3600))
    if [[ ${time_diff} -gt ${max_time} ]]; then
        printf "Error: File '%s' was not modified within the last %d hours.\n" "${file_path}" "${modification_time}" >&2
        return 1
    fi

    # Success case
    return 0
}

validate_file_hash() {
    local file_path="${1}"
    if [ "${DEBUG}" -eq 1 ]; then
        printf "Info: Current file_path: %s\n" "${file_path}"
        printf "Info: Function validate_file_hash %s\n"
    fi

    local hash_file="${file_path}.sha3-512"
    printf "Info: Current hash_file: %s\n" "${hash_file}"
    cat "${hash_file}"
    if [[ ! -f "${hash_file}" ]]; then
        printf "Error: Hash file '%s' does not exist.\n" "${hash_file}" >&2
        return 1
    fi
    local expected_hash=$(cat "${hash_file}")
    printf "Info: expected_hash: %s\n" "${expected_hash}"
    local actual_hash=$(sha512sum "${file_path}" | awk '{print $1}')
    printf "Info: actual_hash: %s\n" "${actual_hash}"
    if [[ "${actual_hash}" != "${expected_hash}" ]]; then
        printf "Error: File '%s' hash does not match expected hash.\n" "${file_path}" >&2
        return 1
    fi

    # Success case
    return 0
}

check_for_symlinks() {
    local file_path="${1}"
    if [ "${DEBUG}" -eq 1 ]; then
        printf "Info: Current file_path: %s\n" "${file_path}"
        printf "Info: Function check_for_symlinks %s\n"
    fi

    if [[ -L "${file_path}" ]]; then
        printf "Error: File '%s' is a symlink.\n" "${file_path}" >&2
        return 1
    fi

    # Success case
    return 0
}

validate_file_path() {
    local file_path="${1}"
    if [ "${DEBUG}" -eq 1 ]; then
        printf "Info: Current file_path: %s\n" "${file_path}"
        printf "Info: Function validate_file_path %s\n"
    fi

    if [[ "${file_path}" == *".."* ]]; then
        printf "Error: File path '%s' contains directory traversal.\n" "${file_path}" >&2
        return 1
    fi

    # Success case
    return 0
}

check_setuid_setgid() {
    local file_path="${1}"
    if [ "${DEBUG}" -eq 1 ]; then
        printf "Info: Current file_path: %s\n" "${file_path}"
        printf "Info: Function check_setuid_setgid %s\n"
    fi

    local file_perms=$(stat -c '%a' "${file_path}")
    if [[ "${file_perms:0:1}" == "2" || "${file_perms:0:1}" == "4" ]]; then
        printf "Error: File '%s' has setuid or setgid bit set.\n" "${file_path}" >&2
        return 1
    fi

    # Success case
    return 0
}

check_file_size() {
    local file_path="${1}"
    if [ "${DEBUG}" -eq 1 ]; then
        printf "Info: Current file_path: %s\n" "${file_path}"
        printf "Info: Function check_file_size %s\n"
    fi

    local limit_file_size=30
    local file_size=$(du -k "${file_path}" | cut -f1)
    if [[ ${file_size} -gt ${limit_file_size} ]]; then
        printf "Error: File '%s' exceeds maximum size of %d KB.\n" "${file_path}" "${limit_file_size}" >&2
        return 1
    fi

    # Success case
    return 0
}

validate_file_structure() {
    local file_path="${1}"
    if [ "${DEBUG}" -eq 1 ]; then
        printf "Info: Current file_path: %s\n" "${file_path}"
        printf "Info: Function validate_file_structure %s\n"
    fi

    # This is a placeholder function. You need to implement specific checks
    # for your configuration file format.
    printf "Warning: File structure validation not implemented for '%s'.\n" "${file_path}" >&2

    # Success case
    return 0
}

check_whitelist() {
    local file_path="${1}"
    if [ "${DEBUG}" -eq 1 ]; then
        printf "Info: Current file_path: %s\n" "${file_path}"
        printf "Info: Function check_whitelist %s\n"
    fi

    local allowed_paths=(
        "/${HOMEDIR}/${USERSCRIPT}/repos/initial_laptop_setup/scripts/"
        "/${HOMEDIR}/${USERSCRIPT}/"
    )
    local is_allowed=false
    for allowed_path in "${allowed_paths[@]}"; do
        if [[ "${file_path}" == "${allowed_path}"* ]]; then
            is_allowed=true
            break
        fi
    done
    if [[ "${is_allowed}" != true ]]; then
        printf "Error: File '%s' is not in the allowed paths.\n" "${file_path}" >&2
        return 1
    fi

    # Success case
    return 0
}

secure_compare() {
    local str1="${1}"
    local str2="${2}"
    if [ "${DEBUG}" -eq 1 ]; then
        printf "Info: Current file_path: %s\n" "${file_path}"
        printf "Info: Function secure_compare %s\n"
    fi

    if [[ ${#str1} -ne ${#str2} ]]; then
        return 1
    fi
    local result=0
    for (( i=0; i<${#str1}; i++ )); do
        result=$((result | $(printf '%d' "'${str1:$i:1}") ^ $(printf '%d' "'${str2:$i:1}")))
    done
    return ${result}

    # Success case
    return 0
}

check_file_is_text() {
    local file_path="${1}"
    if [ "${DEBUG}" -eq 1 ]; then
        printf "Info: Current file_path: %s\n" "${file_path}"
        printf "Info: Function check_file_is_text %s\n"
    fi

    if ! file -b --mime-type "${file_path}" | grep -q "^text/"; then
        printf "Error: File '%s' is not a text file.\n" "${file_path}" >&2
        return 1
    fi
    
    # Success case
    return 0
}

check_file_encoding() {
    local file_path="${1}"
    if [ "${DEBUG}" -eq 1 ]; then
        printf "Info: Current file_path: %s\n" "${file_path}"
        printf "Info: Function check_file_encoding %s\n"
    fi

    if ! file -i "${file_path}" | grep -q "charset=utf-8"; then
        printf "Error: File '%s' is not UTF-8 encoded.\n" "${file_path}" >&2
        return 1
    fi
    
    # Success case
    return 0
}

# Function to check the MIME type and encoding of a file with file
check_file_encoding_with_file() {
    local file="${1}"
    if [ "${DEBUG}" -eq 1 ]; then
        printf "Info: Current file_path: %s\n" "${file_path}"
        printf "Info: Function check_file_encoding_with_file %s\n"
    fi

    check_command "file"

    # Get MIME type and encoding
    mime_info=$(file --mime "${file}")
    printf "MIME info for '%s': %s\n" "${file}" "${mime_info}"
    
    # Success case
    return 0
}

check_line_length() {
    local file_path="${1}"
    if [ "${DEBUG}" -eq 1 ]; then
        printf "Info: Current file_path: %s\n" "${file_path}"
        printf "Info: Function check_line_length %s\n"
    fi

    local max_line_length="${2}"
    local long_lines
    long_lines=$(awk -v max="${max_line_length}" 'length > max' "${file_path}")
    if [[ -n "${long_lines}" ]]; then
        printf "Error: File '%s' contains lines longer than %d characters.\n" "${file_path}" "${max_line_length}" >&2
        return 1
    fi
    
    # Success case
    return 0
}

check_ascii_only() {
    local file_path="${1}"
    if [ "${DEBUG}" -eq 1 ]; then
        printf "Info: Current file_path: %s\n" "${file_path}"
        printf "Info: Function check_ascii_only %s\n"
    fi

    if LC_ALL=C grep -q '[^[:ascii:]]' "${file_path}"; then
        printf "Error: File '%s' contains non-ASCII characters.\n" "${file_path}" >&2
        return 1
    fi

    if file -i "${file_path}" | grep -q "charset=utf-8"; then
        printf "Warning: File '%s' may contain Unicode characters.\n" "${file_path}" >&2
        return 1
    fi

    # Success case
    return 0
}

check_unicode() {
    local file_path="${1}"
    if [ "${DEBUG}" -eq 1 ]; then
        printf "Info: Current file_path: %s\n" "${file_path}"
        printf "Info: Function check_unicode %s\n"
    fi

    # Check if the file exists
    if [[ ! -f "${file_path}" ]]; then
        printf "Error: File '%s' does not exist.\n" "${file_path}"
        return 1
    fi

    # Use grep to check for non-ASCII characters
    if grep -q '[^[:print:]]' "${file_path}"; then
        printf "Unicode character detected in file '%s'. Exiting with Unicode character 🄍.\n" "${file_path}"
        exit 1  # Exit with a non-zero status
    else
        printf "File '%s' contains only ASCII characters. Pass.\n" "${file_path}"
    fi
}

check_unwanted_characters() {
    local file_path="${1}"
    if [ "${DEBUG}" -eq 1 ]; then
        printf "Info: Current file_path: %s\n" "${file_path}"
        printf "Info: Function check_unwanted_characters %s\n"
    fi

    local unwanted_chars="[?!€ßäöüÄÖÜ]"
    if grep -q "${unwanted_chars}" "${file_path}"; then
        printf "Error: File '%s' contains unwanted characters.\n" "${file_path}" >&2
        return 1
    fi
    
    # Success case
    return 0
}

source_file() {
    local file_path="${1}"
    if [ "${DEBUG}" -eq 1 ]; then
        printf "Info: Current file_path: %s\n" "${file_path}"
        printf "Info: Function source_file %s\n"
    fi

    local max_line_length="${2:-1000}"

    check_file_exists "${file_path}" || return 1
    check_file_extension "${file_path}" || return 1
    check_file_readable "${file_path}" || return 1
    
    #check_file_ownership "${file_path}" || return 1
    #check_modification_time "${file_path}" || return 1
    validate_file_hash "${file_path}" || return 1
    check_for_symlinks "${file_path}" || return 1
    validate_file_path "${file_path}" || return 1
    check_setuid_setgid "${file_path}" || return 1
    check_file_size "${file_path}" || return 1
    #validate_file_structure "${file_path}" || return 1
    check_whitelist "${file_path}" || return 1
    #secure_compare "${file_path}" || return 1

    check_file_is_text "${file_path}" || return 1
    #check_file_encoding "${file_path}" || return 1
    check_file_encoding_with_file "${file_path}" || return 1
    check_line_length "${file_path}" "${max_line_length}" || return 1
    #check_ascii_only "${file_path}" "${max_line_length}" || return 1
    check_unicode "${file_path}" "${max_line_length}" || return 1
    check_unwanted_characters "${file_path}" "${max_line_length}" || return 1

    (
        set -e
        set -f
        set -o noglob
        set -o nounset
        . "${file_path}"
    ) || {
        printf "Error: Failed to source file '%s'.\n" "${file_path}" >&2
        return 1
    }

    printf "Info: Successfully sourced file '%s'.\n" "${file_path}"
    
    # Success case
    return 0
}

source_file "/${HOMEDIR}/${USERSCRIPT}/repos/initial_laptop_setup/scripts/functions_for_package_manager.cfg" 250

# Function to SSH into a specific VM
vagrant_ssh() {
    local vm_name="${1}"
    
    # Check if the VM name exists in the array
    if [[ -v vms["${vm_name}"] ]]; then
        printf "Info: Attempting to connect to VM: %s\n" "${vm_name}"
        if vagrant ssh "${vm_name}" -c "exit" > /dev/null 2>&1; then
            printf "Info: Successfully connected to VM: %s\n" "${vm_name}"
        else
            printf "Error: Unable to SSH into VM: %s\n" "${vm_name}"
        fi
    else
        printf "Error: VM '%s' not found.\n" "${vm_name}"
        printf "Info: Available VM through ssh: %s\n" "${!vms[@]}"
    fi

    # Success case
    return 0
}

# Function to create VMs
create_vms() {
    if [ "${DESTROY:-0}" -eq 1 ]; then
        printf "Info: DESTROY is set to 1. Destroying existing VMs...\n"

        for vm_name in "${!vms[@]}"; do
            if [ -d "./vagrant/${vm_name}" ]; then
                pushd "./vagrant/${vm_name}" > /dev/null || exit 1
                vagrant destroy -f || printf "Warning: Failed to destroy VM '%s'.\n" "${vm_name}"
                popd > /dev/null || exit 1
            fi
        done

        printf "Info: All existing VMs destroyed.\n"
    else
        printf "Info: DESTROY is not set to 1. Skipping VM destruction.\n"
    fi

    printf "Info: Creating VMs...\n"

    for vm_name in "${!vms[@]}"; do
        local box="${vms[${vm_name}]}"
        printf "Info: Setting up VM '%s' with box '%s'\n" "${vm_name}" "${box}"

        mkdir -p "./vagrant/${vm_name}"

        if [ "${vm_name}" == "sabayon" ]; then
            cat > "./vagrant/${vm_name}/Vagrantfile" <<EOF
Vagrant.configure("2") do |config|
  config.vm.box = "Sabayon/spinbase-amd64"

  config.vm.provision "shell", inline: <<-SHELL
    if command -v hostnamectl >/dev/null 2>&1; then
      hostnamectl set-hostname 'sabayon'
    else
      echo 'sabayon' > /etc/hostname
      hostname 'sabayon'
    fi
  SHELL
end
EOF
        else
            cat > "./vagrant/${vm_name}/Vagrantfile" <<EOF
Vagrant.configure("2") do |config|
  config.vm.box = "${box}"
  config.vm.hostname = "${vm_name}"
end
EOF
        fi

        pushd "./vagrant/${vm_name}" > /dev/null || exit 1
        vagrant up || { printf "Error: Failed to bring up VM '%s'.\n" "${vm_name}"; exit 1; }
        popd > /dev/null || exit 1

        printf "Info: VM '%s' is up and running.\n" "${vm_name}"
    done

    printf "Info: All VMs created successfully.\n"
    
    # Success case
    return 0
}

# Define packages to install
array_packages=(
    "wget"
    "curl"
    "git"
    "git-lfs"
    "ansible"
    "ansible-lint"
)
readonly array_packages
printf "Info: Packages for all VMs:\n"
printf "Info: Package: %s\n" "${array_packages[@]}"

array_packages_automation_controller=(
    "apt-transport-https"
    "ansible"
    "ansible-lint"
    "terraform"
)
readonly array_packages_automation_controller
printf "Info: Packages for Automation:\n"
printf "Info: Package: %s\n" "${array_packages_automation_controller[@]}"

array_packages_job_controller=(
    "apt-transport-https"
    "openjdk"
)
readonly array_packages_job_controller
printf "Info: Packages for Job Controller:\n"
printf "Info: Package: %s\n" "${array_packages_job_controller[@]}"


# Main function
main() {
    create_vms

    for vm_name in "${!vms[@]}"; do
        printf "Info: Processing VM '%s'...\n" "${vm_name}"

        pushd "./vagrant/${vm_name}" > /dev/null || exit 1

    if [ "${vm_name}" == "debian" ]; then
        package_manager_choice="apt"
        vagrant ssh -c "$(declare -f update_packages); update_packages ${package_manager_choice}" || {
            printf "Error: Failed to update/install packages in VM '%s'.\n" "${vm_name}"
            exit 1
        }
    elif [ "${vm_name}" == "debian-automation-controller" ]; then
        # For Debian controller, install specific packages
        vagrant ssh -c "$(declare -f update_packages install_packages); update_packages apt; install_packages apt ${array_packages[*]}; install_packages apt ${array_packages_automation_controller[*]}" || {
            printf "Error: Failed to update/install packages in VM '%s'.\n" "${vm_name}"
            exit 1
        }
    elif [ "${vm_name}" == "debian-job-controller" ]; then
        # For Debian controller, install specific packages
        vagrant ssh -c "$(declare -f update_packages install_packages); update_packages apt; install_packages apt ${array_packages[*]}; install_packages apt ${array_packages_job_controller[*]}" || {
            printf "Error: Failed to update/install packages in VM '%s'.\n" "${vm_name}"
            exit 1
        }
    elif [ "${vm_name}" == "ubuntu" ]; then
        # For Ubuntu, install specific packages
        vagrant ssh -c "$(declare -f update_packages install_packages); update_packages apt; install_packages apt ${array_packages[*]}" || {
            printf "Error: Failed to update/install packages in VM '%s'.\n" "${vm_name}"
            exit 1
        }
    elif [[ "${vm_name}" == "pacman" || "${vm_name}" == "arch" ]]; then
        vagrant ssh -c "$(declare -f update_packages install_packages); update_packages pacman; install_packages pacman ${array_packages[*]}" || {
            printf "Error: Failed to update/install packages in VM '%s'.\n" "${vm_name}"
            exit 1
        }
    elif [[ "${vm_name}" == "zypper" || "${vm_name}" == "opensuse" || "${vm_name}" == "sles11sp4" ]]; then
        vagrant ssh -c "$(declare -f update_packages install_packages); update_packages zypper; install_packages zypper ${array_packages[*]}" || {
            printf "Error: Failed to update/install packages in VM '%s'.\n" "${vm_name}"
            exit 1
        }
    elif [[ "${vm_name}" == "brew" || "${vm_name}" == "macos" ]]; then
        vagrant ssh -c "$(declare -f update_packages install_packages); update_packages brew; install_packages brew ${array_packages[*]}" || {
            printf "Error: Failed to update/install packages in VM '%s'.\n" "${vm_name}"
            exit 1
        }
    elif [[ "${vm_name}" == "dnf" || "${vm_name}" == "almalinux8" || "${vm_name}" == "almalinux7" || "${vm_name}" == "almalinux" ]]; then
        # For AlmaLinux, install specific packages
        vagrant ssh -c "$(declare -f update_packages install_packages); update_packages dnf; install_packages dnf ${array_packages[*]}" || {
            printf "Error: Failed to update/install packages in VM '%s'.\n" "${vm_name}"
            exit 1
        }
    elif [[ "${vm_name}" == "rpm" || "${vm_name}" == "redhat" ]]; then
        vagrant ssh -c "$(declare -f update_packages install_packages); update_packages rpm; install_packages rpm ${array_packages[*]}" || {
            printf "Error: Failed to update/install packages in VM '%s'.\n" "${vm_name}"
            exit 1
        }
    elif [[ "${vm_name}" == "yum" || "${vm_name}" == "redhat2" ]]; then
        vagrant ssh -c "$(declare -f update_packages install_packages); update_packages yum; install_packages yum ${array_packages[*]}" || {
            printf "Error: Failed to update/install packages in VM '%s'.\n" "${vm_name}"
            exit 1
        }
    elif [[ "${vm_name}" == "xbps" || "${vm_name}" == "void" ]]; then
        vagrant ssh -c "$(declare -f update_packages install_packages); update_packages xbps; install_packages xbps ${array_packages[*]}" || {
            printf "Error: Failed to update/install packages in VM '%s'.\n" "${vm_name}"
            exit 1
        }
    elif [[ "${vm_name}" == "pisi" || "${vm_name}" == "solus" ]]; then
        vagrant ssh -c "$(declare -f update_packages install_packages); update_packages pisi; install_packages pisi ${array_packages[*]}" || {
            printf "Error: Failed to update/install packages in VM '%s'.\n" "${vm_name}"
            exit 1
        }
    elif [[ "${vm_name}" == "nix" || "${vm_name}" == "nixos" ]]; then
        vagrant ssh -c "$(declare -f update_packages install_packages); update_packages nix; install_packages nix ${array_packages[*]}" || {
            printf "Error: Failed to update/install packages in VM '%s'.\n" "${vm_name}"
            exit 1
        }
    elif [[ "${vm_name}" == "yay" || "${vm_name}" == "yay2" ]]; then
        vagrant ssh -c "$(declare -f update_packages install_packages); update_packages yay; install_packages yay ${array_packages[*]}" || {
            printf "Error: Failed to update/install packages in VM '%s'.\n" "${vm_name}"
            exit 1
        }
    elif [[ "${vm_name}" == "equo" || "${vm_name}" == "Sabayon" ]]; then
        vagrant ssh -c "$(declare -f update_packages install_packages); update_packages equo; install_packages equo ${array_packages[*]}" || {
            printf "Error: Failed to update/install packages in VM '%s'.\n" "${vm_name}"
            exit 1
        }
    elif [[ "${vm_name}" == "aurman" || "${vm_name}" == "aurman2" ]]; then
        vagrant ssh -c "$(declare -f update_packages install_packages); update_packages aurman; install_packages aurman ${array_packages[*]}" || {
            printf "Error: Failed to update/install packages in VM '%s'.\n" "${vm_name}"
            exit 1
        }
    elif [[ "${vm_name}" == "flatpak" || "${vm_name}" == "flatpak2" ]]; then
        vagrant ssh -c "$(declare -f update_packages install_packages); update_packages flatpak; install_packages flatpak ${array_packages[*]}" || {
            printf "Error: Failed to update/install packages in VM '%s'.\n" "${vm_name}"
            exit 1
        }
    elif [[ "${vm_name}" == "snap" || "${vm_name}" == "snap2" ]]; then
        vagrant ssh -c "$(declare -f update_packages install_packages); update_packages snap; install_packages snap ${array_packages[*]}" || {
            printf "Error: Failed to update/install packages in VM '%s'.\n" "${vm_name}"
            exit 1
        }
    elif [[ "${vm_name}" == "cargo" || "${vm_name}" == "rust" ]]; then
        vagrant ssh -c "$(declare -f update_packages install_packages); update_packages cargo; install_packages cargo ${array_packages[*]}" || {
            printf "Error: Failed to update/install packages in VM '%s'.\n" "${vm_name}"
            exit 1
        }
    elif [[ "${vm_name}" == "portage" || "${vm_name}" == "Gentoo" ]]; then
        vagrant ssh -c "$(declare -f update_packages install_packages); update_packages portage; install_packages port age ${array_packages[*]}" || {
            printf "Error: Failed to update/install packages in VM '%s'.\n" "${vm_name}"
            exit 1
        }
    elif [[ "${vm_name}" == "paludis" || "${vm_name}" == "Gentoo2" ]]; then
        vagrant ssh -c "$(declare -f update_packages install_packages); update_packages paludis; install_packages paludis ${array_packages[*]}" || {
            printf "Error: Failed to update/install packages in VM '%s'.\n" "${vm_name}"
            exit 1
        }
    elif [[ "${vm_name}" == "conary" || "${vm_name}" == "conary" ]]; then
        vagrant ssh -c "$(declare -f update_packages install_packages); update_packages conary; install_packages conary ${array_packages[*]}" || {
            printf "Error: Failed to update/install packages in VM '%s'.\n" "${vm_name}"
            exit 1
        }
    elif [[ "${vm_name}" == "slackpkg" || "${vm_name}" == "Slackware" ]]; then
        vagrant ssh -c "$(declare -f update_packages install_packages); update_packages slackpkg; install_packages slackpkg ${array_packages[*]}" || {
            printf "Error: Failed to update/install packages in VM '%s'.\n" "${vm_name}"
            exit 1
        }
    elif [[ "${vm_name}" == "zeroinstall" || "${vm_name}" == "zeroinstall2" ]]; then
        vagrant ssh -c "$(declare -f update_packages install_packages); update_packages zeroinstall; install_packages zeroinstall ${array_packages[*]}" || {
            printf "Error: Failed to update/install packages in VM '%s'.\n" "${vm_name}"
            exit 1
        }
    else
        # Skip package installation for other VMs
        printf "Info: Skipping package installation for VM '%s'.\n" "${vm_name}"
    fi
        popd > /dev/null || exit 1

        printf "Info: Processing completed for VM '%s'.\n" "${vm_name}"
    done

    printf "Info: All tasks completed successfully.\n"
    
    # Success case
    return 0
}

main "$@"

cleanup

script_name1="$(basename "${0}")"
printf "\nInfo: script_name1: %s\n" "${script_name1}"
script_path1="$(realpath "$(dirname "${0}")")"
printf "Info: script_path1: %s\n" "${script_path1}"
script_path_with_name="${script_path1}/${script_name1}"
printf "Info: Script path with name: %s\n" "${script_path_with_name}"
printf "Info: Script finished\n"
exit 0
