#!/usr/bin/env bash

# Script: enhanced_vm_manager.sh
# Description: Enhanced VM management script with improved error handling, logging, and monitoring
# Author: Pierre Gronau
# Company: ndaal in Cologne
# License: Apache 2.0
# Developed on: Debian 12.x; macOS Sequoia x86 architecture
# Tested on: Debian 12.x; macOS Sequoia x86 architecture

# Strict mode settings
set -o errexit
set -o errtrace
set -o nounset
set -o pipefail
# set -o xtrace

# Set IFS explicitly
# nosemgrep: ifs-tampering
IFS=$'\n\t'

# Error trapping function
# Add this line before the error_handler function
# shellcheck disable=SC2317
error_handler() {
    local line_number="${1}"
    local error_code="${2}"
    local last_command="${BASH_COMMAND}"

    printf "%s[ERROR] Error in line %s (code: %s): %s%s\n" \
        "${log_colors["ERROR"]}" \
        "${line_number}" \
        "${error_code}" \
        "${last_command}" \
        "${COLOR_RESET}"

    cleanup
    exit "${error_code}"
}

# Set error trap
trap 'error_handler ${LINENO} $?' ERR

# Cleanup function
cleanup() {
    trap - SIGINT SIGTERM ERR EXIT
    printf "Info: Cleanup is running...\n"

    # Backup logs
    if [[ -f "${LOG_FILE}" ]]; then
        cp "${LOG_FILE}" "${LOG_FILE}.${TIMESTAMP}.bak"
    fi

    # Remove temporary files
    #find . -name "*.tmp" -type f -delete

    printf "Info: Cleanup finished\n"
}

trap cleanup SIGINT SIGTERM ERR EXIT

# Date and time variables
TIMESTAMP="$(date +"%Y-%m-%d_%H-%M-%S")"
readonly TIMESTAMP
printf "Info: Current timestamp: %s\n" "${TIMESTAMP}"

DIRDATE="$(date +"%Y-%m-%d")"
readonly DIRDATE
printf "Info: Current date: %s\n" "${DIRDATE}"

# Version and script information
VERSION="2.0.3"
readonly VERSION
printf "Info: Script version: %s\n" "${VERSION}"

SCRIPT_NAME="$(basename "${0}")"
readonly SCRIPT_NAME
printf "Info: Script name: %s\n" "${SCRIPT_NAME}"

SCRIPT_DIR="$(dirname "$(readlink -f "${0}")")"
readonly SCRIPT_DIR
printf "Info: Script directory: %s\n" "${SCRIPT_DIR}"

# Base configuration
if [[ "$(uname)" == "Darwin" ]]; then
    HOMEDIR="Users"
elif [[ "$(uname)" == "Linux" ]]; then
    HOMEDIR="home"
else
    printf "Error: Unsupported operating system: %s\n" "$(uname)"
    exit 1
fi
readonly HOMEDIR
printf "Info: Home directory: %s\n" "${HOMEDIR}"

# User configuration
USERSCRIPT="cloud"
readonly USERSCRIPT
printf "Info: User script: %s\n" "${USERSCRIPT}"

LOG_FILE="/${HOMEDIR}/${USERSCRIPT}/vm_manager.log"
readonly LOG_FILE
printf "Info: Log file: %s\n" "${LOG_FILE}"

CONFIG_DIR="/${HOMEDIR}/${USERSCRIPT}/config"
readonly CONFIG_DIR
printf "Info: Config directory: %s\n" "${CONFIG_DIR}"

DEFAULT_CONFIG="${CONFIG_DIR}/default.conf"
readonly DEFAULT_CONFIG
printf "Info: Default config: %s\n" "${DEFAULT_CONFIG}"

expected_user="cloud"
readonly expected_user
printf "Info: Current expected_user: %s\n" "${expected_user}"

expected_group="staff"
readonly expected_group
printf "Info: Current expected_group: %s\n" "${expected_group}"

default_max_line_length=1000
readonly default_max_line_length
printf "Info: Current default_max_line_length: %s\n" "${default_max_line_length}"

default_encoding="UTF-8"
readonly default_encoding
printf "Info: Current default_encoding: %s\n" "${default_encoding}"

vagrant_scp_check=0  # Change this to 1 to check the vagrant_scp plugin
readonly vagrant_scp_check
printf "Info: Current vagrant_scp_check: %s\n" "${vagrant_scp_check}"

do_copy_files_to_vms=0
readonly do_copy_files_to_vms
printf "Info: Current do_copy_files_to_vms: %s\n" "${do_copy_files_to_vms}"

do_copy_files_from_directory_to_vms=0
readonly do_copy_files_from_directory_to_vms
printf "Info: Current do_copy_files_from_directory_to_vms: %s\n" "${do_copy_files_from_directory_to_vms}"

do_make_files_executable=0
readonly do_make_files_executable
printf "Info: Current do_make_files_executable: %s\n" "${do_make_files_executable}"

package_manager_choice="apt"
printf "Info: Default package_manager_choice: %s\n" "${package_manager_choice}"

vm_name="debian"
printf "Info: Default vm_name: %s\n" "${vm_name}"

do_update_packages=1
readonly do_update_packages
printf "Info: Current do_update_packages: %s\n" "${do_update_packages}"

do_install_packages=1
readonly do_install_packages
printf "Info: Current do_install_packages: %s\n" "${do_install_packages}"

do_cleanup_packages=1
readonly do_cleanup_packages
printf "Info: Current do_cleanup_packages: %s\n" "${do_cleanup_packages}"

functions_for_package_manager_file="/${HOMEDIR}/${USERSCRIPT}/repos/initial_laptop_setup/scripts/functions_for_package_manager.cfg"
readonly functions_for_package_manager_file
printf "Info: Current functions_for_package_manager_file: %s\n" "${functions_for_package_manager_file}"

# Define variables
SSH_AllowUsers_Subnet_1="192.168.56.0/24"
SSH_AllowUsers_Subnet_2="10.0.2.0/24"
SSH_AllowUsers_IP_1="192.168.56.10"
SSH_AllowUsers_IP_2="192.168.56.11"
SSH_AllowUsers_IP_3="203.0.113.3"
SSH_AllowUsers_IP_4="203.0.113.4"
SSH_AllowUsers_IP_5="203.0.113.5"
readonly SSH_AllowUsers_Subnet_1
readonly SSH_AllowUsers_Subnet_2
readonly SSH_AllowUsers_IP_1
readonly SSH_AllowUsers_IP_2
readonly SSH_AllowUsers_IP_3
readonly SSH_AllowUsers_IP_4
readonly SSH_AllowUsers_IP_5
printf "Info: Required entry in sshd_config AllowUsers: %s\n" "${SSH_AllowUsers_Subnet_1}"
printf "Info: Required entry in sshd_config AllowUsers: %s\n" "${SSH_AllowUsers_Subnet_2}"
printf "Info: Required entry in sshd_config AllowUsers: %s\n" "${SSH_AllowUsers_IP_1}"
printf "Info: Required entry in sshd_config AllowUsers: %s\n" "${SSH_AllowUsers_IP_2}"
printf "Info: Required entry in sshd_config AllowUsers: %s\n" "${SSH_AllowUsers_IP_3}"
printf "Info: Required entry in sshd_config AllowUsers: %s\n" "${SSH_AllowUsers_IP_4}"
printf "Info: Required entry in sshd_config AllowUsers: %s\n" "${SSH_AllowUsers_IP_5}"

print_asterisks_value=79
readonly print_asterisks_value 
printf "Info: Current print_asterisks_value: %s\n" "${print_asterisks_value}"

# Function to print a line of x asterisks
print_asterisks() {
    local x="${1}"

    # Check if x is within the valid range
    if [[ "${x}" -lt 10 || "${x}" -gt 79 ]]; then
        printf "Error: The value of x (%d) must be between 10 and 79.\n" "${x}"
        exit 1
    fi

    # Print x asterisks
    printf "%0.s*" $(seq 1 "${x}")
    printf "\n"

    # Success case
    return 0
}

print_asterisks "${print_asterisks_value}"

# Set SSH_CHECK variable
SSH_CHECK=1  # Change this to 1 to SSH into all VMs
readonly SSH_CHECK
# Print messages based on the value of SSH_CHECK
if [[ "${SSH_CHECK}" -eq 0 ]]; then
    printf "Info: SSH_CHECK is set to 0. No SSH actions will be performed on the VMs.\n"
elif [[ "${SSH_CHECK}" -eq 1 ]]; then
    printf "Info: SSH_CHECK is set to 1. SSH will be attempted on all VMs.\n"
else
    printf "Error: Invalid value for SSH_CHECK: %s. Please set it to 0 or 1.\n" "${SSH_CHECK}"
fi

# Define a read-only array of required files for vagrant
declare -r required_files_for_vagrant=(
    "create_VMs_with_Vagrant_and_roboxes.sh"
    "create_VMs_with_Vagrant_and_jborean93.sh"
    "create_VMs_with_Vagrant.sh"
)

printf "Info: required files for all VMs:\n"
printf "Info: required file: %s\n" "${required_files_for_vagrant[@]}"

# Define a read-only array of executable files for vagrant
declare -r executable_files_for_vagrant=(
    "create_VMs_with_Vagrant_and_roboxes.sh"
    "create_VMs_with_Vagrant.sh"
)

printf "Info: executable files for all VMs:\n"
printf "Info: executable file: %s\n" "${required_files_for_vagrant[@]}"

check_ssh_connection() {
    local vm_name="${1}"
    local max_retries=3
    local retry_delay=5
    local retry_count=0

    log_info "Checking SSH connection for VM: ${vm_name}"

    while [ "${retry_count}" -lt "${max_retries}" ]; do
        if vagrant ssh "${vm_name}" -c "echo 'SSH connection successful'"; then
            log_info "Successfully connected to ${vm_name} via SSH"
            return 0
        fi

        retry_count=$((retry_count + 1))
        log_warning "SSH connection attempt ${retry_count}/${max_retries} failed for ${vm_name}"
        sleep "${retry_delay}"
    done

    log_error "Failed to establish SSH connection to ${vm_name} after ${max_retries} attempts"
    return 1
}


try_all_package_managers() {
    local vm_name="${1}"

    # Array of all package managers to try
    local package_managers=(
        "apt" "apk" "pacman" "zypper" "brew" "dnf" "rpm" "yum" "xbps"
        "pisi" "nix" "yay" "equo" "aurman" "flatpak" "snap" "cargo"
        "portage" "paludis" "conary" "slackpkg" "zeroinstall" "pip"
        "pkg" "opkg" "pkg_add" "pkgin" "ipkg" "swupd" "guix" "tazpkg"
        "kiss" "npm" "gem" "composer"
    )

    log_info "Attempting package updates with all available package managers on ${vm_name}"

    # Try each package manager
    for pm in "${package_managers[@]}"; do
        log_info "Trying package manager: ${pm}"
        if ! vagrant ssh "${vm_name}" -c "$(declare -f update_packages); update_packages ${pm}"; then
            log_warning "Package manager ${pm} failed or not available on ${vm_name}"
        fi
    done

    # Success case
    return 0
}

# Check if the vagrant-scp plugin is installed
check_install_vagrant_scp() {
    if [ "${vagrant_scp_check}" -eq 1 ]; then
        if vagrant plugin list | grep -q 'vagrant-scp'; then
            vagrant plugin list
            printf "Info: vagrant-scp is already installed.\n"
            if vagrant plugin update vagrant-scp; then
                printf "Info: vagrant-scp is updated.\n"
                vagrant plugin list
            else
                printf "Error: Failed to update vagrant-scp.\n"
                return 1
            fi
        else
            if vagrant plugin install vagrant-scp; then
                vagrant plugin list
                printf "Info: vagrant-scp has been successfully installed.\n"
            else
                printf "Error: Failed to install vagrant-scp. Please check your Vagrant setup.\n"
                return 1
            fi
        fi
    fi

    # Success case
    return 0
}

# Resource Configurations
declare -A vm_resources=(
    ["debian"]="memory=2048 cpus=2 disk=20GB"
    ["debian-automation-controller"]="memory=4096 cpus=4 disk=40GB"
    ["debian-job-controller"]="memory=2048 cpus=2 disk=30GB"
    ["debian-vulnerability-scanner"]="memory=2048 cpus=2 disk=20GB"
    ["Debian-Forschung-1"]="memory=2048 cpus=2 disk=10GB"
    ["Debian-Forschung-2"]="memory=2048 cpus=2 disk=10GB"
    ["Debian-Forschung-3"]="memory=2048 cpus=2 disk=10GB"
    ["Debian-Forschung-4"]="memory=2048 cpus=2 disk=10GB"
    ["Debian-Forschung-5"]="memory=2048 cpus=2 disk=10GB"
    ["Oracle-93"]="memory=2048 cpus=2 disk=15GB"
    ["AlmaLinux95"]="memory=2048 cpus=2 disk=8GB"
    ["AlmaLinux94"]="memory=2048 cpus=2 disk=8GB"
    ["AlmaLinux93"]="memory=2048 cpus=2 disk=8GB"
    ["AlmaLinux92"]="memory=2048 cpus=2 disk=8GB"
    ["AlmaLinux91"]="memory=2048 cpus=2 disk=8GB"
    ["AlmaLinux90"]="memory=2048 cpus=2 disk=8GB"
)
readonly vm_resources
printf "Info: VM resources configured\n"

# Network Configuration
declare -A vm_networks=(
    ["debian"]="ip: '192.168.56.10'"
    ["debian-automation-controller"]="ip: '192.168.56.11'"
    ["debian-job-controller"]="ip: '192.168.56.12'"
    ["debian-vulnerability-scanner"]="ip: '192.168.56.13'"
    ["Debian-Forschung-1"]="ip: '192.168.56.181'"
    ["Debian-Forschung-2"]="ip: '192.168.56.182'"
    ["Debian-Forschung-3"]="ip: '192.168.56.183'"
    ["Debian-Forschung-4"]="ip: '192.168.56.184'"
    ["Debian-Forschung-5"]="ip: '192.168.56.185'"
    ["Oracle-93"]="ip: '192.168.56.33'"
    ["AlmaLinux95"]="ip: '192.168.56.25'"
    ["AlmaLinux94"]="ip: '192.168.56.24'"
    ["AlmaLinux93"]="ip: '192.168.56.23'"
    ["AlmaLinux92"]="ip: '192.168.56.22'"
    ["AlmaLinux91"]="ip: '192.168.56.21'"
    ["AlmaLinux90"]="ip: '192.168.56.20'"
)
readonly vm_networks
printf "Info: VM networks configured\n"

# Package Configurations
declare -A package_versions=(
    ["ansible"]="2.9.*"
    ["terraform"]="1.0.*"
    ["git"]="latest"
    ["nginx"]="1.18.*"
)
readonly package_versions
printf "Info: Package versions configured\n"

# Package groups
declare -A package_groups=(
    ["base"]="wget curl git git-lfs vim"
    ["debian"]="wget curl git git-lfs vim"
    ["automation"]="ansible terraform ansible-lint"
    ["security"]="ufw fail2ban"
    ["monitoring"]="prometheus node_exporter"
)
readonly package_groups
printf "Info: Package groups configured\n"

# VM Configurations
declare -A vms=(
    ["debian"]="debian/bookworm64"
    ["debian-automation-controller"]="debian/bookworm64"
    #["debian-job-controller"]="debian/bookworm64"
    #["debian-vulnerability-scanner"]="debian/bookworm64"
    #["Debian-Forschung-1"]="debian/bookworm64"
    #["Debian-Forschung-2"]="debian/bookworm64"
    #["Debian-Forschung-3"]="debian/bookworm64"
    #["Debian-Forschung-4"]="debian/bookworm64"
    #["Debian-Forschung-5"]="debian/bookworm64"
    #["Oracle-93"]="roboxes-x64/oracle9"
    #["AlmaLinux95"]="almalinux/9"
    #["AlmaLinux94"]="bento/almalinux-9.4"
    #["AlmaLinux93"]="bento/almalinux-9.3"
    #["AlmaLinux92"]="bento/almalinux-9.2"
    #["AlmaLinux91"]="bento/almalinux-9.1"
    #["AlmaLinux90"]="bento/almalinux-9.0"
)
readonly vms
printf "Info: VMs configured\n"

# Logging Configuration
declare -A log_levels=(
    ["ERROR"]=0
    ["WARNING"]=1
    ["INFO"]=2
    ["DEBUG"]=3
)
readonly log_levels

declare -A log_colors=(
    ["ERROR"]="\033[0;31m"    # Red
    ["WARNING"]="\033[0;33m"  # Yellow
    ["INFO"]="\033[0;32m"     # Green
    ["DEBUG"]="\033[0;34m"    # Blue
)
readonly log_colors

COLOR_RESET="\033[0m"
readonly COLOR_RESET

# Default settings
CURRENT_LOG_LEVEL="${CURRENT_LOG_LEVEL:-INFO}"
DEBUG="${DEBUG:-0}"
DESTROY="${DESTROY:-0}"
REQUIRED_SPACE_MB="${REQUIRED_SPACE_MB:-5000}"
#readonly CURRENT_LOG_LEVEL DEBUG DESTROY REQUIRED_SPACE_MB

REQUIRED_SPACE_KB="$((REQUIRED_SPACE_MB * 1024))"
readonly REQUIRED_SPACE_KB
printf "Info: Required space (KB): %s\n" "${REQUIRED_SPACE_KB}"

# Print separator function
print_separator() {
    printf "%0.s-" {1..80}
    printf "\n"
}


# Logging functions
log_message() {
    local level="${1}"
    local message="${2}"
    local timestamp
    timestamp="$(date "+%Y-%m-%d %H:%M:%S")"

    if [ "${log_levels[${level}]}" -le "${log_levels[${CURRENT_LOG_LEVEL}]}" ]; then
        printf "%s[%s] [%s] %s%s\n" \
            "${log_colors[${level}]}" \
            "${timestamp}" \
            "${level}" \
            "${message}" \
            "${COLOR_RESET}" | tee -a "${LOG_FILE}"

        if [ "${level}" = "ERROR" ] && command -v logger >/dev/null 2>&1; then
            logger -p user.err -t "${SCRIPT_NAME}" "${message}"
        fi
    fi
}

log_error() { log_message "ERROR" "${1}"; }
log_warning() { log_message "WARNING" "${1}"; }
log_info() { log_message "INFO" "${1}"; }
log_debug() { log_message "DEBUG" "${1}"; }

DNS_SERVER_1="2001:4860:4860::8844"
readonly DNS_SERVER_1
printf "Info: Required DNS_SERVER_1: %s\n" "${DNS_SERVER_1}"

DNS_SERVER_2="2606:4700:4700::1111"
readonly DNS_SERVER_2
printf "Info: Required DNS_SERVER_2: %s\n" "${DNS_SERVER_2}"

DNS_SERVER_3="2620:fe::9"
readonly DNS_SERVER_3
printf "Info: Required DNS_SERVER_3: %s\n" "${DNS_SERVER_3}"

DNS_SERVER_4="8.8.8.8"
readonly DNS_SERVER_4
printf "Info: Required DNS_SERVER_1: %s\n" "${DNS_SERVER_4}"

DNS_SERVER_5="1.1.1.1"
readonly DNS_SERVER_5
printf "Info: Required DNS_SERVER_2: %s\n" "${DNS_SERVER_5}"

DNS_SERVER_6="9.9.9.9"
readonly DNS_SERVER_6
printf "Info: Required DNS_SERVER_3: %s\n" "${DNS_SERVER_6}"

log_info "Required DNS_SERVER_1: ${DNS_SERVER_1}"
log_info "Required DNS_SERVER_2: ${DNS_SERVER_2}"
log_info "Required DNS_SERVER_3: ${DNS_SERVER_3}"
log_info "Required DNS_SERVER_3: ${DNS_SERVER_4}"
log_info "Required DNS_SERVER_3: ${DNS_SERVER_5}"
log_info "Required DNS_SERVER_3: ${DNS_SERVER_6}"

log_info "Required entry in sshd_config AllowUsers: ${SSH_AllowUsers_Subnet_1}"
log_info "Required entry in sshd_config AllowUsers: ${SSH_AllowUsers_Subnet_2}"
log_info "Required entry in sshd_config AllowUsers: ${SSH_AllowUsers_IP_1}"
log_info "Required entry in sshd_config AllowUsers: ${SSH_AllowUsers_IP_2}"
log_info "Required entry in sshd_config AllowUsers: ${SSH_AllowUsers_IP_3}"
log_info "Required entry in sshd_config AllowUsers: ${SSH_AllowUsers_IP_4}"
log_info "Required entry in sshd_config AllowUsers: ${SSH_AllowUsers_IP_5}"

log_info "Current functions_for_package_manager_file: ${functions_for_package_manager_file}"

# Utility functions
check_command() {
    local cmd="${1}"

    if ! command -v "${cmd}" &>/dev/null; then
        log_error "${cmd} is not installed or not in PATH"
        return 1
    fi
    log_info "${cmd} is installed"

    # Success case
    return 0
}

create_directory() {
    local dir="${1}"
    printf "Info: Directory will be %s\n" "${dir}"
    if [[ ! -d ${dir} ]]; then
        mkdir -p -v "${dir}"
        printf "Info: Directory %s is created.\n" "${dir}"
        printf "Info: placeholder.txt will be created in %s\n" "${dir}"
        touch "${dir}/placeholder.txt"
        printf "Info: placeholder.txt will be removed from %s\n" "${dir}"
        rm -f -v "${dir}/placeholder.txt"
        printf "Info: placeholder.txt is removed from %s\n" "${dir}"
    else
        printf "Info: Directory %s already exists. No creation needed.\n" "${dir}"
    fi

    # Success case
    return 0
}

check_disk_space() {
    local available_space
    available_space="$(df -k . | awk 'NR==2 {print $4}')"

    log_info "Checking disk space. Required: ${REQUIRED_SPACE_MB}MB, Available: $((available_space / 1024))MB"

    if [ "${available_space}" -lt "${REQUIRED_SPACE_KB}" ]; then
        log_error "Insufficient disk space"
        return 1
    fi

    if [ "${available_space}" -lt "$((REQUIRED_SPACE_KB * 2))" ]; then
        log_warning "Available disk space is less than 2x the required space"
    fi

    # Success case
    return 0
}

# VM Management functions
create_vm() {
    local vm_name="${1}"
    local box="${vms[${vm_name}]}"
    local resources="${vm_resources[${vm_name}]:-memory=1024 cpus=1 disk=10GB}"
    local network_config="${vm_networks[${vm_name}]:-private_network ip=192.168.56.100}"

    vagrant global-status --prune

    log_info "Creating VM '${vm_name}'"
    log_info "Box: ${box}"
    log_info "Resources: ${resources}"
    log_info "Network: ${network_config}"

    mkdir -p -v "./vagrant/${vm_name}"

    # Extract resource values
    local memory
    memory="$(printf "%s" "${resources}" | grep -o 'memory=[0-9]*' | cut -d= -f2)"
    local cpus
    cpus="$(printf "%s" "${resources}" | grep -o 'cpus=[0-9]*' | cut -d= -f2)"

    # Create Vagrantfile
cat > "./vagrant/${vm_name}/Vagrantfile" <<EOF
Vagrant.configure("2") do |config|
    config.vm.box = "${box}"
    config.vm.hostname = "${vm_name}"

    # Fixed network configuration
    # Network configuration
    config.vm.network "private_network", ${network_config}

    config.vm.provider "virtualbox" do |vb|
        vb.memory = ${memory}
        vb.cpus = ${cpus}
        vb.customize ["modifyvm", :id, "--groups", "/VagrantVMs"]
        vb.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
        vb.customize ["modifyvm", :id, "--natdnsproxy1", "on"]
    end

    config.vm.synced_folder ".", "/vagrant",
        owner: "vagrant",
        group: "vagrant",
        mount_options: ["dmode=775,fmode=664"]

    config.vm.provision "shell", inline: <<-SHELL
        hostnamectl set-hostname "${vm_name}"

        printf 'Adding DNS servers...'

        sudo rm -f -v "/etc/resolv.conf"
        printf \"nameserver %s\n\" \"${DNS_SERVER_1}" | sudo tee -a "/etc/resolv.conf"
        printf \"nameserver %s\n\" \"${DNS_SERVER_2}" | sudo tee -a "/etc/resolv.conf"
        printf \"nameserver %s\n\" \"${DNS_SERVER_3}" | sudo tee -a "/etc/resolv.conf"
        printf \"nameserver %s\n\" \"${DNS_SERVER_4}" | sudo tee -a "/etc/resolv.conf"

        printf 'Basic security hardening%s\n'
        #sed -i 's/PermitRootLogin yes/PermitRootLogin no/' '/etc/ssh/sshd_config'
        #sed -i 's/#PasswordAuthentication yes/PasswordAuthentication no/' '/etc/ssh/sshd_config'
        #sed -i 's/#StrictModes yes/StrictModes yes/' '/etc/ssh/sshd_config'
        sed -i 's/#MaxAuthTries 6/MaxAuthTries 4/' '/etc/ssh/sshd_config'
        #sed -i 's/#Compression delayed/Compression no/' '/etc/ssh/sshd_config'
        # Create the sshd_config entry
        # sudo printf "AllowUsers *@%s *@%s *@%s *@%s *@%s *@%s *@%s\n" "${SSH_AllowUsers_Subnet_1}" "${SSH_AllowUsers_Subnet_2}" "${SSH_AllowUsers_IP_1}" "${SSH_AllowUsers_IP_2}" "${SSH_AllowUsers_IP_3}" "${SSH_AllowUsers_IP_4}" "${SSH_AllowUsers_IP_5}" >> '/etc/ssh/sshd_config'
        #echo "AllowUsers *@192.168.56.0/24" >> "/etc/ssh/sshd_config"

        systemctl restart sshd

        if [ "${do_update_packages}" = "1" ]; then
           # Update package lists
           if command -v apt-get >/dev/null 2>&1; then
              sudo apt-get update
           elif command -v yum >/dev/null 2>&1; then
              sudo yum update -y
           elif command -v dnf >/dev/null 2>&1; then
              sudo dnf upgrade --refresh -y
           elif command -v rpm >/dev/null 2>&1; then
              sudo yum update -y
           fi
        fi
        if command -v apt-get >/dev/null 2>&1; then
              sudo apt-get install -y apt-transport-https
              sudo apt list --installed | grep apt-transport-https
              sudo apt-get update
           elif command -v yum >/dev/null 2>&1; then
              sudo yum update -y
           elif command -v dnf >/dev/null 2>&1; then
              sudo dnf upgrade --refresh -y
           elif command -v rpm >/dev/null 2>&1; then
              sudo yum update -y
        fi
        if command -v apt-get >/dev/null 2>&1; then
              sudo apt-get install -y apt-transport-https
              sudo apt list --installed | grep apt-transport-https
              sudo apt-get install -y net-tools
              sudo apt-get install -y git
              sudo apt-get install -y fail2ban
              sudo apt-get install -y tcpd
              sudo apt list --installed | grep tcpd
              #sudo apt-get install -y ntp
              #sudo apt-get install -y tripwire
              sudo sudo apt install -y auditd audispd-plugins
              sudo apt list --installed | grep auditd
              sudo apt-get install -y syslog-ng
              sudo apt list --installed | grep syslog-ng
           elif command -v yum >/dev/null 2>&1; then
              sudo yum install -y net-tools
              sudo yum install -y git
           elif command -v dnf >/dev/null 2>&1; then
              sudo dnf install -y net-tools
              sudo dnf install -y git
           elif command -v rpm >/dev/null 2>&1; then
              rpm -ivh --quiet net-tools
              rpm -ivh --quiet git
        fi

        printf "preparing hardening according to CIS"
        
        git clone https://github.com/ovh/debian-cis.git && cd debian-cis
        sudo cp -f -p -v /home/vagrant/debian-cis/debian/default "/etc/default/cis-hardening"
        sudo ls -la "/etc/default/cis-hardening"
        sudo sed -i "s#CIS_LIB_DIR=.*#CIS_LIB_DIR='\$(pwd)'/lib#" "/etc/default/cis-hardening"
        sudo sed -i "s#CIS_CHECKS_DIR=.*#CIS_CHECKS_DIR='\$(pwd)'/bin/hardening#" "/etc/default/cis-hardening"
        sudo sed -i "s#CIS_CONF_DIR=.*#CIS_CONF_DIR='\$(pwd)'/etc#" "/etc/default/cis-hardening"
        sudo sed -i "s#CIS_TMP_DIR=.*#CIS_TMP_DIR='\$(pwd)'/tmp#" "/etc/default/cis-hardening"
        sudo ls -la "/home/vagrant/debian-cis/bin/hardening.sh"
        sudo chmod +x "/home/vagrant/debian-cis/bin/hardening.sh"
        sudo rm -f -v "/home/vagrant/debian-cis/bin/hardening/3.1.1_disable_ipv6.sh"
        #sudo /home/vagrant/debian-cis/bin/hardening.sh --allow-unsupported-distribution --audit-all || true
        #sudo /home/vagrant/debian-cis/bin/hardening.sh --allow-unsupported-distribution --apply || true
        #sudo /home/vagrant/debian-cis/bin/hardening.sh --allow-unsupported-distribution --audit-all-enable-passed || true
        #exit 0
        #sudo /home/vagrant/debian-cis/bin/hardening.sh --allow-unsupported-distribution --audit-all || true

    SHELL
end
EOF

    pushd "./vagrant/${vm_name}" > /dev/null || return 1
    if ! vagrant up; then
        log_error "Failed to create VM '${vm_name}'"
        return 1
    fi
    popd > /dev/null || return 1

    verify_vm_status "${vm_name}"
}

verify_vm_status() {
    local vm_name="${1}"
    local max_retries=5
    local current_retry=0
    local retry_delay=5

    vagrant global-status --prune

    # Check if VM directory exists
    if [ ! -d "./vagrant/${vm_name}" ]; then
        log_error "VM directory for ${vm_name} not found"
        return 1
    fi

    # Change to VM directory
    pushd "./vagrant/${vm_name}" > /dev/null || {
        log_error "Failed to change to VM directory for ${vm_name}"
        return 1
    }

    while [ "${current_retry}" -lt "${max_retries}" ]; do
        if vagrant global-status --prune "default" | grep -q "running"; then
            log_info "VM '${vm_name}' is running properly"
            popd > /dev/null
            return 0
        fi

        current_retry=$((current_retry + 1))
        log_info "Waiting for VM '${vm_name}' to start (attempt ${current_retry}/${max_retries})"
        sleep "${retry_delay}"
    done

    log_error "Failed to verify VM '${vm_name}' status after ${max_retries} attempts"
    popd > /dev/null
    return 1
}

# Package Management functions
install_packages() {
    local vm_name="${1}"
    local package_manager="${2}"
    shift 2
    local packages=("$@")
    local failed_installs=()

    log_info "Installing packages on VM '${vm_name}' using ${package_manager}"
    log_info "Packages to install: ${packages[*]}"

    case "${package_manager}" in
        apt)
            if ! vagrant ssh "${vm_name}" -c "sudo apt-get update"; then
                log_error "Failed to update package lists on ${vm_name}"
                return 1
            fi

            for pkg in "${packages[@]}"; do
                local version="${package_versions[${pkg}]:-latest}"
                local install_cmd

                if [[ "${version}" == "latest" ]]; then
                    install_cmd="sudo apt-get install -y ${pkg}"
                else
                    install_cmd="sudo apt-get install -y ${pkg}=${version}"
                fi

                if ! vagrant ssh "${vm_name}" -c "${install_cmd}"; then
                    failed_installs+=("${pkg}")
                    log_warning "Failed to install ${pkg} on ${vm_name}"
                fi
            done
            ;;

        yum|dnf)
            if ! vagrant ssh "${vm_name}" -c "sudo ${package_manager} check-update"; then
                log_error "Failed to update package lists on ${vm_name}"
                return 1
            fi

            for pkg in "${packages[@]}"; do
                local version="${package_versions[${pkg}]:-latest}"
                local install_cmd

                if [[ "${version}" == "latest" ]]; then
                    install_cmd="sudo ${package_manager} install -y ${pkg}"
                else
                    install_cmd="sudo ${package_manager} install -y ${pkg}-${version}"
                fi

                if ! vagrant ssh "${vm_name}" -c "${install_cmd}"; then
                    failed_installs+=("${pkg}")
                    log_warning "Failed to install ${pkg} on ${vm_name}"
                fi
            done
            ;;

        *)
            log_error "Unsupported package manager: ${package_manager}"
            return 1
            ;;
    esac

    if [ "${#failed_installs[@]}" -gt 0 ]; then
        log_error "Failed to install packages: ${failed_installs[*]}"
        return 1
    fi

    log_info "Successfully installed all packages on ${vm_name}"

    # Success case
    return 0
}


configure_dns() {
    local vm_name="${1}"
    local dns_list="${DNS_SERVER_1} ${DNS_SERVER_2} ${DNS_SERVER_3} ${DNS_SERVER_4} ${DNS_SERVER_5} ${DNS_SERVER_6}"

    log_info "Configuring DNS settings for VM: ${vm_name}"

    # Check if VM directory exists
    if [ ! -d "./vagrant/${vm_name}" ]; then
        log_error "VM directory for ${vm_name} not found"
        return 1
    fi

    # Change to VM directory
    pushd "./vagrant/${vm_name}" > /dev/null || {
        log_error "Failed to change to VM directory for ${vm_name}"
        return 1
    }

    # Check if Vagrantfile exists
    if [ ! -f "Vagrantfile" ]; then
        log_error "Vagrantfile not found for ${vm_name}"
        popd > /dev/null
        return 1
    fi

    # Execute SSH commands
    vagrant ssh "default" -c "
        # Backup original resolv.conf
        sudo cp /etc/resolv.conf \"/etc/resolv.conf.backup.${TIMESTAMP}\"

        # Clear and recreate resolv.conf
        sudo tee /etc/resolv.conf > /dev/null <<EOL
# Generated by VM provisioning script
# Date: \$(date)
EOL

        # Add DNS servers
        printf 'Adding DNS servers...\n'
        for server in ${dns_list}; do
            printf 'nameserver %s\n' \"\${server}\" | sudo tee -a /etc/resolv.conf
        done

        # Add search domain if needed
        printf 'domain localdomain\n' | sudo tee -a /etc/resolv.conf
        printf 'search localdomain\n' | sudo tee -a /etc/resolv.conf

        # Verify DNS configuration
        printf 'Current DNS configuration:\n'
        cat /etc/resolv.conf

        # Test DNS resolution
        printf 'Testing DNS resolution...\n'
        for domain in google.com cloudflare.com quad9.net; do
            if nslookup \"\${domain}\" >/dev/null 2>&1; then
                printf \"Successfully resolved %s\n\" \"\${domain}\"
            else
                printf \"Failed to resolve %s\n\" \"\${domain}\"
            fi
        done
    " || {
        log_error "Failed to configure DNS for ${vm_name}"
        popd > /dev/null
        return 1
    }

    # Return to original directory
    popd > /dev/null

    # Success case
    return 0
}

configure_systemd_resolved() {
    local vm_name="${1}"
    log_info "Configuring systemd-resolved for VM: ${vm_name}"
    vagrant ssh "${vm_name}" -c "
        if command -v systemctl >/dev/null 2>&1; then
            if systemctl is-active systemd-resolved >/dev/null 2>&1; then
                # Configure systemd-resolved
                sudo tee /etc/systemd/resolved.conf > /dev/null <<EOL
[Resolve]
DNS=${DNS_SERVERS[*]}
FallbackDNS=
DNSSEC=no
DNSOverTLS=no
Cache=yes
DNSStubListener=no
EOL
                # Restart systemd-resolved
                sudo systemctl restart systemd-resolved
                printf 'systemd-resolved configuration updated\n'
            else
                printf 'systemd-resolved is not active\n'
            fi
        else
            printf 'systemctl not found, skipping systemd-resolved configuration\n'
        fi
    "
}

configure_test() {
    local vm_name="${1}"

    log_info "Configuring Test settings for VM: ${vm_name}"

    # Check if VM directory exists
    if [ ! -d "./vagrant/${vm_name}" ]; then
        log_error "VM directory for ${vm_name} not found"
        return 1
    fi

    # Change to VM directory
    pushd "./vagrant/${vm_name}" > /dev/null || {
        log_error "Failed to change to VM directory for ${vm_name}"
        return 1
    }

    # Check if Vagrantfile exists
    if [ ! -f "Vagrantfile" ]; then
        log_error "Vagrantfile not found for ${vm_name}"
        popd > /dev/null
        return 1
    fi

    #apt-get install -y "wget" "git" "git-lfs" "curl" 

    #if ! install_packages "${vm_name}" "apt" "${package_groups["automation"]}"; then
    #                log_error "Failed to install automation packages on ${vm_name}"
    #                failed_vms=$((failed_vms + 1))
    #fi 

    # Execute SSH commands
    vagrant ssh "default" -c "
        
        sudo apt-get update
        sudo apt-get update -y
        sudo apt-get install -y "wget" 
        # "git" "git-lfs" "curl"

    " || {
        log_error "Failed to configure DNS for ${vm_name}"
        popd > /dev/null
        return 1
    }

    # Return to original directory
    popd > /dev/null

    # Success case
    return 0
}

# Security configuration function
configure_security() {
    local vm_name="${1}"
    local firewall_ports=("22" "80" "443")

    log_info "Configuring security for VM: ${vm_name}"

    if ! vagrant ssh "${vm_name}" -c "
        # Enable and configure firewall
        sudo ufw --force enable
        sudo ufw default deny incoming
        sudo ufw default allow outgoing

        # Configure allowed ports
        for port in ${firewall_ports[*]}; do
            sudo ufw allow \${port}/tcp
        done

        # Configure fail2ban if available
        if command -v fail2ban-client >/dev/null 2>&1; then
            sudo systemctl enable fail2ban
            sudo systemctl start fail2ban
        fi

        printf 'Secure SSH configuration ... '
        sed -i 's/PermitRootLogin yes/PermitRootLogin no/' '/etc/ssh/sshd_config'
        sed -i 's/#PasswordAuthentication yes/PasswordAuthentication no/' '/etc/ssh/sshd_config'
        sed -i 's/#StrictModes yes/StrictModes yes/' '/etc/ssh/sshd_config'
        sed -i 's/#MaxAuthTries 6/MaxAuthTries 2/' '/etc/ssh/sshd_config'
        sed -i 's/#Compression delayed/Compression no/' '/etc/ssh/sshd_config'

        sudo systemctl restart sshd

        # Set secure file permissions
        sudo chmod 600 /etc/ssh/ssh_host_*_key
        sudo chmod 644 /etc/ssh/ssh_host_*_key.pub

        # Enable automatic security updates if available
        if command -v unattended-upgrades >/dev/null 2>&1; then
            sudo dpkg-reconfigure -plow unattended-upgrades
        fi
    "; then
        log_error "Failed to configure security for ${vm_name}"
        return 1
    fi

    log_info "Security configuration completed for ${vm_name}"

    # Success case
    return 0
}

# System monitoring functions
monitor_vm_resources() {
    local vm_name="${1}"
    local threshold_cpu=80
    local threshold_mem=80
    local threshold_disk=80

    log_info "Monitoring resources for VM: ${vm_name}"

    vagrant ssh "${vm_name}" -c "
        # CPU Usage
        cpu_usage=\$(top -bn1 | grep 'Cpu(s)' | awk '{print \$2}' | cut -d. -f1)
        if [ \${cpu_usage} -gt ${threshold_cpu} ]; then
            printf \"WARNING: High CPU usage (%s%%)\n\" \"\${cpu_usage}\"
        fi

        # Memory Usage
        mem_usage=\$(free | grep Mem | awk '{print \$3/\$2 * 100}' | cut -d. -f1)
        if [ \${mem_usage} -gt ${threshold_mem} ]; then
            printf \"WARNING: High memory usage (%s%%)\n\" \"\${mem_usage}\"
        fi

        # Disk Usage
        disk_usage=\$(df -h / | awk 'NR==2 {print \$5}' | cut -d% -f1)
        if [ \${disk_usage} -gt ${threshold_disk} ]; then
            printf \"WARNING: High disk usage (%s%%)\n\" \"\${disk_usage}\"
        fi
    "
}

collect_system_metrics() {
    local vm_name="${1}"
    local metrics_dir="/tmp/vm_metrics"
    local metrics_file="${metrics_dir}/${vm_name}_${TIMESTAMP}.log"

    mkdir -p "${metrics_dir}"

    log_info "Collecting system metrics for VM: ${vm_name}"

    if ! vagrant ssh "${vm_name}" -c "
        {
            printf \"=== System Information (%s) ===\n\" \"\$(date)\"
            printf \"Hostname: %s\n\" \"\$(hostname)\"
            printf \"Uptime: %s\n\" \"\$(uptime)\"
            printf \"\n\"

            printf \"=== CPU Usage ===\n\"
            top -bn1 | head -n 5
            printf \"\n\"

            printf \"=== Memory Usage ===\n\"
            free -h
            printf \"\n\"

            printf \"=== Disk Usage ===\n\"
            df -h
            printf \"\n\"

            printf \"=== Network Connections ===\n\"
            netstat -tulpn
            printf \"\n\"

            printf \"=== Process Count ===\n\"
            ps aux | wc -l
            printf \"\n\"

            printf \"=== System Load ===\n\"
            cat /proc/loadavg
            printf \"\n\"

            printf \"=== Memory Information ===\n\"
            cat /proc/meminfo
            printf \"\n\"

            if command -v iostat >/dev/null 2>&1; then
                printf \"=== Disk I/O Statistics ===\n\"
                iostat -x 1 1
                printf \"\n\"
            fi

            printf \"=== Network Statistics ===\n\"
            netstat -s
            printf \"\n\"

        } > \"${metrics_file}\"
    "; then
        log_error "Failed to collect system metrics for ${vm_name}"
        return 1
    fi

    log_info "System metrics collected and saved to ${metrics_file}"

    # Success case
    return 0
}

# Log rotation function
rotate_logs() {
    local max_size_mb=10
    local max_files=5
    local max_size=$((max_size_mb * 1024 * 1024))  # Convert to bytes

    if [ -f "${LOG_FILE}" ]; then
        local current_size
        current_size="$(stat -f%z "${LOG_FILE}" 2>/dev/null || stat -c%s "${LOG_FILE}")"

        if [ "${current_size}" -gt "${max_size}" ]; then
            # Rotate existing log files
            for i in $(seq $((max_files - 1)) -1 1); do
                if [ -f "${LOG_FILE}.${i}" ]; then
                    mv "${LOG_FILE}.${i}" "${LOG_FILE}.$((i + 1))"
                fi
            done

            # Archive current log file
            mv "${LOG_FILE}" "${LOG_FILE}.1"
            touch "${LOG_FILE}"

            log_info "Log file rotated (size: ${current_size} bytes)"
        fi
    fi
}

# Help function
show_help() {
    printf "Usage: %s [OPTIONS]\n\n" "${0}"
    printf "Options:\n"
    printf "    -h, --help              Show this help message\n"
    printf "    -d, --destroy           Destroy existing VMs before creation\n"
    printf "    -v, --verbose           Enable verbose output\n"
    printf "    -l, --log-level LEVEL   Set log level (ERROR|WARNING|INFO|DEBUG)\n"
    printf "    -c, --config FILE       Use specific configuration file\n"
    printf "    --version               Show version information\n\n"
    printf "Examples:\n"
    printf "    %s --destroy           Create new VMs, destroying existing ones\n" "${0}"
    printf "    %s --verbose           Run with verbose output\n" "${0}"
    printf "    %s --log-level DEBUG   Run with debug logging enabled\n\n" "${0}"
    printf "For more information, please refer to the documentation.\n"
}

# Version information function
show_version() {
    printf "VM Management Script v%s\n" "${VERSION}"
    printf "Copyright (c) %s\n" "$(date +%Y)"
    printf "Licensed under the Apache License, Version 2.0\n"
}

# Parse command line arguments
parse_arguments() {
    while [[ $# -gt 0 ]]; do
        case "${1}" in
            -h|--help)
                show_help
                exit 0
                ;;
            -d|--destroy)
                DESTROY=1
                shift
                ;;
            -v|--verbose)
                DEBUG=1
                CURRENT_LOG_LEVEL="DEBUG"
                shift
                ;;
            -l|--log-level)
                if [[ -n "${2}" && "${log_levels[${2}]+exists}" ]]; then
                    CURRENT_LOG_LEVEL="${2}"
                    shift 2
                else
                    log_error "Invalid log level: ${2}"
                    exit 1
                fi
                ;;
            -c|--config)
                if [[ -f "${2}" ]]; then
                    source "${2}"
                    shift 2
                else
                    log_error "Configuration file not found: ${2}"
                    exit 1
                fi
                ;;
            --version)
                show_version
                exit 0
                ;;
            *)
                log_error "Unknown option: ${1}"
                show_help
                exit 1
                ;;
        esac
    done
}

# Initialize script
initialize() {
    local init_timestamp
    init_timestamp="$(date "+%Y-%m-%d %H:%M:%S")"

    # Create necessary directories
    DIRECTORY="$(dirname "${LOG_FILE}")"
    printf "Info: %s\n" "${DIRECTORY}"

    create_directory "${DIRECTORY}"

    DIRECTORY="${CONFIG_DIR}"
    printf "Info: %s\n" "${DIRECTORY}"

    create_directory "${DIRECTORY}"

    # Declare a readonly source for the vagrant files
    VAGRANT_SOURCE_DIR="/${HOMEDIR}/${USERSCRIPT}/"
    # VAGRANT_SOURCE_DIR="/path/to/vagrant/source"
    readonly VAGRANT_SOURCE_DIR
    printf "Info: Current first VAGRANT_SOURCE_DIR: %s\n" "${VAGRANT_SOURCE_DIR}"

    DIRECTORY="${VAGRANT_SOURCE_DIR}"
    printf "Info: %s\n" "${DIRECTORY}"

    create_directory "${DIRECTORY}"

    # Check if the files exist in the VAGRANT_SOURCE_DIR
    for file in "${required_files_for_vagrant[@]}"; do
        full_path="${VAGRANT_SOURCE_DIR}${file}"
        
        if [ -e "${full_path}" ]; then
            printf "Info: File exists: %s\n" "${full_path}"
            find "${full_path}" -maxdepth 1 -type f -name "*.sh" -print0 | xargs -0 ls -la --
        else
            printf "Error: File does not exist: %s\n" "${full_path}"
        fi
    done

    print_asterisks "${print_asterisks_value}"

    # Check if the files exist in the VAGRANT_SOURCE_DIR
    for file in "${executable_files_for_vagrant[@]}"; do
        full_path="${VAGRANT_SOURCE_DIR}${file}"
        
        if [ -e "${full_path}" ]; then
            printf "Info: File exists: %s\n" "${full_path}"
            find "${full_path}" -maxdepth 1 -type f -name "*.sh" -print0 | xargs -0 ls -la --
        else
            printf "Error: File does not exist: %s\n" "${full_path}"
        fi
    done

    print_asterisks "${print_asterisks_value}"

    ls -la "${DIRECTORY}"

    print_asterisks "${print_asterisks_value}"

    # Declare a second readonly source for the vagrant files
    VAGRANT_SOURCE_DIR_2="/${HOMEDIR}/${USERSCRIPT}/repos/initial_laptop_setup/scripts/"
    # VAGRANT_SOURCE_DIR_2="/path/to/vagrant/source"
    readonly VAGRANT_SOURCE_DIR_2
    printf "Info: Current second VAGRANT_SOURCE_DIR_: %s\n" "${VAGRANT_SOURCE_DIR_2}"

    DIRECTORY="${VAGRANT_SOURCE_DIR_2}"
    printf "Info: %s\n" "${DIRECTORY}"

    create_directory "${DIRECTORY}"

    print_asterisks "${print_asterisks_value}"

    ls -la "${DIRECTORY}"

    print_asterisks "${print_asterisks_value}"

    # Initialize logging
    if [[ ! -f "${LOG_FILE}" ]]; then
        touch "${LOG_FILE}"
        printf "Log file created at %s\n" "${init_timestamp}" > "${LOG_FILE}"
    fi

    # Rotate logs if needed
    rotate_logs

    # Log initialization
    log_info "Starting VM management script v${VERSION}"
    log_info "Initialization timestamp: ${init_timestamp}"
    log_info "Configuration:"
    log_info "- Log level: ${CURRENT_LOG_LEVEL}"
    log_info "- Debug mode: ${DEBUG}"
    log_info "- Destroy mode: ${DESTROY}"

    # Check required commands
    local required_commands=(
    "vagrant" \
    "virtualbox" \
    "git" \
    "git-lfs" \
    "awk" \
    "sed"
    )
    local missing_commands=()

    for cmd in "${required_commands[@]}"; do
        if ! check_command "${cmd}"; then
            missing_commands+=("${cmd}")
        fi
    done

    if [ "${#missing_commands[@]}" -gt 0 ]; then
        log_error "Missing required commands: ${missing_commands[*]}"
        exit 1
    fi

    check_install_vagrant_scp "${vagrant_scp_check}"

    # Check disk space
    if ! check_disk_space; then
        log_error "Disk space check failed"
        exit 1
    fi

    # Success case
    return 0
}

check_file_exists() {
    local file_path="${1}"
    if [ "${DEBUG}" -eq 1 ]; then
        printf "Debug: Current file_path: %s\n" "${file_path}"
        printf "Debug: Function check_file_exists %s\n"
    fi

    if [[ ! -f "${file_path}" ]]; then
        printf "Error: File '%s' does not exist.\n" "${file_path}" >&2
        return 1
    fi

    # Success case
    return 0
}

check_file_extension() {
    local file_path="${1}"
    if [ "${DEBUG}" -eq 1 ]; then
        printf "Debug: Current file_path: %s\n" "${file_path}"
        printf "Debug: Function check_file_extension %s\n"
    fi

    if [[ "${file_path}" != *.cfg ]]; then
        printf "Error: File '%s' does not have a .cfg extension.\n" "${file_path}" >&2
        return 1
    fi

    # Success case
    return 0
}

check_file_readable() {
    local file_path="${1}"
    if [ "${DEBUG}" -eq 1 ]; then
        printf "Debug: Current file_path: %s\n" "${file_path}"
        printf "Debug: Function check_file_readable %s\n"
    fi

    if [[ ! -r "${file_path}" ]]; then
        printf "Error: File '%s' is not readable.\n" "${file_path}" >&2
        return 1
    fi

    if [ "${DEBUG}" -eq 1 ]; then
        cat -v -t -e "${file_path}"
    fi

    # Success case
    return 0
}

check_file_ownership() {
    local file_path="${1}"

    if [ "${DEBUG}" -eq 1 ]; then
        printf "Debug: Current file_path: %s\n" "${file_path}"
        printf "Debug: Function check_file_ownership %s\n"
        printf "Debug: Current expected_user: %s\n" "${expected_user}"
        printf "Debug: Current expected_group: %s\n" "${expected_group}"
    fi

    local file_info
    file_info=$(ls -l "${file_path}")
    local actual_user
    actual_user=$(echo "${file_info}" | awk '{print $3}')
    local actual_group
    actual_group=$(echo "${file_info}" | awk '{print $4}')

    if [[ "${actual_user}" != "${expected_user}" ]]; then
        printf "Error: File '%s' is not owned by %s (actual owner: %s).\n" "${file_path}" "${expected_user}" "${actual_user}" >&2
        return 1
    fi

    if [[ "${actual_group}" != "${expected_group}" ]]; then
        printf "Error: File '%s' is not owned by group %s (actual group: %s).\n" "${file_path}" "${expected_group}" "${actual_group}" >&2
        return 1
    fi

    # Success case
    return 0
}

check_modification_time() {
    local file_path="${1}"
    if [ "${DEBUG}" -eq 1 ]; then
        printf "Debug: Current file_path: %s\n" "${file_path}"
        printf "Debug: Function check_modification_time %s\n"
    fi

    local modification_time 
    modification_time=1
    local file_mtime
    file_mtime=$(stat -c %Y "${file_path}")
    local current_time
    current_time=$(date +%s)
    local time_diff
    time_diff=$((current_time - file_mtime))
    local max_time
    max_time=$((modification_time * 3600))
    if [[ ${time_diff} -gt ${max_time} ]]; then
        printf "Error: File '%s' was not modified within the last %d hours.\n" "${file_path}" "${modification_time}" >&2
        return 1
    fi

    # Success case
    return 0
}

validate_file_hash() {
    local file_path="${1}"
    if [ "${DEBUG}" -eq 1 ]; then
        printf "Debug: Current file_path: %s\n" "${file_path}"
        printf "Debug: Function validate_file_hash %s\n"
    fi

    local hash_file="${file_path}.sha3-512"
    printf "Info: Current hash_file: %s\n" "${hash_file}"
    cat "${hash_file}"
    if [[ ! -f "${hash_file}" ]]; then
        printf "Error: Hash file '%s' does not exist.\n" "${hash_file}" >&2
        return 1
    fi
    local expected_hash 
    expected_hash=$(cat "${hash_file}")
    printf "Info: expected_hash: %s\n" "${expected_hash}"
    local actual_hash
    actual_hash=$(sha512sum "${file_path}" | awk '{print $1}')
    printf "Info: actual_hash: %s\n" "${actual_hash}"
    if [[ "${actual_hash}" != "${expected_hash}" ]]; then
        printf "Error: File '%s' hash does not match expected hash.\n" "${file_path}" >&2
        return 1
    fi

    # Success case
    return 0
}

check_for_symlinks() {
    local file_path="${1}"
    if [ "${DEBUG}" -eq 1 ]; then
        printf "Debug: Current file_path: %s\n" "${file_path}"
        printf "Debug: Function check_for_symlinks %s\n"
    fi

    if [[ -L "${file_path}" ]]; then
        printf "Error: File '%s' is a symlink.\n" "${file_path}" >&2
        return 1
    fi

    # Success case
    return 0
}

validate_file_path() {
    local file_path="${1}"
    if [ "${DEBUG}" -eq 1 ]; then
        printf "Debug: Current file_path: %s\n" "${file_path}"
        printf "Debug: Function validate_file_path %s\n"
    fi

    if [[ "${file_path}" == *".."* ]]; then
        printf "Error: File path '%s' contains directory traversal.\n" "${file_path}" >&2
        return 1
    fi

    # Success case
    return 0
}

check_setuid_setgid() {
    local file_path="${1}"
    if [ "${DEBUG}" -eq 1 ]; then
        printf "Debug: Current file_path: %s\n" "${file_path}"
        printf "Debug: Function check_setuid_setgid\n"
    fi

    local os_type
    os_type=$(uname)

    if [ "${os_type}" = "Darwin" ]; then
        # macOS
        local file_perms
        file_perms=$(ls -l "${file_path}" | cut -d ' ' -f 1)
        if [[ "${file_perms:3:1}" == "s" || "${file_perms:6:1}" == "s" ]]; then
            printf "Error: File '%s' has setuid or setgid bit set.\n" "${file_path}" >&2
            return 1
        fi
    elif [ "${os_type}" = "Linux" ]; then
        # Linux
        local file_perms
        file_perms=$(stat -c '%a' "${file_path}")
        if [[ "${file_perms:0:1}" == "2" || "${file_perms:0:1}" == "4" ]]; then
            printf "Error: File '%s' has setuid or setgid bit set.\n" "${file_path}" >&2
            return 1
        fi
    else
        printf "Error: Unsupported operating system.\n" >&2
        return 1
    fi

    # Success case
    return 0
}

check_file_size() {
    local file_path="${1}"
    if [ "${DEBUG}" -eq 1 ]; then
        printf "Debug: Current file_path: %s\n" "${file_path}"
        printf "Debug: Function check_file_size %s\n"
    fi

    local limit_file_size
    limit_file_size=75
    local file_size 
    file_size=$(du -k "${file_path}" | cut -f1)
    if [[ ${file_size} -gt ${limit_file_size} ]]; then
        printf "Error: File '%s' exceeds maximum size of %d KB.\n" "${file_path}" "${limit_file_size}" >&2
        return 1
    fi

    # Success case
    return 0
}

validate_file_structure() {
    local file_path="${1}"
    if [ "${DEBUG}" -eq 1 ]; then
        printf "Debug: Current file_path: %s\n" "${file_path}"
        printf "Debug: Function validate_file_structure %s\n"
    fi

    # This is a placeholder function. You need to implement specific checks
    # for your configuration file format.
    printf "Warning: File structure validation not implemented for '%s'.\n" "${file_path}" >&2

    # Success case
    return 0
}

check_whitelist() {
    local file_path="${1}"
    if [ "${DEBUG}" -eq 1 ]; then
        printf "Debug: Current file_path: %s\n" "${file_path}"
        printf "Debug: Function check_whitelist %s\n"
    fi

    local allowed_paths=(
        "/${HOMEDIR}/${USERSCRIPT}/repos/initial_laptop_setup/scripts/"
        "/${HOMEDIR}/${USERSCRIPT}/"
    )
    local is_allowed
    is_allowed=false
    for allowed_path in "${allowed_paths[@]}"; do
        if [[ "${file_path}" == "${allowed_path}"* ]]; then
            is_allowed=true
            break
        fi
    done
    if [[ "${is_allowed}" != true ]]; then
        printf "Error: File '%s' is not in the allowed paths.\n" "${file_path}" >&2
        return 1
    fi

    # Success case
    return 0
}

secure_compare() {
    local str1="${1}"
    local str2="${2}"
    if [ "${DEBUG}" -eq 1 ]; then
        printf "Debug: Current file_path: %s\n" "${file_path}"
        printf "Debug: Function secure_compare %s\n"
    fi

    if [[ ${#str1} -ne ${#str2} ]]; then
        return 1
    fi
    local result=0
    for (( i=0; i<${#str1}; i++ )); do
        result=$((result | $(printf '%d' "'${str1:$i:1}") ^ $(printf '%d' "'${str2:$i:1}")))
    done
    return ${result}

    # Success case
    return 0
}

check_file_is_text() {
    local file_path="${1}"
    if [ "${DEBUG}" -eq 1 ]; then
        printf "Debug: Current file_path: %s\n" "${file_path}"
        printf "Debug: Function check_file_is_text %s\n"
    fi

    if ! file -b --mime-type "${file_path}" | grep -q "^text/"; then
        printf "Error: File '%s' is not a text file.\n" "${file_path}" >&2
        return 1
    fi

    # Success case
    return 0
}

check_file_encoding() {
    local file_path="${1}"
    local desired_encoding 
    desired_encoding="${2:-UTF-8}"  # Default to UTF-8 if not specified

    if [ "${DEBUG}" -eq 1 ]; then
        printf "Debug: Current file_path: %s\n" "${file_path}"
        printf "Debug: Function check_file_encoding\n"
        printf "Debug: Desired encoding: %s\n" "${desired_encoding}"
    fi

    local detected_encoding
    detected_encoding=$(file -i "${file_path}" | awk -F= '{print $2}')
    printf "Info: Detected encoding: %s\n" "${detected_encoding}"

    case "${desired_encoding}" in
        ASCII|ascii)
            if ! file -i "${file_path}" | grep -q "charset=us-ascii"; then
                printf "Error: File '%s' is not ASCII encoded.\n" "${file_path}" >&2
                return 1
            fi
            ;;
        UTF-8|utf-8)
            if ! file -i "${file_path}" | grep -q "charset=utf-8"; then
                printf "Error: File '%s' is not UTF-8 encoded.\n" "${file_path}" >&2
                return 1
            fi
            ;;
        UTF-16|utf-16)
            if ! file -i "${file_path}" | grep -q "charset=utf-16"; then
                printf "Error: File '%s' is not UTF-16 encoded.\n" "${file_path}" >&2
                return 1
            fi
            ;;
        UTF-32|utf-32)
            if ! file -i "${file_path}" | grep -q "charset=utf-32"; then
                printf "Error: File '%s' is not UTF-32 encoded.\n" "${file_path}" >&2
                return 1
            fi
            ;;
        EBCDIC|ebcdic)
            if ! file -i "${file_path}" | grep -q "charset=ebcdic"; then
                printf "Error: File '%s' is not EBCDIC encoded.\n" "${file_path}" >&2
                return 1
            fi
            ;;
        ISO-8859|iso-8859)
            if ! file -i "${file_path}" | grep -q "charset=iso-8859"; then
                printf "Error: File '%s' is not ISO-8859 encoded.\n" "${file_path}" >&2
                return 1
            fi
            ;;
        *)
            printf "Error: Unsupported encoding '%s'.\n" "${desired_encoding}" >&2
            return 1
            ;;
    esac

    # Success case
    return 0
}

# Function to check the MIME type and encoding of a file with file
check_file_encoding_with_file() {
    local file="${1}"
    local desired_encoding
    desired_encoding="${2:-UTF-8}"  # Default to UTF-8 if not specified

    if [ "${DEBUG}" -eq 1 ]; then
        printf "Debug: Current file: %s\n" "${file}"
        printf "Debug: Function check_file_encoding_with_file\n"
        printf "Debug: Desired encoding: %s\n" "${desired_encoding}"
    fi

    check_command "file" || return 1

    local mime_info
    mime_info=$(file --mime "${file}")
    printf "Info: MIME info for '%s': %s\n" "${file}" "${mime_info}"

    case "${desired_encoding}" in
    ASCII|ascii)
        if ! echo "${mime_info}" | grep -q "charset=us-ascii"; then
            printf "Error: File '%s' is not ASCII encoded.\n" "${file}" >&2
            return 1
        fi
        ;;
    UTF-8|utf-8)
        if ! echo "${mime_info}" | grep -q "charset=utf-8"; then
            printf "Error: File '%s' is not UTF-8 encoded.\n" "${file}" >&2
            return 1
        fi
        ;;
    UTF-16|utf-16)
        if ! echo "${mime_info}" | grep -q "charset=utf-16"; then
            printf "Error: File '%s' is not UTF-16 encoded.\n" "${file}" >&2
            return 1
        fi
        ;;
    UTF-32|utf-32)
        if ! echo "${mime_info}" | grep -q "charset=utf-32"; then
            printf "Error: File '%s' is not UTF-32 encoded.\n" "${file}" >&2
            return 1
        fi
        ;;
    EBCDIC|ebcdic)
        if ! echo "${mime_info}" | grep -q "charset=ebcdic"; then
            printf "Error: File '%s' is not EBCDIC encoded.\n" "${file}" >&2
            return 1
        fi
        ;;
    ISO-8859|iso-8859)
        if ! echo "${mime_info}" | grep -q "charset=iso-8859"; then
            printf "Error: File '%s' is not ISO-8859 encoded.\n" "${file}" >&2
            return 1
        fi
        ;;
    *)
        printf "Error: Unsupported encoding '%s'.\n" "${desired_encoding}" >&2
        return 1
        ;;
esac


    printf "Info: File '%s' has the desired encoding: %s\n" "${file}" "${desired_encoding}"
    return 0
}

check_line_length() {
    local file_path="${1}"
    local max_line_length="${2}"  # Get max_line_length from the argument

    if [ "${DEBUG}" -eq 1 ]; then
        printf "Debug: Current file_path: %s\n" "${file_path}"
        printf "Debug: Function check_line_length %s\n"
    fi

    printf "Info: max_line_length: %s\n" "${file_path}" "${max_line_length}"
    local long_lines
    long_lines=$(awk -v max="${max_line_length}" 'length > max' "${file_path}")
    if [[ -n "${long_lines}" ]]; then
        printf "Error: File '%s' contains lines longer than %d characters.\n" "${file_path}" "${max_line_length}" >&2
        return 1
    fi

    # Success case
    return 0
}

check_ascii_only() {
    local file_path="${1}"
    if [ "${DEBUG}" -eq 1 ]; then
        printf "Debug: Current file_path: %s\n" "${file_path}"
        printf "Debug: Function check_ascii_only %s\n"
    fi

    if LC_ALL=C grep -q '[^[:ascii:]]' "${file_path}"; then
        printf "Error: File '%s' contains non-ASCII characters.\n" "${file_path}" >&2
        return 1
    fi

    if file -i "${file_path}" | grep -q "charset=utf-8"; then
        printf "Warning: File '%s' may contain Unicode characters.\n" "${file_path}" >&2
        return 1
    fi

    # Success case
    return 0
}

check_unicode() {
    local file_path="${1}"
    if [ "${DEBUG}" -eq 1 ]; then
        printf "Debug: Current file_path: %s\n" "${file_path}"
        printf "Debug: Function check_unicode %s\n"
    fi

    # Check if the file exists
    if [[ ! -f "${file_path}" ]]; then
        printf "Error: File '%s' does not exist.\n" "${file_path}"
        return 1
    fi

    # Use grep to check for non-ASCII characters
    if grep -q '[^[:print:]]' "${file_path}"; then
        printf "Unicode character detected in file '%s'. Exiting with Unicode character 🄍.\n" "${file_path}"
        exit 1  # Exit with a non-zero status
    else
        printf "File '%s' contains only ASCII characters. Pass.\n" "${file_path}"
    fi
}

check_unwanted_characters() {
    local file_path="${1}"
    if [ "${DEBUG}" -eq 1 ]; then
        printf "Debug: Current file_path: %s\n" "${file_path}"
        printf "Debug: Function check_unwanted_characters %s\n"
    fi

    local unwanted_chars="[€ßäöüÄÖÜ]" # original "[?!€ßäöüÄÖÜ]"
    if grep -q "${unwanted_chars}" "${file_path}"; then
        printf "Error: File '%s' contains unwanted characters.\n" "${file_path}" >&2
        return 1
    fi

    # Success case
    return 0
}

source_file() {
    local file_path="${1}"
    local max_line_length="${2}"
    # not all functions are working therefore they are deactivated
    printf "Info: Current default_max_line_length: %s\n" "${default_max_line_length}"
    printf "Info: Current local max_line_length: %s\n" "${max_line_length}"

    if [ "${DEBUG}" -eq 1 ]; then
        printf "Debug: Current file_path: %s\n" "${file_path}"
        printf "Debug: Function source_file %s\n"
    fi

    check_file_exists "${file_path}" || return 1
    check_file_extension "${file_path}" || return 1
    check_file_readable "${file_path}" || return 1

    check_file_ownership "${file_path}" || return 1
    #check_modification_time "${file_path}" || return 1
    validate_file_hash "${file_path}" || return 1
    check_for_symlinks "${file_path}" || return 1
    validate_file_path "${file_path}" || return 1
    check_setuid_setgid "${file_path}" || return 1
    check_file_size "${file_path}" || return 1

    #exit 0

    #validate_file_structure "${file_path}" || return 1
    check_whitelist "${file_path}" || return 1
    #secure_compare "${file_path}" || return 1

    check_file_is_text "${file_path}" || return 1
    #check_file_encoding "${file_path}" || return 1

    # Usage example:
    #check_file_encoding "${file_path}" "UTF-8" || return 0
    #check_file_encoding "${file_path}" "UTF-16" || return 0
    #check_file_encoding "${file_path}" "UTF-32" || return 1
    #check_file_encoding "${file_path}" "EBCDIC" || return 1
    #check_file_encoding "${file_path}" "ISO-8859" || return 1
    #check_file_encoding "${file_path}" "ASCII" || return 1

    #check_file_encoding_with_file "${file_path}" "${default_encoding}" || return 1

    #exit 0

    check_line_length "${file_path}" "${max_line_length}" || return 1
    #check_ascii_only "${file_path}" "${max_line_length}" || return 1
    check_unicode "${file_path}" "${max_line_length}" || return 1
    check_unwanted_characters "${file_path}" "${max_line_length}" || return 1

    (
        set -e
        set -f
        set -o noglob
        set -o nounset
        . "${file_path}"
    ) || {
        printf "Error: Failed to source file '%s'.\n" "${file_path}" >&2
        return 1
    }

    printf "Info: Successfully sourced file '%s'.\n" "${file_path}"

    # Success case
    return 0
}

printf "Info: Current functions_for_package_manager_file: %s\n" "${functions_for_package_manager_file}"
source_file "${functions_for_package_manager_file}" 500
#source_file /Users/cloud/repos/initial_laptop_setup/scripts/functions_for_package_manager.cfg 250

# Main function
main() {
    local start_time
    start_time="$(date +%s)"

    log_info "Starting main execution"
    print_separator

    if [ "${DESTROY}" -eq 1 ]; then
        log_info "Destroying existing VMs..."

        vagrant global-status --prune || log_info "vagrant global-status --prune"

        for vm_name in "${!vms[@]}"; do
            if [ -d "./vagrant/${vm_name}" ]; then
                pushd "./vagrant/${vm_name}" > /dev/null || exit 1
                vagrant destroy -f || log_warning "Failed to destroy VM '${vm_name}'"
                popd > /dev/null || exit 1
            fi
        done

        log_info "Existing VMs destroyed"
        print_separator
    fi

    # Process each VM
    local processed_vms=0
    local failed_vms=0

    for vm_name in "${!vms[@]}"; do
        log_info "Processing VM: ${vm_name}"

        # Create VM
        if ! create_vm "${vm_name}"; then
            log_error "Failed to create VM ${vm_name}"
            failed_vms=$((failed_vms + 1))
        fi

        for vm_name in "${!vms[@]}"; do
        if [ -d "./vagrant/${vm_name}" ]; then
            log_info "Processing VM: ${vm_name}"

            # Verify VM status
            if ! verify_vm_status "${vm_name}"; then
                log_error "Failed to verify VM ${vm_name}"
            fi

            # Configure DNS
            log_info "Configuring DNS for VM: ${vm_name}"
            if ! configure_dns "${vm_name}"; then
                log_error "Failed to configure DNS for ${vm_name}"
            fi

            # Configure systemd-resolved if present
            if ! configure_systemd_resolved "${vm_name}"; then
                log_warning "Failed to configure systemd-resolved for ${vm_name}"
            fi

            # Test DNS configuration
            if vagrant ssh "${vm_name}" -c "ping -c 1 google.com"; then
            log_info "DNS configuration verified for ${vm_name}"
            else
            log_warning "DNS configuration may have issues on ${vm_name}"
            fi

            # Configure Test
            log_info "Configuring Test for VM: ${vm_name}"
            if ! configure_test "${vm_name}"; then
                log_error "Failed to configure Test for ${vm_name}"
            fi

            print_separator

            else
                log_warning "Directory not found for VM: ${vm_name}"
            fi
        done

        # Install base packages
        if ! install_packages "${vm_name}" "apt" "${package_groups["base"]}"; then
            log_error "Failed to install base packages on ${vm_name}"
            failed_vms=$((failed_vms + 1))
        fi

        # Configure security
        if ! configure_security "${vm_name}"; then
            log_error "Failed to configure security on ${vm_name}"
            failed_vms=$((failed_vms + 1))
        fi

        # Install specific packages based on VM type
        case "${vm_name}" in
            "debian")
                if ! install_packages "${vm_name}" "apt" "${package_groups["automation"]}"; then
                    log_error "Failed to install automation packages on ${vm_name}"
                    failed_vms=$((failed_vms + 1))
                fi
                ;;
            "debian-automation-controller")
                if ! install_packages "${vm_name}" "apt" "${package_groups["automation"]}"; then
                    log_error "Failed to install automation packages on ${vm_name}"
                    failed_vms=$((failed_vms + 1))
                fi
                ;;
            "debian-job-controller")
                if ! install_packages "${vm_name}" "apt" "openjdk-11-jdk jenkins"; then
                    log_error "Failed to install job controller packages on ${vm_name}"
                    failed_vms=$((failed_vms + 1))
                fi
                ;;
            "debian-vulnerability-scanner")
                if ! install_packages "${vm_name}" "apt" "${package_groups["security"]}"; then
                    log_error "Failed to install security packages on ${vm_name}"
                    failed_vms=$((failed_vms + 1))
                fi
                ;;
        esac

        if [ "${do_update_packages}" -eq 1 ]; then
        log_info "Starting package updates on all VMs..."
        print_separator

        #for vm_name in "${!vms[@]}"; do
        #    if [ -d "./vagrant/${vm_name}" ]; then
        #        log_info "Running package updates on VM: ${vm_name}"
        #        try_all_package_managers "${vm_name}" || {
        #            log_warning "Some package managers failed on ${vm_name}"
        #        }
        #        print_separator
        #    fi
        #done

        if [ "${SSH_CHECK}" -eq 1 ]; then
        log_info "Starting SSH checks on all VMs..."
        print_separator

        local ssh_failed_vms=0

        for vm_name in "${!vms[@]}"; do
            if [ -d "./vagrant/${vm_name}" ]; then
                log_info "Checking SSH connection for VM: ${vm_name}"
                if ! check_ssh_connection "${vm_name}"; then
                    ssh_failed_vms=$((ssh_failed_vms + 1))
                fi
                print_separator
            fi
        done

        if [ "${ssh_failed_vms}" -gt 0 ]; then
            log_warning "SSH checks failed for ${ssh_failed_vms} VMs"
        else
            log_info "All SSH checks completed successfully"
        fi
        print_separator
        fi

        log_info "Package updates completed"
        print_separator
        fi
        # Collect initial metrics
        if ! collect_system_metrics "${vm_name}"; then
            log_warning "Failed to collect initial metrics for ${vm_name}"
        fi

        processed_vms=$((processed_vms + 1))
        log_info "Successfully processed VM: ${vm_name}"
        print_separator
    done

    # Calculate execution time
    local end_time
    end_time="$(date +%s)"
    local execution_time=$((end_time - start_time))

    # Print summary
    printf "\nExecution Summary:\n"
    printf "Total VMs processed: %d\n" "${processed_vms}"
    printf "Failed VMs: %d\n" "${failed_vms}"
    printf "Total execution time: %d seconds\n" "${execution_time}"

    if [ "${failed_vms}" -gt 0 ]; then
        return 1
    fi

    # Success case
    return 0
}

# Script entry point
{
    parse_arguments "$@"
    initialize
    main
    log_info "Script completed successfully"
} || {
    log_error "Script failed"
    exit 1
}

# Final cleanup
cleanup

log_info "Script execution finished"

script_name1="$(basename "${0}")"
printf "\nInfo: script_name1: %s\n" "${script_name1}"
script_path1="$(realpath "$(dirname "${0}")")"
printf "Info: script_path1: %s\n" "${script_path1}"
script_path_with_name="${script_path1}/${script_name1}"
printf "Info: Script path with name: %s\n" "${script_path_with_name}"
printf "Info: Script finished\n"
exit 0
