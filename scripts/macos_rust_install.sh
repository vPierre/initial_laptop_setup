#!/usr/bin/env bash
# Author: Pierre Gronau <Pierre.Gronau@ndaal.eu>
# Copyright 2024
# License: All content is licensed under the terms of the <MIT License>
# Developed on: Debian 12.x; macOS Sequoia x86 architecture
# Tested on: Debian 12.x; macOS Sequoia x86 architecture
#
# Exit on error. Append "|| true" if you expect an error.
set -o errexit
# This is equivalent to set -e. It causes the script to exit
# immediately if any command exits with a non-zero status.
#
# Exit on error inside any functions or subshells.
set -o errtrace
# This setting ensures that the ERR trap is inherited by shell functions,
# command substitutions, and commands executed in a subshell environment.
#
# Do not allow use of undefined vars. Use ${VAR:-} to use an undefined VAR
set -o nounset
# This is equivalent to set -u. It treats unset variables as
# an error when substituting.
#
# Catch the error in case mysqldump fails (but gzip succeeds) in `mysqldump |gzip`
# https://vaneyckt.io/posts/safer_bash
set -o pipefail
# This setting causes a pipeline to return the exit status of the last command
# in the pipe that returned a non-zero status.
#
# Turn on traces, useful while debugging but commented out by default
# set -o xtrace

# Set $IFS to only newline and tab.
#
# http://www.dwheeler.com/essays/filenames-in-shell.html
# nosemgrep: ifs-tampering
IFS=$'\n\t'

# Error trapping function
error_handler() {
    local line_number="${1}"
    local error_code="${2}"
    local last_command="${BASH_COMMAND}"
    printf "Error: Error occurred in line %s (error code: %s)\n" "${line_number}" "${error_code}"
    printf "Error: Failed command: %s\n" "${last_command}"
}

# Set the error trap
trap 'error_handler ${LINENO} $?' ERR

cleanup() {
    trap - SIGINT SIGTERM ERR EXIT
    printf "%b\n" "\nInfo: Cleanup is running ..."
    # Additional cleanup tasks can be added here if needed
    rm -f -v ./*.tmp
    printf "%b\n" "\nInfo: Cleanup finished ..."
}

trap cleanup SIGINT SIGTERM ERR EXIT

# Function to check if a command is available
check_command() {
    if ! command -v "${1}" &>/dev/null; then
        printf "Error: %s is not installed or not in PATH. Please install it and try again.\n" "${1}"
        return 1
    fi
}

check_command "curl"

if ! command -v rustc &> /dev/null
then
    printf "Info: Install Rust via cURL and Bash\n"
    curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y || true

    printf "Info: Activate Rust Environment on macOS\n"
    source ~/.cargo/env || true
else
    printf "Info: Rust is already installed.\n"
fi


printf "Info: Confirm Rust Installation.\n"
rustc -V || true

rustup update || true

printf "Info: Here you find descriptions for the packages:\n"
printf "Info: https://github.com/uhub/awesome-rust \n"
printf "Info: https://project-awesome.org/rust-unofficial/awesome-rust \n"

PACKAGES=(
    "xcp"
    "ytop"
    "tokei"
    "md5sum"
    "sha1sum"
    "sha256sum"
    "sha512sum"
    "sha3sum"
    "k12sum"
    "twox-hash-cli"
    "b2sum"
    "b3sum"
    "ripgrep"
    "rustscan"
    "xsv"
    "base64-cli"
    "exa"
    "hashgood"
    "hashguard"
    "eureka"
    "rargs"
    "bingrep"
    "hx"
    "fd"
    "crabz"
    "thash"
    "wholesum"
    "zoxide"
    "bat"
    "tealdeer"
    "duf"
    "yazi"
    #"rust-brotli"
    #"zopfli"
    )

cargo install "${PACKAGES[@]}" || true

tldr --update || true

cleanup

script_name1="$(basename "${0}")"
printf "\nInfo: script_name1: %s\n" "${script_name1}"
script_path1="$(realpath "$(dirname "${0}")")"
printf "Info: script_path1: %s\n" "${script_path1}"
script_path_with_name="${script_path1}/${script_name1}"
printf "Info: Script path with name: %s\n" "${script_path_with_name}"
printf "Info: Script finished\n"
exit 0
