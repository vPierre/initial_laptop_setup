# Author: Pierre Gronau <Pierre.Gronau@ndaal.eu>
# Copyright 2025
# License: All content is licensed under the terms of the <Apache 2.0>
# Developed on: Debian 12.9x; macOS Sequoia x86 architecture
# Tested on: Debian 12.9x; macOS Sequoia x86 architecture

set -o errexit
set -o errtrace
set -o nounset
set -o pipefail
set -o noclobber
# set -o xtrace

# Set IFS explicitly
# nosemgrep: ifs-tampering
IFS=$'\n\t'

# Error trapping function
# Add this line before the error_handler function
# shellcheck disable=SC2317
error_handler() {
    local line_number="${1}"
    local error_code="${2}"
    local last_command="${BASH_COMMAND}"
    printf "Error: Error occurred in line %s (error code: %s)\n" "${line_number}" "${error_code}"
    printf "Error: Failed command: %s\n" "${last_command}"
}

# Set the error trap
trap 'error_handler ${LINENO} $?' ERR

cleanup() {
    trap - SIGINT SIGTERM ERR EXIT
    printf "Info: Cleanup is running ...\n"
    # Additional cleanup tasks can be added here if needed
    rm -f -v ./*.tmp
    printf "Info: Cleanup finished ...\n"
}

trap cleanup SIGINT SIGTERM EXIT

if [[ "$(uname)" == "Darwin" ]]; then
    HOMEDIR="Users"
elif [[ "$(uname)" == "Linux" ]]; then
    HOMEDIR="home"
else
    printf "Error: Unsupported operating system: %s\n" "$(uname)"
    exit 1
fi
readonly HOMEDIR

printf "Info: Home Directory: %s\n" "${HOMEDIR}"

USERSCRIPT="cloud"
printf "Info: User script: %s\n" "${USERSCRIPT}"

VERSION="0.6.3"
readonly VERSION
printf "Info: Current version: %s\n" "${VERSION}"

LOG_FILE="/${HOMEDIR}/${USERSCRIPT}/create_functions_for_package_manager.log"
readonly LOG_FILE 
printf "Info: log file with path: %s\n" "${LOG_FILE}"

if [[ ! -f "${LOG_FILE}" ]]; then
    printf "Info: Log file does not exist. Creating it now.\n"
    touch "${LOG_FILE}" || {
        printf "Error: Failed to create log file at %s\n" "${LOG_FILE}"
        exit 1
    }
else
    printf "Info: Log file already exists at %s\n" "${LOG_FILE}"
fi

log() {
    printf "%s - %s\n" "$(date '+%Y-%m-%d %H:%M:%S')" "${1}" | tee -a "${LOG_FILE}"
}

log "starting script"

check_command() {
    if ! command -v "${1}" > /dev/null 2>&1; then
        printf "Error: %s is not installed or not in PATH. Please install it and try again.\n" "${1}"
        return 1
    fi

    # Success case
    return 0
}

# Check for required commands
check_command "cat" || exit 1
check_command "sed" || exit 1
check_command "sha512sum" || exit 1

SOURCE_DIRECTORY="/${HOMEDIR}/${USERSCRIPT}/repos/initial_laptop_setup/scripts/"
readonly SOURCE_DIRECTORY
printf "Info: Current SOURCE_DIRECTORY: %s\n" "${SOURCE_DIRECTORY}"

TARGET_DIRECTORY="/${HOMEDIR}/${USERSCRIPT}/repos/initial_laptop_setup/scripts/"
readonly TARGET_DIRECTORY
printf "Info: Current TARGET_DIRECTORY: %s\n" "${TARGET_DIRECTORY}"

TARGET_FILE="functions_for_package_manager.cfg"
readonly TARGET_FILE
printf "Info: Current TARGET_FILE: %s\n" "${TARGET_FILE}"

create_directory() {
    local dir="${1}"
    printf "Info: Directory is %s\n" "${dir}"
    if [[ ! -d "${dir}" ]]; then
        mkdir -p -v "${dir}"
        touch "${dir}/placeholder.txt"
        printf "Info: Directory %s is created.\n" "${dir}"
        rm -f -v "${dir}/placeholder.txt"
    fi

    # Success case
    return 0
}

DIRECTORY="${SOURCE_DIRECTORY}"
printf "Info: %s\n" "${DIRECTORY}"

create_directory "${DIRECTORY}"

DIRECTORY="${TARGET_DIRECTORY}"
printf "Info: %s\n" "${DIRECTORY}"

create_directory "${DIRECTORY}"

Function_Create_Checksums_for_cfg() {
    printf "Info: Create checksums files \n"
    printf "%s\n" "${TARGET_DIRECTORY}"

    find "${TARGET_DIRECTORY}" -type f -name "*.cfg" -print0 |
        xargs -0 -n 1 sh -c "sha512sum \"\$1\" | awk '{print \$1}' | tee \"\$1.sha3-512\"" _

    # Success case
    return 0
}

FILE_ARRAY=(
    "function_cleanup_package_manager_for_package_manager.cfg"
    "function_install_packages_for_package_manager.cfg"
    "function_update_packages_for_package_manager.cfg"
    "function_list_installed_packages_for_package_manager.cfg"
)

printf "Info: required files for combining:\n"
printf "Info: required file: %s\n" "${FILE_ARRAY[@]}"

ls -la "${TARGET_DIRECTORY}${TARGET_FILE}" || true
rm -f -v "${TARGET_DIRECTORY}${TARGET_FILE}" || true


HEADER="# Author: Pierre Gronau <Pierre.Gronau@ndaal.eu>
# Copyright 2025
# License: All content is licensed under the terms of the <Apache 2.0>
# Developed on: Debian 12.9x; macOS Sequoia x86 architecture
# Tested on: Debian 12.9x; macOS Sequoia x86 architecture"

combine_files() {
    local temp_file
    temp_file=$(mktemp /tmp/functions_for_package_manager.cfg.XXXXXX) || {
        printf "Error: Failed to create temporary file\n"
        exit 1
    }

    # Write header to temporary file once
    printf "%s\n\n" "${HEADER}" >> "${temp_file}"

    for file in "${FILE_ARRAY[@]}"; do
        if [[ -f "${SOURCE_DIRECTORY}${file}" ]]; then
            # Remove duplicate headers and append content to temp file
            sed '/^# Author: Pierre Gronau/,/^# Tested on:/d' "${SOURCE_DIRECTORY}${file}" >> "${temp_file}"
            printf "\n" >> "${temp_file}"
        else
            printf "Error: File '%s' not found in '%s'\n" "${file}" "${SOURCE_DIRECTORY}"
            exit 1
        fi
    done

    # Remove duplicate empty lines and ensure only one empty line at the end
    if [[ "$(uname)" == "Darwin" ]]; then
        sed -e '/^$/N;/^\n$/D' -e '$a\' "${temp_file}" > "${TARGET_DIRECTORY}${TARGET_FILE}"
    else
        sed -e '/^$/N;/^\n$/D' -e '$a\\' "${temp_file}" > "${TARGET_DIRECTORY}${TARGET_FILE}"
    fi

    rm -f "${temp_file}"
}

# Main execution flow.
combine_files
Function_Create_Checksums_for_cfg

printf "Info: Combined file created at %s\n" "${TARGET_DIRECTORY}${TARGET_FILE}"

cleanup

script_name1="$(basename "${0}")"
printf "\nInfo: script_name1: %s\n" "${script_name1}"
script_path1="$(realpath "$(dirname "${0}")")"
printf "Info: script_path1: %s\n" "${script_path1}"
script_path_with_name="${script_path1}/${script_name1}"
printf "Info: Script path with name: %s\n" "${script_path_with_name}"
printf "Info: Script finished\n"
exit 0
