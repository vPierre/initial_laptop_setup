#!/usr/bin/env bash
# Author: Pierre Gronau <Pierre.Gronau@ndaal.eu>
# Copyright 2025
# License: All content is licensed under the terms of the <Apache 2.0>
# Developed on: Debian 12.9x; macOS Sequoia x86 architecture
# Tested on: Debian 12.9x; macOS Sequoia x86 architecture
#
set -o errexit
set -o errtrace
set -o nounset
set -o pipefail
# set -o xtrace

# nosemgrep: ifs-tampering
IFS=$'\n\t'

# Error trapping function
# Add this line before the error_handler function
# shellcheck disable=SC2317 
error_handler() {
    local line_number="${1}"
    local error_code="${2}"
    local last_command="${BASH_COMMAND}"
    printf "Error: Error occurred in line %s (error code: %s)\n" "${line_number}" "${error_code}"
    printf "Error: Failed command: %s\n" "${last_command}"
}

# Set the error trap
trap 'error_handler ${LINENO} $?' ERR

cleanup() {
    trap - SIGINT SIGTERM ERR EXIT
    printf "%b\n" "\nInfo: Cleanup is running ..."
    # Additional cleanup tasks can be added here if needed
    rm -f -v ./*.tmp
    printf "%b\n" "\nInfo: Cleanup finished ..."
}

trap cleanup SIGINT SIGTERM EXIT

if [[ "$(uname)" == "Darwin" ]]; then
    HOMEDIR="Users"
elif [[ "$(uname)" == "Linux" ]]; then
    HOMEDIR="home"
else
    printf "Error: Unsupported operating system: %s\n" "$(uname)"
    exit 1
fi
readonly HOMEDIR

# Under Linux you use `home` under macOS `Users`
printf "Info: Home Directory: %s\n" "${HOMEDIR}"

USERSCRIPT="cloud"
# Your user! In which context it SHOULD run
printf "Info: User script: %s\n" "${USERSCRIPT}"

VERSION="0.5.0"
readonly VERSION
printf "Info: Current version: %s\n" "${VERSION}"

SCRIPT_NAME="$(basename "${0}")"
readonly SCRIPT_NAME
printf "Info: Current SCRIPT_NAME: %s\n" "${SCRIPT_NAME}"

SCRIPT_PATH="$(realpath "$(dirname "${0}")")"
readonly SCRIPT_PATH
printf "Info: Current SCRIPT_PATH: %s\n" "${SCRIPT_PATH}"

SCRIPT_PATH_WITH_NAME="${SCRIPT_PATH}/${SCRIPT_NAME}"
readonly SCRIPT_PATH_WITH_NAME
printf "Info: Current SCRIPT_PATH_WITH_NAME: %s\n" "${SCRIPT_PATH_WITH_NAME}"

DESTINATION="${SCRIPT_PATH}"
printf "Info: Destination Directory: %s\n" "${DESTINATION}"

ROLE_NAME="chrony"
readonly ROLE_NAME
printf "Info: Current ROLE_NAME: %s\n" "${ROLE_NAME}"

create_directory() {
    local dir="${1}"
    printf "Info: Directory will be %s\n" "${dir}"
    if [[ ! -d ${dir} ]]; then
        mkdir -p -v "${dir}"
        printf "Info: Directory %s is created.\n" "${dir}"
        printf "Info: placeholder.txt will be created in %s\n" "${dir}"
        touch "${dir}/placeholder.txt"
        printf "Info: placeholder.txt will be removed from %s\n" "${dir}"
        rm -f -v "${dir}/placeholder.txt"
        printf "Info: placeholder.txt is removed from %s\n" "${dir}"
    else
        printf "Info: Directory %s already exists. No creation needed.\n" "${dir}"
    fi

    # Success case
    return 0
}

create_file() {
    local file="${1}"
    printf "Info: File will be %s\n" "${file}"
    if [[ ! -f "${file}" ]]; then
        touch "${file}"
        printf "Info: File %s is created.\n" "${file}"
    else
        printf "Info: File %s already exists. No creation needed.\n" "${file}"
    fi

    # Success case
    return 0
}

DIRECTORY="${DESTINATION}/tools/ansible/role/${ROLE_NAME}"
printf "Info: %s\n" "${DIRECTORY}"

create_directory "${DIRECTORY}"
create_directory "${DIRECTORY}/defaults"
create_directory "${DIRECTORY}/handlers"
create_directory "${DIRECTORY}/tasks"
create_directory "${DIRECTORY}/templates"
create_directory "${DIRECTORY}/molecule/default"
create_directory "${DIRECTORY}/meta"
create_directory "${DIRECTORY}/vars"

create_file "${DIRECTORY}/defaults/main.yml"
create_file "${DIRECTORY}/handlers/main.yml"
create_file "${DIRECTORY}/tasks/main.yml"
#create_file "${DIRECTORY}/templates/chrony.conf.j2"
create_file "${DIRECTORY}/molecule/default/molecule.yml"
create_file "${DIRECTORY}/molecule/default/converge.yml"
create_file "${DIRECTORY}/molecule/default/verify.yml"
create_file "${DIRECTORY}/molecule/default/prepare.yml"
create_file "${DIRECTORY}/meta/main.yml"
create_file "${DESTINATION}.gitignore"
create_file "${DESTINATION}.ansible-lint"

cat << EOF > "${DIRECTORY}/meta/main.yml"
---
galaxy_info:
  role_name: basic_tools
  author: Pierre Gronau
  company: ndaal, Cologne
  description: Role to install and configure YYYY for RHEL-based systems
  license: Apache-2.0
  min_ansible_version: 2.9
  platforms:
    - name: EL
      versions:
        - "9"
  galaxy_tags:
    - 
    - security
    - redhat
    - almalinux
    - rockylinux
    - oraclelinux
dependencies: []
EOF

printf "Info: Content added to %s/meta/main.yml\n" "${DIRECTORY}"

printf "Info: Creating .gitignore file\n"

cat << EOF > "${DESTINATION}/.gitignore"
---
*.py[cod]
*.swp
*.swo
.DS_Store
.molecule
.cache
*.retry
EOF

printf "Info: Content added to %s .gitignore file\n" "${DESTINATION}"

printf "Info: Creating .gitignore file\n"

cat << EOF > "${DESTINATION}/.ansible-lint"
---
# .ansible-lint

# Use the production profile for the most comprehensive linting
profile: production

# Exclude paths to ignore during linting
exclude_paths:
  - .cache/
  - .git/
  - tests/
  - molecule/

# Enable offline mode to improve performance
offline: true

# Enable strict mode for more rigorous checking
strict: true

# Use default rules
use_default_rules: true

# Enable specific opt-in rules
enable_list:
  - empty-string-compare
  - no-log-password
  - no-same-owner
  - yaml

# Warn on these rules instead of failing
warn_list:
  - experimental
  - role-name

# Skip specific rules
skip_list:
  - package-latest

# Enforce variable naming pattern
var_naming_pattern: "^[a-z_][a-z0-9_]*$"

# Set task name prefix
task_name_prefix: "{stem} | "

# Disable automatic fixes
write_list:
  - none

# Set maximum block depth
max_block_depth: 20

# Custom file identification
kinds:
  - yaml: "**/*.{yml,yaml}"
  - playbook: "**/playbooks/*.{yml,yaml}"
  - tasks: "**/tasks/*.{yml,yaml}"
  - handlers: "**/handlers/*.{yml,yaml}"
  - vars: "**/vars/*.{yml,yaml}"
  - meta: "**/meta/main.{yml,yaml}"

# Disable action validation as Ansible syntax check covers it
skip_action_validation: false
EOF

printf "Info: Content added to %s .ansible-lint file\n" "${DESTINATION}"

cleanup

printf "Info: stopping script ...\n"

script_name1="$(basename "${0}")"
printf "\nInfo: script_name1: %s\n" "${script_name1}"
script_path1="$(realpath "$(dirname "${0}")")"
printf "Info: script_path1: %s\n" "${script_path1}"
script_path_with_name="${script_path1}/${script_name1}"
printf "Info: Script path with name: %s\n" "${script_path_with_name}"
printf "Info: Script finished\n"
exit 0
