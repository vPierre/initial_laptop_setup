variable "debian_version" {
  type    = string
  default = "12.8.0"
}

source "virtualbox-iso" "debian" {
  iso_url            = "https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/debian-12.8.0-amd64-netinst.iso"
  iso_checksum       = "sha256:04396d12b0f377958a070c38a923c227832fa3b3e18ddc013936ecf492e9fbb3"
  ssh_username       = "vagrant"
  ssh_password       = "vagrant"
  shutdown_command   = "shutdown now"
  disk_size          = 10240  # 10GB disk size
  memory             = 1024    # 1GB RAM
  cpus               = 1       # 

  communicator       = "ssh"

  boot_command = [
    "<wait>",
    "install auto=true priority=critical preseed/url=http://{{ .HTTPIP }}:{{ .HTTPPort }}/preseed.cfg<wait>",
    "<enter>"
  ]

  http_directory = "http"  # Directory where preseed.cfg will be served
}

build {
  sources = ["source.virtualbox-iso.debian"]

  provisioner "shell" {
    inline = [
      "apt-get update",
      "apt-get install -y build-essential",
      "apt-get install -y virtualbox-guest-utils"
    ]
  }
}
