#!/usr/bin/env bash
# Author: Pierre Gronau <Pierre.Gronau@ndaal.eu>
# Copyright 2024
# License: All content is licensed under the terms of the <MIT License>
# Developed on: Debian 12.x; macOS Sequoia x86 architecture
# Tested on: Debian 12.x; macOS Sequoia x86 architecture
#
# Exit on error. Append "|| true" if you expect an error.
set -o errexit
# This is equivalent to set -e. It causes the script to exit
# immediately if any command exits with a non-zero status.
#
# Exit on error inside any functions or subshells.
set -o errtrace
# This setting ensures that the ERR trap is inherited by shell functions,
# command substitutions, and commands executed in a subshell environment.
#
# Do not allow use of undefined vars. Use ${VAR:-} to use an undefined VAR
set -o nounset
# This is equivalent to set -u. It treats unset variables as
# an error when substituting.
#
# Catch the error in case mysqldump fails (but gzip succeeds) in `mysqldump |gzip`
# https://vaneyckt.io/posts/safer_bash
set -o pipefail
# This setting causes a pipeline to return the exit status of the last command
# in the pipe that returned a non-zero status.
#
# Turn on traces, useful while debugging but commented out by default
# set -o xtrace

# Set $IFS to only newline and tab.
#
# http://www.dwheeler.com/essays/filenames-in-shell.html
# nosemgrep: ifs-tampering
IFS=$'\n\t'

# Error trapping function
error_handler() {
    local line_number="${1}"
    local error_code="${2}"
    local last_command="${BASH_COMMAND}"
    printf "Error: Error occurred in line %s (error code: %s)\n" "${line_number}" "${error_code}"
    printf "Error: Failed command: %s\n" "${last_command}"
}

# Set the error trap
trap "error_handler ${LINENO} \$?" ERR

cleanup() {
    trap - SIGINT SIGTERM ERR EXIT
    printf "%b\n" "\nInfo: Cleanup is running ..."
    # Additional cleanup tasks can be added here if needed
    rm -f -v ./*.tmp
    printf "%b\n" "\nInfo: Cleanup finished ..."
}

trap cleanup SIGINT SIGTERM ERR EXIT

if [ "${EUID}" -ne 0 ]; then
    script_path=$([[ "${0}" = /* ]] && printf "%s" "${0}" || printf "%s" "${PWD}/${0#./}")
    sudo "${script_path}" || (
        printf "Administrator privileges are required.\n"
        exit 1
    )
    exit 0
fi

clear_bash_history="yes"
clear_zsh_history="yes"
clear_diagnostic_logs"yes"
clear_diagnostic_log_details="yes"
clear_apple_system_logs="yes"
clear_installation_logs="yes"
clear_all_system_logs="yes"
clear_system_application_logs="yes"
clear_user_application_logs="yes"
clear_system_maintenance_logs="yes"
clear_app_installation_logs="yes"
clear_chrome_browsing_history="yes"
clear_chrome_cache="yes"
clear_safari_cache="yes"
clear_safari_webpage_previews="yes"
clear_safari_browsing_history="yes"
clear_safari_downloads_history="yes"
clear_safari_frequently_visited_sites="yes"
clear_safari_last_session="yes"
clear_safari_history_copy="yes"
clear_safari_search_history="yes"
clear_safari_cookies="yes"
clear_safari_zoom_preferences="yes"
clear_safari_notification_preferences="yes"
clear_safari_site_preferences="yes"
clear_firefox_cache="yes"
clear_firefox_form_history="yes"
clear_firefox_site_preferences="yes"
clear_firefox_session_restore="yes"
clear_firefox_passwords="yes"
clear_firefox_html5_cookies="yes"
clear_firefox_crash_reports="yes"
clear_firefox_backup_files="yes"
clear_firefox_cookies="yes"
clear_privacy_sexy_history="yes"
clear_adobe_cache="yes"
clear_gradle_cache="yes"
clear_dropbox_cache="yes"
clear_google_drive_cache="yes"
clear_composer_cache="yes"
clear_homebrew_cache="yes"
clear_ruby_gem_cache="yes"
clear_docker_cache="yes"
clear_pyenv_virtualenv_cache="yes"
clear_npm_cache="yes"
clear_yarn_cache="yes"
clear_ios_app_copies="yes"
clear_ios_photo_cache="yes"
clear_ios_device_backups="yes"
clear_ios_simulators="yes"
clear_connected_ios_devices="yes"
reset_all_permissions="yes"
reset_camera_permissions="yes"
reset_microphone_permissions="yes"

clear_bash_history() {
    printf "%s\n" "Info: Clear bash history"
    rm -f "${HOME}/.bash_history"
}

clear_zsh_history() {
    printf "%s\n" "Info: Clear zsh history"
    rm -f "${HOME}/.zsh_history"
}

clear_diagnostic_logs() {
    printf "%s\n" "Info: Clear diagnostic logs"
    glob_pattern="/private/var/db/diagnostics/*"
    sudo rm -rfv "${glob_pattern}"
}

clear_diagnostic_log_details() {
    printf "%s\n" "Info: Clear diagnostic log details"
    glob_pattern="/private/var/db/uuidtext/*"
    sudo rm -rfv "${glob_pattern}"
}

clear_apple_system_logs() {
    printf "%s\n" "Info: Clear Apple System Logs (ASL)"
    sudo rm -rfv "/private/var/log/asl/*"
    sudo rm -fv "/private/var/log/asl.log"
    sudo rm -fv "/private/var/log/asl.db"
}

clear_installation_logs() {
    printf "%s\n" "Info: Clear installation logs"
    sudo rm -fv "/private/var/log/install.log"
}

clear_all_system_logs() {
    printf "%s\n" "Info: Clear all system logs"
    sudo rm -rfv "/private/var/log/*"
}

clear_system_application_logs() {
    printf "%s\n" "Info: Clear system application logs"
    sudo rm -rfv "/Library/Logs/*"
}

clear_user_application_logs() {
    printf "%s\n" "Info: Clear user application logs"
    rm -rfv "${HOME}/Library/Logs/*"
}

clear_mail_app_logs() {
    printf "%s\n" "Info: Clear Mail app logs"
    rm -rfv "${HOME}/Library/Containers/com.apple.mail/Data/Library/Logs/Mail/*"
}

clear_user_activity_audit_logs() {
    printf "%s\n" "Info: Clear user activity audit logs (login, logout, authentication, etc.)"
    sudo rm -rfv "/private/var/audit/*"
}

clear_system_maintenance_logs() {
    printf "%s\n" "Info: Clear system maintenance logs"
    sudo rm -fv "/private/var/log/daily.out"
    sudo rm -fv "/private/var/log/weekly.out"
    sudo rm -fv "/private/var/log/monthly.out"
}

clear_app_installation_logs() {
    printf "%s\n" "Info: Clear app installation logs"
    sudo rm -rfv "/private/var/db/receipts/*"
    rm -fv "/Library/Receipts/InstallHistory.plist"
}

clear_chrome_browsing_history() {
    printf "%s\n" "Info: Clear Chrome browsing history"
    rm -rfv "${HOME}/Library/Application Support/Google/Chrome/Default/History" &>/dev/null
    rm -rfv "${HOME}/Library/Application Support/Google/Chrome/Default/History-journal" &>/dev/null
}

clear_chrome_cache() {
    printf "%s\n" "Info: Clear Chrome cache"
    sudo rm -rfv "${HOME}/Library/Application Support/Google/Chrome/Default/Application Cache/*" &>/dev/null
}

clear_safari_cache() {
    printf "%s\n" "Info: Clear Safari cached blobs, URLs and timestamps"
    rm -f "${HOME}/Library/Caches/com.apple.Safari/Cache.db"
}

clear_safari_url_icons() {
    printf "%s\n" "Info: Clear Safari URL bar web page icons"
    rm -f "${HOME}/Library/Safari/WebpageIcons.db"
}

clear_safari_webpage_previews() {
    printf "%s\n" "Info: Clear Safari webpage previews (thumbnails)"
    rm -rfv "${HOME}/Library/Caches/com.apple.Safari/Webpage Previews"
}

clear_safari_browsing_history() {
    printf "%s\n" "Info: Clear Safari browsing history"
    rm -f "${HOME}/Library/Safari/History.db"
    rm -f "${HOME}/Library/Safari/History.db-lock"
    rm -f "${HOME}/Library/Safari/History.db-shm"
    rm -f "${HOME}/Library/Safari/History.db-wal"
    rm -f "${HOME}/Library/Safari/History.plist"
    rm -f "${HOME}/Library/Safari/HistoryIndex.sk"
}

clear_safari_downloads_history() {
    printf "%s\n" "Info: Clear Safari downloads history"
    rm -f "${HOME}/Library/Safari/Downloads.plist"
}

clear_safari_frequently_visited_sites() {
    printf "%s\n" "Info: Clear Safari frequently visited sites"
    rm -f "${HOME}/Library/Safari/TopSites.plist"
}

clear_safari_last_session() {
    printf "%s\n" "Info: Clear Safari last session (open tabs) history"
    rm -f "${HOME}/Library/Safari/LastSession.plist"
}

clear_safari_history_copy() {
    printf "%s\n" "Info: Clear Safari history copy"
    rm -rfv "${HOME}/Library/Caches/Metadata/Safari/History"
}

clear_safari_search_history() {
    printf "%s\n" "Info: Clear search term history embedded in Safari preferences"
    defaults write "${HOME}/Library/Preferences/com.apple.Safari" RecentSearchStrings '( )'
}

clear_safari_cookies() {
    printf "%s\n" "Info: Clear Safari cookies"
    rm -f "${HOME}/Library/Cookies/Cookies.binarycookies"
    rm -f "${HOME}/Library/Cookies/Cookies.plist"
}

clear_safari_zoom_preferences() {
    printf "%s\n" "Info: Clear Safari zoom level preferences per site"
    rm -f "${HOME}/Library/Safari/PerSiteZoomPreferences.plist"
}

clear_safari_notification_preferences() {
    printf "%s\n" "Info: Clear allowed URLs for Safari notifications"
    rm -f "${HOME}/Library/Safari/UserNotificationPreferences.plist"
}

clear_safari_site_preferences() {
    printf "%s\n" "Info: Clear Safari preferences for downloads, geolocation, pop-ups, and autoplay per site"
    rm -f "${HOME}/Library/Safari/PerSitePreferences.db"
}

clear_firefox_cache() {
    printf "%s\n" "Info: Clear Firefox cache"
    sudo rm -rf "${HOME}/Library/Caches/Mozilla/"
    rm -fv "${HOME}/Library/Application Support/Firefox/Profiles/"*"/netpredictions.sqlite"
}

clear_firefox_form_history() {
    printf "%s\n" "Info: Clear Firefox form history"
    rm -fv "${HOME}/Library/Application Support/Firefox/Profiles/"*"/formhistory.sqlite"
    rm -fv "${HOME}/Library/Application Support/Firefox/Profiles/"*"/formhistory.dat"
}

clear_firefox_site_preferences() {
    printf "%s\n" "Info: Clear Firefox site preferences"
    rm -fv "${HOME}/Library/Application Support/Firefox/Profiles/"*"/content-prefs.sqlite"
}

clear_firefox_session_restore() {
    printf "%s\n" "Info: Clear Firefox session restore data (loads after the browser closes or crashes)"
    rm -fv "${HOME}/Library/Application Support/Firefox/Profiles/"*"/sessionCheckpoints.json"
    rm -fv "${HOME}/Library/Application Support/Firefox/Profiles/"*"/sessionstore"*".js"*
    rm -fv "${HOME}/Library/Application Support/Firefox/Profiles/"*"/sessionstore.bak"*
    rm -fv "${HOME}/Library/Application Support/Firefox/Profiles/"*"/sessionstore-backups/previous.js"*
    rm -fv "${HOME}/Library/Application Support/Firefox/Profiles/"*"/sessionstore-backups/recovery.js"*
    rm -fv "${HOME}/Library/Application Support/Firefox/Profiles/"*"/sessionstore-backups/recovery.bak"*
    rm -fv "${HOME}/Library/Application Support/Firefox/Profiles/"*"/sessionstore-backups/previous.bak"*
    rm -fv "${HOME}/Library/Application Support/Firefox/Profiles/"*"/sessionstore-backups/upgrade.js"*"-20"*
}

clear_firefox_passwords() {
    printf "%s\n" "Info: Clear Firefox passwords"
    rm -fv "${HOME}/Library/Application Support/Firefox/Profiles/"*"/signons.txt"
    rm -fv "${HOME}/Library/Application Support/Firefox/Profiles/"*"/signons2.txt"
    rm -fv "${HOME}/Library/Application Support/Firefox/Profiles/"*"/signons3.txt"
    rm -fv "${HOME}/Library/Application Support/Firefox/Profiles/"*"/signons.sqlite"
    rm -fv "${HOME}/Library/Application Support/Firefox/Profiles/"*"/logins.json"
}

clear_firefox_html5_cookies() {
    printf "%s\n" "Info: Clear Firefox HTML5 cookies"
    rm -fv "${HOME}/Library/Application Support/Firefox/Profiles/"*"/webappsstore.sqlite"
}

clear_firefox_crash_reports() {
    printf "%s\n" "Info: Clear Firefox crash reports"
    rm -rfv "${HOME}/Library/Application Support/Firefox/Crash Reports/"
    rm -fv "${HOME}/Library/Application Support/Firefox/Profiles/"*"/minidumps/"*".dmp"
}

clear_firefox_backup_files() {
    printf "%s\n" "Info: Clear Firefox backup files"
    rm -fv "${HOME}/Library/Application Support/Firefox/Profiles/"*"/bookmarkbackups/"*".json"
    rm -fv "${HOME}/Library/Application Support/Firefox/Profiles/"*"/bookmarkbackups/"*".jsonlz4"
}

clear_firefox_cookies() {
    printf "%s\n" "Info: Clear Firefox cookies"
    rm -fv "${HOME}/Library/Application Support/Firefox/Profiles/"*"/cookies.txt"
    rm -fv "${HOME}/Library/Application Support/Firefox/Profiles/"*"/cookies.sqlite"
    rm -fv "${HOME}/Library/Application Support/Firefox/Profiles/"*"/cookies.sqlite-shm"
    rm -fv "${HOME}/Library/Application Support/Firefox/Profiles/"*"/cookies.sqlite-wal"
    rm -rfv "${HOME}/Library/Application Support/Firefox/Profiles/"*"/storage/default/http"*
}

clear_privacy_sexy_history() {
    printf "%s\n" "Info: Clear privacy.sexy script history"
    rm -rfv "${HOME}/Library/Application Support/privacy.sexy/runs/"*
}

clear_adobe_cache() {
    printf "%s\n" "Info: Clear Adobe cache"
    sudo rm -rfv "${HOME}/Library/Application Support/Adobe/Common/Media Cache Files/"* &>/dev/null
}

clear_gradle_cache() {
    printf "%s\n" "Info: Clear Gradle cache"
    if [ -d "${HOME}/.gradle/caches" ]; then
        rm -rfv "${HOME}/.gradle/caches/" &> /dev/null
    fi
}

clear_dropbox_cache() {
    printf "%s\n" "Info: Clear Dropbox cache"
    if [ -d "${HOME}/Dropbox/.dropbox.cache" ]; then
        sudo rm -rfv "${HOME}/Dropbox/.dropbox.cache/"* &>/dev/null
    fi
}

clear_google_drive_cache() {
    printf "%s\n" "Info: Clear Google Drive File Stream cache"
    killall "Google Drive File Stream"
    rm -rfv "${HOME}/Library/Application Support/Google/DriveFS/[0-9a-zA-Z]*/content_cache" &>/dev/null
}

clear_composer_cache() {
    printf "%s\n" "Info: Clear Composer cache"
    if type "composer" &> /dev/null; then
        composer clearcache &> /dev/null
    fi
}

clear_homebrew_cache() {
    printf "%s\n" "Info: Clear Homebrew cache"
    if type "brew" &>/dev/null; then
        brew cleanup -s &>/dev/null
        rm -rfv "$(brew --cache)" &>/dev/null
        brew tap --repair &>/dev/null
    fi
}

clear_ruby_gem_cache() {
    printf "%s\n" "Info: Clear old Ruby gem versions"
    if type "gem" &> /dev/null; then
        gem cleanup &>/dev/null
    fi
}

clear_docker_cache() {
    printf "%s\n" "Info: Clear unused Docker data"
    if type "docker" &> /dev/null; then
        docker system prune -af
    fi
}

clear_pyenv_virtualenv_cache() {
    printf "%s\n" "Info: Clear Pyenv-Virtualenv cache"
    if [ "${PYENV_VIRTUALENV_CACHE_PATH}" ]; then
        rm -rfv "${PYENV_VIRTUALENV_CACHE_PATH}" &>/dev/null
    fi
}

clear_npm_cache() {
    printf "%s\n" "Info: Clear NPM cache"
    if type "npm" &> /dev/null; then
        npm cache clean --force
    fi
}

clear_yarn_cache() {
    printf "%s\n" "Info: Clear Yarn cache"
    if type "yarn" &> /dev/null; then
        printf "Cleanup Yarn Cache...\n"
        yarn cache clean --force
    fi
}

clear_ios_app_copies() {
    printf "%s\n" "Info: Clear iOS app copies from iTunes"
    rm -rfv "${HOME}/Music/iTunes/iTunes Media/Mobile Applications/"* &>/dev/null
}

clear_ios_photo_cache() {
    printf "%s\n" "Info: Clear iOS photo cache"
    rm -rf "${HOME}/Pictures/iPhoto Library/iPod Photo Cache/"*
}

clear_ios_device_backups() {
    printf "%s\n" "Info: Clear iOS Device Backups"
    rm -rfv "${HOME}/Library/Application Support/MobileSync/Backup/"* &>/dev/null
}

clear_ios_simulators() {
    printf "%s\n" "Info: Clear iOS simulators"
    if type "xcrun" &>/dev/null; then
        osascript -e 'tell application "com.apple.CoreSimulator.CoreSimulatorService" to quit'
        osascript -e 'tell application "iOS Simulator" to quit'
        osascript -e 'tell application "Simulator" to quit'
        xcrun simctl shutdown all
        xcrun simctl erase all
    fi
}

clear_connected_ios_devices() {
    printf "%s\n" "Info: Clear list of connected iOS devices"
    sudo defaults delete "/Users/${USER}/Library/Preferences/com.apple.iPod.plist" "conn:128:Last Connect"
    sudo defaults delete "/Users/${USER}/Library/Preferences/com.apple.iPod.plist" Devices
    sudo defaults delete "/Library/Preferences/com.apple.iPod.plist" "conn:128:Last Connect"
    sudo defaults delete "/Library/Preferences/com.apple.iPod.plist" Devices
    sudo rm -rfv /var/db/lockdown/*
}

reset_permissions() {
    local serviceId="$1"
    printf "Info: Clear \"%s\" permissions\n" "${serviceId}"
    if ! command -v 'tccutil' &> /dev/null; then
        printf "Skipping because \"tccutil\" is not found.\n"
    else
        local reset_output reset_exit_code
        {
            reset_output=$(tccutil reset "${serviceId}" 2>&1)
            reset_exit_code=$?
        }
        if [ ${reset_exit_code} -eq 0 ]; then
            printf "Successfully reset permissions for \"%s\".\n" "${serviceId}"
        elif [ ${reset_exit_code} -eq 70 ]; then
            printf "Skipping, service ID \"%s\" is not supported on your operating system version.\n" "${serviceId}"
        elif [ ${reset_exit_code} -ne 0 ]; then
            printf >&2 "Failed to reset permissions for \"%s\". Exit code: %d.\n" "${serviceId}" "${reset_exit_code}"
            if [ -n "${reset_output}" ]; then
                printf "Output from \`tccutil\`: %s.\n" "${reset_output}"
            fi
        fi
    fi
}

# ******************
if [ "${clear_bash_history}" = "yes" ]; then
    clear_bash_history
else
    printf "%s\n" "The bash history will not be cleared."
fi

if [ "${clear_zsh_history}" = "yes" ]; then
    clear_zsh_history
else
    printf "%s\n" "The zsh history will not be cleared."
fi

if [ "${clear_diagnostic_logs}" = "yes" ]; then
    clear_diagnostic_logs
else
    printf "%s\n" "The diagnostic logs will not be cleared."
fi
if [ "${clear_diagnostic_log_details}" = "yes" ]; then
    clear_diagnostic_log_details
else
    printf "%s\n" "Diagnostic log details will not be cleared."
fi

if [ "${clear_apple_system_logs}" = "yes" ]; then
    clear_apple_system_logs
else
    printf "%s\n" "Apple System Logs will not be cleared."
fi

if [ "${clear_installation_logs}" = "yes" ]; then
    clear_installation_logs
else
    printf "%s\n" "Installation logs will not be cleared."
fi

if [ "${clear_all_system_logs}" = "yes" ]; then
    clear_all_system_logs
else
    printf "%s\n" "All system logs will not be cleared."
fi

if [ "${clear_system_application_logs}" = "yes" ]; then
    clear_system_application_logs
else
    printf "%s\n" "System application logs will not be cleared."
fi

if [ "${clear_user_application_logs}" = "yes" ]; then
    clear_user_application_logs
else
    printf "%s\n" "User application logs will not be cleared."
fi

if [ "${clear_mail_app_logs}" = "yes" ]; then
    clear_mail_app_logs
else
    printf "%s\n" "Mail app logs will not be cleared."
fi

if [ "${clear_user_activity_audit_logs}" = "yes" ]; then
    clear_user_activity_audit_logs
else
    printf "%s\n" "User activity audit logs will not be cleared."
fi

if [ "${clear_system_maintenance_logs}" = "yes" ]; then
    clear_system_maintenance_logs
else
    printf "%s\n" "System maintenance logs will not be cleared."
fi

if [ "${clear_app_installation_logs}" = "yes" ]; then
    clear_app_installation_logs
else
    printf "%s\n" "App installation logs will not be cleared."
fi

if [ "${clear_chrome_browsing_history}" = "yes" ]; then
    clear_chrome_browsing_history
else
    printf "%s\n" "Chrome browsing history will not be cleared."
fi

if [ "${clear_chrome_cache}" = "yes" ]; then
    clear_chrome_cache
else
    printf "%s\n" "Chrome cache will not be cleared."
fi

if [ "${clear_safari_cache}" = "yes" ]; then
    clear_safari_cache
else
    printf "%s\n" "Safari cache will not be cleared."
fi

if [ "${clear_safari_url_icons}" = "yes" ]; then
    clear_safari_url_icons
else
    printf "%s\n" "Safari URL bar web page icons will not be cleared."
fi

if [ "${clear_safari_webpage_previews}" = "yes" ]; then
    clear_safari_webpage_previews
else
    printf "%s\n" "Safari webpage previews will not be cleared."
fi

if [ "${clear_safari_browsing_history}" = "yes" ]; then
    clear_safari_browsing_history
else
    printf "%s\n" "Safari browsing history will not be cleared."
fi

if [ "${clear_safari_downloads_history}" = "yes" ]; then
    clear_safari_downloads_history
else
    printf "%s\n" "Safari downloads history will not be cleared."
fi

if [ "${clear_safari_frequently_visited_sites}" = "yes" ]; then
    clear_safari_frequently_visited_sites
else
    printf "%s\n" "Safari frequently visited sites will not be cleared."
fi

if [ "${clear_safari_last_session}" = "yes" ]; then
    clear_safari_last_session
else
    printf "%s\n" "Safari last session history will not be cleared."
fi

if [ "${clear_safari_history_copy}" = "yes" ]; then
    clear_safari_history_copy
else
    printf "%s\n" "Safari history copy will not be cleared."
fi

if [ "${clear_safari_search_history}" = "yes" ]; then
    clear_safari_search_history
else
    printf "%s\n" "Safari search history will not be cleared."
fi

if [ "${clear_safari_cookies}" = "yes" ]; then
    clear_safari_cookies
else
    printf "%s\n" "Safari cookies will not be cleared."
fi

if [ "${clear_safari_zoom_preferences}" = "yes" ]; then
    clear_safari_zoom_preferences
else
    printf "%s\n" "Safari zoom preferences will not be cleared."
fi

if [ "${clear_safari_notification_preferences}" = "yes" ]; then
    clear_safari_notification_preferences
else
    printf "%s\n" "Safari notification preferences will not be cleared."
fi

if [ "${clear_safari_site_preferences}" = "yes" ]; then
    clear_safari_site_preferences
else
    printf "%s\n" "Safari site preferences will not be cleared."
fi

if [ "${clear_firefox_cache}" = "yes" ]; then
    clear_firefox_cache
else
    printf "%s\n" "Firefox cache will not be cleared."
fi

if [ "${clear_firefox_form_history}" = "yes" ]; then
    clear_firefox_form_history
else
    printf "%s\n" "Firefox form history will not be cleared."
fi

if [ "${clear_firefox_site_preferences}" = "yes" ]; then
    clear_firefox_site_preferences
else
    printf "%s\n" "Firefox site preferences will not be cleared."
fi

if [ "${clear_firefox_session_restore}" = "yes" ]; then
    clear_firefox_session_restore
else
    printf "%s\n" "Firefox session restore data will not be cleared."
fi

if [ "${clear_firefox_passwords}" = "yes" ]; then
    clear_firefox_passwords
else
    printf "%s\n" "Firefox passwords will not be cleared."
fi

if [ "${clear_firefox_html5_cookies}" = "yes" ]; then
    clear_firefox_html5_cookies
else
    printf "%s\n" "Firefox HTML5 cookies will not be cleared."
fi

if [ "${clear_firefox_crash_reports}" = "yes" ]; then
    clear_firefox_crash_reports
else
    printf "%s\n" "Firefox crash reports will not be cleared."
fi

if [ "${clear_firefox_backup_files}" = "yes" ]; then
    clear_firefox_backup_files
else
    printf "%s\n" "Firefox backup files will not be cleared."
fi

if [ "${clear_firefox_cookies}" = "yes" ]; then
    clear_firefox_cookies
else
    printf "%s\n" "Firefox cookies will not be cleared."
fi

if [ "${clear_privacy_sexy_history}" = "yes" ]; then
    clear_privacy_sexy_history
else
    printf "%s\n" "privacy.sexy script history will not be cleared."
fi

if [ "${clear_adobe_cache}" = "yes" ]; then
    clear_adobe_cache
else
    printf "%s\n" "Adobe cache will not be cleared."
fi

if [ "${clear_gradle_cache}" = "yes" ]; then
    clear_gradle_cache
else
    printf "%s\n" "Gradle cache will not be cleared."
fi

if [ "${clear_dropbox_cache}" = "yes" ]; then
    clear_dropbox_cache
else
    printf "%s\n" "Dropbox cache will not be cleared."
fi

if [ "${clear_google_drive_cache}" = "yes" ]; then
    clear_google_drive_cache
else
    printf "%s\n" "Google Drive File Stream cache will not be cleared."
fi

if [ "${clear_composer_cache}" = "yes" ]; then
    clear_composer_cache
else
    printf "%s\n" "Composer cache will not be cleared."
fi

if [ "${clear_homebrew_cache}" = "yes" ]; then
    clear_homebrew_cache
else
    printf "%s\n" "Homebrew cache will not be cleared."
fi

if [ "${clear_ruby_gem_cache}" = "yes" ]; then
    clear_ruby_gem_cache
else
    printf "%s\n" "Old Ruby gem versions will not be cleared."
fi

if [ "${clear_docker_cache}" = "yes" ]; then
    clear_docker_cache
else
    printf "%s\n" "Unused Docker data will not be cleared."
fi

if [ "${clear_pyenv_virtualenv_cache}" = "yes" ]; then
    clear_pyenv_virtualenv_cache
else
    printf "%s\n" "Pyenv-Virtualenv cache will not be cleared."
fi

if [ "${clear_npm_cache}" = "yes" ]; then
    clear_npm_cache
else
    printf "%s\n" "NPM cache will not be cleared."
fi

if [ "${clear_yarn_cache}" = "yes" ]; then
    clear_yarn_cache
else
    printf "%s\n" "Yarn cache will not be cleared."
fi

if [ "${clear_ios_app_copies}" = "yes" ]; then
    clear_ios_app_copies
else
    printf "%s\n" "iOS app copies from iTunes will not be cleared."
fi

if [ "${clear_ios_photo_cache}" = "yes" ]; then
    clear_ios_photo_cache
else
    printf "%s\n" "iOS photo cache will not be cleared."
fi

if [ "${clear_ios_device_backups}" = "yes" ]; then
    clear_ios_device_backups
else
    printf "%s\n" "iOS Device Backups will not be cleared."
fi

if [ "${clear_ios_simulators}" = "yes" ]; then
    clear_ios_simulators
else
    printf "%s\n" "iOS simulators will not be cleared."
fi

if [ "${clear_connected_ios_devices}" = "yes" ]; then
    clear_connected_ios_devices
else
    printf "%s\n" "List of connected iOS devices will not be cleared."
fi

if [ "${reset_all_permissions}" = "yes" ]; then
    reset_permissions "All"
else
    printf "%s\n" "All permissions will not be reset."
fi

if [ "${reset_camera_permissions}" = "yes" ]; then
    reset_permissions "Camera"
else
    printf "%s\n" "Camera permissions will not be reset."
fi

if [ "${reset_microphone_permissions}" = "yes" ]; then
    reset_permissions "Microphone"
else
    printf "%s\n" "Microphone permissions will not be reset."
fi

# ----------------------------------------------------------
# ------------Clear "Accessibility" permissions-------------
# ----------------------------------------------------------
printf "Info: Clear \"Accessibility\" permissions\n"
if ! command -v 'tccutil' &> /dev/null; then
    printf "Skipping because \"tccutil\" is not found.\n"
else
    declare serviceId='Accessibility'
declare reset_output reset_exit_code
{
    reset_output=$(tccutil reset "${serviceId}" 2>&1)
    reset_exit_code=$?
}
if [ ${reset_exit_code} -eq 0 ]; then
    printf "Successfully reset permissions for \"%s\".\n" "${serviceId}"
elif [ ${reset_exit_code} -eq 70 ]; then
    printf "Skipping, service ID \"%s\" is not supported on your operating system version.\n" "${serviceId}"
elif [ ${reset_exit_code} -ne 0 ]; then
    printf >&2 "Failed to reset permissions for \"%s\". Exit code: %d.\n" "${serviceId}" "${reset_exit_code}"
    if [ -n "${reset_output}" ]; then
        printf "Output from \`tccutil\`: %s.\n" "${reset_output}"
    fi
fi
fi
# ----------------------------------------------------------


# ----------------------------------------------------------
# ------------Clear "Screen Capture" permissions------------
# ----------------------------------------------------------
printf "Info: Clear \"Screen Capture\" permissions\n"
if ! command -v 'tccutil' &> /dev/null; then
    printf "Skipping because \"tccutil\" is not found.\n"
else
    declare serviceId='ScreenCapture'
declare reset_output reset_exit_code
{
    reset_output=$(tccutil reset "${serviceId}" 2>&1)
    reset_exit_code=$?
}
if [ ${reset_exit_code} -eq 0 ]; then
    printf "Successfully reset permissions for \"%s\".\n" "${serviceId}"
elif [ ${reset_exit_code} -eq 70 ]; then
    printf "Skipping, service ID \"%s\" is not supported on your operating system version.\n" "${serviceId}"
elif [ ${reset_exit_code} -ne 0 ]; then
    printf >&2 "Failed to reset permissions for \"%s\". Exit code: %d.\n" "${serviceId}" "${reset_exit_code}"
    if [ -n "${reset_output}" ]; then
        printf "Output from \`tccutil\`: %s.\n" "${reset_output}"
    fi
fi
fi
# ----------------------------------------------------------


# ----------------------------------------------------------
# --------------Clear "Reminders" permissions---------------
# ----------------------------------------------------------
printf "Info: Clear \"Reminders\" permissions\n"
if ! command -v 'tccutil' &> /dev/null; then
    printf "Skipping because \"tccutil\" is not found.\n"
else
    declare serviceId='Reminders'
declare reset_output reset_exit_code
{
    reset_output=$(tccutil reset "${serviceId}" 2>&1)
    reset_exit_code=$?
}
if [ ${reset_exit_code} -eq 0 ]; then
    printf "Successfully reset permissions for \"%s\".\n" "${serviceId}"
elif [ ${reset_exit_code} -eq 70 ]; then
    printf "Skipping, service ID \"%s\" is not supported on your operating system version.\n" "${serviceId}"
elif [ ${reset_exit_code} -ne 0 ]; then
    printf >&2 "Failed to reset permissions for \"%s\". Exit code: %d.\n" "${serviceId}" "${reset_exit_code}"
    if [ -n "${reset_output}" ]; then
        printf "Output from \`tccutil\`: %s.\n" "${reset_output}"
    fi
fi
fi
# ----------------------------------------------------------


# ----------------------------------------------------------
# ----------------Clear "Photos" permissions----------------
# ----------------------------------------------------------
printf "Info: Clear \"Photos\" permissions\n"
if ! command -v 'tccutil' &> /dev/null; then
    printf "Skipping because \"tccutil\" is not found.\n"
else
    declare serviceId='Photos'
declare reset_output reset_exit_code
{
    reset_output=$(tccutil reset "${serviceId}" 2>&1)
    reset_exit_code=$?
}
if [ ${reset_exit_code} -eq 0 ]; then
    printf "Successfully reset permissions for \"%s\".\n" "${serviceId}"
elif [ ${reset_exit_code} -eq 70 ]; then
    printf "Skipping, service ID \"%s\" is not supported on your operating system version.\n" "${serviceId}"
elif [ ${reset_exit_code} -ne 0 ]; then
    printf >&2 "Failed to reset permissions for \"%s\". Exit code: %d.\n" "${serviceId}" "${reset_exit_code}"
    if [ -n "${reset_output}" ]; then
        printf "Output from \`tccutil\`: %s.\n" "${reset_output}"
    fi
fi
fi
# ----------------------------------------------------------


# ----------------------------------------------------------
# ---------------Clear "Calendar" permissions---------------
# ----------------------------------------------------------
printf "Info: Clear \"Calendar\" permissions\n"
if ! command -v 'tccutil' &> /dev/null; then
    printf "Skipping because \"tccutil\" is not found.\n"
else
    declare serviceId='Calendar'
declare reset_output reset_exit_code
{
    reset_output=$(tccutil reset "${serviceId}" 2>&1)
    reset_exit_code=$?
}
if [ ${reset_exit_code} -eq 0 ]; then
    printf "Successfully reset permissions for \"%s\".\n" "${serviceId}"
elif [ ${reset_exit_code} -eq 70 ]; then
    printf "Skipping, service ID \"%s\" is not supported on your operating system version.\n" "${serviceId}"
elif [ ${reset_exit_code} -ne 0 ]; then
    printf >&2 "Failed to reset permissions for \"%s\". Exit code: %d.\n" "${serviceId}" "${reset_exit_code}"
    if [ -n "${reset_output}" ]; then
        printf "Output from \`tccutil\`: %s.\n" "${reset_output}"
    fi
fi
fi
# ----------------------------------------------------------


# ----------------------------------------------------------
# -----------Clear "Full Disk Access" permissions-----------
# ----------------------------------------------------------
printf "Info: Clear \"Full Disk Access\" permissions\n"
if ! command -v 'tccutil' &> /dev/null; then
    printf "Skipping because \"tccutil\" is not found.\n"
else
    declare serviceId='SystemPolicyAllFiles'
declare reset_output reset_exit_code
{
    reset_output=$(tccutil reset "${serviceId}" 2>&1)
    reset_exit_code=$?
}
if [ ${reset_exit_code} -eq 0 ]; then
    printf "Successfully reset permissions for \"%s\".\n" "${serviceId}"
elif [ ${reset_exit_code} -eq 70 ]; then
    printf "Skipping, service ID \"%s\" is not supported on your operating system version.\n" "${serviceId}"
elif [ ${reset_exit_code} -ne 0 ]; then
    printf >&2 "Failed to reset permissions for \"%s\". Exit code: %d.\n" "${serviceId}" "${reset_exit_code}"
    if [ -n "${reset_output}" ]; then
        printf "Output from \`tccutil\`: %s.\n" "${reset_output}"
    fi
fi
fi
# ----------------------------------------------------------


# ----------------------------------------------------------
# ---------------Clear "Contacts" permissions---------------
# ----------------------------------------------------------
printf "Info: Clear \"Contacts\" permissions\n"
if ! command -v 'tccutil' &> /dev/null; then
    printf "Skipping because \"tccutil\" is not found.\n"
else
    declare serviceId='AddressBook'
declare reset_output reset_exit_code
{
    reset_output=$(tccutil reset "${serviceId}" 2>&1)
    reset_exit_code=$?
}
if [ ${reset_exit_code} -eq 0 ]; then
    printf "Successfully reset permissions for \"%s\".\n" "${serviceId}"
elif [ ${reset_exit_code} -eq 70 ]; then
    printf "Skipping, service ID \"%s\" is not supported on your operating system version.\n" "${serviceId}"
elif [ ${reset_exit_code} -ne 0 ]; then
    printf >&2 "Failed to reset permissions for \"%s\". Exit code: %d.\n" "${serviceId}" "${reset_exit_code}"
    if [ -n "${reset_output}" ]; then
        printf "Output from \`tccutil\`: %s.\n" "${reset_output}"
    fi
fi
fi
# ----------------------------------------------------------

# ----------------------------------------------------------
# ------------Clear "Desktop Folder" permissions------------
# ----------------------------------------------------------
printf "Info: Clear \"Desktop Folder\" permissions\n"
if ! command -v 'tccutil' &> /dev/null; then
    printf "Skipping because \"tccutil\" is not found.\n"
else
    declare serviceId='SystemPolicyDesktopFolder'
declare reset_output reset_exit_code
{
    reset_output=$(tccutil reset "${serviceId}" 2>&1)
    reset_exit_code=$?
}
if [ ${reset_exit_code} -eq 0 ]; then
    printf "Successfully reset permissions for \"%s\".\n" "${serviceId}"
elif [ ${reset_exit_code} -eq 70 ]; then
    printf "Skipping, service ID \"%s\" is not supported on your operating system version.\n" "${serviceId}"
elif [ ${reset_exit_code} -ne 0 ]; then
    printf >&2 "Failed to reset permissions for \"%s\". Exit code: %d.\n" "${serviceId}" "${reset_exit_code}"
    if [ -n "${reset_output}" ]; then
        printf "Output from \`tccutil\`: %s.\n" "${reset_output}"
    fi
fi
fi
# ----------------------------------------------------------


# ----------------------------------------------------------
# -----------Clear "Documents Folder" permissions-----------
# ----------------------------------------------------------
printf "Info: Clear \"Documents Folder\" permissions\n"
if ! command -v 'tccutil' &> /dev/null; then
    printf "Skipping because \"tccutil\" is not found.\n"
else
    declare serviceId='SystemPolicyDocumentsFolder'
declare reset_output reset_exit_code
{
    reset_output=$(tccutil reset "${serviceId}" 2>&1)
    reset_exit_code=$?
}
if [ ${reset_exit_code} -eq 0 ]; then
    printf "Successfully reset permissions for \"%s\".\n" "${serviceId}"
elif [ ${reset_exit_code} -eq 70 ]; then
    printf "Skipping, service ID \"%s\" is not supported on your operating system version.\n" "${serviceId}"
elif [ ${reset_exit_code} -ne 0 ]; then
    printf >&2 "Failed to reset permissions for \"%s\". Exit code: %d.\n" "${serviceId}" "${reset_exit_code}"
    if [ -n "${reset_output}" ]; then
        printf "Output from \`tccutil\`: %s.\n" "${reset_output}"
    fi
fi
fi
# ----------------------------------------------------------


# ----------------------------------------------------------
# -----------Clear "Downloads Folder" permissions-----------
# ----------------------------------------------------------
printf "Info: Clear \"Downloads Folder\" permissions\n"
if ! command -v 'tccutil' &> /dev/null; then
    printf "Skipping because \"tccutil\" is not found.\n"
else
    declare serviceId='SystemPolicyDownloadsFolder'
declare reset_output reset_exit_code
{
    reset_output=$(tccutil reset "${serviceId}" 2>&1)
    reset_exit_code=$?
}
if [ ${reset_exit_code} -eq 0 ]; then
    printf "Successfully reset permissions for \"%s\".\n" "${serviceId}"
elif [ ${reset_exit_code} -eq 70 ]; then
    printf "Skipping, service ID \"%s\" is not supported on your operating system version.\n" "${serviceId}"
elif [ ${reset_exit_code} -ne 0 ]; then
    printf >&2 "Failed to reset permissions for \"%s\". Exit code: %d.\n" "${serviceId}" "${reset_exit_code}"
    if [ -n "${reset_output}" ]; then
        printf "Output from \`tccutil\`: %s.\n" "${reset_output}"
    fi
fi
fi
# ----------------------------------------------------------


# ----------------------------------------------------------
# -------------Clear "Apple Events" permissions-------------
# ----------------------------------------------------------
printf "Info: Clear \"Apple Events\" permissions\n"
if ! command -v 'tccutil' &> /dev/null; then
    printf "Skipping because \"tccutil\" is not found.\n"
else
    declare serviceId='AppleEvents'
declare reset_output reset_exit_code
{
    reset_output=$(tccutil reset "${serviceId}" 2>&1)
    reset_exit_code=$?
}
if [ ${reset_exit_code} -eq 0 ]; then
    printf "Successfully reset permissions for \"%s\".\n" "${serviceId}"
elif [ ${reset_exit_code} -eq 70 ]; then
    printf "Skipping, service ID \"%s\" is not supported on your operating system version.\n" "${serviceId}"
elif [ ${reset_exit_code} -ne 0 ]; then
    printf >&2 "Failed to reset permissions for \"%s\". Exit code: %d.\n" "${serviceId}" "${reset_exit_code}"
    if [ -n "${reset_output}" ]; then
        printf "Output from \`tccutil\`: %s.\n" "${reset_output}"
    fi
fi
fi
# ----------------------------------------------------------


# ----------------------------------------------------------
# --------Clear "File Provider Presence" permissions--------
# ----------------------------------------------------------
printf "Info: Clear \"File Provider Presence\" permissions\n"
if ! command -v 'tccutil' &> /dev/null; then
    printf "Skipping because \"tccutil\" is not found.\n"
else
    declare serviceId='FileProviderPresence'
declare reset_output reset_exit_code
{
    reset_output=$(tccutil reset "${serviceId}" 2>&1)
    reset_exit_code=$?
}
if [ ${reset_exit_code} -eq 0 ]; then
    printf "Successfully reset permissions for \"%s\".\n" "${serviceId}"
elif [ ${reset_exit_code} -eq 70 ]; then
    printf "Skipping, service ID \"%s\" is not supported on your operating system version.\n" "${serviceId}"
elif [ ${reset_exit_code} -ne 0 ]; then
    printf >&2 "Failed to reset permissions for \"%s\". Exit code: %d.\n" "${serviceId}" "${reset_exit_code}"
    if [ -n "${reset_output}" ]; then
        printf "Output from \`tccutil\`: %s.\n" "${reset_output}"
    fi
fi
fi
# ----------------------------------------------------------


# ----------------------------------------------------------
# ------------Clear "Listen Events" permissions-------------
# ----------------------------------------------------------
printf "Info: Clear \"Listen Events\" permissions\n"
if ! command -v 'tccutil' &> /dev/null; then
    printf "Skipping because \"tccutil\" is not found.\n"
else
    declare serviceId='ListenEvent'
declare reset_output reset_exit_code
{
    reset_output=$(tccutil reset "${serviceId}" 2>&1)
    reset_exit_code=$?
}
if [ ${reset_exit_code} -eq 0 ]; then
    printf "Successfully reset permissions for \"%s\".\n" "${serviceId}"
elif [ ${reset_exit_code} -eq 70 ]; then
    printf "Skipping, service ID \"%s\" is not supported on your operating system version.\n" "${serviceId}"
elif [ ${reset_exit_code} -ne 0 ]; then
    printf >&2 "Failed to reset permissions for \"%s\". Exit code: %d.\n" "${serviceId}" "${reset_exit_code}"
    if [ -n "${reset_output}" ]; then
        printf "Output from \`tccutil\`: %s.\n" "${reset_output}"
    fi
fi
fi
# ----------------------------------------------------------

# ----------------------------------------------------------
# ------------Clear "Media Library" permissions-------------
# ----------------------------------------------------------
printf "Info: Clear \"Media Library\" permissions\n"
if ! command -v 'tccutil' &> /dev/null; then
    printf "Skipping because \"tccutil\" is not found.\n"
else
    declare serviceId='MediaLibrary'
declare reset_output reset_exit_code
{
    reset_output=$(tccutil reset "${serviceId}" 2>&1)
    reset_exit_code=$?
}
if [ ${reset_exit_code} -eq 0 ]; then
    printf "Successfully reset permissions for \"%s\".\n" "${serviceId}"
elif [ ${reset_exit_code} -eq 70 ]; then
    printf "Skipping, service ID \"%s\" is not supported on your operating system version.\n" "${serviceId}"
elif [ ${reset_exit_code} -ne 0 ]; then
    printf >&2 "Failed to reset permissions for \"%s\". Exit code: %d.\n" "${serviceId}" "${reset_exit_code}"
    if [ -n "${reset_output}" ]; then
        printf "Output from \`tccutil\`: %s.\n" "${reset_output}"
    fi
fi
fi
# ----------------------------------------------------------


# ----------------------------------------------------------
# --------------Clear "Post Event" permissions--------------
# ----------------------------------------------------------
printf "Info: Clear \"Post Event\" permissions\n"
if ! command -v 'tccutil' &> /dev/null; then
    printf "Skipping because \"tccutil\" is not found.\n"
else
    declare serviceId='PostEvent'
declare reset_output reset_exit_code
{
    reset_output=$(tccutil reset "${serviceId}" 2>&1)
    reset_exit_code=$?
}
if [ ${reset_exit_code} -eq 0 ]; then
    printf "Successfully reset permissions for \"%s\".\n" "${serviceId}"
elif [ ${reset_exit_code} -eq 70 ]; then
    printf "Skipping, service ID \"%s\" is not supported on your operating system version.\n" "${serviceId}"
elif [ ${reset_exit_code} -ne 0 ]; then
    printf >&2 "Failed to reset permissions for \"%s\". Exit code: %d.\n" "${serviceId}" "${reset_exit_code}"
    if [ -n "${reset_output}" ]; then
        printf "Output from \`tccutil\`: %s.\n" "${reset_output}"
    fi
fi
fi
# ----------------------------------------------------------


# ----------------------------------------------------------
# ----------Clear "Speech Recognition" permissions----------
# ----------------------------------------------------------
printf "Info: Clear \"Speech Recognition\" permissions\n"
if ! command -v 'tccutil' &> /dev/null; then
    printf "Skipping because \"tccutil\" is not found.\n"
else
    declare serviceId='SpeechRecognition'
declare reset_output reset_exit_code
{
    reset_output=$(tccutil reset "${serviceId}" 2>&1)
    reset_exit_code=$?
}
if [ ${reset_exit_code} -eq 0 ]; then
    printf "Successfully reset permissions for \"%s\".\n" "${serviceId}"
elif [ ${reset_exit_code} -eq 70 ]; then
    printf "Skipping, service ID \"%s\" is not supported on your operating system version.\n" "${serviceId}"
elif [ ${reset_exit_code} -ne 0 ]; then
    printf >&2 "Failed to reset permissions for \"%s\". Exit code: %d.\n" "${serviceId}" "${reset_exit_code}"
    if [ -n "${reset_output}" ]; then
        printf "Output from \`tccutil\`: %s.\n" "${reset_output}"
    fi
fi
fi
# ----------------------------------------------------------


# ----------------------------------------------------------
# -----------Clear "App Modification" permissions-----------
# ----------------------------------------------------------
printf "Info: Clear \"App Modification\" permissions\n"
if ! command -v 'tccutil' &> /dev/null; then
    printf "Skipping because \"tccutil\" is not found.\n"
else
    declare serviceId='SystemPolicyAppBundles'
declare reset_output reset_exit_code
{
    reset_output=$(tccutil reset "${serviceId}" 2>&1)
    reset_exit_code=$?
}
if [ ${reset_exit_code} -eq 0 ]; then
    printf "Successfully reset permissions for \"%s\".\n" "${serviceId}"
elif [ ${reset_exit_code} -eq 70 ]; then
    printf "Skipping, service ID \"%s\" is not supported on your operating system version.\n" "${serviceId}"
elif [ ${reset_exit_code} -ne 0 ]; then
    printf >&2 "Failed to reset permissions for \"%s\". Exit code: %d.\n" "${serviceId}" "${reset_exit_code}"
    if [ -n "${reset_output}" ]; then
        printf "Output from \`tccutil\`: %s.\n" "${reset_output}"
    fi
fi
fi
# ----------------------------------------------------------

# ----------------------------------------------------------
# -----------Clear "Application Data" permissions-----------
# ----------------------------------------------------------
printf "Info: Clear \"Application Data\" permissions\n"
if ! command -v 'tccutil' &> /dev/null; then
    printf "Skipping because \"tccutil\" is not found.\n"
else
    declare serviceId='SystemPolicyAppData'
declare reset_output reset_exit_code
{
    reset_output=$(tccutil reset "${serviceId}" 2>&1)
    reset_exit_code=$?
}
if [ ${reset_exit_code} -eq 0 ]; then
    printf "Successfully reset permissions for \"%s\".\n" "${serviceId}"
elif [ ${reset_exit_code} -eq 70 ]; then
    printf "Skipping, service ID \"%s\" is not supported on your operating system version.\n" "${serviceId}"
elif [ ${reset_exit_code} -ne 0 ]; then
    printf >&2 "Failed to reset permissions for \"%s\". Exit code: %d.\n" "${serviceId}" "${reset_exit_code}"
    if [ -n "${reset_output}" ]; then
        printf "Output from \`tccutil\`: %s.\n" "${reset_output}"
    fi
fi
fi
# ----------------------------------------------------------


# ----------------------------------------------------------
# -----------Clear "Network Volumes" permissions------------
# ----------------------------------------------------------
printf "Info: Clear \"Network Volumes\" permissions\n"
if ! command -v 'tccutil' &> /dev/null; then
    printf "Skipping because \"tccutil\" is not found.\n"
else
    declare serviceId='SystemPolicyNetworkVolumes'
declare reset_output reset_exit_code
{
    reset_output=$(tccutil reset "${serviceId}" 2>&1)
    reset_exit_code=$?
}
if [ ${reset_exit_code} -eq 0 ]; then
    printf "Successfully reset permissions for \"%s\".\n" "${serviceId}"
elif [ ${reset_exit_code} -eq 70 ]; then
    printf "Skipping, service ID \"%s\" is not supported on your operating system version.\n" "${serviceId}"
elif [ ${reset_exit_code} -ne 0 ]; then
    printf >&2 "Failed to reset permissions for \"%s\". Exit code: %d.\n" "${serviceId}" "${reset_exit_code}"
    if [ -n "${reset_output}" ]; then
        printf "Output from \`tccutil\`: %s.\n" "${reset_output}"
    fi
fi
fi
# ----------------------------------------------------------


# ----------------------------------------------------------
# ----------Clear "Removable Volumes" permissions-----------
# ----------------------------------------------------------
printf "Info: Clear \"Removable Volumes\" permissions\n"
if ! command -v 'tccutil' &> /dev/null; then
    printf "Skipping because \"tccutil\" is not found.\n"
else
    declare serviceId='SystemPolicyRemovableVolumes'
declare reset_output reset_exit_code
{
    reset_output=$(tccutil reset "${serviceId}" 2>&1)
    reset_exit_code=$?
}
if [ ${reset_exit_code} -eq 0 ]; then
    printf "Successfully reset permissions for \"%s\".\n" "${serviceId}"
elif [ ${reset_exit_code} -eq 70 ]; then
    printf "Skipping, service ID \"%s\" is not supported on your operating system version.\n" "${serviceId}"
elif [ ${reset_exit_code} -ne 0 ]; then
    printf >&2 "Failed to reset permissions for \"%s\". Exit code: %d.\n" "${serviceId}" "${reset_exit_code}"
    if [ -n "${reset_output}" ]; then
        printf "Output from \`tccutil\`: %s.\n" "${reset_output}"
    fi
fi
fi
# ----------------------------------------------------------


# ----------------------------------------------------------
# -----Clear "System Administration Files" permissions------
# ----------------------------------------------------------
printf "Info: Clear \"System Administration Files\" permissions\n"
if ! command -v 'tccutil' &> /dev/null; then
    printf "Skipping because \"tccutil\" is not found.\n"
else
    declare serviceId='SystemPolicySysAdminFiles'
declare reset_output reset_exit_code
{
    reset_output=$(tccutil reset "${serviceId}" 2>&1)
    reset_exit_code=$?
}
if [ ${reset_exit_code} -eq 0 ]; then
    printf "Successfully reset permissions for \"%s\".\n" "${serviceId}"
elif [ ${reset_exit_code} -eq 70 ]; then
    printf "Skipping, service ID \"%s\" is not supported on your operating system version.\n" "${serviceId}"
elif [ ${reset_exit_code} -ne 0 ]; then
    printf >&2 "Failed to reset permissions for \"%s\". Exit code: %d.\n" "${serviceId}" "${reset_exit_code}"
    if [ -n "${reset_output}" ]; then
        printf "Output from \`tccutil\`: %s.\n" "${reset_output}"
    fi
fi
fi
# ----------------------------------------------------------

# ----------------------------------------------------------
# ---------------Clear CUPS printer job cache---------------
# ----------------------------------------------------------
printf "Info: Clear CUPS printer job cache\n"
sudo rm -rfv "/var/spool/cups/c0"*
sudo rm -rfv "/var/spool/cups/tmp/"*
sudo rm -rfv "/var/spool/cups/cache/job.cache"*
# ----------------------------------------------------------


# ----------------------------------------------------------
# ----------------Empty trash on all volumes----------------
# ----------------------------------------------------------
printf "Info: Empty trash on all volumes\n"
# on all mounted volumes
sudo rm -rfv "/Volumes/"*"/.Trashes/"* &>/dev/null
# on main HDD
sudo rm -rfv "${HOME}/.Trash/"* &>/dev/null
# ----------------------------------------------------------


# ----------------------------------------------------------
# --------------------Clear system cache--------------------
# ----------------------------------------------------------
printf "Info: Clear system cache\n"
sudo rm -rfv "/Library/Caches/"* &>/dev/null
sudo rm -rfv "/System/Library/Caches/"* &>/dev/null
sudo rm -rfv "${HOME}/Library/Caches/"* &>/dev/null
# ----------------------------------------------------------


# ----------------------------------------------------------
# ---------Clear Xcode's derived data and archives----------
# ----------------------------------------------------------
printf "Info: Clear Xcode's derived data and archives\n"
rm -rfv "${HOME}/Library/Developer/Xcode/DerivedData/"* &>/dev/null
rm -rfv "${HOME}/Library/Developer/Xcode/Archives/"* &>/dev/null
rm -rfv "${HOME}/Library/Developer/Xcode/iOS Device Logs/"* &>/dev/null
# ----------------------------------------------------------


# ----------------------------------------------------------
# ---------------------Clear DNS cache----------------------
# ----------------------------------------------------------
printf "Info: Clear DNS cache\n"
sudo dscacheutil -flushcache
sudo killall -HUP mDNSResponder
# ----------------------------------------------------------


# ----------------------------------------------------------
# ------------------Clear inactive memory-------------------
# ----------------------------------------------------------
printf "Info: Clear inactive memory\n"
sudo purge
# ----------------------------------------------------------


# ----------------------------------------------------------
# ----------------Disable Firefox telemetry-----------------
# ----------------------------------------------------------
printf "Info: Disable Firefox telemetry\n"
# Enable Firefox policies so the telemetry can be configured.
sudo defaults write /Library/Preferences/org.mozilla.firefox EnterprisePoliciesEnabled -bool TRUE
# Disable sending usage data
sudo defaults write /Library/Preferences/org.mozilla.firefox DisableTelemetry -bool TRUE
# ----------------------------------------------------------


# ----------------------------------------------------------
# ------------Disable Microsoft Office telemetry------------
# ----------------------------------------------------------
printf "Info: Disable Microsoft Office telemetry\n"
defaults write com.microsoft.office DiagnosticDataTypePreference -string ZeroDiagnosticData
# ----------------------------------------------------------


# ----------------------------------------------------------
# ----------Remove Google Software Update service-----------
# ----------------------------------------------------------
printf "Info: Remove Google Software Update service\n"
googleUpdateFile="${HOME}/Library/Google/GoogleSoftwareUpdate/GoogleSoftwareUpdate.bundle/Contents/Resources/ksinstall"
if [ -f "${googleUpdateFile}" ]; then
    "${googleUpdateFile}" --nuke
    printf "Uninstalled Google update\n"
else
    printf "Google update file does not exist\n"
fi
# ----------------------------------------------------------


# ----------------------------------------------------------
# ---------Disable Homebrew user behavior analytics---------
# ----------------------------------------------------------
printf "Info: Disable Homebrew user behavior analytics\n"
command='export HOMEBREW_NO_ANALYTICS=1'
declare -a profile_files=("${HOME}/.bash_profile" "${HOME}/.zprofile")
for profile_file in "${profile_files[@]}"
do
    touch "${profile_file}"
    if ! grep -q "${command}" "${profile_file}"; then
        echo "${command}" >> "${profile_file}"
        printf "[%s] Configured\n" "${profile_file}"
    else
        printf "[%s] No need for any action, already configured\n" "${profile_file}"
    fi
done
# ----------------------------------------------------------


# ----------------------------------------------------------
# --------------Disable NET Core CLI telemetry--------------
# ----------------------------------------------------------
printf "Info: Disable NET Core CLI telemetry\n"
command='export DOTNET_CLI_TELEMETRY_OPTOUT=1'
declare -a profile_files=("${HOME}/.bash_profile" "${HOME}/.zprofile")
for profile_file in "${profile_files[@]}"
do
    touch "${profile_file}"
    if ! grep -q "${command}" "${profile_file}"; then
        echo "${command}" >> "${profile_file}"
        printf "[%s] Configured\n" "${profile_file}"
    else
        printf "[%s] No need for any action, already configured\n" "${profile_file}"
    fi
done
# ----------------------------------------------------------


# ----------------------------------------------------------
# ------------Disable PowerShell Core telemetry-------------
# ----------------------------------------------------------
printf "Info: Disable PowerShell Core telemetry\n"
command='export POWERSHELL_TELEMETRY_OPTOUT=1'
declare -a profile_files=("${HOME}/.bash_profile" "${HOME}/.zprofile")
for profile_file in "${profile_files[@]}"
do
    touch "${profile_file}"
    if ! grep -q "${command}" "${profile_file}"; then
        echo "${command}" >> "${profile_file}"
        printf "[%s] Configured\n" "${profile_file}"
    else
        printf "[%s] No need for any action, already configured\n" "${profile_file}"
    fi
done
# ----------------------------------------------------------

# ----------------------------------------------------------
# -------------Disable online spell correction--------------
# ----------------------------------------------------------
printf "Info: Disable online spell correction\n"
defaults write NSGlobalDomain WebAutomaticSpellingCorrectionEnabled -bool false
# ----------------------------------------------------------


# ----------------------------------------------------------
# ---------------Disable remote Apple events----------------
# ----------------------------------------------------------
printf "Info: Disable remote Apple events\n"
sudo systemsetup -setremoteappleevents off
# ----------------------------------------------------------


# ----------------------------------------------------------
# --Disable automatic storage of documents in iCloud Drive--
# ----------------------------------------------------------
printf "Info: Disable automatic storage of documents in iCloud Drive\n"
defaults write NSGlobalDomain NSDocumentSaveNewDocumentsToCloud -bool false
# ----------------------------------------------------------


# ----------------------------------------------------------
# ------Disable display of recent applications on Dock------
# ----------------------------------------------------------
printf "Info: Disable display of recent applications on Dock\n"
defaults write com.apple.dock show-recents -bool false
# ----------------------------------------------------------


# ----------------------------------------------------------
# ---------------Disable AirDrop file sharing---------------
# ----------------------------------------------------------
printf "Info: Disable AirDrop file sharing\n"
defaults write com.apple.NetworkBrowser DisableAirDrop -bool true
# ----------------------------------------------------------


# ----------------------------------------------------------
# ----------------Disable Spotlight indexing----------------
# ----------------------------------------------------------
printf "Info: Disable Spotlight indexing\n"
sudo mdutil -i off -d /
# ----------------------------------------------------------


# Disable personalized advertisements and identifier tracking
printf "Info: Disable personalized advertisements and identifier tracking\n"
defaults write com.apple.AdLib allowIdentifierForAdvertising -bool false
defaults write com.apple.AdLib allowApplePersonalizedAdvertising -bool false
defaults write com.apple.AdLib forceLimitAdTracking -bool true
# ----------------------------------------------------------


# ----------------------------------------------------------
# -------------Disable captive portal detection-------------
# ----------------------------------------------------------
printf "Info: Disable captive portal detection\n"
sudo defaults write '/Library/Preferences/SystemConfiguration/com.apple.captive.control.plist' Active -bool false
# ----------------------------------------------------------


# Disable library validation entitlement (library signature validation)
printf "Info: Disable library validation entitlement (library signature validation)\n"
sudo defaults write /Library/Preferences/com.apple.security.libraryvalidation.plist 'DisableLibraryValidation' -bool true
# ----------------------------------------------------------


# ----------------------------------------------------------
# ---------Disable Parallels Desktop advertisements---------
# ----------------------------------------------------------
printf "Info: Disable Parallels Desktop advertisements\n"
defaults write 'com.parallels.Parallels Desktop' 'ProductPromo.ForcePromoOff' -bool yes
defaults write 'com.parallels.Parallels Desktop' 'WelcomeScreenPromo.PromoOff' -bool yes
# ----------------------------------------------------------


# ----------------------------------------------------------
# Disable automatic downloads for Parallels Desktop updates-
# ----------------------------------------------------------
printf "Info: Disable automatic downloads for Parallels Desktop updates\n"
defaults write 'com.parallels.Parallels Desktop' 'Application preferences.Download updates automatically' -bool no
# ----------------------------------------------------------


# ----------------------------------------------------------
# --Disable automatic checks for Parallels Desktop updates--
# ----------------------------------------------------------
printf "Info: Disable automatic checks for Parallels Desktop updates\n"
defaults write 'com.parallels.Parallels Desktop' 'Application preferences.Check for updates' -int 0
# ----------------------------------------------------------


# ----------------------------------------------------------
# ------------Disable remote management service-------------
# ----------------------------------------------------------
printf "Info: Disable remote management service\n"
sudo /System/Library/CoreServices/RemoteManagement/ARDAgent.app/Contents/Resources/kickstart -deactivate -stop
# ----------------------------------------------------------


# ----------------------------------------------------------
# -----------Remove Apple Remote Desktop Settings-----------
# ----------------------------------------------------------
printf "Info: Remove Apple Remote Desktop Settings\n"
sudo rm -rf /var/db/RemoteManagement
sudo defaults delete /Library/Preferences/com.apple.RemoteDesktop.plist
defaults delete "${HOME}/Library/Preferences/com.apple.RemoteDesktop.plist"
sudo rm -rf "/Library/Application Support/Apple/Remote Desktop/"
rm -r "${HOME}/Library/Application Support/Remote Desktop/"
rm -r "${HOME}/Library/Containers/com.apple.RemoteDesktop"
# ----------------------------------------------------------


# ----------------------------------------------------------
# ------Disable participation in Siri data collection-------
# ----------------------------------------------------------
printf "Info: Disable participation in Siri data collection\n"
defaults write com.apple.assistant.support 'Siri Data Sharing Opt-In Status' -int 2
# ----------------------------------------------------------


# ----------------------------------------------------------
# --------------------Disable "Ask Siri"Info:-----------------
# ----------------------------------------------------------
printf "Info: Disable \"Ask Siri\"\n"
defaults write com.apple.assistant.support 'Assistant Enabled' -bool false
# ----------------------------------------------------------


# ----------------------------------------------------------
# ---------------Disable Siri voice feedback----------------
# ----------------------------------------------------------
printf "Info: Disable Siri voice feedback\n"
defaults write com.apple.assistant.backedup 'Use device speaker for TTS' -int 3
# ----------------------------------------------------------


# ----------------------------------------------------------
# -------Disable Siri services (Siri and assistantd)--------
# ----------------------------------------------------------
printf "Info: Disable Siri services (Siri and assistantd)\n"
launchctl disable "user/${UID}/com.apple.assistantd"
launchctl disable "gui/${UID}/com.apple.assistantd"
sudo launchctl disable 'system/com.apple.assistantd'
launchctl disable "user/${UID}/com.apple.Siri.agent"
launchctl disable "gui/${UID}/com.apple.Siri.agent"
sudo launchctl disable 'system/com.apple.Siri.agent'
if [ "$(/usr/bin/csrutil status | awk '/status/ {print $5}' | sed 's/\.$//')" = "enabled" ]; then
    printf >&2 "This script requires SIP to be disabled. Read more: https://developer.apple.com/documentation/security/disabling_and_enabling_system_integrity_protection\n"
fi
# ----------------------------------------------------------

# ----------------------------------------------------------
# -------Disable "Do you want to enable Siri?" pop-up-------
# ----------------------------------------------------------
printf "Info: Disable \"Do you want to enable Siri?\" pop-up\n"
defaults write com.apple.SetupAssistant 'DidSeeSiriSetup' -bool True
# ----------------------------------------------------------


# ----------------------------------------------------------
# ----------------Remove Siri from menu bar-----------------
# ----------------------------------------------------------
printf "Info: Remove Siri from menu bar\n"
defaults write com.apple.systemuiserver 'NSStatusItem Visible Siri' 0
# ----------------------------------------------------------


# ----------------------------------------------------------
# ---------------Remove Siri from status menu---------------
# ----------------------------------------------------------
printf "Info: Remove Siri from status menu\n"
defaults write com.apple.Siri 'StatusMenuVisible' -bool false
defaults write com.apple.Siri 'UserHasDeclinedEnable' -bool true
# ----------------------------------------------------------


# ----------------------------------------------------------
# ---------------Enable application firewall----------------
# ----------------------------------------------------------
printf "Info: Enable application firewall\n"
/usr/libexec/ApplicationFirewall/socketfilterfw --setglobalstate on
sudo defaults write /Library/Preferences/com.apple.alf globalstate -bool true
defaults write com.apple.security.firewall EnableFirewall -bool true
# ----------------------------------------------------------


# ----------------------------------------------------------
# -----------------Enable firewall logging------------------
# ----------------------------------------------------------
printf "Info: Enable firewall logging\n"
/usr/libexec/ApplicationFirewall/socketfilterfw --setloggingmode on
sudo defaults write /Library/Preferences/com.apple.alf loggingenabled -bool true
# ----------------------------------------------------------


# ----------------------------------------------------------
# -------------------Enable stealth mode--------------------
# ----------------------------------------------------------
printf "Info: Enable stealth mode\n"
/usr/libexec/ApplicationFirewall/socketfilterfw --setstealthmode on
sudo defaults write /Library/Preferences/com.apple.alf stealthenabled -bool true
defaults write com.apple.security.firewall EnableStealthMode -bool true
# ----------------------------------------------------------


# Enable password requirement for waking from sleep or screen saver
printf "Info: Enable password requirement for waking from sleep or screen saver\n"
sudo defaults write /Library/Preferences/com.apple.screensaver askForPassword -bool true
# ----------------------------------------------------------


# Enable session lock five seconds after screen saver initiation
printf "Info: Enable session lock five seconds after screen saver initiation\n"
sudo defaults write /Library/Preferences/com.apple.screensaver 'askForPasswordDelay' -int 5
# ----------------------------------------------------------


# ----------------------------------------------------------
# ---------Disable guest sign-in from login screen----------
# ----------------------------------------------------------
printf "Info: Disable guest sign-in from login screen\n"
sudo defaults write /Library/Preferences/com.apple.loginwindow GuestEnabled -bool NO
# ----------------------------------------------------------


# ----------------------------------------------------------
# -------Disable guest access to file shares over AF--------
# ----------------------------------------------------------
printf "Info: Disable guest access to file shares over AF\n"
sudo defaults write /Library/Preferences/SystemConfiguration/com.apple.smb.server AllowGuestAccess -bool NO
# ----------------------------------------------------------


# ----------------------------------------------------------
# -------Disable guest access to file shares over SMB-------
# ----------------------------------------------------------
printf "Info: Disable guest access to file shares over SMB\n"
sudo defaults write /Library/Preferences/com.apple.AppleFileServer guestAccess -bool NO
# ----------------------------------------------------------


# ----------------------------------------------------------
# -------Disable incoming SSH and SFTP remote logins--------
# ----------------------------------------------------------
printf "Info: Disable incoming SSH and SFTP remote logins\n"
echo 'yes' | sudo systemsetup -setremotelogin off
# ----------------------------------------------------------


# ----------------------------------------------------------
# ------------Disable the insecure TFTP service-------------
# ----------------------------------------------------------
printf "Info: Disable the insecure TFTP service\n"
sudo launchctl disable 'system/com.apple.tftpd'
# ----------------------------------------------------------


# ----------------------------------------------------------
# ----------Disable Bonjour multicast advertising-----------
# ----------------------------------------------------------
printf "Info: Disable Bonjour multicast advertising\n"
sudo defaults write /Library/Preferences/com.apple.mDNSResponder.plist NoMulticastAdvertisements -bool true
# ----------------------------------------------------------


# ----------------------------------------------------------
# -------------Disable insecure telnet protocol-------------
# ----------------------------------------------------------
printf "Info: Disable insecure telnet protocol\n"
sudo launchctl disable system/com.apple.telnetd
# ----------------------------------------------------------


# ----------------------------------------------------------
# ----Disable local printer sharing with other computers----
# ----------------------------------------------------------
printf "Info: Disable local printer sharing with other computers\n"
cupsctl --no-share-printers
# ----------------------------------------------------------


# Disable printing from external addresses, including the internet
printf "Info: Disable printing from external addresses, including the internet\n"
cupsctl --no-remote-any
# ----------------------------------------------------------


# ----------------------------------------------------------
# ----------Disable remote printer administration-----------
# ----------------------------------------------------------
printf "Info: Disable remote printer administration\n"
cupsctl --no-remote-admin
# ----------------------------------------------------------

# ----------------------------------------------------------
# --Disable automatic incoming connections for signed apps--
# ----------------------------------------------------------
printf "Info: Disable automatic incoming connections for signed apps\n"
sudo defaults write /Library/Preferences/com.apple.alf allowsignedenabled -bool false
# ----------------------------------------------------------


# Disable automatic incoming connections for downloaded signed apps
printf "Info: Disable automatic incoming connections for downloaded signed apps\n"
sudo defaults write /Library/Preferences/com.apple.alf allowdownloadsignedenabled -bool false
# ----------------------------------------------------------


# ----------------------------------------------------------
# -------Disable Gatekeeper's automatic reactivation--------
# ----------------------------------------------------------
printf "Info: Disable Gatekeeper's automatic reactivation\n"
sudo defaults write /Library/Preferences/com.apple.security GKAutoRearm -bool true
# ----------------------------------------------------------


# ----------------------------------------------------------
# --------------------Disable Gatekeeper--------------------
# ----------------------------------------------------------
printf "Info: Disable Gatekeeper\n"
os_major_ver=$(sw_vers -productVersion | awk -F "." '{print $1}')
os_minor_ver=$(sw_vers -productVersion | awk -F "." '{print $2}')
if [[ "${os_major_ver}" -le 10 \
        || ( "${os_major_ver}" -eq 10 && "${os_minor_ver}" -lt 7 ) \
    ]]; then
    printf "No action needed, Gatekeeper is not available this OS version\n"
else
    gatekeeper_status="$(spctl --status | awk '/assessments/ {print $2}')"
    if [ "${gatekeeper_status}" = "disabled" ]; then
        printf "No action needed, Gatekeeper is already disabled\n"
    elif [ "${gatekeeper_status}" = "enabled" ]; then
        sudo spctl --master-disable
        sudo defaults write '/var/db/SystemPolicy-prefs' 'enabled' -string 'no'
        printf "Disabled Gatekeeper\n"
    else
        printf >&2 "Unknown gatekeeper status: %s\n" "${gatekeeper_status}"
    fi
fi
# ----------------------------------------------------------


# ----------------------------------------------------------
# -----------Disable automatic checks for updates-----------
# ----------------------------------------------------------
printf "Info: Disable automatic checks for updates\n"
# For OS X Yosemite and newer (>= 10.10)
#sudo defaults write /Library/Preferences/com.apple.SoftwareUpdate 'AutomaticCheckEnabled' -bool false
# ----------------------------------------------------------


# ----------------------------------------------------------
# ---------Disable automatic downloads for updates----------
# ----------------------------------------------------------
printf "Info: Disable automatic downloads for updates\n"
# For OS X Yosemite and newer (>= 10.10)
#sudo defaults write /Library/Preferences/com.apple.SoftwareUpdate 'AutomaticDownload' -bool false
# ----------------------------------------------------------


# ----------------------------------------------------------
# -----Disable automatic installation of macOS updates------
# ----------------------------------------------------------
printf "Info: Disable automatic installation of macOS updates\n"
# For OS X Yosemite through macOS High Sierra (>= 10.10 && < 10.14)
sudo defaults write /Library/Preferences/com.apple.commerce 'AutoUpdateRestartRequired' -bool false
# For Mojave and newer (>= 10.14)
#sudo defaults write /Library/Preferences/com.apple.SoftwareUpdate 'AutomaticallyInstallMacOSUpdates' -bool false
# ----------------------------------------------------------


# ----------------------------------------------------------
# -----Disable automatic app updates from the App Store-----
# ----------------------------------------------------------
printf "Info: Disable automatic app updates from the App Store\n"
# For OS X Yosemite and newer (>= 10.10)
sudo defaults write /Library/Preferences/com.apple.commerce 'AutoUpdate' -bool false
# For Mojave and newer (>= 10.14)
#sudo defaults write /Library/Preferences/com.apple.SoftwareUpdate 'AutomaticallyInstallAppUpdates' -bool false
# ----------------------------------------------------------


# ----------------------------------------------------------
# ---------Disable macOS beta release installation----------
# ----------------------------------------------------------
printf "Info: Disable macOS beta release installation\n"
# For OS X Yosemite and newer (>= 10.10)
sudo defaults write /Library/Preferences/com.apple.SoftwareUpdate 'AllowPreReleaseInstallation' -bool false
# ----------------------------------------------------------


# Disable automatic installation for configuration data (e.g. XProtect, Gatekeeper, MRT)
printf "Info: Disable automatic installation for configuration data (e.g. XProtect, Gatekeeper, MRT)\n"
# For OS X Yosemite and newer (>= 10.10)
sudo defaults write /Library/Preferences/com.apple.SoftwareUpdate 'ConfigDataInstall' -bool false
# ----------------------------------------------------------


# Disable automatic installation for system data files and security updates
printf "Info: Disable automatic installation for system data files and security updates\n"
# For OS X Yosemite and newer (>= 10.10)
sudo defaults write /Library/Preferences/com.apple.SoftwareUpdate 'CriticalUpdateInstall' -bool false
# ----------------------------------------------------------


# ----------------------------------------------------------
# -Clear logs of all downloaded files from File Quarantine--
# ----------------------------------------------------------
printf "Info: Clear logs of all downloaded files from File Quarantine\n"
db_file="${HOME}/Library/Preferences/com.apple.LaunchServices.QuarantineEventsV2"
db_query='delete from LSQuarantineEvent'
if [ -f "${db_file}" ]; then
    printf "Database exists at \"%s\"\n" "${db_file}"
    if ls -lO "${db_file}" | grep --silent 'schg'; then
        sudo chflags noschg "${db_file}"
        printf "Found and removed system immutable flag\n"
        has_system_immutable_flag=true
    fi
    if ls -lO "${db_file}" | grep --silent 'uchg'; then
        sudo chflags nouchg "${db_file}"
        printf "Found and removed user immutable flag\n"
        has_user_immutable_flag=true
    fi
    sqlite3 "${db_file}" "${db_query}"
    printf "Executed the query \"%s\"\n" "${db_query}"
    if [ "${has_system_immutable_flag}" = true ] ; then
        sudo chflags schg "${db_file}"
        printf "Added system immutable flag back\n"
    fi
    if [ "${has_user_immutable_flag}" = true ] ; then
        sudo chflags uchg "${db_file}"
        printf "Added user immutable flag back\n"
    fi
else
    printf "No action needed, database does not exist at \"%s\"\n" "${db_file}"
fi
# ----------------------------------------------------------

# ----------------------------------------------------------
# --Clear File Quarantine attribute from downloaded files---
# ----------------------------------------------------------
printf "Info: Clear File Quarantine attribute from downloaded files\n"
find ~/Downloads        \
        -type f         \
        -exec           \
            sh -c       \
                '
                    attr="com.apple.quarantine"
                    file="$1"
                    if [[ $(xattr "${file}") = *${attr}* ]]; then
                        if xattr -d "${attr}" "${file}" 2>/dev/null; then
                            printf "🧹 Cleaned attribute from \"%s\"\n" "${file}"
                        else
                            printf >&2 "❌ Failed to clean attribute from \"%s\"\n" "${file}"
                        fi
                    else
                        printf "No attribute in \"%s\"\n" "${file}"
                    fi
                '       \
            {} \;
# ----------------------------------------------------------


# ----------------------------------------------------------
# ------Disable downloaded file logging in quarantine-------
# ----------------------------------------------------------
printf "Info: Disable downloaded file logging in quarantine\n"
file_to_lock="${HOME}/Library/Preferences/com.apple.LaunchServices.QuarantineEventsV2"
if [ -f "${file_to_lock}" ]; then
    sudo chflags schg "${file_to_lock}"
    printf "Made file immutable at \"%s\"\n" "${file_to_lock}"
else
    printf "No action is needed, file does not exist at \"%s\"\n" "${file_to_lock}"
fi
# ----------------------------------------------------------


# Disable extended quarantine attribute for downloaded files (disables warning)
printf "Info: Disable extended quarantine attribute for downloaded files (disables warning)\n"
sudo defaults write com.apple.LaunchServices 'LSQuarantine' -bool NO
# ----------------------------------------------------------


printf "Your privacy and security is now hardened 🎉💪\n"
printf "Press any key to exit.\n"
read -n 1 -s

cleanup

script_name1="$(basename "${0}")"
printf "\nInfo: script_name1: %s\n" "${script_name1}"
script_path1="$(realpath "$(dirname "${0}")")"
printf "Info: script_path1: %s\n" "${script_path1}"
script_path_with_name="${script_path1}/${script_name1}"
printf "Info: Script path with name: %s\n" "${script_path_with_name}"
printf "Info: Script finished\n"
exit 0
