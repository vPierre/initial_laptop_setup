.. image:: 
   _static/Logo.png

Privacy
*********************************************************************

.. contents:: Content
    :depth: 3

The following chapter will describe measures to increase the privacy.

Computer-/Hostname (Mandatory)
---------------------------------------------------------------------

It is recommended to mask the name of the host since it can comprise the user name and the current operating system (MacBook → Mac OS). Therefore the following command can be issued to change the name which is displayed over the network:

.. Code:: Bash

    sudo scutil --set ComputerName ExampleName
    sudo scutil --set LocalHostName ExampleName

Limit Ad Tracking (Mandatory)
---------------------------------------------------------------------
To limit tracking of the device disable the following:

System Preferences → Security & Privacy → Privacy → Advertising
Check “Limit Ad Tracking”

Tracking Services (Mandatory)
---------------------------------------------------------------------

It is recommended to disable the Tracking Services. If you decide to use the Tracking Services, it is possible to disable it just for the Spotlight Suggestions.

*System Preferences → Security & Privacy → Privacy → Location Services
Select "System Services" → Details
Uncheck "Spotlight Suggestions"

Web Browser
---------------------------------------------------------------------

Search Engine
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Set https://www.startpage.com/ [1]_ or https://duckduckgo.com/ [2]_
as your default search engine to hide a little yourself:

**Startpage** is highly recommended 

- #1 Click on the magnifying glass in the search bar

.. figure:: 
   _static/duckduckgo-firefox-ss-step1.jpg
   :align: center
   :width: 150px

- #2 Click on Change Search Settings in the dropdown menu

.. figure:: 
   _static/duckduckgo-firefox-ss-step2.jpg
   :align: center
   :width: 150px

- #3 Select DuckDuckGo under the Default Search Engine section

.. figure:: 
   _static/duckduckgo-firefox-ss-step3.jpg
   :align: center
   :width: 150px
   
To get more update protection, remove at least on each browser at least the following search engines:

- eBay
- Twitter
- Bing
- Yahoo
- Google

Do not track
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Many online tracking companies monitor user clicks, searches and other browsing habits. This information is then used for targeted advertising, analytic purpose, and as such. It may feel unsettling because this usually happens without user awareness. If you’re concerned about your online privacy and do not wish your browsing behaviour to be followed, then you may want to enable the new “Do Not Track” (DNT) feature on your web browser.

Do Not Track is a technology that allows users to opt out of tracking by websites. When this feature is activated, the browser signals the website with an HTTP header about your opt-out preference. Technically, when enabled, a bit of text which reads “DNT: 1” (where “1” stands for “on” and “0” means “off”) is sent as metadata. Eventually it’s up to the companies to comply to your settings, but with more and more websites now respecting the Do Not Track signal, it becomes an effective tool for protecting your privacy.

The following tutorial explains how to flip the DNT switch ON in the five most popular browsers: Mozilla Firefox, Internet Explorer, Opera, Apple Safari, and Google Chrome. These five desktop browsers account for nearly all computer surfing.
Activate Do Not Track In Firefox

    Click on Firefox button > Options
    Now select Privacy Tab, and check the box labeled “Tell websites I do not want to be tracked.”

That’s it. You have enabled Do Not Track on your Firefox browser. The same steps work on Firefox Nightly and Waterfox browser.
Activate Do Not Track In Internet Explorer 9

    Press Alt to activate menu bar and click on Tools > Tracking Protection, or click on the button shaped like a gear > Safety > Tracking Protection
    In the Manage Add-on dialog box, click Tracking Protection
    With the “Your Personalized List” item selected, click the Enable button in the lower right-hand corner of the box.

That’s it. You have enabled Do Not Track on your Internet Explorer 9 browser.

Activate Do Not Track In Opera

    Open Preferences windows by clicking on Opera button > Settings > Preferences, or press Ctrl + F12
    Switch to Advanced tab, and select Security in the left menu panel
    Now check the box labeled “Ask websites not to track me”

    Click OK and your settings will be saved.

That’s it. You have enabled Do Not Track on your Opera browser.

Activate Do Not Track In Safari

In Safari browser, you need to turn on the Develop menu item first. To do that:

    Open Safari Preferences from the Safari menu, and click on “Advanced”
    Next check the box that says “Show Develop menu in menu bar”, and close the preferences window
    Now under the new Develop menu, select “Send Do Not Track HTTP Header”. and save changes.

With the Develop menu enabled:

    Select “Send Do Not Track HTTP Header” menu item

That’s it. You have enabled Do Not Track on your Safari browser.

Activate Do Not Track In Chrome

Google Chrome doesn’t offer a native feature to turn on DNT. To get this functionality, you have to install a third-party extension called “Do Not Track”.

    Open Extensions page by clicking on the wrench icon > Tools > Extensions, or go to this URL: chrome://chrome/extensions/
    Scroll down the Extension page and click the link that says “Get more extensions”. This links will take you to the Chrome Web Store.
    In the search box in the upper left hand corner, search for “Do Not Track” extension created by Jonathan Mayer.

    Do Not Track Google Chrome
    To install it on your browser, just click the “Add To Chrome” button

That’s it. You have enabled Do Not Track on your Chrome browser.

and so forth ...

Check your Web Browser fingerprints
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Trackers use a variety of methods to identify and track users. Most often, this includes tracking cookies, but it can also include browser fingerprinting. Fingerprinting is a sneakier way to track users and makes it harder for users to regain control of their browsers. This report measures how easily trackers might be able to fingerprint your browser. [3]_ 

.....

.. Rubric:: Footnotes

.. [1]
   https://www.startpage.com

.. [2]
   https://duckduckgo.com

.. [3]
   https://coveryourtracks.eff.org/
