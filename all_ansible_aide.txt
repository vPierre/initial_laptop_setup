File: ./tools/ansible/roles/EL_AIDE/tasks/main.yml
---
---
- name: RedHat family AIDE package installation
  become: true
  ansible.builtin.dnf:
    name: "{{ aide_package_name }}"
    state: present
  when: ansible_os_family == "RedHat" and ansible_distribution_major_version >= "8"
  tags:
    - aide
    - CCE-80844-4

- name: RedHat family AIDE package installation (RHEL 7)
  become: true
  ansible.builtin.dnf:
    name: "{{ aide_package_name }}"
    state: present
  when: ansible_distribution == "RedHat" and ansible_distribution_major_version == "7"
  tags:
    - aide
    - CCE-80844-4

- name: Stat AIDE cron.daily
  become: true
  ansible.builtin.stat:
    path: "{{ aide_cron_path }}"
  register: aidecron
  tags:
    - aide
    - CCE-80844-4
    - CIS-UBUNTU2004-1.3.1

- name: Install AIDE service
  become: true
  ansible.builtin.template:
    src: lib/systemd/system/aidecheck.service.j2
    dest: "{{ aide_service_path }}"
    backup: true
    mode: "0644"
    owner: root
    group: root
  when: not aidecron.stat.exists
  tags:
    - aide

- name: Install AIDE timer
  become: true
  ansible.builtin.template:
    src: lib/systemd/system/aidecheck.timer.j2
    dest: "{{ aide_timer_path }}"
    backup: true
    mode: "0644"
    owner: root
    group: root
  when: not aidecron.stat.exists
  notify:
    - Reload systemd
    - Enable aidecheck
  tags:
    - aide
    - CCE-80676-0
    - CIS-UBUNTU2004-1.3.2

- name: Verify the Audit tools
  become: true
  ansible.builtin.lineinfile:
    dest: "{{ aide_conf_path }}"
    state: present
    regexp: ^[# ]*{{ item }}
    line: "{{ item }} p+i+n+u+g+s+b+acl+xattrs+sha512"
    mode: "0640"
    create: false
  with_items: "{{ aide_audit_tools }}"
  tags:
    - aide

- name: Add AIDE dir exclusions
  become: true
  ansible.builtin.blockinfile:
    path: "{{ aide_conf_path }}"
    marker: "# {mark} ANSIBLE MANAGED EXCLUSION BLOCK"
    backup: true
    insertafter: EOF
    block: |
      {% for exclusion in aide_exclusions %}
      !{{ exclusion }}
      {% endfor %}
  tags:
    - aide

- name: Stat RedHat aide.db
  become: true
  ansible.builtin.stat:
    path: "{{ aide_db_path }}"
  register: rhelaidedb
  changed_when: false
  failed_when: false
  when: ansible_os_family == "RedHat"
  tags:
    - aide

- name: Initialize RedHat AIDE
  become: true
  ansible.builtin.command:
    cmd: aide --init -B 'database_out=file:{{ aide_db_path }}'
  register: init_redhat_aide
  changed_when: init_redhat_aide.rc != 0
  failed_when: init_redhat_aide.rc != 0
  when: ansible_os_family == "RedHat" and not rhelaidedb.stat.exists
  tags:
    - aide
    - CCE-80675-2

- name: Stat RedHat aide.db.new.gz
  become: true
  ansible.builtin.stat:
    path: "{{ aide_db_new_path }}"
  register: rhelaidedbnew
  changed_when: false
  failed_when: false
  when: ansible_os_family == "RedHat"
  tags:
    - aide

- name: Copy RedHat AIDE database
  become: true
  ansible.builtin.command:
    cmd: cp {{ aide_db_path }} {{ aide_db_new_path }}
  register: cp_redhat_aide_db
  changed_when: cp_redhat_aide_db.rc != 0
  failed_when: cp_redhat_aide_db.rc != 0
  when: ansible_os_family == "RedHat" and not rhelaidedbnew.stat.exists
  tags:
    - aide

- name: Run all notified handlers
  ansible.builtin.meta: flush_handlers
---

File: ./tools/ansible/roles/EL_AIDE/meta/main.yml
---
galaxy_info:
  author: Ayesha Shafqat, Pierre Gronau
  description: This role installs, configures, and manages AIDE (Advanced Intrusion Detection Environment) on Linux systems.
  company: ndaal, Cologne
  license: Apache 2.0
  min_ansible_version: "2.9"
  platforms:
    - name: EL
      versions:
        - "7"
        - "8"
        - "9"
  galaxy_tags:
    - aide
    - security
    - compliance
    - cis
    - linux
    - rhel
    - centos
    - almalinux
    - rockylinux
    - oracle
    - fedora
    - redhat
    - enterprise
dependencies: []
---

File: ./tools/ansible/roles/EL_AIDE/defaults/main.yml
---
---
aide_package_name: aide
aide_conf_path: /etc/aide.conf
aide_db_path: /var/lib/aide/aide.db.gz
aide_db_new_path: /var/lib/aide/aide.db.new.gz
aide_cron_path: /etc/cron.daily/aide
aide_service_path: /lib/systemd/system/aidecheck.service
aide_timer_path: /lib/systemd/system/aidecheck.timer
aide_audit_tools:
  - /usr/sbin/auditctl
  - /usr/sbin/auditd
  - /usr/sbin/ausearch
  - /usr/sbin/aureport
  - /usr/sbin/autrace
  - /usr/sbin/augenrules
  - /usr/sbin/audispd
aide_exclusions:
  - /var/lib/docker
  - /var/lib/lxcfs
  - /var/lib/private/systemd
  - /var/log/audit
  - /var/log/journal
---

File: ./tools/ansible/roles/EL_AIDE/handlers/main.yml
---
---
- name: Reload systemd
  become: true
  ansible.builtin.systemd:
    daemon_reload: true

- name: Enable aidecheck
  become: true
  ansible.builtin.systemd:
    name: aidecheck.timer
    enabled: true
    state: started

# - name: Disable aidecheck
#   become: true
#   ansible.builtin.systemd:
#     name: aidecheck.timer
#     state: stopped
#     enabled: false
#
# - name: Mask aidecheck
#   become: true
#   ansible.builtin.systemd:
#     name: aidecheck.timer
#     masked: true
#     state: stopped
#     enabled: false
---

