.. image:: 
   _static/Logo.png

Back up your Mac with Time Machine
*********************************************************************

.. contents:: Content
    :depth: 3


Learn how to create a backup of the files on your Mac.

Use Time Machine, the built-in backup feature of your Mac, to automatically back up your personal data, including apps, music, photos, email, and documents. Having a backup allows you to recover files that were deleted, or that were lost because the hard disk (or SSD) in your Mac needed to be erased or replaced. Learn how to restore your Mac from a backup.

Create a Time Machine backup
---------------------------------------------------------------------

To create backups with Time Machine, all you need is an external storage device. After you connect the storage device and select it as your backup disk, Time Machine automatically makes hourly backups for the past 24 hours, daily backups for the past month, and weekly backups for all previous months. The oldest backups are deleted when your backup disk is full.

Connect an external storage device
---------------------------------------------------------------------

Connect one of the following external storage devices, sold separately. Learn more about backup disks that you can use with Time Machine.

External drive connected to your Mac, such as a USB or Thunderbolt drive
Network-attached storage (NAS) device that supports Time Machine over SMB
Mac shared as a Time Machine backup destination
AirPort Time Capsule, or external drive connected to an AirPort Time capsule or AirPort Extreme Base Station (802.11ac)
Select your storage device as the backup disk
Open Time Machine preferences from the Time Machine menu  in the menu bar. Or choose Apple menu  > System Preferences, then click Time Machine.
Click Select Backup Disk.
macOS System Preferences Time Machine window
Select your backup disk from the list of available disks. To make your backup accessible only to users who have the backup password, you can select “Encrypt backups”. Then click Use Disk:
macOS System Preferences Time Machine window Select Backup Disk chooser
If the disk you selected isn't formatted as required by Time Machine, you're prompted to erase the disk first. Click Erase to proceed. This erases all information on the backup disk. 

Enjoy the convenience of automatic backups
---------------------------------------------------------------------

After you select a backup disk, Time Machine immediately begins making periodic backups—automatically and without further action by you. The first backup may take a long time, but you can continue using your Mac while a backup is underway. Time Machine backs up only the files that changed since the previous backup, so future backups will be faster.

To start a backup manually, choose Back Up Now from the Time Machine menu  in the menu bar. Use the same menu to check the status of a backup or skip a backup in progress.

Learn more
---------------------------------------------------------------------

Learn about other ways to back up and restore files
If you back up to multiple disks, you can press and hold the Option key, then choose Browse Other Backup Disks from the Time Machine menu.
To exclude items from your backup, open Time Machine preferences, click Options, then click the add (+) button to add an item to be excluded. To stop excluding an item, such as an external hard drive, select the item and click the remove (–) button.
If using Time Machine to back up to a network disk, you can verify those backups to make sure they're in good condition. Press and hold Option, then choose Verify Backups from the Time Machine menu. 

.. Notes:: If you will ask for Encryption, choose every time with Encryption.

.....

.. Rubric:: Footnotes


.. [1]
   https://support.apple.com/en-us/HT201250
