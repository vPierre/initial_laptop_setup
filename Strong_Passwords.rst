.. image:: 
   _static/Logo.png

Passwords
*********************************************************************

.. contents:: Content
    :depth: 3
    
Generate strong passwords with several programs or directly from /dev/urandom:

.. code:: bash

   $ openssl rand -base64 30
   2edtUERYsyvGYt8/TVoCOK4j9wKKjsdeNsWvyom7

   $ gpg --gen-random -a 0 90 | fold -w 40
   HcNLItMVWqE8jG9085vnewGE9t4jvG1oqVO38fP/
   n3ibhjc33JMYVnSKRDDZwsroeJgWHy8YVd7W0Xnz
   9b8fx/JfWaRh4Gx0yd/h+zC9OFtNO2iFwVha/kW7

   $ LANG=C tr -dc 'A-F0-9' < /dev/urandom | fold -w 40 | head -n 5
   A779D8B1D032D099F4C63671E2872011BF2AFA5C
   26C7D07007258E1CB07FB22F3F47FD0CDDCD64BD
   8DC023D4C6DC1DB0D1878D4934E3028C7ED9A941
   240AB70C6DC4EDFC6C73AA6FB6DFA317A9F4D3CF
   5142F9F1075920759A8AE2796188B2F557C7C924

   $ tr -dc '[:alnum:]' < /dev/urandom | fold -w 40 | head -n5
   zmj8S0iuxud8y8YHjzdg7Hefu6U1KAYBiLl3aE8v
   nCNpuMkWohTjQHntTzbiLQJG5zLzEHWSWaYSwjtm
   R2L6M909S3ih852IkJqQFMDawCiHcpPBxlllAPrt
   aZOXKVUmxhzQwVSYb6nqAbGTVMFSJOLf094bFZAb
   HfgwSNlkVBXwIPQST6E6x6vDNCCasMLSSOoTUfSK

   $ tr -dc '[:lower:]' < /dev/urandom | fold -w 40 | head -n5
   gfvkanntxutzwxficgvavbwdvttexdezdftvvtmn
   lgrsuiugwkqbtbkyggcbpbqlynwbiyxzlabstqcf
   ufctdlsbyonkowzpmotxiksnsbwdzkjrjsupoqvr
   hjwibdjxtmuvqricljayzkgdfztcmapsgwsubggr
   bjstlmvwjczakgeetkbmwbjnidbeaerhaonpkacg

   $ tr -dc '[:upper:]' < /dev/urandom | fold -w 40 | head -n5
   EUHZMAOBOLNFXUNNDSTLJTPDCPVQBPUEQOLRZUQZ
   HVNVKBEPAAYMXRCGVCNEZLFHNUYMRYPTWPWOOZVM
   TAHEUPQJTSYQVJVYSKLURESMKWEZONXLUDHWQODB
   PRDITWMAXXZLTRXEEOGOSGAWUXYDGDRJYRHUWICM
   VHERIQBLBPHSIUZSGYZRDHTNAPUGJMRODIKBWZRJ

   $ tr -dc '[:graph:]' < /dev/urandom | fold -w 40 | head -n5
   n\T2|zUz:\C,@z9!#p3!B/[t6m:B94}q&t(^)Ol~
   J%MMDbAgGdP}zrSQO!3mrP3$w!.[Ng_xx-_[C<3g
   ^)6V&*<2"ZOgU.mBd]iInvFKiT<dq~y\O[cdDK`V
   +RE]UYPIf3:StX`y#w,.iG~g"urD)'FnDIFI_q^)
   6?HRillpgvvFDBAr4[:H{^oAL<`Em7$roF=2w;1~

or with KeePassXC [1]_ please use Decryption Time slider
at highest values [2]_

.....

.. Rubric:: Footnotes

.. [1]
   KeePassXC
   https://keepassxc.org

.. [2]
   KeePassXC  creating your first database
   https://keepassxc.org/docs/KeePassXC_UserGuide#_creating_your_first_database
