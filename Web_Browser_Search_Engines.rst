.. image:: 
   _static/Logo.png

Web Browser Search Engines
*********************************************************************

.. contents:: Content
    :depth: 3

The following search engine are privacy friendly and hide some of your behavior: 

Startpage
---------------------------------------------------------------------

DuckDuckGo
---------------------------------------------------------------------

Qwant
---------------------------------------------------------------------

YaCy
---------------------------------------------------------------------

We’ve dusted off YaCy, a decade old, neglected, open source,
search engine network, that relies upon DHT torrent sharing
of results, in a permissionless decentralized network. I’m now
running 4 global instances of this, with over 7 million pages
indexed. Liberty flows through my veins, with a key focus
on oppressed political news and technology.

- Moldova [5]_

- Amsterdam: [6]_

- Romania: [7]_
  (Whole network, Global DHT results)

.....

.. Rubric:: Footnotes

.. [1]
   Metager https://metager.de

.. [2]
   Startpage
   https://www.startpage.com

.. [3]
   DuckDuckGo
   https://duckduckgo.com – only for people trusting US companies

.. [4]
   Qwant
   https://www.qwant.com

.. [5]
   YaCy Moldova:
   https://search.simplifiedprivacy.is

.. [6]
   YaCy Amsterdam:
   https://search.simplifiedprivacy.com

.. [7]
   YaCy Romania: (Whole network, Global DHT results)
   https://yacy.rebelnet.me
