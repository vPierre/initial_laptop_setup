.. image:: 
   _static/Logo.png

.. contents:: Content
    :depth: 3
   
Initial Laptop Setup
*********************************************************************

.SYNOPSIS
---------------------------------------------------------------------

Initially Setup macOS Laptop with wished crucial software for ndaal.

Create a User "cloud" and so forth. "cloud" SHOULD be your Administrator Account.

.DESCRIPTION
---------------------------------------------------------------------

The initial setup based on brew of [1]_ 

Requirements
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

- GitLab Account and granted rights to gain access to our Repos

- macOS Sequoia Intel x86 architecture based (not tested and not checked on ARM)

- and Xcode see [2]_ 

.. code:: bash
   
   xcode-select --install

-  later on Internet

Needed Accounts
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

- GitLab
- GitHub
- Signal
- Threema (Voucher ask the office)
- email (ask the office)
- Jitsi (ask the office, https://meet.ndaal.eu))
- Timebuttler

optional

- Discord
- Slack


Preparation
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Note Before setting up macOS, consider disconnecting networking and configuring a firewall(s) first. However, late 2016 MacBooks with Touch Bar hardware require online OS activation.

On first boot, hold Command Option P R keys to clear NVRAM.

When macOS first starts, you'll be greeted by Setup Assistant. [3]_ 

When creating the first account, use a strong password minimum 24 characters without a hint.

You follow the Apple initial phase without touching the internet, if possible.

Installation
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. Rubric:: Firewall

There are several types of firewalls available for macOS.

.. Rubric:: Application layer firewall

Built-in, basic firewall which blocks incoming connections only. This firewall does not have the ability to monitor, nor block outgoing connections.

It can be controlled by the Firewall tab of Security & Privacy in System Preferences, or with the following commands.

Enable the firewall with logging and stealth mode:

.. code:: bash

   $ sudo /usr/libexec/ApplicationFirewall/socketfilterfw --setglobalstate on
   Firewall is enabled. (State = 1)

   $ sudo /usr/libexec/ApplicationFirewall/socketfilterfw --setloggingmode on
   Turning on log mode

   $ sudo /usr/libexec/ApplicationFirewall/socketfilterfw --setstealthmode on
   Stealth mode enabled

.. Rubric:: Internet

Now establish a Internet connections

.. code:: bash

   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"

.. code:: bash

   brew update && brew upgrade && brew cleanup

Edit PATH in your shell or shell rc file to use ~/homebrew/bin and ~/homebrew/sbin. For example, echo 'PATH=$PATH:~/homebrew/sbin:~/homebrew/bin' >> .zshrc, then change your login shell to Z shell with chsh -s /bin/zsh, open a new Terminal window and run brew update.

Homebrew uses SSL/TLS to talk with GitHub and verifies integrity of downloaded packages, so it's fairly secure.

Remember to periodically run brew update and brew upgrade on trusted and secure networks to download and install software updates. To get information on a package before installation, run brew info <package> and check its recipe online.

To opt out of Homebrew's analytics, you can set export HOMEBREW_NO_ANALYTICS=1 in your environment or shell rc file, or use brew analytics off.

.. code:: bash

   brew analytics off

   brew cask upgrade iridium chromium brave-browser iterm2 mucommander vlc firefox thunderbird adium cyberduck google-chrome waterfox-classic mediainfo      
   mediathekview osxfuse java xee scribus wireshark dash diffmerge epub-to-pdf libreoffice microsoft-office skim skitch keepassxc boxcryptor cryptomator fugu 
   gimp kodi vienna quicksilver clementine inkscape mono-mdk adobe-acrobat-reader epic tor-browser vivaldi wget curl vscodium kiwix gpg-suite 
   tor-browser xquartz brave-browser free-download-manager virtualbox docker anydesk mactracker vmware-fusion keka ssh-audit

   brew tap buo/cask-upgrade [5]_ 

.. Seealso:: Git ignore here initial [7]_ 

Reinstall for existing hardware
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Please follow the procedure described in the Links:

- <https://support.apple.com/en-us/102655>
- <https://support.apple.com/de-de/102655>
- <https://www.macobserver.com/tips/how-to/reinstall-macos-using-internet-recovery-mode/>

macOS Updates
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code:: bash

   sudo softwareupdate -i -a
   sudo defaults write /Library/Preferences/com.apple.SoftwareUpdate AutomaticallyInstallMacOSUpdates -int 1
   sudo shutdown -r now

Full disk encryption
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

FileVault provides full disk (technically, full volume) encryption on macOS.

FileVault encryption protects data at rest and hardens (but not always prevents) someone with physical access from stealing data or tampering with your Mac.

With much of the cryptographic operations happening efficiently in hardware, the performance penalty for FileVault is not noticeable.

Enable FileVault with *sudo fdesetup enable* or through System Preferences > Security & Privacy and reboot.

Optional Enforce system hibernation and evict FileVault keys from memory instead of traditional sleep to memory:

.. code:: bash

   $ sudo pmset -a destroyfvkeyonstandby 1
   $ sudo pmset -a hibernatemode 25

If you choose to evict FileVault keys in standby mode, you should also modify your standby and power nap settings. These settings can be changed with:

.. code:: bash

   $ sudo pmset -a powernap 0
   $ sudo pmset -a standby 0
   $ sudo pmset -a standbydelay 0
   $ sudo pmset -a autopoweroff 0

The most do it all script
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
- https://gitlab.com/vPierre/initial_laptop_setup/-/blob/main/scripts/macos_install_complete.sh

Turn off netbiosd (WINS)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``launchctl unload -w /System/Library/LaunchDaemons/com.apple.netbiosd.plist``

Disable Captive Portal (use your browser instead):
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``sudo defaults write /Library/Preferences/SystemConfiguration/com.apple.captive.control Active -bool false``

Secure FileVault When Sleeping
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``sudo sh -c 'pmset -a destroyfvkeyonstandby 1; pmset -a hibernatemode 25; pmset -a powernap 0; pmset -a standby 0; pmset -a standbydelay 0; pmset -a autopoweroff 0'``

.. Info::
   To see all hardware ports w/ MAC Addresses, run 
   
   ..

   ``sudo networksetup -listallhardwareports```

script hax.sh
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

macOS provisioning, hardening, tweaking, whatever script - no more phone home are partly done with the script ``hax.sh`` in ``/scripts/``. 

Hardening deep dive
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. Rubric:: Firmware

Setting a firmware password prevents a Mac from starting up from any device other than the startup disk. It may also be set to be required on each boot. This may be useful for mitigating some attacks which require physical access to hardware.

.. code:: bash

   $ sudo firmwarepasswd -setpasswd -setmode command
   $ sudo firmwarepasswd -verify

.. Rubric:: Hosts file

Use the hosts file to block known malware, advertising or otherwise unwanted domains.

Edit the hosts file as root, for example with sudo vi ``/etc/hosts``. 

To block a domain by A record, append any one of the following lines to ``/etc/hosts``:

.. code:: bash

   0 example.com
   0.0.0.0 example.com
   127.0.0.1 example.com

.. Notes:: IPv6 uses the AAAA DNS record type, rather than A record type, so you may also want to block those connections by also including ::1 example.com entries, like shown here.

There are many lists of domains available online which you can paste in, just make sure each line starts with 0, 0.0.0.0, 127.0.0.1, and the line 127.0.0.1 localhost is included.

Here are some popular and useful hosts lists:

- StevenBlack/hosts [4]_ 
- https://gitlab.com/vPierre/dns_hosts_updates/ [6]_ 

Append a list of hosts with the tee command and confirm only non-routable addresses or comments were added:

.. code:: bash

   $ curl https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts | sudo tee -a /etc/hosts

   $ wc -l /etc/hosts
   65580

   $ 
   egrep -ve "^#|^255.255.255.255|^127.|^0.|^::1|^ff..::|^fe80::" /etc/hosts | sort | uniq | egrep -e "[1,2]|::"
   [No output]

.. Rubric:: dnscrypt

To encrypt outgoing DNS traffic, consider using jedisct1/dnscrypt-proxy. In combination with dnsmasq and DNSSEC, the integrity and authenticity of DNS traffic is greatly improved.

JayBrown/DNSCrypt-Menu and jedisct1/bitbar-dnscrypt-proxy-switcher provide a graphical user interface to dnscrypt.

Install dnscrypt from Homebrew and follow the instructions to configure and start dnscrypt-proxy:

.. code:: bash

   $ brew install dnscrypt-proxy

If using in combination with Dnsmasq, find the file homebrew.mxcl.dnscrypt-proxy.plist by running

.. code:: bash

   $ brew info dnscrypt-proxy

which will show a location like /usr/local/etc/dnscrypt-proxy.toml

Open it in a text editor, find the line starting with listen_addresses = and edit that line to use DNScrypt on a port other than 53, like 5355:

.. code:: bash

   listen_addresses = ['127.0.0.1:5355', '[::1]:5355']
   Start DNSCrypt:

   $ sudo brew services restart dnscrypt-proxy

Make sure DNSCrypt is running:

.. code:: bash

   $ sudo lsof +c 15 -Pni UDP:5355
   COMMAND          PID   USER   FD   TYPE             DEVICE SIZE/OFF NODE NAME
   dnscrypt-proxy 15244 nobody    7u  IPv4 0x1337f85ff9f8beef      0t0  UDP 127.0.0.1:5355
   dnscrypt-proxy 15244 nobody   10u  IPv6 0x1337f85ff9f8beef      0t0  UDP [::1]:5355
   dnscrypt-proxy 15244 nobody   12u  IPv4 0x1337f85ff9f8beef      0t0  UDP 127.0.0.1:5355
   dnscrypt-proxy 15244 nobody   14u  IPv6 0x1337f85ff9f8beef      0t0  UDP [::1]:5355

By default, dnscrypt-proxy runs on localhost (127.0.0.1), port 53, and under the "nobody" user using the resolvers specified in https://raw.githubusercontent.com/DNSCrypt/dnscrypt-resolvers/master/v2/public-resolvers.md. If you would like to change these settings, you will have to edit the configuration file (e.g. listen_addresses, user_name, urls, etc.)

This can be accomplished by editing /usr/local/etc/dnscrypt-proxy.toml as described above.

You can run your own dnscrypt server (see also drduh/Debian-Privacy-Server-Guide#dnscrypt) from a trusted location or use one of many public servers instead.

Confirm outgoing DNS traffic is encrypted:

.. code:: bash

   $ sudo tcpdump -qtni en0
   IP 10.8.8.8.59636 > 107.181.168.52: UDP, length 512
   IP 107.181.168.52 > 10.8.8.8.59636: UDP, length 368

   $ 
   
   
   d0wn-us-ns4

dnscrypt-proxy also has the capability to blacklist domains, including the use of wild-cards. See the Sample configuration file for dnscrypt-proxy for the options.

Note Applications and programs may resolve DNS using their own provided servers. If dnscrypt-proxy is used, it is possible to disable all other, non-dnscrypt DNS traffic with the following pf rules:

.. code:: bash

   block drop quick on !lo0 proto udp from any to any port = 53
   block drop quick on !lo0 proto tcp from any to any port = 53

.. Rubric:: Dnsmasq

Among other features, dnsmasq is able to cache replies, prevent upstream queries for unqualified names, and block entire top-level domain names.

Use in combination with DNSCrypt to additionally encrypt outgoing DNS traffic.

If you don't wish to use DNSCrypt, you should at least use DNS not provided by your ISP. Two popular alternatives are Google DNS and OpenDNS.

(Optional) DNSSEC is a set of extensions to DNS which provide to DNS clients (resolvers) origin authentication of DNS data, authenticated denial of existence, and data integrity. All answers from DNSSEC protected zones are digitally signed. The signed records are authenticated via a chain of trust, starting with a set of verified public keys for the DNS root-zone. The current root-zone trust anchors may be downloaded from IANA website. There are a number of resources on DNSSEC, but probably the best one is dnssec.net website.

Install Dnsmasq (DNSSEC is optional):

.. code:: bash

   $ brew install dnsmasq --with-dnssec

Download drduh/config/dnsmasq.conf:

.. code:: bash

   $ curl -o homebrew/etc/dnsmasq.conf https://raw.githubusercontent.com/drduh/config/master/dnsmasq.conf

.. code:: bash

   # https://github.com/drduh/config/blob/master/dnsmasq.conf
   # http://www.thekelleys.org.uk/dnsmasq/docs/dnsmasq-man.html
   listen-address=127.0.0.1
   #listen-address=127.0.0.1,10.8.1.1,172.16.1.1,192.168.1.1
   #dhcp-range=172.16.1.2,172.16.1.15,20h
   #dhcp-option=option:router,172.16.1.1
   #dhcp-option=option:ntp-server,192.168.1.1,10.8.1.1
   #dhcp-script=/etc/dnsmasq-dhcp.sh
   #address=/local.upload/10.8.1.1
   addn-hosts=/etc/dns-blocklist
   bogus-priv
   cache-size=2000
   domain-needed
   no-poll
   no-resolv
   rebind-localhost-ok
   stop-dns-rebind
   log-dhcp
   log-facility=/var/log/dnsmasq
   #log-facility=/tmp/dnsmasq
   #log-queries
   # Block AAAA queries
   #address=/COM/::
   #server=/COM/192.168.1.1
   #
   # See drduh/config/domains for domains to block
   #
   # Un-comment one or multiple 'server' lines:
   # Local network router(s)
   #server=192.168.1.1
   #server=10.8.1.1
   # DNSCrypt on localhost port 4200
   #server=127.0.0.1#4200
   # Tor on localhost port 53
   #server=127.26.255.1
   # Cloudflare
   #server=1.0.0.1
   #server=1.1.1.1
   # OpenDNS
   #server=208.67.220.220
   #server=208.67.222.222
   # L3
   #server=4.2.2.1
   #server=4.2.2.2
   #server=4.2.2.3
   #server=4.2.2.4
   #server=4.2.2.5
   #server=4.2.2.6
   #server=209.244.0.3
   #server=209.244.0.4
   # Google
   #server=8.8.4.4
   #server=8.8.8.8
   # Verisign
   #server=64.6.64.6
   #server=64.6.65.6
   # Hurricane Electric
   #server=74.82.42.42
   # UncensoredDNS:
   #server=91.239.100.100
   # Quad9
   #server=9.9.9.9
   #server=9.9.9.10
   #server=149.112.112.9
   #server=149.112.112.10
   #server=149.112.112.112

Edit the file and examine all the options. To block entire levels of domains, append drduh/config/domains or your own rules.

Install and start the program (sudo is required to bind to privileged port 53):

.. code:: bash

   $ sudo brew services start dnsmasq

To set Dnsmasq as your local DNS server, open System Preferences > Network and select the active interface, then the DNS tab, select + and add 127.0.0.1, or use:

.. code:: bash

   $ sudo networksetup -setdnsservers "Wi-Fi" 127.0.0.1

Make sure Dnsmasq is correctly configured:

.. code:: bash

   $ scutil --dns | head

DNS configuration

.. code:: bash

   resolver #1
   search domain[0] : whatever
   nameserver[0] : 127.0.0.1
   flags    : Request A records, Request AAAA records
   reach    : 0x00030002 (Reachable,Local Address,Directly Reachable Address)

   $ networksetup -getdnsservers "Wi-Fi"
   127.0.0.1

.. Notes:: Some VPN software overrides DNS settings on connect. See issue #24 and drduh/config/scripts/macos-dns.sh.

Test DNSSEC validation

Test DNSSEC validation succeeds for signed zones - the reply should have NOERROR status and contain ad flag:

.. code:: bash

   $ dig +dnssec icann.org
   ;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 47039
   ;; flags: qr rd ra ad; QUERY: 1, ANSWER: 2, AUTHORITY: 0, ADDITIONAL: 1
   Test DNSSEC validation fails for zones that are signed improperly - the reply should have SERVFAIL status:

   $ dig www.dnssec-failed.org
   ;; ->>HEADER<<- opcode: QUERY, status: SERVFAIL, id: 15190
   ;; flags: qr rd ra; QUERY: 1, ANSWER: 0, AUTHORITY: 0, ADDITIONAL: 1

.. Rubric:: Captive portal

When macOS connects to new networks, it checks for Internet connectivity and may launch a Captive Portal assistant utility application.

.. code:: bash

   sudo defaults write /Library/Preferences/SystemConfiguration/com.apple.captive.control.plist Active -bool false

.. Rubric:: Web Privoxy

Consider using Privoxy as a local proxy to filter Web browsing traffic.

Note macOS proxy settings are not universal; apps and services may not honor system proxy settings. Ensure the application you wish to proxy is correctly configured and manually verify connections don't leak. Additionally, it may be possible to configure the pf firewall to transparently proxy all traffic.

.. code:: bash

   $ brew install privoxy

   $ brew services start privoxy

By default, privoxy listens on localhost, TCP port 8118.

Set the system HTTP proxy for your active network interface 127.0.0.1 and 8118 (This can be done through System Preferences > Network > Advanced > Proxies):

.. code:: bash

   $ sudo networksetup -setwebproxy "Wi-Fi" 127.0.0.1 8118

(Optional) Set the system HTTPS proxy, which still allows for domain name filtering, with:

.. code:: bash

   $ sudo networksetup -setsecurewebproxy "Wi-Fi" 127.0.0.1 8118

Confirm the proxy is set:

.. code:: bash

   $ scutil --proxy
   <dictionary> {
   ExceptionsList : <array> {
      0 : *.local
      1 : 169.254/16
   }
   FTPPassive : 1
   HTTPEnable : 1
   HTTPPort : 8118
   HTTPProxy : 127.0.0.1
   }

Visit http://p.p/ in a browser, or with Curl:

.. code:: bash

   $ ALL_PROXY=127.0.0.1:8118 curl -I http://p.p/
   HTTP/1.1 200 OK
   Content-Length: 2401
   Content-Type: text/html
   Cache-Control: no-cache

Privoxy already comes with many good rules, however you can also write your own.

Download drduh/config/privoxy/config and drduh/config/privoxy/user.action to get started:

.. code:: bash

   $ curl -o homebrew/etc/privoxy/config https://raw.githubusercontent.com/drduh/config/master/privoxy/config

   $ curl -o homebrew/etc/privoxy/user.action https://raw.githubusercontent.com/drduh/config/master/privoxy/user.action

Restart Privoxy and verify traffic is blocked or redirected:

.. code:: bash

   $ sudo brew services restart privoxy

   $ ALL_PROXY=127.0.0.1:8118 curl ads.foo.com/ -IL
   HTTP/1.1 403 Request blocked by Privoxy
   Content-Type: image/gif
   Content-Length: 64
   Cache-Control: no-cache

   $ ALL_PROXY=127.0.0.1:8118 curl imgur.com/ -IL
   HTTP/1.1 302 Local Redirect from Privoxy
   Location: https://imgur.com/
   Content-Length: 0

   HTTP/1.1 200 OK
   Content-Type: text/html; charset=utf-8

You can replace ad images with pictures of kittens, for example, by starting a local Web server and redirecting blocked requests to localhost.

.. Rubric:: Browser

The Web browser poses the largest security and privacy risk, as its fundamental job is to download and execute untrusted code from the Internet. This is an important statement. The unique use case of Web Browsers of operation in hostile environments, has forced them to adopt certain impressive security features. The cornerstone of Web Browser security is the Same Origin Policy (SOP). In a few words, SOP prevents a malicious script on one page from obtaining access to sensitive data on another web page through that page's Document Object Model (DOM). If SOP is compromised, the security of the whole Web Browser is compromised.

.. Notes:: More settings in :ref:`Privacy<Privacy Settings in Web Browser and other Hints>`

.. Rubric:: Plugins

Adobe Flash, Oracle Java, Adobe Reader, Microsoft Silverlight (Netflix now works with HTML5) and other plugins are security risks and should not be installed.

Backup with Time Machine
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Please have a look on the Time_Machine.rst in this Repo.

Improve your ssh server settings
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
see description and needed example 😭[8]_ 

Other interested application not covered by automation
---------------------------------------------------------------------

- A personal search engine: Create a searchable library from your personal documents, interests, and more! Spyglass
  - <https://github.com/spyglass-search/spyglass>
  - <https://github.com/spyglass-search/spyglass/releases>

- <https://gitlab.com/vPierre/initial_laptop_setup/-/blob/main/scripts/macos_download_not_covered_apps.sh?ref_type=heads>

- tbd.

Further readings
---------------------------------------------------------------------

- https://gitlab.com/vPierre/ndaal_sicherheitskonzept

.....

.. toctree::
   :maxdepth: 2
   :numbered:
   :caption: Proceed with the following Content:

   General_Configuration.rst
   Privacy.rst
   Strong_Passwords.rst

.....

.. Rubric:: Footnotes

.. [1]
   https://docs.brew.sh/

.. [2]
   https://developer.apple.com/xcode/

.. [3]
   https://support.apple.com/guide/macbook-pro/set-up-your-mac-apd831707cb3/mac

.. [4]
   https://github.com/StevenBlack/hosts

.. [5]
   https://github.com/buo/homebrew-cask-upgrade

.. [6]
   https://gitlab.com/vPierre/dns_hosts_updates/ 

.. [7]
   https://gitlab.com/vPierre/ndaal_git_ignore

.. [8]
   https://gitlab.com/vPierre/ssh_config
